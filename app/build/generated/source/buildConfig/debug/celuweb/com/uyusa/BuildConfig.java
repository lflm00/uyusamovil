/**
 * Automatically generated file. DO NOT MODIFY
 */
package celuweb.com.uyusa;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "celuweb.com.uyusa";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 105;
  public static final String VERSION_NAME = "1.0.5";
}
