package celuweb.com.keyboard.custom;

import android.app.Activity;
import android.content.DialogInterface;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.text.Editable;
import android.text.InputType;
import android.text.Layout;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import celuweb.com.uyusa.R;

/**
 * When an activity hosts a keyboardView, this class allows several EditText's to register for it.
 *
 * @author Maarten Pennings, extended by SimplicityApks
 * @date   2012 December 23
 */
public class CustomKeyboard implements android.content.DialogInterface.OnClickListener {

	/** A link to the KeyboardView that is used to render this CustomKeyboard. */
	private KeyboardView mKeyboardView;
	/** A link to the activity that hosts the {@link #mKeyboardView}. */
	private Activity mHostActivity;
	private boolean hapticFeedback;

	/** The key (code) handler. */
	private OnKeyboardActionListener mOnKeyboardActionListener = new OnKeyboardActionListener() {

		// add your own special keys here:
		public final static int CodeDelete   = -5; // Keyboard.KEYCODE_DELETE
		public final static int CodeEnie     = 165;
		public final static int CodEspacio     = 3703;
		public final static int cambioTecladoNom = 3701;
		public final static int cambioTecladoDir = 3702;
		
		
		//definicion de constantes usadas por el teclado de nomenclatura de direcciones
		public final static int AV = 4900;
		public final static int CL = 4901;
		public final static int DG = 4902;
		public final static int MZ = 4903;
		public final static int CS = 4904;
		public final static int AK = 4905;
		public final static int TV = 4906;
		public final static int KR = 4907;
		public final static int ET = 4908;
		public final static int AC = 4909;
		
		public final static int ED = 5000;
		public final static int AUT = 5001;
		public final static int BLQ = 5002;
		public final static int INT = 5003;
		public final static int APT = 5004;
		public final static int CIR = 5005;
		public final static int AGR = 5006;
		public final static int MOD = 5007;
		public final static int TO = 5008;
		
		public final static int CC = 5100;
		public final static int BG = 5101;
		public final static int OF = 5102;
		public final static int LOC = 5103;
		public final static int CRT = 5104;
		public final static int PLZ = 5105;
		public final static int VIA = 5106;
		public final static int KM = 5107;
		public final static int CN = 5108;
		public final static int LT = 5109;
		
		public final static int URB = 5200;
		public final static int SEC = 5201;
		public final static int AER = 5202;
		public final static int CLJ = 5203;
		public final static int VTE = 5204;
		public final static int ESQ = 5205;
		public final static int PTO = 5206;
		public final static int SOT = 5207;
		public final static int CRV = 5208;
		
		public final static int PISO = 5300;
		public final static int CONJ = 5301;
		public final static int UNID = 5302;
		public final static int PORT = 5303;
		public final static int VRD = 5304;
		public final static int DEP = 5305;
		
		public final static int TER = 5400;
		public final static int BLV = 5402;
		public final static int OESTE = 5403;
		public final static int ESTE = 5404;
		public final static int NORTE = 5405;
		public final static int SUR = 5406;
		
		

		@Override public void onKey(int primaryCode, int[] keyCodes) {

			View focusCurrent = mHostActivity.getWindow().getCurrentFocus();
			if( focusCurrent==null || focusCurrent.getClass()!=EditText.class ){
				return;
			} 
			EditText edittext = (EditText) focusCurrent;
			Editable editable = edittext.getText();
			int start = edittext.getSelectionStart();
			
			// borrar caracter si es seleccionado:
			int end = edittext.getSelectionEnd();
			if(end > start){
				editable.delete(start, end);
			}
			
			switch (primaryCode) {
			case cambioTecladoNom: // CAMBIAR A TECLADO DE NOMENCLATURA DE DIRECCIONES
				mKeyboardView.setKeyboard(new Keyboard(mHostActivity, R.xml.keyboard_direccion));
				showCustomKeyboard(focusCurrent);
				break;
			case cambioTecladoDir: // CAMBIAR A TECLADO CONVENCIONAL
				mKeyboardView.setKeyboard(new Keyboard(mHostActivity, R.xml.keyboard_nom));
				showCustomKeyboard(focusCurrent);
				break;
			
			case CodEspacio:
				editable.insert(end, " ");
				break;
				
			case CodeDelete:
				if(start > 0)editable.delete(start - 1, start);
				break;
				
			case CodeEnie:
				editable.insert(end, "�");
				break;
				
				
			case AV:
				editable.insert(end, "AV ");
				break;
			case CL:
				editable.insert(end, "CL ");
				break;
			case DG:
				editable.insert(end, "DG ");
				break;
			case MZ:
				editable.insert(end, "MZ ");
				break;
			case CS:
				editable.insert(end, "CS ");
				break;
			case AK:
				editable.insert(end, "AK ");
				break;
			case TV:
				editable.insert(end, "TV ");
				break;
			case KR:
				editable.insert(end, "KR ");
				break;
			case ET:
				editable.insert(end, "ET ");
				break;
			case AC:
				editable.insert(end, "AC ");
				break;
			
			
			case ED:
				editable.insert(end, "ED ");
				break;
			case AUT:
				editable.insert(end, "AUT ");
				break;
			case BLQ:
				editable.insert(end, "BLQ ");
				break;
			case INT:
				editable.insert(end, "INT ");
				break;
			case APT:
				editable.insert(end, "APT ");
				break;
			case CIR:
				editable.insert(end, "CIR ");
				break;
			case AGR:
				editable.insert(end, "AGR ");
				break;
			case MOD:
				editable.insert(end, "MOD ");
				break;
			case TO:
				editable.insert(end, "TO ");
				break;
				
				
			case CC:
				editable.insert(end, "CC ");
				break;
			case BG:
				editable.insert(end, "BG ");
				break;
			case OF:
				editable.insert(end, "OF ");
				break;
			case LOC:
				editable.insert(end, "LOC ");
				break;
			case CRT:
				editable.insert(end, "CRT ");
				break;
			case PLZ:
				editable.insert(end, "PLZ ");
				break;
			case VIA:
				editable.insert(end, "VIA ");
				break;
			case KM:
				editable.insert(end, "KM ");
				break;
			case CN:
				editable.insert(end, "CN ");
				break;
			case LT:
				editable.insert(end, "LT ");
				break;
				
			
			case URB:
				editable.insert(end, "URB ");
				break;
			case SEC:
				editable.insert(end, "SEC ");
				break;
			case AER:
				editable.insert(end, "AER ");
				break;
			case CLJ:
				editable.insert(end, "CLJ ");
				break;
			case VTE:
				editable.insert(end, "VTE ");
				break;
			case ESQ:
				editable.insert(end, "ESQ ");
				break;
			case PTO:
				editable.insert(end, "PTO ");
				break;
			case SOT:
				editable.insert(end, "SOT ");
				break;
			case CRV:
				editable.insert(end, "CRV ");
				break;
				
				
			case PISO:
				editable.insert(end, "PISO ");
				break;
			case CONJ:
				editable.insert(end, "CONJ ");
				break;
			case UNID:
				editable.insert(end, "UNID ");
				break;
			case PORT:
				editable.insert(end, "PORT ");
				break;
			case VRD:
				editable.insert(end, "VRD ");
				break;
			case DEP:
				editable.insert(end, "DEP ");
				break;
				
				
			case TER:
				editable.insert(end, "TER ");
				break;
			case BLV:
				editable.insert(end, "BLV ");
				break;
			case OESTE:
				editable.insert(end, "OESTE ");
				break;
			case ESTE:
				editable.insert(end, "ESTE ");
				break;
			case NORTE:
				editable.insert(end, "NORTE ");
				break;
			case SUR:
				editable.insert(end, "SUR ");
				break;
			default:
				// insertar caracter ascii
				editable.insert(start, Character.toString((char) primaryCode));  
				break;
			}			
		}

		@Override public void onPress(int arg0) {
			// vibrate if haptic feedback is enabled:
			if(hapticFeedback && arg0!=0)
				mKeyboardView.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		}

		@Override public void onRelease(int primaryCode) {
		}

		@Override public void onText(CharSequence text) {
		}

		@Override public void swipeDown() {
		}

		@Override public void swipeLeft() {
		}

		@Override public void swipeRight() {
		}

		@Override public void swipeUp() {
		}
	};

	/**
	 * Create a custom keyboard, that uses the KeyboardView (with resource id <var>viewid</var>) of the <var>host</var> activity,
	 * and load the keyboard layout from xml file <var>layoutid</var> (see {@link Keyboard} for description).
	 * Note that the <var>host</var> activity must have a <var>KeyboardView</var> in its layout (typically aligned with the bottom of the activity).
	 * Note that the keyboard layout xml file may include key codes for navigation; see the constants in this class for their values.
	 * Note that to enable EditText's to use this custom keyboard, call the {@link #registerEditText(int)}.
	 *
	 * @param host The hosting activity.
	 * @param viewid The id of the KeyboardView.
	 * @param layoutid The id of the xml file containing the keyboard layout.
	 */
	public CustomKeyboard(final Activity host, final int viewid, final int layoutid) {
		mHostActivity = host;
		mKeyboardView= (KeyboardView)mHostActivity.findViewById(viewid);
		mKeyboardView.setKeyboard(new Keyboard(mHostActivity, layoutid));
		mKeyboardView.setPreviewEnabled(false); // NOTE Do not show the preview balloons
		mKeyboardView.setOnKeyboardActionListener(mOnKeyboardActionListener);
		// Hide the standard keyboard initially
		mHostActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
	}

	/** Returns whether the CustomKeyboard is visible. */
	public boolean isCustomKeyboardVisible() {
		return mKeyboardView.getVisibility() == View.VISIBLE;
	}

	/** Make the CustomKeyboard visible, and hide the system keyboard for view v. */
	public void showCustomKeyboard( View v ) {
		mKeyboardView.setVisibility(View.VISIBLE);
		mKeyboardView.setEnabled(true);
		if( v!=null ) ((InputMethodManager)mHostActivity.getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	/** Make the CustomKeyboard invisible. */
	public void hideCustomKeyboard() {
		mKeyboardView.setVisibility(View.GONE);
		mKeyboardView.setEnabled(false);
	}

	/**
	 * Register <var>EditText<var> with resource id <var>resid</var> (on the hosting activity) for using this custom keyboard.
	 *
	 * @param resid The resource id of the EditText that registers to the custom keyboard.
	 */
	public void registerEditText(int resid) {
		// Find the EditText 'resid'
		final EditText edittext= (EditText)mHostActivity.findViewById(resid);
		// Make the custom keyboard appear
		edittext.setOnFocusChangeListener(new OnFocusChangeListener() {
			// NOTE By setting the on focus listener, we can show the custom keyboard when the edit box gets focus, but also hide it when the edit box loses focus
			@Override 
			public void onFocusChange(View v, boolean hasFocus) {
				if( hasFocus ){
					showCustomKeyboard(v); 
				} 
				else hideCustomKeyboard();
				
			}
		});
		edittext.setOnClickListener(new OnClickListener() {
			// NOTE By setting the on click listener, we can show the custom keyboard again, by tapping on an edit box that already had focus (but that had the keyboard hidden).
			@Override 
			public void onClick(View v) {
				showCustomKeyboard(v);
			}
		});
		// Disable standard keyboard hard way
		// NOTE There is also an easy way: 'edittext.setInputType(InputType.TYPE_NULL)' (but you will not have a cursor, and no 'edittext.setCursorVisible(true)' doesn't work )
		edittext.setOnTouchListener(new OnTouchListener() {
			@Override 
			public boolean onTouch(View v, MotionEvent event) {
				EditText edittext = (EditText) v;
				
				if(event.getAction() == MotionEvent.ACTION_MOVE){
					 Layout layout = edittext.getLayout();
                     float x = event.getX() + edittext.getScrollX();
                     int offset = layout.getOffsetForHorizontal(0, x);
                     if (offset > 0)
                         if (x > layout.getLineMax(0))
                             edittext.setSelection(offset);     // Touchpoint was at the end of the text
                         else
                             edittext.setSelection(offset - 1);	
                     edittext.setCursorVisible(true);
				}
				else {
					int inType = edittext.getInputType();       // Backup the input type
					edittext.setInputType(InputType.TYPE_NULL); // Disable standard keyboard
					edittext.onTouchEvent(event);               // Call native handler
					edittext.setInputType(inType);              // Restore input type
					edittext.setCursorVisible(true);					
				}
				return true; // Consume touch event
			}
		});        
        
     // Disable spell check (hex strings look like words to Android)
        edittext.setInputType(edittext.getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }
	
	

	

	



	/**
	 * @param popupCode Code indicating which popup is currently opened for the onClick getter
	 */
	private int popupCode;
	@Override
	public void onClick(DialogInterface dialog, int which) { // insert one of the additional popups to the current Focus field
		String insertion="";
		switch(popupCode){
		case 55009:
//			insertion = mHostActivity.getResources().getStringArray(R.array.insert_const_values)[which];
			break;
		case 55010:
//			insertion = mHostActivity.getResources().getStringArray(R.array.insert_log_values)[which];
			break;
		case 55011:
//			insertion = mHostActivity.getResources().getStringArray(R.array.insert_conv_values)[which];
			break;
		case 55012:
//			insertion = mHostActivity.getResources().getStringArray(R.array.insert_trig_values)[which];
			break;
		}
		
		View focusCurrent = mHostActivity.getWindow().getCurrentFocus();
		if( focusCurrent==null || focusCurrent.getClass()!=EditText.class ) return;
		EditText edittext = (EditText) focusCurrent;
		Editable editable = edittext.getText();
		int start = edittext.getSelectionStart();
		// delete the selection, if chars are selected:
		int end = edittext.getSelectionEnd();
		editable.replace(start, end, insertion);
	}

	/**
	 * Enables or disables the Haptic feedback on keyboard touches
	 * @param goEnabled true if you want haptic feedback, falso otherwise
	 */
	public void enableHapticFeedback(boolean goEnabled){
		mKeyboardView.setHapticFeedbackEnabled(goEnabled);
		hapticFeedback = goEnabled;
	}
	
	
}