package celuweb.com.BusinessObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.util.Vector;

//import celuweb.com.DataObject.CicloVisita;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.Util;


public class DataBaseBOJF {

	public static String mensaje;

	public static Vector<Cliente> ListaClientesRutero(Vector<ItemListView> listaItems, String dia) {

		mensaje = "";
		Cliente cliente;
		SQLiteDatabase db = null;
        int contadorRutero = 0;
		ItemListView itemListView;
		Vector<Cliente> listaClientes = new Vector<Cliente>();
		Usuario usuario = DataBaseBO.ObtenerUsuario();

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			String query = "";

			if (usuario.tipoRutero != 0) {

				int diaRuta2 = Integer.parseInt(dia);
//				diaRuta2 = 4;

//				if (diaRuta2 > 3 && diaRuta2 < 11) {
				if (diaRuta2 >= 0  && diaRuta2 < 11) {

					String condicion = "substr(rutanueva, " + diaRuta2 + ",1) = '1'";

//					query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, "
//							+ "TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, rutanueva, "
//							+ "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "
////							+ "0 AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "
//
//							+ "from clientes " + "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
//							+ "and Rutero.pendiente = 0 " + "WHERE " + condicion + " "
//							+ "AND clientes.bloqueado <> 'S' " + "order by rutero.ordenVisita";

					query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad,  Canal, SubCanal, Cupo, "
							+ "TipoCredito, DANE, Bloqueado, diasIngreso, bodega,cliacummesbon, "
//							+ "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "
							+ "0 AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "

							+ "from clientes " + "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
							+ "and Rutero.pendiente = 0 " + "WHERE "
							+ " Rutero.CodDia = "+diaRuta2+""
							+ " and  clientes.bloqueado <> 'S' " + "order by rutero.ordenVisita";

					System.out.println("Consulta --> " + query);

				} else if (diaRuta2 == 11) {

					String condicion = "rutanueva = '" + usuario.codigoVendedor + "000000" + "' ";

//					query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, "
//							+ "TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, rutanueva, "
//							+ "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "
//							+ "from clientes " + "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
//							+ "and Rutero.pendiente = 0 " + "and " + condicion + " "
//							+ "WHERE clientes.bloqueado <> 'S' " + "order by rutero.ordenVisita";

					query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, Canal, SubCanal, Cupo, "
							+ "TipoCredito, DANE, Bloqueado, diasIngreso, bodega,cliacummesbon, "
							+ "0 AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "
							+ "from clientes " + "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
							+ " and Rutero.pendiente = 0 "
							+ " WHERE Rutero.CodDia = "+diaRuta2+ " and  clientes.bloqueado <> 'S' " + "order by rutero.ordenVisita";

				}

			} else {

//				query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, "
//						+ "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "
//						+ "from clientes "
//						+ "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo and Rutero.pendiente = 0 "
//						+ "and Rutero.diaVisita = '" + dia + "' " + "WHERE clientes.bloqueado <> 'S' "
//						+ "order by rutero.ordenVisita";
				query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, Canal, SubCanal, Cupo, TipoCredito, DANE, Bloqueado, diasIngreso, bodega,cliacummesbon, "
						+ "0 AS oportunidad,Clientes.latitud as latitud, Clientes.longitud as longitud "
						+ "from clientes "
						+ "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo and Rutero.pendiente = 0 "
						+ "and Rutero.CodDia = '" + dia + "' " + "WHERE clientes.bloqueado <> 'S' "
						+ "order by rutero.ordenVisita";
			}
			System.out.println("query-> mapa: ----> "+query);
			Cursor cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {

				do {

					cliente = new Cliente();
					itemListView = new ItemListView();
					contadorRutero++;
					cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
					cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
					cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
					cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
					cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
					cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
					cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
//					cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
					cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
					cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
					cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
					cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
					cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
					cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
//					cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2"));
					cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));
					cliente.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
					cliente.AcomuladoBonif = cursor.getString(cursor.getColumnIndex("cliacummesbon"));
					cliente.oportunidad = cursor.getInt(cursor.getColumnIndex("oportunidad"));
					
					cliente.latitud = cursor.getDouble(cursor.getColumnIndex("latitud"));
					cliente.longitud = cursor.getDouble(cursor.getColumnIndex("longitud"));
					
					cliente.contadorRutero = contadorRutero;

					itemListView.titulo = cliente.codigo + " - " + cliente.Nombre;
					itemListView.subTitulo = cliente.direccion;

					// si hay oportunidad remarcar la fila
					if (cliente.oportunidad >= 1) {
						itemListView.resaltado = 1;
					}

					listaItems.add(itemListView);
					listaClientes.addElement(cliente);

				} while (cursor.moveToNext());

				mensaje = "Rutero Cargado Correctamente";

			} else {

				mensaje = "Consulta sin Resultados";
			}

			if (cursor != null)
				cursor.close();

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("ListaClientesRutero", mensaje, e);

		} finally {

			if (db != null)
				db.close();
		}

		return listaClientes;
	}

	
	public static Vector<Encabezado> CargarPedidosRealizadosMaMapa(String Cliente) {



		mensaje = "";
		SQLiteDatabase db = null;
		ItemListView itemListView;

		Encabezado encabezado;
		Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");

			if (dbFile.exists()) {

				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

				String query = "";

				query = "select Description, Quantity from CustomerSales where CustomerId='"+Cliente+"' ";
				Cursor cursor = db.rawQuery(query, null);

				if (cursor.moveToFirst()) {

					do {

						encabezado = new Encabezado();
						encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("Quantity"));
						encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("Description"));
				 
						listaPedidos.addElement(encabezado);

					} while (cursor.moveToNext());

				}

				if (cursor != null)
					cursor.close();

			} else {

				
			}

		} catch (Exception e) {

			mensaje = e.getMessage();
			

		} finally {

			if (db != null)
				db.close();
		}

		return listaPedidos;

	

	}
	
	public static boolean GuardarFotoNoCompra(String codCliente,String codVendedor,String numeroDoc,byte[] image) {

		SQLiteDatabase dbPedido = null;

		try {

			File filePedido = new File(Util.DirApp(), "Temp.db");
			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			ContentValues valuesTemp = new ContentValues();

			if (Main.fotoNoCompra != null && image != null) {

				valuesTemp = new ContentValues();
				valuesTemp.put("Id", "A" + Util.ObtenerFechaId());
				valuesTemp.put("Imagen", image);
				valuesTemp.put("CodCliente", codCliente);
				valuesTemp.put("CodVendedor", codVendedor);
				valuesTemp.put("modulo", 1);
				valuesTemp.put("nroDoc", numeroDoc);

				dbPedido.insertOrThrow("Fotos", null, valuesTemp);
			}

			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			return false;

		} finally {

			if (dbPedido != null)
				dbPedido.close();
		}
	}

/*
	public static Vector<CicloVisita> listaCiclosVisita(String cliente) {

		mensaje = "";
		SQLiteDatabase db = null;

		CicloVisita cicloVisita = new CicloVisita();
		Vector<CicloVisita> listaDocumento = new Vector<CicloVisita>();

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,SQLiteDatabase.OPEN_READWRITE);

			String query = "SELECT t.codigo as codigo,MAX(t.Rango) as Rango,MAX(t.Color) as Color,COUNT(DISTINCT c.Codigo) as Cantidad "
					+" FROM TipoCicloVisita T LEFT JOIN clientes c ON t.Codigo = c.TipoCicloVisita"
					+" GROUP BY t.codigo"
					+" ORDER BY t.Codigo";

			Cursor cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {

				do {

					cicloVisita = new CicloVisita();

					cicloVisita.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
					cicloVisita.rango = cursor.getString(cursor.getColumnIndex("Rango"));
					cicloVisita.color = cursor.getString(cursor.getColumnIndex("Color"));
					cicloVisita.cantidad = cursor.getString(cursor.getColumnIndex("Cantidad"));



					listaDocumento.addElement(cicloVisita);

				} while (cursor.moveToNext());
			}

			if (cursor != null) {
				cursor.close();
			}

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("CICLO", "listaCartera -> " + mensaje, e);

		} finally {

			db.close();
		}

		return listaDocumento;
	}
*/
}
