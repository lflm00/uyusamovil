package celuweb.com.BusinessObject;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.drawable.Drawable;
import android.util.Log;

import celuweb.com.DataObject.Actividad;
import celuweb.com.DataObject.AgotadoYDevolucion;
import celuweb.com.DataObject.Autorizaciones;
import celuweb.com.DataObject.Banco;
import celuweb.com.DataObject.Bonificaciones;
import celuweb.com.DataObject.CabeceraTirilla;
import celuweb.com.DataObject.Canal;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.CarteraConsignacion;
import celuweb.com.DataObject.CarteraPendiente;
import celuweb.com.DataObject.Categoria;
import celuweb.com.DataObject.ChequePostFechado;
import celuweb.com.DataObject.Ciudad;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.ClienteNuevo;
import celuweb.com.DataObject.Concepto;
import celuweb.com.DataObject.CondicionesBonificacion;
import celuweb.com.DataObject.ConsecutivoFactura;
import celuweb.com.DataObject.ConsecutivosRecibos;
import celuweb.com.DataObject.Consignacion;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.CuadreCaja;
import celuweb.com.DataObject.DescuentoAutorizado;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.DetalleAnulacionDeRecibos;
import celuweb.com.DataObject.DetalleGasto;
import celuweb.com.DataObject.DetalleTiempos;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.Encuestas;
import celuweb.com.DataObject.EncuestasPreguntas;
import celuweb.com.DataObject.EncuestasPreguntasParametros;
import celuweb.com.DataObject.EstadisticaRecorrido;
import celuweb.com.DataObject.FacturasImpresion;
import celuweb.com.DataObject.Fiado;
import celuweb.com.DataObject.Filtro;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.DataObject.Foto;
import celuweb.com.DataObject.Impresion;
import celuweb.com.DataObject.ImpresionDetalle;
import celuweb.com.DataObject.ImpresionEncabezado;
import celuweb.com.DataObject.ImpresionNotaCredito;
import celuweb.com.DataObject.ImpresionRecaudo;
import celuweb.com.DataObject.InformacionFP;
import celuweb.com.DataObject.InformacionReciboCobro;
import celuweb.com.DataObject.InformeInventario;
import celuweb.com.DataObject.InformeRecaudo;
import celuweb.com.DataObject.ItemCarteraListView;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Linea;
import celuweb.com.DataObject.MotivoAbono;
import celuweb.com.DataObject.MotivoCambio;
import celuweb.com.DataObject.MotivoCompra;
import celuweb.com.DataObject.MotivoDescuento;
import celuweb.com.DataObject.Oportunidad;
import celuweb.com.DataObject.OrdenLiquidacion;
import celuweb.com.DataObject.PedidoAmarre;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.ProductosCompensacion;
import celuweb.com.DataObject.RecibosRecaudo;
import celuweb.com.DataObject.RegCanastilla;
import celuweb.com.DataObject.ResumenSync;
import celuweb.com.DataObject.Ruta;
import celuweb.com.DataObject.SeguimientoVenta;
import celuweb.com.DataObject.SelloCamion;
import celuweb.com.DataObject.SubCanal;
import celuweb.com.DataObject.TopeBonificacion;
import celuweb.com.DataObject.TotalInventarioDia;
import celuweb.com.DataObject.UltimoPedido;
import celuweb.com.DataObject.Usuario;
import celuweb.com.DataObject.VendedorBonificacion;
import celuweb.com.DataObject.objeto;
import celuweb.com.uyusa.Const;
import celuweb.com.uyusa.FormTerminarRecaudoActivity;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.Msg;
import celuweb.com.uyusa.R;
import celuweb.com.uyusa.Util;


@SuppressLint("NewApi")
public class DataBaseBOJ {

    public static final String TAG = "BusinessObject.DataBaseBO";

    private static File dbFile;
    public static String mensaje;

    /*public static void ActualizarSnEnvio() {

		SQLiteDatabase db = null;

		try {

			dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			db.execSQL("UPDATE Encabezado SET sn_envio = 1 WHERE sn_envio IS NULL AND sync = 1");

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("ActualizarSnEnvio", mensaje, e);

		} finally {

			if (db != null)
				db.close();
		}
	}*/

    /*public static void ActualizarSnEnvio(String id, int sn_envio) {

		SQLiteDatabase db = null;

		try {

			dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			db.execSQL("UPDATE Encabezado SET sn_envio = " + sn_envio + " WHERE id = " + id);

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("ActualizarSnEnvio", mensaje, e);

		} finally {

			if (db != null)
				db.close();
		}
	}*/

    public static void BorrarPedidosSinTerminar() {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM Detalle WHERE numDoc IN (SELECT numeroDoc FROM Encabezado WHERE fechaFinal IS NULL)");
                db.execSQL("DELETE FROM NovedadesCompras WHERE NroDoc IN (SELECT numeroDoc FROM Encabezado WHERE fechaFinal IS NULL)");
                db.execSQL("DELETE FROM Encabezado WHERE fechaFinal IS NULL");

                dbPedido.execSQL("DELETE FROM Detalle WHERE numDoc IN (SELECT numeroDoc FROM Encabezado WHERE fechaFinal IS NULL)");
                dbPedido.execSQL("DELETE FROM NovedadesCompras WHERE NroDoc IN (SELECT numeroDoc FROM Encabezado WHERE fechaFinal IS NULL)");
                dbPedido.execSQL("DELETE FROM Encabezado WHERE fechaFinal IS NULL");
                dbPedido.execSQL("VACUUM");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static void BorrarPedidosSinTerminar_() {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM DetalleAndroid WHERE numDoc IN (SELECT numeroDoc FROM EncabezadoAndroid WHERE fechaFinal IS NULL)");
                db.execSQL("DELETE FROM NovedadesComprasAndroid WHERE NroDoc IN (SELECT numeroDoc FROM EncabezadoAndroid WHERE fechaFinal IS NULL)");
                db.execSQL("DELETE FROM EncabezadoAndroid WHERE fechaFinal IS NULL");

                dbPedido.execSQL("DELETE FROM DetalleAndroid WHERE numDoc IN (SELECT numeroDoc FROM EncabezadoAndroid WHERE fechaFinal IS NULL)");
                dbPedido.execSQL("DELETE FROM NovedadesComprasAndroid WHERE NroDoc IN (SELECT numeroDoc FROM EncabezadoAndroid WHERE fechaFinal IS NULL)");
                dbPedido.execSQL("DELETE FROM EncabezadoAndroid WHERE fechaFinal IS NULL");
                dbPedido.execSQL("VACUUM");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean ExisteDataBase() {

        File dbFile = new File(Util.DirApp(), "DataBase.db");
        return dbFile.exists();
    }

    public static boolean LogIn(String usuario, String password) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigo FROM Usuario WHERE codigo = '" + usuario + "' AND clave = '" + password + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    /**
     * Metodo encargado de Verificar si el Usuario Ingresado en el Formulario de Login es igual al Ultimo Usuario que se Logueo en la Aplicacion
     **/
    public static String ObtenerUsuarioActual() {

        SQLiteDatabase db = null;
        String usuarioActual = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT usuario FROM Usuario";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst())
                usuarioActual = cursor.getString(cursor.getColumnIndex("usuario"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return usuarioActual;
    }

    public static Vector<Cliente> BuscarCliente(String where, String orderby, Vector<ItemListView> listaItemsCliente) {

        Cliente cliente;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Cliente> listaClientes = new Vector<Cliente>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        if (where.equals(""))
            where = " where tipocliente = 0";
        else
            where += " and tipocliente = 0";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "" +
                    "SELECT Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, " +
                    "Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, TipoCredito, DANE, " +
                    "Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon," +
                    "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad " +
                    "FROM Clientes " +
                    where + " " + orderby;


            System.out.println("Lista clientes: " + query);
            //"AND Codigo NOT IN (SELECT CodigoCliente FROM NovedadesCompras) " +
            //					orderby;
            //"ORDER BY codigo";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cliente = new Cliente();
                    itemListView = new ItemListView();

                    cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                    cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                    cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
                    cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
                    cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
                    cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                    cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
                    cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
                    cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
                    cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                    cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                    cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
                    cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                    cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2"));
                    cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));
                    cliente.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    cliente.AcomuladoBonif = cursor.getString(cursor.getColumnIndex("cliacummesbon"));
                    cliente.oportunidad = cursor.getInt(cursor.getColumnIndex("oportunidad"));


                    itemListView.titulo = cliente.codigo + " - " + cliente.Nombre;
                    itemListView.subTitulo = cliente.direccion;

                    //resaltar los clientes con oportunidad
                    if (cliente.oportunidad > 0) {
                        itemListView.resaltado = 1;
                    }

                    listaClientes.addElement(cliente);
                    listaItemsCliente.addElement(itemListView);

                } while (cursor.moveToNext());

                mensaje = "Busqueda de Clientes Satisfactoria";

            } else {

                mensaje = "Busqueda de Clientes sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaClientes;
    }

    public static Vector<Cliente> ClienteNoCompra(Vector<ItemListView> listaItemsCliente) {

        Cliente cliente;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Cliente> listaClientes = new Vector<Cliente>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT Clientes.Codigo, Clientes.Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, TipoCredito, DANE, " +
                    "Bloqueado, tipologia2, diasIngreso, NoCompra.Nombre AS MotivoNoCompra " +
                    "FROM novedadescompras " +
                    "INNER JOIN Clientes ON novedadescompras.codigoCliente = Clientes.Codigo " +
                    "INNER JOIN NoCompra ON novedadescompras.motivo = NoCompra.Codigo " +
                    "WHERE motivo > 1";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cliente = new Cliente();
                    itemListView = new ItemListView();

                    cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                    cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                    cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
                    cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
                    cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
                    cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                    cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
                    cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
                    cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
                    cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                    cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                    cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
                    cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                    cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2"));
                    cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));

                    String motivo = cursor.getString(cursor.getColumnIndex("MotivoNoCompra"));

                    itemListView.titulo = cliente.razonSocial;
                    itemListView.subTitulo = "Motivo: " + motivo;

                    listaClientes.addElement(cliente);
                    listaItemsCliente.addElement(itemListView);

                } while (cursor.moveToNext());

                mensaje = "Busqueda de Clientes Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaClientes;
    }


    public static Vector<Cliente> ClienteNoCompra_(Vector<ItemListView> listaItemsCliente) {

        Cliente cliente;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Cliente> listaClientes = new Vector<Cliente>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT Clientes.Codigo, Clientes.Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, TipoCredito, DANE, " +
                    "Bloqueado, tipologia2, diasIngreso, bodega, NoCompra.Nombre AS MotivoNoCompra " +
                    "FROM novedadescomprasAndroid " +
                    "INNER JOIN Clientes ON novedadescomprasAndroid.codigoCliente = Clientes.Codigo " +
                    "INNER JOIN NoCompra ON novedadescomprasAndroid.motivo = NoCompra.Codigo " +
                    "WHERE motivo > 1";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cliente = new Cliente();
                    itemListView = new ItemListView();

                    cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                    cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                    cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
                    cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
                    cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
                    cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                    cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
                    cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
                    cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
                    cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                    cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                    cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
                    cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                    cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2"));
                    cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));

                    String motivo = cursor.getString(cursor.getColumnIndex("MotivoNoCompra"));

                    itemListView.titulo = cliente.razonSocial;
                    itemListView.subTitulo = "Motivo: " + motivo;

                    listaClientes.addElement(cliente);
                    listaItemsCliente.addElement(itemListView);

                } while (cursor.moveToNext());

                mensaje = "Busqueda de Clientes Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaClientes;
    }


    public static Vector<FormaPago> CargarFormasPago(String nroDoc) {

        FormaPago formaPago;
        SQLiteDatabase db = null;
        Vector<FormaPago> listaFormasPago = new Vector<FormaPago>();
        System.out.println("FormpagoNroDoc");

        try {

            int i = 0;
            File dbFile = new File(Util.DirApp(), "Temp.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT forma_pago, monto, nro_cheque_cons, banco, cuentaCheque, serial " +
                    "FROM FormaPago " +
                    "WHERE nroDoc = '" + nroDoc + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    formaPago = new FormaPago();
                    formaPago.position = i++;
                    formaPago.nroDoc = nroDoc;

                    int cod = formaPago.formaPago = cursor.getInt(cursor.getColumnIndex("forma_pago"));
                    formaPago.descripcion = cod == 1 ? "Efectivo" : cod == 2 ? "Cheque" : "";

                    formaPago.monto = cursor.getInt(cursor.getColumnIndex("monto"));
                    formaPago.nroCheque = cursor.getString(cursor.getColumnIndex("nro_cheque_cons"));
                    formaPago.codigoBanco = cursor.getString(cursor.getColumnIndex("banco"));
                    formaPago.serial = cursor.getString(cursor.getColumnIndex("serial"));

                    listaFormasPago.addElement(formaPago);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ListaFormasPago:" + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaFormasPago;
    }

    public static Vector<Cliente> ListaClientesRutero(Vector<ItemListView> listaItems, String dia) {

        mensaje = "";
        Cliente cliente;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Cliente> listaClientes = new Vector<Cliente>();
        Usuario usuario = DataBaseBOJ.ObtenerUsuario();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";

            if (usuario.tipoRutero != 0) {

                int diaRuta2 = Integer.parseInt(dia);

                if (diaRuta2 > 3 && diaRuta2 < 11) {

                    String condicion = "substr(rutanueva, " + diaRuta2 + ",1) = '1'";

                    query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, "
                            + "TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, rutanueva, " +
                            "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad " +
                            "from clientes " +
                            "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
                            + "and Rutero.pendiente = 0 "
                            + "WHERE " + condicion + " "
                            + "AND clientes.bloqueado <> 'S' "
                            + "order by rutero.ordenVisita";

                    System.out.println("Consulta --> " + query);

                } else if (diaRuta2 == 11) {

                    String condicion = "rutanueva = '" + usuario.codigoVendedor + "000000" + "' ";

                    query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, "
                            + "TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, rutanueva, " +
                            "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad " +
                            "from clientes " +
                            "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
                            + "and Rutero.pendiente = 0 "
                            + "and " + condicion + " "
                            + "WHERE clientes.bloqueado <> 'S' "
                            + "order by rutero.ordenVisita";

                }

            } else {


                query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, " +
                        "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad " +
                        "from clientes " +
                        "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo and Rutero.pendiente = 0 "
                        + "and Rutero.diaVisita = '" + dia + "' "
                        + "WHERE clientes.bloqueado <> 'S' " +
                        "order by rutero.ordenVisita";
            }


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cliente = new Cliente();
                    itemListView = new ItemListView();

                    cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                    cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                    cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
                    cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
                    cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
                    cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                    cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
                    cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
                    cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
                    cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                    cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                    cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
                    cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                    cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2"));
                    cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));
                    cliente.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    cliente.AcomuladoBonif = cursor.getString(cursor.getColumnIndex("cliacummesbon"));
                    cliente.oportunidad = cursor.getInt(cursor.getColumnIndex("oportunidad"));

                    itemListView.titulo = cliente.codigo + " - " + cliente.Nombre;
                    itemListView.subTitulo = cliente.direccion;

                    //si hay oportunidad remarcar la fila
                    if (cliente.oportunidad >= 1) {
                        itemListView.resaltado = 1;
                    }

                    listaItems.add(itemListView);
                    listaClientes.addElement(cliente);

                } while (cursor.moveToNext());

                mensaje = "Rutero Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaClientesRutero", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaClientes;
    }

    public static Vector<Categoria> ListaCategorias(Vector<String> listaItems) {

        mensaje = "";
        Categoria categoria;

        SQLiteDatabase db = null;
        Vector<Categoria> listaCategorias = new Vector<Categoria>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT Codigo, Descripcion FROM Categorias";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    categoria = new Categoria();
                    categoria.codigo = cursor.getString(cursor.getColumnIndex("Codigo"));
                    categoria.descripcion = cursor.getString(cursor.getColumnIndex("Descripcion"));

                    listaItems.addElement(categoria.descripcion);
                    listaCategorias.addElement(categoria);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaClientesRutero", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCategorias;
    }

    public static Vector<Linea> ListaLineas(Vector<String> listaItems) {

        mensaje = "";
        Linea linea;

        SQLiteDatabase db = null;
        Vector<Linea> listaLineas = new Vector<Linea>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigo, descripcion FROM Lineas order by codigo";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    linea = new Linea();
                    linea.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    linea.descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));

                    listaItems.addElement(linea.descripcion);
                    listaLineas.addElement(linea);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ListaLineas: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaLineas;
    }

    /*public static Vector<Cliente> ListaClientesRuteroTAT(Vector<ItemListView> listaItems, int dia, int semana) {

		mensaje = "";
		Cliente cliente;
		SQLiteDatabase db = null;

		ItemListView itemListView;
		Vector<Cliente> listaClientes = new Vector<Cliente>();

		try {

			String notIn = ClientesVisitados(Main.usuario.fechaConsecutivo, Main.usuario.codigoVendedor);

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			String query = "SELECT Codigo, Nombre, Direccion, Telefono, Nit, Razonsocial, Ciudad, CodigoAmarre, Canal, Cupo, Bloqueado " +
						   "FROM Clientes " +
						   "WHERE Ciudad LIKE '%" + dia + "%' AND Codigo NOT IN (SELECT CodigoCliente FROM NovedadesCompras) " +
						   (notIn.equals("") ? "" : "AND Codigo NOT IN " + notIn + " ") +
						   "ORDER BY OrdenReal";

			query = "SELECT Codigo, Nombre, Direccion, Telefono, Nit, Razonsocial, Ciudad, CodigoAmarre, Canal, Cupo, Bloqueado " +
					"FROM Clientes INNER JOIN Rutero ON Clientes.codigo = Rutero.codCliente " +
					"WHERE Rutero.dia = '" + dia + "' AND Rutero.semana = '" + semana + "' AND codigo NOT IN (SELECT codigocliente FROM NovedadesCompras) " +
					"ORDER BY Rutero.Orden";

			Cursor cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {

				do {

					cliente = new Cliente();
					itemListView = new ItemListView();

					cliente.codigo         = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
					cliente.nombre         = cursor.getString(cursor.getColumnIndex("Nombre"));
					cliente.direccion      = cursor.getString(cursor.getColumnIndex("Direccion")).trim();
					cliente.telefono       = cursor.getString(cursor.getColumnIndex("Telefono"));
					cliente.nit            = cursor.getString(cursor.getColumnIndex("Nit")).trim();
					cliente.razonSocial    = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
					cliente.ciudad         = cursor.getString(cursor.getColumnIndex("Ciudad"));
					cliente.listaPrecio    = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
					cliente.canal          = cursor.getString(cursor.getColumnIndex("Canal"));
					cliente.cupo           = cursor.getString(cursor.getColumnIndex("Cupo"));
					cliente.bloqueado      = cursor.getString(cursor.getColumnIndex("Bloqueado")).trim();
					cliente.extra_ruta     = 0; //Los Clientes del Rutero no son Extra Ruta

					itemListView.titulo    = cliente.codigo + " - " + cliente.razonSocial;
					itemListView.subTitulo = cliente.direccion;

					listaItems.add(itemListView);
					listaClientes.addElement(cliente);

				} while (cursor.moveToNext());

				mensaje = "Rutero Cargado Correctamente";

			  } else {

				  mensaje = "Consulta sin Resultados";
			  }

			if (cursor != null)
				cursor.close();

		} catch (Exception e) {

			mensaje = e.getMessage();

		} finally {

			if (db != null)
				db.close();
		}

		return listaClientes;
	}*/

    public static Cliente CargarCliente(String codigoCliente) {

        mensaje = "";
        SQLiteDatabase db = null;
        Cliente cliente = new Cliente();

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, "
                    + "Canal, SubCanal, Cupo,  TipoCredito, DANE, Bloqueado,  Tipologia2, diasIngreso, TipoCredito,portafolio as portafolio,bodega " +
                    "FROM Clientes " +
                    "WHERE Codigo = '" + codigoCliente + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
                cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
                cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
                cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
                cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
                cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
                cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
                cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2"));
                cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));
                cliente.tipoCredito = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                cliente.portafolio = cursor.getString(cursor.getColumnIndex("portafolio"));
                cliente.bodega = cursor.getString(cursor.getColumnIndex("bodega"));

                mensaje = "Cliente Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("DataBaseBO", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cliente;
    }

    public static CarteraPendiente CarteraPendiente(String codigoCliente) {

        mensaje = "";
        SQLiteDatabase db = null;
        CarteraPendiente carteraPendiente = new CarteraPendiente();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT Documento, fechavecto, Concepto, Saldo, abono, clasifca1 || '-' || clasifca2 || '-' || clasifca3 AS EC, " +
                    "IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaVencimiento)), -1) AS dias " +
                    "FROM Cartera " +
                    "WHERE cliente = '" + codigoCliente + "' AND documento NOT IN (SELECT nrodoc FROM TmpDetalleRecaudo) " +
                    "AND documento NOT IN (SELECT NUMDOC FROM DetalleRecaudo WHERE TipoPago = 'T') AND saldo > 0 " +
                    "ORDER BY dias DESC";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    float saldo = cursor.getFloat(cursor.getColumnIndex("Saldo"));
                    int abono = cursor.getInt(cursor.getColumnIndex("abono"));
                    int dias = cursor.getInt(cursor.getColumnIndex("dias"));

                    carteraPendiente.total += saldo - abono;

                    if (dias >= 10 && carteraPendiente.diasVencido == 0)
                        carteraPendiente.diasVencido = dias;

                } while (cursor.moveToNext());

                mensaje = "Cartera Pendiente Cargada con Exito";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("DataBaseBO", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return carteraPendiente;
    }

    /*public static String ClientesVisitados(String fechaConsecutivo, String codigoVendedor) {

		mensaje = "";
		String notIn = "";
		SQLiteDatabase db = null;

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			String query = "SELECT DISTINCT codigo_cliente " +
						   "FROM Encabezado " +
						   "WHERE fecha_consecutivo = '" + fechaConsecutivo + "' AND codigo_vendedor = '" + codigoVendedor + "' AND " +
						   "hora_final IS NOT NULL";

			Cursor cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {

				do {

					if (notIn.equals("")) {

						notIn += "'" + cursor.getString(cursor.getColumnIndex("codigo_cliente")) + "'";

					} else {

						notIn += ", " + "'" + cursor.getString(cursor.getColumnIndex("codigo_cliente")) + "'";
					}

				} while (cursor.moveToNext());

				notIn = "(" + notIn + ")";
				mensaje = "Consulta Exitosa";

			} else {

				mensaje = "Consulta sin Resultados";
			}

			if (cursor != null)
				cursor.close();

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("ClientesVisitados", mensaje, e);

		} finally {

			if (db != null)
				db.close();
		}

		return notIn;
	}*/

    public static int DiferenciaDias(String fecha) {

        int dias = 0;
        mensaje = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY('" + fecha + "')), -1) AS dias";
            Cursor cursor = db.rawQuery(query, null);

            System.out.println("QUERY DE FECHA : " + query);

            if (cursor.moveToFirst()) {

                dias = cursor.getInt(cursor.getColumnIndex("dias"));
                System.out.println("Dias: " + dias);
                mensaje = "Calculo la diferencia en dias correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return dias;
    }

    public static String NombreProducto(String codigo) {

        SQLiteDatabase db = null;
        String nombre = "";
        mensaje = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT nombre FROM Productos WHERE Codigo = '" + codigo + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                mensaje = "Consulta Satisfactoria";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return nombre;
    }

    public static Vector<Cartera> ListaCartera(String codigoCliente, Vector<ItemCarteraListView> listaItems) {

        mensaje = "";
        SQLiteDatabase db = null;

        Cartera cartera = new Cartera();
        ItemCarteraListView itemCarteraListView;
        Vector<Cartera> listaCartera = new Vector<Cartera>();

        if (listaItems == null)
            listaItems = new Vector<ItemCarteraListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT referencia, Saldo - abono AS valor, " +
                    "IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaVencimiento)), -1) AS dias, " +
                    "FechaVecto, fecha, concepto, documento, descripcion " +
                    "FROM Cartera " +
                    "WHERE cliente = '" + codigoCliente + "' " +
                    "AND documento NOT IN (SELECT Nrodoc FROM tmpdetallerecaudo) " +
                    "AND documento NOT IN (SELECT NUMDOC FROM DETALLERECAUDO WHERE TIPOPAGO = 'T') " +
                    "ORDER BY dias DESC  ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cartera = new Cartera();
                    itemCarteraListView = new ItemCarteraListView();

                    cartera.codCliente = codigoCliente;
                    cartera.referencia = cursor.getString(cursor.getColumnIndex("referencia"));
                    cartera.saldo = Util.ToFloat(Util.Redondear("" + cursor.getFloat(cursor.getColumnIndex("valor")), 0));
                    cartera.valorARecaudar = Util.ToFloat(Util.Redondear("" + cursor.getFloat(cursor.getColumnIndex("valor")), 0));
                    cartera.dias = cursor.getInt(cursor.getColumnIndex("dias"));
                    cartera.FechaVecto = cursor.getString(cursor.getColumnIndex("FechaVecto"));
                    cartera.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    cartera.concepto = cursor.getString(cursor.getColumnIndex("Concepto"));
                    cartera.documento = cursor.getString(cursor.getColumnIndex("Documento"));
                    cartera.descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));

                    if (ExisteCartera(cartera.referencia, codigoCliente))
                        continue;

                    itemCarteraListView.codigo = cartera.referencia;
                    //itemCarteraListView.seleccionado = ExisteCartera(cartera.referencia, codigoCliente);
                    itemCarteraListView.titulo = Util.ToLong(cartera.referencia) + "  " + Util.SepararMiles("" + cartera.saldo);
                    itemCarteraListView.subTitulo = "Dias Venc: " + cartera.dias + ". Fecha Vec: " + cartera.FechaVecto;

                    listaCartera.addElement(cartera);
                    listaItems.addElement(itemCarteraListView);

                } while (cursor.moveToNext());

                mensaje = "Cartera Cargada con Exito";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaCartera;
    }

    public static boolean ExisteCartera(String referencia, String codigoCliente) {

        for (Cartera cartera : Main.cartera) {

            if (cartera.referencia.equals(referencia) && cartera.codCliente.equals(codigoCliente))
                return true;
        }

        return false;
    }

    public static Vector<String> ListaCiudades() {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<String> listaCiudades = new Vector<String>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String ciudad;
            Cursor cursor = db.rawQuery("SELECT DISTINCT Ciudad FROM Clientes", null);

            if (cursor.moveToFirst()) {

                do {

                    ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                    listaCiudades.addElement(ciudad);

                } while (cursor.moveToNext());

                mensaje = "Ciudades Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaCiudades;
    }

    public static Vector<String> ListaDiasRutero() {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<String> listaDiasRutero = new Vector<String>();

        Main.posRutaProgRutero = -1;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String descripcion;
            Cursor cursor = db.rawQuery("SELECT DISTINCT ruta_parada FROM Clientes ORDER BY ruta_parada", null);

            if (cursor.moveToFirst()) {

                do {

                    descripcion = cursor.getString(cursor.getColumnIndex("ruta_parada"));
                    listaDiasRutero.addElement(descripcion);

                    if (descripcion.trim().equals(Main.rutaEscogidaVendedor.trim())) {

                        Main.posRutaProgRutero = listaDiasRutero.size() - 1;
                    }


                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaDiasRutero;
    }


    public static Vector<String> ListasPrecios() {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<String> listadoListasPrecios = new Vector<String>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String descripcion;
            Cursor cursor = db.rawQuery("SELECT DISTINCT listaprecios FROM ListaPrecios", null);

            if (cursor.moveToFirst()) {

                do {

                    descripcion = cursor.getString(cursor.getColumnIndex("listaprecios"));
                    listadoListasPrecios.addElement(descripcion);


                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listadoListasPrecios;
    }


    public static Vector<String> ListaCiudadCliente() {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<String> listaCiudad = new Vector<String>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT DISTINCT Ciudad FROM Clientes ORDER BY Ciudad", null);

            if (cursor.moveToFirst()) {

                do {

                    String ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                    listaCiudad.addElement(ciudad);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaCiudad;
    }

    public static Vector<MotivoCambio> ListaMotivosCambio(Vector<String> listaItems) {

        mensaje = "";
        MotivoCambio motivoCambio;

        SQLiteDatabase db = null;
        Vector<MotivoCambio> listaMotivosCambio = new Vector<MotivoCambio>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT codigo, concepto FROM MotivosCambio WHERE tipo = 'D' ORDER BY PosDefault DESC", null);

            if (cursor.moveToFirst()) {

                do {

                    motivoCambio = new MotivoCambio();
                    motivoCambio.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    motivoCambio.concepto = cursor.getString(cursor.getColumnIndex("concepto"));

                    listaItems.addElement(motivoCambio.concepto);
                    listaMotivosCambio.addElement(motivoCambio);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosCambio", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaMotivosCambio;
    }


    /**
     * determinar si actualmente un cliente ya tiene factura.
     * es decir realizo una compra.
     *
     * @param codigoCliente
     * @return true,  si tiene factura, false en caso contrario.
     */
    public static boolean tieneFacturasCliente(String codigoCliente) {

        SQLiteDatabase db = null;
        boolean tieneFactura = false;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT 1 FROM [Encabezado] WHERE [codigo] = '" + codigoCliente + "' GROUP BY [codigo];";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    tieneFactura = true;
                }
                if (cursor != null)
                    cursor.close();

            }
        } catch (Exception e) {
            mensaje = e.getMessage();

        } finally {
            closeDataBase(db);
        }
        return tieneFactura;
    }


    public static Vector<MotivoAbono> ListaMotivoAbono(Vector<String> items) {

        mensaje = "";
        SQLiteDatabase db = null;

        MotivoAbono motivoAbono;
        Vector<MotivoAbono> listaMotivos = new Vector<MotivoAbono>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            Cursor cursor = db.rawQuery("SELECT codigo, descripcion AS nombre FROM MotivoAbono ORDER BY codigo", null);

            if (cursor.moveToFirst()) {

                do {

                    motivoAbono = new MotivoAbono();
                    motivoAbono.codigo = cursor.getInt(cursor.getColumnIndex("codigo"));
                    motivoAbono.nombre = cursor.getString(cursor.getColumnIndex("nombre"));

                    items.addElement(motivoAbono.nombre);
                    listaMotivos.addElement(motivoAbono);

                } while (cursor.moveToNext());

                mensaje = "Motivos Abonos Cargados Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaMotivos;
    }

    /*public static InformeInventario CargarInformeInventario(String codigoRef) {

		SQLiteDatabase db = null;
		InformeInventario informe = null;

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			String query =

					"SELECT DISTINCT Productos.Codigo AS Codigo, Productos.Nombre AS Nombre, " +
					"CASE WHEN Cantidad IS NULL THEN 0 ELSE Cantidad END AS InvActual, " +

					"(SELECT " +
					"CASE WHEN SUM(cantidad) IS NULL THEN 0 ELSE SUM(cantidad) END " +
					"FROM Detalle INNER JOIN Encabezado ON Detalle.numDoc = Encabezado.numeroDoc " +
					"WHERE codigoRef = '"+ codigoRef +"' AND tipoTrans = 0) AS TotalVentas, " +

					"(SELECT " +
					"CASE WHEN SUM(cantidad) IS NULL THEN 0 ELSE SUM(cantidad) END " +
					"FROM Detalle INNER JOIN Encabezado ON Detalle.numDoc = Encabezado.numeroDoc " +
					"WHERE codigoRef = '" + codigoRef + "' AND tipoTrans = 2) AS TotalPNC " +

					"FROM Productos INNER JOIN ListaPrecios ON productos.codigo = ListaPrecios.codigo " +
					"LEFT JOIN Inventario ON Productos.Codigo = Inventario.codigo " +
					"WHERE Productos.Codigo = '" + codigoRef + "'";

			Cursor cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {

				informe = new InformeInventario();
				informe.Codigo = codigoRef;
				informe.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));

				informe.InvActual = cursor.getInt(cursor.getColumnIndex("InvActual"));
				informe.TotalVentas = cursor.getInt(cursor.getColumnIndex("TotalVentas"));
				informe.TotalPNC = cursor.getInt(cursor.getColumnIndex("TotalPNC"));

				informe.InvInicial = informe.InvActual + informe.TotalVentas + informe.TotalPNC;
			}

			if (cursor != null)
				cursor.close();

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("CargarInformeInventario", mensaje, e);

		} finally {

			if (db != null)
				db.close();
		}

		return informe;
	}*/

    public static Vector<Producto> BuscarProductos(boolean porCodigo, String opBusqueda, String listaPrecio, Vector<ItemListView> listaItems, String and) {

        Producto producto;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Producto> listaProductos = new Vector<Producto>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query;


            if (porCodigo) {


		/*query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
						"FROM Productos " +
						"INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '"+ listaPrecio +"' " +
						"WHERE Productos.codigo LIKE '%"+ opBusqueda +"%'";*/

                query = "SELECT codigo, nombre, precio, iva, saldo, (precio + ((precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                        "FROM ProductosTmp " +
                        "WHERE codigo LIKE '%" + opBusqueda + "%' " + and + " ";


            } else {

		/*query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
						"FROM Productos " +
						"INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '"+ listaPrecio +"' " +
						"WHERE Productos.nombre LIKE '%"+ opBusqueda +"%'";*/

                query = "SELECT codigo, nombre, precio, iva, saldo, (precio + ((precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                        "FROM ProductosTmp " +
                        "WHERE nombre LIKE '%" + opBusqueda + "%' " + and + "  ";
            }

            System.out.println("Obtener Producto---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    producto = new Producto();
                    itemListView = new ItemListView();

                    producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    producto.precio = cursor.getInt(cursor.getColumnIndex("precio"));
                    producto.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    producto.inventario = cursor.getInt(cursor.getColumnIndex("saldo"));
                    producto.precioIva = cursor.getFloat(cursor.getColumnIndex("precioIva"));
                    producto.unidadesXCaja = cursor.getString(cursor.getColumnIndex("unidadesxcaja"));
                    producto.ean = cursor.getString(cursor.getColumnIndex("ean"));
                    producto.agrupacion = cursor.getString(cursor.getColumnIndex("agrupacion"));
                    producto.grupo = cursor.getString(cursor.getColumnIndex("grupo"));
                    producto.linea = cursor.getString(cursor.getColumnIndex("linea"));
                    producto.unidadMedida = cursor.getString(cursor.getColumnIndex("unidadmedida"));
                    producto.factorLibras = cursor.getInt(cursor.getColumnIndex("factorlibras"));
                    producto.fechaLimite = cursor.getString(cursor.getColumnIndex("fechalimite"));
                    producto.devol = cursor.getString(cursor.getColumnIndex("devol"));
                    producto.itf = cursor.getString(cursor.getColumnIndex("itf"));
                    producto.impto1 = cursor.getInt(cursor.getColumnIndex("impto1"));
                    producto.indice = cursor.getInt(cursor.getColumnIndex("Indice"));

                    producto.precioDescuento = 0;
                    producto.descuento = 0;

                    itemListView.titulo = producto.codigo + " - " + producto.descripcion;
                    itemListView.subTitulo = "Precio: $" + producto.precio + " - Iva: " + producto.iva;

                    listaProductos.addElement(producto);
                    listaItems.addElement(itemListView);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("BuscarProductos", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaProductos;
    }

    public static Vector<InformeInventario> CargarInformeInventario(boolean isLiquidacion) {

        SQLiteDatabase db = null;
        InformeInventario informeInv;
        Vector<InformeInventario> listaInformeInv = new Vector<InformeInventario>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "";

                if (isLiquidacion) {

                    System.out.println("QUERY CON LIQUIDACION ----> " + query);

                    query = "SELECT p.codigo AS codigo, p.nombre AS nombre, p.saldo ii, "
                            + "p.saldo - COALESCE(v.cantidad, 0) + COALESCE(v.cantidad2, 0)  AS if, COALESCE(v.cantidad, 0) v,COALESCE(v.cantidad2, 0) as s FROM Productos p "
                            + "LEFT JOIN ( select codigo,sum(cantidad) cantidad,sum(cantidad2) cantidad2 from "
                            + "( SELECT d.codigoRef AS codigo, SUM(d.cantidad) AS cantidad, 0 cantidad2 FROM Detalle d INNER JOIN Encabezado e ON e.numeroDoc = d.numDoc "
                            + "AND ifnull(e.anulado,0) = 0 AND e.tipotrans = 0 and e.observaciones<>'FALTANTE PRODUCTO DANADO' GROUP BY d.codigoRef "
                            + "union all select ds.codigoref,sum(cantidad) cantidad, 0 cantidad2 from encabezadosugerido es inner join detallesugerido ds  "
                            + "on es.numerodoc = ds.numdoc where es.tipotrans=8  group by ds.codigoref "
                            + "union all "
                            + "SELECT d2.codigoref,0 cantidad, sum(d2.cantidad) cantidad2 FROM encabezado e2 INNER JOIN detalle d2 ON e2.numerodoc = d2.numdoc "
                            + "WHERE e2.tipotrans = 2 AND "
                            + "COALESCE(e2.anulado, 0) = 0 AND e2.observaciones = 'SOBRANTE INVENTARIO PRODUCTO' "
                            + "GROUP BY d2.codigoref "
                            + ") tmp group by codigo  ) "
                            + "v ON v.codigo = p.codigo WHERE p.saldo > 0 AND p.tipocliente = 0 "
                            + "ORDER BY p.orden ASC ";

                } else {

                    System.out.println("QUERY SIN LIQUIDACION ----> " + query);

                    query = ""
                            + "SELECT p.codigo AS codigo, p.nombre AS nombre, p.saldo AS ii, p.saldo - COALESCE(tmp.cantidad, 0) AS if, "
                            + "COALESCE(tmp.cantidad, 0) AS v "
                            + "FROM productos p "
                            + "LEFT JOIN ("
                            + " SELECT d.codigoref, "
                            + "sum(d.cantidad) cantidad FROM encabezado e "
                            + "INNER JOIN detalle d ON e.numerodoc = d.numdoc "
                            + "WHERE e.tipotrans = 0 AND "
                            + "COALESCE(e.anulado, 0) = 0 AND e.observaciones <> 'FALTANTE PRODUCTO DANADO' "
                            + "GROUP BY d.codigoref  )tmp ON p.codigo = tmp.codigoref "
                            + "WHERE p.saldo > 0 "
                            + "AND p.tipocliente = 0 "
                            + "ORDER BY p.orden ASC ";

                }


                System.out.println("cONSULTA DEL INFORME INVENTARIO ----> " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        informeInv = new InformeInventario();
                        informeInv.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                        informeInv.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                        informeInv.invInicial = Util.ToInt(cursor.getString(cursor.getColumnIndex("ii")));
                        informeInv.invActual = Util.ToInt(cursor.getString(cursor.getColumnIndex("if")));
                        informeInv.cantVentas = Util.ToInt(cursor.getString(cursor.getColumnIndex("v")));

                        if (isLiquidacion)
                            informeInv.sobrante = Util.ToInt(cursor.getString(cursor.getColumnIndex("s")));

                        //						informeInv.cantPNC    = Util.ToInt(cursor.getString(cursor.getColumnIndex("CantPNC")));
                        //						informeInv.invInicial = informeInv.invActual + informeInv.cantVentas + informeInv.cantPNC;

                        listaInformeInv.addElement(informeInv);

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarInformeInventario -> La Base de Datos No Existe");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarInformeInventario -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaInformeInv;
    }


    public static void aregarInformeCanastas(Vector<InformeInventario> listaInventario) {
        SQLiteDatabase db = null;
        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = ""
                        + "SELECT p.codigo AS codigo,"
                        + "       p.nombre AS nombre,"
                        + "       p.saldo AS ii,"
                        + "       0 AS [if],"
                        + "       0 AS v "
                        + "FROM productos p "
                        + "WHERE p.tipocliente = 1 and p.saldo > 0 "
                        + "ORDER BY p.orden ASC; ";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {
                        InformeInventario informeInv = new InformeInventario();
                        informeInv.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                        informeInv.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                        informeInv.invInicial = Util.ToInt(cursor.getString(cursor.getColumnIndex("ii")));
                        informeInv.invActual = Util.ToInt(cursor.getString(cursor.getColumnIndex("if")));
                        informeInv.cantVentas = Util.ToInt(cursor.getString(cursor.getColumnIndex("v")));
                        listaInventario.addElement(informeInv);
                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();
            } else {

                Log.e(TAG, "Agregar Canastas -> La Base de Datos No Existe");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "Agregar Canastas -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }
    }


    /**
     * Calcular los totales o sumatorias de cada elemento registrado en el inventario.
     * totalizando productos por cantidades y precios.
     * tambien se totalizan las canastas de inventario.
     *
     * @param totalInventarioDia
     */
    public static void calcularTotalesInventarioDia(TotalInventarioDia totalInventarioDia) {

        SQLiteDatabase db = null;
        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = ""
                        + "SELECT sum(p.saldo) unidadesii,"
                        + "       sum(p.saldo * l.precio) valorCargue, "
                        + "       COALESCE(c.canastillas, 0) totalCanastillas "
                        + "FROM productos p "
                        + "     INNER JOIN ( "
                        + "     SELECT codigo, "
                        + "            max(precio) precio "
                        + "     FROM listaprecios "
                        + "     GROUP BY codigo "
                        + "     ) l ON l.codigo = p.codigo "
                        + "     LEFT JOIN ( "
                        + "     SELECT sum(p.saldo) canastillas "
                        + "     FROM productos p "
                        + "     WHERE p.tipocliente = 1 "
                        + "     ORDER BY p.orden ASC "
                        + "     ) c "
                        + "WHERE p.saldo > 0 AND p.tipocliente = 0; ";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {
                        totalInventarioDia.setTotalUnidadesII(cursor.getInt(cursor.getColumnIndex("unidadesii")));
                        totalInventarioDia.setValorCargue(cursor.getDouble(cursor.getColumnIndex("valorCargue")));
                        totalInventarioDia.setTotalCanastillas(cursor.getInt(cursor.getColumnIndex("totalCanastillas")));
                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();
            } else {

                Log.e(TAG, "calcularTotalesInventarioDia -> La Base de Datos No Existe");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "calcularTotalesInventarioDia -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static Vector<InformeInventario> CargarInformeInventario_() {

        SQLiteDatabase db = null;
        InformeInventario informeInv;
        Vector<InformeInventario> listaInformeInv = new Vector<InformeInventario>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query =

                        "SELECT Productos.Codigo AS Codigo, Productos.Nombre AS Nombre, Inventario.cantidad as InvActual, " +
                                "(SELECT SUM(CASE WHEN tipoTrans = 0 THEN cantidad END) " +
                                "FROM EncabezadoAndroid INNER JOIN DetalleAndroid ON DetalleAndroid.numDoc = EncabezadoAndroid.numeroDoc " +
                                "WHERE DetalleAndroid.codigoRef = D.codigoRef " +
                                ") AS CantVentas, " +
                                "(SELECT SUM(CASE WHEN tipoTrans = 2 THEN cantidad END) " +
                                "FROM EncabezadoAndroid INNER JOIN DetalleAndroid ON DetalleAndroid.numDoc = EncabezadoAndroid.numeroDoc " +
                                "WHERE DetalleAndroid.codigoRef = D.codigoRef" +
                                ") AS CantPNC " +
                                "FROM Productos INNER JOIN Inventario ON Productos.Codigo = Inventario.codigo " +
                                "LEFT JOIN DetalleAndroid D ON D.codigoRef = Productos.codigo " +
                                "GROUP BY Productos.Codigo, Productos.Nombre, Inventario.cantidad";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        informeInv = new InformeInventario();
                        informeInv.codigo = cursor.getString(cursor.getColumnIndex("Codigo"));
                        informeInv.nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                        informeInv.invActual = Util.ToInt(cursor.getString(cursor.getColumnIndex("InvActual")));
                        informeInv.cantVentas = Util.ToInt(cursor.getString(cursor.getColumnIndex("CantVentas")));
                        informeInv.cantPNC = Util.ToInt(cursor.getString(cursor.getColumnIndex("CantPNC")));
                        informeInv.invInicial = informeInv.invActual + informeInv.cantVentas + informeInv.cantPNC;

                        listaInformeInv.addElement(informeInv);

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarInformeInventario -> La Base de Datos No Existe");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarInformeInventario -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaInformeInv;
    }


    public static Vector<Fiado> CargarListaFiados() {

        SQLiteDatabase db = null;
        Fiado fiado;
        Vector<Fiado> listaFiados = new Vector<Fiado>();

        try {

            int i = 0;
            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT numeroDoc, montoFact, nombre, strftime('%Y-%m-%d', fechaTrans) AS fecha, fiado FROM Fiados";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        fiado = new Fiado();
                        fiado.position = i++;
                        fiado.numeroDoc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                        fiado.montoFact = Util.SepararMiles(cursor.getString(cursor.getColumnIndex("montoFact")));
                        fiado.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                        fiado.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                        fiado.fiado = cursor.getInt(cursor.getColumnIndex("fiado"));

                        listaFiados.addElement(fiado);

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarListaFiados -> La Base de Datos No Existe");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarListaFiados -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaFiados;
    }

    public static Vector<UltimoPedido> getUltimoPedido(String codCliente, Vector<ItemListView> listaItems) {

        UltimoPedido ultPedido;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<UltimoPedido> listaUltPedido = new Vector<UltimoPedido>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query =

                    "SELECT Codigo, CodigoRef, Cantidad, Nombre " +
                            "FROM UltimoPedido " +
                            "WHERE Codigo = '" + codCliente + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    ultPedido = new UltimoPedido();
                    itemListView = new ItemListView();

                    ultPedido.codCliente = cursor.getString(cursor.getColumnIndex("Codigo"));
                    ultPedido.codProducto = cursor.getString(cursor.getColumnIndex("CodigoRef"));
                    ultPedido.cantidad = cursor.getInt(cursor.getColumnIndex("Cantidad"));
                    ultPedido.descProducto = cursor.getString(cursor.getColumnIndex("Nombre"));


                    itemListView.titulo = ultPedido.codProducto + " - " + ultPedido.descProducto;
                    itemListView.subTitulo = "Cantidad: " + String.valueOf(ultPedido.cantidad);

                    if (Main.detallePedido.containsKey(ultPedido.codProducto)) {

                        //El producto ya esta inlcuido en el Pedido

                        itemListView.colorTitulo = 0xFB179D1A;

                    } else {

                        //itemListView.colorTitulo = ultPedido.cantidad < producto.cantidadInv ? 0xFF2E65AD : 0xF5A40000;
                    }

                    listaUltPedido.addElement(ultPedido);
                    listaItems.addElement(itemListView);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("UltimoPedido", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaUltPedido;
    }

    public static Vector<Producto> UltimoPedido(String codigoCliente, String listaPrecio, Vector<ItemListView> listaItems, String numDoc) {

        Producto producto;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Producto> listaProductos = new Vector<Producto>();

        try {

            Vector<String> listaRef = ReferenciasPedidoActual(numDoc);

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query =

                    "SELECT Productos.Codigo AS Codigo, Productos.Nombre AS Nombre, ListaPrecios.Precio AS Precio, Iva, UltimoPedido.Cantidad AS CantUltimo, " +
                            "CASE WHEN Inventario.Cantidad IS NULL THEN 0 ELSE Inventario.Cantidad END AS CantidadInv, " +
                            "CASE WHEN Saldo IS NULL THEN 0 ELSE Saldo END AS Saldo, UnidadMedida, Productos.Linea AS linea, Grupo, Indice " +
                            "FROM UltimoPedido INNER JOIN Productos ON UltimoPedido.CodigoRef = Productos.codigo " +
                            "INNER JOIN ListaPrecios ON productos.codigo = ListaPrecios.codigo " +
                            "LEFT JOIN Inventario ON Productos.Codigo = Inventario.codigo " +
                            "WHERE UltimoPedido.codigo = '" + codigoCliente + "' AND ListaPrecios.ListaPrecios = '" + listaPrecio + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    producto = new Producto();
                    itemListView = new ItemListView();

		    /*producto.codigo           = cursor.getString(cursor.getColumnIndex("Codigo"));
					producto.descripcion      = cursor.getString(cursor.getColumnIndex("Nombre"));
					producto.precio           = cursor.getFloat(cursor.getColumnIndex("Precio"));
					producto.iva              = cursor.getFloat(cursor.getColumnIndex("Iva"));
					producto.promocion        = cursor.getInt(cursor.getColumnIndex("CantidadInv"));
					producto.grupo            = cursor.getString(cursor.getColumnIndex("Saldo"));
					producto.factor           = cursor.getFloat(cursor.getColumnIndex("UnidadMedida"));
					producto.concatEstrato    = cursor.getString(cursor.getColumnIndex("linea"));
					producto.bloqueadoCliente = cursor.getString(cursor.getColumnIndex("Grupo"));
					producto.promocionCumplio = cursor.getString(cursor.getColumnIndex("Indice"));
					producto.descLinea        = cursor.getString(cursor.getColumnIndex("descuento"));
					producto.prodZona3        = cursor.getInt(cursor.getColumnIndex("descuento"));
					//producto.descuento     = cursor.getFloat(cursor.getColumnIndex("descuento"));*/

                    itemListView.titulo = producto.codigo + " - " + producto.descripcion;
                    itemListView.subTitulo = "Precio: " + Util.SepararMiles(Util.Redondear("" + producto.precio, 0)) + " - Iva: " + ((int) producto.iva) + "% ";

		    /*if (listaRef.contains(producto.codigo)) {

						//El producto ya esta inlcuido en el Pedido

						producto.existe = true;
						itemListView.colorTitulo = 0xFB179D1A;

					} else {

						itemListView.colorTitulo = producto.cantUltimo < producto.cantidadInv ? 0xFF2E65AD : 0xF5A40000;
					}*/

                    listaProductos.addElement(producto);
                    listaItems.addElement(itemListView);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("UltimoPedido", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaProductos;
    }

    public static Vector<String> ReferenciasPedidoActual(String numDoc) {

        SQLiteDatabase db = null;
        Vector<String> listaCodRef = new Vector<String>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigoRef FROM Detalle WHERE numDoc = '" + numDoc + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    String codigoRef = cursor.getString(cursor.getColumnIndex("codigoRef"));
                    listaCodRef.addElement(codigoRef);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ReferenciasPedidoActual: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCodRef;
    }


    public static Vector<String> ReferenciasPedidoActual_(String numDoc) {

        SQLiteDatabase db = null;
        Vector<String> listaCodRef = new Vector<String>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigoRef FROM DetalleAndroid WHERE numDoc = '" + numDoc + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    String codigoRef = cursor.getString(cursor.getColumnIndex("codigoRef"));
                    listaCodRef.addElement(codigoRef);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ReferenciasPedidoActual: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCodRef;
    }


    public static boolean ExistePedidoCliente(String codigoCliente) {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total " +
                    "FROM Encabezado " +
                    "WHERE codigo = '" + codigoCliente + "' " +
                    "AND fechaFinal IS NOT NULL";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getInt(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean ExistePedidoCliente_(String codigoCliente) {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total " +
                    "FROM EncabezadoAndroid " +
                    "WHERE codigo = '" + codigoCliente + "' " +
                    "AND fechaFinal IS NOT NULL";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getInt(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }



    /*public static boolean GuardarPedido(Encabezado encabezado, Hashtable<String, Detalle> detallePedido) {

		SQLiteDatabase db = null;
		SQLiteDatabase dbPedido = null;

		try {

			String obserCartera = "";

			if (encabezado.excedeCupo)
				obserCartera = "EXCEDE EL CUPO";

			if (encabezado.carteraVencida) {

				obserCartera += (obserCartera.equals("") ? "" : ". ") + "OJO FACTURAS VENCIDAS";
			}

			dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			File filePedido = new File(Util.DirApp(), "Temp.db");
			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			ContentValues values = new ContentValues();
			ContentValues valuesTemp = new ContentValues();

			values.put("codigo_cliente",    encabezado.codigo_cliente);
			values.put("nombre_cliente",    encabezado.nombre_cliente);
			values.put("razon_social",      encabezado.razon_social);
			values.put("codigo_novedad",    encabezado.codigo_novedad);
			values.put("lista_precio",      encabezado.lista_precio);
			values.put("valor_neto",        encabezado.valor_neto);
			values.put("observacion",       encabezado.observacion);
			values.put("hora_inicial",      encabezado.hora_inicial);
			values.put("hora_final",        encabezado.hora_final);
			values.put("tipo_cliente",      encabezado.tipo_cliente);
			values.put("extra_ruta",        encabezado.extra_ruta);
			values.put("numero_doc",        encabezado.numero_doc);
			values.put("no_compra",         encabezado.no_compra);
			values.put("fecha_consecutivo", Main.usuario.fechaConsecutivo);
			values.put("sync",              0);
			values.put("version",           Main.versionApp);
			values.put("codigo_vendedor",   Main.usuario.codigoVendedor);
			values.put("ObserCartera",      obserCartera);

			long rowId = db.insertOrThrow("Encabezado", null, values);

			Enumeration<Detalle> e = detallePedido.elements();

			while (e.hasMoreElements()) {

				Detalle detalle = e.nextElement();

				values = new ContentValues();
				values.put("cod_encabezado",  rowId);
				values.put("codigo_cliente",  detalle.codigo_cliente);
				values.put("codigo_producto", detalle.codigo_producto);
				values.put("precio",          detalle.precio);
				values.put("iva",             detalle.iva);
				values.put("descuento",       detalle.descuento_autorizado);
				values.put("cantidad",        detalle.cantidad);
				values.put("tipo_pedido",     detalle.tipo_pedido);
				values.put("sync",            0);

				db.insertOrThrow("Detalle", null, values);

				valuesTemp = new ContentValues();
				valuesTemp.put("cod_encabezado", rowId);
				valuesTemp.put("codigoCliente",  encabezado.codigo_cliente);
				valuesTemp.put("codigoRef",      detalle.codigo_producto);
				valuesTemp.put("cantidad",       detalle.cantidad);
				valuesTemp.put("precio",         detalle.precio);
				valuesTemp.put("iva", 			 detalle.iva);
				valuesTemp.put("descuento",      detalle.descuento_autorizado);
				valuesTemp.put("tipoPedido",     detalle.tipo_pedido);
				valuesTemp.put("inicioPedido",   encabezado.hora_inicial);
				valuesTemp.put("finPedido",      encabezado.hora_final);
				valuesTemp.put("observacion",    encabezado.observacion);
				valuesTemp.put("tipo_cliente",   encabezado.tipo_cliente);
				valuesTemp.put("extra_ruta",     encabezado.extra_ruta);
				valuesTemp.put("no_compra",      encabezado.no_compra);
				valuesTemp.put("numero_doc",     encabezado.numero_doc);
				valuesTemp.put("version",        Main.versionApp);
				valuesTemp.put("ObserCartera",      obserCartera);

				dbPedido.insertOrThrow("TmpPedido", null, valuesTemp);
			}

			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			return false;

		} finally {

			if (db != null)
				db.close();

			if (dbPedido != null)
				dbPedido.close();
		}
	}*/

    public static void eliminarDescAutorizado2(String codCliente, String codProducto, String porcentaje) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM DescuentosAutorizados WHERE codigocliente = '" + codCliente + "' and material = '" + codProducto + "' and porcentaje ='" + porcentaje + "'");

                dbPedido.execSQL("DELETE FROM DescuentosAutorizados WHERE codigocliente = '" + codCliente + "' and material = '" + codProducto + "' and porcentaje ='" + porcentaje + "'");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static void eliminarDescAutorizado(String codCliente, String codProducto) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM DescuentosAutorizados WHERE codigocliente = '" + codCliente + "' and material = '" + codProducto + "'");

                dbPedido.execSQL("DELETE FROM DescuentosAutorizados WHERE codigocliente = '" + codCliente + "' and material = '" + codProducto + "'");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static void eliminarDescAutorizado_(String codCliente, String codProducto) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM DescuentosAutorizadosAndroid WHERE codigocliente = '" + codCliente + "' and material = '" + codProducto + "'");

                dbPedido.execSQL("DELETE FROM DescuentosAutorizadosAndroid WHERE codigocliente = '" + codCliente + "' and material = '" + codProducto + "'");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean GuardarDescAutorizado(String bodega, String vendedor, String codCliente, String codProducto, String cantidad, String porcentaje, String valor, String autorizadoPor, String precio) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("bodega", bodega);
            values.put("vendedor", vendedor);
            values.put("codigocliente", codCliente);
            values.put("material", codProducto);
            values.put("materialcompleto", codProducto);
            values.put("cantidad", cantidad);
            values.put("porcentaje", porcentaje);
            values.put("valor", valor);
            values.put("autorizadopor", autorizadoPor);
            values.put("fechacarga", fechaActual);
            values.put("numerodoc", codCliente);
            values.put("precio", precio);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);

            db.insertOrThrow("DescuentosAutorizados", null, values);
            dbTemp.insertOrThrow("DescuentosAutorizados", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarNoCompra: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static boolean GuardarDescAutorizado_(String bodega, String vendedor, String codCliente, String codProducto, String cantidad, String porcentaje, String valor, String autorizadoPor, String precio) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("bodega", bodega);
            values.put("vendedor", vendedor);
            values.put("codigocliente", codCliente);
            values.put("material", codProducto);
            values.put("materialcompleto", codProducto);
            values.put("cantidad", cantidad);
            values.put("porcentaje", porcentaje);
            values.put("valor", porcentaje);
            values.put("autorizadopor", autorizadoPor);
            values.put("fechacarga", fechaActual);
            values.put("numerodoc", codCliente);
            values.put("precio", precio);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);

            db.insertOrThrow("DescuentosAutorizadosAndroid", null, values);
            dbTemp.insertOrThrow("DescuentosAutorizadosAndroid", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarNoCompra: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static boolean GuardarNoCompra(Encabezado encabezado, String version, String imei, byte[] image) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("codigoCliente", encabezado.codigo_cliente);
            values.put("motivo", encabezado.codigo_novedad);
            values.put("vendedor", Main.usuario.codigoVendedor);
            values.put("valor", 0);
            values.put("fecha", fechaActual);
            values.put("NroDoc", encabezado.numero_doc);
            values.put("horaInicio", encabezado.hora_inicial);
            values.put("horaFinal", encabezado.hora_final);
            values.put("version", Main.versionApp);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);
            values.put("bodega", Main.usuario.bodega);

            db.insertOrThrow("NovedadesCompras", null, values);
            dbTemp.insertOrThrow("NovedadesCompras", null, values);


	    /*values = new ContentValues();
			values.put("Id",          "A" + Util.ObtenerFechaId());
			values.put("Imagen",      image);
			values.put("CodCliente",  encabezado.codigo_cliente);
			values.put("CodVendedor", Main.usuario.codigoVendedor);
			values.put("modulo",      1);
			values.put("nroDoc",      encabezado.numero_doc);

			dbTemp.insertOrThrow("Fotos", null, values);*/
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarNoCompra: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static boolean GuardarNoCompra_(Encabezado encabezado, String version, String imei, byte[] image) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("codigoCliente", encabezado.codigo_cliente);
            values.put("motivo", encabezado.codigo_novedad);
            values.put("vendedor", Main.usuario.codigoVendedor);
            values.put("valor", 0);
            values.put("fecha", fechaActual);
            values.put("NroDoc", encabezado.numero_doc);
            values.put("horaInicio", encabezado.hora_inicial);
            values.put("horaFinal", encabezado.hora_final);
            values.put("version", Main.versionApp);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);

            db.insertOrThrow("NovedadesComprasAndroid", null, values);
            dbTemp.insertOrThrow("NovedadesComprasAndroid", null, values);


	    /*values = new ContentValues();
			values.put("Id",          "A" + Util.ObtenerFechaId());
			values.put("Imagen",      image);
			values.put("CodCliente",  encabezado.codigo_cliente);
			values.put("CodVendedor", Main.usuario.codigoVendedor);
			values.put("modulo",      1);
			values.put("nroDoc",      encabezado.numero_doc);

			dbTemp.insertOrThrow("Fotos", null, values);*/
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarNoCompra: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static boolean GuardarCoordenada(Coordenada coordenada) {

        long rows = 0;
        SQLiteDatabase dbTemp = null;

        try {

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();
            values.put("CodigoVendedor", coordenada.codigoVendedor);
            values.put("CodigoCliente", coordenada.codigoCliente);
            values.put("latitud", coordenada.latitud);
            values.put("longitud", coordenada.longitud);
            values.put("fecha", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
            values.put("id", coordenada.id);
            values.put("horaCoordenada", coordenada.horaCoordenada);
            values.put("sincronizado", coordenada.sincronizado);
            //values.put("bandera",        coordenada.bandera);


            rows = dbTemp.insertOrThrow("Coordenadas", null, values);
            return rows > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarCoordenada -> " + mensaje, e);
            return false;

        } finally {

            if (dbTemp != null)
                dbTemp.close();
        }
    }

    public static boolean GuardarCoordenadaCliente(Coordenada coordenada) {

        long rows = 0;
        boolean existe = false;
        SQLiteDatabase dbTemp = null;

        try {

            File filePedido = new File(Util.DirApp(), "Temp.db");

            if (filePedido.exists()) {

                if (coordenada.codigoCliente != null) {

                    dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    String query = "SELECT CodigoCliente FROM Coordenadas WHERE CodigoCliente = '" + coordenada.codigoCliente + "'";
                    Cursor cursor = dbTemp.rawQuery(query, null);

                    if (cursor.moveToFirst())
                        existe = true;

                    if (cursor != null)
                        cursor.close();

                    if (existe) {

                        ContentValues values = new ContentValues();
                        values.put("CodigoVendedor", coordenada.codigoVendedor);

                        values.put("latitud", coordenada.latitud);
                        values.put("longitud", coordenada.longitud);
                        values.put("horaCoordenada", coordenada.horaCoordenada);
                        values.put("id", coordenada.id);

                        rows = dbTemp.update("Coordenadas", values, "CodigoCliente = ?", new String[]{coordenada.codigoCliente});

                    } else {

                        ContentValues values = new ContentValues();
                        values = new ContentValues();
                        values.put("CodigoVendedor", coordenada.codigoVendedor);
                        values.put("CodigoCliente", coordenada.codigoCliente);
                        values.put("latitud", coordenada.latitud);
                        values.put("longitud", coordenada.longitud);
                        values.put("sincronizado", coordenada.sincronizado);
                        values.put("bandera", coordenada.bandera);
                        values.put("horaCoordenada", coordenada.horaCoordenada);
                        values.put("id", coordenada.id);

                        rows = dbTemp.insertOrThrow("Coordenadas", null, values);
                    }

                    return rows > 0;

                } else {

                    Log.e(TAG, "GuardarCoordenada -> El codigo del cliente es NULL");
                    return false;
                }

            } else {

                Log.e(TAG, "GuardarCoordenada -> " + Msg.NO_EXISTE_BD);
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarCoordenada -> " + mensaje, e);
            return false;

        } finally {

            if (dbTemp != null)
                dbTemp.close();
        }
    }

    public static boolean AgregarFormaPago(FormaPago formaPago) {

        SQLiteDatabase db = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //			File filePedido = new File(Util.DirApp(), "Temp.db");
            //			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();
            values.put("NroDoc", formaPago.nroDoc);
            values.put("forma_pago", formaPago.codigo);
            values.put("monto", Util.Redondear("" + formaPago.monto, 2));
            values.put("nro_cheque_cons", formaPago.nro_cheque_cons);
            values.put("banco", formaPago.codigoBanco);
            values.put("fecha_registro", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
            values.put("fecha_post", formaPago.fechaPago);
            values.put("codigoCliente", Main.cliente.codigo);
            values.put("vendedor", Main.usuario.codigoVendedor);
            values.put("bodega", Main.usuario.bodega);
            values.put("tipoCliente", 0);
            values.put("plaza", formaPago.plaza);
            values.put("descripcion", formaPago.descripcion);
            values.put("disponible", Util.Redondear("" + formaPago.monto, 2));
            values.put("serial", formaPago.serial);


            db.insertOrThrow("FormaPagoTemp", null, values);
            //			dbPedido.insertOrThrow("FormaPagoTemp", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "AgregarFormaPago: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            //			if (dbPedido != null)
            //				dbPedido.close();
        }
    }

    /**
     * Insertar en formapagotemp de DataBase.db
     *
     * @param formaPago
     * @param numeroDoc
     * @param nroChequeCons
     */
    public static void guardarFormaPagoTablaTemporal(String numeroDocTemporal) {

        SQLiteDatabase db = null;

        try {

            Usuario usuario = ObtenerUsuario();
            if (usuario == null) {
                mensaje = "No se pudo cargar la informacion del Usuario";
            }

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.beginTransaction();

            for (FormaPago formaPago : Main.listaFormasPago) {

                /*si es forma de pago efectivo se genera un nro_cheque_cons temporal*/
                if (formaPago.codigo == 1) {
                    formaPago.nro_cheque_cons = Util.FechaActual("HHmmssSSS");
                }

                TransaccionBO.insertFormaPagoTemporal(db, usuario, formaPago, numeroDocTemporal);
            }

            /****************************
             * Se Confirma la Transaccion
             ****************************/
            db.setTransactionSuccessful();
        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "guardarRecaudo - > " + mensaje, e);
        } finally {
            closeDataBase(db);
        }
    }

    public static boolean CancelarFiado(Fiado fiado) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            long rowsFiado = 0;
            boolean existe = false;

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File fileTemp = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues valuesUpd = new ContentValues();
            valuesUpd.put("fiado", fiado.fiado);

            String whereArgs[] = new String[]{fiado.numeroDoc};
            rowsFiado += db.update("Fiados", valuesUpd, "numeroDoc = ?", whereArgs);

            //Valida si ya existe En Fiados_Update
            String query = "SELECT numeroDoc FROM Fiados_Update WHERE numeroDoc = '" + fiado.numeroDoc + "'";
            Cursor cursor = dbTemp.rawQuery(query, null);

            if (cursor.moveToFirst())
                existe = true;

            if (cursor != null)
                cursor.close();

            if (existe) {

                rowsFiado += dbTemp.update("Fiados_Update", valuesUpd, "numeroDoc = ?", whereArgs);

            } else {

                ContentValues values = new ContentValues();
                values = new ContentValues();
                values.put("numeroDoc", fiado.numeroDoc);
                values.put("fiado", fiado.fiado);

                rowsFiado += dbTemp.insertOrThrow("Fiados_Update", null, values);
            }

            return rowsFiado > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CancelarFiado: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }

    public static boolean EliminarFormaPago(String serial) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            //
            //			File filePedido = new File(Util.DirApp(), "Temp.db");
            //			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{serial};
            int rowsFormaPago;

            if (serial != null) {
                rowsFormaPago = db.delete("FormaPagoTemp", "serial = ?", whereArgs);
            } else {
                rowsFormaPago = db.delete("FormaPagoTemp", null, null);
            }


            //			rowsFormaPago += dbPedido.delete("FormaPago", "serial = ?", whereArgs);

            return rowsFormaPago > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "EliminarFormaPago: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }

    public static boolean GuardarClienteNuevo_(ClienteNuevo clienteNuevo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values.put("codigo", clienteNuevo.nit);
            values.put("nombre", clienteNuevo.nombre);
            values.put("razonSocial", clienteNuevo.razonSocial);
            values.put("nit", clienteNuevo.nit);
            values.put("direccion", clienteNuevo.direccion);
            values.put("ciudad", clienteNuevo.ciudad);
            values.put("telefono", clienteNuevo.telefono);
            values.put("vendedor", clienteNuevo.vendedor);
            values.put("ruta_parada", clienteNuevo.ruta_parada);
            values.put("Actividad", clienteNuevo.actividad);
            values.put("OrdenCompra", clienteNuevo.ordenVisita);
            values.put("fechaSinc", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
            values.put("TipoDoc", clienteNuevo.tipoDoc);
            values.put("Territorio", clienteNuevo.territorio);
            values.put("Agencia", clienteNuevo.agencia);

            String[] whereAgrs = new String[]{clienteNuevo.nit, clienteNuevo.vendedor};

            db.delete("ClientesNuevos", "codigo = ? AND vendedor = ?", whereAgrs);
            dbPedido.delete("ClientesNuevos", "codigo = ? AND vendedor = ?", whereAgrs);

            db.insertOrThrow("ClientesNuevos", null, values);
            dbPedido.insertOrThrow("ClientesNuevos", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarClienteNuevo: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean GuardarClienteNuevo(ClienteNuevo clienteNuevo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            ContentValues values = new ContentValues();
            values.put("CODIGO", clienteNuevo.codigo);
            values.put("NOMBRE", clienteNuevo.nombre);
            values.put("RAZONSOCIAL", clienteNuevo.razonSocial);
            values.put("NIT", clienteNuevo.codigo);
            values.put("DIRECCION", clienteNuevo.direccion);
            values.put("CIUDAD", clienteNuevo.codCiudad);
            values.put("TELEFONO", clienteNuevo.telefono);
            values.put("VENDEDOR", clienteNuevo.vendedor);
            values.put("FECHAINGRESO", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
            values.put("RUTA_PARADA", clienteNuevo.ruta_parada);
            values.put("CANAL", clienteNuevo.Canal);
            values.put("SUBCANAL", clienteNuevo.SubCanal);
            values.put("AGENCIA", Main.usuario.bodega);
            values.put("CODIGOAMARRE", clienteNuevo.listaPrecio);
            values.put("SINCRONIZADO", "0");
            values.put("tipocliente", clienteNuevo.tipoCliente);
            values.put("territorio", clienteNuevo.territorio);
            values.put("tipocredito", "EF");
            values.put("cupo", 0);
            values.put("sincronizadoandroid", 0);

            String[] whereAgrs = new String[]{clienteNuevo.codigo, clienteNuevo.vendedor};

            db.delete("ClientesNuevos", "CODIGO = ? AND VENDEDOR = ?", whereAgrs);
            dbPedido.delete("ClientesNuevos", "codigo = ? AND VENDEDOR = ?", whereAgrs);

            db.insertOrThrow("ClientesNuevos", null, values);
            dbPedido.insertOrThrow("ClientesNuevos", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarClienteNuevo: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean GuardarClienteMod(ClienteNuevo clienteMod) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values.put("codigo", clienteMod.codigo);
            values.put("nombre", clienteMod.nombre);
            values.put("razonSocial", clienteMod.razonSocial);
            values.put("nit", clienteMod.nit);
            values.put("direccion", clienteMod.direccion);
            values.put("ciudad", clienteMod.ciudad);
            values.put("telefono", clienteMod.telefono);
            values.put("vendedor", clienteMod.vendedor);
            values.put("ruta_parada", clienteMod.ruta_parada);
            values.put("Actividad", clienteMod.actividad);
            values.put("Ordenruta", clienteMod.ordenVisita);
            values.put("TipoDoc", clienteMod.tipoDoc);
            values.put("TipoCliente", "1");
            values.put("Territorio", clienteMod.territorio);
            values.put("Agencia", clienteMod.agencia);

            db.insertOrThrow("ClientesMod", null, values);
            dbPedido.insertOrThrow("ClientesMod", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarClienteNuevo: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }

    public static boolean RegistrarEncabezado(Encabezado encabezado, Detalle detalle, String version, String imei, String tipoTrans) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            /**********************************
             * Se crea el Encabezado del Pedido
             *********************************/
            values = new ContentValues();
            values.put("codigoCliente", encabezado.codigo_cliente);
            values.put("numeroDoc", encabezado.numero_doc);
            values.put("fechatrans", fechaActual);
            values.put("tipoTrans", tipoTrans); // 0 -> Pedido, 2 -> Cambio (PNC)
            values.put("montoFact", 0);
            values.put("descuento", 0);
            values.put("iva", 0);
            values.put("litros", 0);
            values.put("vendedor", Main.usuario.codigoVendedor);
            values.put("inicioPedido", encabezado.hora_inicial);
            values.put("finalPedido", encabezado.hora_final);
            values.put("sincronizado", 0);
            values.put("sync", 0);
            values.put("sincronizadoandroid", 0);

            db.insertOrThrow("EncabezadoAndroid", null, values);
            dbPedido.insertOrThrow("EncabezadoAndroid", null, values);

            /*******************************
             * Se crea el Detalle del Pedido
             *******************************/
            values = new ContentValues();
            values.put("numeroDoc", encabezado.numero_doc);
            values.put("fecha", fechaActual);
            values.put("codigoRef", detalle.codProducto);
            values.put("precio", detalle.precio);
            values.put("tarifaIva", detalle.iva);
            values.put("descuento", detalle.descuento);
            values.put("cantidad", detalle.cantidad);
            values.put("tipoPedido", tipoTrans);
            values.put("litros", 0);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);


            db.insertOrThrow("DetalleAndroid", null, values);
            dbPedido.insertOrThrow("DetalleAndroid", null, values);

            /******************************
             * Se crea la Novedad del Pedido
             ******************************/
	    /*values = new ContentValues();
			values.put("codigoCliente", encabezado.codigo_cliente);
			values.put("motivo",        encabezado.motivo);
			values.put("vendedor",      Main.usuario.codigoVendedor);
			values.put("valor",         0);
			values.put("NroDoc",        encabezado.numero_doc);
			values.put("horaInicio",    encabezado.hora_inicial);
			values.put("horaFinal",     encabezado.hora_final);
			values.put("version",       version);
			values.put("dispositivo",   "ANDROID");
			values.put("imei",          imei);

			db.insertOrThrow("NovedadesCompras", null, values);
			dbPedido.insertOrThrow("NovedadesCompras", null, values);*/

	    /*
			values.put("lista_precio",      encabezado.lista_precio);


			values.put("tipo_cliente",      encabezado.tipo_cliente);
			values.put("extra_ruta",        encabezado.extra_ruta);

			values.put("no_compra",         encabezado.no_compra);

			values.put("fecha_consecutivo", Main.usuario.fechaConsecutivo);
			values.put("sync",              0);
			values.put("version",           Main.versionApp);
			values.put("codigo_vendedor",   Main.usuario.codigoVendedor);*/

            //long rowId = db.insertOrThrow("Encabezado", null, values);

            //if (rowId != -1) {

            //encabezado.id = "" + rowId;

            /************************************
             * Se Ingresa el Detalle del Producto
             ************************************/


            /*******************************************************
             * Se Ingresa la Informacion del Producto en la Temporal
             ******************************************************/
	    /*ContentValues valuesTemp = new ContentValues();
				valuesTemp.put("cod_encabezado", rowId);
				valuesTemp.put("codigoCliente",  encabezado.codigo_cliente);
				valuesTemp.put("codigoRef",      detalle.codigo_producto);
				valuesTemp.put("cantidad",       detalle.cantidad);
				valuesTemp.put("precio",         detalle.precio);
				valuesTemp.put("iva", 			 detalle.iva);
				valuesTemp.put("descuento",      detalle.descuento_autorizado);
				valuesTemp.put("tipoPedido",     detalle.tipo_pedido);
				valuesTemp.put("inicioPedido",   encabezado.hora_inicial);
				valuesTemp.put("tipo_cliente",   encabezado.tipo_cliente);
				valuesTemp.put("extra_ruta",     encabezado.extra_ruta);
				valuesTemp.put("no_compra",      encabezado.no_compra);
				valuesTemp.put("numero_doc",     encabezado.numero_doc);
				valuesTemp.put("version",        Main.versionApp);

				dbPedido.insertOrThrow("TmpPedido", null, valuesTemp);*/

            return true;
            //}

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("RegistrarEncabezado", mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }

        return false;
    }


    public static boolean TerminoLabores() {

        int total = 0;
        SQLiteDatabase db = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT COUNT(*) AS total FROM NovedadesCompras WHERE fechaTermino IS NOT NULL";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    total = cursor.getInt(cursor.getColumnIndex("total"));
                }

                if (cursor != null)
                    cursor.close();

                return total > 0;

            } else {

                Log.e(TAG, "Verificando si Termino Labores: No existe la base de Datos: DataBase.db");
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "Verificando si Termino Labores: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean TerminoLabores_() {

        int total = 0;
        SQLiteDatabase db = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT COUNT(*) AS total FROM NovedadesComprasAndroid WHERE fechaTermino IS NOT NULL";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    total = cursor.getInt(cursor.getColumnIndex("total"));
                }

                if (cursor != null)
                    cursor.close();

                return total > 0;

            } else {

                Log.e(TAG, "Verificando si Termino Labores: No existe la base de Datos: DataBase.db");
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "Verificando si Termino Labores: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean ActulizarTerminoLabores() {

        SQLiteDatabase db = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values.put("fechaTermino", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));

            String whereArgs[] = new String[]{Main.usuario.codigoVendedor};
            int rowsNovedadesCompras = db.update("NovedadesCompras", values, "fecha IS NULL AND vendedor = ?", whereArgs);
            return rowsNovedadesCompras > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ActulizarTerminoLabores, " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean ActulizarTerminoLabores_() {

        SQLiteDatabase db = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values.put("fechaTermino", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));

            String whereArgs[] = new String[]{Main.usuario.codigoVendedor};
            int rowsNovedadesCompras = db.update("NovedadesComprasAndroid", values, "fecha IS NULL AND vendedor = ?", whereArgs);
            return rowsNovedadesCompras > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ActulizarTerminoLabores, " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean CancelarPedido(Encabezado encabezado) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{encabezado.numero_doc, encabezado.codigo_cliente};

            int rowsEncabezado = db.delete("Encabezado", "numeroDoc = ? AND codigo = ?", whereArgs);
            int rowDetalle = db.delete("Detalle", "numDoc = ?", new String[]{encabezado.numero_doc});
            int rowNovedades = db.delete("NovedadesCompras", "NroDoc = ? AND codigoCliente = ?", whereArgs);

            rowsEncabezado += dbPedido.delete("Encabezado", "numeroDoc = ? AND codigo = ?", whereArgs);
            rowDetalle += dbPedido.delete("Detalle", "numDoc = ?", new String[]{encabezado.numero_doc});
            rowNovedades += dbPedido.delete("NovedadesCompras", "NroDoc = ? AND codigoCliente = ?", whereArgs);

            Log.i("CancelarPedido", "rowsEncabezado = " + rowsEncabezado + " rowDetalle = " + rowDetalle + " rowNovedades = " + rowNovedades);
            return rowsEncabezado > 0 && rowDetalle > 0 && rowNovedades > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean CancelarPedido_(Encabezado encabezado) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{encabezado.numero_doc, encabezado.codigo_cliente};

            int rowsEncabezado = db.delete("EncabezadoAndroid", "numeroDoc = ? AND codigo = ?", whereArgs);
            int rowDetalle = db.delete("DetalleAndroid", "numDoc = ?", new String[]{encabezado.numero_doc});
            int rowNovedades = db.delete("NovedadesComprasAndroid", "NroDoc = ? AND codigoCliente = ?", whereArgs);

            rowsEncabezado += dbPedido.delete("EncabezadoAndroid", "numeroDoc = ? AND codigo = ?", whereArgs);
            rowDetalle += dbPedido.delete("DetalleAndroid", "numDoc = ?", new String[]{encabezado.numero_doc});
            rowNovedades += dbPedido.delete("NovedadesComprasAndroid", "NroDoc = ? AND codigoCliente = ?", whereArgs);

            Log.i("CancelarPedido", "rowsEncabezado = " + rowsEncabezado + " rowDetalle = " + rowDetalle + " rowNovedades = " + rowNovedades);
            return rowsEncabezado > 0 && rowDetalle > 0 && rowNovedades > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean RegistrarProductoPedido(Encabezado encabezado, Detalle detalle) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            /************************************
             * Se Ingresa el Detalle del Producto
             ************************************/
            values = new ContentValues();
            values.put("numeroDoc", encabezado.numero_doc);
            values.put("fecha", fechaActual);
            values.put("codigoRef", detalle.codProducto);
            values.put("precio", detalle.precio);
            values.put("tarifaIva", detalle.iva);
            values.put("descuento", detalle.descuento);
            values.put("cantidad", detalle.cantidad);
            values.put("tipoPedido", "0");
            values.put("litros", 0);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);
            values.put("tipoventa", 0);


            db.insertOrThrow("Detalle", null, values);
            dbPedido.insertOrThrow("Detalle", null, values);




	    /*values = new ContentValues();
			values.put("cod_encabezado",  encabezado.id);
			values.put("codigo_cliente",  detalle.codigo_cliente);
			values.put("codigo_producto", detalle.codigo_producto);
			values.put("precio",          detalle.precio);
			values.put("iva",             detalle.iva);
			values.put("descuento",       detalle.descuento_autorizado);
			values.put("cantidad",        detalle.cantidad);
			values.put("tipo_pedido",     detalle.tipo_pedido);
			values.put("sync",            0);

			db.insertOrThrow("Detalle", null, values);

			/*******************************************************
	     * Se Ingresa la Informacion del Producto en la Temporal
	     *******************************************************
			ContentValues valuesTemp = new ContentValues();
			valuesTemp.put("cod_encabezado", encabezado.id);
			valuesTemp.put("codigoCliente",  encabezado.codigo_cliente);
			valuesTemp.put("codigoRef",      detalle.codigo_producto);
			valuesTemp.put("cantidad",       detalle.cantidad);
			valuesTemp.put("precio",         detalle.precio);
			valuesTemp.put("iva", 			 detalle.iva);
			valuesTemp.put("descuento",      detalle.descuento_autorizado);
			valuesTemp.put("tipoPedido",     detalle.tipo_pedido);
			valuesTemp.put("inicioPedido",   encabezado.hora_inicial);
			valuesTemp.put("tipo_cliente",   encabezado.tipo_cliente);
			valuesTemp.put("extra_ruta",     encabezado.extra_ruta);
			valuesTemp.put("no_compra",      encabezado.no_compra);
			valuesTemp.put("numero_doc",     encabezado.numero_doc);
			valuesTemp.put("version",        Main.versionApp);

			dbPedido.insertOrThrow("TmpPedido", null, valuesTemp);*/

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean RegistrarProductoPedido_(Encabezado encabezado, Detalle detalle) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            /************************************
             * Se Ingresa el Detalle del Producto
             ************************************/
            values = new ContentValues();
            values.put("numeroDoc", encabezado.numero_doc);
            values.put("fecha", fechaActual);
            values.put("codigoRef", detalle.codProducto);
            values.put("precio", detalle.precio);
            values.put("tarifaIva", detalle.iva);
            values.put("descuento", detalle.descuento);
            values.put("cantidad", detalle.cantidad);
            values.put("tipoPedido", "0");
            values.put("litros", 0);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);


            db.insertOrThrow("DetalleAndroid", null, values);
            dbPedido.insertOrThrow("DetalleAndroid", null, values);




	    /*values = new ContentValues();
			values.put("cod_encabezado",  encabezado.id);
			values.put("codigo_cliente",  detalle.codigo_cliente);
			values.put("codigo_producto", detalle.codigo_producto);
			values.put("precio",          detalle.precio);
			values.put("iva",             detalle.iva);
			values.put("descuento",       detalle.descuento_autorizado);
			values.put("cantidad",        detalle.cantidad);
			values.put("tipo_pedido",     detalle.tipo_pedido);
			values.put("sync",            0);

			db.insertOrThrow("Detalle", null, values);

			/*******************************************************
	     * Se Ingresa la Informacion del Producto en la Temporal
	     *******************************************************
			ContentValues valuesTemp = new ContentValues();
			valuesTemp.put("cod_encabezado", encabezado.id);
			valuesTemp.put("codigoCliente",  encabezado.codigo_cliente);
			valuesTemp.put("codigoRef",      detalle.codigo_producto);
			valuesTemp.put("cantidad",       detalle.cantidad);
			valuesTemp.put("precio",         detalle.precio);
			valuesTemp.put("iva", 			 detalle.iva);
			valuesTemp.put("descuento",      detalle.descuento_autorizado);
			valuesTemp.put("tipoPedido",     detalle.tipo_pedido);
			valuesTemp.put("inicioPedido",   encabezado.hora_inicial);
			valuesTemp.put("tipo_cliente",   encabezado.tipo_cliente);
			valuesTemp.put("extra_ruta",     encabezado.extra_ruta);
			valuesTemp.put("no_compra",      encabezado.no_compra);
			valuesTemp.put("numero_doc",     encabezado.numero_doc);
			valuesTemp.put("version",        Main.versionApp);

			dbPedido.insertOrThrow("TmpPedido", null, valuesTemp);*/

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean ActualizarProductoPedido(Encabezado encabezado, String codProducto, int cantidad) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{encabezado.numero_doc, codProducto};

            /*************************************************************
             * Se Actualiza los datos del Producto, en Detalle y TmpPedido
             ************************************************************/
            values = new ContentValues();
            values.put("cantidad", cantidad);

            int rowDetalle = db.update("Detalle", values, "numeroDoc = ? AND codigoRef = ?", whereArgs);
            int rowsDetalleTemp = dbPedido.update("Detalle", values, "numeroDoc = ? AND codigoRef = ?", whereArgs);

            Log.i("ActualizarProductoPedido", "rowDetalle = " + rowDetalle + " rowsDetalleTemp = " + rowsDetalleTemp);
            return rowDetalle > 0 && rowsDetalleTemp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean ActualizarProductoPedido_(Encabezado encabezado, String codProducto, int cantidad) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{encabezado.numero_doc, codProducto};

            /*************************************************************
             * Se Actualiza los datos del Producto, en Detalle y TmpPedido
             ************************************************************/
            values = new ContentValues();
            values.put("cantidad", cantidad);

            int rowDetalle = db.update("DetalleAndroid", values, "numeroDoc = ? AND codigoRef = ?", whereArgs);
            int rowsDetalleTemp = dbPedido.update("DetalleAndroid", values, "numeroDoc = ? AND codigoRef = ?", whereArgs);

            Log.i("ActualizarProductoPedido", "rowDetalle = " + rowDetalle + " rowsDetalleTemp = " + rowsDetalleTemp);
            return rowDetalle > 0 && rowsDetalleTemp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean EliminarProductoPedido(Encabezado encabezado, String codProducto) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{encabezado.numero_doc, codProducto};

            /*************************************************************
             * Se Elimina el Producto del pedido, en Detalle y TmpPedido
             ************************************************************/

            int rowDetalle = db.delete("Detalle", "numeroDoc = ? AND codigoRef = ?", whereArgs);
            int rowsDetalleTmp = dbPedido.delete("Detalle", "numeroDoc = ? AND codigoRef = ?", whereArgs);

            Log.i("EliminarProductoPedido", "rowDetalle = " + rowDetalle + " rowsTmpPedido = " + rowsDetalleTmp);
            return rowDetalle > 0 && rowsDetalleTmp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean EliminarProductoPedido_(Encabezado encabezado, String codProducto) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{encabezado.numero_doc, codProducto};

            /*************************************************************
             * Se Elimina el Producto del pedido, en Detalle y TmpPedido
             ************************************************************/

            int rowDetalle = db.delete("DetalleAndroid", "numeroDoc = ? AND codigoRef = ?", whereArgs);
            int rowsDetalleTmp = dbPedido.delete("DetalleAndroid", "numeroDoc = ? AND codigoRef = ?", whereArgs);

            Log.i("EliminarProductoPedido", "rowDetalle = " + rowDetalle + " rowsTmpPedido = " + rowsDetalleTmp);
            return rowDetalle > 0 && rowsDetalleTmp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static Vector<MotivoCompra> ListaMotivosNoCompra(Vector<String> items) {

        SQLiteDatabase db = null;
        MotivoCompra motivoCompra;
        Vector<MotivoCompra> listaMotivos = new Vector<MotivoCompra>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("NoCompra",
                    new String[]{"Codigo", "Nombre"},
                    "Codigo <> 1",
                    null,
                    null,
                    null,
                    "Codigo");

            if (cursor.moveToFirst()) {

                do {

                    motivoCompra = new MotivoCompra();

                    motivoCompra.codigo = Util.ToInt(cursor.getString(cursor.getColumnIndex("Codigo")));
                    motivoCompra.motivo = cursor.getString(cursor.getColumnIndex("Nombre"));
                    motivoCompra.tipo = "nocompra";

                    listaMotivos.addElement(motivoCompra);
                    items.addElement(motivoCompra.motivo);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosNoCompra", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaMotivos;
    }

    public static Vector<Autorizaciones> ListaAutorizaciones(Vector<String> items) {

        SQLiteDatabase db = null;
        Autorizaciones autorizaciones;
        Vector<Autorizaciones> listaAutorizaciones = new Vector<Autorizaciones>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("Autorizados",
                    new String[]{"codigo", "nombre"},
                    null,
                    null,
                    null,
                    null,
                    "codigo");

            if (cursor.moveToFirst()) {

                do {

                    autorizaciones = new Autorizaciones();

                    autorizaciones.codigo = Util.ToInt(cursor.getString(cursor.getColumnIndex("codigo")));
                    autorizaciones.nombre = cursor.getString(cursor.getColumnIndex("nombre"));

                    listaAutorizaciones.addElement(autorizaciones);
                    items.addElement(String.valueOf(autorizaciones.codigo) + " - " + autorizaciones.nombre);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosNoCompra", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaAutorizaciones;
    }

    public static Vector<MotivoDescuento> ListaMotivosDescuentos(Vector<String> items) {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<MotivoDescuento> listaMotivos = new Vector<MotivoDescuento>();

        try {

            if (items == null)
                items = new Vector<String>();

            MotivoDescuento motivoDescuento;
            listaMotivos = new Vector<MotivoDescuento>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("MotivosDescuentos",
                    new String[]{"CodMotivo", "Descripcion"},
                    null,
                    null,
                    null,
                    null,
                    "CodMotivo");

            if (cursor.moveToFirst()) {

                do {

                    motivoDescuento = new MotivoDescuento();

                    motivoDescuento.CodMotivo = cursor.getInt(cursor.getColumnIndex("CodMotivo"));
                    motivoDescuento.Descripcion = cursor.getString(cursor.getColumnIndex("Descripcion"));

                    listaMotivos.addElement(motivoDescuento);
                    items.addElement(motivoDescuento.Descripcion);

                } while (cursor.moveToNext());

                mensaje = "Cargo correctamente los Motivos de Descuentos";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosDescuentos", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaMotivos;
    }

    public static Vector<Banco> ListaBancos(Vector<String> items) {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<Banco> listaBancos = new Vector<Banco>();

        try {

            Banco banco;
            listaBancos = new Vector<Banco>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("Bancos",
                    new String[]{"codigo", "nombre", "htkid"},
                    null,
                    null,
                    null,
                    null,
                    "codigo");

            if (cursor.moveToFirst()) {

                do {

                    banco = new Banco();
                    banco.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    banco.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    banco.htkid = cursor.getString(cursor.getColumnIndex("htkid"));

                    listaBancos.addElement(banco);
                    items.addElement(banco.nombre);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ListaBancos: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaBancos;
    }

    /*public static void UpdateSchema() {

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(dbFile.getPath(), null, SQLiteDatabase);

			try {

				db.execSQL("SELECT visitado FROM Rutero LIMIT 1");

			} catch (Exception e) {

				db.execSQL("ALTER TABLE Rutero ADD COLUMN visitado int");

				db.execSQL("UPDATE Rutero SET visitado = 0");
			}

		} catch (Exception e) {

				mensaje = e.getMessage();

			} finally {

				if (db != null)
					db.close();
			}
		}*/

    /*public static boolean ActulizarEstadoRutero(String codigoCliente) {

		File dbFile = new File(Util.DirApp(), "DataBase.db");
		SQLiteDatabase db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

		try {

			db.execSQL("UPDATE Rutero SET visitado = 1 WHERE codigo = '" + codigoCliente + "'");
			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			return false;

		} finally {

			if (db != null)
				db.close();
		}
	}*/

    /*public static boolean HayInformacionXEnviar() {

		mensaje = "";
		boolean hayInfoPendiente;

		try {

			File dbFile = new File(Util.DirApp(), "Temp.dp");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			String query = "SELECT COUNT(cod_encabezado) AS total FROM TmpPedido";
			Cursor cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {

				int total = cursor.getInt(cursor.getColumnIndex("total"));
				hayInfoPendiente = total > 0;

				mensaje = "Consulta satisfactoria";

			} else {

				hayInfoPendiente = false;
				mensaje = "Consulta sin Resultados";
			}

			if (cursor != null)
				cursor.close();

		} catch (Exception e) {

			hayInfoPendiente = false;
			mensaje = e.getMessage();

		} finally {

			if (db != null)
				db.close();
		}

		return hayInfoPendiente;
	}*/

    public static boolean HayInformacionXEnviar() {

        mensaje = "";
        SQLiteDatabase db = null;
        boolean hayInfoPendiente = false;
        int cont = 0;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT count(1) as cont FROM encabezado where sincronizado = 0";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        cont = cursor.getInt(cursor.getColumnIndex("cont"));

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                if (cont == 0) {

                    query = "SELECT count(1) as cont FROM detalle where sincronizado = 0";
                    cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            cont = cursor.getInt(cursor.getColumnIndex("cont"));

                        } while (cursor.moveToNext());
                    }

                    if (cursor != null)
                        cursor.close();
                }

                if (cont == 0) {

                    query = "SELECT count(1) as cont FROM NovedadesCompras where sincronizado = 0";
                    cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            cont = cursor.getInt(cursor.getColumnIndex("cont"));

                        } while (cursor.moveToNext());
                    }

                    if (cursor != null)
                        cursor.close();
                }

            } else {

                Log.e(TAG, "La base datos Temp.db No Existe o No tiene Acceso");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "HayInformacionXEnviar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        if (cont == 0)
            hayInfoPendiente = false;
        else
            hayInfoPendiente = true;

        return hayInfoPendiente;
    }


    public static boolean HayInformacionXEnviar_() {

        mensaje = "";
        SQLiteDatabase db = null;
        boolean hayInfoPendiente = false;
        int cont = 0;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT count(1) as cont FROM encabezadoAndroid where sincronizado = 0";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        cont = cursor.getInt(cursor.getColumnIndex("cont"));

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                if (cont == 0) {

                    query = "SELECT count(1) as cont FROM detalleAndroid where sincronizado = 0";
                    cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            cont = cursor.getInt(cursor.getColumnIndex("cont"));

                        } while (cursor.moveToNext());
                    }

                    if (cursor != null)
                        cursor.close();
                }

                if (cont == 0) {

                    query = "SELECT count(1) as cont FROM NovedadesComprasAndroid where sincronizado = 0";
                    cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            cont = cursor.getInt(cursor.getColumnIndex("cont"));

                        } while (cursor.moveToNext());
                    }

                    if (cursor != null)
                        cursor.close();
                }

            } else {

                Log.e(TAG, "La base datos Temp.db No Existe o No tiene Acceso");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "HayInformacionXEnviar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        if (cont == 0)
            hayInfoPendiente = false;
        else
            hayInfoPendiente = true;

        return hayInfoPendiente;
    }


    public static boolean HayInformacionXEnviar1() {

        mensaje = "";
        SQLiteDatabase db = null;
        boolean hayInfoPendiente = false;

        try {

            File dbFile = new File(Util.DirApp(), "Temp.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                Vector<String> tableNames = new Vector<String>();
                String query = "SELECT tbl_name FROM sqlite_master";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        String tableName = cursor.getString(cursor.getColumnIndex("tbl_name"));

                        if (tableName.equals("android_metadata"))
                            continue;

                        tableNames.addElement(tableName);

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                for (String tableName : tableNames) {

                    query = "SELECT COUNT(*) AS total FROM " + tableName;
                    cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        int total = cursor.getInt(cursor.getColumnIndex("total"));

                        if (total > 0) {

                            hayInfoPendiente = true;
                            break;
                        }
                    }

                    if (cursor != null)
                        cursor.close();
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "La base datos Temp.db No Existe o No tiene Acceso");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "HayInformacionXEnviar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return hayInfoPendiente;
    }

    public static String ObtenterNumeroDoc(String codVendedor) {

        String num = "-";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

	    /*
    		String fechaConsecutivo = "";
    		String query = "SELECT fechaConsecutivo FROM Vendedor";
    		Cursor cursor = db.rawQuery(query, null);

    		if (cursor.moveToFirst()) {

    			fechaConsecutivo = cursor.getString(cursor.getColumnIndex("fechaConsecutivo"));
    		}

    		if (cursor != null)
    			cursor.close();
	     */

            int consecutivo = 0;
            Cursor cursor = db.rawQuery("SELECT consecutivo FROM Vendedor", null);

            if (cursor.moveToFirst())
                consecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));

            if (cursor != null)
                cursor.close();

            consecutivo = (consecutivo + 1) % 200;
            db.execSQL("UPDATE Vendedor SET consecutivo = " + consecutivo);

            if (cursor != null)
                cursor.close();

            //A+vendedor+anomesdia+consecutivo(4)

            num = "A" + codVendedor + Util.FechaActual("yyyyMMddHHmm") + Util.lpad("" + consecutivo, 4, "0");
            return num;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenterNumeroDoc", mensaje, e);
            return "-";

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static String ObtenterNumeroDocLiq(String codVendedor) {

        String num = "-";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            int consecutivo = 0;
            Cursor cursor = db.rawQuery("SELECT consecutivo FROM Vendedor", null);

            if (cursor.moveToFirst())
                consecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));

            if (cursor != null)
                cursor.close();

            consecutivo = (consecutivo + 1) % 200;
            db.execSQL("UPDATE Vendedor SET consecutivo = " + consecutivo);

            if (cursor != null)
                cursor.close();

            num = "L" + codVendedor + Util.FechaActual("yyyyMMddHHmm") + Util.lpad("" + consecutivo, 4, "0");
            return num;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenterNumeroDoc", mensaje, e);
            return "-";

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static String ObtenterNumeroDocGastoViaje(String codVendedor) {

        String num = "-";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            int consecutivo = 0;
            Cursor cursor = db.rawQuery("SELECT consecutivo FROM Vendedor", null);

            if (cursor.moveToFirst())
                consecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));

            if (cursor != null)
                cursor.close();

            consecutivo = (consecutivo + 1) % 200;
            db.execSQL("UPDATE Vendedor SET consecutivo = " + consecutivo);

            if (cursor != null)
                cursor.close();

            //A+vendedor+anomesdia+consecutivo(4)

            num = "5" + codVendedor + Util.FechaActual("yyyyMMdd") + Util.lpad("" + consecutivo, 4, "0");
            return num;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenterNumeroDoc", mensaje, e);
            return "-";

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static String ObtenterNumeroDocRecaudo() {

        String num = "-";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            int total = 0;
            Cursor cursor = db.rawQuery("SELECT COUNT(fecha_consecutivo) AS total FROM EncabezadoRecaudo WHERE fecha_consecutivo = '" + Main.usuario.fechaConsecutivo + "'", null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));
            }

            if (cursor != null)
                cursor.close();

            if (total == 0) {

                //No se ha generado ningun Numero Doc para el dia Actual, se Reincia el consecutivo
                db.execSQL("DELETE FROM ConsecutivoRecaudo");
            }

            db.execSQL("ConsecutivoRecaudo(id) VALUES ((SELECT CASE WHEN MAX(id) IS NULL THEN 0 ELSE MAX(ROWID) END FROM ConsecutivoRecaudo) + 1)");


            cursor = db.rawQuery("SELECT MAX(id) AS id FROM ConsecutivoRecaudo", null);

            if (cursor.moveToFirst()) {

                int consecutivo = cursor.getInt(cursor.getColumnIndex("id"));
                //num = Util.lpad(Main.usuario.codigoEmpresa, 4, "0") + Main.usuario.codigoVendedor + Main.usuario.fechaConsecutivo + Util.lpad("" + consecutivo, 3, "0");
                num = Main.deviceId + Main.usuario.fechaConsecutivo + Util.lpad("" + consecutivo, 3, "0");
            }

            if (cursor != null)
                cursor.close();

            return num;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return "-";

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static boolean ProductoXCodigo(String codigoProducto, Producto producto) {

        if (producto == null) {

            mensaje = "El producto no puede ser NULL";
            return false;
        }

        mensaje = "";
        boolean cargo = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";

	    /*query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
					"FROM Productos " +
					"INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '"+ listaPrecio +"' " +
					"WHERE Productos.codigo = '%"+ codigoProducto +"%'";*/

            query = "SELECT codigo, nombre, precio, iva, saldo, (precio + ((precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                    "FROM ProductosTmp " +
                    "WHERE codigo = '" + codigoProducto + "'";

            System.out.println("Consulta del producto barras ---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                producto.precio = cursor.getInt(cursor.getColumnIndex("precio"));
                producto.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                producto.inventario = cursor.getInt(cursor.getColumnIndex("saldo"));
                producto.precioIva = cursor.getFloat(cursor.getColumnIndex("precioIva"));
                producto.unidadesXCaja = cursor.getString(cursor.getColumnIndex("unidadesxcaja"));
                producto.ean = cursor.getString(cursor.getColumnIndex("ean"));
                producto.agrupacion = cursor.getString(cursor.getColumnIndex("agrupacion"));
                producto.grupo = cursor.getString(cursor.getColumnIndex("grupo"));
                producto.linea = cursor.getString(cursor.getColumnIndex("linea"));
                producto.unidadMedida = cursor.getString(cursor.getColumnIndex("unidadmedida"));
                producto.factorLibras = cursor.getInt(cursor.getColumnIndex("factorlibras"));
                producto.fechaLimite = cursor.getString(cursor.getColumnIndex("fechalimite"));
                producto.devol = cursor.getString(cursor.getColumnIndex("devol"));
                producto.itf = cursor.getString(cursor.getColumnIndex("itf"));
                producto.impto1 = cursor.getInt(cursor.getColumnIndex("impto1"));
                producto.indice = cursor.getInt(cursor.getColumnIndex("Indice"));

                producto.precioDescuento = 0;
                producto.descuento = 0;

                cargo = true;
                mensaje = "Cargo el producto correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

            return cargo;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ProductoXCodigo", mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    /**
     * verificar que sea un producto valido
     *
     * @param codigoProducto
     * @return
     */
    public static boolean validarProducto(String codigoProducto) {

        mensaje = "";
        boolean cargo = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";


            query = "SELECT codigo, nombre, precio, iva, saldo, (precio + ((precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                    "FROM ProductosTmp " +
                    "WHERE codigo = '" + codigoProducto + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                cargo = true;
                mensaje = "Cargo el producto correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

            return cargo;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ProductoXCodigo", mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static Hashtable<String, Producto> ProductoXCodigo_(String codigoProducto) {

        Producto producto;
        Hashtable<String, Producto> listaProducto = new Hashtable<String, Producto>();

        mensaje = "";
        boolean cargo = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";

	    /*query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
					"FROM Productos " +
					"INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '"+ listaPrecio +"' " +
					"WHERE Productos.codigo = '%"+ codigoProducto +"%'";*/

            query = "SELECT codigo, nombre, precio, iva, saldo, (precio + ((precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                    "FROM ProductosTmp " +
                    "WHERE codigo = '" + codigoProducto + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    producto = new Producto();
                    producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    producto.precio = cursor.getInt(cursor.getColumnIndex("precio"));
                    producto.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    producto.inventario = cursor.getInt(cursor.getColumnIndex("saldo"));
                    producto.precioIva = cursor.getFloat(cursor.getColumnIndex("precioIva"));
                    producto.unidadesXCaja = cursor.getString(cursor.getColumnIndex("unidadesxcaja"));
                    producto.ean = cursor.getString(cursor.getColumnIndex("ean"));
                    producto.agrupacion = cursor.getString(cursor.getColumnIndex("agrupacion"));
                    producto.grupo = cursor.getString(cursor.getColumnIndex("grupo"));
                    producto.linea = cursor.getString(cursor.getColumnIndex("linea"));
                    producto.unidadMedida = cursor.getString(cursor.getColumnIndex("unidadmedida"));
                    producto.factorLibras = cursor.getInt(cursor.getColumnIndex("factorlibras"));
                    producto.fechaLimite = cursor.getString(cursor.getColumnIndex("fechalimite"));
                    producto.devol = cursor.getString(cursor.getColumnIndex("devol"));
                    producto.itf = cursor.getString(cursor.getColumnIndex("itf"));
                    producto.impto1 = cursor.getInt(cursor.getColumnIndex("impto1"));
                    producto.indice = cursor.getInt(cursor.getColumnIndex("Indice"));

                    producto.precioDescuento = 0;
                    producto.descuento = 0;


                    listaProducto.put(producto.codigo, producto);

                } while (cursor.moveToNext());
                mensaje = "Cargo el producto correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

            return listaProducto;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ProductoXCodigo", mensaje, e);
            return listaProducto;

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static boolean ListarProductos(String listaPrecio) {

        mensaje = "";
        boolean cargo = false;
        SQLiteDatabase db = null;
        Producto producto;
        Main.navegadorProductos = null;

        Main.navegadorProductos = new ArrayList<Producto>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";

	    /*query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
					"FROM Productos " +
					"INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '"+ listaPrecio +"' " +
					"ORDER BY Indice";*/

	    /*
			query = "SELECT codigo, nombre, precio, iva, saldo, (precio + ((precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
					"FROM ProductosTmp " +
					"ORDER BY Indice";*/

            query = "" +
                    "SELECT CASE WHEN itf='S' THEN round((impto1*100.0/precio)) ELSE iva END AS iva," +
                    "CASE WHEN itf='S' THEN round(precio + ((precio * round((impto1*100.0/precio),2)) / 100.0),2) ELSE round(precio + ((precio * iva) / 100.0),2) END AS precioIva, " +
                    "codigo, nombre, precio, saldo, unidadesxcaja, ean, " +
                    "agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                    "FROM ProductosTmp " +
                    "ORDER BY Indice";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    producto = new Producto();

                    producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    producto.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    producto.iva = cursor.getFloat(cursor.getColumnIndex("iva"));

                    producto.inventario = cursor.getInt(cursor.getColumnIndex("saldo"));

                    producto.precioIva = cursor.getFloat(cursor.getColumnIndex("precioIva"));
                    producto.unidadesXCaja = cursor.getString(cursor.getColumnIndex("unidadesxcaja"));
                    producto.ean = cursor.getString(cursor.getColumnIndex("ean"));
                    producto.agrupacion = cursor.getString(cursor.getColumnIndex("agrupacion"));
                    producto.grupo = cursor.getString(cursor.getColumnIndex("grupo"));
                    producto.linea = cursor.getString(cursor.getColumnIndex("linea"));
                    producto.unidadMedida = cursor.getString(cursor.getColumnIndex("unidadmedida"));
                    producto.factorLibras = cursor.getInt(cursor.getColumnIndex("factorlibras"));
                    producto.fechaLimite = cursor.getString(cursor.getColumnIndex("fechalimite"));
                    producto.devol = cursor.getString(cursor.getColumnIndex("devol"));
                    producto.itf = cursor.getString(cursor.getColumnIndex("itf"));
                    producto.impto1 = cursor.getInt(cursor.getColumnIndex("impto1"));
                    producto.indice = cursor.getInt(cursor.getColumnIndex("Indice"));

                    producto.precioDescuento = 0f;
                    producto.descuento = 0f;

                    Main.navegadorProductos.add(producto);

                } while (cursor.moveToNext());

            } else {
                mensaje = "Consulta sin resultados";
            }
            if (cursor != null)
                cursor.close();

            return cargo;
        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ProductoXCodigo", mensaje, e);
            return false;

        } finally {
            if (db != null)
                db.close();
        }
    }


    /**
     * Metodo para crear una vista con el saldo disponible de productos (Inicial menos ventas).
     * La vista es creada solamente para usuario de tipo autoventa.
     */
    public static void crearVistaProductosAutoventa() {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            db.execSQL("DROP VIEW IF EXISTS PRODUCTOS_VIEW");
            /* Crear una vista a la cual consultar los productos con su inventario actual del producto.
             * el saldo es la cantidad de producto menos las ventas realizadas que no han sido anuladas*/
            String createView = ""
                    + "CREATE VIEW IF NOT EXISTS PRODUCTOS_VIEW AS "
                    + "    SELECT p.codigo AS codigo, "
                    + "p.nombre AS nombre, "
                    + "p.precio AS precio, "
                    + "p.saldo - COALESCE(v.cantidad, 0) AS saldo, "
                    + "p.iva AS iva, "
                    + "p.unidadesxcaja AS unidadesxcaja, "
                    + "p.ean AS ean, "
                    + "p.agrupacion AS agrupacion, "
                    + "p.grupo AS grupo, "
                    + "p.linea AS linea, "
                    + "p.unidadmedida AS unidadmedida, "
                    + "p.factorlibras AS factorlibras, "
                    + "p.fechalimite AS fechalimite, "
                    + "p.devol AS devol, "
                    + "p.itf AS itf, "
                    + "p.impto1 AS impto1, "
                    + "p.Indice AS Indice, "
                    + "p.Orden AS Orden "
                    + "FROM Productos p "
                    + "LEFT JOIN ( "
                    + "select codigo,sum(cantidad) cantidad from ( "
                    + "SELECT d.codigoRef AS codigo, "
                    + "SUM(d.cantidad) AS cantidad "
                    + "FROM Detalle d "
                    + "INNER JOIN Encabezado e ON e.numeroDoc = d.numDoc AND ifnull(e.anulado,0) = 0 AND e.tipotrans = 0 and e.observaciones <> 'FALTANTE PRODUCTO DANADO' "
                    + "GROUP BY d.codigoRef "
                    + "union all "
                    + "select ds.codigoref,sum(cantidad) cantidad from encabezadosugerido es inner join detallesugerido ds  "
                    + "on es.numerodoc = ds.numdoc where es.tipotrans=8  "
                    + "group by ds.codigoref) tmp "
                    + "group by codigo order by codigo "
                    + ") v ON v.codigo = p.codigo "
                    + "WHERE p.tipocliente = 0;";
            db.execSQL(createView);

            System.out.println("VISTAAAA---> " + createView);

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e("crearVistaProductosAutoventa", mensaje, e);
        } finally {
            if (db != null)
                db.close();
        }
    }




    /*public static boolean EliminarPedidos() {

		File dbFile = new File(Util.DirApp(), "DataBase.db");
		db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

		try {

			db.execSQL("DELETE FROM Encabezado");
			db.execSQL("DELETE FROM Detalle");
			db.execSQL("VACUUM");

			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			return false;

		} finally {

			if (db != null)
				db.close();
		}
	}*/

    /*public static boolean BorrarDatosTemporal() {

		SQLiteDatabase dbTemp = null;

		try {

			File fileTemp = new File(Util.DirApp(), "Temp.db");
			dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			dbTemp.execSQL("DELETE FROM Encabezado");
			dbTemp.execSQL("DELETE FROM Detalle");
			dbTemp.execSQL("DELETE FROM NovedadesCompras");
			dbTemp.execSQL("DELETE FROM Fotos");
			dbTemp.execSQL("VACUUM");

			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.i("BorrarDatosTemporal", mensaje, e);
			return false;

		} finally {

			if (dbTemp != null)
				dbTemp.close();
		}
	}*/

    public static boolean BorrarInfoTemp() {

        SQLiteDatabase dbTemp = null;

        try {

            File dbFile = new File(Util.DirApp(), "Temp.db");

            if (dbFile.exists()) {

                dbTemp = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                Vector<String> tableNames = new Vector<String>();
                String query = "SELECT tbl_name FROM sqlite_master";
                Cursor cursor = dbTemp.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        String tableName = cursor.getString(cursor.getColumnIndex("tbl_name"));

                        if (tableName.equals("android_metadata"))
                            continue;

                        tableNames.addElement(tableName);

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                for (String tableName : tableNames) {

                    query = "DELETE FROM " + tableName;
                    dbTemp.execSQL(query);
                }
            }

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarInfoTemp: " + mensaje, e);
            return false;

        } finally {

            if (dbTemp != null)
                dbTemp.close();
        }
    }

    public static boolean BorrarFotosTemporal(String nroDoc) {

        SQLiteDatabase dbTemp = null;

        try {

            File fileTemp = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            dbTemp.execSQL("DELETE FROM Fotos WHERE nroDoc = '" + nroDoc + "'");
            dbTemp.execSQL("VACUUM");

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.i("BorrarFotosTemporal", mensaje, e);
            return false;

        } finally {

            if (dbTemp != null)
                dbTemp.close();
        }
    }

    public static boolean ActualizarSyncPedidos() {

        SQLiteDatabase db = null;
        //SQLiteDatabase dbTemp = null;

        try {

            //int i = 0;
            //String in = "(";

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

	    /*File filePedido = new File(Util.DirApp(), "Temp.db");
			dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			Cursor cursor = dbTemp.rawQuery("SELECT DISTINCT numeroDoc FROM Encabezado", null);
			if (cursor.moveToFirst()) {

				do {

					if (i++ > 0)
						in += ", ";

					in += cursor.getInt(cursor.getColumnIndex("numeroDoc"));

				} while(cursor.moveToNext());
			}

			in += ")";

			if (cursor !=  null)
				cursor.close();*/

            db.execSQL("UPDATE Encabezado SET syncmobile = 1 WHERE syncmobile = 0");
            db.execSQL("UPDATE Tiempos SET sincronizado = 1 WHERE sincronizado = 0");
            db.execSQL("UPDATE detallegastos SET sincronizado = 1 WHERE sincronizado = 0");
            db.execSQL("UPDATE consignaciones SET sincronizado = 1 WHERE sincronizado = 0");
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ActualizarSyncPedidos: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            //if (dbTemp != null)
            //	dbTemp.close();
        }
    }


    public static boolean ActualizarSyncPedidos_() {

        SQLiteDatabase db = null;
        //SQLiteDatabase dbTemp = null;

        try {

            //int i = 0;
            //String in = "(";

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

	    /*File filePedido = new File(Util.DirApp(), "Temp.db");
			dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			Cursor cursor = dbTemp.rawQuery("SELECT DISTINCT numeroDoc FROM Encabezado", null);
			if (cursor.moveToFirst()) {

				do {

					if (i++ > 0)
						in += ", ";

					in += cursor.getInt(cursor.getColumnIndex("numeroDoc"));

				} while(cursor.moveToNext());
			}

			in += ")";

			if (cursor !=  null)
				cursor.close();*/

            db.execSQL("UPDATE EncabezadoAndroid SET sync = 1 WHERE sync = 0");
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ActualizarSyncPedidos: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            //if (dbTemp != null)
            //	dbTemp.close();
        }
    }



    /*public static boolean ActualizarSnPedidosTmp() {

		SQLiteDatabase db = null;
		SQLiteDatabase dbPedido = null;

		try {

			int i = 0;
			String in = "(";

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			File filePedido = new File(Util.DirApp(), "Temp.dp");
			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			Cursor cursor = dbPedido.rawQuery("SELECT DISTINCT cod_encabezado FROM TmpPedido", null);
			if (cursor.moveToFirst()) {

				do {

					if (i++ > 0)
						in += ", ";

					in += cursor.getInt(cursor.getColumnIndex("cod_encabezado"));

				} while(cursor.moveToNext());
			}

			in += ")";

			if (cursor !=  null)
				cursor.close();

			db.execSQL("UPDATE Encabezado SET sn_envio = 1 WHERE id IN " + in);

			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("ActualizarSnPedidosTmp", mensaje, e);
			return false;

		} finally {

			if (db != null)
				db.close();

			if (dbPedido != null)
				dbPedido.close();
		}
	}*/

    /*public static boolean ActualizarSyncNovedad(String numero_doc) {

		SQLiteDatabase db = null;
		SQLiteDatabase dbPedido = null;

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			File filePedido = new File(Util.DirApp(), "Temp.dp");
			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			db.execSQL("UPDATE Encabezado SET sync = 1 WHERE numero_doc = '" + numero_doc + "'");
			dbPedido.execSQL("DELETE FROM TmpPedido WHERE numero_doc = '" + numero_doc + "'");

			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			return false;

		} finally {

			if (dbPedido != null)
				dbPedido.close();

			if (db != null)
				db.close();
		}
	}*/

    /*public static boolean GuardarRecaudo() {

		SQLiteDatabase db = null;
		SQLiteDatabase dbPedido = null;

		try {

			String nroDoc = ObtenterNumeroDocRecaudo();

			File dbFile = new File(Util.DirApp(), "DataBase.db");
    		db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			File filePedido = new File(Util.DirApp(), "Temp.dp");
			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			ContentValues values = new ContentValues();
			//ContentValues valuesTemp = new ContentValues();

			//Insert into Encabezadorecaudo (nrodoc, codigocliente, total, fecha_recaudo, vendedor, bodega, tipocliente, sincronizadomapas)

			//Cartera cartera = new Cartera();
			//cartera.codCliente;

			values.put("Nrodoc",            nroDoc);
			values.put("CodigoCliente",     Main.cliente.codigo);
			values.put("Total",             Main.total_recaudo);
			values.put("Fecha_recaudo",     Main.usuario.fechaLabores);
			values.put("Vendedor",          Main.usuario.codigoVendedor);
			values.put("Bodega",            Main.usuario.bodega);
			values.put("Tipocliente",       0);
			values.put("sincronizadomapas", 0);
			values.put("fecha_consecutivo", Main.usuario.fechaConsecutivo);


			long rowId = db.insertOrThrow("EncabezadoRecaudo", null, values);
			long rowIdTemp = dbPedido.insertOrThrow("EncabezadoRecaudo", null, values);

			for (Cartera cartera : Main.cartera) {

				//INSERT INTO detallerecaudo (Nrodoc, Tiporecaudo, valor, CodigoBanco, vendedor, bodega, Tipocliente, numdoc, observacion, prontopago, tipopago)

				values = new ContentValues();
				values.put("id",          rowId);
				values.put("Nrodoc",      nroDoc);
				values.put("Tiporecaudo", 0);
				values.put("valor",       cartera.valorARecaudar);
				values.put("CodigoBanco", "00");
				values.put("vendedor",    Main.usuario.codigoVendedor);
				values.put("bodega",      Main.usuario.bodega);
				values.put("Tipocliente", 0);
				values.put("numdoc",      cartera.referencia);
				values.put("observacion", cartera.observacion);
				values.put("prontopago",  0);
				values.put("tipopago",    cartera.pagoTotal ? "T" : "P");

				db.insertOrThrow("DetalleRecaudo", null, values);

				//values.remove("id");
				values.put("id", rowIdTemp);
				dbPedido.insertOrThrow("DetalleRecaudo", null, values);
			}

			Enumeration<FormaPago> e = Main.listaFormaPago.elements();

			while (e.hasMoreElements()) {

				//INSERT INTO formapago (Nrodoc, forma_pago, monto, nro_cheque_cons, banco, fecha_registro, fecha_post, codigocliente, vendedor, bodega, tipocliente)
				FormaPago formaPago = e.nextElement();
				values = new ContentValues();
				values.put("id",              rowId);
				values.put("Nrodoc",          nroDoc);
				values.put("forma_pago",      formaPago.codigo);
				values.put("monto",           formaPago.valor);
				values.put("nro_cheque_cons", formaPago.nroDocumento);
				values.put("banco",           formaPago.codigoBanco);
				values.put("fecha_registro",  Main.usuario.fechaLabores);
				values.put("fecha_post",      formaPago.fechaPago);
				values.put("codigocliente",   Main.cliente.codigo);
				values.put("vendedor",        Main.usuario.codigoVendedor);
				values.put("bodega",          Main.usuario.bodega);
				values.put("tipocliente",     0);

				db.insertOrThrow("FormaPago", null, values);

				values.put("id", rowIdTemp);
				dbPedido.insertOrThrow("FormaPago", null, values);
			}

			return true;

		} catch (Exception e) {

			mensaje = e.getMessage();
			Log.e("GuardarRecaudo", mensaje, e);
			return false;

		} finally {

			if (db != null)
				db.close();

			if (dbPedido != null)
				dbPedido.close();
		}
	}*/

    public static ResumenSync CagarResumenSincronizacion() {

        mensaje = "";
        SQLiteDatabase db = null;
        ResumenSync resumenSync = new ResumenSync();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT COUNT(codigo) AS totalClientes, " +
                    "(SELECT COUNT(codigo) FROM Referencias) AS totalReferencias, " +
                    "(SELECT COUNT(codigo) FROM Rutero) AS totalRutero, " +
                    "(SELECT COUNT(codigo) FROM MotivosCompras) AS totalComplementos " +
                    "FROM Clientes", new String[]{});

            if (cursor.moveToFirst()) {

                resumenSync.totalClientes = cursor.getInt(cursor.getColumnIndex("totalClientes"));
                resumenSync.totalReferencias = cursor.getInt(cursor.getColumnIndex("totalReferencias"));
                resumenSync.totalRutero = cursor.getInt(cursor.getColumnIndex("totalRutero"));
                resumenSync.totalComplementos = cursor.getInt(cursor.getColumnIndex("totalComplementos"));
                mensaje = "Cargo el Resumen correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

            return resumenSync;

        } catch (Exception e) {

            mensaje = "Error Cargando Resumen: " + e.getMessage();
            return null;

        } finally {

            if (db != null)
                db.close();
        }
    }

    /**
     * Se encarga de Verficiar si Existe Informacion para la Tabla Especificada.
     * Si existe el archivo de Base de datos y si Contiene Datos
     **/
    public static boolean ExisteInformacion(String nombreTabla) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (!dbFile.exists())
                return false;

            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            Cursor cursor = db.rawQuery("SELECT COUNT(*) AS total FROM " + nombreTabla, null);

            if (cursor.moveToFirst()) {

                int total = cursor.getInt(cursor.getColumnIndex("total"));
                existe = total > 0;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExisteInformacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }

    public static EstadisticaRecorrido CargarEstadisticas1(String fechaConsecutivo) {

        mensaje = "";
        SQLiteDatabase db = null;
        EstadisticaRecorrido estadisticaRecorrido = new EstadisticaRecorrido();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query =

                        "SELECT COUNT(*) AS total_visitas, " +
                                "(SELECT COUNT(*) FROM Encabezado WHERE tipoTrans = 0) AS total_pedidos, " +
                                "(SELECT COUNT(*) FROM Encabezado WHERE tipoTrans = 0 AND sincronizado = 1) AS total_pedidos_sync, " +
                                "(SELECT COUNT(*) FROM Encabezado WHERE tipoTrans = 0 AND sincronizado = 0) AS total_pedidos_sin_sync, " +
                                "(SELECT SUM(montoFact) FROM EncabezadoA WHERE tipoTrans = 0) AS total_venta " +
                                "FROM NovedadesCompras";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    estadisticaRecorrido.total_visitas = cursor.getInt(cursor.getColumnIndex("total_visitas"));
                    estadisticaRecorrido.total_pedidos = cursor.getInt(cursor.getColumnIndex("total_pedidos"));
                    estadisticaRecorrido.total_pedidos_sync = cursor.getInt(cursor.getColumnIndex("total_pedidos_sync"));
                    estadisticaRecorrido.total_pedidos_sin_sync = cursor.getInt(cursor.getColumnIndex("total_pedidos_sin_sync"));
                    estadisticaRecorrido.total_venta = cursor.getInt(cursor.getColumnIndex("total_venta"));

                    if (estadisticaRecorrido.total_visitas > 0)
                        estadisticaRecorrido.efectividad = (estadisticaRecorrido.total_pedidos * 100) / estadisticaRecorrido.total_visitas;
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarEstadisticas -> No Existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarEstadisticas -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return estadisticaRecorrido;
    }


    public static EstadisticaRecorrido CargarEstadisticas(String fechaConsecutivo) {

        mensaje = "";
        SQLiteDatabase db = null;
        EstadisticaRecorrido estadisticaRecorrido = new EstadisticaRecorrido();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = ""
                        + " SELECT COUNT(*) AS visitas,"
                        + " (SELECT COUNT(1) FROM (SELECT COUNT(DISTINCT 1) FROM Encabezado GROUP BY codigo HAVING COALESCE(anulado,0) = 0)) AS total_visitas,"
                        + " (SELECT COUNT(*) FROM Encabezado WHERE tipoTrans = 0 AND (anulado IS NULL OR anulado = 0)) AS total_pedidos, "
                        + " (SELECT COUNT(*) FROM Encabezado WHERE tipoTrans = 0 AND syncmobile = 1 AND (anulado IS NULL OR anulado = 0)) AS total_pedidos_sync, "
                        + " (SELECT COUNT(*) FROM Encabezado WHERE tipoTrans = 0 AND syncmobile = 0 AND (anulado IS NULL OR anulado = 0)) AS total_pedidos_sin_sync, "
                        + " (SELECT SUM(montoFact) FROM Encabezado WHERE tipoTrans = 0 AND (anulado IS NULL OR anulado = 0)) AS total_venta, "
                        + " (SELECT SUM(iva) FROM Encabezado WHERE tipoTrans = 0 AND (anulado IS NULL OR anulado = 0)) AS iva, "
                        + " (SELECT COUNT(*) FROM Encabezado WHERE tipoTrans = 2 AND (anulado IS NULL OR anulado = 0)) AS devoluciones, "
                        + " (SELECT COALESCE(SUM(montoFact),0) FROM Encabezado WHERE tipoTrans = 2 AND (anulado IS NULL OR anulado = 0)) AS montoDevoluciones,"
                        + " (SELECT COALESCE(SUM(Total),0) FROM encabezadorecaudo) AS totalRecaudos "
                        + " FROM Encabezado WHERE fechaFinal IS NOT NULL;";

                Cursor cursor = db.rawQuery(query, null);

                System.out.println("Consulta del Recorridoo: " + query);

                if (cursor.moveToFirst()) {

                    estadisticaRecorrido.total_visitas = cursor.getInt(cursor.getColumnIndex("total_visitas"));
                    estadisticaRecorrido.total_pedidos = cursor.getInt(cursor.getColumnIndex("total_pedidos"));
                    estadisticaRecorrido.total_pedidos_sync = cursor.getInt(cursor.getColumnIndex("total_pedidos_sync"));
                    estadisticaRecorrido.total_pedidos_sin_sync = cursor.getInt(cursor.getColumnIndex("total_pedidos_sin_sync"));
                    estadisticaRecorrido.total_venta = cursor.getFloat(cursor.getColumnIndex("total_venta"));
                    estadisticaRecorrido.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    estadisticaRecorrido.devoluciones = cursor.getInt(cursor.getColumnIndex("devoluciones"));
                    estadisticaRecorrido.montoDevoluciones = cursor.getDouble(cursor.getColumnIndex("montoDevoluciones"));
                    estadisticaRecorrido.totalVentaBruta = (double) (estadisticaRecorrido.total_venta - estadisticaRecorrido.montoDevoluciones - estadisticaRecorrido.iva);
                    estadisticaRecorrido.totalRecaudo = cursor.getDouble(cursor.getColumnIndex("totalRecaudos"));
                    estadisticaRecorrido.totalGlobal = estadisticaRecorrido.totalVentaBruta + estadisticaRecorrido.totalRecaudo;
                    if (estadisticaRecorrido.total_visitas > 0)
                        estadisticaRecorrido.efectividad = (estadisticaRecorrido.total_pedidos * 100) / estadisticaRecorrido.total_visitas;
                }

                if (cursor != null)
                    cursor.close();

            } else {
                Log.e(TAG, "CargarEstadisticas -> No Existe la Base de Datos");
            }
        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarEstadisticas -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return estadisticaRecorrido;
    }


    public static EstadisticaRecorrido CargarEstadisticas_(String fechaConsecutivo) {

        mensaje = "";
        SQLiteDatabase db = null;
        EstadisticaRecorrido estadisticaRecorrido = new EstadisticaRecorrido();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query =

                        "SELECT COUNT(*) AS total_visitas, " +
                                "(SELECT COUNT(*) FROM EncabezadoAndroid WHERE tipoTrans = 0) AS total_pedidos, " +
                                "(SELECT COUNT(*) FROM EncabezadoAndroid WHERE tipoTrans = 0 AND sincronizado = 1) AS total_pedidos_sync, " +
                                "(SELECT COUNT(*) FROM EncabezadoAndroid WHERE tipoTrans = 0 AND sincronizado = 0) AS total_pedidos_sin_sync, " +
                                "(SELECT SUM(montoFact) FROM EncabezadoAndroid WHERE tipoTrans = 0) AS total_venta " +
                                "FROM NovedadesComprasAndroid";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    estadisticaRecorrido.total_visitas = cursor.getInt(cursor.getColumnIndex("total_visitas"));
                    estadisticaRecorrido.total_pedidos = cursor.getInt(cursor.getColumnIndex("total_pedidos"));
                    estadisticaRecorrido.total_pedidos_sync = cursor.getInt(cursor.getColumnIndex("total_pedidos_sync"));
                    estadisticaRecorrido.total_pedidos_sin_sync = cursor.getInt(cursor.getColumnIndex("total_pedidos_sin_sync"));
                    estadisticaRecorrido.total_venta = cursor.getInt(cursor.getColumnIndex("total_venta"));

                    if (estadisticaRecorrido.total_visitas > 0)
                        estadisticaRecorrido.efectividad = (estadisticaRecorrido.total_pedidos * 100) / estadisticaRecorrido.total_visitas;
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarEstadisticas -> No Existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarEstadisticas -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return estadisticaRecorrido;
    }


    public static CuadreCaja CargarCuadreCaja(String fechaConsecutivo) {

        mensaje = "";
        SQLiteDatabase db = null;
        CuadreCaja cuadreCaja = new CuadreCaja();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query =

                        "SELECT SUM(montoFact) AS total_venta, " + "(SELECT SUM(monto) FROM FormaPago WHERE forma_pago = 1) AS total_efectivo, "
                                + "(SELECT SUM(monto) FROM FormaPago WHERE forma_pago = 2) AS total_cheque " + "FROM EncabezadoAndroid WHERE tipoTrans = 0 ";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    cuadreCaja.total_efectivo = cursor.getInt(cursor.getColumnIndex("total_efectivo"));
                    cuadreCaja.total_cheque = cursor.getInt(cursor.getColumnIndex("total_cheque"));
                    cuadreCaja.total_venta = cursor.getInt(cursor.getColumnIndex("total_venta"));
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarCuadreCaja -> No Existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarCuadreCaja -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cuadreCaja;
    }

    public static Vector<Encabezado> CargarVisitasRealizadas(String fechaConsecutivo, Vector<ItemListView> listaItemsCliente, int sync) {

        mensaje = "";
        SQLiteDatabase db = null;
        ItemListView itemListView;

        Encabezado encabezado;
        Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        //		try {

        File dbFile = new File(Util.DirApp(), "DataBase.db");

        if (dbFile.exists()) {

            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT Encabezado.codigo AS codigo_cliente, Clientes.Nombre AS nombre_cliente, numeroDoc,  montoFact, tipoTrans, desc1, iva,"
                    + "syncmobile, anulado, fp2 " +
                    "FROM Encabezado " +
                    "INNER JOIN Clientes ON Encabezado.codigo = Clientes.Codigo " +
                    //					       "WHERE tipoTrans = '0' AND FechaFinal IS NOT NULL AND syncmobile = " + sync;
                    "WHERE FechaFinal IS NOT NULL " +
                    "ORDER BY tipoTrans";

            System.out.println("Consulta visita ralizada: " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    encabezado = new Encabezado();
                    encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                    encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                    encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                    encabezado.sincronizado = cursor.getInt(cursor.getColumnIndex("syncmobile"));
                    encabezado.anulado = cursor.getInt(cursor.getColumnIndex("anulado"));
                    encabezado.tipoTrans = cursor.getInt(cursor.getColumnIndex("tipoTrans"));
                    encabezado.valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));
                    encabezado.amarre = cursor.getString(cursor.getColumnIndex("fp2"));

                    float valor_neto = encabezado.valor_neto;

                    itemListView = new ItemListView();

                    String tipoTrans = "";

                    switch (encabezado.tipoTrans) {
                        case Const.OP_PEDIDO:

                            tipoTrans = "VENTA";

                            break;

                        case Const.OP_DEVOLUCION:

                            tipoTrans = "DEVOLUCION";

                            break;

                        case Const.OP_CAMBIO:

                            tipoTrans = "CAMBIO";

                            break;
                    }

                    itemListView.titulo = tipoTrans + "\n" + encabezado.nombre_cliente;

                    itemListView.icono = (encabezado.anulado == 1) ? R.drawable.cliente_anulado : R.drawable.cliente;
                    itemListView.subTitulo = "Valor " + tipoTrans.toLowerCase(Locale.getDefault()) + ": " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 2));

                    listaItemsCliente.addElement(itemListView);
                    listaPedidos.addElement(encabezado);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } else {

            Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
        }

        //		} catch (Exception e) {
        //
        //			mensaje = e.getMessage();
        //			Log.e(TAG, "CargarVisitasRealizadas: " + mensaje, e);
        //
        //		} finally {
        //
        //			if (db != null)
        //				db.close();
        //		}

        return listaPedidos;
    }

    public static Vector<Encabezado> CargarListaAnuladas(String fechaConsecutivo,
                                                         Vector<ItemListView> listaItemsCliente, int tipoPedido) {

        mensaje = "";
        SQLiteDatabase db = null;
        ItemListView itemListView;

        Encabezado encabezado;
        Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT Encabezado.codigo AS codigo_cliente, Clientes.Nombre AS nombre_cliente, numeroDoc,  montoFact, tipoTrans, desc1, iva,"
                        + "syncmobile, anulado "
                        + "FROM Encabezado "
                        + "INNER JOIN Clientes ON Encabezado.codigo = Clientes.Codigo "
                        + "WHERE anulado = '1' AND tipoTrans = '" + tipoPedido + "' ";

                System.out.println("Consulta lista Anulados: " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        encabezado = new Encabezado();
                        encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                        encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                        encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                        encabezado.sincronizado = cursor.getInt(cursor.getColumnIndex("syncmobile"));
                        encabezado.anulado = cursor.getInt(cursor.getColumnIndex("anulado"));
                        encabezado.tipoTrans = cursor.getInt(cursor.getColumnIndex("tipoTrans"));

                        float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));

                        itemListView = new ItemListView();

                        itemListView.titulo = encabezado.nombre_cliente;

                        itemListView.icono = (encabezado.anulado == 1) ? R.drawable.cliente_anulado : R.drawable.cliente;
                        itemListView.subTitulo = "Valor Venta: " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 2));

                        listaItemsCliente.addElement(itemListView);
                        listaPedidos.addElement(encabezado);

                    } while (cursor.moveToNext());

                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarVisitasRealizadas: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaPedidos;
    }


    public static Vector<Encabezado> CargarVisitasRealizadas_(String fechaConsecutivo, Vector<ItemListView> listaItemsCliente) {

        mensaje = "";
        SQLiteDatabase db = null;
        ItemListView itemListView;

        Encabezado encabezado;
        Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT NovedadesComprasAndroid.codigoCliente AS codigo_cliente, Clientes.nombre AS nombre_cliente, " +
                        "nroDoc, 0 as montoFact, 0 as desc1, 0 as iva, motivo as tipoTrans " +
                        "FROM NovedadesComprasAndroid " +
                        "INNER JOIN Clientes ON NovedadesComprasAndroid.codigoCliente = Clientes.Codigo " +
                        "WHERE motivo <> 1 " +
                        "union " +
                        "SELECT NovedadesComprasAndroid.codigoCliente AS codigo_cliente, Clientes.nombre AS nombre_cliente, " +
                        "nroDoc, EncabezadoAndroid.montoFact, EncabezadoAndroid.desc1, EncabezadoAndroid.iva, EncabezadoAndroid.tipoTrans " +
                        "FROM NovedadesComprasAndroid " +
                        "INNER JOIN EncabezadoAndroid ON NovedadesComprasAndroid.nrodoc = EncabezadoAndroid.numerodoc " +
                        "INNER JOIN Clientes ON NovedadesComprasAndroid.codigoCliente = Clientes.Codigo " +
                        "WHERE motivo = 1";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        encabezado = new Encabezado();
                        encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                        encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                        encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("nroDoc"));

                        int tipoTrans = cursor.getInt(cursor.getColumnIndex("tipoTrans"));

                        //encabezado.lista_precio   = cursor.getString(cursor.getColumnIndex("lista_precio"));
                        //encabezado.sync           = cursor.getInt(cursor.getColumnIndex("sync"));
                        //encabezado.sn_envio       = cursor.getInt(cursor.getColumnIndex("sn_envio"));

                        float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));

                        itemListView = new ItemListView();
                        itemListView.titulo = encabezado.nombre_cliente;
                        itemListView.subTitulo = "Valor Venta: " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 0));

                        if (tipoTrans == 0)
                            itemListView.subTitulo = itemListView.subTitulo + "\nPEDIDO";
                        else
                            itemListView.subTitulo = itemListView.subTitulo + "\nNO COMPRA";

                        listaItemsCliente.addElement(itemListView);
                        listaPedidos.addElement(encabezado);

                    } while (cursor.moveToNext());

                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarVisitasRealizadas: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaPedidos;
    }


    public static Vector<Encabezado> CargarVisitasRealizadasSinEnviar(String fechaConsecutivo, Vector<ItemListView> listaItemsCliente, int sync) {

        mensaje = "";
        SQLiteDatabase db = null;
        ItemListView itemListView;

        Encabezado encabezado;
        Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT NovedadesCompras.codigoCliente AS codigo_cliente, Clientes.nombre AS nombre_cliente, " +
                        "nroDoc, 0 as montoFact, 0 as desc1, 0 as iva, motivo as tipoTrans " +
                        "FROM NovedadesCompras " +
                        "INNER JOIN Clientes ON NovedadesCompras.codigoCliente = Clientes.Codigo " +
                        "WHERE motivo <> 1 and sincronizado = 0 " +
                        "union " +
                        "SELECT NovedadesCompras.codigoCliente AS codigo_cliente, Clientes.nombre AS nombre_cliente, " +
                        "nroDoc, Encabezado.montoFact, Encabezado.desc1, Encabezado.iva, Encabezado.tipoTrans " +
                        "FROM NovedadesCompras " +
                        "INNER JOIN Encabezado ON NovedadesCompras.nrodoc = Encabezado.numerodoc " +
                        "INNER JOIN Clientes ON NovedadesCompras.codigoCliente = Clientes.Codigo " +
                        "WHERE motivo = 1 and NovedadesCompras.sincronizado = 0";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        encabezado = new Encabezado();
                        encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                        encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                        encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("nroDoc"));

                        //encabezado.lista_precio   = cursor.getString(cursor.getColumnIndex("lista_precio"));
                        //encabezado.sync           = cursor.getInt(cursor.getColumnIndex("sync"));
                        //encabezado.sn_envio       = cursor.getInt(cursor.getColumnIndex("sn_envio"));

                        float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));

                        itemListView = new ItemListView();
                        itemListView.titulo = encabezado.nombre_cliente;
                        itemListView.subTitulo = "Valor Venta: " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 0));

                        listaItemsCliente.addElement(itemListView);
                        listaPedidos.addElement(encabezado);

                    } while (cursor.moveToNext());

                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarPedidosRealizados: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaPedidos;
    }


    public static Vector<Encabezado> CargarPedidosRealizados(String fechaConsecutivo, Vector<ItemListView> listaItemsCliente, int sync) {

        if (sync == 0 || sync == 1) {

            mensaje = "";
            SQLiteDatabase db = null;
            ItemListView itemListView;

            Encabezado encabezado;
            Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

            if (listaItemsCliente == null)
                listaItemsCliente = new Vector<ItemListView>();

            try {

                File dbFile = new File(Util.DirApp(), "DataBase.db");

                if (dbFile.exists()) {

                    db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    String query = "SELECT Encabezado.codigo AS codigo_cliente, Clientes.Nombre AS nombre_cliente, numeroDoc, montoFact, desc1, iva, tipoTrans, anulado, syncmobile, fp2 " +
                            "FROM Encabezado " +
                            "INNER JOIN Clientes ON Encabezado.codigo = Clientes.Codigo " +
                            "WHERE (tipoTrans = '0' OR tipoTrans = '2') AND fechaFinal IS NOT NULL AND syncmobile = " + sync + " ORDER BY tipoTrans";
                    System.out.println("Consulta visita no enviada: " + query);

                    Cursor cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            encabezado = new Encabezado();
                            encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                            encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                            encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                            encabezado.tipoTrans = cursor.getInt(cursor.getColumnIndex("tipoTrans"));
                            encabezado.anulado = cursor.getInt(cursor.getColumnIndex("anulado"));
                            encabezado.sincronizado = cursor.getInt(cursor.getColumnIndex("syncmobile"));
                            String tipo = (encabezado.tipoTrans == 0) ? "VENTA" : "DEVOLUCION";
                            float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));
                            encabezado.amarre = cursor.getString(cursor.getColumnIndex("fp2"));

                            itemListView = new ItemListView();
                            itemListView.titulo = tipo + "\n" + encabezado.nombre_cliente;
                            itemListView.subTitulo = "Valor " + tipo.toLowerCase(Locale.getDefault()) + ": " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 2));
                            if (encabezado.anulado == 1) {
                                itemListView.icono = R.drawable.cliente_anulado;
                            }
                            listaItemsCliente.addElement(itemListView);
                            listaPedidos.addElement(encabezado);

                        } while (cursor.moveToNext());

                    }

                    if (cursor != null)
                        cursor.close();

                } else {

                    Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
                }

            } catch (Exception e) {

                mensaje = e.getMessage();
                Log.e(TAG, "CargarPedidosRealizados: " + mensaje, e);

            } finally {

                if (db != null)
                    db.close();
            }

            return listaPedidos;

        } else {


            mensaje = "";
            SQLiteDatabase db = null;
            ItemListView itemListView;

            Encabezado encabezado;
            Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

            if (listaItemsCliente == null)
                listaItemsCliente = new Vector<ItemListView>();

            try {

                File dbFile = new File(Util.DirApp(), "DataBase.db");

                if (dbFile.exists()) {

                    db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    String query = "SELECT Encabezado.codigo AS codigo_cliente, ClientesNuevos.nombre AS nombre_cliente, numeroDoc, montoFact, desc1, iva " +
                            "FROM Encabezado " +
                            "INNER JOIN ClientesNuevos ON Encabezado.codigo = ClientesNuevos.nit " +
                            "WHERE tipoTrans = '0' AND fechaFinal IS NOT NULL AND sync = '1' or sync = '0' ";

                    Cursor cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            encabezado = new Encabezado();
                            encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                            encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                            encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));

                            //encabezado.lista_precio   = cursor.getString(cursor.getColumnIndex("lista_precio"));
                            //encabezado.sync           = cursor.getInt(cursor.getColumnIndex("sync"));
                            //encabezado.sn_envio       = cursor.getInt(cursor.getColumnIndex("sn_envio"));

                            float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));

                            itemListView = new ItemListView();
                            itemListView.titulo = encabezado.nombre_cliente;
                            itemListView.subTitulo = "Valor Venta: " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 0));

                            listaItemsCliente.addElement(itemListView);
                            listaPedidos.addElement(encabezado);

                        } while (cursor.moveToNext());

                    }

                    if (cursor != null)
                        cursor.close();

                } else {

                    Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
                }

            } catch (Exception e) {

                mensaje = e.getMessage();
                Log.e(TAG, "CargarPedidosRealizados: " + mensaje, e);

            } finally {

                if (db != null)
                    db.close();
            }

            return listaPedidos;


        }

    }


    public static Vector<Encabezado> CargarPedidosRealizadosCN(String fechaConsecutivo, Vector<ItemListView> listaItemsCliente, int sync) {

        if (sync == 0 || sync == 1) {

            mensaje = "";
            SQLiteDatabase db = null;
            ItemListView itemListView;

            Encabezado encabezado;
            Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

            if (listaItemsCliente == null)
                listaItemsCliente = new Vector<ItemListView>();

            try {

                File dbFile = new File(Util.DirApp(), "DataBase.db");

                if (dbFile.exists()) {

                    db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    String query = "SELECT Encabezado.codigo AS codigo_cliente, ClientesNuevos.Nombre AS nombre_cliente, numeroDoc, montoFact, desc1, iva,"
                            + "syncmobile, fp2 " +
                            "FROM Encabezado " +
                            "INNER JOIN ClientesNuevos ON Encabezado.codigo = ClientesNuevos.Codigo " +
                            "WHERE tipoTrans = '0' AND fechaFinal IS NOT NULL"; //AND syncmobile = " + sync;

                    Cursor cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            encabezado = new Encabezado();
                            encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                            encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                            encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                            encabezado.sincronizado = cursor.getInt(cursor.getColumnIndex("syncmobile"));
                            //encabezado.lista_precio   = cursor.getString(cursor.getColumnIndex("lista_precio"));
                            //encabezado.sync           = cursor.getInt(cursor.getColumnIndex("sync"));
                            //encabezado.sn_envio       = cursor.getInt(cursor.getColumnIndex("sn_envio"));
                            encabezado.amarre = cursor.getString(cursor.getColumnIndex("fp2"));

                            float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));

                            itemListView = new ItemListView();
                            itemListView.titulo = encabezado.nombre_cliente;
                            itemListView.subTitulo = "Valor Venta: " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 2));

                            listaItemsCliente.addElement(itemListView);
                            listaPedidos.addElement(encabezado);

                        } while (cursor.moveToNext());

                    }

                    if (cursor != null)
                        cursor.close();

                } else {

                    Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
                }

            } catch (Exception e) {

                mensaje = e.getMessage();
                Log.e(TAG, "CargarPedidosRealizados: " + mensaje, e);

            } finally {

                if (db != null)
                    db.close();
            }

            return listaPedidos;

        } else {


            mensaje = "";
            SQLiteDatabase db = null;
            ItemListView itemListView;

            Encabezado encabezado;
            Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

            if (listaItemsCliente == null)
                listaItemsCliente = new Vector<ItemListView>();

            try {

                File dbFile = new File(Util.DirApp(), "DataBase.db");

                if (dbFile.exists()) {

                    db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    String query = "SELECT Encabezado.codigo AS codigo_cliente, ClientesNuevos.nombre AS nombre_cliente, numeroDoc, montoFact, desc1, iva " +
                            "FROM Encabezado " +
                            "INNER JOIN ClientesNuevos ON Encabezado.codigo = ClientesNuevos.nit " +
                            "WHERE tipoTrans = '0' AND fechaFinal IS NOT NULL AND sync = '1' or sync = '0' ";

                    Cursor cursor = db.rawQuery(query, null);

                    if (cursor.moveToFirst()) {

                        do {

                            encabezado = new Encabezado();
                            encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                            encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                            encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));

                            //encabezado.lista_precio   = cursor.getString(cursor.getColumnIndex("lista_precio"));
                            //encabezado.sync           = cursor.getInt(cursor.getColumnIndex("sync"));
                            //encabezado.sn_envio       = cursor.getInt(cursor.getColumnIndex("sn_envio"));

                            float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));

                            itemListView = new ItemListView();
                            itemListView.titulo = encabezado.nombre_cliente;
                            itemListView.subTitulo = "Valor Venta: " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 0));

                            listaItemsCliente.addElement(itemListView);
                            listaPedidos.addElement(encabezado);

                        } while (cursor.moveToNext());

                    }

                    if (cursor != null)
                        cursor.close();

                } else {

                    Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
                }

            } catch (Exception e) {

                mensaje = e.getMessage();
                Log.e(TAG, "CargarPedidosRealizados: " + mensaje, e);

            } finally {

                if (db != null)
                    db.close();
            }

            return listaPedidos;


        }

    }


    public static Vector<Encabezado> CargarVisitasRealizadasSinEnviar_(String fechaConsecutivo, Vector<ItemListView> listaItemsCliente, int sync) {

        mensaje = "";
        SQLiteDatabase db = null;
        ItemListView itemListView;

        Encabezado encabezado;
        Vector<Encabezado> listaPedidos = new Vector<Encabezado>();

        if (listaItemsCliente == null)
            listaItemsCliente = new Vector<ItemListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT NovedadesComprasAndroid.codigoCliente AS codigo_cliente, Clientes.nombre AS nombre_cliente, " +
                        "nroDoc, 0 as montoFact, 0 as desc1, 0 as iva, motivo as tipoTrans " +
                        "FROM NovedadesComprasAndroid " +
                        "INNER JOIN Clientes ON NovedadesComprasAndroid.codigoCliente = Clientes.Codigo " +
                        "WHERE motivo <> 1 and sincronizado = 0" +
                        "union " +
                        "SELECT NovedadesComprasAndroid.codigoCliente AS codigo_cliente, Clientes.nombre AS nombre_cliente, " +
                        "nroDoc, EncabezadoAndroid.montoFact, EncabezadoAndroid.desc1, EncabezadoAndroid.iva, EncabezadoAndroid.tipoTrans " +
                        "FROM NovedadesComprasAndroid " +
                        "INNER JOIN EncabezadoAndroid ON NovedadesComprasAndroid.nrodoc = EncabezadoAndroid.numerodoc " +
                        "INNER JOIN Clientes ON NovedadesComprasAndroid.codigoCliente = Clientes.Codigo " +
                        "WHERE motivo = 1 and NovedadesComprasAndroid.sincronizado = 0";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        encabezado = new Encabezado();
                        encabezado.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo_cliente"));
                        encabezado.nombre_cliente = cursor.getString(cursor.getColumnIndex("nombre_cliente"));
                        encabezado.numero_doc = cursor.getString(cursor.getColumnIndex("nroDoc"));

                        //encabezado.lista_precio   = cursor.getString(cursor.getColumnIndex("lista_precio"));
                        //encabezado.sync           = cursor.getInt(cursor.getColumnIndex("sync"));
                        //encabezado.sn_envio       = cursor.getInt(cursor.getColumnIndex("sn_envio"));

                        float valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));

                        itemListView = new ItemListView();
                        itemListView.titulo = encabezado.nombre_cliente;
                        itemListView.subTitulo = "Valor Venta: " + Util.SepararMiles(Util.Redondear(Util.QuitarE("" + valor_neto), 0));

                        listaItemsCliente.addElement(itemListView);
                        listaPedidos.addElement(encabezado);

                    } while (cursor.moveToNext());

                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarPedidosRealizados: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarPedidosRealizados: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaPedidos;
    }






    /*public static Hashtable<String, Detalle> CargarDetallePedido(Encabezado encabezado) {

		mensaje = "";
		SQLiteDatabase db = null;

		Detalle detalle;
		Hashtable<String, Detalle> listaDetalle = new Hashtable<String, Detalle>();

		try {

			File dbFile = new File(Util.DirApp(), "Temp.db");
			db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

			String query = "SELECT codigoCliente, codigoRef, cantidad, precio, iva, descuento, tipoPedido, " +
						   "inicioPedido, finPedido, observacion, tipo_cliente, no_compra, " +
						   "ObserCartera " +
						   "FROM TmpPedido " +
						   "WHERE numero_doc = '" + encabezado.numero_doc + "'";

			Log.i("CargarDetallePedido", "query = " + query);

			Cursor cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {

				do {

					detalle = new Detalle();
					detalle.codigo_cliente       = cursor.getString(cursor.getColumnIndex("codigoCliente"));
					detalle.codigo_producto      = cursor.getString(cursor.getColumnIndex("codigoRef"));
					detalle.desc_producto        = NombreProducto(detalle.codigo_producto);
					detalle.precio               = cursor.getFloat(cursor.getColumnIndex("precio"));
					detalle.iva                  = cursor.getFloat(cursor.getColumnIndex("iva"));
					detalle.descuento_autorizado = cursor.getInt(cursor.getColumnIndex("descuento"));
					detalle.cantidad             = cursor.getInt(cursor.getColumnIndex("cantidad"));
					detalle.tipo_pedido          = cursor.getInt(cursor.getColumnIndex("tipoPedido"));

					encabezado.hora_inicial      = cursor.getString(cursor.getColumnIndex("inicioPedido"));
					encabezado.hora_final        = cursor.getString(cursor.getColumnIndex("finPedido"));
					encabezado.observacion       = cursor.getString(cursor.getColumnIndex("observacion"));
					encabezado.tipo_cliente      = cursor.getString(cursor.getColumnIndex("tipo_cliente"));
					encabezado.motivo            = cursor.getInt(cursor.getColumnIndex("motivo"));

					//String ObserCartera          = cursor.getString(cursor.getColumnIndex("ObserCartera"));
					//encabezado.carteraVencida    = ObserCartera.contains("OJO FACTURAS VENCIDAS");
					//encabezado.excedeCupo        = ObserCartera.contains("EXCEDE EL CUPO");

					listaDetalle.put(detalle.codigo_producto, detalle);

				} while(cursor.moveToNext());

				mensaje = "Cargo Pedidos Realizados Correctamente";

			} else {

				mensaje = "Consulta sin resultados";
			}

			if (cursor != null)
				cursor.close();

		} catch (Exception e) {

			mensaje = e.getMessage();

		} finally {

			if (db != null)
				db.close();
		}

		return listaDetalle;
	}*/

    public static Hashtable<String, Detalle> CargarDetallePedido(String numDoc) {

        mensaje = "";
        SQLiteDatabase db = null;

        Detalle detalle;
        Hashtable<String, Detalle> listaDetalle = new Hashtable<String, Detalle>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigoRef AS codigo_producto, precio, tarifaIva AS iva, cantidad, descuentoRenglon as descuento, motivo, item "
                    + "FROM Detalle WHERE numDoc = '" + numDoc + "' "
                    + "group by codigo_producto, precio "
                    + "order by item asc ";

            Log.i("CargarDetallePedido", "query = " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalle = new Detalle();
                    detalle.codProducto = cursor.getString(cursor.getColumnIndex("codigo_producto"));
                    detalle.nombProducto = NombreProducto(detalle.codProducto);
                    detalle.desc_producto = detalle.nombProducto;
                    detalle.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    detalle.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    detalle.descuento = cursor.getInt(cursor.getColumnIndex("descuento"));
                    detalle.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    detalle.motivo = cursor.getString(cursor.getColumnIndex("motivo"));
                    detalle.posicion = cursor.getInt(cursor.getColumnIndex("item"));

                    listaDetalle.put(detalle.codProducto, detalle);

                } while (cursor.moveToNext());

                mensaje = "Cargo Pedidos Realizados Correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalle;
    }

    public static Vector<Detalle> CargarDetallePedidoVector(String numDoc) {

        mensaje = "";
        SQLiteDatabase db = null;
        Detalle detalle;

        Vector<Detalle> listaDetalle = new Vector<Detalle>();

        //		Detalle detalle;
        //		Hashtable<String, Detalle> listaDetalle = new Hashtable<String, Detalle>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigoRef AS codigo_producto, precio, tarifaIva AS iva, cantidad, descuentoRenglon as descuento, motivo, item " +
                    "FROM Detalle  " +
                    "WHERE numDoc = '" + numDoc + "' "
                    + "group by codigo_producto, precio "
                    + "order by item asc ";

            Log.i("CargarDetallePedido", "query = " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalle = new Detalle();
                    detalle.codProducto = cursor.getString(cursor.getColumnIndex("codigo_producto"));
                    detalle.nombProducto = NombreProducto(detalle.codProducto);
                    detalle.desc_producto = detalle.nombProducto;
                    detalle.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    detalle.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    detalle.descuento = cursor.getInt(cursor.getColumnIndex("descuento"));
                    detalle.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    detalle.motivo = cursor.getString(cursor.getColumnIndex("motivo"));
                    detalle.posicion = cursor.getInt(cursor.getColumnIndex("item"));

                    listaDetalle.add(detalle);

                } while (cursor.moveToNext());

                mensaje = "Cargo Pedidos Realizados Correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalle;
    }


    public static Hashtable<String, Detalle> CargarDetallePedido_(String numDoc) {

        mensaje = "";
        SQLiteDatabase db = null;

        Detalle detalle;
        Hashtable<String, Detalle> listaDetalle = new Hashtable<String, Detalle>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigoRef AS codigo_producto, precio, tarifaIva AS iva, cantidad, descuentoRenglon as descuento " +
                    "FROM DetalleAndroid  " +
                    "WHERE numDoc = '" + numDoc + "'";

            Log.i("CargarDetallePedido", "query = " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalle = new Detalle();
                    detalle.codProducto = cursor.getString(cursor.getColumnIndex("codigo_producto"));
                    detalle.nombProducto = NombreProducto(detalle.codProducto);
                    detalle.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    detalle.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    detalle.descuento = cursor.getInt(cursor.getColumnIndex("descuento"));
                    detalle.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));

                    listaDetalle.put(detalle.codProducto, detalle);

                } while (cursor.moveToNext());

                mensaje = "Cargo Pedidos Realizados Correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalle;
    }




    /*public static void Vacuum() {

		SQLiteDatabase dbPedido = null;

		try {

			File filePedido = new File(Util.DirApp(), "Temp.dp");
			dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
			dbPedido.execSQL("VACUUM");

		} catch (Exception e) {

			mensaje = e.getMessage();

		} finally {

			if (dbPedido != null)
				dbPedido.close();
		}
	}*/

    public static boolean CargarInfomacionUsuario() {

        mensaje = "";
        boolean cargo = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = ""
                        + "SELECT bodega, codigo AS codigo, nombre AS nombre, "
                        + "	fechaLabores, fechaConsecutivo, pedidoMinimo, tipoventa AS tipoVenta,viajera,imprimedetallerecaudo "
                        + "FROM Vendedor;";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    Main.usuario = null;
                    System.gc();

                    Main.usuario = new Usuario();
                    Main.usuario.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    Main.usuario.codigoVendedor = cursor.getString(cursor.getColumnIndex("codigo"));
                    Main.usuario.nombreVendedor = cursor.getString(cursor.getColumnIndex("nombre"));
                    Main.usuario.fechaLabores = cursor.getString(cursor.getColumnIndex("fechaLabores"));
                    Main.usuario.fechaConsecutivo = cursor.getString(cursor.getColumnIndex("fechaConsecutivo"));
                    Main.usuario.pedidoMinimo = cursor.getLong(cursor.getColumnIndex("pedidoMinimo"));
                    Main.usuario.tipoVenta = cursor.getString(cursor.getColumnIndex("tipoVenta"));
                    Main.usuario.viajero = cursor.getString(cursor.getColumnIndex("viajera"));
                    Main.usuario.impresionRecaudo = cursor.getString(cursor.getColumnIndex("imprimedetallerecaudo"));

                    cargo = true;
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarInfomacionUsuario: No Existe la Base de Datos DataBase.db o No tiene Acceso a la SD");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarInfomacionUsuario: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cargo;
    }

    public static Usuario ObtenerUsuario() {

        mensaje = "";
        Usuario usuario = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT codigo AS codigo, nombre AS nombre, fechaLabores, "
                        + "fechaConsecutivo, pedidoMinimo, bodega, tiporutero, tipoventa AS tipoVenta, viajera, imprimedetallerecaudo FROM Vendedor";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    usuario = new Usuario();
                    usuario.codigoVendedor = cursor.getString(cursor.getColumnIndex("codigo"));
                    //usuario.bodega           = usuario.codigoVendedor;
                    usuario.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    usuario.nombreVendedor = cursor.getString(cursor.getColumnIndex("nombre"));
                    usuario.fechaLabores = cursor.getString(cursor.getColumnIndex("fechaLabores"));
                    usuario.fechaConsecutivo = cursor.getString(cursor.getColumnIndex("fechaConsecutivo"));
                    usuario.pedidoMinimo = cursor.getLong(cursor.getColumnIndex("pedidoMinimo"));
                    usuario.tipoRutero = cursor.getInt(cursor.getColumnIndex("tiporutero"));
                    usuario.tipoVenta = cursor.getString(cursor.getColumnIndex("tipoVenta"));
                    usuario.viajero = cursor.getString(cursor.getColumnIndex("viajera"));
                    usuario.impresionRecaudo = cursor.getString(cursor.getColumnIndex("imprimedetallerecaudo"));
                    mensaje = "Cargo Informacion del Usuario Correctamente";
                }

                if (cursor != null)
                    cursor.close();

            } else {
                Log.e(TAG, "ObtenerUsuario -> " + Msg.NO_EXISTE_BD);
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "ObtenerUsuario -> " + mensaje, e);
        } finally {

            if (db != null)
                db.close();
        }
        return usuario;
    }

    public static String ObtenerVersionApp() {

        String version = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT version FROM Version";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                version = cursor.getString(cursor.getColumnIndex("version"));
            }

            Log.i("ObtenerVersionApp", "version = " + version);

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ObtenerVersionApp: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return version;
    }

    public static String ListaPrecioClienteOcasional() {

        String listaPrecio = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT CodigoAmarre FROM Clientes WHERE Codigo LIKE '%9999%'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                listaPrecio = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
            }

            Log.i(TAG, "listaPrecio = " + listaPrecio);

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ListaPrecioClienteOcasional -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaPrecio;
    }

    public static int ObtenerConsecutivoVend() {

        int consecutivo = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT consecutivo FROM Vendedor", null);

            if (cursor.moveToFirst())
                consecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return consecutivo;
    }

    public static boolean GuardarImagen(Foto foto, byte[] image) {

        SQLiteDatabase db = null;
        mensaje = "";

        try {

            File dbFile = new File(Util.DirApp(), "Temp.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values.put("Id", foto.id);
            values.put("Imagen", image);
            values.put("CodCliente", foto.codCliente);
            values.put("CodVendedor", foto.codVendedor);
            values.put("Modulo", foto.modulo);
            values.put("nroDoc", foto.nroDoc);

            db.insertOrThrow("Fotos", null, values);
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static boolean BorrarImagen(String idImagen) {

        mensaje = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "Temp.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("DELETE FROM Fotos WHERE id = '" + idImagen + "'");
            db.execSQL("VACUUM");

            mensaje = "Imagen borrada con exito";
            return true;

        } catch (Exception e) {

            mensaje = "Error cargando Imagen: " + e.getMessage();
            Log.e("DataBaseBO - BorrarImagen", mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static byte[] CargarImagen(String idImagen) {

        mensaje = "";
        byte[] image = null;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "Temp.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("Fotos",
                    new String[]{"Imagen"},
                    "id = '" + idImagen + "'",
                    null,
                    null,
                    null,
                    null);

            if (cursor.moveToFirst()) {

                image = cursor.getBlob(cursor.getColumnIndex("Imagen"));
                mensaje = "Imagen cargada correctamente";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = "Error cargando Imagen: " + e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return image;
    }

    public static byte[] CargarImagen(String nroDoc, String codCliente, String codVendedor) {

        mensaje = "";
        byte[] image = null;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "Temp.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("Fotos",
                    new String[]{"Imagen"},
                    "nroDoc = '" + nroDoc + "' AND CodCliente = '" + codCliente + "' AND CodVendedor = '" + codVendedor + "'",
                    null,
                    null,
                    null,
                    null);

            if (cursor.moveToFirst()) {

                image = cursor.getBlob(cursor.getColumnIndex("Imagen"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarImagen", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return image;
    }

    public static boolean FotosXCliente(String nroDoc, String codCliente, String codVendedor) {

        SQLiteDatabase db = null;

        try {

            Foto foto;
            boolean cargo;

            File dbFile = new File(Util.DirApp(), "Temp.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query =

                    "SELECT Id, Imagen " +
                            "FROM Fotos " +
                            "WHERE codCliente = '" + codCliente + "' AND codVendedor = '" + codVendedor + "' AND nroDoc = '" + nroDoc + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                Main.fotosGaleria.removeAllElements();
                Main.listaInfoFotos.removeAllElements();

                do {

                    byte[] image = cursor.getBlob(cursor.getColumnIndex("Imagen"));
                    Drawable img = Util.ResizedImage(image, 30, 30);

                    if (img != null) {

                        foto = new Foto();
                        foto.id = cursor.getString(cursor.getColumnIndex("Id"));
                        foto.codCliente = codCliente;
                        foto.codVendedor = codVendedor;
                        foto.modulo = 1;
                        foto.nroDoc = nroDoc;

                        Main.fotosGaleria.addElement(img);
                        Main.listaInfoFotos.addElement(foto);

                    } else {

                        mensaje = "Error cargardo Fotos";
                        cargo = false;
                    }

                } while (cursor.moveToNext());

                mensaje = "Fotos Cargadas Correctamente";
                cargo = true;

            } else {

                mensaje = "Consulta sin Resultados";
                cargo = true;
            }

            if (cursor != null)
                cursor.close();

            return cargo;

        } catch (Exception e) {

            mensaje = "Error cargardo Fotos: " + e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static void ValidarUsuario() {

        if (Main.usuario == null)
            Main.usuario = new Usuario();

        if (Main.usuario.codigoVendedor == null || Main.usuario.nombreVendedor == null || Main.usuario.fechaConsecutivo == null) {

            Usuario usuario = ObtenerUsuario();

            if (usuario != null) {

                Main.usuario.codigoVendedor = usuario.codigoVendedor;
                Main.usuario.nombreVendedor = usuario.nombreVendedor;
                Main.usuario.fechaLabores = usuario.fechaLabores;
                Main.usuario.fechaConsecutivo = usuario.fechaConsecutivo;
                Main.usuario.pedidoMinimo = usuario.pedidoMinimo;
                Main.usuario.tipoVenta = usuario.tipoVenta;
                Main.usuario.viajero = usuario.viajero;
            }
        }
    }

    public static int[] ObtenerLimitesVentas(String codProducto) {

        int[] datos = {0, 99999};
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select ventaMinima, ventaMaxima from limiteVentas where listaprecio = '" + Main.cliente.CodigoAmarre + "' and codProducto = '" + codProducto + "'", null);

            if (cursor.moveToFirst()) {

                datos[0] = cursor.getInt(cursor.getColumnIndex("ventaMinima"));
                datos[1] = cursor.getInt(cursor.getColumnIndex("ventaMaxima"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return datos;
    }

    public static int ObtenerMultiploVentas(String codProducto) {

        int multiplo = 1;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select multiplo from MultiplosVentas where listaprecio = '" + Main.cliente.CodigoAmarre + "' and codProducto = '" + codProducto + "'", null);

            if (cursor.moveToFirst()) {

                multiplo = cursor.getInt(cursor.getColumnIndex("multiplo"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return multiplo;
    }

    public static int ObtenerPrecio(String codProducto, String listaPrecio) {

        int precio = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select precio from listaPrecios where codigo = '" + codProducto + "' and listaprecios = '" + listaPrecio + "'", null);

            if (cursor.moveToFirst()) {

                precio = cursor.getInt(cursor.getColumnIndex("precio"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return precio;
    }

    public static boolean productoRestringidoVenta(String codProducto) {

        int cont = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select count(1) as cont from", null);

            if (cursor.moveToFirst()) {

                cont = cursor.getInt(cursor.getColumnIndex("cont"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        if (cont > 0)
            return true;
        else
            return false;

    }

    public static boolean clienteHabilitadoProductoST(String codProducto) {

        int cont = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select count(1) as cont from ClienteProdBloq where referencia = '" + codProducto + "' and cliente = '" + Main.cliente.codigo + "'", null);

            if (cursor.moveToFirst()) {

                cont = cursor.getInt(cursor.getColumnIndex("cont"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        if (cont > 0)
            return true;
        else
            return false;

    }

    public static Vector getTramasEncabezado() {

        int cont = 0;
        SQLiteDatabase db = null;
        String sql = "", trama = "";
        Vector tramas = new Vector();

        String codCliente, motivo, hora, motivoExtraruta, tiempoCliente, encuestaEntregada;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT CODIGOCLIENTE, TIPOTRANS, strftime(\"%H@%M\", INICIOPEDIDO) AS HORA, " +
                    "'1' AS MOTIVOEXTRARUTA, 'V' AS TIEMPOCLIENTE, '1' AS ENCUESTAENTREGADA " +
                    "FROM ENCABEZADO " +
                    "WHERE SINCRONIZADO = 0 AND TIPOTRANS <> 0";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    codCliente = cursor.getString(cursor.getColumnIndex("codigoCliente"));
                    motivo = cursor.getString(cursor.getColumnIndex("tipoTrans"));
                    hora = cursor.getString(cursor.getColumnIndex("HORA"));
                    motivoExtraruta = cursor.getString(cursor.getColumnIndex("MOTIVOEXTRARUTA"));
                    tiempoCliente = cursor.getString(cursor.getColumnIndex("TIEMPOCLIENTE"));
                    encuestaEntregada = cursor.getString(cursor.getColumnIndex("ENCUESTAENTREGADA"));


                    trama = codCliente + "," + motivo + "," + hora + "," + motivoExtraruta + "," +
                            tiempoCliente + "," + encuestaEntregada;

                    tramas.add(trama);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return tramas;

    }


    public static Vector getTramasEncabezado_() {

        int cont = 0;
        SQLiteDatabase db = null;
        String sql = "", trama = "";
        Vector tramas = new Vector();

        String codCliente, motivo, hora, motivoExtraruta, tiempoCliente, encuestaEntregada;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT CODIGOCLIENTE, TIPOTRANS, strftime(\"%H@%M\", INICIOPEDIDO) AS HORA, " +
                    "'1' AS MOTIVOEXTRARUTA, 'V' AS TIEMPOCLIENTE, '1' AS ENCUESTAENTREGADA " +
                    "FROM ENCABEZADOAndroid " +
                    "WHERE SINCRONIZADO = 0 AND TIPOTRANS <> 0";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    codCliente = cursor.getString(cursor.getColumnIndex("codigoCliente"));
                    motivo = cursor.getString(cursor.getColumnIndex("tipoTrans"));
                    hora = cursor.getString(cursor.getColumnIndex("HORA"));
                    motivoExtraruta = cursor.getString(cursor.getColumnIndex("MOTIVOEXTRARUTA"));
                    tiempoCliente = cursor.getString(cursor.getColumnIndex("TIEMPOCLIENTE"));
                    encuestaEntregada = cursor.getString(cursor.getColumnIndex("ENCUESTAENTREGADA"));


                    trama = codCliente + "," + motivo + "," + hora + "," + motivoExtraruta + "," +
                            tiempoCliente + "," + encuestaEntregada;

                    tramas.add(trama);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return tramas;

    }


    public static void ActualizarSyncEncabezado() {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("update encabezado set sincronizado = 1 where sincronizado = 0");
            db.execSQL("update encabezado set sync = 1 where sync = 0");

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static void ActualizarSyncEncabezado_() {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("update encabezadoAndroid set sincronizado = 1 where sincronizado = 0");
            db.execSQL("update encabezadoAndroid set sync = 1 where sync = 0");

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static Vector<String> getTramasDetalle() {

        int cont = 0;
        SQLiteDatabase db = null;
        String sql = "", trama = "";
        Vector<String> tramas = new Vector<String>();

        String codCliente, codProducto, cantidad, horaInicio, horaFin, pedidoEntregado, precio, tipoVenta, tiempoCliente;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT ENCABEZADO.CODIGOCLIENTE, DETALLE.CODIGOREF, DETALLE.CANTIDAD, " +
                    "strftime(\"%H@%M\", ENCABEZADO.INICIOPEDIDO) AS HORAINICIO, strftime(\"%H@%M\", ENCABEZADO.FINALPEDIDO) AS HORAFIN, '1' AS PEDIDOENTREGADO, " +
                    "DETALLEAndroid.PRECIO, '1' AS TIPOVENTA, 'V' AS TIEMPOCLIENTE " +
                    "FROM ENCABEZADO " +
                    "INNER JOIN DETALLE ON DETALLE.NUMERODOC = ENCABEZADO.NUMERODOC " +
                    "WHERE ENCABEZADO.SINCRONIZADO = 0 AND TIPOTRANS = 0";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {


                do {

                    codCliente = cursor.getString(cursor.getColumnIndex("codigoCliente"));
                    codProducto = cursor.getString(cursor.getColumnIndex("codigoref"));
                    cantidad = cursor.getString(cursor.getColumnIndex("cantidad"));
                    horaInicio = cursor.getString(cursor.getColumnIndex("HORAINICIO"));
                    horaFin = cursor.getString(cursor.getColumnIndex("HORAFIN"));
                    pedidoEntregado = cursor.getString(cursor.getColumnIndex("PEDIDOENTREGADO"));
                    precio = cursor.getString(cursor.getColumnIndex("precio"));
                    tipoVenta = cursor.getString(cursor.getColumnIndex("TIPOVENTA"));
                    tiempoCliente = cursor.getString(cursor.getColumnIndex("TIEMPOCLIENTE"));

                    trama = codCliente + "," + codProducto + "," + cantidad + "," + horaInicio + "," +
                            horaFin + "," + pedidoEntregado + "," + precio + "," + tipoVenta + "," +
                            tiempoCliente;

                    tramas.add(trama);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return tramas;

    }

    public static Vector<String> getTramasDetalle_() {

        int cont = 0;
        SQLiteDatabase db = null;
        String sql = "", trama = "";
        Vector<String> tramas = new Vector<String>();

        String codCliente, codProducto, cantidad, horaInicio, horaFin, pedidoEntregado, precio, tipoVenta, tiempoCliente;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT ENCABEZADOAndroid.CODIGOCLIENTE, DETALLEAndroid.CODIGOREF, DETALLEAndroid.CANTIDAD, " +
                    "strftime(\"%H@%M\", ENCABEZADOAndroid.INICIOPEDIDO) AS HORAINICIO, strftime(\"%H@%M\", ENCABEZADO.FINALPEDIDO) AS HORAFIN, '1' AS PEDIDOENTREGADO, " +
                    "DETALLEAndroid.PRECIO, '1' AS TIPOVENTA, 'V' AS TIEMPOCLIENTE " +
                    "FROM ENCABEZADOAndroid " +
                    "INNER JOIN DETALLEAndroid ON DETALLEAndroid.NUMERODOC = ENCABEZADOAndroid.NUMERODOC " +
                    "WHERE ENCABEZADOAndroid.SINCRONIZADO = 0 AND TIPOTRANS = 0";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {


                do {

                    codCliente = cursor.getString(cursor.getColumnIndex("codigoCliente"));
                    codProducto = cursor.getString(cursor.getColumnIndex("codigoref"));
                    cantidad = cursor.getString(cursor.getColumnIndex("cantidad"));
                    horaInicio = cursor.getString(cursor.getColumnIndex("HORAINICIO"));
                    horaFin = cursor.getString(cursor.getColumnIndex("HORAFIN"));
                    pedidoEntregado = cursor.getString(cursor.getColumnIndex("PEDIDOENTREGADO"));
                    precio = cursor.getString(cursor.getColumnIndex("precio"));
                    tipoVenta = cursor.getString(cursor.getColumnIndex("TIPOVENTA"));
                    tiempoCliente = cursor.getString(cursor.getColumnIndex("TIEMPOCLIENTE"));

                    trama = codCliente + "," + codProducto + "," + cantidad + "," + horaInicio + "," +
                            horaFin + "," + pedidoEntregado + "," + precio + "," + tipoVenta + "," +
                            tiempoCliente;

                    tramas.add(trama);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return tramas;

    }


    public static void ActualizarSyncDetalle() {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("update detalle set sincronizado = 1 where sincronizado = 0");

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static void ActualizarSyncDetalle_() {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("update detalleAndroid set sincronizado = 1 where sincronizado = 0");

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static void ActualizaEstadoRutero(String codCliente) {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("update rutero set pendiente = 1 where codigo = '" + codCliente.trim() + "'");

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static void setRutaProgramada() {

        SQLiteDatabase db = null;
        String sql = "";

        String rutaProgramada = "";
        float presupuesto = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT ruta, presupuesto " +
                    "FROM CalendarioRuta ";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    rutaProgramada = cursor.getString(cursor.getColumnIndex("ruta"));
                    presupuesto = cursor.getFloat(cursor.getColumnIndex("presupuesto"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        Main.rutaProgramada = rutaProgramada;
        Main.presupuesto = presupuesto;
        Main.rutaEscogidaVendedor = rutaProgramada;
    }

    public static int getCantClientesRuta() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM Clientes " +
                    "WHERE ruta_parada = '" + Main.rutaProgramada + "'";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }

    public static int getCantVisitasRuta() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesCompras " +
                    "INNER JOIN Clientes on NovedadesCompras.codigoCliente = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE motivo <> 10 " +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static int getCantVisitasRuta_() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesComprasAndroid " +
                    "INNER JOIN Clientes on NovedadesComprasAndroid.codigoCliente = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE motivo <> 10 " +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static int getCantVisitas() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesCompras " +
                    "WHERE motivo <> 10 " +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static int getCantVisitas_() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesComprasAndroid " +
                    "WHERE motivo <> 10 " +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static int getCantPedidosRuta() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM EncabezadoAndroid " +
                    "INNER JOIN Clientes on EncabezadoAndroid.codigo = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE tipoTrans = 0 " +
                    "GROUP BY EncabezadoAndroid.codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }

    public static int getCantPedidosRuta_() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM Encabezado " +
                    "INNER JOIN Clientes on Encabezado.codigo = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE tipoTrans = 0 " +
                    "GROUP BY Encabezado.codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static int getCantPedidos() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM Encabezado " +
                    "WHERE tipoTrans = 0 and ifnull(anulado,0) = 0 " +
                    "GROUP BY codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
        return contador;
    }


    public static int getCantPedidos_() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM Encabezado" +
                    "WHERE tipoTrans = 0 " +
                    "GROUP BY codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static int getCantNovedadesRuta() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesCompras " +
                    "INNER JOIN Clientes on NovedadesCompras.codigoCliente = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE motivo <> 10 and motivo <> 1" +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }

    public static int getCantNovedadesRuta_() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesComprasAndroid " +
                    "INNER JOIN Clientes on NovedadesComprasAndroid.codigoCliente = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE motivo <> 10 and motivo <> 1" +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static int getCantNovedades() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesCompras " +
                    "WHERE motivo <> 10 and motivo <> 1 " +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }

    public static int getCantNovedades_() {

        SQLiteDatabase db = null;
        String sql = "";

        int contador = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT count(*) as cont " +
                    "FROM NovedadesComprasAndroid " +
                    "WHERE motivo <> 10 and motivo <> 1 " +
                    "GROUP BY CodigoCliente";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    contador += cursor.getInt(cursor.getColumnIndex("cont"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return contador;
    }


    public static long getTotalVentasRuta() {

        SQLiteDatabase db = null;
        String sql = "";

        long valor = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT sum( montoFact ) as total " +
                    "FROM Encabezado " +
                    "INNER JOIN Clientes on Encabezado.codigo = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE tipoTrans = 0 " +
                    "GROUP BY Encabezado.codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    valor += cursor.getLong(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return valor;
    }


    public static long getTotalVentasRuta_() {

        SQLiteDatabase db = null;
        String sql = "";

        long valor = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT sum( montoFact ) as total " +
                    "FROM EncabezadoAndroid " +
                    "INNER JOIN Clientes on EncabezadoAndroid.codigo = Clientes.codigo and Clientes.ruta_parada = '" + Main.rutaProgramada + "' " +
                    "WHERE tipoTrans = 0 " +
                    "GROUP BY EncabezadoAndroid.codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    valor += cursor.getLong(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return valor;
    }


    public static double getTotalVentas() {

        SQLiteDatabase db = null;
        String sql = "";

        double valor = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT sum( montoFact ) as total " +
                    "FROM Encabezado " +
                    "WHERE tipoTrans = 0 and ifnull(anulado,0) = 0  " +
                    "GROUP BY codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    valor += cursor.getDouble(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return valor;
    }


    public static long getTotalVentas_() {

        SQLiteDatabase db = null;
        String sql = "";

        long valor = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT sum( montoFact ) as total " +
                    "FROM EncabezadoAndroid " +
                    "WHERE tipoTrans = 0 " +
                    "GROUP BY codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    valor += cursor.getLong(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }


        return valor;
    }


    public static void organizarTmpDescuentos() {

        SQLiteDatabase db = null;
        String sql = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            System.out.println("Cliente -->" + Main.cliente.Canal + " " + Main.cliente.bodega + " " + Main.cliente.SubCanal + " " + Main.cliente.codigo);

            db.execSQL("delete from TmpDescuentos");

            //Bodega
            sql = "insert into tmpdescuentos " +
                    " select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and canal = '" + Main.cliente.Canal + "' and bodega = '" + Main.cliente.bodega + "'" +
                    " UNION select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and subcanal = '" + Main.cliente.SubCanal + "' and bodega = '" + Main.cliente.bodega + "'" +
                    " UNION select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and bodega = '" + Main.cliente.bodega + "'";

            System.out.println(" Insercion Desc 1 -->" + sql);

            db.execSQL(sql);

            //Ciudad
            sql = "insert into tmpdescuentos " +
                    " select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and canal = '" + Main.cliente.Canal + "' and municipio = '" + Main.cliente.Ciudad + "'" +
                    " UNION select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and subcanal = '" + Main.cliente.SubCanal + "' and municipio = '" + Main.cliente.Ciudad + "'" +
                    " UNION select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and municipio = '" + Main.cliente.Ciudad + "'";
            System.out.println(" Insercion Desc 2 -->" + sql);

            db.execSQL(sql);

            //Lista de Precios
            sql = "insert into tmpdescuentos " +
                    " select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and canal = '" + Main.cliente.Canal + "' and distrito = '" + Main.cliente.CodigoAmarre + "'" +
                    " UNION select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and subcanal = '" + Main.cliente.SubCanal + "' and distrito = '" + Main.cliente.CodigoAmarre + "'" +
                    " UNION select linea, grupo, marca, descuento, material, jerarquia from descuentos where cliente = '" + Main.cliente.codigo + "' and distrito = '" + Main.cliente.CodigoAmarre + "'";
            System.out.println(" Insercion Desc 3 -->" + sql);

            db.execSQL(sql);

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static void setDescProducto(Producto producto) {

        SQLiteDatabase db = null;
        String sql = "";

        float descuento = 0;
        float precio = producto.precio;
        float precioInicial = producto.precio;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "SELECT descuento " +
                    "FROM TmpDescuentos " +
                    "where  marca = '" + producto.grupo + "' or linea = '" + producto.agrupacion + "' or grupo = '" + producto.linea + "' or material = '" + producto.codigo + "'";

            System.out.println("setDescProducto :" + sql);
            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    descuento = cursor.getFloat(cursor.getColumnIndex("descuento"));

                    descuento = ((precio * descuento) / 100);
                    precio = precio - descuento;

                } while (cursor.moveToNext());

                if (producto.precio > 0)
                    descuento = (((producto.precio - precio) * 100) / producto.precio);
                else
                    descuento = 0;

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        producto.precioDescuento = precio;
        producto.descuento = descuento;
    }


    public static void organizarTmpProductos(String listaPrecio) {
        SQLiteDatabase db = null;
        String sql = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("VACUUM;");
            db.execSQL("DROP TABLE IF EXISTS ProductosTmp");


            sql = "CREATE TABLE ProductosTmp " +
                    "(codigo varchar(8), nombre varchar(40), precio int, saldo int, iva float, " +
                    "unidadesxcaja varchar(30), ean varchar(13), agrupacion varchar(10), grupo varchar(5), " +
                    "linea varchar(5), unidadmedida varchar(2), factorlibras varchar(9), fechalimite varchar(10), " +
                    "devol varchar(5), itf varchar(14), impto1 int, orden int, Indice integer PRIMARY KEY AUTOINCREMENT)";

            db.execSQL(sql);

	    /*sql = "insert into ProductosTmp ( codigo, nombre, precio, saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, orden )" +
					" SELECT Productos.codigo, nombre, listaprecios.precio, saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Orden " +
					"FROM Productos " +
					"INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios IN "+ listaPrecio +
					" GROUP BY Productos.codigo "
					+ "ORDER BY Indice";*/


            sql = "insert into ProductosTmp ( codigo, nombre, precio, saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, orden )" +
                    " SELECT Productos.codigo, nombre, listaprecios.precio, Productos.saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Orden " +
                    "FROM PRODUCTOS_VIEW Productos " +
                    "INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios IN " + listaPrecio +
                    " GROUP BY Productos.codigo "
                    + "ORDER BY Indice";


            System.out.println("Precarga de productos precio --> :" + sql);
            db.execSQL(sql);
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {
            if (db != null)
                db.close();
        }
    }

    public static void organizarTmpProductos(String listaPrecio, String tipoTransaccion) {

        SQLiteDatabase db = null;
        String sql = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("VACUUM;");
            db.execSQL("DROP TABLE IF EXISTS ProductosTmp");

            sql = "CREATE TABLE ProductosTmp " +
                    "(codigo varchar(8), nombre varchar(40), precio int, saldo int, iva float, " +
                    "unidadesxcaja varchar(30), ean varchar(13), agrupacion varchar(10), grupo varchar(5), " +
                    "linea varchar(5), unidadmedida varchar(2), factorlibras varchar(9), fechalimite varchar(10), " +
                    "devol varchar(5), itf varchar(14), impto1 int, orden int, Indice integer PRIMARY KEY AUTOINCREMENT)";

            db.execSQL(sql);

            db.execSQL("DELETE FROM ProductosTmp;");

            /*Cargar los productos que son para autoventa y preventa segun el tipo de transccion
             * Autoventa lleva un control del inventario por lo que debe consultarse el inventario de la vista creada para tal fin
             * PRODUCTOS_VIEW*/
            if (tipoTransaccion.equals(Const.AUTOVENTA)) {
                sql = "insert into ProductosTmp ( codigo, nombre, precio, saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, orden )" +
                        " SELECT Productos.codigo, nombre, listaprecios.precio, Productos.saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Orden " +
                        "FROM PRODUCTOS_VIEW Productos " +
                        "INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios IN " + listaPrecio +
                        " GROUP BY Productos.codigo "
                        + "ORDER BY Indice";
            } else {
                sql = "insert into ProductosTmp ( codigo, nombre, precio, saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, orden )" +
                        " SELECT Productos.codigo, nombre, listaprecios.precio, saldo, iva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Orden " +
                        "FROM Productos " +
                        "INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios IN " + listaPrecio +
                        " GROUP BY Productos.codigo "
                        + "ORDER BY Indice";
            }
            db.execSQL(sql);
            System.out.println("Precarga de productos precio --> :" + sql);
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {
            if (db != null)
                db.close();
        }
    }


    public static boolean RegistrarPedido(Encabezado encabezado, Hashtable<String, Detalle> detalleTmp, String version, String imei, boolean isAutoVenta, String nroDocAmarre) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
            Cliente cliente = CargarCliente(encabezado.codigo_cliente);


            /************************************
             * Se Ingresa la Novedad Compra del Pedido
             ************************************/


            //			if(Main.usuario.tipoVenta.equals(Const.AUTOVENTA) && encabezado.tipoTrans == 0 )
            //				encabezado.numero_doc = encabezado.strFactura;


            values = new ContentValues();

            values.put("codigoCliente", encabezado.codigo_cliente);
            values.put("motivo", Main.encabezado.motivo);
            values.put("vendedor", Main.usuario.codigoVendedor);
            //			values.put("valor",         encabezado.sub_total);
            values.put("valor", Util.Redondear(String.valueOf(encabezado.valor_neto), 2));
            values.put("NroDoc", encabezado.numero_doc);
            values.put("horaInicio", encabezado.hora_inicial);
            values.put("horaFinal", encabezado.hora_final);
            values.put("version", version);
            //values.put("serial",        imei);
            values.put("fecha", fechaActual);
            values.put("sincronizado", "0");
            values.put("bodega", Main.usuario.bodega);
            values.put("tipoCliente", "0");
            values.put("syncmapas", "0");
            values.put("longitud", "0");
            values.put("latitud", "0");
            values.put("sincronizadoandroid", 0);


            db.insertOrThrow("NovedadesCompras", null, values);
            dbPedido.insertOrThrow("NovedadesCompras", null, values);

            /************************************
             * Se Ingresa el Encabezado del Producto
             ************************************/

            String CodigoAmarre = isAutoVenta ? nroDocAmarre : "";
            String fp = encabezado.tipoTrans == 0 ? cliente.tipoCredito : "0";


            values = new ContentValues();


            values.put("codigo", encabezado.codigo_cliente);
            values.put("numeroDoc", encabezado.numero_doc);
            values.put("fechaReal", fechaActual);
            values.put("tipoTrans", encabezado.tipoTrans); // 0 para pedido y 2 para devolucion
            values.put("fechaTrans", fechaActual);
            //			values.put("montoFact",     Util.Redondear(""+encabezado.sub_total, 2));
            values.put("montoFact", Util.Redondear(String.valueOf(encabezado.valor_neto), 2));
            values.put("desc1", Util.Redondear(String.valueOf(encabezado.valor_descuento), 2));
            values.put("iva", Util.Redondear(String.valueOf(encabezado.total_iva), 2));
            values.put("usuario", Main.usuario.codigoVendedor);
            values.put("fechaInicial", encabezado.hora_inicial);
            values.put("fechaFinal", encabezado.hora_final);
            values.put("observaciones", encabezado.observacion);
            //			values.put("observaciones2",encabezado.observacion2);
            values.put("bodega", Main.usuario.bodega);
            values.put("sincronizado", "0");
            values.put("fechaEntrega", encabezado.fechaEntrega);
            //			values.put("oc",            encabezado.ordenCompra);
            values.put("sincronizadomapas", "0");
            values.put("entregado", "0");
            values.put("factura", encabezado.strFactura);
            //			values.put("serial",            imei);
            values.put("syncmobile", "0");
            values.put("sincronizadoandroid", 0);
            values.put("FP2", CodigoAmarre);
            values.put("idconsecutivo", encabezado.idConsecutivo);
            values.put("prefijo_consecutivo", encabezado.prefijoConsecutivo);
            values.put("numero_consecutivo", encabezado.nroConsecutivo);
            values.put("FP", fp);


            db.insertOrThrow("Encabezado", null, values);
            dbPedido.insertOrThrow("Encabezado", null, values);


            /************************************
             * Se Ingresa el Detalle del Producto
             ************************************/

            Enumeration<Detalle> e = detalleTmp.elements();
            Detalle detalle;

            int contItem = 1;

            while (e.hasMoreElements()) {

                detalle = e.nextElement();
                values = new ContentValues();

                values.put("numDoc", encabezado.numero_doc);
                values.put("codigoRef", detalle.codProducto);
                values.put("precio", detalle.strPrecio);
                values.put("tarifaIva", detalle.iva);
                values.put("descuentoRenglon", detalle.descuento);
                values.put("cantidad", detalle.cantidad);
                values.put("vendedor", Main.usuario.codigoVendedor);
                // values.put("motivo", encabezado.codigo_novedad);
                //cero para
                // pedido si es devolucion select * from motivoscambio where
                // tipo='D'
                values.put("motivo", detalle.motivo);
                values.put("fechaReal", fechaActual);
                values.put("fecha", fechaActual);
                values.put("sincronizado", "0");
                values.put("cantidadcambio", "0");
                values.put("bodega", Main.usuario.bodega);
                values.put("bodega2", "");
                values.put("tipocliente", "0");
                values.put("inventario", "0");
                values.put("sincronizadoandroid", 0);
                values.put("item", detalle.posicion + "");
                values.put("tipoventa", 0);

                contItem++;

                db.insertOrThrow("Detalle", null, values);
                dbPedido.insertOrThrow("Detalle", null, values);
            }
            //			db.execSQL("Update descuentosautorizados set numerodoc='" + encabezado.numero_doc + "' where numerodoc='" + encabezado.codigo_cliente + "'");
            //			dbPedido.execSQL("Update descuentosautorizados set numerodoc='" + encabezado.numero_doc + "' where numerodoc='" + encabezado.codigo_cliente + "'");
            return true;
        } catch (Exception e) {
            mensaje = e.getMessage();
            return false;
        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean RegistrarPedido_(Encabezado encabezado, Hashtable<String, Detalle> detalleTmp) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            /************************************
             * Se Ingresa la Novedad Compra del Pedido
             ************************************/


            values = new ContentValues();

            values.put("codigoCliente", encabezado.codigo_cliente);
            values.put("motivo", "1");
            values.put("vendedor", Main.usuario.codigoVendedor);
            //			values.put("valor",         encabezado.sub_total);
            values.put("valor", encabezado.valor_neto);
            values.put("fecha", fechaActual);
            values.put("NroDoc", encabezado.numero_doc);
            values.put("horaInicio", encabezado.hora_inicial);
            values.put("horaFinal", encabezado.hora_final);
            values.put("version", Main.versionApp);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);

            db.insertOrThrow("NovedadesCompras", null, values);
            dbPedido.insertOrThrow("NovedadesCompras", null, values);

            /************************************
             * Se Ingresa el Encabezado del Producto
             ************************************/

            values = new ContentValues();

            values.put("codigo", encabezado.codigo_cliente);
            values.put("numeroDoc", encabezado.numero_doc);
            values.put("fechaTrans", fechaActual);
            values.put("tipoTrans", encabezado.tipoTrans);
            //			values.put("montoFact",     encabezado.sub_total);
            values.put("montoFact", encabezado.valor_neto);
            values.put("desc1", encabezado.valor_descuento);
            values.put("iva", encabezado.total_iva);
            values.put("usuario", Main.usuario.codigoVendedor);
            values.put("fechaInicial", encabezado.hora_inicial);
            values.put("fechaFinal", encabezado.hora_final);
            values.put("observaciones", encabezado.observacion);
            values.put("observaciones2", encabezado.observacion2);
            values.put("ordenCompra", encabezado.ordenCompra);
            values.put("fechaEntrega", encabezado.fechaEntrega);
            values.put("bodega", Main.cliente.bodega);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);
            values.put("tipoventa", 0);

            db.insertOrThrow("Encabezado", null, values);
            dbPedido.insertOrThrow("Encabezado", null, values);


            /************************************
             * Se Ingresa el Detalle del Producto
             ************************************/

            Enumeration<Detalle> e = detalleTmp.elements();
            Detalle detalle;

            while (e.hasMoreElements()) {

                detalle = e.nextElement();
                values = new ContentValues();

                values.put("numDoc", encabezado.numero_doc);
                values.put("codigoRef", detalle.codProducto);
                values.put("precio", detalle.precio);
                values.put("tarifaIva", detalle.iva);
                values.put("descuentoRenglon", detalle.descuento);
                values.put("cantidad", detalle.cantidad);
                values.put("vendedor", Main.usuario.codigoVendedor);
                values.put("motivo", encabezado.codigo_novedad);
                values.put("fecha", fechaActual);
                values.put("sincronizado", 0);
                values.put("sincronizadoandroid", 0);

                db.insertOrThrow("Detalle", null, values);
                dbPedido.insertOrThrow("Detalle", null, values);
            }

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean ActualizarSyncTablaEnvio_() {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //Marca el envio de los Encabezados
            db.execSQL("UPDATE EncabezadoAndroid SET sincronizado = 1 WHERE sincronizado = 0");
            dbTemp.execSQL("UPDATE EncabezadoAndroid SET sincronizado = 1 WHERE sincronizado = 0");

            //Marca el envio de los Detalles
            db.execSQL("UPDATE DetalleAndroid SET sincronizado = 1 WHERE sincronizado = 0");
            dbTemp.execSQL("UPDATE DetalleAndroid SET sincronizado = 1 WHERE sincronizado = 0");

            //Marca el envio de las NovedadesCompras
            db.execSQL("UPDATE NovedadesComprasAndroid SET sincronizado = 1 WHERE sincronizado = 0");
            dbTemp.execSQL("UPDATE NovedadesComprasAndroid SET sincronizado = 1 WHERE sincronizado = 0");

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ActualizarSyncTablaEnvio: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static boolean ActualizarSyncTablaEnvio() {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //Marca el envio de los Encabezados
            db.execSQL("UPDATE Encabezado SET sincronizado = 1 WHERE sincronizado = 0");
            dbTemp.execSQL("UPDATE Encabezado SET sincronizado = 1 WHERE sincronizado = 0");

            //Marca el envio de los Detalles
            db.execSQL("UPDATE Detalle SET sincronizado = 1 WHERE sincronizado = 0");
            dbTemp.execSQL("UPDATE Detalle SET sincronizado = 1 WHERE sincronizado = 0");

            //Marca el envio de las NovedadesCompras
            db.execSQL("UPDATE NovedadesCompras SET sincronizado = 1 WHERE sincronizado = 0");
            dbTemp.execSQL("UPDATE NovedadesCompras SET sincronizado = 1 WHERE sincronizado = 0");

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ActualizarSyncTablaEnvio: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static void BorrarPedidoIncompleto(String numeroDoc) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM Detalle WHERE numDoc = '" + numeroDoc + "'");
                db.execSQL("DELETE FROM NovedadesCompras WHERE NroDoc = '" + numeroDoc + "'");
                db.execSQL("DELETE FROM Encabezado WHERE numeroDoc = '" + numeroDoc + "'");

                dbPedido.execSQL("DELETE FROM Detalle WHERE numDoc = '" + numeroDoc + "'");
                dbPedido.execSQL("DELETE FROM NovedadesCompras WHERE NroDoc = '" + numeroDoc + "'");
                dbPedido.execSQL("DELETE FROM Encabezado WHERE numeroDoc = '" + numeroDoc + "'");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static void BorrarPedidoIncompleto_(String numeroDoc) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM DetalleAndroid WHERE numDoc = '" + numeroDoc + "'");
                db.execSQL("DELETE FROM NovedadesComprasAndroid WHERE NroDoc = '" + numeroDoc + "'");
                db.execSQL("DELETE FROM EncabezadoAndroid WHERE numeroDoc = '" + numeroDoc + "'");

                dbPedido.execSQL("DELETE FROM DetalleAndroid WHERE numDoc = '" + numeroDoc + "'");
                dbPedido.execSQL("DELETE FROM NovedadesComprasAndroid WHERE NroDoc = '" + numeroDoc + "'");
                dbPedido.execSQL("DELETE FROM EncabezadoAndroid WHERE numeroDoc = '" + numeroDoc + "'");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static double getValorCartera(String codCliente) {

        SQLiteDatabase db = null;
        String sql = "";

        double cartera = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select sum( saldo - abono ) as valor from cartera where cliente = '" + codCliente + "'";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    cartera = cursor.getDouble(cursor.getColumnIndex("valor"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return cartera;
    }

    public static String getExperienciaCrediticia(String codCliente) {

        SQLiteDatabase db = null;
        String sql = "";

        String clasif1, clasif2, clasif3, EC;

        clasif1 = clasif2 = clasif3 = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select ltrim(rtrim(clasifca1)) as clasifica1, ltrim(rtrim(clasifca2)) as clasifica2, ltrim(rtrim(clasifca3)) as clasifica3 from cartera where cliente = '" + codCliente + "' limit 1";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    clasif1 = cursor.getString(cursor.getColumnIndex("clasifica1"));
                    clasif2 = cursor.getString(cursor.getColumnIndex("clasifica2"));
                    clasif3 = cursor.getString(cursor.getColumnIndex("clasifica3"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        EC = clasif1 + "-" + clasif2 + "-" + clasif3;

        return EC;
    }

    public static long getPedidoMinimo() {

        SQLiteDatabase db = null;
        String sql = "";

        float minimo = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select minimo from Minimos limit 1";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    minimo = cursor.getFloat(cursor.getColumnIndex("minimo"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return (long) minimo;
    }

    public static Vector<MotivoDescuento> listaMotivosDescuentos(Vector<String> items) {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<MotivoDescuento> listaMotivos = new Vector<MotivoDescuento>();

        try {

            MotivoDescuento motivoDescuento;
            listaMotivos = new Vector<MotivoDescuento>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("MotivosDescuentos",
                    new String[]{"CodMotivo", "Descripcion"},
                    null,
                    null,
                    null,
                    null,
                    "CodMotivo");

            if (cursor.moveToFirst()) {

                do {

                    motivoDescuento = new MotivoDescuento();
                    motivoDescuento.CodMotivo = cursor.getInt(cursor.getColumnIndex("CodMotivo"));
                    motivoDescuento.Descripcion = cursor.getString(cursor.getColumnIndex("Descripcion"));

                    listaMotivos.addElement(motivoDescuento);
                    items.addElement(motivoDescuento.Descripcion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaMotivosDescuentos -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaMotivos;
    }

    public static Vector<MotivoAbono> listaMotivoAbono(Vector<String> items) {

        mensaje = "";
        SQLiteDatabase db = null;

        MotivoAbono motivoAbono;
        Vector<MotivoAbono> listaMotivos = new Vector<MotivoAbono>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            Cursor cursor = db.rawQuery("SELECT codigo, descripcion AS nombre FROM MotivoAbono ORDER BY codigo", null);

            if (cursor.moveToFirst()) {

                do {

                    motivoAbono = new MotivoAbono();
                    motivoAbono.codigo = cursor.getInt(cursor.getColumnIndex("codigo"));
                    motivoAbono.nombre = cursor.getString(cursor.getColumnIndex("nombre"));

                    items.addElement(motivoAbono.nombre);
                    listaMotivos.addElement(motivoAbono);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaMotivoAbono -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaMotivos;
    }

    public static Vector<Cartera> listaCartera(String codigoCliente) {

        mensaje = "";
        SQLiteDatabase db = null;

        Cartera cartera = new Cartera();
        Vector<Cartera> listaCartera = new Vector<Cartera>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query =

                    "SELECT Documento, " +
                            "Saldo - (CASE WHEN DetalleRecaudo.valor IS NULL THEN 0 ELSE DetalleRecaudo.valor END) AS Saldo, " +
                            "abono, Concepto, fecha, FechaVecto, " +
                            "IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaVencimiento)), -1) AS dias, NumFiscal " +
                            "FROM Cartera LEFT JOIN DetalleRecaudo ON Cartera.Documento = DetalleRecaudo.numDoc AND tipoPago = 'P' " +
                            "WHERE Cliente = '" + codigoCliente + "' " +
                            "AND Documento NOT IN (SELECT numDoc FROM DetalleRecaudo WHERE tipoPago = 'T') " +
                            "ORDER BY dias DESC";

            System.out.println("consulta de cartera : " + query);


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cartera = new Cartera();

                    //Se valida que el Saldo no Contenga e+
                    String saldo = cursor.getString(cursor.getColumnIndex("Saldo"));

                    if (saldo.contains("e+")) {
                        saldo = saldo.replace("e+", "E");
                    }
                    saldo = Util.QuitarE(saldo);


                    //Se valida que el Abono no Contenga e+
                    String abono = cursor.getString(cursor.getColumnIndex("abono"));

                    if (abono.contains("e+")) {
                        abono = abono.replace("e+", "E");
                    }
                    abono = Util.QuitarE(abono);

                    cartera.strSaldo = saldo;
                    cartera.strAbono = abono;

                    cartera.codCliente = codigoCliente;
                    cartera.documento = cursor.getString(cursor.getColumnIndex("documento"));
                    cartera.saldo = Util.ToFloat(Util.Redondear(saldo, 2));
                    cartera.abono = cursor.getFloat(cursor.getColumnIndex("abono"));
                    cartera.concepto = cursor.getString(cursor.getColumnIndex("concepto"));
                    cartera.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    cartera.FechaVecto = cursor.getString(cursor.getColumnIndex("fechavecto"));
                    cartera.dias = cursor.getInt(cursor.getColumnIndex("dias"));
                    cartera.nroFiscal = cursor.getString(cursor.getColumnIndex("NumFiscal"));

                    listaCartera.addElement(cartera);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaCartera - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCartera;
    }


    public static Vector<DetalleGasto> listaDetalleGastos(String codigoVendedor) {

        mensaje = "";
        SQLiteDatabase db = null;

        DetalleGasto detalleGasto = new DetalleGasto();
        Vector<DetalleGasto> listaDetalleGastos = new Vector<DetalleGasto>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = " SELECT CodigoGastos.Nombre, detallegastos.Valor, detallegastos.fecha,detallegastos.sincronizado, " +
                    "detallegastos.concepto FROM  detallegastos INNER JOIN CodigoGastos " +
                    " ON detallegastos.concepto = CodigoGastos.codigo order by detallegastos.concepto ";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalleGasto = new DetalleGasto();

                    //Se valida que el Saldo no Contenga e+
                    double valor = cursor.getDouble(cursor.getColumnIndex("Valor"));


                    detalleGasto.valor = valor;
                    detalleGasto.concepto = cursor.getString(cursor.getColumnIndex("concepto"));
                    detalleGasto.nombreConcepto = cursor.getString(cursor.getColumnIndex("nombre"));
                    detalleGasto.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    detalleGasto.sincronizado = cursor.getString(cursor.getColumnIndex("sincronizado"));
                    //detalleGasto.id = Util.ToInt(cursor.getString(cursor.getColumnIndex("id")));

                    if (detalleGasto.sincronizado.equals("0"))
                        listaDetalleGastos.addElement(detalleGasto);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaDetalleGastos - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalleGastos;
    }


    public static Vector<DetalleTiempos> listaDetalleTiempos(String codigoVendedor) {

        mensaje = "";
        SQLiteDatabase db = null;

        DetalleTiempos detalleTiempos = new DetalleTiempos();
        Vector<DetalleTiempos> listaDetalleTiempos = new Vector<DetalleTiempos>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select actividad,horainicial,horafinal,Codigovendedor,Numerodoc,0 as minutos,Actividades.descripcion,FECHA,sincronizado from Tiempos inner join Actividades on Tiempos.actividad=Actividades.codigo";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalleTiempos = new DetalleTiempos();


                    detalleTiempos.Actividad = cursor.getString(cursor.getColumnIndex("actividad"));
                    detalleTiempos.horainicial = cursor.getString(cursor.getColumnIndex("horainicial"));
                    detalleTiempos.horafinal = cursor.getString(cursor.getColumnIndex("horafinal"));
                    detalleTiempos.CodigoVendedor = cursor.getString(cursor.getColumnIndex("CodigoVendedor"));
                    detalleTiempos.Numerodoc = cursor.getString(cursor.getColumnIndex("Numerodoc"));
                    detalleTiempos.descripcionActividad = cursor.getString(cursor.getColumnIndex("descripcion"));
                    detalleTiempos.fecha = cursor.getString(cursor.getColumnIndex("FECHA"));
                    detalleTiempos.sincronizado = cursor.getString(cursor.getColumnIndex("sincronizado"));

                    listaDetalleTiempos.addElement(detalleTiempos);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaDetallTiempos - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalleTiempos;
    }


    public static Vector<Cartera> listaCartera(String codigoCliente, Vector<ItemListView> listaItems) {

        mensaje = "";
        SQLiteDatabase db = null;

        Cartera cartera = new Cartera();
        ItemListView itemListView;
        Vector<Cartera> listaCartera = new Vector<Cartera>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT referencia, " +
                    " saldo - abono as saldo , " //(CASE WHEN DetalleRecaudo.valor IS NULL THEN 0 ELSE DetalleRecaudo.valor END) AS saldo,
                    + "abono , " +
                    " IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaVencimiento)), -1) AS dias, IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaProntoPago)), -1) AS dias2, " +
                    " fechavecto, fecha, concepto, documento, descripcion, Cartera.prontopago,  referencia, NumFiscal " +
                    " FROM Cartera  " +
                    " WHERE cliente = '" + codigoCliente + "' " +
                    " AND documento NOT IN (SELECT numdoc FROM DetalleRecaudo WHERE tipoPago = 'T') " +
                    " AND documento NOT IN (SELECT Nrodoc from tmpdetallerecaudo) " +
                    " ORDER BY dias DESC";

            System.out.println("Consulta lista Recaudo: " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cartera = new Cartera();

                    //Se valida que el Saldo no Contenga e+
                    long saldo = cursor.getLong(cursor.getColumnIndex("saldo"));


                    //Se valida que el Abono no Contenga e+
                    String abono = cursor.getString(cursor.getColumnIndex("abono"));

                    if (abono.contains("e+")) {
                        abono = abono.replace("e+", "E");
                    }
                    abono = Util.QuitarE(abono);

                    cartera.strSaldo = saldo + "";
                    cartera.strAbono = abono;

                    cartera.codCliente = codigoCliente;
                    cartera.documento = cursor.getString(cursor.getColumnIndex("documento"));
                    cartera.saldo = cursor.getFloat(cursor.getColumnIndex("saldo"));
                    ;
                    cartera.abono = cursor.getFloat(cursor.getColumnIndex("abono"));
                    cartera.concepto = cursor.getString(cursor.getColumnIndex("concepto"));
                    cartera.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    cartera.FechaVecto = cursor.getString(cursor.getColumnIndex("fechavecto"));
                    cartera.dias = cursor.getInt(cursor.getColumnIndex("dias"));
                    cartera.vendedor = Main.usuario.codigoVendedor;
                    cartera.referencia = cursor.getString(cursor.getColumnIndex("referencia"));
                    cartera.nroFiscal = cursor.getString(cursor.getColumnIndex("NumFiscal"));

                    listaCartera.addElement(cartera);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaCartera - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCartera;
    }


    public static Vector<Cartera> listaCarteraPorReferencia(String codigoCliente, String referencia, String documento) {

        mensaje = "";
        SQLiteDatabase db = null;

        Cartera cartera = new Cartera();

        Vector<Cartera> listaCartera = new Vector<Cartera>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "SELECT referencia, " +
                    "Saldo -  (CASE WHEN DetalleRecaudo.valor IS NULL THEN 0 ELSE DetalleRecaudo.valor END) AS valor, " +
                    "IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaVencimiento)), -1) AS dias, IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaProntoPago)), -1) AS dias2, " +
                    "FechaVecto, fecha, concepto, documento, descripcion, NumFiscal, " +
                    "CASE WHEN DetalleRecaudo.valor IS NULL THEN 0 ELSE DetalleRecaudo.valor END AS pagoParcial " +
                    "FROM Cartera LEFT JOIN DetalleRecaudo ON Cartera.Documento = DetalleRecaudo.numDoc AND tipoPago = 'P' " +
                    "WHERE cliente = '" + codigoCliente + "' and referencia = '" + referencia +
                    "'  AND documento NOT IN (SELECT numDoc FROM DetalleRecaudo WHERE tipoPago = 'T') " +
                    "ORDER BY dias DESC";

            System.out.println("Referenciaaaa: " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cartera = new Cartera();

                    //Se valida que el Valor no Contenga e+
                    String valor = cursor.getString(cursor.getColumnIndex("valor"));

                    if (valor.contains("e+")) {
                        valor = valor.replace("e+", "E");
                    }
                    valor = Util.QuitarE(valor);

                    cartera.strSaldo = valor;
                    cartera.strAbono = "0";

                    cartera.codCliente = codigoCliente;
                    cartera.referencia = cursor.getString(cursor.getColumnIndex("referencia"));
                    cartera.saldo = Util.ToFloat(Util.Redondear(valor, 2));
                    cartera.valorARecaudar = cartera.saldo;
                    cartera.dias = cursor.getInt(cursor.getColumnIndex("dias"));
                    cartera.FechaVecto = cursor.getString(cursor.getColumnIndex("fechavecto"));
                    cartera.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    cartera.concepto = cursor.getString(cursor.getColumnIndex("concepto"));
                    cartera.documento = cursor.getString(cursor.getColumnIndex("documento"));
                    cartera.descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));
                    cartera.dias2 = cursor.getInt(cursor.getColumnIndex("dias2"));
                    cartera.nroFiscal = cursor.getString(cursor.getColumnIndex("NumFiscal"));

                    if (existeCartera2(cartera.documento, codigoCliente))
                        continue;

                    //itemCarteraListView.codigo = cartera.referencia;
                    //itemCarteraListView.titulo = cartera.referencia + "  " + Util.SepararMiles("" + cartera.strSaldo);
                    //itemCarteraListView.subTitulo = "Dias Venc: " + cartera.dias + ". Fecha Vec: " + cartera.FechaVecto;

                    //itemListView.codigo = cartera.documento;


                    listaCartera.addElement(cartera);


                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaCartera - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCartera;
    }


    public static boolean existeCartera(String referencia, String codigoCliente) {

        for (Cartera cartera : Main.cartera) {

            if (cartera.referencia.equals(referencia) && cartera.codCliente.equals(codigoCliente))
                return true;
        }

        return false;
    }


    public static boolean existeCartera2(String referencia, String codigoCliente) {

        for (Cartera cartera : Main.cartera) {

            if (cartera.documento.equals(referencia) && cartera.codCliente.equals(codigoCliente))
                return true;
        }

        return false;
    }


    public static boolean guardarRecaudo(String deviceId, String nroRecibo, String consecutivo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            Usuario usuario = ObtenerUsuario();
            if (usuario == null) {

                mensaje = "No se pudo cargar la informacion del Usuario";
                return false;
            }

            String nroDoc = obtenterNumeroDocRecaudo(consecutivo);

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File fileTemp = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.beginTransaction();
            dbTemp.beginTransaction();

            TransaccionBO.insertEncabezadoRecaudo(db, usuario, nroDoc, nroRecibo, deviceId);
            TransaccionBO.insertEncabezadoRecaudo(dbTemp, usuario, nroDoc, nroRecibo, deviceId);

            for (Cartera cartera : Main.cartera) {

                TransaccionBO.insertDetalleRecaudo(db, usuario, cartera, nroDoc, nroRecibo, deviceId);
                TransaccionBO.insertDetalleRecaudo2(dbTemp, usuario, cartera, nroDoc, nroRecibo, deviceId);
            }

            Enumeration<FormaPago> e = Main.listaFormasPago.elements();

            while (e.hasMoreElements()) {

                FormaPago formaPago = e.nextElement();

                TransaccionBO.insertFormaPago(db, usuario, formaPago, nroDoc, deviceId);
                TransaccionBO.insertFormaPago(dbTemp, usuario, formaPago, nroDoc, deviceId);
            }

            /****************************
             * Se Confirma la Transaccion
             ****************************/
            db.setTransactionSuccessful();
            dbTemp.setTransactionSuccessful();

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "guardarRecaudo - > " + mensaje, e);
            return false;

        } finally {

            closeDataBase(db);
            closeDataBase(dbTemp);
        }
    }

    public static String obtenterNumeroDocRecaudo(String consecutivo) {

        String num = "-";
        //SQLiteDatabase db = null;

        try {

            //File dbFile = new File(Util.DirApp(), "DataBase.db");
            //db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //int total = 0;
            //Cursor cursor = db.rawQuery("SELECT COUNT(fecha_consecutivo) AS total FROM EncabezadoRecaudo WHERE fecha_consecutivo = '" + Main.usuario.fechaConsecutivo + "'", null);

            //if (cursor.moveToFirst()) {

            //	total = cursor.getInt(cursor.getColumnIndex("total"));
            //}

            //if (cursor != null)
            //	cursor.close();

            //if (total == 0) {

            //No se ha generado ningun Numero Doc para el dia Actual, se Reincia el consecutivo
            //	db.execSQL("DELETE FROM ConsecutivoRecaudo");
            //}

            //db.execSQL("INSERT INTO ConsecutivoRecaudo(id) VALUES ((SELECT CASE WHEN MAX(id) IS NULL THEN 0 ELSE MAX(ROWID) END FROM ConsecutivoRecaudo) + 1)");


            //cursor = db.rawQuery("SELECT MAX(id) AS id FROM ConsecutivoRecaudo", null);

            //if (cursor.moveToFirst()) {

            //int consecutivo = cursor.getInt(cursor.getColumnIndex("id"));
            //num = Util.lpad(Main.usuario.codigoEmpresa, 4, "0") + Main.usuario.codigoVendedor + Main.usuario.fechaConsecutivo + Util.lpad("" + consecutivo, 3, "0");

            DataBaseBOJ.validarUsuario();
            num = "A" + Main.usuario.codigoVendedor + Util.FechaActual("yyyyMMddHHmmssSS") + Util.lpad("" + consecutivo, 4, "0");
            //}

            //if (cursor != null)
            //	cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            //if (db != null)
            //	db.close();
        }

        return num;
    }


    public static void closeDataBase(SQLiteDatabase db) {

        if (db != null) {

            if (db.inTransaction())
                db.endTransaction();

            db.close();
        }
    }


    public static void validarUsuario() {

        if (Main.usuario == null)
            Main.usuario = new Usuario();

        if (Main.usuario.codigoVendedor == null || Main.usuario.nombreVendedor == null || Main.usuario.fechaConsecutivo == null) {

            Usuario usuario = ObtenerUsuario();

            if (usuario != null) {

                Main.usuario.codigoVendedor = usuario.codigoVendedor;
                Main.usuario.nombreVendedor = usuario.nombreVendedor;
                Main.usuario.fechaLabores = usuario.fechaLabores;
                Main.usuario.fechaConsecutivo = usuario.fechaConsecutivo;
                Main.usuario.bodega = usuario.bodega;
                Main.usuario.tipoVenta = usuario.tipoVenta;
            }
        }
    }

    public static void validarCliente() {

        if (Main.cliente == null)
            Main.cliente = new Cliente();

        if (Main.cliente.codigo == null || Main.cliente.Nombre == null || Main.cliente.fechaIngreso == null) {

            Cliente cliente = CargarClienteSeleccionado();
            if (cliente != null) {

                Main.cliente.codigo = cliente.codigo;
                Main.cliente.Nombre = cliente.Nombre;
                Main.cliente.razonSocial = cliente.razonSocial;
            }
        }
    }


    public static Vector<Producto> BuscarProductos2(int opBusq, String opBusqueda, String listaPrecio) {

        Producto producto;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Producto> listaProductos = new Vector<Producto>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";


            if (opBusq == 1) {


                query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                        "FROM Productos " +
                        "INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '" + listaPrecio + "' " +
                        "WHERE Productos.codigo LIKE '" + opBusqueda + "%' LIMIT 100";


            } else {


                if (opBusq == 2) {

                    query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                            "FROM Productos " +
                            "INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '" + listaPrecio + "' " +
                            "WHERE Productos.nombre LIKE '" + opBusqueda + "%'  LIMIT 100";

                } else {


                    if (opBusq == 3) {


                        query = "SELECT Productos.codigo, nombre, listaprecios.precio, iva, saldo, (listaprecios.precio + ((listaprecios.precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice " +
                                "FROM Productos " +
                                "INNER JOIN ListaPrecios on productos.codigo = listaprecios.codigo and listaprecios.listaprecios = '" + listaPrecio + "' LIMIT 100 ";


                    } else {


                    }


                }


            }

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    producto = new Producto();
                    itemListView = new ItemListView();

                    producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    producto.precio = cursor.getInt(cursor.getColumnIndex("precio"));
                    producto.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    producto.inventario = cursor.getInt(cursor.getColumnIndex("saldo"));
                    producto.precioIva = cursor.getFloat(cursor.getColumnIndex("precioIva"));
                    producto.unidadesXCaja = cursor.getString(cursor.getColumnIndex("unidadesxcaja"));
                    producto.ean = cursor.getString(cursor.getColumnIndex("ean"));
                    producto.agrupacion = cursor.getString(cursor.getColumnIndex("agrupacion"));
                    producto.grupo = cursor.getString(cursor.getColumnIndex("grupo"));
                    producto.linea = cursor.getString(cursor.getColumnIndex("linea"));
                    producto.unidadMedida = cursor.getString(cursor.getColumnIndex("unidadmedida"));
                    producto.factorLibras = cursor.getInt(cursor.getColumnIndex("factorlibras"));
                    producto.fechaLimite = cursor.getString(cursor.getColumnIndex("fechalimite"));
                    producto.devol = cursor.getString(cursor.getColumnIndex("devol"));
                    producto.itf = cursor.getString(cursor.getColumnIndex("itf"));
                    producto.impto1 = cursor.getInt(cursor.getColumnIndex("impto1"));
                    producto.indice = cursor.getInt(cursor.getColumnIndex("Indice"));

                    producto.precioDescuento = 0;
                    producto.descuento = 0;

                    itemListView.titulo = producto.codigo + " - " + producto.descripcion;
                    itemListView.subTitulo = "Precio: $" + producto.precio + " - Iva: " + producto.iva;

                    listaProductos.addElement(producto);


                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("BuscarProductos", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaProductos;
    }


    public static Vector<Concepto> ListaConceptos(Vector<String> listaItems) {

        SQLiteDatabase db = null;
        Concepto concepto;
        Vector<Concepto> listaConceptos = new Vector<Concepto>();

        listaItems = new Vector<String>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("CodigoGastos",
                    new String[]{"codigo", "nombre", "tipo", "transporte", "obligatorio"},
                    null,
                    null,
                    null,
                    null,
                    "codigo");

            if (cursor.moveToFirst()) {

                do {

                    concepto = new Concepto();

                    concepto.codigoConcepto = cursor.getString(cursor.getColumnIndex("codigo"));
                    concepto.nombreConcepto = cursor.getString(cursor.getColumnIndex("nombre"));
                    concepto.tipo = cursor.getString(cursor.getColumnIndex("tipo"));
                    concepto.transporte = cursor.getString(cursor.getColumnIndex("transporte"));
                    concepto.obligatorio = cursor.getString(cursor.getColumnIndex("obligatorio"));

                    listaConceptos.addElement(concepto);
                    listaItems.add(concepto.nombreConcepto);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaConceptos", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaConceptos;
    }


    public static Vector<Actividad> ListaActividades(Vector<String> listaItems) {

        SQLiteDatabase db = null;
        Actividad actividad;
        Vector<Actividad> listaActividades = new Vector<Actividad>();

        listaItems = new Vector<String>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("Actividades",
                    new String[]{"Codigo", "descripcion"},
                    null,
                    null,
                    null,
                    null,
                    "Codigo");

            if (cursor.moveToFirst()) {

                do {

                    actividad = new Actividad();

                    actividad.codigo = cursor.getString(cursor.getColumnIndex("Codigo"));
                    actividad.descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));


                    listaActividades.addElement(actividad);
                    listaItems.add(actividad.descripcion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaConceptos", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaActividades;
    }


    public static String ObtenterConsecutivo(String codVendedor) {

        String num = "-";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            int consecutivo = 0;
            Cursor cursor = db.rawQuery("SELECT consecutivo FROM Vendedor", null);

            if (cursor.moveToFirst())
                consecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));

            if (cursor != null)
                cursor.close();

            consecutivo = (consecutivo + 1) % 200;
            db.execSQL("UPDATE Vendedor SET consecutivo = " + consecutivo);

            if (cursor != null)
                cursor.close();


            return num;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenerConsecutivo", mensaje, e);
            return "-";

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean GuardarDetalleDeGastos(DetalleGasto detalleGasto) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {


            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("nrodoc", detalleGasto.nroDoc);
            values.put("vendedor", detalleGasto.vendedor);
            values.put("fecha", detalleGasto.fecha);
            values.put("Valor", detalleGasto.valor);
            values.put("concepto", detalleGasto.concepto);
            values.put("NIT", detalleGasto.nit);
            //values.put("id",    detalleGasto.id);
            values.put("Observacion", detalleGasto.observacion);
            values.put("Anticipo", detalleGasto.anticipo);
            values.put("sincronizado", 0);
            values.put("bodega", detalleGasto.bodega);
            values.put("codcedula", 0);
            values.put("bandera", 0);
            values.put("ordencompra", 0);
            values.put("sincronizadoandroid", 0);

            db.insertOrThrow("detallegastos", null, values);
            dbTemp.insertOrThrow("detallegastos", null, values);


            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarNoCompra: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static boolean GuardarDetalleTiempos(DetalleTiempos tiempos) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {


            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("Numerodoc", tiempos.Numerodoc);
            values.put("CodigoVendedor", tiempos.CodigoVendedor);
            values.put("horainicial", tiempos.horainicial);
            values.put("horafinal", tiempos.horafinal);
            values.put("actividad", tiempos.Actividad);
            values.put("FECHA", tiempos.fecha);
            values.put("Observacion", tiempos.observacion);
            values.put("sincronizado", 0);
            values.put("sincronizadoandroid", 0);


            db.insertOrThrow("Tiempos", null, values);
            dbTemp.insertOrThrow("Tiempos", null, values);


            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarTiempos: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static int getIdDetalleGasto() {

        SQLiteDatabase db = null;
        int id = 1;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT id FROM detallegastos order by id desc";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                id = Util.ToInt(cursor.getString(cursor.getColumnIndex("id")));
                id++;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return id;
    }


    public static boolean EliminarDetalleGasto_(DetalleGasto detalleGasto) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{"" + detalleGasto.fecha};

            /*************************************************************
             * Se Elimina el Producto del pedido, en Detalle y TmpPedido
             ************************************************************/

            int rowDetalle = db.delete("detallegastos", "fecha = ?", whereArgs);
            int rowsDetalleTmp = dbPedido.delete("detallegastos", "fecha = ?", whereArgs);

            Log.i("EliminarDetalleGasto", "fecha = " + detalleGasto.fecha);
            return rowDetalle > 0 && rowsDetalleTmp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean EliminarDetalleGasto(DetalleGasto detalleGasto) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{"" + detalleGasto.id};

            /*************************************************************
             * Se Elimina el Producto del pedido, en Detalle y TmpPedido
             ************************************************************/

            int rowDetalle = db.delete("detallegastos", "id = ?", whereArgs);
            int rowsDetalleTmp = dbPedido.delete("detallegastos", "id = ?", whereArgs);

            Log.i("EliminarDetalleGasto", "id = " + detalleGasto.id);
            return rowDetalle > 0 && rowsDetalleTmp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean EliminarDetalleTiempos(DetalleTiempos detalleTiempos) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{"" + detalleTiempos.Numerodoc};

            /*************************************************************
             * Se Elimina el Producto del pedido, en Detalle y TmpPedido
             ************************************************************/

            int rowDetalle = db.delete("Tiempos", "Numerodoc = ?", whereArgs);
            int rowsDetalleTmp = dbPedido.delete("Tiempos", "Numerodoc = ?", whereArgs);

            Log.i("EliminarDetalleTiempos", "NroDoc = " + detalleTiempos.Numerodoc);
            return rowDetalle > 0 && rowsDetalleTmp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static Vector<Ciudad> ListaCiudades(Vector<String> items) {

        SQLiteDatabase db = null;
        Ciudad ciudad;
        Vector<Ciudad> listaCiuades = new Vector<Ciudad>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("Ciudades",
                    new String[]{"codigo", "nombre"},
                    null,
                    null,
                    null,
                    null,
                    "codigo");

            if (cursor.moveToFirst()) {

                do {

                    ciudad = new Ciudad();

                    ciudad.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    ciudad.nombre = cursor.getString(cursor.getColumnIndex("nombre"));

                    listaCiuades.addElement(ciudad);
                    items.addElement(ciudad.nombre);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaCiudades", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCiuades;
    }


    public static Vector<Canal> ListaCanales(Vector<String> items) {

        SQLiteDatabase db = null;
        Canal canal;
        Vector<Canal> listaCanales = new Vector<Canal>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("Canales",
                    new String[]{"codigo", "nombre"},
                    null,
                    null,
                    null,
                    null,
                    "codigo");

            if (cursor.moveToFirst()) {

                do {

                    canal = new Canal();

                    canal.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    canal.nombre = cursor.getString(cursor.getColumnIndex("nombre"));

                    listaCanales.addElement(canal);
                    items.addElement(canal.nombre);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaCanales", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCanales;
    }


    public static Vector<SubCanal> ListaSubCanales(Vector<String> items) {

        SQLiteDatabase db = null;
        SubCanal subCanal;
        Vector<SubCanal> listaSubCanales = new Vector<SubCanal>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("SubCanales",
                    new String[]{"codigo", "nombre"},
                    null,
                    null,
                    null,
                    null,
                    "codigo");

            if (cursor.moveToFirst()) {

                do {

                    subCanal = new SubCanal();

                    subCanal.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    subCanal.nombre = cursor.getString(cursor.getColumnIndex("nombre"));

                    listaSubCanales.addElement(subCanal);
                    items.addElement(subCanal.nombre);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaSubCanales", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaSubCanales;
    }


    public static Vector<SubCanal> ListaSubCanales(Vector<String> items, String canal) {

        SQLiteDatabase db = null;
        SubCanal subCanal;
        Vector<SubCanal> listaSubCanales = new Vector<SubCanal>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("SubCanales",
                    new String[]{"codigo", "nombre"},
                    "canal ='" + canal + "'",
                    null,
                    null,
                    null,
                    "codigo");

            if (cursor.moveToFirst()) {

                do {

                    subCanal = new SubCanal();

                    subCanal.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    subCanal.nombre = cursor.getString(cursor.getColumnIndex("nombre"));

                    listaSubCanales.addElement(subCanal);
                    items.addElement(subCanal.nombre);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaSubCanales", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaSubCanales;
    }


    public static boolean ExisteCodigotEnClientes(String codigo) {

        mensaje = "";
        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT *  " +
                    "FROM Clientes " +
                    "WHERE Codigo = '" + codigo + "' ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    existe = true;

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExisteCodigo", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    public static boolean ExisteCodigotEnClientesNuevos(String codigo) {

        mensaje = "";
        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT *  " +
                    "FROM ClientesNuevos " +
                    "WHERE Codigo = '" + codigo + "' ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    existe = true;

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExisteCodigo", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    public static Usuario CargarUsuario() {

        mensaje = "";
        Usuario usuario = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigo, nombre, fechaLabores, fechaConsecutivo, bodega, tipoventa AS tipoVenta,viajera,imprimedetallerecaudo  FROM Vendedor";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                usuario = new Usuario();
                usuario.codigoVendedor = cursor.getString(cursor.getColumnIndex("codigo"));
                usuario.nombreVendedor = cursor.getString(cursor.getColumnIndex("nombre"));
                usuario.fechaLabores = cursor.getString(cursor.getColumnIndex("fechaLabores"));
                usuario.fechaConsecutivo = cursor.getString(cursor.getColumnIndex("fechaConsecutivo"));
                usuario.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                usuario.tipoVenta = cursor.getString(cursor.getColumnIndex("tipoVenta"));
                usuario.viajero = cursor.getString(cursor.getColumnIndex("viajera"));
                usuario.impresionRecaudo = cursor.getString(cursor.getColumnIndex("imprimedetallerecaudo"));
                mensaje = "Cargo Informacion del Usuario Correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarUsuario", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return usuario;
    }


    public static boolean GuardarCodPdv(String codPDV, int tipoCliente) {

        SQLiteDatabase db = null;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("DELETE FROM PdvMovil");

            ContentValues values = new ContentValues();
            values.put("codigo", codPDV);
            values.put("tipoCliente", tipoCliente);
            db.insertOrThrow("PdvMovil", null, values);

            Log.i("GuardarCodPdv", "Guardor PDV: " + codPDV);
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("DataBaseBO - GuardarCodPdv", mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static int ObtenerTipoClienteSeleccionado() {

        mensaje = "";

        int tipoClienteSeleccionado = 1;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT tipoCliente  FROM PdvMovil Limit 1 ";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {


                tipoClienteSeleccionado = cursor.getInt(cursor.getColumnIndex("tipoCliente"));


                mensaje = "Cargo Informacion del Tipo Cliente Seleccionado";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenerTipoClienteSeleccionado", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return tipoClienteSeleccionado;
    }


    public static Cliente CargarClienteSeleccionado() {

        Cliente cliente = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = ""
                    + "SELECT DISTINCT Clientes.Codigo, Clientes.Nombre, Clientes.RazonSocial, Clientes.Direccion, "
                    + " Clientes.Telefono, Clientes.Nit, Clientes.Ciudad, Clientes.CodigoAmarre, Clientes.Canal, Clientes.SubCanal, "
                    + " Clientes.Cupo, Clientes.TipoCredito, Clientes.DANE, Clientes.Bloqueado, Clientes.tipologia2, Clientes.diasIngreso, "
                    + "Clientes.bodega,Clientes.cliacummesbon, Clientes.portafolio  "
                    + "FROM Clientes "
                    + " INNER JOIN PdvMovil ON Clientes.codigo = PdvMovil.Codigo ";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                cliente = new Cliente();
                cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
                cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
                cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
                cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
                cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
                cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
                cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
                cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2"));
                cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));
                cliente.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                cliente.AcomuladoBonif = cursor.getString(cursor.getColumnIndex("cliacummesbon"));
                cliente.portafolio = cursor.getString(cursor.getColumnIndex("portafolio"));
                mensaje = "Cliente Viejo Cargado Correctamente";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cliente;
    }

    public static Cliente CargarClienteNuevoSeleccionado() {

        Cliente cliente = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "SELECT DISTINCT ClientesNuevos.Codigo, ClientesNuevos.Nombre, ClientesNuevos.RazonSocial," +
                    " ClientesNuevos.Direccion, ClientesNuevos.Telefono, ClientesNuevos.Nit, ClientesNuevos.Ciudad, " +
                    " ClientesNuevos.CodigoAmarre, ClientesNuevos.Canal, ClientesNuevos.SubCanal, " +
                    " ClientesNuevos.Cupo, ClientesNuevos.TipoCredito " +
                    " FROM ClientesNuevos " +
                    " INNER JOIN PdvMovil ON ClientesNuevos.codigo = PdvMovil.Codigo ";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                cliente = new Cliente();


                cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion"));
                cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono"));
                cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit"));
                cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad"));
                cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));
                cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal"));
                cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal"));
                cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito"));
                cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                cliente.AcomuladoBonif = "0";
                cliente.tipologia = "";


                mensaje = "Cliente Nuevo Cargado Correctamente";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargaDeClienteNuevo", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cliente;
    }


    public static double obtenerAnticipo() {

        mensaje = "";
        double valorAnticipo = 0.0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT valor FROM detallegastos WHERE  concepto= '0000' ", null);

            if (cursor.moveToFirst()) {

                do {


                    double valAux = cursor.getDouble(cursor.getColumnIndex("Valor"));
                    valorAnticipo = valorAnticipo + valAux;


                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosCambio", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return valorAnticipo;
    }


    public static boolean ActualizarAnticipo(double valor) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {


            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            db.execSQL("Update detallegastos set Anticipo ='" + valor + "'");
            dbTemp.execSQL("Update detallegastos set Anticipo ='" + valor + "'");

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            //Log.e(TAG, "ActualizarSyncPedidos: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static String obtenerNumeroDocGastosViaje() {

        mensaje = "";
        String numeroDoc = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT nrodoc FROM detallegastos", null);

            if (cursor.moveToFirst()) {

                do {


                    numeroDoc = cursor.getString(cursor.getColumnIndex("nrodoc"));
                    break;


                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosCambio", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return numeroDoc;
    }


    public static String ObtenterNumeroDocTiempos(String codVendedor) {

        String num = "-";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            int consecutivo = 0;
            Cursor cursor = db.rawQuery("SELECT consecutivo FROM Vendedor", null);

            if (cursor.moveToFirst())
                consecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));

            if (cursor != null)
                cursor.close();

            consecutivo = (consecutivo + 1) % 200;
            db.execSQL("UPDATE Vendedor SET consecutivo = " + consecutivo);

            if (cursor != null)
                cursor.close();

            //A+vendedor+anomesdia+consecutivo(4)

            num = "9" + codVendedor + Util.FechaActual("yyyyMMdd") + Util.lpad("" + consecutivo, 4, "0");
            return num;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenterNumeroDoc", mensaje, e);
            return "-";

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static Vector<MotivoCompra> ListaMotivosAnulacion(Vector<String> items) {

        SQLiteDatabase db = null;
        MotivoCompra motivoCompra;
        Vector<MotivoCompra> listaMotivos = new Vector<MotivoCompra>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.query("MotivosAnulacion",
                    new String[]{"Codigo", "Nombre"},
                    null,
                    null,
                    null,
                    null,
                    "Codigo");

            if (cursor.moveToFirst()) {

                do {

                    motivoCompra = new MotivoCompra();

                    motivoCompra.codigo = Util.ToInt(cursor.getString(cursor.getColumnIndex("Codigo")));
                    motivoCompra.motivo = cursor.getString(cursor.getColumnIndex("Nombre"));

                    listaMotivos.addElement(motivoCompra);
                    items.addElement(motivoCompra.motivo);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosAnulacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaMotivos;
    }


    public static boolean existeNumeroDeRecibo(String numeroRecibo) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select * from anulacionrecibos where nrorecibo='" + numeroRecibo + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    public static boolean existeNumeroDeRecibo2(String numeroRecibo) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select * from consignaciones where recibo='" + numeroRecibo + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    public static boolean GuardarAnulacionDeRecibos(String vendedor, String bodega, String nroRecibo, String fecha, String motivo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values.put("vendedor", vendedor);
            values.put("Bodega", bodega);
            values.put("NroRecibo", nroRecibo);
            values.put("fecha", fecha);
            values.put("Motivo", motivo);


            db.insertOrThrow("AnulacionRecibos", null, values);
            dbPedido.insertOrThrow("AnulacionRecibos", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarAnulacionDeRecibos: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static Vector<DetalleAnulacionDeRecibos> listaDetalleAnulacionRecibos() {

        mensaje = "";
        SQLiteDatabase db = null;

        DetalleAnulacionDeRecibos detalleAnulacionRecibos = new DetalleAnulacionDeRecibos();
        Vector<DetalleAnulacionDeRecibos> listaDetalleAnulacionRecibos = new Vector<DetalleAnulacionDeRecibos>();

        try {

            File dbFile = new File(Util.DirApp(), "Temp.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT AnulacionRecibos.NroRecibo, AnulacionRecibos.Motivo FROM AnulacionRecibos";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalleAnulacionRecibos = new DetalleAnulacionDeRecibos();


                    detalleAnulacionRecibos.nroRecibo = cursor.getString(cursor.getColumnIndex("NroRecibo"));
                    detalleAnulacionRecibos.nombre = cursor.getString(cursor.getColumnIndex("Motivo"));

                    listaDetalleAnulacionRecibos.addElement(detalleAnulacionRecibos);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaDetalleAnulacionRecibos - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalleAnulacionRecibos;
    }


    public static String getMotivo(String codigoMotivo) {

        String nombre = "";
        SQLiteDatabase db = null;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT Nombre FROM MotivosAnulacion where Codigo = '" + codigoMotivo + "'", null);

            if (cursor.moveToFirst())
                nombre = cursor.getString(cursor.getColumnIndex("Nombre"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return nombre;
    }


    public static boolean EliminarDetalleAnulacionRecibos(DetalleAnulacionDeRecibos detalleAnulacionRecibo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String whereArgs[] = new String[]{"" + detalleAnulacionRecibo.nroRecibo};

            /*************************************************************
             * Se Elimina el Producto del pedido, en Detalle y TmpPedido
             ************************************************************/

            int rowDetalle = db.delete("AnulacionRecibos", "NroRecibo = ?", whereArgs);
            int rowsDetalleTmp = dbPedido.delete("AnulacionRecibos", "NroRecibo = ?", whereArgs);

            Log.i("EliminarDetalleAnulacionRecibos", "Nro Recibo = " + detalleAnulacionRecibo.nroRecibo);
            return rowDetalle > 0 && rowsDetalleTmp > 0;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean eliminarTodosGastosDeViaje() {

        boolean resultado = false;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM detallegastos");

                dbPedido.execSQL("DELETE FROM detallegastos");

                resultado = true;

            } else {

                Log.e(TAG, "BorrarDetallegastos: No existe la Base de Datos");
            }

        } catch (Exception e) {

            resultado = false;
            mensaje = e.getMessage();
            Log.e(TAG, "eliminarTodosGastosDeViaje: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }

        return resultado;

    }


    public static boolean ExisteDetalleGastosViaje() {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total  " +
                    "FROM detallegastos ";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getInt(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean ExisteAnticipoDetalleGastosViaje() {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total  " +
                    "FROM detallegastos where concepto ='0000' ";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getInt(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static Vector<Cliente> listadoClientesConCartera(boolean opcion) {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<Cliente> listadoClientesConCartera = new Vector<Cliente>();
        Cliente cliente;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            String query = "";

            if (opcion) {

                query = "  SELECT Clientes.Codigo, Clientes.NOMBRE,SUM(SALDO) as saldo,count(1) as documentos  FROM Cartera INNER JOIN Clientes ON "
                        + "  Cartera.Cliente = Clientes.Codigo where IFNULL((JULIANDAY(CURRENT_DATE) - JULIANDAY(FechaVencimiento)), -1)  < 0 GROUP BY Clientes.Codigo, Clientes.Nombre  ";

            } else {

                query = "SELECT  Clientes.Codigo, Clientes.NOMBRE,SUM(SALDO) as saldo,count(1) as documentos FROM Cartera INNER JOIN Clientes ON Cartera.Cliente = Clientes.Codigo  GROUP BY Clientes.Codigo, Clientes.Nombre ";

            }

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cliente = new Cliente();

                    cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo"));
                    cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                    cliente.saldo = cursor.getDouble(cursor.getColumnIndex("saldo"));
                    cliente.documentos = cursor.getInt(cursor.getColumnIndex("documentos"));

                    listadoClientesConCartera.add(cliente);

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("listadoClientesConCartera", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listadoClientesConCartera;
    }


    public static Cliente BuscarCliente(String CodigoCliente) {

        Cliente cliente = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            String query = "SELECT Codigo, Nombre, Direccion, Telefono, Nit, RazonSocial, Ciudad, CodigoAmarre, Canal, SubCanal," +
                    "Cupo, ruta_parada, OrdenRuta, TipoCredito, DANE, Bloqueado, fechaingreso, ordenreal, ifnull(tipologia, '') as tipologia, ifnull( tipologia2, '' ) as tipologia2," +
                    "diasIngreso, bodega, cliacummesbon " +
                    "FROM Clientes " +
                    "WHERE Codigo =  '" + CodigoCliente + "'";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {
                    cliente = new Cliente();
                    cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo")).trim();
                    cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre")).trim();
                    cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion")).trim();
                    cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono")).trim();
                    cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit")).trim();
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                    cliente.Ciudad = cursor.getString(cursor.getColumnIndex("Ciudad")).trim();
                    cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre")).trim();
                    cliente.Canal = cursor.getString(cursor.getColumnIndex("Canal")).trim();
                    cliente.SubCanal = cursor.getString(cursor.getColumnIndex("SubCanal")).trim();
                    cliente.Cupo = cursor.getLong(cursor.getColumnIndex("Cupo"));
                    cliente.rutaParada = cursor.getString(cursor.getColumnIndex("ruta_parada")).trim();
                    cliente.ordenRuta = cursor.getInt(cursor.getColumnIndex("OrdenRuta"));
                    cliente.formaPago = cursor.getString(cursor.getColumnIndex("TipoCredito")).trim();
                    cliente.DANE = cursor.getInt(cursor.getColumnIndex("DANE"));
                    cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado")).trim();
                    cliente.fechaIngreso = cursor.getString(cursor.getColumnIndex("fechaingreso")).trim();
                    cliente.ordenReal = cursor.getInt(cursor.getColumnIndex("ordenreal"));
                    cliente.tipologia2 = cursor.getString(cursor.getColumnIndex("tipologia")).trim();
                    cliente.tipologia = cursor.getString(cursor.getColumnIndex("tipologia2")).trim();
                    cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));
                    cliente.bodega = cursor.getString(cursor.getColumnIndex("bodega")).trim();
                    cliente.cliacummesbon = cursor.getInt(cursor.getColumnIndex("cliacummesbon"));

                } while (cursor.moveToNext());

                mensaje = "Busqueda de Clientes Satisfactoria";

            } else {

                mensaje = "Busqueda de Clientes sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }
        return cliente;
    }


    public static Cliente obtenerInfoClienteXCod(String nroDoc) {

        Cliente cliente = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            String query = "SELECT RazonSocial, Nombre, Direccion, Telefono, Codigo AS Nit FROM Clientes WHERE Codigo = (SELECT codigo FROM Encabezado WHERE numeroDoc = '"
                    + nroDoc + "')";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {
                    cliente = new Cliente();
                    cliente.Nombre = cursor.getString(cursor.getColumnIndex("Nombre")).trim();
                    cliente.direccion = cursor.getString(cursor.getColumnIndex("Direccion")).trim();
                    cliente.Telefono = cursor.getString(cursor.getColumnIndex("Telefono")).trim();
                    cliente.Nit = cursor.getString(cursor.getColumnIndex("Nit")).trim();
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial")).trim();
                    //					cliente.Ciudad       = cursor.getString(cursor.getColumnIndex("Ciudad")).trim();
                    //					cliente.CodigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre")).trim();
                    //					cliente.Canal        = cursor.getString(cursor.getColumnIndex("Canal")).trim();
                    //					cliente.SubCanal     = cursor.getString(cursor.getColumnIndex("SubCanal")).trim();
                    //					cliente.Cupo         = cursor.getLong(cursor.getColumnIndex("Cupo"));
                    //					cliente.rutaParada   = cursor.getString(cursor.getColumnIndex("ruta_parada")).trim();
                    //					cliente.ordenRuta    = cursor.getInt(cursor.getColumnIndex("OrdenRuta"));
                    //					cliente.formaPago    = cursor.getString(cursor.getColumnIndex("TipoCredito")).trim();
                    //					cliente.DANE         = cursor.getInt(cursor.getColumnIndex("DANE"));
                    //					cliente.Bloqueado    = cursor.getString(cursor.getColumnIndex("Bloqueado")).trim();
                    //					cliente.fechaIngreso = cursor.getString(cursor.getColumnIndex("fechaingreso")).trim();
                    //					cliente.ordenReal    = cursor.getInt(cursor.getColumnIndex("ordenreal"));
                    //					cliente.tipologia2    = cursor.getString(cursor.getColumnIndex("tipologia")).trim();
                    //					cliente.tipologia    = cursor.getString(cursor.getColumnIndex("tipologia2")).trim();
                    //					cliente.diasCreacion = cursor.getInt(cursor.getColumnIndex("diasIngreso"));
                    //					cliente.bodega       = cursor.getString(cursor.getColumnIndex("bodega")).trim();
                    //					cliente.cliacummesbon =  cursor.getInt(cursor.getColumnIndex("cliacummesbon"));

                } while (cursor.moveToNext());

                mensaje = "Busqueda de Clientes Satisfactoria";

            } else {

                mensaje = "Busqueda de Clientes sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }
        return cliente;
    }


    public static Vector<Consignacion> listaConsignaciones(String codigoVendedor) {

        mensaje = "";
        SQLiteDatabase db = null;

        Consignacion consignacion = new Consignacion();
        Vector<Consignacion> listaConsgnacines = new Vector<Consignacion>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = " SELECT recibo,codigobanco,sum(VALOR) as valor,sincronizadoandroid  FROM CONSIGNACIONES GROUP BY recibo,codigobanco";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    consignacion = new Consignacion();

                    consignacion.recibo = cursor.getString(cursor.getColumnIndex("recibo"));
                    consignacion.codigoBanco = cursor.getString(cursor.getColumnIndex("codigobanco"));
                    consignacion.valor = cursor.getDouble(cursor.getColumnIndex("valor"));
                    consignacion.sincronizado = cursor.getString(cursor.getColumnIndex("sincronizadoandroid"));
                    listaConsgnacines.addElement(consignacion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaConsignaciones - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaConsgnacines;
    }


    public static boolean GuardarConsignacion(Consignacion consignacion) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            ContentValues values = new ContentValues();
            values.put("recibo", consignacion.recibo);
            values.put("vendedor", consignacion.vendedor);
            values.put("fecha", fechaActual);
            values.put("codigobanco", consignacion.codigoBanco);
            values.put("cuentabanco", consignacion.cuentaBanco);
            values.put("formapago", consignacion.formaPago);
            values.put("valor", consignacion.valor);
            values.put("nro_cheque", consignacion.numeroCheque);
            values.put("fechagrabo", consignacion.fechaGrabo);
            values.put("fechabanco", consignacion.fechaBanco);
            values.put("fechabanco", consignacion.fechaBanco);
            values.put("nrodoc", consignacion.consecutivo);
            values.put("sincronizado", "0");
            values.put("cargues", "");
            values.put("sincronizadoandroid", "0");
            values.put("formapago", "1");


            db.insertOrThrow("consignaciones", null, values);
            dbPedido.insertOrThrow("consignaciones", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "consignaciones: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static boolean validarNroConsignacion(String nroRecibo) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select recibo from consignaciones where recibo='" + nroRecibo + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    public static boolean eliminarConsignacion(String numeroRecibo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;
        int filasAfectadas = 0;
        int filasAfectadas2 = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                filasAfectadas = db.delete("consignaciones", "recibo = ?", new String[]{numeroRecibo});
                filasAfectadas2 = dbPedido.delete("consignaciones", "recibo = ?", new String[]{numeroRecibo});


            } else {

                Log.e(TAG, "eliminarConsignacion: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "eliminarConsignacion: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }

        if (filasAfectadas2 > 0)
            return true;
        else
            return false;

    }

    public static Vector<Ruta> ListaRutas(Vector<String> items) {

        SQLiteDatabase db = null;
        Ruta ruta;
        Vector<Ruta> listaRutas = new Vector<Ruta>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select SUBSTR(ruta_parada,3,3) as codigo from clientes  group by SUBSTR(ruta_parada,3,3)";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    ruta = new Ruta();

                    ruta.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    ruta.descripcion = ruta.codigo;

                    listaRutas.addElement(ruta);
                    items.addElement(ruta.descripcion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaRuta", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaRutas;
    }


    public static VendedorBonificacion ExisteBonificacionVendedor() {

        mensaje = "";
        SQLiteDatabase db = null;
        VendedorBonificacion vendedorBonificacion = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select zcod from vendedorbonificacion";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    vendedorBonificacion = new VendedorBonificacion();
                    vendedorBonificacion.zcod = cursor.getString(cursor.getColumnIndex("zcod"));


                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return vendedorBonificacion;
    }


    public static TopeBonificacion aplicaBonificacionVendedor(String tipologia) {

        mensaje = "";
        SQLiteDatabase db = null;
        TopeBonificacion topeBonificacion = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select clase_abc,tope,zcod  from topebonificaciones where clase_abc = '" + tipologia + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    topeBonificacion = new TopeBonificacion();

                    topeBonificacion.clase_abc = cursor.getString(cursor.getColumnIndex("clase_abc"));
                    topeBonificacion.tope = cursor.getDouble(cursor.getColumnIndex("tope"));
                    topeBonificacion.zcod = cursor.getString(cursor.getColumnIndex("zcod"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();


        } finally {

            if (db != null)
                db.close();
        }

        return topeBonificacion;
    }


    public static String tipoGrupo(String codGrupo) {

        mensaje = "";
        SQLiteDatabase db = null;
        String tipo = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select tipo from detallebonificaciones where cod_grupo = '" + codGrupo + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    tipo = cursor.getString(cursor.getColumnIndex("tipo"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();


        } finally {

            if (db != null)
                db.close();
        }

        return tipo;
    }


    public static CondicionesBonificacion condicionesBonificaciones(String vendedorTerminal, String codigoCliente, String auxCanal, String bodega) {


        mensaje = "";
        SQLiteDatabase db = null;
        CondicionesBonificacion condicionesBonificacion = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "select zcod ,encabezadobonificaciones.descripcion as promocion,productos.codigo,productos.nombre as material  from encabezadobonificaciones inner join productos on encabezadobonificaciones.matnr=productos.codigo and zcod in ( ";
            query = query + " select zcod from condicionesbonificaciones where opcion='EQ' and campo='vendedor' and low='" + vendedorTerminal + "' union ";
            query = query + " select zcod from condicionesbonificaciones where opcion='BT' and campo='vendedor' and '" + vendedorTerminal + "'<=high  and '" + vendedorTerminal + "'>=low union ";
            query = query + " select zcod from condicionesbonificaciones where opcion='EQ' and campo='cliente' and low='" + codigoCliente + "' union ";
            query = query + " select zcod from condicionesbonificaciones where opcion='BT' and campo='cliente' and '" + codigoCliente + "'<=high  and '" + codigoCliente + "'>=low union ";
            query = query + " select zcod from condicionesbonificaciones where opcion='EQ' and campo='canal' and low='" + auxCanal + "' union ";
            query = query + " select zcod from condicionesbonificaciones where opcion='BT' and campo='canal' and '" + auxCanal + "'<=high  and '" + auxCanal + "'>=low union ";
            query = query + " select zcod from condicionesbonificaciones where opcion='EQ' and campo='agencia' and low='" + bodega + "' union ";
            query = query + " select zcod from condicionesbonificaciones where opcion='BT' and campo='agencia' and '" + bodega + "'<=high  and '" + bodega + "'>=low ";
            query = query + " ) ";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    condicionesBonificacion = new CondicionesBonificacion();
                    condicionesBonificacion.codigo = cursor.getString(cursor.getColumnIndex("codigo"));


                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return condicionesBonificacion;


    }


    public static boolean guardarCoordenada(Coordenada coordenada) {

        long rows = 0;
        SQLiteDatabase dbTemp = null;

        try {

            File fileTemp = new File(Util.DirApp(), "Temp.db");

            if (fileTemp.exists()) {

                dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                ContentValues values = new ContentValues();
                values = new ContentValues();


                if (coordenada != null) {

                    values = new ContentValues();

                    values.put("CodigoVendedor", coordenada.codigoVendedor);
                    values.put("CodigoCliente", coordenada.codigoCliente);
                    values.put("latitud", coordenada.latitud);
                    values.put("longitud", coordenada.longitud);
                    values.put("horaCoordenada", coordenada.horaCoordenada);
                    values.put("id", coordenada.id);
                    values.put("estado", coordenada.estado);
                    values.put("fecha", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));


                    rows = dbTemp.insertOrThrow("Coordenadas", null, values);
                }


                return rows > 0;

            } else {

                //Log.e(TAG, "guardarCoordenada -> " + Msg.NO_EXISTE_BD);
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            //Log.e(TAG, "guardarCoordenada -> " +  mensaje, e);
            return false;

        } finally {

            if (dbTemp != null)
                dbTemp.close();
        }
    }

    public static boolean guardarCoordenadaCliente(Coordenada coordenada) {

        long rows = 0;
        boolean existe = false;
        SQLiteDatabase dbTemp = null;

        try {

            File fileTemp = new File(Util.DirApp(), "Temp.db");

            if (fileTemp.exists()) {

                if (coordenada.codigoCliente != null) {

                    dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    String query = "SELECT CodigoCliente FROM Coordenadas WHERE CodigoCliente = '" + coordenada.codigoCliente + "'";
                    Cursor cursor = dbTemp.rawQuery(query, null);

                    if (cursor.moveToFirst())
                        existe = true;

                    if (cursor != null)
                        cursor.close();

                    if (existe) {

                        ContentValues values = new ContentValues();
                        values.put("CodigoVendedor", coordenada.codigoVendedor);

                        values.put("latitud", coordenada.latitud);
                        values.put("longitud", coordenada.longitud);
                        values.put("horaCoordenada", coordenada.horaCoordenada);
                        values.put("id", coordenada.id);

                        rows = dbTemp.update("Coordenadas", values, "CodigoCliente = ?", new String[]{coordenada.codigoCliente});

                    } else {

                        ContentValues values = new ContentValues();
                        values = new ContentValues();
                        values.put("CodigoVendedor", coordenada.codigoVendedor);
                        values.put("CodigoCliente", coordenada.codigoCliente);
                        values.put("latitud", coordenada.latitud);
                        values.put("longitud", coordenada.longitud);
                        values.put("sincronizado", coordenada.sincronizado);
                        values.put("bandera", coordenada.bandera);
                        values.put("horaCoordenada", coordenada.horaCoordenada);
                        values.put("id", coordenada.id);
                        values.put("fecha", coordenada.fecha);

                        rows = dbTemp.insertOrThrow("Coordenadas", null, values);
                    }

                    return rows > 0;

                } else {

                    //Log.e(TAG, "guardarCoordenadaCliente -> El codigo del cliente es NULL");
                    return false;
                }

            } else {

                //Log.e(TAG, "guardarCoordenadaCliente -> " +  Msg.NO_EXISTE_BD);
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            //Log.e(TAG, "guardarCoordenadaCliente -> " +  mensaje, e);
            return false;

        } finally {

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static Vector<DescuentoAutorizado> BuscarDescuentosAutorizados(String codCliente, String codVendedor) {

        DescuentoAutorizado descuentoAutorizado;
        SQLiteDatabase db = null;

        Vector<DescuentoAutorizado> listaDescuentosAutorizados = new Vector<DescuentoAutorizado>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query =

                    " SELECT bodega,porcentaje, vendedor, codigocliente, material, materialcompleto, cantidad, valor, " +
                            " autorizadopor, fechacarga, numerodoc, DescuentosAutorizados.precio, sincronizado, sincronizadoandroid ,Productos.nombre " +
                            " FROM DescuentosAutorizados  inner join Productos on productos.codigo  = DescuentosAutorizados.material " +
                            " WHERE codigocliente = '" + codCliente + "' and vendedor = '" + codVendedor + "'";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    descuentoAutorizado = new DescuentoAutorizado();

                    descuentoAutorizado.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    descuentoAutorizado.vendedor = cursor.getString(cursor.getColumnIndex("vendedor"));
                    descuentoAutorizado.codigocliente = cursor.getString(cursor.getColumnIndex("codigocliente"));
                    descuentoAutorizado.material = cursor.getString(cursor.getColumnIndex("material"));
                    descuentoAutorizado.material = cursor.getString(cursor.getColumnIndex("materialcompleto"));
                    descuentoAutorizado.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    descuentoAutorizado.valor = cursor.getDouble(cursor.getColumnIndex("valor"));
                    descuentoAutorizado.autorizadopor = cursor.getString(cursor.getColumnIndex("autorizadopor"));
                    descuentoAutorizado.fecha = cursor.getString(cursor.getColumnIndex("fechacarga"));
                    descuentoAutorizado.numerodoc = cursor.getString(cursor.getColumnIndex("numerodoc"));
                    descuentoAutorizado.precio = cursor.getDouble(cursor.getColumnIndex("precio"));
                    descuentoAutorizado.sincronizado = cursor.getInt(cursor.getColumnIndex("sincronizado"));
                    descuentoAutorizado.sincronizadoandroid = cursor.getInt(cursor.getColumnIndex("sincronizadoandroid"));
                    descuentoAutorizado.descripcionproducto = cursor.getString(cursor.getColumnIndex("nombre"));
                    descuentoAutorizado.porcentaje = cursor.getInt(cursor.getColumnIndex("porcentaje"));

                    listaDescuentosAutorizados.addElement(descuentoAutorizado);


                } while (cursor.moveToNext());

                mensaje = "Busqueda de Descuentos Autorizados Satisfactoria";

            } else {

                mensaje = "Busqueda de Descuentos Autorizados sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaDescuentosAutorizados;
    }


    public static boolean GuardarProgramacionPago(String vendedor, String documento, String fecha, String cliente, String numerodoc, String observacion) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("vendedor", vendedor);
            values.put("documento", documento);
            values.put("fecha", fecha);
            values.put("cliente", cliente);
            values.put("numerodoc", numerodoc);
            values.put("fechaingreso", fechaActual);
            values.put("observacion", observacion);
            values.put("sincronizadoandroid", "0");

            db.insertOrThrow("programacionpago", null, values);
            dbTemp.insertOrThrow("programacionpago", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarProgramacionPago: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static Vector<InformeRecaudo> ListaInformeDeRecaudo() {

        mensaje = "";
        SQLiteDatabase db = null;

        InformeRecaudo informeRecaudo = new InformeRecaudo();
        Vector<InformeRecaudo> listaInformeDeRecaudo = new Vector<InformeRecaudo>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT DISTINCT encabezadorecaudo.nrodoc, encabezadorecaudo.CodigoCliente,clientes.razonsocial,encabezadorecaudo.total,"
                    + "fecha_recaudo FROM encabezadorecaudo " +
                    " inner join clientes on encabezadorecaudo.codigocliente=clientes.codigo  "
                    + "WHERE encabezadorecaudo.nrodoc in (select Nrodoc from detallerecaudo) "
                    + "order by fecha_recaudo desc ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    informeRecaudo = new InformeRecaudo();


                    informeRecaudo.nroDoc = cursor.getString(cursor.getColumnIndex("nroDoc"));
                    informeRecaudo.codCliente = cursor.getString(cursor.getColumnIndex("CodigoCliente"));
                    informeRecaudo.nombre = cursor.getString(cursor.getColumnIndex("RazonSocial"));
                    informeRecaudo.valor = cursor.getFloat(cursor.getColumnIndex("Total"));
                    informeRecaudo.fecha = cursor.getString(cursor.getColumnIndex("Fecha_recaudo"));

                    listaInformeDeRecaudo.addElement(informeRecaudo);

                } while (cursor.moveToNext());

                mensaje = "Informe De Recaudo Cargado con Exito";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaInformeDeRecaudo;
    }


    public static InformacionFP cargarInformacionFormasPago() {

        mensaje = "";
        SQLiteDatabase db = null;
        InformacionFP informacionFP = new InformacionFP();
        int fp = 0;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT FORMA_PAGO,SUM(MONTO) AS MONTO FROM FORMAPAGO "
                    + "WHERE nroDoc in (SELECT NroDoc from DetalleRecaudo)"
                    + "GROUP BY FORMA_PAGO";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {


                do {

                    fp = cursor.getInt(cursor.getColumnIndex("forma_pago"));

                    if (fp == 1) {

                        informacionFP.efectivo = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                    } else {

                        if (fp == 2) {

                            informacionFP.ch = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                        } else {

                            if (fp == 4) {


                                informacionFP.cp = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                            } else {

                                informacionFP.consignaciones = cursor.getFloat(cursor.getColumnIndex("MONTO"));
                            }


                        }


                    }

                } while (cursor.moveToNext());

                mensaje = "Informe De Recaudo Cargado con Exito";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return informacionFP;
    }


    public static int obtenerTotalDeVisitas() {

        mensaje = "";
        SQLiteDatabase db = null;
        int totalVisitas = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(1) AS totalVisitas FROM (SELECT COUNT(DISTINCT 1) FROM Encabezado where tipoTrans <> 90 GROUP BY codigo HAVING COALESCE(anulado,0) = 0)";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {


                do {

                    totalVisitas = cursor.getInt(cursor.getColumnIndex("totalVisitas"));

                } while (cursor.moveToNext());

                mensaje = "";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return totalVisitas;
    }


    public static Vector<Banco> ListaBancos2(Vector<String> items, String busqueda) {

        mensaje = "";
        SQLiteDatabase db = null;
        Vector<Banco> listaBancos = new Vector<Banco>();

        try {

            Banco banco;
            listaBancos = new Vector<Banco>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
	    /*
			Cursor cursor = db.query("Bancos",
									 new String[] { "codigo", "nombre", "htkid" },
									 null,
									 null,
									 null,
									 null,
									 "codigo");*/

            String sql = "select * from bancos  WHERE cuenta like '%" + busqueda + "%' order by Codigo";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    banco = new Banco();
                    banco.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    banco.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    banco.htkid = cursor.getString(cursor.getColumnIndex("htkid"));
                    banco.cuenta = cursor.getString(cursor.getColumnIndex("cuenta"));
                    listaBancos.addElement(banco);
                    items.addElement(banco.nombre);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ListaBancos: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaBancos;
    }

    public static boolean CrearInfoTemp() {
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
            String config = "CREATE TABLE IF NOT EXISTS javaobject (javaObject longblob) ";
            db.execSQL(config);

            mensaje = "Esquema Creado Correctamente";
            return true;

        } catch (Exception e) {
            mensaje = e.getMessage();
            return false;
        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean saveObject(objeto obj) {

        long rows = 0;
        SQLiteDatabase dbTemp = null;

        try {

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);

            oos.writeObject(obj);
            oos.flush();
            oos.close();
            bos.close();

            byte[] data = bos.toByteArray();


            File fileTemp = new File(Util.DirApp(), "DataBase.db");

            if (fileTemp.exists()) {

                dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                ContentValues values = new ContentValues();
                values = new ContentValues();


                rows = dbTemp.insertOrThrow("javaobject", null, values);
                return rows > 0;

            } else {

                Log.e("DataBaseBO -->", "saveObject -> " + "No Existe Base de Datos");
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("DataBaseBO -->", "saveObject -> " + mensaje, e);
            return false;

        } finally {

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    public static objeto obtenerObjeto() {

        mensaje = "";
        objeto obj = null;
        SQLiteDatabase db = null;

        ByteArrayInputStream bais;

        ObjectInputStream ins;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "select javaObject from javaobject limit 1 ";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    //obj = new objeto();

                    //bais = new ByteArrayInputStream(rs.getBytes("javaObject"));
                    bais = new ByteArrayInputStream(cursor.getBlob(cursor.getColumnIndex("javaObject")));

                    ins = new ObjectInputStream(bais);

                    obj = (objeto) ins.readObject();
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e("Usuario Actual", "obtenerUsuario -> " + "No Existe DataBase");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("Usuario Actual", "obtenerUsuario -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return obj;
    }


    public static boolean borrarObjeto() {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("DELETE FROM javaobject");

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("DataBaseBO - GuardarCodPdv", mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    /**
     * Guardar en la tabla tmpdetallerecaudo las carteras que estan en la lista.
     *
     * @param imei
     * @param cartera, lista de carteras que seran guardadas en la tabla temporal.
     * @return
     */
    public static boolean guardarCarterasEnTablasTemporales(Vector<Cartera> cartera, String imei) {

        SQLiteDatabase db = null;
        boolean retorno = false;

        try {

            Usuario usuario = ObtenerUsuario();
            if (usuario == null) {

                mensaje = "No se pudo cargar la informacion del Usuario";
                retorno = false;
            }

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.beginTransaction();

            for (Cartera c : Main.cartera) {
                TransaccionBO.insertDetalleRecaudoTemporal(db, usuario, c, c.documento, c.documento, imei);
            }

            /****************************
             * Se Confirma la Transaccion
             ****************************/
            db.setTransactionSuccessful();

            retorno = true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "guardarRecaudo - > " + mensaje, e);
            retorno = false;

        } finally {

            closeDataBase(db);
        }
        return retorno;
    }


    public static Vector<RecibosRecaudo> BuscarRecibosDeRecaudo() {

        RecibosRecaudo recibosRecaudo;
        SQLiteDatabase db = null;

        Vector<RecibosRecaudo> listaRecibosRecaudo = new Vector<RecibosRecaudo>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select * from recibos where activo = 1";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    recibosRecaudo = new RecibosRecaudo();

                    recibosRecaudo.vendedor = cursor.getString(cursor.getColumnIndex("Vendedor"));
                    recibosRecaudo.limiteInferior = cursor.getInt(cursor.getColumnIndex("LimiteInferior"));
                    recibosRecaudo.limiteSuperior = cursor.getInt(cursor.getColumnIndex("LimiteSuperior"));
                    recibosRecaudo.actual = cursor.getInt(cursor.getColumnIndex("Actual"));
                    recibosRecaudo.serie = cursor.getString(cursor.getColumnIndex("Serie"));
                    recibosRecaudo.activo = cursor.getInt(cursor.getColumnIndex("Activo"));
                    recibosRecaudo.fechaActivacion = cursor.getString(cursor.getColumnIndex("FechaActivacion"));
                    recibosRecaudo.orden = cursor.getInt(cursor.getColumnIndex("Orden"));
                    recibosRecaudo.id = cursor.getInt(cursor.getColumnIndex("id"));


                    listaRecibosRecaudo.addElement(recibosRecaudo);


                } while (cursor.moveToNext());

                mensaje = "Busqueda de Descuentos Autorizados Satisfactoria";

            } else {

                mensaje = "Busqueda de Descuentos Autorizados sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaRecibosRecaudo;
    }


    public static RecibosRecaudo BuscarRecibo() {

        RecibosRecaudo recibosRecaudo = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select ifnull(min(actual),-1) as actual, min(limitesuperior) as limitesuperior,id from recibos where activo=1";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                recibosRecaudo = new RecibosRecaudo();

                recibosRecaudo.limiteSuperior = cursor.getInt(cursor.getColumnIndex("limitesuperior"));
                recibosRecaudo.actual = cursor.getInt(cursor.getColumnIndex("actual"));
                recibosRecaudo.id = cursor.getInt(cursor.getColumnIndex("id"));

                if (recibosRecaudo.actual == -1)
                    recibosRecaudo = null;


            } else {


            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {


        } finally {

            if (db != null)
                db.close();
        }

        return recibosRecaudo;
    }


    public static void ActualizarRecibo(int limiteSuperior, int actual) {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("Update recibos set activo=0 where limitesuperior='" + limiteSuperior + "' and actual='" + actual + "'");

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean guardarRecaudo2(String deviceId, String nroRecibo, String consecutivo, RecibosRecaudo reciboRecaudo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            Usuario usuario = ObtenerUsuario();
            if (usuario == null) {

                mensaje = "No se pudo cargar la informacion del Usuario";
                return false;
            }

            String nroDoc = obtenterNumeroDocRecaudo(consecutivo);

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File fileTemp = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.beginTransaction();
            dbTemp.beginTransaction();

            TransaccionBO.insertEncabezadoRecaudo(db, usuario, nroDoc, nroRecibo, deviceId);
            TransaccionBO.insertEncabezadoRecaudo(dbTemp, usuario, nroDoc, nroRecibo, deviceId);

            for (Cartera cartera : Main.cartera) {

                TransaccionBO.insertDetalleRecaudo(db, usuario, cartera, nroDoc, nroRecibo, deviceId);
                TransaccionBO.insertDetalleRecaudo2(dbTemp, usuario, cartera, nroDoc, nroRecibo, deviceId);
            }

            Enumeration<FormaPago> e = Main.listaFormasPago.elements();

            while (e.hasMoreElements()) {

                FormaPago formaPago = e.nextElement();

                TransaccionBO.insertFormaPago(db, usuario, formaPago, nroDoc, deviceId);
                TransaccionBO.insertFormaPago(dbTemp, usuario, formaPago, nroDoc, deviceId);
            }

            TransaccionBO.actualizarConsecutivo(db, reciboRecaudo);


            ConsecutivosRecibos consecutivosRecibos = new ConsecutivosRecibos();
            consecutivosRecibos.id = reciboRecaudo.id;
            consecutivosRecibos.nrorecibo = reciboRecaudo.actual;
            consecutivosRecibos.sincronizado = 0;
            consecutivosRecibos.sincronizadoandroid = 0;
            consecutivosRecibos.vendedor = Main.usuario.codigoVendedor;

            TransaccionBO.insertConsecutivoRecibos(db, consecutivosRecibos);
            TransaccionBO.insertConsecutivoRecibos(dbTemp, consecutivosRecibos);

            /****************************
             * Se Confirma la Transaccion
             ****************************/
            db.setTransactionSuccessful();
            dbTemp.setTransactionSuccessful();

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "guardarRecaudo - > " + mensaje, e);
            return false;

        } finally {

            closeDataBase(db);
            closeDataBase(dbTemp);
        }
    }


    public static void ActualizarConsecutivoReciboDeRecaudo(String idTalonario) {

        SQLiteDatabase db = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            String updateConsecutivo = "update recibos set actual= actual+1 where id ="
                    + idTalonario + " ";

            db.execSQL(updateConsecutivo);

        } catch (Exception e) {

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean GuardarConsecutivosRecibos(ConsecutivosRecibos consecutivosRecibos) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("id", consecutivosRecibos.id);
            values.put("vendedor", consecutivosRecibos.vendedor);
            values.put("sincronizado", consecutivosRecibos.sincronizado);
            values.put("sincronizadoandroid", consecutivosRecibos.sincronizadoandroid);
            values.put("nrorecibo", consecutivosRecibos.nrorecibo);

            db.insertOrThrow("consecutivosrecibos", null, values);
            dbTemp.insertOrThrow("consecutivosrecibos", null, values);

            return true;

        } catch (Exception e) {

            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }

    //Encuestas

    public static Vector<Encuestas> ListaEncuestas(Vector<ItemListView> listaItems, int activoVendedor, String canal) {

        mensaje = "";
        Encuestas encuesta;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Encuestas> listaEncuestas = new Vector<Encuestas>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select e.cod_encuesta, e.titulo " +
                    "from encuestas e, canal_encuesta ce " +
                    "where e.cod_encuesta = ce.[Cod_encuesta]  AND  ce.[Vendedor] ='" + canal + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    encuesta = new Encuestas();
                    itemListView = new ItemListView();

                    encuesta.codigo = cursor.getString(cursor.getColumnIndex("cod_encuesta")).trim();
                    encuesta.titulo = cursor.getString(cursor.getColumnIndex("titulo"));

                    int codEnc = Util.ToInt(encuesta.codigo);

                    if (!respondioEncuesta(codEnc)) {

                        itemListView.titulo = encuesta.titulo;
                        listaItems.add(itemListView);
                        listaEncuestas.addElement(encuesta);
                    }


                } while (cursor.moveToNext());

                mensaje = "Encuestas Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaClientesRutero", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaEncuestas;
    }

    public static Vector<EncuestasPreguntas> ListaEncuestasPreguntas(int codEncuesta) {

        mensaje = "";
        EncuestasPreguntas encuestaPreguntas;
        SQLiteDatabase db = null;

        Vector<EncuestasPreguntas> listaEncuestasPreguntas = new Vector<EncuestasPreguntas>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select cod_pregunta, cod_encuesta, tipo, titulo " +
                    "from preguntas " +
                    "where cod_encuesta = " + String.valueOf(codEncuesta) + " " +
                    "order by cod_pregunta";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    encuestaPreguntas = new EncuestasPreguntas();

                    encuestaPreguntas.codPregunta = cursor.getInt(cursor.getColumnIndex("cod_pregunta"));
                    encuestaPreguntas.codEncuesta = cursor.getInt(cursor.getColumnIndex("cod_encuesta"));
                    encuestaPreguntas.tipoPregunta = cursor.getInt(cursor.getColumnIndex("tipo"));
                    encuestaPreguntas.titulo = cursor.getString(cursor.getColumnIndex("titulo"));
                    encuestaPreguntas.verificable = 0;

                    listaEncuestasPreguntas.addElement(encuestaPreguntas);

                } while (cursor.moveToNext());

                mensaje = "Encuestas Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaClientesRutero", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaEncuestasPreguntas;
    }

    public static Vector<EncuestasPreguntasParametros> ListaEncuestasPreguntasParametros(int codPregunta) {

        mensaje = "";
        EncuestasPreguntasParametros encuestaPreguntasParametros;
        SQLiteDatabase db = null;

        Vector<EncuestasPreguntasParametros> listaEncuestasPreguntasParametros = new Vector<EncuestasPreguntasParametros>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select cod_param, cod_pregunta, valor, etiqueta " +
                    "from param_preg " +
                    "where cod_pregunta = " + String.valueOf(codPregunta) + " " +
                    "order by cod_param";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    encuestaPreguntasParametros = new EncuestasPreguntasParametros();

                    encuestaPreguntasParametros.codParametro = cursor.getInt(cursor.getColumnIndex("cod_param"));
                    encuestaPreguntasParametros.codPregunta = cursor.getInt(cursor.getColumnIndex("cod_pregunta"));
                    encuestaPreguntasParametros.valor = cursor.getInt(cursor.getColumnIndex("valor"));
                    encuestaPreguntasParametros.etiqueta = cursor.getString(cursor.getColumnIndex("etiqueta"));

                    listaEncuestasPreguntasParametros.addElement(encuestaPreguntasParametros);

                } while (cursor.moveToNext());

                mensaje = "Encuestas Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaClientesRutero", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaEncuestasPreguntasParametros;
    }


    /**
     * metodoq ue guarda cada respuesta que decide un cliente.
     *
     * @param codEncuesta
     * @param codPregunta
     * @param codCliente
     * @param respuesta
     * @return
     */
    public static boolean GuardarRespuestas(int codEncuesta, int codPregunta, String codCliente, String respuesta) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;
        boolean finalizado = false;

        try {

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("cod_encuesta", codEncuesta);
            values.put("cod_pregunta", codPregunta);
            values.put("cod_cliente", codCliente);
            values.put("respuesta", respuesta); // 0 -> Pedido, 2 -> Cambio (PNC)
            values.put("vendedor", Main.usuario.codigoVendedor);
            values.put("tipocliente", "1");
            values.put("bodega", Main.usuario.bodega);

            //condicion de borrado.
            String where = "cod_encuesta=" + codEncuesta + " AND cod_pregunta=" + codPregunta + " AND cod_cliente='" + codCliente + "'";

            //borrar antes de insertar para evitar duplicidad de datos, al momento de devolver a la anterior pregunta.
            db.delete("Respuestas", where, null);
            dbTemp.delete("Respuestas", where, null);

            // insertar
            db.insertOrThrow("Respuestas", null, values);
            dbTemp.insertOrThrow("Respuestas", null, values);


            finalizado = true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarNoCompra: " + mensaje, e);
            finalizado = false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
        return finalizado;
    }


    /**
     * Metodo que permite borrar las respuestas que se han dado a una encuesta, esto es porque el usuario puede devolver a preguntas anteriores en la encuesta,
     * por lo cual se hace necesario borrar las respuestas que ha dejado almacenadas y asi evitar redundancia de datos y evitar que no se haga una encuesta por error
     * de informacion no valida almacenada.
     *
     * @param codEncuesta, encuesta que sera borrada.
     * @param codCliente,  cliente que tiene la encuesta disponible.
     * @return el numero de datos que fueron borrados. -1 si los datos borrados no fueron los mismos en las dos bases de datos (Rollback).
     * @by JICZ.
     */
    public static int borrarRespuestasEncuesta(int codEncuesta, String codCliente) {
        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;
        int datosBorrados = 0;

        try {
            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.beginTransaction();
            dbTemp.beginTransaction();

            // condicion de borrado.
            String where = "cod_encuesta=" + codEncuesta + " AND cod_cliente='" + codCliente + "'";

            //borrar datos insertados para evitar duplicidad de datos.
            int x = db.delete("Respuestas", where, null);
            int y = dbTemp.delete("Respuestas", where, null);

            // si el numero de datos borrados no coincide retornar -1;
            if (x != y) {
                datosBorrados = -1;
            } else {
                if (x == y) {
                    datosBorrados = x;
                    db.setTransactionSuccessful();
                    dbTemp.setTransactionSuccessful();
                }
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "GuardarNoCompra: " + mensaje, e);
        } finally {
            closeDataBase(db);
            closeDataBase(dbTemp);
        }
        return datosBorrados;
    }


    public static boolean respondioEncuesta(int codEncuesta) {

        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT count(1) as total " +
                    "FROM Respuestas " +
                    "where cod_encuesta = " + codEncuesta + " and cod_cliente = '" + Main.cliente.codigo + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    /**
     * metodo que permite conocer el numero de encuestas que tiene disponible un canal.
     *
     * @return, el numero de encuestas disponibles.
     */
    public static int encuestasDisponibles(String cod_encuesta, String canal) {
        SQLiteDatabase db = null;
        int disponible = 0;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS disponible FROM  canal_encuesta ce WHERE  ce.[Cod_encuesta] = " + cod_encuesta + " "
                    + "AND ce.[Vendedor] = '" + canal + "';";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    disponible += cursor.getInt(cursor.getColumnIndex("disponible"));
                } while (cursor.moveToNext());
            }

            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }

        return disponible;
    }


    /**
     * metodo para obtener la cedula  y el codigo del vendedor. concatenados y separados por un (;)
     * por ejemplo: "41234345;304" siendo el 304 el codigo y el resto la cedula.
     *
     * @return codigo y cedula concatenados y separados por un (;)
     */
    public static String obtenerCedulaCodigoVendedor() {

        SQLiteDatabase db = null;
        String cedulaCodigo = null;
        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigo, cedula FROM vendedor;";

            Cursor cursor = db.rawQuery(query, null);

            // obtener el codigo y la cedula y concatenarlos.
            if (cursor.moveToFirst()) {
                do {
                    cedulaCodigo = cursor.getString(cursor.getColumnIndex("cedula")) + ";";
                    cedulaCodigo += cursor.getString(cursor.getColumnIndex("codigo"));
                } while (cursor.moveToNext());
            }

            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return cedulaCodigo;
    }


    /**
     * metodo que devuleve una oportunidad de ventas a un cliente determinado.
     *
     * @param codigo
     * @return
     */
    public static Oportunidad obtenerOportunidad(String codigo) {

        Oportunidad oportunidad = null;
        SQLiteDatabase db = null;
        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //generar consulta solo para obtener el primer registro capturado en la consulta.
            String query = "" +
                    "SELECT op.[codigo], op.[labuena], op.[campi], op.[gourmet], op.[oliosoya], op.[sol], op.[chocoexpress]," +
                    "       op.[lukafe], op.[aromapulverizado], op.[fluocardent], op.[tarritorojo] " +
                    "FROM [oportunidadclientes] op " +
                    "WHERE op.[codigo] = '" + codigo + "' LIMIT 1;";

            Cursor cursor = db.rawQuery(query, null);

            // obtener el codigo y la cedula y concatenarlos.
            if (cursor.moveToFirst()) {
                do {
                    oportunidad = new Oportunidad();
                    oportunidad.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    oportunidad.laBuena = cursor.getString(cursor.getColumnIndex("labuena"));
                    oportunidad.campi = cursor.getString(cursor.getColumnIndex("campi"));
                    oportunidad.gourmet = cursor.getString(cursor.getColumnIndex("gourmet"));
                    oportunidad.olioSoya = cursor.getString(cursor.getColumnIndex("oliosoya"));
                    oportunidad.sol = cursor.getString(cursor.getColumnIndex("sol"));
                    oportunidad.chocoExpress = cursor.getString(cursor.getColumnIndex("chocoexpress"));
                    oportunidad.lukafe = cursor.getString(cursor.getColumnIndex("lukafe"));
                    oportunidad.aromaPulverizado = cursor.getString(cursor.getColumnIndex("aromapulverizado"));
                    oportunidad.fluocardent = cursor.getString(cursor.getColumnIndex("fluocardent"));
                    oportunidad.tarritoRojo = cursor.getString(cursor.getColumnIndex("tarritorojo"));

                } while (cursor.moveToNext());
            }

            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return oportunidad;

    }


    /**
     * Retorna el primer producto encontrado que contenga la parte del codigo ingresado.
     *
     * @param codigoBusqueda
     * @return
     */
    public static String buscarProductoPorCodigo(String codigoBusqueda) {


        String codigo = null;
        SQLiteDatabase db = null;
        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //generar consulta solo para obtener el primer registro capturado en la consulta.
            String query = "" +
                    "select  Productos.codigo,nombre,indice  " +
                    "from productos where Productos.codigo like '" + codigoBusqueda + "%' order by codigo LIMIT 1";

            Cursor cursor = db.rawQuery(query, null);

            // obtener el codigo y la cedula y concatenarlos.
            if (cursor.moveToFirst()) {
                do {
                    codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                } while (cursor.moveToNext());
            }

            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }

            // si el valor leido es nulo o vacio, devolver el mismo codigo ingresado
            if (codigo == null || codigo.length() == 0) {
                codigo = codigoBusqueda;
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return codigo;
    }


    public static ArrayList<SeguimientoVenta> cargarSeguimientoVenta() {

        ArrayList<SeguimientoVenta> lista = new ArrayList<SeguimientoVenta>();
        SQLiteDatabase db = null;

        DecimalFormat numFormat = new DecimalFormat("'$' ###,###,###.00");

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            //generar consulta solo para obtener datos necesario de seguimiento
            String query = "" +
                    "SELECT  s.[categoriasalarial] AS categoria, s.[TOTAL] AS total " +
                    "FROM [seguimientoventas] s " +
                    "ORDER BY categoria ASC";

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    SeguimientoVenta seguimiento = new SeguimientoVenta();
                    seguimiento.categoria = cursor.getString(cursor.getColumnIndex("categoria"));
                    double total = cursor.getDouble(cursor.getColumnIndex("total"));
                    seguimiento.total = String.valueOf(numFormat.format(total));
                    lista.add(seguimiento);
                } while (cursor.moveToNext());
            }

            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return lista;
    }


    /**
     * @param codigoCliente
     * @param tipo
     * @param evento,       indica si es siguiente pagina. o anterior pagina
     * @param indice
     * @return
     */
    public static ArrayList<AgotadoYDevolucion> cargarAgotadoYDevoluciones(String codigoCliente, String tipo, boolean evento, int indice) {

        ArrayList<AgotadoYDevolucion> lista = new ArrayList<AgotadoYDevolucion>();
        SQLiteDatabase db = null;
        String consulta = (evento) ? ">" + indice + " " : "<" + indice + " AND rowid > (" + (indice - 14) + ")";

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            //generar consulta solo para obtener datos necesario de seguimiento
            String query = "" +
                    "SELECT DISTINCT ad.[codigo], ad.[fecha], ad.[producto], ad.[cantidad], ad.[motivo], ad.[tipo], ad.[mercaderista], rowid AS indice, " +
                    "(SELECT DISTINCT MAX(rowid) FROM [agotadosdevolucionesmercadeo] ad " +
                    " WHERE ad.[codigo] = '" + codigoCliente + "' AND ad.[tipo] = '" + tipo + "' AND rowid " + consulta + ") AS maximo " +
                    "FROM [agotadosdevolucionesmercadeo] ad " +
                    "WHERE ad.[codigo] = '" + codigoCliente + "' AND ad.[tipo] = '" + tipo + "' AND rowid " + consulta +
                    " ORDER BY rowid LIMIT 7";

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    AgotadoYDevolucion ad = new AgotadoYDevolucion();
                    ad.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    ad.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    ad.producto = cursor.getString(cursor.getColumnIndex("producto"));
                    ad.cantidad = cursor.getString(cursor.getColumnIndex("cantidad"));
                    ad.motivo = cursor.getString(cursor.getColumnIndex("motivo"));
                    ad.tipo = cursor.getString(cursor.getColumnIndex("tipo"));
                    ad.mercaderista = cursor.getString(cursor.getColumnIndex("mercaderista"));
                    ad.indice = cursor.getInt(cursor.getColumnIndex("indice"));
                    ad.maximo = cursor.getInt(cursor.getColumnIndex("maximo"));
                    lista.add(ad);
                } while (cursor.moveToNext());
            }
            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return lista;
    }


    /**
     * verificar cuantas devoluciones o agotados tiene un cliente.
     *
     * @param codigo
     * @return
     */
    public static int verificarAgotasYdevoluciones(String codigo) {
        int tiene = 0;
        SQLiteDatabase db = null;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            //generar consulta solo para obtener datos necesario de seguimiento
            String query = "" +
                    "SELECT COUNT(ad.[codigo]) AS tiene " +
                    "FROM [agotadosdevolucionesmercadeo] ad " +
                    "WHERE ad.[codigo] = '" + codigo + "' " +
                    "ORDER BY ad.[producto]";

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    tiene = cursor.getInt(cursor.getColumnIndex("tiene"));
                } while (cursor.moveToNext());
            }
            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return tiene;
    }


    /**
     * metodo para verificar si un cliente ya existe en la base de datos local.
     *
     * @param nit
     * @return Cliente si existe, null en caso contrario
     */
    public static Cliente verificarExistenciaClienteEnBD(String nit) {

        Cliente cliente = null;
        SQLiteDatabase db = null;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            //generar consulta solo para obtener datos necesario de seguimiento
            String query = "" +
                    "SELECT c.[RazonSocial], c.[Bloqueado] FROM [Clientes] c WHERE c.[Nit] = '" + nit + "';";

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    cliente = new Cliente();
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial"));
                    cliente.Bloqueado = cursor.getString(cursor.getColumnIndex("Bloqueado"));
                } while (cursor.moveToNext());
            }
            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return cliente;
    }


    public static ArrayList<Cliente> clientesConAgotadosYDevoluciones() {

        ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
        Cliente cliente = null;
        SQLiteDatabase db = null;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            String query = "" +
                    "SELECT c.[Codigo], c.[RazonSocial] " +
                    "FROM [agotadosdevolucionesmercadeo] agdm " +
                    "INNER JOIN [Clientes] c " +
                    "      ON c.[Codigo] = agdm.[codigo] " +
                    "GROUP BY agdm.[codigo]";

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    cliente = new Cliente();
                    cliente.codigo = cursor.getString(cursor.getColumnIndex("Codigo"));
                    cliente.razonSocial = cursor.getString(cursor.getColumnIndex("RazonSocial"));
                    listaCliente.add(cliente);
                } while (cursor.moveToNext());
            }
            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }
        return listaCliente;
    }


    /**
     * lista de opciones para ruta_parada desde (RU01 hasta RU05)
     *
     * @param rutas_parada
     */
    public static void cargarRutasParada(ArrayList<String> rutas_parada) {

        SQLiteDatabase db = null;
        if (rutas_parada == null) {
            rutas_parada = new ArrayList<String>(5); // cantidad inicial
        }

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            String query = "SELECT [diaVisita] FROM [Rutero] GROUP BY [diaVisita]";

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    String diaVisita = cursor.getString(cursor.getColumnIndex("diaVisita"));
                    rutas_parada.add(diaVisita);
                } while (cursor.moveToNext());
            }
            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {
            if (db != null)
                db.close();
        }
    }


    /**
     * guardar el cliente modificado en la tabla  clientesmod.
     *
     * @param cliente
     * @param vendedor
     * @return
     */
    public static boolean guardarClienteModificado(Cliente cliente, Usuario vendedor) {

        boolean modificado = false;
        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;
        ContentValues values = new ContentValues();

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File dbFileTemp = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(dbFileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            values.put("Codigo", cliente.codigo);
            values.put("Nombre", cliente.Nombre);
            values.put("Razonsocial", cliente.razonSocial);
            values.put("Nit", cliente.codigo);
            values.put("Direccion", cliente.direccion);
            values.put("Telefono", cliente.Telefono);
            values.put("Vendedor", vendedor.codigoVendedor);
            values.put("FechaIngreso", Util.FechaActual("yyyy-MM-dd HH:mm:ss.SSS"));
            values.put("Ruta_parada", cliente.rutaParada);
            values.put("Agencia", cliente.bodega);
            values.put("TipoCliente", 0);
            values.put("sincronizado", 0);

            db.beginTransaction();
            dbTemp.beginTransaction();

            long rowDB = db.insert("clientesmod", null, values);
            long rowTmp = dbTemp.insert("clientesmod", null, values);

            if (rowDB > 0 && rowTmp > 0) {
                db.setTransactionSuccessful();
                dbTemp.setTransactionSuccessful();
                modificado = true;
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {
            values.clear();
            closeDataBase(db);
            closeDataBase(dbTemp);
        }
        return modificado;
    }


    /**
     * eliminar clientes que pudieran estar repetidos por intentos de modificacion anteriores
     *
     * @param codigo, codigo del cliente a eliminar, s� existe en la tabla.
     */
    public static void elimiarClienteRepetidoModificado(String codigo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File dbFileTmp = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(dbFileTmp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String[] whereArgs = new String[]{codigo};

            db.delete("clientesmod", "Codigo=?", whereArgs);
            dbTemp.delete("clientesmod", "Codigo=?", whereArgs);

        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {
            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }
    }


    /**
     * cargar la lista de pedidos realizados hasta un momento determinado
     *
     * @param sincronizado
     * @return
     */
    public static ArrayList<Encabezado> cargarListaPedidos(boolean sincronizado) {

        SQLiteDatabase db = null;
        ArrayList<Encabezado> lista = new ArrayList<Encabezado>();

        //determinar si se listan los pedidos sincronizados o los no sincronizados.
        String condicion = sincronizado ? "e.[syncmobile] = 1" : "e.[syncmobile] = 0";

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            String query = "" +
                    "SELECT e.[codigo], e.[numeroDoc], e.[fechaReal], e.[tipoTrans], e.[fechaTrans], e. [montoFact], e.[desc1], e.[iva]," +
                    "e.[usuario], e.[fechaInicial], e.[fechaFinal], e. [observaciones], e.[bodega], e.[sincronizado], e.[entregado], e.[factura], " +
                    "e.[sincronizadomapas], e.[serial], e.[observaciones2], e.[fechaEntrega], e.[oc], e.[syncmobile], c.[RazonSocial] " +
                    "FROM [Encabezado] e " +
                    "INNER JOIN [Clientes] c " +
                    "      ON e.[codigo] = c.[Codigo] " +
                    "WHERE " + condicion + " " +
                    "ORDER BY e.[numeroDoc];";

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    Encabezado e = new Encabezado();
                    e.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo"));
                    e.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                    e.fechaReal = cursor.getString(cursor.getColumnIndex("fechaReal"));
                    e.tipoTrans = cursor.getInt(cursor.getColumnIndex("tipoTrans"));
                    e.fechaTrans = cursor.getString(cursor.getColumnIndex("fechaTrans"));
                    e.valor_neto = cursor.getFloat(cursor.getColumnIndex("montoFact"));
                    e.valor_descuento = cursor.getFloat(cursor.getColumnIndex("desc1"));
                    e.total_iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    e.usuario = cursor.getString(cursor.getColumnIndex("usuario"));
                    e.hora_inicial = cursor.getString(cursor.getColumnIndex("fechaInicial"));
                    e.hora_final = cursor.getString(cursor.getColumnIndex("fechaFinal"));
                    e.observacion = cursor.getString(cursor.getColumnIndex("observaciones"));
                    e.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    e.sincronizado = cursor.getInt(cursor.getColumnIndex("sincronizado"));
                    e.entregado = cursor.getInt(cursor.getColumnIndex("entregado"));
                    e.factura = cursor.getInt(cursor.getColumnIndex("factura"));
                    e.sincronizadoMapas = cursor.getInt(cursor.getColumnIndex("sincronizadomapas"));
                    e.serial = cursor.getString(cursor.getColumnIndex("serial"));
                    e.observacion2 = cursor.getString(cursor.getColumnIndex("observaciones2"));
                    e.fechaEntrega = cursor.getString(cursor.getColumnIndex("fechaEntrega"));
                    e.oc = cursor.getString(cursor.getColumnIndex("oc"));
                    e.syncmobile = cursor.getInt(cursor.getColumnIndex("syncmobile"));
                    e.razon_social = cursor.getString(cursor.getColumnIndex("RazonSocial"));

                    //agregar el encabezado
                    lista.add(e);
                } while (cursor.moveToNext());
            }
            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {
            closeDataBase(db);
        }
        return lista;
    }


    /**
     * metodo que carga la lista de detalles que pertenecen a un pedido.
     *
     * @param numero_doc
     * @return
     */
    public static ArrayList<Detalle> cargarDetallesDePedido(String numero_doc, boolean isAnulado) {

        SQLiteDatabase db = null;
        ArrayList<Detalle> lista = new ArrayList<Detalle>();
        String and = "";

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            String query = "";

	    /*if(isAnulado)
				and = " AND d.precio > 0 AND p.[saldo] >= d.[cantidad] ";*/

            if (isAnulado) {

                query = "SELECT        d.[numDoc], d.[codigoRef], d.[precio], d.[tarifaIva], d.[descuentoRenglon], " +
                        " d.[cantidad],       d.[vendedor], d.[motivo], d.[sincronizado], " +
                        " d.[cantidadcambio], d.[bodega], d.[item],        d.[tipocliente], d.[inventario], d.[fecha], p.[nombre] " +
                        " FROM [Detalle] d INNER JOIN [Productos] p ON d.[codigoRef] = p.[codigo] " +
                        " left join (select d.codigoref, sum(cantidad) cantidad from encabezado e inner join detalle d on e.numerodoc = d.numdoc " +
                        " where e.tipotrans = 0 and ifnull(e.anulado,0)=0 " +
                        " group by d.codigoref) ventas on p.codigo = ventas.codigoref " +
                        " WHERE d.[numDoc] = '" + numero_doc + "' AND d.precio > 0  " +
                        " and  (p.[saldo] - ifnull(ventas.cantidad,0)) >= d.[cantidad] ";


            } else {


                query = "" +
                        "SELECT " +
                        "       d.[numDoc], d.[codigoRef], d.[precio], d.[tarifaIva], d.[descuentoRenglon], d.[cantidad]," +
                        "       d.[vendedor], d.[motivo], d.[sincronizado], d.[cantidadcambio], d.[bodega], d.[item], " +
                        "       d.[tipocliente], d.[inventario], d.[fecha], p.[nombre]  " +
                        "FROM [Detalle] d " +
                        "INNER JOIN [Productos] p ON d.[codigoRef] = p.[codigo] " +
                        "WHERE d.[numDoc] = '" + numero_doc + "' " + and;


            }

            System.out.println("cargarDetallesDePedido ---<<< " + query);

            Cursor cursor = db.rawQuery(query, null);

            // obtener datos leidos
            if (cursor.moveToFirst()) {
                do {
                    Detalle d = new Detalle();
                    d.numDoc = cursor.getString(cursor.getColumnIndex("numDoc"));
                    d.codProducto = cursor.getString(cursor.getColumnIndex("codigoRef"));
                    d.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    d.iva = cursor.getFloat(cursor.getColumnIndex("tarifaIva"));
                    d.descuento = cursor.getFloat(cursor.getColumnIndex("descuentoRenglon"));
                    d.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    d.vendedor = cursor.getString(cursor.getColumnIndex("vendedor"));
                    d.motivo = cursor.getString(cursor.getColumnIndex("motivo"));
                    d.sincronizado = cursor.getInt(cursor.getColumnIndex("sincronizado"));
                    d.cantCambio = cursor.getInt(cursor.getColumnIndex("cantidadcambio"));
                    d.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    d.item = cursor.getInt(cursor.getColumnIndex("item"));
                    d.tipoCliente = cursor.getInt(cursor.getColumnIndex("tipocliente"));
                    d.inventario = cursor.getInt(cursor.getColumnIndex("inventario"));
                    d.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    d.nombProducto = d.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    d.descripcionMotivoCambio = DataBaseBOJ.obtenerMotivo(d.motivo);

                    lista.add(d);
                } while (cursor.moveToNext());
            }
            //cerrar el cursor
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {
            closeDataBase(db);
        }
        return lista;
    }


    /**
     * actualizar el pedido seleecionado por el usuario.
     *
     * @param encabezadoModificar
     * @param detallePedido
     * @return
     */
    public static boolean actualizarPedido(Encabezado encabezadoModificar, Hashtable<String, Detalle> detallePedido) {

        boolean terminado = false;
        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
            db.beginTransaction();
            dbPedido.beginTransaction();

            /************************************
             * Se Ingresa la Novedad Compra del Pedido
             ************************************/


            values = new ContentValues();

            values.put("valor", encabezadoModificar.valor_neto);
            values.put("horaFinal", encabezadoModificar.hora_final);
            values.put("fecha", fechaActual);

            String[] whereArgs = new String[]{encabezadoModificar.numero_doc};

            int rowNovedadesDB = db.update("NovedadesCompras", values, "NroDoc=?", whereArgs);
            int rowNovedadesTP = dbPedido.update("NovedadesCompras", values, "NroDoc=?", whereArgs);
            values.clear();

            /************************************
             * Se Ingresa el Encabezado del Producto
             ************************************/

            values.put("fechaTrans", fechaActual);
            values.put("montoFact", encabezadoModificar.valor_neto);
            values.put("desc1", encabezadoModificar.valor_descuento);
            values.put("iva", encabezadoModificar.total_iva);
            values.put("fechaInicial", encabezadoModificar.hora_inicial);
            values.put("fechaFinal", encabezadoModificar.hora_final);
            values.put("observaciones", encabezadoModificar.observacion);
            values.put("observaciones2", encabezadoModificar.observacion2);
            values.put("fechaEntrega", encabezadoModificar.fechaEntrega);
            values.put("oc", encabezadoModificar.ordenCompra);

            int rowEncabezadoDB = db.update("Encabezado", values, "numeroDoc=?", whereArgs);
            int rowEncabezadoTP = dbPedido.update("Encabezado", values, "numeroDoc=?", whereArgs);
            values.clear();


            /************************************
             * Se Ingresa el Detalle del Producto
             ************************************/

            //limpiar el detalle anterior
            String clearDetalle = "DELETE FROM [Detalle] WHERE [Detalle].[numDoc] = '" + encabezadoModificar.numero_doc + "';";
            db.execSQL(clearDetalle);
            dbPedido.execSQL(clearDetalle);

            Enumeration<Detalle> e = detallePedido.elements();
            Detalle detalle;

            int contItem = 1;
            int rowDetalleDB = -1;
            int rowDetalleTP = -1;

            while (e.hasMoreElements()) {

                detalle = e.nextElement();
                values = new ContentValues();

                values.put("numDoc", encabezadoModificar.numero_doc);
                values.put("codigoRef", detalle.codProducto);
                values.put("precio", detalle.precio);
                values.put("tarifaIva", detalle.iva);
                values.put("descuentoRenglon", detalle.descuento);
                values.put("cantidad", detalle.cantidad);
                values.put("vendedor", Main.usuario.codigoVendedor);
                //values.put("motivo",           encabezado.codigo_novedad);//cero para pedido si es devolucion select * from motivoscambio where tipo='D'
                values.put("motivo", detalle.codMotivo);
                values.put("fecha", fechaActual);
                values.put("sincronizado", "0");
                values.put("cantidadcambio", "0");
                values.put("bodega", Main.usuario.bodega);
                values.put("bodega2", "");
                values.put("tipocliente", "0");
                values.put("inventario", "0");
                values.put("sincronizadoandroid", 0);
                values.put("tipoventa", 0);
                values.put("item", contItem + "");


                rowDetalleDB += db.insertOrThrow("Detalle", null, values);
                rowDetalleTP += dbPedido.insertOrThrow("Detalle", null, values);

                contItem++;
            }


            db.execSQL("Update descuentosautorizados set numerodoc='" + encabezadoModificar.numero_doc + "' where numerodoc='" + encabezadoModificar.codigo_cliente + "'");
            dbPedido.execSQL("Update descuentosautorizados set numerodoc='" + encabezadoModificar.numero_doc + "' where numerodoc='" + encabezadoModificar.codigo_cliente + "'");


            //validar que todas las inserciones se realizaron.
            if (rowNovedadesDB > 0 && rowNovedadesTP > 0 - 1) {
                if (rowEncabezadoDB > 0 && rowEncabezadoTP > 0) {
                    if (rowDetalleDB > -1 && rowDetalleTP > -1) {
                        db.setTransactionSuccessful();
                        dbPedido.setTransactionSuccessful();
                        terminado = true;
                    }
                }
            }
        } catch (Exception e) {
            mensaje = e.getMessage();

        } finally {
            closeDataBase(db);
            closeDataBase(dbPedido);
        }
        return terminado;
    }


    public static Vector<Detalle> CargarPedidosLineaProductos(String fechaConsecutivo, Vector<ItemListView> listaItemsPedido) {

        mensaje = "";
        SQLiteDatabase db = null;
        ItemListView itemListView;

        Detalle encabezado;
        Vector<Detalle> listaPedidos = new Vector<Detalle>();

        if (listaItemsPedido == null)
            listaItemsPedido = new Vector<ItemListView>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select l.codigo,MAX(l.nombre) as nombre,SUM(cantidad) as cantidad,SUM(cantidad*Precio) as total  "
                    + "from detalle d  "
                    + "inner join lineas l on d.bodega2 = l.codigo  "
                    + "inner join encabezado e on e.numerodoc = d.numdoc   "
                    + "where tipotrans=0  "
                    + "group by l.codigo ";

            System.out.println("Consulta venta Por Linea --> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    encabezado = new Detalle();
                    encabezado.codProducto = cursor.getString(cursor.getColumnIndex("Codigo"));
                    encabezado.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    encabezado.precio = cursor.getFloat(cursor.getColumnIndex("total"));


                    itemListView = new ItemListView();
                    itemListView.titulo = encabezado.codProducto + " - " + DataBaseBOJ.NombreLineaProducto(encabezado.codProducto);
                    itemListView.subTitulo = "Cantidad: " + encabezado.cantidad;

                    listaItemsPedido.addElement(itemListView);
                    listaPedidos.addElement(encabezado);


                } while (cursor.moveToNext());

                mensaje = "Cargo Pedidos Realizados Correctamente";

            } else {

                mensaje = "Consulta sin resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarPedidosRealizados", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaPedidos;
    }

    public static String NombreLineaProducto(String codigo) {

        SQLiteDatabase db = null;
        String nombre = "";
        mensaje = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT Nombre FROM Lineas WHERE Codigo = '" + codigo + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nombre = cursor.getString(cursor.getColumnIndex("Nombre"));
                mensaje = "Consulta Satisfactoria";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return nombre;
    }

    public static void borrarDatosRecaudo() {
        SQLiteDatabase db = null;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //iniciar transacciones para garantizar la correcta insercion en las base de datos.
            db.beginTransaction();

            db.execSQL("delete from detallerecaudo");
            db.execSQL("delete from formapago");

            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.e("Eliminando detallerecaudo formapago", "error: " + e.getMessage());
        } finally {
            closeDataBase(db);
        }
    }

    public static boolean guardarRecaudoFormasDePagoNuevoxxx(String numRecibo,
                                                             double totalRecaudo, String consecutivo, float totalFP) {

        String fechaSistema = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
        SQLiteDatabase db = null;
        SQLiteDatabase tmp = null;
        boolean insertado = false;
        String ultimoNumeroDoc = "";
        double total = 0;
        int i = 0;
        int j = 0;

        // se define variables para contar inserciones
        long rowDb = -2;
        long rowTmp = -4;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            File dbTmp = new File(Util.DirApp(), "Temp.db");
            tmp = SQLiteDatabase.openDatabase(dbTmp.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            // iniciar transacciones para garantizar la correcta insercion en
            // las base de datos.
            db.beginTransaction();
            tmp.beginTransaction();

            String sqlFormaPagoTemp = ""
                    + "SELECT Nrodoc, "
                    + "       forma_pago, "
                    + "       MAX(monto) as monto, "
                    + "       nro_cheque_cons, "
                    + "       banco, "
                    + "       fecha_registro, "
                    + "       fecha_post, "
                    + "       codigocliente, "
                    + "       vendedor, "
                    + "       bodega, "
                    + "       tipocliente, "
                    + "       disponible, "
                    + "       plaza "
                    + "FROM [formapagotemp] "
                    + "GROUP BY [Nrodoc], [forma_pago], [nro_cheque_cons], [banco], "
                    + "      [fecha_registro], [fecha_post], [codigocliente], [vendedor], [bodega], "
                    + "      [tipocliente], [disponible], [plaza] "
                    + "ORDER BY monto DESC;";

            Cursor cursor = db.rawQuery(sqlFormaPagoTemp, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getDouble(cursor.getColumnIndex("disponible"));
                    System.out.println("Disponible Forma Pago: " + total);

                    if (j == 0) {

                        String sql2 = "select * from tmpdetallerecaudo where disponible < 0";
                        System.out.println("Disponible < 0");

                        Cursor cursor2 = db.rawQuery(sql2, null);

                        if (cursor2.moveToFirst()) {

                            do {

                                ContentValues values = new ContentValues();

                                values.put("Nrodoc", consecutivo);
                                values.put("Tiporecaudo", "0");
                                values.put("valor", Util.Redondear("" + ((float) cursor2.getDouble(cursor2.getColumnIndex("valor"))), 2));
                                values.put("CodigoBanco", "00");
                                values.put("bodega", Main.usuario.bodega);
                                values.put("vendedor", Main.usuario.codigoVendedor);
                                values.put("Tipocliente", "0");
                                values.put("numdoc", cursor2.getString(cursor2.getColumnIndex("Nrodoc")));
                                values.put("observacion", cursor2.getString(cursor2.getColumnIndex("observaciones")));
                                values.put("prontopago", "1");
                                values.put("tipopago", cursor2.getString(cursor2.getColumnIndex("tipo_pago")));
                                values.put("valor_fp", Util.Redondear(Util.QuitarE("" + (float) cursor2.getDouble(cursor2.getColumnIndex("valor"))), 2));
                                values.put("numero_fp", cursor.getString(cursor.getColumnIndex("nro_cheque_cons")));
                                values.put("formapago", cursor.getString(cursor.getColumnIndex("forma_pago")));
                                values.put("plaza", cursor.getString(cursor.getColumnIndex("plaza")));
                                values.put("reference", cursor2.getString(cursor2.getColumnIndex("referencia")));
                                values.put("exercise", cursor2.getString(cursor2.getColumnIndex("exercise")));
                                values.put("finnancial", cursor2.getString(cursor2.getColumnIndex("finnancial")));


                                db.insertOrThrow("detallerecaudo", null, values);
                                tmp.insertOrThrow("detallerecaudo", null, values);

                                total = total - (float) cursor2.getDouble(cursor2.getColumnIndex("valor"));

                                i = i + 1;
                            } while (cursor2.moveToNext());
                        }


                        if (cursor2 != null)
                            cursor2.close();

                    }

                    String sql3 = "select * from tmpdetallerecaudo where disponible > 0 order by valor desc";
                    System.out.println("Disponible > 0");

                    Cursor cursor3 = db.rawQuery(sql3, null);

                    cursor3.moveToPosition(-1);

                    while (cursor3.moveToNext() && total > 0) {

                        ContentValues values = new ContentValues();

                        if (cursor3.getFloat(cursor3.getColumnIndex("disponible")) > total) {

                            System.out.println("Disponible > Total ? " + cursor3.getFloat(cursor3.getColumnIndex("disponible")) + ">" + total);
                            System.out.println("Valor: " + Util.Redondear("" + ((double) cursor3.getDouble(cursor3.getColumnIndex("valor"))), 2));
                            System.out.println("ValorFP: " + Util.Redondear("" + ((float) total), 2));


                            values = new ContentValues();

                            values.put("Nrodoc", consecutivo);
                            values.put("Tiporecaudo", "0");
                            values.put("valor", Util.Redondear("" + ((float) cursor3.getDouble(cursor3.getColumnIndex("valor"))), 2));
                            values.put("CodigoBanco", "00");
                            values.put("bodega", Main.usuario.bodega);
                            values.put("vendedor", Main.usuario.codigoVendedor);
                            values.put("Tipocliente", "0");
                            values.put("numdoc", cursor3.getString(cursor3.getColumnIndex("Nrodoc")));
                            values.put("observacion", cursor3.getString(cursor3.getColumnIndex("observaciones")));
                            values.put("prontopago", "1");
                            values.put("tipopago", cursor3.getString(cursor3.getColumnIndex("tipo_pago")));
                            values.put("valor_fp", Util.Redondear("" + ((float) total), 2));
                            values.put("numero_fp", cursor.getString(cursor.getColumnIndex("nro_cheque_cons")));
                            values.put("formapago", cursor.getString(cursor.getColumnIndex("forma_pago")));
                            values.put("plaza", cursor.getString(cursor.getColumnIndex("plaza")));
                            values.put("reference", cursor3.getString(cursor3.getColumnIndex("referencia")));

                            String sqlAux = "Update tmpdetallerecaudo set disponible=disponible-"
                                    + total
                                    + " where nrodoc='"
                                    + cursor3.getString(cursor3.getColumnIndex("Nrodoc")) + "'";

                            total = 0;
                            db.execSQL(sqlAux);

                        } else {
                            values = new ContentValues();

                            values.put("Nrodoc", consecutivo);
                            values.put("Tiporecaudo", "0");
                            values.put("valor", Util.Redondear("" + (float) cursor3.getDouble(cursor3.getColumnIndex("valor")), 2));
                            values.put("CodigoBanco", "00");
                            values.put("vendedor", Main.usuario.codigoVendedor);
                            values.put("bodega", Main.usuario.bodega);
                            values.put("Tipocliente", "0");
                            values.put("numdoc", cursor3.getString(cursor3.getColumnIndex("Nrodoc")));
                            values.put("Observacion", cursor3.getString(cursor3.getColumnIndex("observaciones")));
                            values.put("prontopago", "1");
                            values.put("tipopago", cursor3.getString(cursor3.getColumnIndex("tipo_pago")));
                            values.put("valor_fp", Util.Redondear(Util.QuitarE("" + (float) cursor3.getDouble(cursor3.getColumnIndex("disponible"))), 2));
                            values.put("numero_fp", cursor.getString(cursor.getColumnIndex("nro_cheque_cons")));
                            values.put("formapago", cursor.getString(cursor.getColumnIndex("forma_pago")));
                            values.put("plaza", cursor.getString(cursor.getColumnIndex("plaza")));
                            values.put("reference", cursor3.getString(cursor3.getColumnIndex("referencia")));


                            String sqlAux = "Update tmpdetallerecaudo set disponible = disponible- "
                                    + (float) cursor3.getDouble(cursor3.getColumnIndex("valor"))
                                    + " where nrodoc='"
                                    + cursor3.getString(cursor3.getColumnIndex("Nrodoc")) + "'";
                            total = total - cursor3.getFloat(cursor3.getColumnIndex("disponible"));
                            db.execSQL(sqlAux);

                        }

                        db.insertOrThrow("detallerecaudo", null, values);
                        tmp.insertOrThrow("detallerecaudo", null, values);

                        ultimoNumeroDoc = cursor3.getString(cursor3.getColumnIndex("Nrodoc"));

                        i += 1;
                    }

                    j = j + 1;

                    if (cursor3 != null)
                        cursor3.close();

                } while (cursor.moveToNext());
            }

            float totalRecaudoAux = Math.round(totalRecaudo);

            if (totalFP > totalRecaudoAux) {

                System.out.println("TotalFP: " + totalFP + " > TotalRecaudoAux" + totalRecaudoAux);
                System.out.println("TotalRecaudo: " + totalRecaudo);

                float varAux = (float) (totalFP - totalRecaudo);

                String query = "Update detallerecaudo set valor_fp = valor_fp + "
                        + Util.Redondear("" + varAux, 2)
                        + ",tipopago='P' where numdoc='" + ultimoNumeroDoc
                        + "'   AND rowid IN (SELECT MAX(rowid) FROM detallerecaudo)";

                System.out.println("Query Raro--> " + query);

                db.execSQL(query);
                tmp.execSQL(query);

            }


            if (cursor != null)
                cursor.close();


            String query2 = ""
                    + "SELECT Nrodoc, "
                    + "       forma_pago, "
                    + "       MAX(monto) as monto, "
                    + "       nro_cheque_cons, "
                    + "       banco, "
                    + "       fecha_registro, "
                    + "       fecha_post, "
                    + "       codigocliente, "
                    + "       vendedor, "
                    + "       bodega, "
                    + "       tipocliente, "
                    + "       disponible, "
                    + "       plaza "
                    + "FROM [formapagotemp] "
                    + "GROUP BY [Nrodoc], [forma_pago], [nro_cheque_cons], [banco], "
                    + "      [fecha_registro], [fecha_post], [codigocliente], [vendedor], [bodega], "
                    + "      [tipocliente], [disponible], [plaza];";

            Cursor cursor4 = db.rawQuery(query2, null);

            cursor4.moveToPosition(-1);

            while (cursor4.moveToNext()) {

                ContentValues values = new ContentValues();
                values.put("Nrodoc", consecutivo);
                values.put("forma_pago", cursor4.getString(cursor4.getColumnIndex("forma_pago")));
                values.put("monto", cursor4.getDouble(cursor4.getColumnIndex("monto")));
                values.put("nro_cheque_cons", cursor4.getString(cursor4.getColumnIndex("nro_cheque_cons")));
                values.put("banco", cursor4.getString(cursor4.getColumnIndex("banco")));
                values.put("fecha_registro", fechaSistema);
                values.put("fecha_post", cursor4.getString(cursor4.getColumnIndex("fecha_post")));
                values.put("codigocliente", Main.cliente.codigo);
                values.put("vendedor", Main.usuario.codigoVendedor);
                values.put("bodega", Main.usuario.bodega);
                values.put("tipocliente", "0");

                db.insertOrThrow("formapago", null, values);
                tmp.insertOrThrow("formapago", null, values);

            }

            if (cursor4 != null) {
                cursor4.close();
            }

            ContentValues values = new ContentValues();
            values.put("nrodoc", consecutivo);
            values.put("codigocliente", Main.cliente.codigo);
            values.put("total", Util.Redondear("" + totalFP, 2));
            values.put("fecha_recaudo", fechaSistema);
            values.put("vendedor", Main.usuario.codigoVendedor);
            values.put("bodega", Main.usuario.bodega);
            values.put("tipocliente", "0");
            values.put("sincronizadomapas", 0);
            values.put("sincronizado", 0);
            values.put("nrorecibo", numRecibo);

            db.insertOrThrow("Encabezadorecaudo", null, values);
            tmp.insertOrThrow("Encabezadorecaudo", null, values);

            String query = "select * from tmpdetallerecaudo where tipo_pago='P' ";

            Cursor cursor5 = db.rawQuery(query, null);

            cursor5.moveToPosition(-1);

            while (cursor5.moveToNext()) {

                String sqlUpdate = "UPDATE CARTERA SET ABONO=ABONO+"
                        + cursor5.getFloat(cursor5.getColumnIndex("valor"))
                        + " WHERE DOCUMENTO='"
                        + cursor5.getString(cursor5
                        .getColumnIndex("Nrodoc")) + "'";
                db.execSQL(sqlUpdate);

            }

            if (cursor5 != null)
                cursor5.close();


            // verificar que la actualizacion fue correcta en ambas bases de
            // datos. y confirmar la transaccion como exitosa.
            if (rowDb != -1 && rowTmp != -1) {

                insertado = true;
                db.setTransactionSuccessful();
                tmp.setTransactionSuccessful();
            } else {
                throw new Exception("Error");
            }
        } catch (Exception e) {
            Log.e("No se logro insertar datos de encabezado",
                    "error: " + e.getMessage());
        } finally {
            closeDataBase(db);
            closeDataBase(tmp);
        }
        return insertado;
    }

    public static Impresion ObtenerEncabezadoImpresion(String numeroDoc) {

        mensaje = "";
        Impresion impresion = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT bodega, descripcion, razonSocial, nit, direccion, telefono, tipo FROM VendedorImpresion";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    impresion = new Impresion();
                    impresion.bodega = cursor.getString(cursor.getColumnIndex("bodega"));
                    impresion.descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));
                    impresion.razonSocial = cursor.getString(cursor.getColumnIndex("razonSocial"));
                    impresion.nit = cursor.getString(cursor.getColumnIndex("nit"));
                    impresion.direccion = cursor.getString(cursor.getColumnIndex("direccion"));
                    impresion.telefono = cursor.getString(cursor.getColumnIndex("telefono"));
                    impresion.tipo = cursor.getString(cursor.getColumnIndex("tipo"));
                }

                if (cursor != null)
                    cursor.close();

                query = "SELECT RazonSocial, Codigo AS Nit FROM Clientes WHERE Codigo = (SELECT codigo FROM Encabezado WHERE numeroDoc = '" + numeroDoc + "')";
                cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    impresion.razonSocialCliente = cursor.getString(cursor.getColumnIndex("RazonSocial"));
                    impresion.nitCliente = cursor.getString(cursor.getColumnIndex("Nit"));
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "ObtenerEncabezadoImpresion -> " + Msg.NO_EXISTE_BD);
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ObtenerEncabezadoImpresion -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return impresion;
    }

    public static Vector<RecibosRecaudo> getRecibosRecaudo() {

        SQLiteDatabase db = null;
        String sql = "";

        RecibosRecaudo rr;
        Vector<RecibosRecaudo> vRR = new Vector<RecibosRecaudo>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select distinct rc.numRecaudo as numRecaudo, rc.numRecibo as numRecibo, "
                    + "( select sum( valor ) from RecaudoFormaPago where numRecaudo = rc.numRecaudo ) as valor "
                    + "from RecaudoConsecutivo rc "
                    + "where numRecaudo not in (select distinct numeroDoc from RecaudosAnulados  ) ";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    rr = new RecibosRecaudo();

                    rr.numRecaudo = cursor.getString(cursor.getColumnIndex("numRecaudo"));
                    rr.numRecibo = cursor.getString(cursor.getColumnIndex("numRecibo"));
                    rr.valor = cursor.getLong(cursor.getColumnIndex("valor"));

                    vRR.add(rr);
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            if (db != null)
                db.close();
        }

        return vRR;
    }

    public static InformacionReciboCobro getInformacionReciboCobro(String numRecibo) {

        SQLiteDatabase db = null;
        String sql = "";
        InformacionReciboCobro irc = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select codigo, nombre, razonsocial, direccion, telefono, ciudad, " +
                    "( select strftime( '%Y-%m-%d', fecha ) fecha from recaudoFacturas where numRecaudo = '" + numRecibo + "' limit 1 ) as fecha, " +
                    "( select numRecibo from RecaudoConsecutivo where numRecaudo = '" + numRecibo + "' limit 1 ) as numRecibo " +
                    "from clientes where codigo in ( select codCliente from recaudoFacturas where numRecaudo = '" + numRecibo + "' )";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    irc = new InformacionReciboCobro();

                    irc.numRecibo = cursor.getString(cursor.getColumnIndex("numRecibo"));
                    irc.codCliente = cursor.getString(cursor.getColumnIndex("codigo"));
                    irc.nombreCliente = cursor.getString(cursor.getColumnIndex("nombre"));
                    irc.razonSocial = cursor.getString(cursor.getColumnIndex("razonsocial"));
                    irc.direccion = cursor.getString(cursor.getColumnIndex("direccion"));
                    irc.telefono = cursor.getString(cursor.getColumnIndex("telefono"));
                    irc.fecha = cursor.getString(cursor.getColumnIndex("fecha"));
                    irc.ciudad = cursor.getString(cursor.getColumnIndex("ciudad"));
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

            if (irc != null) {

                irc.codVendedor = Main.usuario.codigoVendedor;
                irc.nombreVendedor = Main.usuario.nombreVendedor;

                sql = "select retefuente, descuento, flete, otrosDescuentos, observacion " +
                        "from RecaudoDescuentos where numRecaudo = '" + numRecibo + "'";

                cursor = db.rawQuery(sql, null);

                if (cursor.moveToFirst()) {

                    do {

                        irc.retefuente = cursor.getLong(cursor.getColumnIndex("retefuente"));
                        irc.descuento = cursor.getLong(cursor.getColumnIndex("descuento"));
                        irc.flete = cursor.getLong(cursor.getColumnIndex("flete"));
                        irc.descuento += cursor.getLong(cursor.getColumnIndex("otrosDescuentos"));
                        irc.obserDesc = cursor.getString(cursor.getColumnIndex("observacion"));
                        irc.totalDesc = irc.retefuente + irc.descuento + irc.flete;
                    }
                    while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                sql = "select sum( valor ) as valor, 'EFECTIVO' as fp from RecaudoFormaPago where numRecaudo = '" + numRecibo + "' and tipoFormaPago = 1 " +
                        "union " +
                        "select sum( valor ) as valor, 'CHEQUE' as fp from RecaudoFormaPago where numRecaudo = '" + numRecibo + "' and tipoFormaPago = 2 " +
                        "union " +
                        "select sum( valor ) as valor, 'CHEQUEPOST' as fp from RecaudoFormaPago where numRecaudo = '" + numRecibo + "' and tipoFormaPago = 4 " +
                        "union " +
                        "select sum( valor ) as valor, 'CONSIGNACION' as fp from RecaudoFormaPago where numRecaudo = '" + numRecibo + "' and tipoFormaPago = 3";

                cursor = db.rawQuery(sql, null);

                if (cursor.moveToFirst()) {

                    do {

                        String fp = cursor.getString(cursor.getColumnIndex("fp"));

                        if (fp.equals("EFECTIVO")) {

                            irc.efectivo = cursor.getLong(cursor.getColumnIndex("valor"));
                        } else if (fp.equals("CHEQUE")) {

                            irc.cheque += cursor.getLong(cursor.getColumnIndex("valor"));
                        } else if (fp.equals("CHEQUEPOST")) {

                            irc.chequePost += cursor.getLong(cursor.getColumnIndex("valor"));
                        } else if (fp.equals("CONSIGNACION")) {

                            irc.consignacion += cursor.getLong(cursor.getColumnIndex("valor"));
                        }
                    }
                    while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                irc.totalRecaudo = irc.cheque + irc.efectivo + irc.consignacion + irc.chequePost;

                sql = "select numFactura, valor "
                        + "from recaudoFacturas "
                        + "where numRecaudo = '" + numRecibo + "'";

                cursor = db.rawQuery(sql, null);

                long totalFacturas = 0;
                Vector<FacturasImpresion> vFactImpresion = new Vector<FacturasImpresion>();
                FacturasImpresion fi;

                if (cursor.moveToFirst()) {

                    do {

                        fi = new FacturasImpresion();

                        fi.numeroFactura = cursor.getString(cursor.getColumnIndex("numFactura"));
                        fi.valor = cursor.getLong(cursor.getColumnIndex("valor"));

                        totalFacturas += fi.valor;

                        vFactImpresion.add(fi);
                    }
                    while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                irc.vFactImpresion = vFactImpresion;

                sql = "select numCheque, strftime( '%Y-%m-%d', fechaCheque ) as fecha " +
                        "from RecaudoFormaPago " +
                        "where numRecaudo = '" + numRecibo + "' and tipoFormaPago = 4";

                cursor = db.rawQuery(sql, null);

                Vector<ChequePostFechado> vChequePost = new Vector<ChequePostFechado>();
                ChequePostFechado cpf;

                if (cursor.moveToFirst()) {

                    do {

                        cpf = new ChequePostFechado();

                        cpf.numeroCheque = cursor.getString(cursor.getColumnIndex("numCheque"));
                        cpf.fecha = cursor.getString(cursor.getColumnIndex("fecha"));

                        vChequePost.add(cpf);
                    }
                    while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                sql = "select ifnull( observacion, '' ) as observacion " +
                        "from RecaudoFormaPago " +
                        "where numRecaudo = '" + numRecibo + "'";

                String observacion = "";

                cursor = db.rawQuery(sql, null);

                if (cursor.moveToFirst()) {

                    do {

                        String obser = cursor.getString(cursor.getColumnIndex("observacion"));

                        if (!obser.equals("")) {

                            if (observacion.equals("")) {

                                observacion = obser;
                            } else {

                                observacion += ". " + obser;
                            }
                        }
                    }
                    while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

                irc.obserFP = observacion;
                irc.vChequePost = vChequePost;
            }
        } catch (Exception e) {

        } finally {

            if (db != null)
                db.close();
        }

        return irc;
    }

    public static long[] getDatosCierreCaja() {

        mensaje = "";
        SQLiteDatabase db = null;

        long efectivo = 0, cheque = 0, chequepost = 0, consignacion = 0, valor = 0, totalCheque;
        String fp = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select sum( valor ) as valor, case tipoFormaPago when 1 then 'EFECTIVO' when 2 then 'CHEQUE' when 3 then 'CONSIGNACION' else 'CHEQUEPOST' end as formaPago " +
                    "from RecaudoFormaPago " +
                    "where numRecaudo not in (select distinct numeroDoc from RecaudosAnulados ) " +
                    "group by tipoFormaPago ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    fp = cursor.getString(cursor.getColumnIndex("formaPago"));
                    valor = cursor.getLong(cursor.getColumnIndex("valor"));

                    if (fp.equals("EFECTIVO"))
                        efectivo = valor;
                    else if (fp.equals("CHEQUE"))
                        cheque = valor;
                    else if (fp.equals("CONSIGNACION"))
                        consignacion = valor;
                    else if (fp.equals("CHEQUEPOST"))
                        chequepost = valor;

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }


        totalCheque = cheque + chequepost;
        long[] valores = {efectivo, cheque, chequepost, consignacion, (efectivo + cheque + chequepost + consignacion)};

        return valores;
    }

    public static boolean documentoConFacturaPendiente(String referencia) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select * from cartera where referencia='" + referencia + "' and saldo>0 and documento not in (select nrodoc from tmpdetallerecaudo) and saldo>0";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }

    //Metodo utilizado para guardar el factura en temp

    public static boolean agregarFactura(Cartera cartera, Cartera carteraAux) throws SQLException {

        SQLiteDatabase db = null;
        boolean inserto = false;

        //se inserta en la base de datos.
        long rowDb = -2;
        long rowTmp = -4;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //iniciar transacciones para garantizar la correcta insercion en las base de datos.
            db.beginTransaction();

            /*captura de los datos que seran insertados en las bases de datos.*/
            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("Nrodoc", cartera.documento);
            values.put("Tiporecaudo", "1");
            values.put("valor", String.valueOf(cartera.valorARecaudar));
            values.put("CodigoBanco", "00");
            values.put("numerointerno", "0");
            values.put("observaciones", cartera.observacion);

            if (cartera.pagoTotal)
                values.put("tipo_pago", "T");
            else
                values.put("tipo_pago", "P");


            if (cartera.prontoPago)
                values.put("prontopago", "1");
            else
                values.put("prontopago", "0");


            values.put("disponible", Util.Redondear(Util.QuitarE(String.valueOf(cartera.valorARecaudar)), 2));
            values.put("referencia", cartera.referencia);
            values.put("exercise", "");
            values.put("finnancial", "");
            //values.put("valordpp",cartera.valorProntoPago);
            //values.put("valorretencion",0);

            rowDb = db.insertOrThrow("tmpdetallerecaudo", null, values);

            ///*********

            if (carteraAux != null) {

                if (carteraAux.nroFiscal.equals(cartera.nroFiscal)) {

                    ContentValues valuesAux = new ContentValues();
                    valuesAux = new ContentValues();

                    valuesAux.put("Nrodoc", carteraAux.documento);
                    valuesAux.put("Tiporecaudo", "1");
                    valuesAux.put("valor", String.valueOf(carteraAux.valorARecaudar));
                    valuesAux.put("CodigoBanco", "00");
                    valuesAux.put("numerointerno", "0");
                    valuesAux.put("observaciones", carteraAux.observacion);

                    if (carteraAux.pagoTotal)
                        valuesAux.put("tipo_pago", "T");
                    else
                        valuesAux.put("tipo_pago", "P");


                    if (carteraAux.prontoPago)
                        valuesAux.put("prontopago", "1");
                    else
                        valuesAux.put("prontopago", "0");


                    valuesAux.put("disponible", Util.Redondear(Util.QuitarE(String.valueOf(carteraAux.valorARecaudar)), 2));
                    valuesAux.put("referencia", carteraAux.referencia);
                    valuesAux.put("exercise", "");
                    valuesAux.put("finnancial", "");
                    //values.put("valordpp",cartera.valorProntoPago);
                    //values.put("valorretencion",0);

                    rowDb = db.insertOrThrow("tmpdetallerecaudo", null, valuesAux);
                }

            }


            //verificar que no haya ocurrido error en la insercion
            if (rowDb == -1 || rowTmp == -1) {
                throw new SQLException("Error Insertando en base de datos");
            } else {
                /*confirmar transacciones exitosas*/
                db.setTransactionSuccessful();
                inserto = true;
                System.out.println("Guardo Recaudo");
            }
        } catch (SQLException e) {
            Log.e("insertando factura en tmpdetallerecaudo", "error: " + e.getMessage());
        } finally {
            closeDataBase(db);
        }
        return inserto;
    }

    public static boolean guardarRecaudoFormasDePagoNuevo(String consecutivo, double totalRecaudo, float totalFP, String imei, String nroRecibo) {

        String fechaSistema = Util.FechaActual("yyyy-MM-dd HH:mm:ss.SSS");
        SQLiteDatabase db = null;
        SQLiteDatabase tmp = null;
        boolean insertado = false;
        String ultimoNumeroDoc = "";
        Main.usuario = ObtenerUsuario();
        String bodegas = Main.usuario.bodega;
        double total = 0;
        int i = 0;
        int j = 0;

        // se define variables para contar inserciones
        long rowDb = -2;
        long rowTmp = -4;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File dbTmp = new File(Util.DirApp(), "Temp.db");
            tmp = SQLiteDatabase.openDatabase(dbTmp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            // iniciar transacciones para garantizar la correcta insercion en la base de datos.
            db.beginTransaction();
            tmp.beginTransaction();

            //			String sqlFormaPagoTemp = "select * from formapagotemp order by monto desc;";

            String sqlFormaPagoTemp = ""
                    + "SELECT Nrodoc, "
                    + "       forma_pago, "
                    + "       MAX(monto) as monto, "
                    + "       nro_cheque_cons, "
                    + "       banco, "
                    + "       fecha_registro, "
                    + "       fecha_post, "
                    + "       codigocliente, "
                    + "       vendedor, "
                    + "       bodega, "
                    + "       tipocliente, "
                    + "       disponible, "
                    + "       plaza "
                    + "FROM [formapagotemp] "
                    + "GROUP BY [Nrodoc], [forma_pago], [nro_cheque_cons], [banco], "
                    + "      [fecha_registro], [fecha_post], [codigocliente], [vendedor], [bodega], "
                    + "      [tipocliente], [disponible], [plaza] "
                    + "ORDER BY monto DESC;";

            Cursor cursor = db.rawQuery(sqlFormaPagoTemp, null);

            if (cursor.moveToFirst()) {
                do {

                    total = cursor.getDouble(cursor.getColumnIndex("disponible"));

                    if (j == 0) {

                        String sql2 = "select * from tmpdetallerecaudo where disponible < 0 ;";

                        Cursor cursor2 = db.rawQuery(sql2, null);

                        if (cursor2.moveToFirst()) {
                            do {
                                ContentValues values = new ContentValues();

                                values.put("Nrodoc", consecutivo);
                                values.put("Tiporecaudo", "0");
                                values.put("valor", Util.Redondear("" + ((float) cursor2.getDouble(cursor2.getColumnIndex("valor"))), 2));
                                values.put("CodigoBanco", "00");
                                values.put("vendedor", Main.usuario.codigoVendedor);
                                values.put("bodega", bodegas);
                                values.put("Tipocliente", "0");
                                values.put("numdoc", cursor2.getString(cursor2.getColumnIndex("Nrodoc")));
                                values.put("observacion", cursor2.getString(cursor2.getColumnIndex("observaciones")));
                                values.put("prontopago", cursor2.getString(cursor2.getColumnIndex("prontopago")));
                                values.put("tipopago", cursor2.getString(cursor2.getColumnIndex("tipo_pago")));
                                values.put("valor_fp", Util.Redondear(Util.QuitarE("" + (float) cursor2.getDouble(cursor2.getColumnIndex("valor"))), 2));
                                values.put("numero_fp", cursor.getString(cursor.getColumnIndex("nro_cheque_cons")));
                                values.put("formapago", cursor.getString(cursor.getColumnIndex("forma_pago")));
                                values.put("plaza", cursor.getString(cursor.getColumnIndex("plaza")));
                                values.put("reference", cursor2.getString(cursor2.getColumnIndex("referencia")));
                                //								values.put("exercise", cursor2.getString(cursor2.getColumnIndex("exercise")));
                                //								values.put("finnancial", cursor2.getString(cursor2.getColumnIndex("finnancial")));
                                values.put("valordpp", "0");

                                db.insertOrThrow("detallerecaudo", null, values);
                                tmp.insertOrThrow("detallerecaudo", null, values);

                                total = total - (float) cursor2.getDouble(cursor2.getColumnIndex("valor"));

                                i = i + 1;
                            } while (cursor2.moveToNext());
                        }

                        if (cursor2 != null)
                            cursor2.close();
                    }

                    String sql3 = "select * from tmpdetallerecaudo where disponible > 0 order by valor desc;";

                    Cursor cursor3 = db.rawQuery(sql3, null);

                    cursor3.moveToPosition(-1);

                    while (cursor3.moveToNext() && total > 0) {

                        ContentValues values = new ContentValues();

                        if (cursor3.getFloat(cursor3.getColumnIndex("disponible")) > total) {

                            values = new ContentValues();

                            values.put("Nrodoc", consecutivo);
                            values.put("Tiporecaudo", "0");
                            values.put("valor", Util.Redondear("" + ((float) cursor3.getDouble(cursor3.getColumnIndex("valor"))), 2));
                            values.put("CodigoBanco", "00");
                            values.put("vendedor", Main.usuario.codigoVendedor);
                            values.put("bodega", bodegas);
                            values.put("Tipocliente", "0");
                            values.put("numdoc", cursor3.getString(cursor3.getColumnIndex("Nrodoc")));
                            values.put("observacion", cursor3.getString(cursor3.getColumnIndex("observaciones")));
                            values.put("prontopago", cursor3.getString(cursor3.getColumnIndex("prontopago")));
                            values.put("tipopago", cursor3.getString(cursor3.getColumnIndex("tipo_pago")));
                            values.put("valor_fp", Util.Redondear("" + ((float) total), 2));
                            values.put("numero_fp", cursor.getString(cursor.getColumnIndex("nro_cheque_cons")));
                            values.put("formapago", cursor.getString(cursor.getColumnIndex("forma_pago")));
                            values.put("plaza", cursor.getString(cursor.getColumnIndex("plaza")));
                            values.put("reference", cursor3.getString(cursor3.getColumnIndex("referencia")));
                            //							values.put("exercise", cursor3.getString(cursor3.getColumnIndex("exercise")));
                            //							values.put("finnancial", cursor3.getString(cursor3.getColumnIndex("finnancial")));
                            values.put("valordpp", "0");// **********

                            String sqlAux = "Update tmpdetallerecaudo set disponible=disponible-"
                                    + total
                                    + " where nrodoc='"
                                    + cursor3.getString(cursor3.getColumnIndex("Nrodoc")) + "'";
                            total = 0;
                            db.execSQL(sqlAux);

                        } else {

                            values = new ContentValues();

                            values.put("Nrodoc", consecutivo);
                            values.put("Tiporecaudo", "0");
                            values.put("valor", Util.Redondear("" + (float) cursor3.getDouble(cursor3.getColumnIndex("valor")), 2));
                            values.put("CodigoBanco", "00");
                            values.put("vendedor", Main.usuario.codigoVendedor);
                            values.put("bodega", bodegas);
                            values.put("Tipocliente", "0");
                            values.put("numdoc", cursor3.getString(cursor3.getColumnIndex("Nrodoc")));
                            values.put("observacion", cursor3.getString(cursor3.getColumnIndex("observaciones")));
                            values.put("prontopago", cursor3.getString(cursor3.getColumnIndex("prontopago")));
                            values.put("tipopago", cursor3.getString(cursor3.getColumnIndex("tipo_pago")));
                            values.put("valor_fp", Util.Redondear(Util.QuitarE("" + (float) cursor3.getDouble(cursor3.getColumnIndex("disponible"))), 2));
                            values.put("numero_fp", cursor.getString(cursor.getColumnIndex("nro_cheque_cons")));
                            values.put("formapago", cursor.getString(cursor.getColumnIndex("forma_pago")));
                            values.put("plaza", cursor.getString(cursor.getColumnIndex("plaza")));
                            values.put("reference", cursor3.getString(cursor3.getColumnIndex("referencia")));
                            //							values.put("exercise", cursor3.getString(cursor3.getColumnIndex("exercise")));
                            //							values.put("finnancial", cursor3.getString(cursor3.getColumnIndex("finnancial")));
                            values.put("valordpp", "0");

                            String sqlAux = "Update tmpdetallerecaudo set disponible=disponible-"
                                    + (float) cursor3.getDouble(cursor3.getColumnIndex("valor"))
                                    + " where nrodoc='"
                                    + cursor3.getString(cursor3.getColumnIndex("Nrodoc")) + "'";
                            total = total - cursor3.getFloat(cursor3.getColumnIndex("disponible"));
                            db.execSQL(sqlAux);
                        }

                        db.insertOrThrow("detallerecaudo", null, values);
                        tmp.insertOrThrow("detallerecaudo", null, values);

                        ultimoNumeroDoc = cursor3.getString(cursor3.getColumnIndex("Nrodoc"));

                        i += 1;
                    }

                    j += 1;

                    if (cursor3 != null)
                        cursor3.close();

                } while (cursor.moveToNext());
            }

            if (totalFP > totalRecaudo) {

                float varAux = (float) (totalFP - totalRecaudo);

                String query = "Update detallerecaudo set valor_fp=valor_fp + "
                        + Util.Redondear("" + varAux, 2)
                        + " where numdoc='" + ultimoNumeroDoc
                        + "'   AND rowid IN (SELECT MAX(rowid) FROM detallerecaudo)";
                db.execSQL(query);
                tmp.execSQL(query);
            }

            if (cursor != null)
                cursor.close();


            /*Crear cursor4*/
            //			String query2 = "SELECT DISTINCT * FROM formapagotemp;";
            String query2 = ""
                    + "SELECT Nrodoc, "
                    + "       forma_pago, "
                    + "       MAX(monto) as monto, "
                    + "       nro_cheque_cons, "
                    + "       banco, "
                    + "       fecha_registro, "
                    + "       fecha_post, "
                    + "       codigocliente, "
                    + "       vendedor, "
                    + "       bodega, "
                    + "       tipocliente, "
                    + "       disponible, "
                    + "       plaza "
                    + "FROM [formapagotemp] "
                    + "GROUP BY [Nrodoc], [forma_pago], [nro_cheque_cons], [banco], "
                    + "      [fecha_registro], [fecha_post], [codigocliente], [vendedor], [bodega], "
                    + "      [tipocliente], [disponible], [plaza];";

            Cursor cursor4 = db.rawQuery(query2, null);

            if (cursor4.moveToFirst()) {
                do {
                    ContentValues values = new ContentValues();
                    values.put("Nrodoc", consecutivo);
                    values.put("forma_pago", cursor4.getString(cursor4.getColumnIndex("forma_pago")));
                    values.put("monto", cursor4.getDouble(cursor4.getColumnIndex("monto")));
                    values.put("nro_cheque_cons", cursor4.getString(cursor4.getColumnIndex("nro_cheque_cons")));
                    values.put("banco", cursor4.getString(cursor4.getColumnIndex("banco")));
                    values.put("fecha_registro", fechaSistema);
                    values.put("fecha_post", cursor4.getString(cursor4.getColumnIndex("fecha_post")));
                    values.put("codigocliente", Main.cliente.codigo);
                    values.put("vendedor", Main.usuario.codigoVendedor);
                    values.put("bodega", bodegas);
                    values.put("tipocliente", "0");

                    db.insertOrThrow("formapago", null, values);
                    tmp.insertOrThrow("formapago", null, values);

                } while (cursor4.moveToNext());
            }
            //cerrar cursor4
            if (cursor4 != null) {
                cursor4.close();
            }


            if (j > 0) {

                ContentValues values = new ContentValues();
                values.put("nroDoc", consecutivo);
                values.put("CodigoCliente", Main.cliente.codigo);
                values.put("Total", Util.Redondear("" + totalFP, 2));
                values.put("Fecha_recaudo", fechaSistema);
                values.put("Vendedor", Main.usuario.codigoVendedor);
                values.put("Bodega", bodegas);
                values.put("Tipocliente", "0");
                values.put("sincronizadomapas", 0);
                values.put("sincronizado", 0);
                values.put("Nrorecibo", nroRecibo);
                values.put("fechaterminal", fechaSistema);


                db.insertOrThrow("Encabezadorecaudo", null, values);
                tmp.insertOrThrow("Encabezadorecaudo", null, values);


                //inicia cursor5
                String query = "select * from tmpdetallerecaudo where tipo_pago='P' ";
                Cursor cursor5 = db.rawQuery(query, null);
                //lectura del cursor5
                if (cursor5.moveToFirst()) {
                    do {
                        String sqlUpdate = "UPDATE CARTERA SET ABONO=ABONO +"
                                + cursor5.getFloat(cursor5.getColumnIndex("valor"))
                                + " WHERE DOCUMENTO='"
                                + cursor5.getString(cursor5.getColumnIndex("Nrodoc")) + "'";
                        db.execSQL(sqlUpdate);

                    } while (cursor5.moveToNext());
                }
                //cerrar cursor5
                if (cursor5 != null)
                    cursor5.close();
            }

            //			//actualizar consecutivos de recibo.
            //			TransaccionBO.actualizarConsecutivo(db, recibosRecaudo);
            //
            //			ConsecutivosRecibos consecutivosRecibos = new ConsecutivosRecibos();
            //			consecutivosRecibos.id = recibosRecaudo.id;
            //			consecutivosRecibos.nrorecibo = recibosRecaudo.actual;
            //			consecutivosRecibos.sincronizado = 0;
            //			consecutivosRecibos.sincronizadoandroid = 0;
            //			consecutivosRecibos.vendedor = Main.usuario.codigoVendedor;

            //			TransaccionBO.insertConsecutivoRecibos(db, consecutivosRecibos);
            //			TransaccionBO.insertConsecutivoRecibos(tmp, consecutivosRecibos);

            // verificar que la actualizacion fue correcta en ambas bases de
            // datos. y confirmar la transaccion como exitosa.
            if (rowDb != -1 && rowTmp != -1) {
                insertado = true;
                db.setTransactionSuccessful();
                tmp.setTransactionSuccessful();
            } else {
                throw new Exception("Error");
            }
        } catch (Exception e) {
            Log.e("No se logro insertar datos de encabezado", "error: " + e.getMessage());
        } finally {
            closeDataBase(db);
            closeDataBase(tmp);
        }
        return insertado;
    }

    public static Vector<Cartera> cargarCarteraRecaudo(String codCliente) {

        SQLiteDatabase db = null;
        Vector<Cartera> listaCarteraDeRecaudo = new Vector<Cartera>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "Select Nrodoc,saldo as  Tiporecaudo,valor, cartera.cliente from tmpdetallerecaudo "
                        + "inner join cartera on tmpdetallerecaudo.Nrodoc=cartera.documento "
                        + "where cartera.cliente = '" + codCliente + "' ";

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        Cartera cartera = new Cartera();
                        cartera.documento = cursor.getString(cursor.getColumnIndex("Nrodoc"));
                        cartera.referencia = cursor.getString(cursor.getColumnIndex("Nrodoc"));
                        cartera.valorARecaudar = (float) cursor.getDouble(cursor.getColumnIndex("valor"));
                        cartera.vendedor = Main.usuario.codigoVendedor;

                        listaCarteraDeRecaudo.addElement(cartera);

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

            } else {

            }

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaCarteraDeRecaudo;
    }


    public static boolean quitarFacturaRecaudo(Cartera cartera) {
        SQLiteDatabase db = null;
        boolean borrado = false;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //iniciar transacciones para garantizar la correcta insercion en las base de datos.
            db.beginTransaction();
            // se hace el borrado del producto pedidos. en ambas bases de datos.
            long dbDel = db.delete("tmpdetallerecaudo", "Nrodoc = '" + cartera.documento + "'", null);

            //verificar que el borrado fue correcto en ambas bases de datos. y confirmar la transaccion como exitosa.
            if (dbDel > 0) {
                db.setTransactionSuccessful();
                borrado = true;
                System.out.println("Elimino Factura Correctamente \n Nro: " + cartera.documento);
            } else {
                throw new Exception("No se logro borrar datos de factura " + cartera.documento);
            }
        } catch (Exception e) {
            Log.e("Eliminando factura en tmpdetallerecaudo", "error: " + e.getMessage());
        } finally {
            closeDataBase(db);
        }

        return borrado;
    }

    public static Vector<FormaPago> CargarFormasPagos(String codCliente) {

        FormaPago formaPago;
        SQLiteDatabase db = null;
        Vector<FormaPago> listaFormasPago = new Vector<FormaPago>();
        System.out.println("FormpagoCodCliente");

        try {

            int i = 0;
            File dbFile = new File(Util.DirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select forma_pago,monto,nro_cheque_cons from formapagotemp where codigoCliente = '" + codCliente + "' ";
            //				+ "where codigocliente ='"+codCliente+"' ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    formaPago = new FormaPago();
                    formaPago.position = i++;

                    int cod = formaPago.formaPago = cursor.getInt(cursor.getColumnIndex("forma_pago"));
                    //formaPago.descripcion = cod == 1 ? "Efectivo" : cod == 2 ? "Cheque" : "";


                    if (cod == 1) {

                        formaPago.descripcion = "Efectivo";

                    }

                    if (cod == 2) {

                        formaPago.descripcion = "Cheque";

                    }

                    if (cod == 3) {

                        formaPago.descripcion = "Consignacion";

                    }


                    if (cod == 4) {

                        formaPago.descripcion = "Cheque PostFechado";

                    }


                    formaPago.monto = cursor.getFloat(cursor.getColumnIndex("monto"));
                    formaPago.codigo = cod;
                    formaPago.nro_cheque_cons = cursor.getString(cursor.getColumnIndex("nro_cheque_cons"));
                    //formaPago.id_auxiliar = cursor.getString(cursor.getColumnIndex("id_auxiliar"));

                    listaFormasPago.addElement(formaPago);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ListaFormasPago:" + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaFormasPago;
    }

    public static Vector<Cartera> CargarCarteraRecaudo(String codCliente) {

        SQLiteDatabase db = null;
        Vector<Cartera> listaCarteraDeRecaudo = new Vector<Cartera>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String where = "";
                if (!codCliente.equals(""))
                    where = "where cartera.cliente = '" + codCliente + "' ";

                String query = "Select Nrodoc, saldo as Tiporecaudo,valor, cartera.cliente "
                        + "from tmpdetallerecaudo "
                        + "inner join cartera on tmpdetallerecaudo.Nrodoc=cartera.documento "
                        + where;

                System.out.println("Consulta CargarCarteraRecaudo---> " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {

                        Cartera cartera = new Cartera();
                        cartera.documento = cursor.getString(cursor.getColumnIndex("Nrodoc"));
                        cartera.referencia = cursor.getString(cursor.getColumnIndex("Nrodoc"));
                        cartera.valorARecaudar = (float) cursor.getDouble(cursor.getColumnIndex("valor"));
                        cartera.saldo = (float) cursor.getDouble(cursor.getColumnIndex("Tiporecaudo"));
                        cartera.vendedor = Main.usuario.codigoVendedor;

                        listaCarteraDeRecaudo.addElement(cartera);

                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

            } else {

            }

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaCarteraDeRecaudo;
    }

    //Metodos de impresi�n

    public static Impresion getImpresionCliente(String numeroDoc) {

        SQLiteDatabase db = null;
        String sql = "";
        Impresion impClient = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select c.Codigo as codigo , c.Nombre as nombre, c.Direccion as direccion, c.nit as nit, c.RazonSocial as razonSocial, "
                    + "c.Telefono as telefono "
                    + "from encabezado e "
                    + "inner join clientes c on c.Codigo = e.codigo "
                    + "where e.numeroDoc = '" + numeroDoc + "' ";

            System.out.println("Impresion infoCliente---> " + sql);


            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impClient = new Impresion();

                    impClient.razonSocial = cursor.getString(cursor.getColumnIndex("razonSocial"));
                    impClient.nit = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impClient.ruc = cursor.getString(cursor.getColumnIndex("nit"));
                    impClient.direccion = cursor.getString(cursor.getColumnIndex("direccion"));
                    impClient.telefono = cursor.getString(cursor.getColumnIndex("telefono"));
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return impClient;
    }

    public static Impresion getImpresionClienteLiquidador(String numeroDoc) {

        SQLiteDatabase db = null;
        String sql = "";
        Impresion impClient = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select c.Codigo as codigo , c.Nombre as nombre, c.Direccion as direccion, c.nit as nit, c.RazonSocial as razonSocial, "
                    + "c.Telefono as telefono "
                    + "from encabezado e "
                    + "inner join clientes c on c.Codigo = e.codigo "
                    + "where e.numeroDoc = '" + numeroDoc + "' ";

            System.out.println("Impresion infoCliente---> " + sql);


            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impClient = new Impresion();

                    impClient.razonSocial = cursor.getString(cursor.getColumnIndex("razonSocial"));
                    impClient.nit = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impClient.direccion = cursor.getString(cursor.getColumnIndex("direccion"));
                    impClient.ruc = cursor.getString(cursor.getColumnIndex("nit"));
                    impClient.telefono = cursor.getString(cursor.getColumnIndex("telefono"));
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return impClient;
    }

    public static Impresion getImpresionClienteRecaudo(String numeroDoc) {

        SQLiteDatabase db = null;
        String sql = "";
        Impresion impClient = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "select c.Codigo as codigo , c.Nombre as nombre, c.Direccion as direccion, c.nit as nit, c.RazonSocial as razonSocial, "
                    + "c.Telefono as telefono from encabezadoRecaudo e "
                    + "inner join clientes c on c.Codigo = e.CodigoCliente "
                    + "where e.nroDoc = '" + numeroDoc + "' "
                    + "AND e.nroDoc in (select Nrodoc from detallerecaudo)";

            System.out.println("Impresion infoCliente recaudo---> " + sql);


            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impClient = new Impresion();

                    impClient.razonSocial = cursor.getString(cursor.getColumnIndex("razonSocial"));
                    impClient.nit = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impClient.direccion = cursor.getString(cursor.getColumnIndex("direccion"));
                    impClient.ruc = cursor.getString(cursor.getColumnIndex("nit"));
                    impClient.telefono = cursor.getString(cursor.getColumnIndex("telefono"));
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return impClient;
    }


    public static Vector<ImpresionDetalle> getImpresionDetalle(String numeroDoc, boolean isPedido, boolean isCargue, boolean isAveria) {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionDetalle impDet = null;
        Vector<ImpresionDetalle> vImpDet = new Vector<ImpresionDetalle>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            if (!isCargue && !isAveria) {

                if (isPedido) {

                    sql = "select p.codigo as codigo, p.nombre as nombre, d.precio as precio, d.cantidad as cantidad, d.descuentorenglon as descuento,d.tarifaiva as iva "
                            + "from detalle d "
                            + "inner join productos p on p.codigo = d.[codigoRef] "
                            + "inner join encabezado e on e.numeroDoc = d.numDoc "
                            + "where numDoc = '" + numeroDoc + "'  "
                            + "order by p.orden asc ";
                } else {
                    sql = "select p.codigo as codigo, p.nombre as nombre, d.precio as precio, d.cantidad as cantidad, d.motivo as codMotivo, mc.concepto as descMotivo, d.descuentorenglon as descuento,d.tarifaiva as iva "
                            + "from detalle d "
                            + "inner join productos p on p.codigo = d.[codigoRef] "
                            + "inner join MotivosCambio mc on mc.codigo = d.motivo "
                            + "where numDoc = '" + numeroDoc + "' order by p.orden asc ";
                }

            } else if (isCargue) {

                sql = "select p.codigo as codigo, p.nombre as nombre, d.Precio as precio, d.Cantidad as cantidad, Cantidadcambio "
                        + "from detallesugerido d "
                        + "inner join productos p on p.codigo = d.[CodigoRef] "
                        + "where NumDoc = '" + numeroDoc + "' order by p.orden asc ";
            } else if (isAveria) {

                sql = "select p.codigo as codigo, p.nombre as nombre, d.Precio as precio, d.Cantidad as cantidad "
                        + "from detallesugerido d "
                        + "inner join productos p on p.codigo = d.[CodigoRef] "
                        + "where NumDoc = '" + numeroDoc + "' order by p.orden asc ";

            }

            System.out.println("ImpresionDetalleProductos: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impDet = new ImpresionDetalle();

                    impDet.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impDet.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impDet.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    impDet.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));


                    if (!isCargue && !isAveria) {

                        impDet.descuento = cursor.getFloat(cursor.getColumnIndex("descuento"));
                        impDet.iva = cursor.getFloat(cursor.getColumnIndex("iva"));


                    }

	/*	    if(isPedido){
		    	impDet.descuento  = cursor.getFloat( cursor.getColumnIndex( "descuento" ) );
		    	impDet.iva  = cursor.getFloat( cursor.getColumnIndex( "iva" ) );

		    }
	 */
                    if (isCargue)
                        impDet.cantidadUnidades = cursor.getInt(cursor.getColumnIndex("Cantidadcambio"));


                    if (!isCargue && !isAveria) {

                        if (!isPedido) {

                            impDet.codMotivo = cursor.getString(cursor.getColumnIndex("codMotivo"));
                            impDet.descMotivo = cursor.getString(cursor.getColumnIndex("descMotivo"));
                            impDet.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                        }

                    }

                    vImpDet.add(impDet);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpDet;
    }

    public static Vector<ImpresionDetalle> getImpresionDetalleLiquidacionProducto(String numeroDoc) {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionDetalle impDet = null;
        Vector<ImpresionDetalle> vImpDet = new Vector<ImpresionDetalle>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select p.codigo as codigo, p.nombre as nombre, d.precio as precio,  CASE WHEN d.cantidad  < 0 THEN -1 * d.cantidad  ELSE d.cantidad  END AS cantidad "
                    + "from detalle d " + "inner join productos p on p.codigo = d.[codigoRef] " + "where numDoc = '"
                    + numeroDoc + "' order by p.orden asc ";

            System.out.println("getImpresionDetalleLiquidacionProducto: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impDet = new ImpresionDetalle();

                    impDet.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impDet.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impDet.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    int cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    impDet.cantidad = cantidad < 0 ? -1 * cantidad : cantidad;

                    vImpDet.add(impDet);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpDet;
    }

    public static Vector<ImpresionDetalle> getImpresionDetalleLiquidacion(String codUser, boolean isDanado) {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionDetalle impDet = null;
        Vector<ImpresionDetalle> vImpDet = new Vector<ImpresionDetalle>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            //			if(isDanado){

            sql = "SELECT tipo, producto, pr.nombre as nombre, cantidaddigitada, cantidadtransaccion, "
                    + "cantidadinventario, diferencia "
                    + "FROM LIQUIDACIONDIFERENCIAS liq "
                    + "INNER JOIN Productos pr ON liq.producto = pr.codigo "
                    + "WHERE vendedor = '" + codUser + "' AND tipo = 'DANADO' ORDER BY pr.Orden";

            //			}else{
            //
            //
            //			}


            System.out.println("getImpresionDetalleLiquidacion: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impDet = new ImpresionDetalle();

                    impDet.tipoProducto = cursor.getString(cursor.getColumnIndex("tipo"));
                    impDet.codigo = cursor.getString(cursor.getColumnIndex("producto"));
                    impDet.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impDet.cantidadDigitada = cursor.getInt(cursor.getColumnIndex("cantidaddigitada"));
                    impDet.cantidadTransaccion = cursor.getInt(cursor.getColumnIndex("cantidadtransaccion"));
                    impDet.cantidadInventario = cursor.getInt(cursor.getColumnIndex("cantidadinventario"));
                    impDet.diferencia = cursor.getInt(cursor.getColumnIndex("diferencia"));

                    vImpDet.add(impDet);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpDet;
    }

    public static Vector<ImpresionDetalle> getImpresionDetalleAveria(String nroDoc, String codUser, boolean isDanado) {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionDetalle impDet = null;
        Vector<ImpresionDetalle> vImpDet = new Vector<ImpresionDetalle>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select p.codigo as codigo, p.nombre as nombre, d.Precio as precio, d.Cantidad as cantidad "
                    + "from detallesugerido d "
                    + "inner join productos p on p.codigo = d.[CodigoRef] "
                    + "where NumDoc = '" + nroDoc + "' order by p.orden asc ";


            System.out.println("getImpresionDetalleAVERIA: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impDet = new ImpresionDetalle();

                    impDet.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impDet.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impDet.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    impDet.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));

                    vImpDet.add(impDet);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpDet;
    }


    public static Vector<ImpresionDetalle> getImpresionDetalleLiquidacionInventarioProducto(String codUser) {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionDetalle impDet = null;
        Vector<ImpresionDetalle> vImpDet = new Vector<ImpresionDetalle>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT tipo, producto, pr.nombre as nombre, cantidaddigitada, cantidadtransaccion, "
                    + "cantidadinventario, diferencia "
                    + "FROM LIQUIDACIONDIFERENCIAS liq "
                    + "INNER JOIN Productos pr ON liq.producto = pr.codigo "
                    + "WHERE vendedor = '" + codUser + "' AND tipo = 'PRODUCTO INVENTARIO' "
                    + "ORDER BY pr.Orden; ";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impDet = new ImpresionDetalle();

                    impDet.tipoProducto = cursor.getString(cursor.getColumnIndex("tipo"));
                    impDet.codigo = cursor.getString(cursor.getColumnIndex("producto"));
                    impDet.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impDet.cantidadDigitada = cursor.getInt(cursor.getColumnIndex("cantidaddigitada"));
                    impDet.cantidadTransaccion = cursor.getInt(cursor.getColumnIndex("cantidadtransaccion"));
                    impDet.cantidadInventario = cursor.getInt(cursor.getColumnIndex("cantidadinventario"));
                    impDet.diferencia = cursor.getInt(cursor.getColumnIndex("diferencia"));

                    vImpDet.add(impDet);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpDet;
    }


    public static Vector<FormaPago> getImpresionRecaudo(String numeroDoc) {

        SQLiteDatabase db = null;
        String sql = "";
        FormaPago formaPago;
        Vector<FormaPago> listaFormasPago = new Vector<FormaPago>();


        try {

            int i = 0;
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "select fp.codigoCliente as codigo, fp.forma_pago, fp.monto "
                    + "from formapago as fp "
                    + "where fp.nroDoc =  '" + numeroDoc + "' "
                    + "AND fp.nroDoc in (select Nrodoc from detallerecaudo) ";

            System.out.println("ImpresionRecadoFormaPago " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    formaPago = new FormaPago();
                    formaPago.position = i++;

                    int cod = formaPago.formaPago = cursor.getInt(cursor.getColumnIndex("forma_pago"));
                    //formaPago.descripcion = cod == 1 ? "Efectivo" : cod == 2 ? "Cheque" : "";


                    if (cod == 1) {

                        formaPago.descripcion = "Efectivo";

                    }

                    if (cod == 2) {

                        formaPago.descripcion = "Cheque";

                    }

                    if (cod == 3) {

                        formaPago.descripcion = "Consignacion";

                    }


                    if (cod == 4) {

                        formaPago.descripcion = "Cheque PostFechado";

                    }

                    formaPago.monto = cursor.getFloat(cursor.getColumnIndex("monto"));
                    formaPago.codigo = cod;

                    listaFormasPago.addElement(formaPago);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return listaFormasPago;
    }


    public static Vector<FormaPago> getAllFormaPago() {

        SQLiteDatabase db = null;
        String sql = "";
        FormaPago formaPago;
        Vector<FormaPago> listaFormasPago = new Vector<FormaPago>();


        try {

            int i = 0;
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "select fp.codigoCliente as codigo, fp.forma_pago, fp.monto from formapago as fp ";

            System.out.println("ImpresionRecadoFormaPago " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    formaPago = new FormaPago();
                    formaPago.position = i++;

                    int cod = formaPago.formaPago = cursor.getInt(cursor.getColumnIndex("forma_pago"));
                    //formaPago.descripcion = cod == 1 ? "Efectivo" : cod == 2 ? "Cheque" : "";


                    if (cod == 1) {

                        formaPago.descripcion = "Efectivo";

                    }

                    if (cod == 2) {

                        formaPago.descripcion = "Cheque";

                    }

                    if (cod == 3) {

                        formaPago.descripcion = "Consignacion";

                    }


                    if (cod == 4) {

                        formaPago.descripcion = "Cheque PostFechado";

                    }

                    formaPago.monto = cursor.getFloat(cursor.getColumnIndex("monto"));
                    formaPago.codigo = cod;

                    listaFormasPago.addElement(formaPago);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return listaFormasPago;
    }


    /**
     * Metodo encargado de validar quen la factura ya este anulada
     *
     * @param numDoc
     * @return
     */
    public static boolean verificarAnulacion(String numDoc) {

        mensaje = "";
        SQLiteDatabase db = null;
        boolean isverdadero = false;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT NroRecibo FROM AnulacionRecibos where NroRecibo = '"
                    + numDoc + "'";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                isverdadero = true;

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return isverdadero;
    }

    public static InformacionFP cargarInformacionFormasPago(String nroDoc) {

        mensaje = "";
        SQLiteDatabase db = null;
        InformacionFP informacionFP = new InformacionFP();
        int fp = 0;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT FORMA_PAGO, SUM(MONTO) AS MONTO FROM FORMAPAGO WHERE Nrodoc = '" + nroDoc + "' GROUP BY FORMA_PAGO";
            System.out.println("Consulta montopago: " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    fp = cursor.getInt(cursor.getColumnIndex("forma_pago"));
                    System.out.println("FormaPago: " + fp);

                    if (fp == 1) {

                        informacionFP.efectivo = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                    } else {

                        if (fp == 2) {

                            informacionFP.ch = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                        } else {

                            if (fp == 4) {


                                informacionFP.cp = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                            } else {

                                informacionFP.consignaciones = cursor.getFloat(cursor.getColumnIndex("MONTO"));
                            }


                        }


                    }

                } while (cursor.moveToNext());

                mensaje = "Informe De Recaudo Cargado con Exito";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return informacionFP;
    }

    /**
     * Borrar los contenidos de las tablas temporales de formapagotemp y tmpdetallerecaudo.
     */
    public static void borrarTempFormaPagoDetalleRecaudos() {
        SQLiteDatabase db = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String deletetmpdetallerecaudo = "DELETE FROM tmpdetallerecaudo;";
            String deleteformapagotemp = "DELETE FROM formapagotemp;";

            db.execSQL(deletetmpdetallerecaudo);
            db.execSQL(deleteformapagotemp);

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "guardarRecaudo - > " + mensaje, e);
        } finally {
            closeDataBase(db);
        }
    }


    /**
     * Borrar los contenidos de las tablas temporales de formapagotemp y tmpdetallerecaudo.
     */
    public static void borrarTempFormaPagoRecaudos() {
        SQLiteDatabase db = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String deleteformapagotemp = "DELETE FROM formapagotemp;";

            db.execSQL(deleteformapagotemp);

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "guardarRecaudo - > " + mensaje, e);
        } finally {
            closeDataBase(db);
        }
    }

    /**
     * Borrar la tabla temporal cartera_prontopago_consig_temp.
     */
    public static void borrarTablaTemporalCarteraProntoPagoConsig() {

        SQLiteDatabase db = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String borrarTabla = "DROP TABLE IF EXISTS cartera_prontopago_consig_temp;";
            db.execSQL(borrarTabla);

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "no se logro borrar tabla temporal cartera_prontopago_consig_temp  - > " + mensaje, e);
        } finally {
            closeDataBase(db);
        }
    }

    /**
     * crea una Tabla temporal en DataBase.db.
     * esta tabla temporal es usada para conservar las carteras que le han
     * aplicado pronto pago pero vencido al momento de hacer el recaudo.
     * por lo que se espera en las formas de pago una consignacion de esa cartera que
     * cumpla con la condicion de que la fecha de la consignacion tiene que ser mayor o igual a los
     * dias de vencimiento de la columna dias_vencido_prontopago de esta tabla.
     * Se conserva el valor de esa cartera (saldo_cartera) porque en un futuro
     * puede ser necesario validar que ademas de cumplir con la fecha
     * se tenga que comparar que el monto de la consignacion cubra el total de la cartera.
     */
    public static void crearTablaTemporalCarteraProntoPagoConsig() {

        SQLiteDatabase db = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String crearTabla = "" +
                    "CREATE TABLE IF NOT EXISTS cartera_prontopago_consig_temp( " +
                    "       documento varchar(30), " +
                    "       referencia varchar(50), " +
                    "       saldo_cartera double, " +
                    "       dias_vencido_prontopago int, " +
                    "       saldada_por_fecha int, " +
                    "       saldada_por_monto int " +
                    ");";

            db.execSQL(crearTabla);

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "no se logro crear tabla temporal cartera_prontopago_consig_temp  - > " + mensaje, e);
        } finally {
            closeDataBase(db);
        }
    }


    /**
     * Cambiar el estado del campo anulado a 1, en tabla encabezado.
     *
     * @param encabezado
     * @return true si es anulado, false en caso contrario.
     */
    public static boolean anularEncabezado(Encabezado encabezado) {

        SQLiteDatabase db = null;
        SQLiteDatabase tmp = null;
        boolean anulado = false;
        try {
            Usuario usuario = DataBaseBOJ.ObtenerUsuario();
            int dbRow = 0;
            int tmpRow = 0;
            String amarreDev = "";

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            File tmpFile = new File(Util.DirApp(), "Temp.db");

            if (dbFile.exists() && tmpFile.exists()) {
                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                tmp = SQLiteDatabase.openDatabase(tmpFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.beginTransaction();
                tmp.beginTransaction();

                ContentValues values = new ContentValues();
                values.put("anulado", 1);

                if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {

                    if (encabezado.tipoTrans == 0) {

                        dbRow = db.update("Encabezado", values, "numeroDoc = ?", new String[]{encabezado.numero_doc});
                        tmpRow = tmp.update("Encabezado", values, "numeroDoc = ?", new String[]{encabezado.numero_doc});

                        if (dbRow > 0 && tmpRow > 0) {

                            //Procedemos a eliminar la devolucion amarrada a ese pedido
                            amarreDev = DataBaseBOJ.obtenerNroDocDevolucion(db, tmp, encabezado.numero_doc, encabezado.codigo_cliente);
                            System.out.println("AMARRE FP2 ---> " + amarreDev);

                            if (!amarreDev.equals("")) {
                                dbRow = db.update("Encabezado", values, "numeroDoc = ?", new String[]{amarreDev});
                                tmpRow = tmp.update("Encabezado", values, "numeroDoc = ?", new String[]{amarreDev});
                            }

                        }


                    } else if (encabezado.tipoTrans == 2) {

                        dbRow = db.update("Encabezado", values, "fp2 = ?", new String[]{encabezado.amarre});
                        tmpRow = tmp.update("Encabezado", values, "fp2 = ?", new String[]{encabezado.amarre});

                        if (dbRow > 0 && tmpRow > 0) {
                            dbRow = db.update("Encabezado", values, "numeroDoc = ?", new String[]{encabezado.amarre});
                            tmpRow = tmp.update("Encabezado", values, "numeroDoc = ?", new String[]{encabezado.amarre});
                        }
                    }

                } else {

                    dbRow = db.update("Encabezado", values, "numeroDoc = ?", new String[]{encabezado.numero_doc});
                    tmpRow = tmp.update("Encabezado", values, "numeroDoc = ?", new String[]{encabezado.numero_doc});
                }


                if (dbRow > 0 && tmpRow > 0) {
                    db.setTransactionSuccessful();
                    tmp.setTransactionSuccessful();
                    anulado = true;
                }
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "anularEncabezado - > " + mensaje, e);
        } finally {
            closeDataBase(db);
            closeDataBase(tmp);
        }
        return anulado;
    }


    //Filtro Pedidos Familia
    public static Vector<Filtro> listaFiltroFamilia(Vector<String> items) {
        SQLiteDatabase db = null;
        Filtro filtro;
        Vector<Filtro> listadoFamilia = new Vector<Filtro>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT f.linea FROM filtros f "
                    + "INNER JOIN ProductosTmp p on f.codigo = p.codigo "
                    + "GROUP BY f.linea";

            System.out.println("Query de Familia---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                filtro = new Filtro();

                filtro.filtroDescripcion = "TODOS";

                listadoFamilia.addElement(filtro);
                items.addElement(filtro.filtroDescripcion);

                do {

                    filtro = new Filtro();

                    filtro.filtroDescripcion = cursor.getString(cursor.getColumnIndex("linea"));

                    listadoFamilia.addElement(filtro);
                    items.addElement(filtro.filtroDescripcion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaProveedores", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listadoFamilia;
    }

    //Filtro Pedidos Clase
    public static Vector<Filtro> listaFiltroClase(Vector<String> items, String filtroFamilia) {
        SQLiteDatabase db = null;
        Filtro clase;
        Vector<Filtro> listadoClase = new Vector<Filtro>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select categoria from filtros as f inner join ProductosTmp p on f.codigo = p.codigo " + filtroFamilia + " GROUP BY categoria";

            System.out.println("Query de Clase---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                clase = new Filtro();

                clase.filtroDescripcion = "TODOS";

                listadoClase.addElement(clase);
                items.addElement(clase.filtroDescripcion);

                do {

                    clase = new Filtro();

                    //								familia.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    clase.filtroDescripcion = cursor.getString(cursor.getColumnIndex("categoria"));

                    listadoClase.addElement(clase);
                    items.addElement(clase.filtroDescripcion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaProveedores", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listadoClase;
    }


    public static Vector<Producto> listaProducto(String filtro, Vector<ItemListView> listaItems, boolean isAutoVenta, String and) {

        mensaje = "";
        SQLiteDatabase db = null;

        Producto producto = new Producto();
        ItemListView itemListView;
        Vector<Producto> listaProducto = new Vector<Producto>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "Select f.codigo as codigo, f.nombre, p.precio, p.iva, p.saldo, (p.precio + ((p.precio * p.iva) / 100)) as precioIva, p.unidadesxcaja, p.ean, p.agrupacion, p.grupo, p.linea, p.unidadmedida, p.factorlibras, p.fechalimite, p.devol, p.itf, p.impto1, p.Indice "
                    + "from filtros f "
                    + "inner join ProductosTmp p on f.codigo = p.codigo " + and + filtro + " order by p.orden";


            System.out.println("Consulta listaFiltro Productos: " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    producto = new Producto();
                    itemListView = new ItemListView();


                    //Se valida que el Saldo no Contenga e+
                    producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    producto.precio = cursor.getInt(cursor.getColumnIndex("precio"));
                    producto.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
                    producto.inventario = cursor.getInt(cursor.getColumnIndex("saldo"));
                    producto.precioIva = cursor.getFloat(cursor.getColumnIndex("precioIva"));
                    producto.unidadesXCaja = cursor.getString(cursor.getColumnIndex("unidadesxcaja"));
                    producto.ean = cursor.getString(cursor.getColumnIndex("ean"));
                    producto.agrupacion = cursor.getString(cursor.getColumnIndex("agrupacion"));
                    producto.grupo = cursor.getString(cursor.getColumnIndex("grupo"));
                    producto.linea = cursor.getString(cursor.getColumnIndex("linea"));
                    producto.unidadMedida = cursor.getString(cursor.getColumnIndex("unidadmedida"));
                    producto.factorLibras = cursor.getInt(cursor.getColumnIndex("factorlibras"));
                    producto.fechaLimite = cursor.getString(cursor.getColumnIndex("fechalimite"));
                    producto.devol = cursor.getString(cursor.getColumnIndex("devol"));
                    producto.itf = cursor.getString(cursor.getColumnIndex("itf"));
                    producto.impto1 = cursor.getInt(cursor.getColumnIndex("impto1"));
                    producto.indice = cursor.getInt(cursor.getColumnIndex("Indice"));

                    itemListView.titulo = producto.codigo + " - " + producto.descripcion;
                    itemListView.subTitulo = "Inventario: " + producto.inventario;

                    listaProducto.addElement(producto);
                    listaItems.addElement(itemListView);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaCartera - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaProducto;
    }

    public static Producto ObtenerProducto(String codigo) {

        SQLiteDatabase db = null;
        Producto producto = new Producto();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigo, nombre, precio, iva, saldo, (precio + ((precio * iva) / 100)) as precioIva, unidadesxcaja, ean, agrupacion, grupo, linea, unidadmedida, factorlibras, fechalimite, devol, itf, impto1, Indice "
                    + "FROM ProductosTmp WHERE codigo = '" + codigo + "' ";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst())

                producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
            producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
            producto.precio = cursor.getInt(cursor.getColumnIndex("precio"));
            producto.iva = cursor.getFloat(cursor.getColumnIndex("iva"));
            producto.inventario = cursor.getInt(cursor.getColumnIndex("saldo"));
            producto.precioIva = cursor.getFloat(cursor.getColumnIndex("precioIva"));
            producto.unidadesXCaja = cursor.getString(cursor.getColumnIndex("unidadesxcaja"));
            producto.ean = cursor.getString(cursor.getColumnIndex("ean"));
            producto.agrupacion = cursor.getString(cursor.getColumnIndex("agrupacion"));
            producto.grupo = cursor.getString(cursor.getColumnIndex("grupo"));
            producto.linea = cursor.getString(cursor.getColumnIndex("linea"));
            producto.unidadMedida = cursor.getString(cursor.getColumnIndex("unidadmedida"));
            producto.factorLibras = cursor.getInt(cursor.getColumnIndex("factorlibras"));
            producto.fechaLimite = cursor.getString(cursor.getColumnIndex("fechalimite"));
            producto.devol = cursor.getString(cursor.getColumnIndex("devol"));
            producto.itf = cursor.getString(cursor.getColumnIndex("itf"));
            producto.impto1 = cursor.getInt(cursor.getColumnIndex("impto1"));
            producto.indice = cursor.getInt(cursor.getColumnIndex("Indice"));

            producto.precioDescuento = 0;
            producto.descuento = 0;

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return producto;
    }

    /**
     * Permite borrar una cartera conociendo su numero de documento.
     *
     * @param documento
     */
    public static void borrarCarteraTablaTemporalCarteraProntoPagoConsig(String documento) {

        SQLiteDatabase db = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.beginTransaction();

            String whereClause = "documento = ?";
            String[] whereArgs = new String[]{documento};
            db.delete("cartera_prontopago_consig_temp", whereClause, whereArgs);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "no se logro borrar cartera de cartera_prontopago_consig_temp  - > " + mensaje, e);
        } finally {
            closeDataBase(db);
        }
    }


    /**
     * Cargar la cartera con el maximo dias de vencimiento. desde la tabla temporal
     *
     * @param carteraConsig
     */
    public static void cargarCarteraConMaxProntoPagoVencido(CarteraConsignacion carteraConsig) {

        SQLiteDatabase db = null;

        if (carteraConsig == null) {
            carteraConsig = new CarteraConsignacion();
        }
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

            String query = "" +
                    "SELECT COALESCE(MAX(c.dias_vencido_prontopago),0) AS max_dias_venc, " +
                    "		COALESCE(c.[documento],'') AS documento, " +
                    "		COALESCE(c.[saldo_cartera],0) AS saldo " +
                    "FROM cartera_prontopago_consig_temp c;";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    carteraConsig.diasVencidoProntoPago = cursor.getInt(cursor.getColumnIndex("max_dias_venc"));
                    carteraConsig.documento = cursor.getString(cursor.getColumnIndex("documento"));
                    carteraConsig.saldoCartera = cursor.getDouble(cursor.getColumnIndex("saldo"));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "no se logro consultar tabla temporal cartera_prontopago_consig_temp  - > " + mensaje, e);
        } finally {
            closeDataBase(db);
        }
    }

    /**
     * Metodo que permite actualizar el cupo de un cliente.
     * llamado desde {@link FormTerminarRecaudoActivity}.
     *
     * @param codigoCliente
     * @param total_recaudo, nuevo total del cupo.
     */
    public static void liberarCupoCliente(float total_recaudo, String codigoCliente) {
        SQLiteDatabase db = null;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "" +
                    "UPDATE [Clientes] " +
                    "SET [Cupo] = '" + total_recaudo + "' " +
                    "WHERE [Codigo] = '" + codigoCliente + "';";
            db.execSQL(query);
        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e("Actualizar cupo error", mensaje);
        } finally {
            closeDataBase(db);
        }
    }


    public static String obtenerMotivo(String motivo) {

        mensaje = "";
        String descMotivo = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT concepto FROM MotivosCambio Where codigo = '" + motivo + "' ";
                Cursor cursor = db.rawQuery(query, null);

                System.out.println("Busqueda de Motivos ---> " + query);

                if (cursor.moveToFirst()) {


                    descMotivo = cursor.getString(cursor.getColumnIndex("concepto"));

                    mensaje = "Cargo Informacion del Usuario Correctamente";
                }

                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "ObtenerUsuario -> " + Msg.NO_EXISTE_BD);
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ObtenerUsuario -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return descMotivo;
    }

    public static String ExisteCodigoEnProductos(String codigo) {

        mensaje = "";
        SQLiteDatabase db = null;
        String codProducto = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT codigo FROM ProductosTmp WHERE ean = '" + codigo + "' ";
            System.out.println("Consulta prod CoBarras " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                codProducto = cursor.getString(cursor.getColumnIndex("codigo"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExisteCodigo", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return codProducto;
    }

    public static int ObtenerCodigoMotivoDev(String concepto) {

        int codigo = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT codigo FROM MotivosCambio where concepto = '" + concepto + "' ", null);

            if (cursor.moveToFirst())
                codigo = cursor.getInt(cursor.getColumnIndex("codigo"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return codigo;
    }


    public static int ObtenerUnidadesCajaProducto(String canalCliente, String codigoProducto) {

        int unidadesCaja = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT unidadescaja from MATECAJAS where canal = '" + canalCliente
                    + "' and codigo = '" + codigoProducto + "'", null);

            if (cursor.moveToFirst())
                unidadesCaja = cursor.getInt(cursor.getColumnIndex("unidadescaja"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return unidadesCaja;
    }


    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static boolean RegistrarPedidoTemp(Hashtable<String, Detalle> detalleTmp) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");


            /************************************
             * Se Ingresa el DetalleTemporal del Producto
             ************************************/

            Enumeration<Detalle> e = detalleTmp.elements();
            Detalle detalle;

            int contItem = 1;

            db.execSQL("DELETE FROM TmpPedido");

            while (e.hasMoreElements()) {

                detalle = e.nextElement();
                values = new ContentValues();

                values.put("Id", "12345");

                values.put("ProductId", detalle.codProducto);
                values.put("Descripcion", detalle.descripcion);
                values.put("Precio", detalle.precio);

                values.put("Descuento", detalle.descuento);
                values.put("IVA", detalle.iva);
                values.put("Cantidad", detalle.cantidad);
                values.put("cantidadcambio", "0");
                values.put("bodega2", Main.usuario.codigoVendedor);
                values.put("inventario", "0");
                values.put("hora", fechaActual);
                values.put("motivo", detalle.motivo);
                values.put("posicion", detalle.posicion + "");

                contItem++;

                db.insertOrThrow("TmpPedido", null, values);

            }


            //
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static boolean BorraPedidoCero() {


        SQLiteDatabase db = null;


        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("DELETE from TmpPedido WHERE  precio=0");

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static Vector<Bonificaciones> CalcularBonificacion(String codigoVendedor, String codigoCliente, String canalAuxiliar, String bodega, String subCanalAuxiliar) {

        Bonificaciones bonificaciones = null;
        Vector<Bonificaciones> listaBonificaciones = new Vector<Bonificaciones>();

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "select zcod as zcod,encabezadobonificaciones.descripcion as promocion,productos.codigo,productos.nombre as material  from encabezadobonificaciones inner join productos on encabezadobonificaciones.matnr=productos.codigo and zcod in \n" +
                    "(\n" +
                    "select zcod from condicionesbonificaciones where opcion='EQ' and campo= 'vendedor' and low= '" + codigoVendedor + "' \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='BT' and campo='vendedor' and '" + codigoVendedor + "'<=high  and '" + codigoVendedor + "'>=low \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='EQ' and campo='cliente' and low='" + codigoCliente + "' \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='BT' and campo='cliente' and '" + codigoCliente + "'<=high  and '" + codigoCliente + "'>=low \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='EQ' and campo='canal' and low='" + canalAuxiliar + "' \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='BT' and campo='canal' and '" + canalAuxiliar + "'<=high  and '" + canalAuxiliar + "'>=low \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='EQ' and campo='agencia' and low='" + bodega + "' \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='BT' and campo='agencia' and '" + bodega + "'<=high  and '" + bodega + "'>=low \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='EQ' and campo='KDGRP' and low='" + subCanalAuxiliar + "' \n" +
                    "union\n" +
                    "select zcod from condicionesbonificaciones where opcion='BT' and campo='KDGRP' and '" + subCanalAuxiliar + "'<=high  and '" + subCanalAuxiliar + "'>=low\n" +
                    ")";

            System.out.println("Consulta de bonificaci�n ---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    bonificaciones = new Bonificaciones();

                    bonificaciones.codigoBono = cursor.getString(cursor.getColumnIndex("zcod")).trim();
                    bonificaciones.codigoRegalo = cursor.getString(cursor.getColumnIndex("codigo")).trim();
                    bonificaciones.descripcionRegalo = cursor.getString(cursor.getColumnIndex("material")).trim();

                    listaBonificaciones.addElement(bonificaciones);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaBonificaciones;
    }


    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static Vector<Bonificaciones> ObtenerTipoBonificacion(String codigoBonificacion) {

        Bonificaciones bonificaciones = null;
        Vector<Bonificaciones> listaBonificaciones = new Vector<Bonificaciones>();

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "select distinct encabezadobonificaciones.tipobon as tipobon  from detallebonificaciones \n" +
                    "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                    "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                    "where encabezadobonificaciones.zcod='" + codigoBonificacion + "'";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    bonificaciones = new Bonificaciones();

                    bonificaciones.tipoBono = cursor.getString(cursor.getColumnIndex("tipobon")).trim();

                    listaBonificaciones.addElement(bonificaciones);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaBonificaciones;
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static Vector<Detalle> ObtenerCantidadPedida(String codigoBonificacion, String tipoBonificacion) {


        Detalle detalle = null;
        Vector<Detalle> listaDetalle = new Vector<Detalle>();


        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";


            switch (tipoBonificacion) {

                case "E":

                    query = "select SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE \n" +
                            "productid IN (select productos.codigo from detallebonificaciones \n" +
                            "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' ) and precio>0";
                    break;


                case "A":

                    query = "select SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE \n" +
                            "productid IN (select productos.codigo from detallebonificaciones \n" +
                            "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' ) and precio>0";
                    break;


                case "Z":


                    query = "select productid as codigoProducto,SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE \n" +
                            "productid IN (select productos.codigo  from detallebonificaciones \n" +
                            "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' ) \n" +
                            "and precio>0 GROUP BY productid\n" +
                            "UNION\n" +
                            "select productos.codigo as codigoProducto,0 AS TOTAL  from detallebonificaciones \n" +
                            "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' \n" +
                            "and productos.codigo not in (select productid from tmppedido ) \n" +
                            "order by total";
                    break;


                case "P":

                    query = "select SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE \n" +
                            "productid IN (select productos.codigo from detallebonificaciones \n" +
                            "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' ) and precio>0";
                    break;
            }


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalle = new Detalle();


                    switch (tipoBonificacion) {

                        case "E":

                            detalle.cantidadBonos = cursor.getInt(cursor.getColumnIndex("TOTAL"));

                            break;


                        case "A":


                            detalle.cantidadBonos = cursor.getInt(cursor.getColumnIndex("TOTAL"));


                            break;


                        case "Z":

                            detalle.cantidadBonos = cursor.getInt(cursor.getColumnIndex("TOTAL"));
                            detalle.codigoBonos = cursor.getString(cursor.getColumnIndex("codigoProducto")).trim();

                            break;


                        case "P":


                            detalle.cantidadBonos = cursor.getInt(cursor.getColumnIndex("TOTAL"));
                            break;

                    }

                    listaDetalle.addElement(detalle);


                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalle;
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static int ObtenerCantidad(String codigoBonificacion, String tipoBonificacion, int cantidadPedida, String codigoBonoTemp) {

        int cantitad = 0;
        int cantitad2 = 0;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";


            switch (tipoBonificacion) {

                case "E":

                    query = "select productos.codigo as codigo,productos.nombre as nombre ,candesde as candesde,canhasta as canhasta,canbon as canbon,encabezadobonificaciones.tipobon as tipobon from \n" +
                            "detallebonificaciones inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' \n" +
                            "AND  candesde<='" + cantidadPedida + "' \n" +
                            "AND canhasta>='" + cantidadPedida + "' \n" +
                            "Order by candesde desc";
                    break;


                case "A":


                    query = "select productos.codigo as codigo,productos.nombre as nombre ,candesde as candesde,canhasta as canhasta,canbon as canbon,encabezadobonificaciones.tipobon as tipobon  from \n" +
                            "detallebonificaciones inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "'";
                    break;

                case "Z":

                    query = "select productos.codigo as codigo,productos.nombre as nombre ,candesde as candesde,canhasta as canhasta,canbon as canbon,encabezadobonificaciones.tipobon as tipobon  from detallebonificaciones \n" +
                            "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' \n" +
                            "AND PRODUCTOS.CODIGO='" + codigoBonoTemp + "'";
                    break;

                case "P":

                    query = "select productos.codigo as codigo,productos.nombre as nombre ,candesde as candesde,canhasta as canhasta,canbon as canbon,encabezadobonificaciones.tipobon as tipobon  from detallebonificaciones \n" +
                            "inner join productos on productos.codigo=detallebonificaciones.vlrcpo \n" +
                            "inner join encabezadobonificaciones on encabezadobonificaciones.zcod=detallebonificaciones.zcod  \n" +
                            "where encabezadobonificaciones.zcod='" + codigoBonificacion + "' \n" +
                            "AND  candesde<='" + cantidadPedida + "' ";
                    break;

            }


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {


                    switch (tipoBonificacion) {

                        case "E":

                            cantitad = cursor.getInt(cursor.getColumnIndex("canbon"));
                            break;


                        case "A":


                            cantitad = cursor.getInt(cursor.getColumnIndex("candesde"));

                            cantitad = (cantidadPedida / cantitad);
                            break;


                        case "Z":

                            int cantidadAuxiliar = 1000;
                            cantitad = cursor.getInt(cursor.getColumnIndex("canbon"));
                            cantitad2 = cursor.getInt(cursor.getColumnIndex("candesde"));

                            cantitad = ((cantidadPedida / cantitad2) * cantitad);

                            if (cantitad < cantidadAuxiliar) {
                                cantidadAuxiliar = cantitad;
                            } else {
                                cantitad = cantidadAuxiliar;
                            }
                            break;


                        case "P":


                            cantitad = cursor.getInt(cursor.getColumnIndex("canbon"));
                            cantitad2 = cursor.getInt(cursor.getColumnIndex("candesde"));


                            if (cantitad2 > 0) {
                                cantitad = ((cantidadPedida / cantitad2) * cantitad);
                            } else {
                                cantitad = 0;
                            }
                            break;

                    }


                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cantitad;
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static int ObtenerCantidadRegalo(String tipoBonificacion, String cantidadRegalo) {

        int cantitad = 0;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";


            switch (tipoBonificacion) {

                case "E":

                    query = "select SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE productid ='" + cantidadRegalo + "'";
                    break;


                case "A":


                    query = "select SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE productid ='" + cantidadRegalo + "'";
                    break;

                case "Z":

                    query = "select SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE productid ='" + cantidadRegalo + "'";
                    break;
                case "P":


                    query = "select SUM(CANTIDAD) AS TOTAL FROM TMPPEDIDO WHERE productid ='" + cantidadRegalo + "'";
                    break;

            }


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cantitad = cursor.getInt(cursor.getColumnIndex("TOTAL"));


                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cantitad;
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static int ObtenerCantidadInventario(String tipoBonificacion, String codigoRegalo) {

        int cantitad = 0;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = ""
                    + "SELECT p.saldo - COALESCE(v.cantidad, 0) - COALESCE(tp.cantidad, 0) AS saldo "
                    + "FROM Productos p "
                    + "LEFT JOIN ( "
                    + "	select codigo,sum(cantidad) cantidad from ( "
                    + "		SELECT  d.codigoRef AS codigo, "
                    + "				SUM(d.cantidad) AS cantidad "
                    + "		FROM Detalle d "
                    + "		INNER JOIN Encabezado e ON e.numeroDoc = d.numDoc AND ifnull(e.anulado,0) = 0 AND e.tipotrans = 0 and e.observaciones<>'FALTANTE PRODUCTO DANADO' "
                    + "		GROUP BY d.codigoRef "
                    + "		union "
                    + "		select ds.codigoref,sum(cantidad) cantidad "
                    + "		from encabezadosugerido es "
                    + "		inner join detallesugerido ds on es.numerodoc = ds.numdoc where es.tipotrans=8 "
                    + "		group by ds.codigoref "
                    + "	) tmp "
                    + "	group by codigo order by codigo ) v ON v.codigo = p.codigo "
                    + "LEFT JOIN TmpPedido tp ON tp.ProductId = p.codigo AND tp.Precio > 0 "
                    + "where p.codigo='" + codigoRegalo + "' ;";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    cantitad = cursor.getInt(cursor.getColumnIndex("saldo"));

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cantitad;
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static boolean RegistrarBonoTemp(String tipoBonificacion, String codigoProducto, String descriProducto, int cantidadProducto, String codigoBono) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;


        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");


            /************************************
             * Se Ingresa el DetalleTemporal del Producto Bonificado
             ************************************/

            switch (tipoBonificacion) {

                case "E":

                    values = new ContentValues();
                    values.put("Id", "123456");
                    values.put("ProductId", codigoProducto);
                    values.put("Descripcion", descriProducto);
                    values.put("Precio", "0");
                    values.put("Descuento", "0");
                    values.put("IVA", "0");
                    values.put("Cantidad", cantidadProducto);
                    values.put("cantidadcambio", "0");
                    values.put("bodega2", Main.usuario.codigoVendedor);
                    values.put("inventario", "0");
                    values.put("hora", fechaActual);
                    values.put("motivo", codigoBono);
                    values.put("posicion", "0");
                    db.insertOrThrow("TmpPedido", null, values);

                    break;


                case "A":


                    values = new ContentValues();
                    values.put("Id", "123456");
                    values.put("ProductId", codigoProducto);
                    values.put("Descripcion", descriProducto);
                    values.put("Precio", "0");
                    values.put("Descuento", "0");
                    values.put("IVA", "0");
                    values.put("Cantidad", cantidadProducto);
                    values.put("cantidadcambio", "0");
                    values.put("bodega2", Main.usuario.codigoVendedor);
                    values.put("inventario", "0");
                    values.put("hora", fechaActual);
                    values.put("motivo", codigoBono);
                    values.put("posicion", "0");
                    db.insertOrThrow("TmpPedido", null, values);

                    break;

                case "Z":

                    boolean estadoBono = obtenerEstadoBono(codigoProducto);

                    if (estadoBono == true) {

                        db.execSQL("Update tmppedido  set cantidad=cantidad+" + cantidadProducto + " where Productid='" + codigoProducto + "' and precio=0");

                    } else {

                        values = new ContentValues();
                        values.put("Id", "123456");
                        values.put("ProductId", codigoProducto);
                        values.put("Descripcion", descriProducto);
                        values.put("Precio", "0");
                        values.put("Descuento", "0");
                        values.put("IVA", "0");
                        values.put("Cantidad", cantidadProducto);
                        values.put("cantidadcambio", "0");
                        values.put("bodega2", Main.usuario.codigoVendedor);
                        values.put("inventario", "0");
                        values.put("hora", fechaActual);
                        values.put("motivo", codigoBono);
                        values.put("posicion", "0");
                        db.insertOrThrow("TmpPedido", null, values);

                    }

                    break;

                case "P":


                    values = new ContentValues();
                    values.put("Id", "123456");
                    values.put("ProductId", codigoProducto);
                    values.put("Descripcion", descriProducto);
                    values.put("Precio", "0");
                    values.put("Descuento", "0");
                    values.put("IVA", "0");
                    values.put("Cantidad", cantidadProducto);
                    values.put("cantidadcambio", "0");
                    values.put("bodega2", Main.usuario.codigoVendedor);
                    values.put("inventario", "0");
                    values.put("hora", fechaActual);
                    values.put("motivo", codigoBono);
                    values.put("posicion", "0");
                    db.insertOrThrow("TmpPedido", null, values);
                    break;
            }


            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static boolean RegistrarPedidoDetalle(Vector<Detalle> detalleTmp, String version, String imei, String numeroDocumento) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");


            /************************************
             * Se Ingresa el Detalle del Producto que corresponde al bono
             ************************************/

            Enumeration<Detalle> e = detalleTmp.elements();
            Detalle detalle;

            int contItem = 1;

            while (e.hasMoreElements()) {

                detalle = e.nextElement();
                values = new ContentValues();

                values.put("numDoc", numeroDocumento);
                values.put("codigoRef", detalle.codProducto);
                values.put("precio", 0);
                values.put("tarifaIva", 0);
                values.put("descuentoRenglon", 0);
                values.put("cantidad", detalle.cantidad);
                values.put("vendedor", Main.usuario.codigoVendedor);
                //values.put("motivo",           encabezado.codigo_novedad);//cero para pedido si es devolucion select * from motivoscambio where tipo='D'
                values.put("motivo", detalle.codigoRelacion);
                values.put("fechaReal", fechaActual);
                values.put("fecha", fechaActual);
                values.put("sincronizado", "0");
                values.put("cantidadcambio", "0");
                values.put("bodega", Main.usuario.bodega);
                values.put("bodega2", "");
                values.put("tipocliente", "0");
                values.put("inventario", "0");
                values.put("sincronizadoandroid", 0);
                values.put("item", "0");
                values.put("tipoventa", "0");

                contItem++;

                db.insertOrThrow("Detalle", null, values);
                dbPedido.insertOrThrow("Detalle", null, values);
            }

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static Vector<Detalle> ObtenerDetalleBono(Vector<ItemListView> listaItems) {

        Detalle detalle = null;
        Vector<Detalle> listaDetalle = new Vector<Detalle>();
        ItemListView itemListView;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "select ProductID as codigoProducto, Cantidad as cantidad, motivo, Descripcion as descripcion from TmpPedido where precio=0";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalle = new Detalle();
                    itemListView = new ItemListView();

                    detalle.codProducto = cursor.getString(cursor.getColumnIndex("codigoProducto")).trim();
                    detalle.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    detalle.codigoRelacion = cursor.getString(cursor.getColumnIndex("motivo")).trim();
                    detalle.descripcion = cursor.getString(cursor.getColumnIndex("descripcion")).trim();

                    itemListView.titulo = "Cod Prod: " + detalle.codProducto + " Cod Bono: " + detalle.codProducto;
                    itemListView.subTitulo = "Cant: " + detalle.cantidad + " Nombre: " + detalle.descripcion;
                    //itemListView.referencia = "Nombre: " + detalle.descripcion;
                    listaItems.add(itemListView);


                    //				Main.detallePedido.put(detalle.codProducto , detalle);
                    listaDetalle.addElement(detalle);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalle;
    }


    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static Vector<Detalle> ObtenerDetalleBono2() {

        Detalle detalle = null;
        Vector<Detalle> listaDetalle = new Vector<Detalle>();


        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "select ProductID as codigoProducto, Cantidad as cantidad, motivo, Descripcion as descripcion from TmpPedido where precio=0";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    detalle = new Detalle();


                    detalle.codProducto = cursor.getString(cursor.getColumnIndex("codigoProducto")).trim();
                    detalle.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    detalle.codigoRelacion = cursor.getString(cursor.getColumnIndex("motivo")).trim();
                    detalle.descripcion = cursor.getString(cursor.getColumnIndex("descripcion")).trim();


                    listaDetalle.addElement(detalle);

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("CargarBonificacion", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaDetalle;
    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    public static boolean obtenerEstadoBono(String codigoProductoBono) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select Productid from TmpPedido where  Productid='" + codigoProductoBono + "' and precio=0";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    public static PedidoAmarre existePedidoSinAmarre(String codigoCliente, String codigoVendedor) {

        mensaje = "";
        SQLiteDatabase db = null;
        PedidoAmarre pedido = null;
        boolean existeAmarre = false;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String nroDocPedido = obtenerUltNroDocAmarrePedido(codigoCliente, codigoVendedor);

            existeAmarre = existeAmarreConDev(nroDocPedido, codigoCliente, 0);

            if (!existeAmarre) {

                String query = "Select codigo, numeroDoc, montoFact FROM encabezado "
                        + "WHERE numeroDoc = '" + nroDocPedido + "' AND codigo = '" + codigoCliente + "' ";

                System.out.println("Query para obtener amarre del pedido y la devolucion ---> " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    pedido = new PedidoAmarre();
                    pedido.codigo_cliente = cursor.getString(cursor.getColumnIndex("codigo"));
                    pedido.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                    pedido.valorMonto = cursor.getFloat(cursor.getColumnIndex("montoFact"));


                    mensaje = "Consulta Satisfactoria";
                }

                if (cursor != null)
                    cursor.close();


            }


        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return pedido;

    }


    public static String obtenerUltNroDocAmarrePedido(String codigoCliente, String codigoVendedor) {

        mensaje = "";
        SQLiteDatabase db = null;
        String nroDoc = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select MAX(numeroDoc) as numeroDoc "
                    + "FROM encabezado where tipoTrans = 0 and codigo = '" + codigoCliente + "' and usuario = '" + codigoVendedor + "' ";

            System.out.println("Query para obtener  el nrodoc del amarre del ultimo pedido  ---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDoc = cursor.getString(cursor.getColumnIndex("numeroDoc"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return nroDoc;

    }


    public static boolean existeAmarreConDev(String nroDoc, String codCliente, double valorDev) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select * from encabezado where fp2 = '" + nroDoc + "' and tipoTrans = '2' and codigo = '" + codCliente + "' ";
            System.out.println("Query Existe Valor montoFact " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
                valorDev = cursor.getDouble(cursor.getColumnIndex("montoFact"));
                System.out.println("Monto Dev IN database: " + valorDev);
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }

    public static String obtenerNroDocDevAmarre(String nroDoc, String codCliente) {

        SQLiteDatabase db = null;
        String nroDocDev = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select numeroDoc from encabezado where fp2 = '" + nroDoc + "' and tipoTrans = '2' and codigo = '" + codCliente + "' ";
            System.out.println("Query obtenerNroDocDevAmarre " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDocDev = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                System.out.println("NUMERODOC DEVOLUCION: " + nroDocDev);
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return nroDocDev;
    }


    public static double obtenerValorDevAmarre(String nroDoc, String codCliente) {

        SQLiteDatabase db = null;
        double valorDev = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select montoFact from encabezado where fp2 = '" + nroDoc + "' and tipoTrans = '2' and codigo = '" + codCliente + "' ";
            System.out.println("Query Existe Valor montoFact dEV " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                valorDev = cursor.getDouble(cursor.getColumnIndex("montoFact"));
                System.out.println("Monto Dev IN database: " + valorDev);
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return valorDev;
    }

    public static String obtenerNroDocDevolucion(SQLiteDatabase db, SQLiteDatabase tmp, String nroDoc, String codCliente) {


        String nroDocDev = "";

        try {

            String query = "Select numeroDoc from encabezado where fp2 = '" + nroDoc + "' and tipoTrans = '2' and codigo = '" + codCliente + "' ";
            System.out.println("Query obtenerNroDocDevAmarre " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDocDev = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                System.out.println("NUMERODOC DEVOLUCION: " + nroDocDev);
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        }

        return nroDocDev;
    }

    public static Vector<RegCanastilla> listaCanastilla(String codCliente, String nroDoc, boolean tienePedido, int tipoLiquidacion) {

        Vector<RegCanastilla> listaProducto = new Vector<RegCanastilla>();

        mensaje = "";
        SQLiteDatabase db = null;
        int tipoTrans = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            String query = "";

            if (dbFile.exists()) {

                if (tipoLiquidacion > 0) {

                    if (tipoLiquidacion == Const.CANASTILLA_VACIA)
                        tipoTrans = 2;
                    else if (tipoLiquidacion == Const.INVENTARIO_CANASTA)
                        tipoTrans = 4;
                    else if (tipoLiquidacion == Const.DESCARGA_CANASTA) {
                        tipoTrans = 10;
                    }

                    if (tipoLiquidacion == Const.DESCARGA_CANASTA && !existeRegDescargaCanastas()) {

                        query = "SELECT reg.numerodoc as  nroDoc, codigo as Codigo, nombre as Nombre, CASE WHEN reg.presupuesto IS NULL THEN 0 ELSE reg.presupuesto END AS recibido "
                                + "FROM productos as p " + "LEFT JOIN presupuestolinea as reg on p.Codigo = reg.marca "
                                + "AND reg.tipotrans = '" + Const.TIPO_TRANS_CANASTILLA_VACIA + "' OR reg.tipotrans IS NULL "
                                // + "AND nroDoc = '"+nroDoc+"' OR nroDoc IS
                                // NULL "
                                + "WHERE tipocliente= 1 " + "ORDER BY codigo ";

                    } else {

                        query = "SELECT reg.numerodoc as  nroDoc, codigo as Codigo, nombre as Nombre, CASE WHEN reg.presupuesto IS NULL THEN 0 ELSE reg.presupuesto END AS recibido "
                                + "FROM productos as p " + "LEFT JOIN presupuestolinea as reg on p.Codigo = reg.marca "
                                + "AND reg.tipotrans = '" + tipoTrans + "' OR reg.tipotrans IS NULL "
                                // + "AND nroDoc = '"+nroDoc+"' OR nroDoc IS
                                // NULL "
                                + "WHERE tipocliente= 1 " + "ORDER BY codigo ";

                    }
                } else {

                    if (tienePedido) {

                        query = "SELECT reg.numerodoc as  nroDoc, codigo as Codigo, nombre as Nombre, CASE WHEN reg.presupuesto IS NULL THEN 0 ELSE reg.presupuesto END AS recibido, CASE WHEN reg.presupuesto2 IS NULL THEN 0 ELSE reg.presupuesto2 END AS entregado "
                                + "FROM productos as p "
                                + "LEFT JOIN presupuestolinea as reg on p.Codigo = reg.marca "
                                + "AND nroDoc  = '" + nroDoc + "' OR nroDoc IS NULL "
                                + "AND vendedor ='" + codCliente + "' "
                                + "WHERE tipocliente= 1 "
                                + "ORDER BY codigo";

                    } else {

                        query = "SELECT reg.numerodoc as  nroDoc, codigo as Codigo, nombre as Nombre, reg.presupuesto AS recibido "
                                + "FROM productos as p "
                                + "LEFT JOIN presupuestolinea as reg on p.Codigo = reg.marca "
                                + "AND nroDoc  = '" + nroDoc + "' OR nroDoc IS NULL "
                                + "AND vendedor ='" + codCliente + "' "
                                + "WHERE tipocliente= 1 "
                                + "ORDER BY codigo";
                    }

                }


                System.out.println("Query listaCanastilla--> " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {


                    do {
                        RegCanastilla producto = new RegCanastilla();
                        producto.nroDoc = cursor.getString(cursor.getColumnIndex("nroDoc"));
                        producto.codigo = cursor.getString(cursor.getColumnIndex("Codigo"));
                        producto.descripcion = obtenerNombreCanastilla(producto.codigo);
                        producto.descripcion = cursor.getString(cursor.getColumnIndex("Nombre"));
                        producto.devuelta = cursor.getString(cursor.getColumnIndex("recibido"));

                        if (tienePedido)
                            producto.entregada = cursor.getString(cursor.getColumnIndex("entregado"));

                        listaProducto.addElement(producto);
                    } while (cursor.moveToNext());

                }
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            closeDataBase(db);
        }
        return listaProducto;
    }


    //Tabla de inserci�n para guardarla sesi�n de cliente

    public static boolean GuardarSesionCliente(String codPDV, int tipoCliente) {

        SQLiteDatabase db = null;
        Usuario usuario = DataBaseBOJ.ObtenerUsuario();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            db.execSQL("DELETE FROM sessioncliente");
            String nroDoc = Util.generarNumdoc("SC", codPDV, usuario.codigoVendedor);

            ContentValues values = new ContentValues();
            values.put("NroDoc", nroDoc);
            values.put("CodCliente", codPDV);
            values.put("TipoCliente", tipoCliente);
            values.put("UsuarioSys", usuario.codigoVendedor);
            db.insertOrThrow("sessioncliente", null, values);

            Log.i("GuardarSesionCliente", "Guardo Sesion de: " + codPDV);
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("DataBaseBO - GuardarSesionCliente", mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }


    public static boolean guardarRegCanastilla(Vector<RegCanastilla> listaItemCanastillaToSave, boolean tienePedido, int tipoLiquidacion, String codUsuario) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;
        int tipoTrans = 0;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            if (listaItemCanastillaToSave != null) {

                System.out.println("Numero de Items a Guardar ---> " + listaItemCanastillaToSave.size());

                for (RegCanastilla item : listaItemCanastillaToSave) {


                    item.consecutivo = "RC" + item.codigo + Util.FechaActual("yyyyMMddHHmmss");

                    ContentValues values = new ContentValues();


                    values.put("bodega", item.vendedor);
                    values.put("marca", item.codigo);
                    values.put("presupuesto", item.devuelta);
                    values.put("fecharegistro", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
                    values.put("consecutivo", item.consecutivo);
                    values.put("numerodoc", item.nroDoc);
                    values.put("vendedor", codUsuario);

                    if (tipoLiquidacion > 0) {

                        if (tipoLiquidacion == Const.CANASTILLA_VACIA)
                            tipoTrans = 2;
                        else if (tipoLiquidacion == Const.INVENTARIO_CANASTA)
                            tipoTrans = 4;
                        else if (tipoLiquidacion == Const.DESCARGA_CANASTA)
                            tipoTrans = 10;

                        values.put("presupuesto2", 0);
                        values.put("tipotrans", tipoTrans);

                    } else {

                        if (tienePedido) {
                            values.put("presupuesto2", item.entregada);
                            values.put("tipotrans", 1);

                        } else {
                            values.put("presupuesto2", 0);
                            values.put("tipotrans", 9);
                        }


                    }


                    values.put("fechasinc", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
                    values.put("fechallegada", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
                    values.put("numerodoc", item.nroDoc);
                    values.put("sincronizado", 0);

                    String[] whereAgrs = new String[]{item.codigo, item.nroDoc}; //item.codCliente

                    System.out.println(item.codigo + " - " + item.codCliente + " - " + item.nroDoc);

                    db.delete("presupuestolinea", "marca = ?  AND numerodoc = ?", whereAgrs); //AND vendedor = ?
                    dbPedido.delete("presupuestolinea", "marca = ?  AND numerodoc = ?", whereAgrs); //AND vendedor = ?


                    db.insertOrThrow("presupuestolinea", null, values);
                    dbPedido.insertOrThrow("presupuestolinea", null, values);


                }
            }


            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarClienteNuevo: " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }

    }

    public static void eliminarClientePropio(String codCliente) {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM Clientes  WHERE Codigo = '" + codCliente + "'");

            } else {

            }

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();

        }
    }


    public static boolean insertarInformacionClientePropio(Cliente cliente) {

        SQLiteDatabase db = null;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values = new ContentValues();

            values.put("Codigo", cliente.codigo);
            values.put("Nombre", cliente.Nombre);
            values.put("RazonSocial", cliente.razonSocial);
            values.put("portafolio", cliente.portafolio);
            values.put("CodigoAmarre", cliente.CodigoAmarre);

            db.insertOrThrow("Clientes", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();

        }
    }


    public static String ObtenerPortafolioDeClientes() {

        SQLiteDatabase db = null;
        String portafolio = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT distinct portafolio as portafolio  FROM Clientes";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst())
                portafolio = cursor.getString(cursor.getColumnIndex("portafolio"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return portafolio;
    }


    public static String ObtenerCodigoAmarreDeClientes() {

        SQLiteDatabase db = null;
        String codigoAmarre = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT distinct CodigoAmarre as CodigoAmarre  FROM Clientes";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst())
                codigoAmarre = cursor.getString(cursor.getColumnIndex("CodigoAmarre"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return codigoAmarre;
    }


    public static boolean ExisteCargue(String codigoCliente) {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select count(*) as total from encabezadosugerido where Codigo = '" + codigoCliente + "' and TipoTrans = 3";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getInt(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static Vector<RegCanastilla> getImpresionCanastillas(String numeroDoc, boolean isLiquidacionCanasta) {
        SQLiteDatabase db = null;
        String sql = "";
        RegCanastilla canastilla = null;
        Vector<RegCanastilla> vCanastillas = new Vector<RegCanastilla>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            if (!isLiquidacionCanasta)
                sql = "SELECT marca, presupuesto, presupuesto2 FROM presupuestolinea WHERE numerodoc = '" + numeroDoc + "' AND presupuesto2 IS NOT NULL ";
            else
                sql = "SELECT marca, presupuesto FROM presupuestolinea WHERE numerodoc = '" + numeroDoc + "'  ";

            System.out.println("getImpresionCanastillas: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    canastilla = new RegCanastilla();

                    canastilla.codigo = cursor.getString(cursor.getColumnIndex("marca"));
                    canastilla.descripcion = obtenerNombreCanastilla(canastilla.codigo);

                    if (!isLiquidacionCanasta)
                        canastilla.nroEntrega = cursor.getInt(cursor.getColumnIndex("presupuesto2"));

                    canastilla.nroDevuelto = cursor.getInt(cursor.getColumnIndex("presupuesto"));

                    vCanastillas.add(canastilla);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vCanastillas;
    }

    public static Vector<ImpresionDetalle> getImpresionCanastillasInv(String numeroDoc, boolean isLiquidacionCanasta) {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionDetalle detalle = null;
        Vector<ImpresionDetalle> vDetalle = new Vector<ImpresionDetalle>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "SELECT  producto, p.nombre nombre, cantidadinventario, cantidaddigitada, diferencia "
                    + "FROM liquidaciondiferencias l "
                    + "inner join productos p on l.producto = p.codigo "
                    + "WHERE tipo = 'CANASTAS'";

            System.out.println("getImpresionCanastillas: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    detalle = new ImpresionDetalle();

                    detalle.codigo = cursor.getString(cursor.getColumnIndex("producto"));
                    detalle.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    detalle.cantidadInventario = cursor.getInt(cursor.getColumnIndex("cantidadinventario"));
                    detalle.cantidadDigitada = cursor.getInt(cursor.getColumnIndex("cantidaddigitada"));
                    detalle.diferencia = cursor.getInt(cursor.getColumnIndex("diferencia"));

                    vDetalle.add(detalle);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vDetalle;
    }


    private static String obtenerNombreCanastilla(String codigoCanastilla) {

        SQLiteDatabase db = null;
        String nombre = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT nombre FROM Productos where codigo ='" + codigoCanastilla + "' and TipoCliente = 1 ";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst())
                nombre = cursor.getString(cursor.getColumnIndex("nombre"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return nombre;
    }


    public static RegCanastilla obtenerCantidadMinimaCanastillas(String codigoCanasta, String nroDocSesion) {

        RegCanastilla canasta = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = ""
                    + "SELECT p.tipo_canasta AS codCanasta, "
                    + "		  sum(d.cantidad) AS Cantidad, "
                    + "       round(sum(d.cantidad / p.factor_canasta), 0) AS CantidadCanasta "
                    + "FROM ENCABEZADO e "
                    + "       INNER JOIN DETALLE d ON e.numerodoc = d.Numdoc "
                    + "       INNER JOIN productos p ON d.CodigoRef = p.Codigo "
                    + "WHERE   p.tipo_canasta <> '' AND "
                    + "		   e.numeroDoc = '" + nroDocSesion + "' AND "
                    + "		   p.tipo_canasta = '" + codigoCanasta + "' "
                    + "GROUP BY p.tipo_canasta;";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {


                canasta = new RegCanastilla();
                canasta.codigo = cursor.getString(cursor.getColumnIndex("codCanasta"));
                canasta.cantidad = cursor.getInt(cursor.getColumnIndex("Cantidad"));

                double cantRendondeado = cursor.getDouble(cursor.getColumnIndex("CantidadCanasta"));
                cantRendondeado = Math.ceil(cantRendondeado);
                canasta.entregaMin = (int) cantRendondeado;

                System.out.println(" " + cantRendondeado + " -- " + canasta.entregaMin);

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return canasta;
    }


    public static boolean tieneRegistroCanastilla(String codigoCanasta, String nroDocSesion) {

        SQLiteDatabase db = null;
        int total = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select count (*) as Total from ( "
                    + "select d.codigoref,sum(cantidad) cantidad,factor_canasta, tipo_canasta  "
                    + "from encabezado e inner join detalle d on e.numerodoc = d.numdoc "
                    + "inner join productos p on p.codigo = d.codigoref "
                    + "where numerodoc = '" + nroDocSesion + "' "
                    + "group by d.codigoref "
                    + ") tmp WHERE tipo_canasta = '" + codigoCanasta + "' group by tmp.tipo_canasta", null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("Total"));


            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean existeRegCanastilla(String nroDocSesion) {

        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select count(*) as total from presupuestolinea where numerodoc = '" + nroDocSesion + "' ";
            System.out.println("existeRegCanastilla " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }

    public static boolean existeRegDescargaCanastas() {

        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select count(*) as total from presupuestolinea where tipoTrans = " + Const.TIPO_TRANS_ESCARGA_CANASTA;
            System.out.println("existeRegCanastillaVacia " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean RegistrarPedidoSugerido(Encabezado encabezado, Hashtable<String, Detalle> detalleTmp,
                                                  String version, String imei, boolean isAutoVenta, String nroDocAmarre, int tipoTransaccion, String codLiquidador) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            Usuario usuario = DataBaseBOJ.CargarUsuario();


            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            /************************************
             * Se Ingresa el Encabezado del Producto
             ************************************/

            String CodigoAmarre = isAutoVenta ? nroDocAmarre : "";
            int tipoTrans = 0;
            String observacion = "";
            String fp = "";

            if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {

                if (tipoTransaccion > 0) {

                    switch (tipoTransaccion) {
                        case Const.IS_PRODUCTO_DANADO:
                            tipoTrans = Const.TIPO_TRANS_PRODUCTO_DANADO;
                            observacion = "INV FISICO DANADO";
                            fp = "FPA2";
                            break;

                        case Const.IS_AVERIA_TRANSPORTE:
                            tipoTrans = Const.TIPO_TRANS_AVERIA_TRANSPORTE;
                            observacion = "Averias por transporte";
                            fp = "FPA2";
                            break;

                        case Const.IS_INVENTARIO_LIQUIDACION:
                            tipoTrans = Const.TIPO_TRANS_INVENTARIO_LIQUIDACION;
                            observacion = "INV FISICO PRODUCTO BUENO";
                            fp = "FPA2";
                            break;

                        default:
                            break;
                    }

                } else {

                    tipoTrans = Const.TIPO_TRANS_CARGUE;
                    observacion = encabezado.observacion;
                }
            } else {
                observacion = encabezado.observacion;
            }

            values = new ContentValues();

            values.put("Codigo", encabezado.codigo_cliente);
            values.put("NumeroDoc", encabezado.numero_doc);
            values.put("FechaTrans", fechaActual);
            values.put("TipoTrans", tipoTrans);
            values.put("MontoFact", Util.Redondear(String.valueOf(encabezado.valor_neto), 2));
            values.put("Desc1", Util.Redondear(String.valueOf(encabezado.valor_descuento), 2));
            values.put("Desc2", Util.Redondear(String.valueOf(encabezado.valor_descuento), 2));
            values.put("Flete", "0");
            values.put("Costo", "0");
            values.put("Iva", encabezado.total_iva);
            values.put("Usuario", Main.usuario.codigoVendedor);
            values.put("Sincronizado", "0");
            values.put("Telefono", Main.usuario.bodega);
            values.put("Observaciones", observacion);
            values.put("Bodega", Main.usuario.bodega);
            values.put("FechaSinc", encabezado.hora_final);
            values.put("OrdenCompra", "");
            values.put("Fechainicial", encabezado.hora_inicial);
            values.put("FechaFinal", encabezado.hora_final);
            values.put("Tipocliente", "");
            values.put("fechareal", fechaActual);
            values.put("Fechaentrega", encabezado.fechaEntrega);
            values.put("entregado", "0");
            values.put("factura", "");
            values.put("FP", "");
            values.put("anulado", "0");
            values.put("impreso", "0");
            values.put("liquidador", codLiquidador);
            values.put("FP2", CodigoAmarre);
            values.put("FP", 0);

            db.insertOrThrow("encabezadosugerido", null, values);
            dbPedido.insertOrThrow("encabezadosugerido", null, values);

            /************************************
             * Se Ingresa el Detalle del Producto
             ************************************/

            Enumeration<Detalle> e = detalleTmp.elements();
            Detalle detalle;

            int contItem = 1;

            while (e.hasMoreElements()) {

                detalle = e.nextElement();
                values = new ContentValues();

                values.put("Numdoc", encabezado.numero_doc);
                values.put("Fecha", fechaActual);
                values.put("CodigoRef", detalle.codProducto);
                values.put("Precio", Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + detalle.precio), 2)));
                values.put("TarifaIva", detalle.iva);
                values.put("DescuentoRenglon", detalle.descuento);
                values.put("Cantidad", detalle.cantidad);
                values.put("Sincronizado", "0");
                values.put("vendedor", Main.usuario.codigoVendedor);
                values.put("Cantidadcambio", String.valueOf(detalle.cantidad * detalle.factorCajas));
                values.put("Bodega", Main.usuario.bodega);
                values.put("Bodega2", "");
                values.put("Item", detalle.posicion + "");
                values.put("Inventario", detalle.cantidad);
                values.put("Tipocliente", "0");
                values.put("Motivo", "");
                values.put("tipoventa", "0");
                values.put("anulado", "0");

                contItem++;

                db.insertOrThrow("detallesugerido", null, values);
                dbPedido.insertOrThrow("detallesugerido", null, values);

            }


            return true;
        } catch (Exception e) {
            mensaje = e.getMessage();
            return false;
        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }


    public static int ObtenerFactorCanastaProducto(String codigo) {

        int factorCanasta = 1;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("select factor_canasta  from productos where codigo = '" + codigo + "'", null);

            if (cursor.moveToFirst())
                factorCanasta = cursor.getInt(cursor.getColumnIndex("factor_canasta"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return factorCanasta;
    }


    public static Vector<ImpresionDetalle> getImpresionDetalleSugerido(String numeroDoc) {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionDetalle impDet = null;
        Vector<ImpresionDetalle> vImpDet = new Vector<ImpresionDetalle>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select p.codigo as codigo, p.nombre as nombre, d.precio as precio, d.Cantidadcambio as cantidad ,d.cantidad as cantidadCajas "
                    + "from detallesugerido d "
                    + "inner join productos p on p.codigo = d.[CodigoRef] "
                    + "where numDoc = '" + numeroDoc + "' order by p.orden asc ";


            System.out.println("ImpresionDetalleProductos: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impDet = new ImpresionDetalle();

                    impDet.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impDet.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impDet.precio = cursor.getFloat(cursor.getColumnIndex("precio"));
                    impDet.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    impDet.cantidadCajas = cursor.getInt(cursor.getColumnIndex("cantidadCajas"));

                    vImpDet.add(impDet);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpDet;
    }


    public static Impresion getImpresionClienteSugerido(String numeroDoc) {

        SQLiteDatabase db = null;
        String sql = "";
        Impresion impClient = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select c.Codigo as codigo , c.Nombre as nombre "
                    + "from encabezadosugerido e "
                    + "inner join vendedor c on c.Codigo = e.codigo "
                    + "where e.numeroDoc = '" + numeroDoc + "' ";

            System.out.println("Impresion infoCliente---> " + sql);


            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impClient = new Impresion();

                    impClient.razonSocial = cursor.getString(cursor.getColumnIndex("nombre"));
                    impClient.nit = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    impClient.nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    impClient.direccion = ""; //cursor.getString( cursor.getColumnIndex( "direccion" ) );
                    impClient.telefono = "";//cursor.getString( cursor.getColumnIndex( "telefono" ) );
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return impClient;
    }


    public static boolean LogInLiquidadores(String usuario, String password, boolean tipoCargo) {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            String cargo = tipoCargo ? "liquidadores" : "jefes";
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT cedula FROM " + cargo + " WHERE cedula = '" + usuario + "' AND clave = '" + password + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }


    public static Vector<Usuario> ListaLiquidadores(Vector<String> listaItems, boolean tipoLiquidador) {

        mensaje = "";
        Usuario usuario;

        SQLiteDatabase db = null;
        Vector<Usuario> listaUsuarios = new Vector<Usuario>();

        try {

            String cargo = tipoLiquidador ? "liquidadores" : "jefes";
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT cedula, nombre FROM " + cargo, null);

            if (cursor.moveToFirst()) {

                do {

                    usuario = new Usuario();
                    usuario.codigoVendedor = cursor.getString(cursor.getColumnIndex("cedula"));
                    usuario.nombreVendedor = cursor.getString(cursor.getColumnIndex("nombre"));

                    listaItems.addElement(usuario.nombreVendedor);
                    listaUsuarios.addElement(usuario);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return listaUsuarios;
    }


    public static EstadisticaRecorrido getInformeResumenVenta() {

        mensaje = "";
        SQLiteDatabase db = null;
        EstadisticaRecorrido estadisticaRecorrido = new EstadisticaRecorrido();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = ""
                        + " SELECT COUNT(*) AS visitas, (select ifnull(sum(e.montofact),0) montofact  from encabezado e where codigo in (select codigo from clientes where tipocredito = 'FPA2') AND ifnull(anulado , 0)=0 and tipotrans = 0) AS venta_contado,  "
                        + "(SELECT ifnull(sum(e.montofact),0) montofact  from encabezado e where codigo in (select codigo from clientes where tipocredito = 'FPA2') and ifnull(anulado , 0) = 0 and tipotrans =  2) AS dev_contado,   "
                        + "(SELECT ifnull(sum(e.montofact),0) montofact from encabezado e where codigo in (select codigo from clientes where tipocredito <> 'FPA2') and ifnull(anulado , 0) = 0 and tipotrans = 0) AS venta_credito,   "
                        + "(SELECT ifnull(sum(e.montofact),0) montofact from encabezado e where codigo in (select codigo from clientes where tipocredito <> 'FPA2') and ifnull(anulado , 0) = 0 and tipotrans = 2) AS dev_credito "
                        + "FROM Encabezado "
                        + "WHERE fechaFinal IS NOT NULL";

                Cursor cursor = db.rawQuery(query, null);

                System.out.println("Consulta del Recorridoo: " + query);

                if (cursor.moveToFirst()) {
                    estadisticaRecorrido.venta_contado = cursor.getFloat(cursor.getColumnIndex("venta_contado"));
                    estadisticaRecorrido.dev_contado = cursor.getFloat(cursor.getColumnIndex("dev_contado"));
                    estadisticaRecorrido.venta_credito = cursor.getFloat(cursor.getColumnIndex("venta_credito"));
                    estadisticaRecorrido.dev_credito = cursor.getFloat(cursor.getColumnIndex("dev_credito"));
                    estadisticaRecorrido.totalVentaBruta = (long) (estadisticaRecorrido.venta_contado + estadisticaRecorrido.venta_credito);
                    estadisticaRecorrido.montoDevoluciones = estadisticaRecorrido.dev_contado + estadisticaRecorrido.dev_credito;
                    estadisticaRecorrido.totalGlobal = estadisticaRecorrido.totalVentaBruta - estadisticaRecorrido.montoDevoluciones;
                }

                if (cursor != null)
                    cursor.close();

            } else {
                Log.e(TAG, "CargarEstadisticas -> No Existe la Base de Datos");
            }
        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarEstadisticas -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return estadisticaRecorrido;
    }

    public static Vector<FormaPago> getImpresionVentaContado() {
        SQLiteDatabase db = null;
        String sql = "";
        FormaPago impEnc = null;
        Vector<FormaPago> vImpEnc = new Vector<FormaPago>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "select t.id, max(t.tipo_pago) tipopago, ifnull(SUM(F.MONTO),0) MONTO  "
                    + "from tipos_pago t "
                    + "left join formapago F on t.id = f.forma_pago  "
                    + "where t.id <> 1 "
                    + "group by t.id order by t.id ";

            System.out.println("getImpresionVentaContado: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {


                impEnc = new FormaPago();
                impEnc.descripcion = cursor.getString(cursor.getColumnIndex("tipopago"));

                do {

                    impEnc = new FormaPago();
                    impEnc.descripcion = cursor.getString(cursor.getColumnIndex("tipopago"));
                    impEnc.monto = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                    vImpEnc.add(impEnc);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpEnc;
    }

    public static Vector<FormaPago> getImpresionVentaCredito() {
        SQLiteDatabase db = null;
        String sql = "";
        FormaPago impEnc = null;
        Vector<FormaPago> vImpEnc = new Vector<FormaPago>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select t.id, max(t.tipo_pago) tipopago, ifnull(SUM(F.MONTO),0) MONTO  "
                    + "from tipos_pago t "
                    + "left join formapago F on t.id = f.forma_pago   "
                    + "group by t.id order by t.id ";

            System.out.println("getImpresionVentaContado: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impEnc = new FormaPago();

                    impEnc.descripcion = cursor.getString(cursor.getColumnIndex("tipopago"));
                    impEnc.monto = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                    vImpEnc.add(impEnc);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpEnc;
    }

    public static Vector<FormaPago> getImpresionGranTotal() {
        SQLiteDatabase db = null;
        String sql = "";
        FormaPago impEnc = null;
        Vector<FormaPago> vImpEnc = new Vector<FormaPago>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select 'VENTAS GRAVADAS' DESCRIPCION,round(sum((precio*cantidad)) - round(sum(((precio*descuentorenglon/100)*cantidad)),0) + "
                    + "round(sum((((precio-(precio*descuentorenglon/100))*tarifaiva/100)*cantidad)),2),0) TOTAL, 1 orden "
                    + "FROM encabezado e inner join detalle d on e.numerodoc = d.numdoc where e.TipoTrans = 0 and ifnull(e.anulado,0)=0 "
                    + "and d.tarifaiva <> 0 "
                    + "UNION "
                    + "select 'VENTAS NO GRAVADAS' DESCRIPCION,ifnull(round(sum((precio*cantidad)) - round(sum(((precio*descuentorenglon/100)*cantidad)),0) +  "
                    + "round(sum((((precio-(precio*descuentorenglon/100))*tarifaiva/100)*cantidad)),2),0),0) TOTAL, 2 orden "
                    + "from encabezado e inner join detalle d on e.numerodoc = d.numdoc where e.TipoTrans = 0 and ifnull(e.anulado,0)=0 "
                    + "and d.tarifaiva = 0 "
                    + "UNION "
                    + "select 'GRAN TOTAL: ' DESCRIPCION,MAX(totalz) + MAX(TMP.TOTAL) TOTAL, 3 orden from consecutivosautoventa C LEFT JOIN  "
                    + "(select E.Usuario, ifnull(round(sum((precio*cantidad)) - round(sum(((precio*descuentorenglon/100)*cantidad)),0) +  "
                    + "round(sum((((precio-(precio*descuentorenglon/100))*tarifaiva/100)*cantidad)),2),0),0) TOTAL "
                    + "from encabezado e inner join detalle d on e.numerodoc = d.numdoc where e.TipoTrans = 0 and ifnull(e.anulado,0)=0 GROUP BY E.Usuario) "
                    + "TMP ON C.vendedor = TMP.Usuario "
                    + "union "
                    + "SELECT 'PROXIMA FACTURA:' DESCRIPCION,Consecutivo total,4 orden FROM consecutivosautoventa "
                    + "order by orden";

            System.out.println("getImpresionVentaContado: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impEnc = new FormaPago();

                    impEnc.descripcion = cursor.getString(cursor.getColumnIndex("DESCRIPCION"));
                    impEnc.monto = cursor.getFloat(cursor.getColumnIndex("TOTAL"));

                    vImpEnc.add(impEnc);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpEnc;
    }

    public static Vector<FormaPago> getImpresionLiquidacion() {
        SQLiteDatabase db = null;
        String sql = "";
        FormaPago impEnc = null;
        Vector<FormaPago> vImpEnc = new Vector<FormaPago>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select t.id, max(t.tipo_pago) tipopago, ifnull(SUM(F.MONTO),0) MONTO  "
                    + "from tipos_pago t "
                    + "left join formapago F on t.id = f.forma_pago  "
                    + "where t.id <> 1 "
                    + "group by t.id order by t.id ";

            System.out.println("getImpresionVentaContado: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    impEnc = new FormaPago();

                    impEnc.descripcion = cursor.getString(cursor.getColumnIndex("tipopago"));
                    impEnc.monto = cursor.getFloat(cursor.getColumnIndex("MONTO"));

                    vImpEnc.add(impEnc);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpEnc;
    }


    public static Vector<ImpresionEncabezado> getImpresionEncabezadoContado() {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionEncabezado impEnc = null;
        Vector<ImpresionEncabezado> vImpEnc = new Vector<ImpresionEncabezado>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "select factura, case when iva > 0 then montofact-iva else 0 end montosiniva, montofact montoconiva, "
                    + "CASE WHEN anulado IS NULL THEN 0 ELSE anulado END AS anulado "
                    + "FROM encabezado e "
                    + "where codigo in (select codigo from clientes where tipocredito = 'FPA2') and tipotrans = 0 order by factura asc";

            System.out.println("getImpresionEncabezadoContado: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                //				int cont = 1;

                do {

                    impEnc = new ImpresionEncabezado();

                    //					impEnc.numero_doc = String.valueOf(cont);
                    impEnc.numero_doc = cursor.getString(cursor.getColumnIndex("factura"));
                    impEnc.valor_descuento = cursor.getFloat(cursor.getColumnIndex("montosiniva"));
                    impEnc.valor_neto = cursor.getFloat(cursor.getColumnIndex("montoconiva"));
                    impEnc.anulado = cursor.getInt(cursor.getColumnIndex("anulado"));

                    //					cont++;

                    vImpEnc.add(impEnc);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpEnc;
    }

    public static Vector<ImpresionEncabezado> getImpresionEncabezadoCredito() {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionEncabezado impEnc = null;
        Vector<ImpresionEncabezado> vImpEnc = new Vector<ImpresionEncabezado>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "select factura, case when iva > 0 then montofact-iva else 0 end montosiniva, montofact montoconiva, "
                    + "CASE WHEN anulado IS NULL THEN 0 ELSE anulado END AS anulado "
                    + "FROM encabezado e "
                    + "where codigo in (select codigo from clientes where tipocredito <> 'FPA2') and tipotrans = 0 order by factura asc";

            System.out.println("getImpresionEncabezadoCredito: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                //				int cont = 1;

                do {

                    impEnc = new ImpresionEncabezado();

                    //					impEnc.numero_doc = String.valueOf(cont);
                    impEnc.numero_doc = cursor.getString(cursor.getColumnIndex("factura"));
                    impEnc.valor_descuento = cursor.getFloat(cursor.getColumnIndex("montosiniva"));
                    impEnc.valor_neto = cursor.getFloat(cursor.getColumnIndex("montoconiva"));
                    impEnc.anulado = cursor.getInt(cursor.getColumnIndex("anulado"));

                    //					cont++;

                    vImpEnc.add(impEnc);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpEnc;
    }

    public static Vector<ImpresionEncabezado> getImpresionEncabezadoAnulado() {
        SQLiteDatabase db = null;
        String sql = "";
        ImpresionEncabezado impEnc = null;
        Vector<ImpresionEncabezado> vImpEnc = new Vector<ImpresionEncabezado>();


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            sql = "select factura, case when iva > 0 then montofact-iva else 0 end montosiniva, montofact montoconiva, "
                    + "CASE WHEN anulado IS NULL THEN 0 ELSE anulado END AS anulado "
                    + "FROM encabezado e where tipotrans = 0 order by factura asc";

            System.out.println("getImpresionEncabezadoContado: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                //				int cont = 1;

                do {

                    impEnc = new ImpresionEncabezado();

                    //					impEnc.numero_doc = String.valueOf(cont);
                    impEnc.numero_doc = cursor.getString(cursor.getColumnIndex("factura"));
                    impEnc.valor_descuento = cursor.getFloat(cursor.getColumnIndex("montosiniva"));
                    impEnc.valor_neto = cursor.getFloat(cursor.getColumnIndex("montoconiva"));
                    impEnc.anulado = cursor.getInt(cursor.getColumnIndex("anulado"));

                    //					cont++;

                    vImpEnc.add(impEnc);

                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return vImpEnc;
    }


    public static Vector<ImpresionRecaudo> getInformeRecaudo() {
        SQLiteDatabase db = null;
        String sql = "";

        ImpresionRecaudo rr;
        Vector<ImpresionRecaudo> vRR = new Vector<ImpresionRecaudo>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select CodigoCliente, nroDoc, Total from encabezadorecaudo order by fecha_recaudo asc  ";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    rr = new ImpresionRecaudo();

                    rr.codigo = cursor.getString(cursor.getColumnIndex("CodigoCliente"));
                    rr.numero_doc = cursor.getString(cursor.getColumnIndex("nroDoc"));
                    rr.sub_total = cursor.getFloat(cursor.getColumnIndex("Total"));

                    vRR.add(rr);
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            if (db != null)
                db.close();
        }

        return vRR;
    }

    public static Vector<ImpresionNotaCredito> getInformeNotasCredito() {
        SQLiteDatabase db = null;
        String sql = "";

        ImpresionNotaCredito rr;
        Vector<ImpresionNotaCredito> vRR = new Vector<ImpresionNotaCredito>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select numeroDoc, case when iva > 0 then montoFact-iva else 0 end montosiniva, montoFact montoconiva "
                    + "from encabezado e "
                    + "where IFNULL(anulado,0) = 0 and TipoTrans = 2 "
                    + "order by factura asc";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    rr = new ImpresionNotaCredito();

                    rr.numero_doc = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                    rr.valorSinIva = cursor.getFloat(cursor.getColumnIndex("montosiniva"));
                    rr.valor_neto = cursor.getFloat(cursor.getColumnIndex("montoconiva"));

                    vRR.add(rr);
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            if (db != null)
                db.close();
        }

        return vRR;
    }


    public static boolean existeLiquidacionDatos() {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total FROM LiquidacionDatos ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getInt(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }

    public static boolean existeRegistroLiquidacion() {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select COUNT(1) CANTIDAD FROM encabezadosugerido "
                    + "where Observaciones in ('INV FISICO DANADO','Averias por transporte','INV FISICO PRODUCTO BUENO');";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("CANTIDAD"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean existeRegLiquidacionCanastilla(int tipoLiquidacion) {
        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;
        int tipoTrans = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            if (tipoLiquidacion == Const.CANASTILLA_VACIA)
                tipoTrans = 2;
            else if (tipoLiquidacion == Const.INVENTARIO_CANASTA)
                tipoTrans = 4;
            else if (tipoLiquidacion == Const.IS_DESCARGA_CANASTA)
                tipoTrans = 10;


            String query = "SELECT COUNT(*) AS total FROM presupuestolinea WHERE tipotrans = " + tipoTrans;

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    total = cursor.getInt(cursor.getColumnIndex("total"));

                } while (cursor.moveToNext());

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }

    public static boolean existeRegLiquidacionProductos(String tipo) {
        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total FROM LIQUIDACIONDIFERENCIAS WHERE tipo = '" + tipo + "' ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }

    public static boolean existeRegLiquidacionAveria(int tipoTrans) {
        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total FROM encabezadosugerido WHERE TipoTrans = '" + tipoTrans + "' ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static String obtenerNroDocRegLiquidacionCanastas(int tipoLiquidacion) {
        mensaje = "";
        String nroDoc = "";
        SQLiteDatabase db = null;
        int tipoTrans = 0;

        try {

            Usuario user = DataBaseBOJ.ObtenerUsuario();
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            if (tipoLiquidacion == Const.CANASTILLA_VACIA)
                tipoTrans = 2;
            else if (tipoLiquidacion == Const.INVENTARIO_CANASTA)
                tipoTrans = 4;
            else if (tipoLiquidacion == Const.IS_DESCARGA_CANASTA)
                tipoTrans = 10;


            String query = "SELECT numerodoc FROM presupuestolinea WHERE bodega  = '" + user.codigoVendedor + "' AND tipotrans = " + tipoTrans;

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDoc = cursor.getString(cursor.getColumnIndex("numerodoc"));


                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return nroDoc;
    }

    public static String obtenerNroDocRegLiquidacionProductos(String observacion) {
        mensaje = "";
        String nroDoc = "";
        SQLiteDatabase db = null;

        try {

            Usuario user = DataBaseBOJ.ObtenerUsuario();
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "SELECT numeroDoc from encabezado where observaciones= '" + observacion + "' AND FP2 = '' "
                    + "order by  factura desc "
                    + "limit 1 ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDoc = cursor.getString(cursor.getColumnIndex("numeroDoc"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return nroDoc;
    }

    public static String obtenerNroDocRegLiquidacionAveria(int tipoTrans) {
        mensaje = "";
        String nroDoc = "";
        SQLiteDatabase db = null;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT NumeroDoc FROM encabezadosugerido WHERE TipoTrans = " + tipoTrans;

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDoc = cursor.getString(cursor.getColumnIndex("NumeroDoc"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return nroDoc;
    }


    public static String obtenerNroDocRegLiquidacionProductosSugerido(int tipoTrans) {
        mensaje = "";
        String nroDoc = "";
        SQLiteDatabase db = null;

        try {

            Usuario user = DataBaseBOJ.ObtenerUsuario();
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "SELECT NumeroDoc FROM encabezadosugerido where TipoTrans = " + tipoTrans;


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDoc = cursor.getString(cursor.getColumnIndex("NumeroDoc"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return nroDoc;
    }


    public static ConsecutivoFactura obtenerConsecutivoFactura() {

        SQLiteDatabase db = null;
        ConsecutivoFactura consecutivo = null;
        int actualizarConsecutivo = 0;


        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT id, vendedor, prefijo,  consecutivo, totalz FROM consecutivosautoventa ", null);

            if (cursor.moveToFirst()) {

                consecutivo = new ConsecutivoFactura();

                consecutivo.codigoConsecutivo = cursor.getString(cursor.getColumnIndex("id"));
                consecutivo.vendedor = cursor.getString(cursor.getColumnIndex("vendedor"));
                consecutivo.prefijo = cursor.getString(cursor.getColumnIndex("prefijo"));
                consecutivo.nroConsecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));
                actualizarConsecutivo = cursor.getInt(cursor.getColumnIndex("consecutivo"));
                consecutivo.total = cursor.getInt(cursor.getColumnIndex("totalz"));

                consecutivo.StrConsecutivo = consecutivo.prefijo + Util.lpad("" + consecutivo.nroConsecutivo, 7, "0");
                actualizarConsecutivo = actualizarConsecutivo + 1;
                db.execSQL("UPDATE consecutivosautoventa SET consecutivo = " + actualizarConsecutivo);

            } else {

                consecutivo = new ConsecutivoFactura();
                consecutivo.StrConsecutivo = "N/A";
                ;

            }

            if (cursor != null)
                cursor.close();

            return consecutivo;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenerConsecutivo", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }
        return consecutivo;
    }

    public static boolean existeConsecutivoAutoventa() {

        SQLiteDatabase db = null;
        int total = 0;


        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT COUNT(*) as total FROM consecutivosautoventa ", null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));
            }

            if (cursor != null)
                cursor.close();


        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ObtenerConsecutivo", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }
        return total > 0;
    }

    public static boolean ExisteAveriaTransporte(String codigoCliente, int tipoTrans) {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "SELECT count(*) as total "
                    + "FROM encabezadosugerido "
                    + "WHERE Codigo = '" + codigoCliente + "' "
                    + "AND TipoTrans = " + tipoTrans;

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean guardarRegSelloCamion(SelloCamion regSello) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            File fileTemp = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(fileTemp.getPath(), null,
                    SQLiteDatabase.OPEN_READWRITE);

            ContentValues values = new ContentValues();
            values.put("Ruta", regSello.ruta);
            values.put("Liquidador", regSello.liquidador);
            values.put("FechaInicio", regSello.fechaInicial);
            values.put("FechaFinal", regSello.fechaFinal);
            values.put("NumeroSello", regSello.nroSello);
            values.put("NumeroCamion", regSello.nroCamion);
            values.put("sincronizado", regSello.sicronizado);
            values.put("bandera", regSello.bandera);

            db.insertOrThrow("LiquidacionDatos", null, values);
            dbTemp.insertOrThrow("LiquidacionDatos", null, values);

            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "RegNoVisita -> " + mensaje, e);
            return false;

        } finally {

            if (db != null)
                db.close();

            if (dbTemp != null)
                dbTemp.close();
        }


    }

    public static String obtenerMaxFechaCanastaVacia(String codVendedor) {
        mensaje = "";
        SQLiteDatabase db = null;
        String fechaInicial = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "SELECT MAX(fecharegistro) as fechaInicial "
                    + "FROM presupuestolinea "
                    + "WHERE vendedor  = '" + codVendedor + "' AND tipotrans = 2 ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                fechaInicial = cursor.getString(cursor.getColumnIndex("fechaInicial"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return fechaInicial;
    }

    public static boolean isDataSelloSeguridad(String codLiquidador) {


        SQLiteDatabase db = null;
        Cursor cursor = null;
        int total = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
                        SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT COUNT(*) AS Total FROM LiquidacionDatos WHERE Liquidador = '" + codLiquidador + "' ";

                cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    total = cursor.getInt(cursor.getColumnIndex("Total"));
                }

            }
        } catch (Exception e) {
            mensaje = e.getMessage();

        } finally {

            if (db != null)
                closeDataBase(db);

            if (cursor != null)
                cursor.close();


        }
        return total > 0;
    }


    public static OrdenLiquidacion validarOrdenRegistros(String codigoLiquidador) {

        mensaje = "";
        SQLiteDatabase db = null;
        OrdenLiquidacion ordenRecorrido = new OrdenLiquidacion();
        Usuario user = DataBaseBOJ.CargarUsuario();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = ""
                        + " SELECT COUNT (*) totalVacias , "
                        + "(select COUNT(*) FROM encabezadosugerido WHERE TipoTrans = " + Const.TIPO_TRANS_PRODUCTO_DANADO + " AND usuario = '" + user.codigoVendedor + "' ) AS totalproductoDanado, "
                        + "(select COUNT(*) FROM encabezadosugerido WHERE TipoTrans = " + Const.TIPO_TRANS_AVERIA_TRANSPORTE + " AND usuario = '" + user.codigoVendedor + "') AS totalAveria, "
                        + "(select COUNT(*) FROM encabezadosugerido WHERE TipoTrans = " + Const.TIPO_TRANS_INVENTARIO_LIQUIDACION + " AND usuario = '" + user.codigoVendedor + "' ) AS totalInventario, "
                        + "(select COUNT(*) FROM presupuestolinea WHERE tipotrans = " + Const.TIPO_TRANS_INVENTARIO_CANASTA + " AND vendedor = '" + user.codigoVendedor + "') AS totalinventarioCanastas, "
                        + "(select COUNT(*) FROM presupuestolinea WHERE tipotrans = " + Const.TIPO_TRANS_ESCARGA_CANASTA + " AND vendedor = '" + user.codigoVendedor + "' ) AS totalDescargas "
                        + "FROM presupuestolinea WHERE tipotrans = " + Const.TIPO_TRANS_CANASTILLA_VACIA + " AND vendedor = '" + user.codigoVendedor + "' ";

                Cursor cursor = db.rawQuery(query, null);

                System.out.println("Consulta del total de entradas en el mod liquidacion -->  " + query);

                if (cursor.moveToFirst()) {
                    ordenRecorrido.regCanastaVacia = cursor.getInt(cursor.getColumnIndex("totalVacias"));
                    ordenRecorrido.regProductoDanado = cursor.getInt(cursor.getColumnIndex("totalproductoDanado"));
                    ordenRecorrido.regAveria = cursor.getInt(cursor.getColumnIndex("totalAveria"));
                    ordenRecorrido.regInventarioLiquidacion = cursor.getInt(cursor.getColumnIndex("totalInventario"));
                    ordenRecorrido.regInventarioCanasta = cursor.getInt(cursor.getColumnIndex("totalinventarioCanastas"));
                    ordenRecorrido.regDescarga = cursor.getInt(cursor.getColumnIndex("totalDescargas"));
                    ;
                }

                if (cursor != null)
                    cursor.close();

            } else {
                Log.e(TAG, "CargarEstadisticas -> No Existe la Base de Datos");
            }
        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarEstadisticas -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return ordenRecorrido;


    }

    private static int obtenerProdDevolucionXCliente(String codigoVendedor, String codProducto) {

        int cantidad = 0;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "select sum(cantidad) cantidad from ( "
                    + "SELECT d.codigoRef AS codigo, "
                    + "SUM(d.cantidad) AS cantidad "
                    + "FROM Detalle d "
                    + "INNER JOIN Encabezado e ON e.numeroDoc = d.numDoc AND ifnull(e.anulado,0) = 0 AND e.tipotrans = 2 "
                    + "and d.codigoref='" + codProducto + "' "
                    + "GROUP BY d.codigoRef) tmp "
                    + "group by codigo ";
            Cursor cursor = db.rawQuery(query, null);

            System.out.println("Conulta cargar productos liquidacion PRODUCTO DANADO ---> " + query);

            if (cursor.moveToFirst())
                cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return cantidad;
    }

    private static int obtenerProdIngresadosXCliente(String codigoVendedor, String codProducto) {

        int cantidad = 0;

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            String query = "select sum(cantidad) cantidad from ("
                    + "SELECT d.codigoRef AS codigo, "
                    + "SUM(d.cantidad) AS cantidad "
                    + "FROM Detalle d "
                    + "INNER JOIN Encabezado e ON e.numeroDoc = d.numDoc AND ifnull(e.anulado,0) = 0 AND e.observaciones<>'FALTANTE PRODUCTO DANADO'  AND e.tipotrans = 0 "
                    + "and d.codigoref='" + codProducto + "' "
                    + "GROUP BY d.codigoRef "
                    + "union all  "
                    + "select ds.codigoref,sum(cantidad) cantidad from encabezadosugerido es "
                    + "inner join detallesugerido ds "
                    + "on es.numerodoc = ds.numdoc "
                    + "where es.tipotrans= " + Const.TIPO_TRANS_AVERIA_TRANSPORTE + " "
                    + "and ds.codigoref='" + codProducto + "' "
                    + "group by ds.codigoref) tmp "
                    + "group by codigo ";

            System.out.println("Consulta cargar productos liquidacion INVENTARIO ---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst())
                cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return cantidad;
    }


    //Metodo de insercion de registros para comparar la liquidacion

    public static boolean RegistrarLiquidacionComparacion(Hashtable<String, Detalle> detalleTmp, int tipoTransaccion) {

        ContentValues values;
        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;
        int cantidadTransaccion = 0;
        int cantInventario = 0;

        try {

            dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            /************************************
             * Se Ingresa el Encabezado del Producto
             ************************************/

            int tipoTrans = 0;
            String observacion = "";
            Usuario user = ObtenerUsuario();
            String nroDoc = ObtenterNumeroDocLiq(user.codigoVendedor);

            if (tipoTransaccion > 0) {

                switch (tipoTransaccion) {
                    case Const.IS_PRODUCTO_DANADO:
                        tipoTrans = Const.TIPO_TRANS_PRODUCTO_DANADO;
                        observacion = "DANADO";
                        break;

                    case Const.IS_INVENTARIO_LIQUIDACION:
                        tipoTrans = Const.TIPO_TRANS_INVENTARIO_LIQUIDACION;
                        observacion = "PRODUCTO INVENTARIO";
                        break;

                    default:
                        break;
                }

            }

            /************************************
             * Se Ingresa el registro en tabla temporal para validar diferencias
             * antes de insertar en de encabezado y detalle
             ************************************/
            values = new ContentValues();

            Enumeration<Detalle> e = detalleTmp.elements();
            Detalle detalle;

            int contItem = 1;

            while (e.hasMoreElements()) {

                detalle = e.nextElement();
                values = new ContentValues();

                values.put("numerodoc", nroDoc);
                values.put("vendedor", Main.usuario.codigoVendedor);
                values.put("tipo", observacion);
                values.put("producto", detalle.codProducto);
                values.put("cantidaddigitada", detalle.cantidad);
                values.put("fecha", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
                cantInventario = DataBaseBOJ.ObtenerCantidadInventarioProducto(detalle.codProducto);

                if (tipoTrans == Const.TIPO_TRANS_PRODUCTO_DANADO) {
                    System.out.println("Ingreso a ProductoDanado");
                    cantidadTransaccion = obtenerProdDevolucionXCliente(Main.usuario.codigoVendedor, detalle.codProducto);
                    cantInventario = cantidadTransaccion;
                    values.put("diferencia", cantidadTransaccion - detalle.cantidad);
                }

                if (tipoTrans == Const.TIPO_TRANS_INVENTARIO_LIQUIDACION) {
                    cantidadTransaccion = obtenerProdIngresadosXCliente(Main.usuario.codigoVendedor, detalle.codProducto);
                    values.put("diferencia", (cantInventario - cantidadTransaccion) - detalle.cantidad);
                }

                System.out.println("Retorno de la cantidad ---> " + cantidadTransaccion);
                values.put("cantidadinventario", cantInventario);
                values.put("cantidadtransaccion", cantidadTransaccion);

                contItem++;
                db.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
                dbPedido.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
            }

            validarRegistroInternoProductos(db, dbPedido, observacion, nroDoc);

            return true;
        } catch (Exception e) {
            mensaje = e.getMessage();
            return false;
        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }

    private static int ObtenerCantidadInventarioProducto(String codProducto) {

        SQLiteDatabase db = null;
        int cantidad = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT saldo as cantidad from productos where codigo = '" + codProducto + "'  ";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return cantidad;
    }


    private static void validarRegistroInternoProductos(SQLiteDatabase db, SQLiteDatabase dbPedido, String observacion, String nroDoc) {

        ContentValues values;

        if (observacion.equals("DANADO")) {

            String query = "SELECT e.usuario as vendedor, '" + observacion + "' tipo,d.codigoref producto,0 cantidaddigitada, SUM(d.cantidad) cantidadtransaccion, "
                    + "SUM(d.cantidad) cantidadinventario, sum(d.cantidad) diferencia "
                    + "FROM encabezado e "
                    + "INNER JOIN detalle d on e.numerodoc = d.numdoc  "
                    + "WHERE e.tipotrans = 2 and ifnull(e.anulado,0) = 0 "
                    + "AND d.codigoref NOT IN (SELECT producto FROM liquidaciondiferencias WHERE tipo = '" + observacion + "') "
                    + "GROUP BY e.usuario,d.codigoref ";


            System.out.println("Query validarRegistroDevolucionProductos--> " + query);


            db.execSQL("DELETE FROM LIQUIDACIONDIFERENCIAS WHERE cantidaddigitada = 0 AND tipo = '" + observacion + "' ");
            dbPedido.execSQL("DELETE FROM LIQUIDACIONDIFERENCIAS WHERE cantidaddigitada = 0 AND tipo = '" + observacion + "' ");

            Cursor cursor = db.rawQuery(query, null);
            values = new ContentValues();

            if (cursor.moveToFirst()) {

                do {
                    values = new ContentValues();

                    values.put("numerodoc", nroDoc);
                    values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                    values.put("tipo", cursor.getString(cursor.getColumnIndex("tipo")));
                    values.put("producto", cursor.getString(cursor.getColumnIndex("producto")));
                    values.put("cantidaddigitada", cursor.getString(cursor.getColumnIndex("cantidaddigitada")));
                    values.put("cantidadtransaccion", cursor.getString(cursor.getColumnIndex("cantidadtransaccion")));
                    values.put("cantidadinventario", cursor.getString(cursor.getColumnIndex("cantidadinventario")));
                    values.put("diferencia", cursor.getString(cursor.getColumnIndex("diferencia")));
                    values.put("fecha", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
                    db.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
                    dbPedido.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
                } while (cursor.moveToNext());


            }

        } else if (observacion.equals("PRODUCTO INVENTARIO")) {


            String query = "SELECT (select codigo from vendedor) vendedor, '" + observacion + "' tipo,p.codigo producto,0 cantidaddigitada, "
                    + "COALESCE(v.cantidad, 0) cantidadtransaccion, p.saldo - COALESCE(v.cantidad, 0) cantidadinventario, "
                    + "(p.saldo - COALESCE(v.cantidad, 0)) diferencia "
                    + "FROM Productos p "
                    + "LEFT JOIN ("
                    + "select codigo,sum(cantidad) cantidad from ( "
                    + "SELECT d.codigoRef AS codigo, "
                    + "SUM(d.cantidad) AS cantidad "
                    + "FROM Detalle d "
                    + "INNER JOIN Encabezado e ON e.numeroDoc = d.numDoc AND ifnull(e.anulado,0) = 0 AND e.tipotrans = 0 AND e.observaciones<>'FALTANTE PRODUCTO DANADO'  "
                    + "GROUP BY d.codigoRef "
                    + "union all "
                    + "select ds.codigoref,sum(cantidad) cantidad from encabezadosugerido es inner join detallesugerido ds "
                    + "on es.numerodoc = ds.numdoc where es.tipotrans= 8 "
                    + "group by ds.codigoref) tmp "
                    + "group by codigo "
                    + ") v ON v.codigo = p.codigo "
                    + "where (p.saldo - COALESCE(v.cantidad, 0))>0 and p.tipocliente=0 "
                    + "and p.codigo not in (select producto from liquidaciondiferencias where tipo = 'PRODUCTO INVENTARIO') ";


            System.out.println("Query validarRegistroDevolucionProductos--> " + query);


            db.execSQL("DELETE FROM LIQUIDACIONDIFERENCIAS WHERE cantidaddigitada = 0 AND tipo = '" + observacion + "' ");
            dbPedido.execSQL("DELETE FROM LIQUIDACIONDIFERENCIAS WHERE cantidaddigitada = 0 AND tipo = '" + observacion + "' ");

            Cursor cursor = db.rawQuery(query, null);
            values = new ContentValues();

            if (cursor.moveToFirst()) {

                do {
                    values = new ContentValues();

                    values.put("numerodoc", nroDoc);
                    values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                    values.put("tipo", cursor.getString(cursor.getColumnIndex("tipo")));
                    values.put("producto", cursor.getString(cursor.getColumnIndex("producto")));
                    values.put("cantidaddigitada", cursor.getString(cursor.getColumnIndex("cantidaddigitada")));
                    values.put("cantidadtransaccion", cursor.getString(cursor.getColumnIndex("cantidadtransaccion")));
                    values.put("cantidadinventario", cursor.getString(cursor.getColumnIndex("cantidadinventario")));
                    values.put("diferencia", cursor.getString(cursor.getColumnIndex("diferencia")));
                    values.put("fecha", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
                    db.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
                    dbPedido.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
                } while (cursor.moveToNext());

            }

        }


    }


    public static Vector<Producto> listaProductosLiquidados(String tipo, String and) {

        Vector<Producto> listaProducto = new Vector<Producto>();
        Usuario user = DataBaseBOJ.CargarUsuario();

        mensaje = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            String query = "";

            if (dbFile.exists()) {

                if (tipo.equals("PRODUCTO INVENTARIO")) {

                    query = "SELECT tipo, producto as codProducto, pr.nombre as descripcion, cantidaddigitada,COALESCE(v.cantidad, 0) cantidadtransaccion,pr.saldo - COALESCE(v.cantidad, 0) cantidadinventario, " +
                            " diferencia " +
                            " FROM LIQUIDACIONDIFERENCIAS liq INNER JOIN PRODUCTOS pr ON liq.producto = pr.codigo " +
                            " LEFT JOIN ( select codigo,sum(cantidad) cantidad from " +
                            " ( SELECT d.codigoRef AS codigo, SUM(d.cantidad) AS cantidad FROM Detalle d INNER JOIN Encabezado e ON e.numeroDoc = d.numDoc " +
                            " AND ifnull(e.anulado,0) = 0 AND e.tipotrans = 0 and e.observaciones<>'FALTANTE PRODUCTO DANADO' GROUP BY d.codigoRef " +
                            " union all  select ds.codigoref,sum(cantidad) cantidad from encabezadosugerido es inner join detallesugerido ds  " +
                            " on es.numerodoc = ds.numdoc where es.tipotrans=8  group by ds.codigoref) tmp group by codigo order by codigo ) " +
                            " v ON v.codigo = pr.codigo " +
                            " WHERE Vendedor = '" + user.codigoVendedor + "' AND tipo = '" + tipo + "'  AND diferencia <> 0 ";


                } else {

                    query = "SELECT tipo, producto as codProducto, pr.nombre as descripcion, cantidaddigitada, cantidadtransaccion, "
                            + "cantidadinventario, diferencia FROM LIQUIDACIONDIFERENCIAS liq "
                            + "INNER JOIN PRODUCTOS pr ON liq.producto = pr.codigo "
                            + "WHERE Vendedor = '" + user.codigoVendedor + "' "
                            + "AND tipo = '" + tipo + "' " + and + " ";

                }


                System.out.println("Query LIQUIDACIONDIFERENCIAS--> " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {
                        Producto producto = new Producto();
                        producto.tipo = cursor.getString(cursor.getColumnIndex("tipo"));
                        producto.codigo = cursor.getString(cursor.getColumnIndex("codProducto"));
                        producto.descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));
                        producto.cantidadDigitada = cursor.getInt(cursor.getColumnIndex("cantidaddigitada"));
                        producto.cantidadTransaccion = cursor.getInt(cursor.getColumnIndex("cantidadtransaccion"));
                        producto.cantidadInventario = cursor.getInt(cursor.getColumnIndex("cantidadinventario"));
                        producto.diferencia = cursor.getInt(cursor.getColumnIndex("diferencia"));

                        listaProducto.addElement(producto);
                    } while (cursor.moveToNext());

                }
            }
        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            closeDataBase(db);
        }
        return listaProducto;
    }


    public static boolean eliminarRegistrosLiq(String tipo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;
        boolean exito = false;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            Usuario user = DataBaseBOJ.CargarUsuario();

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM LIQUIDACIONDIFERENCIAS WHERE vendedor = '" + user.codigoVendedor + "' AND tipo = '" + tipo + "' ");
                exito = true;

            } else {

                Log.e(TAG, "BorrarLIQUIDACIONDIFERENCIAS: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarLIQUIDACIONDIFERENCIAS: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }

        return exito;

    }


    public static boolean guardarInformeLiquidado(String nroDocInforme, ConsecutivoFactura consecutivo, String tipo) {

        String fecha = Util.FechaActual("yyyy-MM-dd HH:mm:ss");


        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;
        String sql = "";
        ContentValues values;
        boolean existe = false;
        String observacion = "";

        System.out.println("Entra a guardar el informe *****");

        try {

            //Seleccionamos el tipo de observacion que se inserta de acuerdo al tipo de liquidacion que se este realizando
            if (tipo.equals("DANADO"))
                observacion = "FALTANTE PRODUCTO DANADO";
            else
                observacion = "REGISTRO PRODUCTO INVENTARIO";

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


            if (tipo.equals("DANADO")) {
                //Logica de insercion para el mod. productos danados "faltantes"
                String query = "SELECT '" + nroDocInforme + "' numDoc, l.producto, p.precio, p.iva, 0 descuento, l.diferencia cantidad, "
                        + "l.vendedor, '0' motivo, datetime('now','localtime') fechareal, 0 sincronizado,0 cantidadcambio, v.bodega  bodega, "
                        + "("
                        + "SELECT count( 1 ) from LIQUIDACIONDIFERENCIAS l1 "
                        + "INNER JOIN productos p1 on l1.producto = p1.codigo "
                        + "INNER JOIN vendedor v1 on v1.codigo = l1.vendedor    "
                        + "WHERE diferencia > 0 AND tipo = 'DANADO' AND p.codigo >= p1.codigo"
                        + ")  as item, "
                        + " '' bodega2, 0 tipocliente,0 inventario, "
                        + "datetime('now','localtime') fecha, 1  sincronizadoandroid, 0 as tipoventa  "
                        + "FROM LIQUIDACIONDIFERENCIAS l "
                        + "INNER JOIN productos p on l.producto = p.codigo "
                        + "INNER JOIN vendedor v on v.codigo = l.vendedor "
                        + "WHERE diferencia > 0 AND tipo = 'DANADO'  ";

                Cursor cursor = db.rawQuery(query, null);
                values = new ContentValues();

                //Insercion del detalle de productos danados
                if (cursor.moveToFirst()) {

                    do {
                        values = new ContentValues();

                        values.put("numDoc", cursor.getString(cursor.getColumnIndex("numDoc")));
                        values.put("codigoRef", cursor.getString(cursor.getColumnIndex("producto")));
                        values.put("precio", cursor.getString(cursor.getColumnIndex("precio")));
                        values.put("tarifaIva", cursor.getString(cursor.getColumnIndex("iva")));
                        values.put("descuentoRenglon", cursor.getString(cursor.getColumnIndex("descuento")));
                        values.put("cantidad", cursor.getString(cursor.getColumnIndex("cantidad")));
                        values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                        values.put("motivo", cursor.getString(cursor.getColumnIndex("motivo")));
                        values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                        values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                        values.put("cantidadcambio", cursor.getString(cursor.getColumnIndex("cantidadcambio")));
                        values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                        values.put("tipoventa", cursor.getString(cursor.getColumnIndex("tipoventa")));
                        db.insertOrThrow("detalle", null, values);
                        dbTemp.insertOrThrow("detalle", null, values);
                    } while (cursor.moveToNext());


                }

                //Se arma el encabezado de Productos danados
                query = "SELECT (SELECT codigo FROM clientes WHERE tipocliente = 1) Codigo, '" + nroDocInforme + "' numdoc , datetime('now','localtime') fechareal, "
                        + "0 AS TIPOTRANS,datetime('now','localtime') FECHATRANS, SUM(p.PRECIO*l.diferencia) montofact, 0 desc1, 0 iva, l.vendedor usuario, "
                        + "datetime('now','localtime') fechainicial, datetime('now','localtime') fechafinal,'" + observacion + "' observaciones, max(v.bodega) bodega, "
                        + "0 sincronizado, 0 entregado, '" + consecutivo.StrConsecutivo + "' factura, 0 sincronizadomapas, current_date fechaentrega, "
                        + "0 syncmobile, 0 sincronizadoandroid, 0 anulado,(SELECT TipoCredito FROM clientes WHERE tipocliente = 1) tipoCredito ,  '' fp2, '" + consecutivo.codigoConsecutivo + "' idconsecutivo, "
                        + "'" + consecutivo.prefijo + "' prefijo_consecutivo,'" + consecutivo.nroConsecutivo + "' numero_consecutivo "
                        + "FROM LIQUIDACIONDIFERENCIAS l "
                        + "INNER JOIN productos p on l.producto = p.codigo "
                        + "INNER JOIN vendedor v on v.codigo = l.vendedor "
                        + "WHERE diferencia > 0 "
                        + "AND tipo = 'DANADO' "
                        + "GROUP BY l.vendedor ";

                System.out.println("Query producto danado faltante ******--->" + query);

                cursor = db.rawQuery(query, null);
                values = new ContentValues();

                if (cursor.moveToFirst()) {

                    values.put("codigo", cursor.getString(cursor.getColumnIndex("Codigo")));
                    values.put("numeroDoc", cursor.getString(cursor.getColumnIndex("numdoc")));
                    values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                    values.put("tipoTrans", cursor.getString(cursor.getColumnIndex("TIPOTRANS")));
                    values.put("fechaTrans", cursor.getString(cursor.getColumnIndex("FECHATRANS")));
                    values.put("montoFact", cursor.getString(cursor.getColumnIndex("montofact")));
                    values.put("desc1", cursor.getString(cursor.getColumnIndex("desc1")));
                    values.put("iva", cursor.getString(cursor.getColumnIndex("iva")));
                    values.put("usuario", cursor.getString(cursor.getColumnIndex("usuario")));
                    values.put("fechaInicial", cursor.getString(cursor.getColumnIndex("fechainicial")));
                    values.put("fechaFinal", cursor.getString(cursor.getColumnIndex("fechafinal")));
                    values.put("observaciones", cursor.getString(cursor.getColumnIndex("observaciones")));
                    values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                    values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                    values.put("entregado", cursor.getString(cursor.getColumnIndex("entregado")));
                    values.put("factura", cursor.getString(cursor.getColumnIndex("factura")));
                    values.put("sincronizadomapas", cursor.getString(cursor.getColumnIndex("sincronizadomapas")));
                    values.put("fechaEntrega", cursor.getString(cursor.getColumnIndex("fechaentrega")));
                    values.put("syncmobile", cursor.getString(cursor.getColumnIndex("syncmobile")));
                    values.put("sincronizadoandroid", cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                    values.put("anulado", cursor.getString(cursor.getColumnIndex("anulado")));
                    values.put("FP", cursor.getString(cursor.getColumnIndex("tipoCredito")));
                    values.put("fp2", cursor.getString(cursor.getColumnIndex("fp2")));
                    values.put("idconsecutivo", cursor.getString(cursor.getColumnIndex("idconsecutivo")));
                    values.put("prefijo_consecutivo", cursor.getString(cursor.getColumnIndex("prefijo_consecutivo")));
                    values.put("numero_consecutivo", cursor.getString(cursor.getColumnIndex("numero_consecutivo")));

                    db.insertOrThrow("encabezado", null, values);
                    dbTemp.insertOrThrow("encabezado", null, values);

                    existe = true;

                }

            } else if (tipo.equals("PRODUCTO INVENTARIO")) {

                //Si proviene del mod. inventario
                //
                //Se arma el registro para insertar el detalle de Inventario FALTANTE


                String query = "SELECT '" + nroDocInforme + "' numDoc, l.producto, p.precio, p.iva,0 descuento, l.diferencia cantidad, "
                        + "l.vendedor, '0' motivo, datetime('now','localtime') fechareal, 0 sincronizado,0 cantidadcambio, v.bodega  bodega, "
                        + "("
                        + "SELECT count( 1 )   FROM liquidaciondiferencias l1   "
                        + "INNER JOIN productos p1 on l1.producto = p1.codigo "
                        + "INNER JOIN vendedor v1 on v1.codigo = l1.vendedor    "
                        + "WHERE diferencia > 0 and tipo = 'PRODUCTO INVENTARIO' and p.codigo >= p1.codigo"
                        + ")  as item, "
                        + "'' bodega2, 0 tipocliente,0 inventario, "
                        + "datetime('now','localtime') fecha, 1  sincronizadoandroid, 0 as tipoventa  "
                        + "FROM liquidaciondiferencias l "
                        + "INNER JOIN productos p on l.producto = p.codigo "
                        + "INNER JOIN vendedor v on v.codigo = l.vendedor "
                        + "WHERE diferencia > 0 and tipo = 'PRODUCTO INVENTARIO' ";

                Cursor cursor = db.rawQuery(query, null);
                values = new ContentValues();

                if (cursor.moveToFirst()) {

                    do {
                        values = new ContentValues();

                        values.put("numDoc", cursor.getString(cursor.getColumnIndex("numDoc")));
                        values.put("codigoRef", cursor.getString(cursor.getColumnIndex("producto")));
                        values.put("precio", cursor.getString(cursor.getColumnIndex("precio")));
                        values.put("tarifaIva", cursor.getString(cursor.getColumnIndex("iva")));
                        values.put("descuentoRenglon", cursor.getString(cursor.getColumnIndex("descuento")));
                        values.put("cantidad", cursor.getString(cursor.getColumnIndex("cantidad")));
                        values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                        values.put("motivo", cursor.getString(cursor.getColumnIndex("motivo")));
                        values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                        values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                        values.put("cantidadcambio", cursor.getString(cursor.getColumnIndex("cantidadcambio")));
                        values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                        values.put("item", cursor.getString(cursor.getColumnIndex("item")));
                        values.put("bodega2", cursor.getString(cursor.getColumnIndex("bodega2")));
                        values.put("tipocliente", cursor.getString(cursor.getColumnIndex("tipocliente")));
                        values.put("inventario", cursor.getString(cursor.getColumnIndex("inventario")));
                        values.put("fecha", cursor.getString(cursor.getColumnIndex("fecha")));
                        values.put("sincronizadoandroid", cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                        values.put("tipoventa", cursor.getString(cursor.getColumnIndex("tipoventa")));

                        db.insertOrThrow("detalle", null, values);
                        dbTemp.insertOrThrow("detalle", null, values);
                    } while (cursor.moveToNext());

                }


                query = "SELECT (SELECT CODIGO FROM clientes where tipocliente = 1) Codigo, '" + nroDocInforme + "' numdoc,datetime('now','localtime') fechareal, "
                        + "0 AS TIPOTRANS,datetime('now','localtime') FECHATRANS, SUM(p.PRECIO*l.diferencia) montofact, 0 desc1, 0 iva, l.vendedor usuario, "
                        + "datetime('now','localtime') fechainicial, datetime('now','localtime') fechafinal,'FALTANTE INVENTARIO PRODUCTO' observaciones, max(v.bodega) bodega, 0 sincronizado, 0 entregado, "
                        + "'" + consecutivo.StrConsecutivo + "' factura, 0 sincronizadomapas, current_date fechaentrega,0 syncmobile, 0 sincronizadoandroid, "
                        + "0 anulado, (SELECT TipoCredito FROM clientes WHERE TipoCliente = 1) tipoCredito ,''  fp2, '" + consecutivo.codigoConsecutivo + "' idconsecutivo, '" + consecutivo.prefijo + "' prefijo_consecutivo,'" + consecutivo.nroConsecutivo + "' numero_consecutivo "
                        + "FROM liquidaciondiferencias l "
                        + "INNER JOIN productos p on l.producto = p.codigo "
                        + "INNER JOIN vendedor v on v.codigo = l.vendedor "
                        + "WHERE diferencia > 0 AND tipo = 'PRODUCTO INVENTARIO' "
                        + "GROUP by l.vendedor;";

                System.out.println("Query Inventario FALTANTE ****--->" + query);

                cursor = db.rawQuery(query, null);
                values = new ContentValues();

                if (cursor.moveToFirst()) {


                    values.put("codigo", cursor.getString(cursor.getColumnIndex("Codigo")));
                    values.put("numeroDoc", cursor.getString(cursor.getColumnIndex("numdoc")));
                    values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                    values.put("tipoTrans", cursor.getString(cursor.getColumnIndex("TIPOTRANS")));
                    values.put("fechaTrans", cursor.getString(cursor.getColumnIndex("FECHATRANS")));
                    values.put("montoFact", cursor.getString(cursor.getColumnIndex("montofact")));
                    values.put("desc1", cursor.getString(cursor.getColumnIndex("desc1")));
                    values.put("iva", cursor.getString(cursor.getColumnIndex("iva")));
                    values.put("usuario", cursor.getString(cursor.getColumnIndex("usuario")));
                    values.put("fechaInicial", cursor.getString(cursor.getColumnIndex("fechainicial")));
                    values.put("fechaFinal", cursor.getString(cursor.getColumnIndex("fechafinal")));
                    values.put("observaciones", cursor.getString(cursor.getColumnIndex("observaciones")));
                    values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                    values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                    values.put("entregado", cursor.getString(cursor.getColumnIndex("entregado")));
                    values.put("factura", cursor.getString(cursor.getColumnIndex("factura")));
                    values.put("sincronizadomapas", cursor.getString(cursor.getColumnIndex("sincronizadomapas")));
                    values.put("fechaEntrega", cursor.getString(cursor.getColumnIndex("fechaentrega")));
                    values.put("syncmobile", cursor.getString(cursor.getColumnIndex("syncmobile")));
                    values.put("sincronizadoandroid", cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                    values.put("anulado", cursor.getString(cursor.getColumnIndex("anulado")));
                    values.put("FP", cursor.getString(cursor.getColumnIndex("tipoCredito")));
                    values.put("fp2", cursor.getString(cursor.getColumnIndex("fp2")));
                    values.put("idconsecutivo", cursor.getString(cursor.getColumnIndex("idconsecutivo")));
                    values.put("prefijo_consecutivo", cursor.getString(cursor.getColumnIndex("prefijo_consecutivo")));
                    values.put("numero_consecutivo", cursor.getString(cursor.getColumnIndex("numero_consecutivo")));

//		    long inserto =	db.insertOrThrow("encabezado", null, values);
//		    long insertoTemp =	dbTemp.insertOrThrow("encabezado", null, values);

                    db.insertOrThrow("encabezado", null, values);
                    dbTemp.insertOrThrow("encabezado", null, values);

                }

//		    if(inserto > 0 && insertoTemp > 0){

                //Se arma el registro para insertar el detalle de Inventario SOBRANTE

                Usuario user = DataBaseBOJ.CargarUsuario();
                String nroDocInformeSobrante = DataBaseBOJ.ObtenterNumeroDocLiq(user.codigoVendedor);
                consecutivo = new ConsecutivoFactura();
                consecutivo.codigoConsecutivo = "0";
                consecutivo.vendedor = user.codigoVendedor;
                consecutivo.prefijo = "";
                consecutivo.nroConsecutivo = 0;
                consecutivo.total = 0;


                query = "select '" + nroDocInformeSobrante + "' numDoc, l.producto, p.precio, p.iva,0 descuento, (l.diferencia * -1) cantidad, "
                        + "l.vendedor, 'D14' motivo, datetime('now','localtime') fechareal, 0 sincronizado,0 cantidadcambio, v.bodega  bodega, "
                        + "( "
                        + "select count( 1 )   from liquidaciondiferencias l1   inner join productos p1 on l1.producto = p1.codigo "
                        + "inner join vendedor v1 on v1.codigo = l1.vendedor    where diferencia > 0 and tipo = 'PRODUCTO INVENTARIO' and p.codigo >= p1.codigo"
                        + ")  as item, "
                        + "'' bodega2, 0 tipocliente,0 inventario, "
                        + "datetime('now','localtime') fecha, 1  sincronizadoandroid, 0 as tipoventa  "
                        + "from liquidaciondiferencias l "
                        + "inner join productos p on l.producto = p.codigo "
                        + "inner join vendedor v on v.codigo = l.vendedor "
                        + "where diferencia < 0 and tipo = 'PRODUCTO INVENTARIO'";

                cursor = db.rawQuery(query, null);
                values = new ContentValues();

                if (cursor.moveToFirst()) {

                    do {
                        values = new ContentValues();

                        values.put("numDoc", cursor.getString(cursor.getColumnIndex("numDoc")));
                        values.put("codigoRef", cursor.getString(cursor.getColumnIndex("producto")));
                        values.put("precio", cursor.getString(cursor.getColumnIndex("precio")));
                        values.put("tarifaIva", cursor.getString(cursor.getColumnIndex("iva")));
                        values.put("descuentoRenglon", cursor.getString(cursor.getColumnIndex("descuento")));
                        values.put("cantidad", cursor.getString(cursor.getColumnIndex("cantidad")));
                        values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                        values.put("motivo", cursor.getString(cursor.getColumnIndex("motivo")));
                        values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                        values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                        values.put("cantidadcambio", cursor.getString(cursor.getColumnIndex("cantidadcambio")));
                        values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                        values.put("item", cursor.getString(cursor.getColumnIndex("item")));
                        values.put("bodega2", cursor.getString(cursor.getColumnIndex("bodega2")));
                        values.put("tipocliente", cursor.getString(cursor.getColumnIndex("tipocliente")));
                        values.put("inventario", cursor.getString(cursor.getColumnIndex("inventario")));
                        values.put("fecha", cursor.getString(cursor.getColumnIndex("fecha")));
                        values.put("sincronizadoandroid", cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                        values.put("tipoventa", cursor.getString(cursor.getColumnIndex("tipoventa")));

                        db.insertOrThrow("detalle", null, values);
                        dbTemp.insertOrThrow("detalle", null, values);

                    } while (cursor.moveToNext());

                }

                query = "select (select CODIGO from clientes where tipocliente = 2) Codigo, '"
                        + nroDocInformeSobrante + "' numdoc,datetime('now','localtime') fechareal, "
                        + "2 AS TIPOTRANS,datetime('now','localtime') FECHATRANS, SUM(p.PRECIO* (l.diferencia * -1)) montofact, 0 desc1, 0 iva, l.vendedor usuario,  "
                        + "datetime('now','localtime') fechainicial, datetime('now','localtime') fechafinal,'SOBRANTE INVENTARIO PRODUCTO' observaciones, max(v.bodega) bodega, 0 sincronizado, "
                        + "0 entregado, '" + consecutivo.StrConsecutivo
                        + "' factura, 0 sincronizadomapas, current_date fechaentrega,0 syncmobile, "
                        + "0 sincronizadoandroid, 0 anulado, 0 tipoCredito  , '" + nroDocInforme + "'  fp2, '"
                        + consecutivo.codigoConsecutivo + "' idconsecutivo, '" + consecutivo.prefijo
                        + "' prefijo_consecutivo, " + "'" + consecutivo.nroConsecutivo + "' numero_consecutivo "
                        + "from liquidaciondiferencias l " + "inner join productos p on l.producto = p.codigo "
                        + "inner join vendedor v on v.codigo = l.vendedor "
                        + "where diferencia < 0 and tipo = 'PRODUCTO INVENTARIO' " + "group by l.vendedor";

                System.out.println("Query Inventario Sobrante --->  ***** " + query);

                cursor = db.rawQuery(query, null);
                values = new ContentValues();

                if (cursor.moveToFirst()) {

                    values.put("codigo", cursor.getString(cursor.getColumnIndex("Codigo")));
                    values.put("numeroDoc", cursor.getString(cursor.getColumnIndex("numdoc")));
                    values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                    values.put("tipoTrans", cursor.getString(cursor.getColumnIndex("TIPOTRANS")));
                    values.put("fechaTrans", cursor.getString(cursor.getColumnIndex("FECHATRANS")));
                    values.put("montoFact", cursor.getString(cursor.getColumnIndex("montofact")));
                    values.put("desc1", cursor.getString(cursor.getColumnIndex("desc1")));
                    values.put("iva", cursor.getString(cursor.getColumnIndex("iva")));
                    values.put("usuario", cursor.getString(cursor.getColumnIndex("usuario")));
                    values.put("fechaInicial", cursor.getString(cursor.getColumnIndex("fechainicial")));
                    values.put("fechaFinal", cursor.getString(cursor.getColumnIndex("fechafinal")));
                    values.put("observaciones", cursor.getString(cursor.getColumnIndex("observaciones")));
                    values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                    values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                    values.put("entregado", cursor.getString(cursor.getColumnIndex("entregado")));
                    values.put("factura", cursor.getString(cursor.getColumnIndex("factura")));
                    values.put("sincronizadomapas",
                            cursor.getString(cursor.getColumnIndex("sincronizadomapas")));
                    values.put("fechaEntrega", cursor.getString(cursor.getColumnIndex("fechaentrega")));
                    values.put("syncmobile", cursor.getString(cursor.getColumnIndex("syncmobile")));
                    values.put("sincronizadoandroid",
                            cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                    values.put("anulado", cursor.getString(cursor.getColumnIndex("anulado")));
                    values.put("FP", cursor.getString(cursor.getColumnIndex("tipoCredito")));
                    values.put("fp2", cursor.getString(cursor.getColumnIndex("fp2")));
                    values.put("idconsecutivo", cursor.getString(cursor.getColumnIndex("idconsecutivo")));
                    values.put("prefijo_consecutivo",
                            cursor.getString(cursor.getColumnIndex("prefijo_consecutivo")));
                    values.put("numero_consecutivo",
                            cursor.getString(cursor.getColumnIndex("numero_consecutivo")));

//			    inserto += db.insertOrThrow("encabezado", null, values);
//			    insertoTemp += dbTemp.insertOrThrow("encabezado", null, values);

                    db.insertOrThrow("encabezado", null, values);
                    dbTemp.insertOrThrow("encabezado", null, values);

//			    if (inserto > 1 && inserto > 1)
                    existe = true;

                }

//		    }

            }


        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;

    }

    public static boolean existeTipoClienteLiquidacion() {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            Usuario user = CargarUsuario();
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) total FROM CLIENTES WHERE tipocliente = 1 and bodega in (SELECT bodega from clientes where tipocliente = 2) "
                    + "AND bodega = '" + user.bodega + "' ";


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean isFinalizadoRegistro(int tipoTrans) {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total FROM encabezadosugerido WHERE TipoTrans = " + tipoTrans;

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;


    }


    public static void eliminarRegistroTemporal(String tipo) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;

        try {

            Usuario user = ObtenerUsuario();
            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                db.execSQL("DELETE FROM LIQUIDACIONDIFERENCIAS WHERE tipo = '" + tipo + "' AND vendedor = '" + user.codigoVendedor + "' ");

            } else {

                Log.e(TAG, "BorrarPedidosSinFinalizar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "BorrarPedidosSinFinalizar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }
    }

    public static String obtenerNroDocSobranteAmarre(String nroDoc, String observacion) {

        SQLiteDatabase db = null;
        String nroDocDev = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select numeroDoc from encabezado where fp2 = '" + nroDoc + "' AND "
                    + "observaciones = '" + observacion + "' "; //and tipoTrans = '2'
            System.out.println("Query obtenerNroDocSobranteAmarre " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDocDev = cursor.getString(cursor.getColumnIndex("numeroDoc"));
                System.out.println("NUMERODOC DEVOLUCION: " + nroDocDev);
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return nroDocDev;
    }

    public static String obtenerNroDocCargue() {

        SQLiteDatabase db = null;
        String nroDoc = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select NumeroDoc from encabezadosugerido WHERE TipoTrans = " + Const.TIPO_TRANS_CARGUE; //and tipoTrans = '2'
            System.out.println("Query obtenerNroDocCargue--->  " + query);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                nroDoc = cursor.getString(cursor.getColumnIndex("NumeroDoc"));
                System.out.println("NUMERODOC CARGUE: " + nroDoc);
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return nroDoc;
    }


    public static boolean existeSobranteNroDoc(String nroDocRegistro) {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS total FROM encabezado WHERE numeroDoc = '" + nroDocRegistro + "' ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }


    public static boolean hayRegPorImprimir(String numeroDoc) {
        SQLiteDatabase db = null;
        String sql = "";
        int total = 0;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT COUNT (*) as total FROM detalle d INNER JOIN productos p on p.codigo = d.[codigoRef] WHERE numDoc = '"
                    + numeroDoc + "' order by p.orden asc ";

            System.out.println("hayRegPorImprimir: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return total > 0;
    }

    public static boolean hayRegPorImprimirCompensacion(String numeroDoc) {
        SQLiteDatabase db = null;
        String sql = "";
        int total = 0;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "SELECT COUNT (*) as total FROM detalleSugerido d INNER JOIN productos p on p.codigo = d.[codigoRef] WHERE NumDoc = '"
                    + numeroDoc + "' order by p.orden asc ";

            System.out.println("hayRegPorImprimir: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return total > 0;
    }

    public static boolean guardarInformeLiquidadoCanastillas() {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;
        ContentValues values;
        boolean existe = false;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Usuario user = ObtenerUsuario();
            String nrodoc = ObtenterNumeroDocLiq(user.codigoVendedor);


            //L�gica de insercion para el mod. productos danados
            String query = "select (select codigo from vendedor)  vendedor,'CANASTAS' tipo, "
                    + "codigo,(vacias + invcanastas) cantidaddigitada,(invpedidos +invsinped ) cantidadtransaccion,(saldo + (invpedidos +invsinped )) cantidadinventario, "
                    + "(saldo + (invpedidos +invsinped )) - (vacias + invcanastas) diferencia "
                    + "from ("
                    + "select codigo, max(Nombre) nombre,MAX(Saldo) saldo, ifnull(max(invcli.diferencia),0) invpedidos, ifnull(MAX(invsinped.diferencia),0) invsinped, "
                    + "ifnull(MAX(vacias.diferencia),0) vacias,ifnull(MAX(invcanastas.diferencia),0) invcanastas "
                    + "from Productos p "
                    + "left join (select marca,sum(presupuesto2) entregadas,sum(presupuesto) devueltas,sum(presupuesto2) - sum(presupuesto) diferencia  from presupuestolinea where tipotrans = 1 group by marca) invcli "
                    + "on invcli.marca = p.Codigo "
                    + "left join (select marca,sum(presupuesto) diferencia  from presupuestolinea where tipotrans = 9  group by marca) invsinped "
                    + "on invsinped.marca = p.Codigo "
                    + "left join (select marca,sum(presupuesto) diferencia from presupuestolinea where tipotrans = 2 and presupuesto>0 "
                    + "group by marca) vacias on vacias.marca = p.Codigo "
                    + "left join (select marca,sum(presupuesto) diferencia from presupuestolinea where tipotrans = 4 and presupuesto>0 "
                    + "group by marca) invcanastas on invcanastas.marca = p.Codigo "
                    + "where Tipocliente=1 and p.saldo>0 group by codigo "
                    + ") tmp";

            Cursor cursor = db.rawQuery(query, null);
            values = new ContentValues();

            if (cursor.moveToFirst()) {

                db.execSQL("DELETE FROM LIQUIDACIONDIFERENCIAS WHERE tipo = 'CANASTAS' ");

                do {
                    values = new ContentValues();

                    values.put("numerodoc", nrodoc);
                    values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                    values.put("tipo", cursor.getString(cursor.getColumnIndex("tipo")));
                    values.put("producto", cursor.getString(cursor.getColumnIndex("codigo")));
                    values.put("cantidaddigitada", cursor.getString(cursor.getColumnIndex("cantidaddigitada")));
                    values.put("cantidadtransaccion", cursor.getString(cursor.getColumnIndex("cantidadtransaccion")));
                    values.put("cantidadInventario", cursor.getString(cursor.getColumnIndex("cantidadinventario")));
                    values.put("diferencia", cursor.getString(cursor.getColumnIndex("diferencia")));
                    values.put("fecha", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));

                    db.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
                    dbTemp.insertOrThrow("LIQUIDACIONDIFERENCIAS", null, values);
                } while (cursor.moveToNext());

                existe = true;
            }


        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;

    }


    public static boolean guardarEncabezadoCanastilla() {

        SQLiteDatabase db = null;
        SQLiteDatabase dbTemp = null;
        String sql = "";
        ContentValues values;
        boolean existe = false;
        String observacion = "";

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            File filePedido = new File(Util.DirApp(), "Temp.db");
            dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Usuario user = ObtenerUsuario();
            String nroDoc = ObtenterNumeroDocLiq(user.codigoVendedor);

            String query = "select '" + nroDoc + "' numDoc, l.producto, p.precio,0 iva,0 descuento, l.diferencia cantidad, "
                    + "l.vendedor, 'D14' motivo, datetime('now','localtime') fechareal, 0 sincronizado,0 cantidadcambio, v.bodega  bodega, "
                    + "( "
                    + "select count( 1 )   from liquidaciondiferencias l1   inner join productos p1 on l1.producto = p1.codigo "
                    + "inner join vendedor v1 on v1.codigo = l1.vendedor   where diferencia > 0 and tipo = 'CANASTAS' and p.codigo >= p1.codigo "
                    + ")  as item,  "
                    + "'' bodega2, 0 tipocliente,0 inventario, "
                    + "datetime('now','localtime') fecha, 1  sincronizadoandroid, 1 tipoventa " // ,
                    + "from liquidaciondiferencias l  "
                    + "inner join productos p on l.producto = p.codigo  "
                    + "inner join vendedor v on v.codigo = l.vendedor  "
                    + "where diferencia > 0 and tipo = 'CANASTAS' ";

            Cursor cursor = db.rawQuery(query, null);
            values = new ContentValues();

            if (cursor.moveToFirst()) {

                do {
                    values = new ContentValues();

                    values.put("numDoc", cursor.getString(cursor.getColumnIndex("numDoc")));
                    values.put("codigoRef", cursor.getString(cursor.getColumnIndex("producto")));
                    values.put("precio", cursor.getString(cursor.getColumnIndex("precio")));
                    values.put("tarifaIva", cursor.getString(cursor.getColumnIndex("iva")));
                    values.put("descuentoRenglon", cursor.getString(cursor.getColumnIndex("descuento")));
                    values.put("cantidad", cursor.getString(cursor.getColumnIndex("cantidad")));
                    values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                    values.put("motivo", cursor.getString(cursor.getColumnIndex("motivo")));
                    values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                    values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                    values.put("cantidadcambio", cursor.getString(cursor.getColumnIndex("cantidadcambio")));
                    values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                    values.put("item", cursor.getString(cursor.getColumnIndex("item")));
                    values.put("bodega2", cursor.getString(cursor.getColumnIndex("bodega2")));
                    values.put("tipocliente", cursor.getString(cursor.getColumnIndex("tipocliente")));
                    values.put("inventario", cursor.getString(cursor.getColumnIndex("inventario")));
                    values.put("fecha", cursor.getString(cursor.getColumnIndex("fecha")));
                    values.put("sincronizadoandroid", cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                    values.put("tipoventa", cursor.getString(cursor.getColumnIndex("tipoventa")));

                    db.insertOrThrow("detalle", null, values);
                    dbTemp.insertOrThrow("detalle", null, values);
                } while (cursor.moveToNext());

            }


            query = "select (select CODIGO from clientes where tipocliente = 1) Codigo, '" + nroDoc + "' numdoc,datetime('now','localtime') fechareal, "
                    + "1 AS TIPOTRANS,datetime('now','localtime') FECHATRANS, SUM(p.PRECIO*l.diferencia) montofact, 0 desc1, 0 iva, l.vendedor usuario, "
                    + "datetime('now','localtime') fechainicial, datetime('now','localtime') fechafinal,'FALTANTE CANASTAS' observaciones, max(v.bodega) bodega, 0 sincronizado, 0 entregado, "
                    + "'0' factura, 0 sincronizadomapas, current_date fechaentrega,0 syncmobile, 0 sincronizadoandroid, "
                    + "0 anulado, (select TipoCredito from clientes where tipocliente = 1) tipoCredito ,''  fp2, 0 idconsecutivo, '' prefijo_consecutivo,0 numero_consecutivo "
                    + "from liquidaciondiferencias l "
                    + "inner join productos p on l.producto = p.codigo "
                    + "inner join vendedor v on v.codigo = l.vendedor "
                    + "where diferencia > 0 and tipo = 'CANASTAS' "
                    + "group by l.vendedor";

            System.out.println("cargarEncabezado --->" + query);

            cursor = db.rawQuery(query, null);
            values = new ContentValues();

            if (cursor.moveToFirst()) {


                values.put("codigo", cursor.getString(cursor.getColumnIndex("Codigo")));
                values.put("numeroDoc", cursor.getString(cursor.getColumnIndex("numdoc")));
                values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                values.put("tipoTrans", cursor.getString(cursor.getColumnIndex("TIPOTRANS")));
                values.put("fechaTrans", cursor.getString(cursor.getColumnIndex("FECHATRANS")));
                values.put("montoFact", cursor.getString(cursor.getColumnIndex("montofact")));
                values.put("desc1", cursor.getString(cursor.getColumnIndex("desc1")));
                values.put("iva", cursor.getString(cursor.getColumnIndex("iva")));
                values.put("usuario", cursor.getString(cursor.getColumnIndex("usuario")));
                values.put("fechaInicial", cursor.getString(cursor.getColumnIndex("fechainicial")));
                values.put("fechaFinal", cursor.getString(cursor.getColumnIndex("fechafinal")));
                values.put("observaciones", cursor.getString(cursor.getColumnIndex("observaciones")));
                values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                values.put("entregado", cursor.getString(cursor.getColumnIndex("entregado")));
                values.put("factura", cursor.getString(cursor.getColumnIndex("factura")));
                values.put("sincronizadomapas", cursor.getString(cursor.getColumnIndex("sincronizadomapas")));
                values.put("fechaEntrega", cursor.getString(cursor.getColumnIndex("fechaentrega")));
                values.put("syncmobile", cursor.getString(cursor.getColumnIndex("syncmobile")));
                values.put("sincronizadoandroid", cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                values.put("anulado", cursor.getString(cursor.getColumnIndex("anulado")));
                values.put("FP", cursor.getString(cursor.getColumnIndex("tipoCredito")));
                values.put("fp2", cursor.getString(cursor.getColumnIndex("fp2")));
                values.put("idconsecutivo", cursor.getString(cursor.getColumnIndex("idconsecutivo")));
                values.put("prefijo_consecutivo", cursor.getString(cursor.getColumnIndex("prefijo_consecutivo")));
                values.put("numero_consecutivo", cursor.getString(cursor.getColumnIndex("numero_consecutivo")));

                long inserto = db.insertOrThrow("encabezado", null, values);
                long insertoTemp = dbTemp.insertOrThrow("encabezado", null, values);

                if (inserto > 0 && insertoTemp > 0) {

                    //Se arma el registro para insertar el detalle de Inventario SOBRANTE

                    String nroDocInformeSobrante = DataBaseBOJ.ObtenterNumeroDocLiq(user.codigoVendedor);

                    query = "select '" + nroDocInformeSobrante + "' numDoc, l.producto, p.precio,0 iva,0 descuento, CASE WHEN l.diferencia  < 0 THEN -1 * l.diferencia ELSE l.diferencia END AS cantidad, "  // l.diferencia cantidad
                            + "l.vendedor, 'D14' motivo, datetime('now','localtime') fechareal, 0 sincronizado,0 cantidadcambio, v.bodega  bodega, "
                            + "( "
                            + "select count( 1 )   from liquidaciondiferencias l1   inner join productos p1 on l1.producto = p1.codigo "
                            + "inner join vendedor v1 on v1.codigo = l1.vendedor    where diferencia > 0 and tipo = 'CANASTAS' and p.codigo >= p1.codigo "
                            + ") as item, '' bodega2, 0 tipocliente,0 inventario, datetime('now','localtime') fecha, 1  sincronizadoandroid, 0 tipoventa "//,
                            + "from liquidaciondiferencias l "
                            + "inner join productos p on l.producto = p.codigo "
                            + "inner join vendedor v on v.codigo = l.vendedor "
                            + "where diferencia < 0 and tipo = 'CANASTAS'";

                    cursor = db.rawQuery(query, null);
                    values = new ContentValues();

                    if (cursor.moveToFirst()) {

                        do {
                            values = new ContentValues();

                            values.put("numDoc", cursor.getString(cursor.getColumnIndex("numDoc")));
                            values.put("codigoRef", cursor.getString(cursor.getColumnIndex("producto")));
                            values.put("precio", cursor.getString(cursor.getColumnIndex("precio")));
                            values.put("tarifaIva", cursor.getString(cursor.getColumnIndex("iva")));
                            values.put("descuentoRenglon", cursor.getString(cursor.getColumnIndex("descuento")));
                            values.put("cantidad", cursor.getString(cursor.getColumnIndex("cantidad")));
                            values.put("vendedor", cursor.getString(cursor.getColumnIndex("vendedor")));
                            values.put("motivo", cursor.getString(cursor.getColumnIndex("motivo")));
                            values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                            values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                            values.put("cantidadcambio", cursor.getString(cursor.getColumnIndex("cantidadcambio")));
                            values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                            values.put("item", cursor.getString(cursor.getColumnIndex("item")));
                            values.put("bodega2", cursor.getString(cursor.getColumnIndex("bodega2")));
                            values.put("tipocliente", cursor.getString(cursor.getColumnIndex("tipocliente")));
                            values.put("inventario", cursor.getString(cursor.getColumnIndex("inventario")));
                            values.put("fecha", cursor.getString(cursor.getColumnIndex("fecha")));
                            values.put("sincronizadoandroid", cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                            values.put("tipoventa", cursor.getString(cursor.getColumnIndex("tipoventa")));

                            db.insertOrThrow("detalle", null, values);
                            dbTemp.insertOrThrow("detalle", null, values);

                        } while (cursor.moveToNext());

                    }

                    //
                    query = "select (select CODIGO from clientes where tipocliente = 2) Codigo, '" + nroDocInformeSobrante + "' numdoc,datetime('now','localtime') fechareal, "
                            + "1 AS TIPOTRANS,datetime('now','localtime') FECHATRANS, CASE WHEN l.diferencia < 0 THEN ROUND(SUM(p.PRECIO*(-1 * l.diferencia)), 2) ELSE ROUND(SUM(p.PRECIO*l.diferencia), 2) END AS montofact, 0 desc1, 0 iva, l.vendedor usuario, "
                            + "datetime('now','localtime') fechainicial, datetime('now','localtime') fechafinal,'SOBRANTE CANASTAS' observaciones, max(v.bodega) bodega, 0 sincronizado, 0 entregado, "
                            + "'0' factura, 0 sincronizadomapas, current_date fechaentrega,0 syncmobile, 0 sincronizadoandroid, "
                            + "0 anulado,(select TipoCredito from clientes where tipocliente = 1) tipoCredito , '" + nroDoc + "'  fp2, 0 idconsecutivo, '' prefijo_consecutivo,0 numero_consecutivo "
                            + "from liquidaciondiferencias l "
                            + "inner join productos p on l.producto = p.codigo "
                            + "inner join vendedor v on v.codigo = l.vendedor "
                            + "where diferencia < 0 and tipo = 'CANASTAS' "
                            + "group by l.vendedor";

                    System.out.println("cargarEncabezado mod. inventario --->" + query);

                    cursor = db.rawQuery(query, null);
                    values = new ContentValues();

                    if (cursor.moveToFirst()) {

                        values.put("codigo", cursor.getString(cursor.getColumnIndex("Codigo")));
                        values.put("numeroDoc", cursor.getString(cursor.getColumnIndex("numdoc")));
                        values.put("fechaReal", cursor.getString(cursor.getColumnIndex("fechareal")));
                        values.put("tipoTrans", cursor.getString(cursor.getColumnIndex("TIPOTRANS")));
                        values.put("fechaTrans", cursor.getString(cursor.getColumnIndex("FECHATRANS")));

                        //							float validarPrecio = cursor.getFloat(cursor.getColumnIndex("montofact"));
                        //							if(validarPrecio < 0) validarPrecio = validarPrecio * -1;

                        values.put("montoFact", cursor.getFloat(cursor.getColumnIndex("montofact")));
                        values.put("desc1", cursor.getString(cursor.getColumnIndex("desc1")));
                        values.put("iva", cursor.getString(cursor.getColumnIndex("iva")));
                        values.put("usuario", cursor.getString(cursor.getColumnIndex("usuario")));
                        values.put("fechaInicial", cursor.getString(cursor.getColumnIndex("fechainicial")));
                        values.put("fechaFinal", cursor.getString(cursor.getColumnIndex("fechafinal")));
                        values.put("observaciones", cursor.getString(cursor.getColumnIndex("observaciones")));
                        values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                        values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                        values.put("entregado", cursor.getString(cursor.getColumnIndex("entregado")));
                        values.put("factura", cursor.getString(cursor.getColumnIndex("factura")));
                        values.put("sincronizadomapas",
                                cursor.getString(cursor.getColumnIndex("sincronizadomapas")));
                        values.put("fechaEntrega", cursor.getString(cursor.getColumnIndex("fechaentrega")));
                        values.put("syncmobile", cursor.getString(cursor.getColumnIndex("syncmobile")));
                        values.put("sincronizadoandroid",
                                cursor.getString(cursor.getColumnIndex("sincronizadoandroid")));
                        values.put("anulado", cursor.getString(cursor.getColumnIndex("anulado")));
                        values.put("FP", cursor.getString(cursor.getColumnIndex("tipoCredito")));
                        values.put("fp2", cursor.getString(cursor.getColumnIndex("fp2")));
                        values.put("idconsecutivo", cursor.getString(cursor.getColumnIndex("idconsecutivo")));
                        values.put("prefijo_consecutivo",
                                cursor.getString(cursor.getColumnIndex("prefijo_consecutivo")));
                        values.put("numero_consecutivo",
                                cursor.getString(cursor.getColumnIndex("numero_consecutivo")));

                        inserto += db.insertOrThrow("encabezado", null, values);
                        insertoTemp += dbTemp.insertOrThrow("encabezado", null, values);

                        if (inserto > 1 && inserto > 1)
                            existe = true;

                    }

                }

            }


        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }

    public static float getTotalConbranza() {
        SQLiteDatabase db = null;
        String sql = "";
        float total = 0;


        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select t.id, max(t.tipo_pago) tipopago, ifnull(SUM(F.MONTO),0) MONTO  "
                    + "from tipos_pago t "
                    + "left join formapago F on t.id = f.forma_pago  "
                    + "where t.id = 1";

            System.out.println("getImpresionVentaContado: " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                total = cursor.getFloat(cursor.getColumnIndex("MONTO"));

            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return total;
    }


    /**
     * metodo para llenar la lista de faltantes de la liquidacion de productos actual.
     *
     * @param listaFaltantes
     */
    public static void llenarListaFaltantes(ArrayList<ProductosCompensacion> listaFaltantes) {

        SQLiteDatabase db = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");


            if (dbFile.exists()) {
                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);
                String queryFaltantes = ""
                        + "SELECT ld.producto AS producto,"
                        + "       ld.diferencia AS diferencia, "
                        + "       c.linea AS linea "
                        + "FROM LIQUIDACIONDIFERENCIAS ld "
                        + "     INNER JOIN compensacion c ON c.codigo = ld.producto "
                        + "WHERE ld.diferencia > 0 AND tipo = 'PRODUCTO INVENTARIO' "
                        + "GROUP BY ld.producto, "
                        + "         ld.diferencia, "
                        + "         c.linea; ";

                Cursor cursor = db.rawQuery(queryFaltantes, null);

                if (cursor.moveToFirst()) {

                    do {
                        ProductosCompensacion productosCompensacion = new ProductosCompensacion();
                        productosCompensacion.producto = cursor.getString(cursor.getColumnIndex("producto")).trim();
                        productosCompensacion.diferencia = cursor.getInt(cursor.getColumnIndex("diferencia"));
                        productosCompensacion.linea = cursor.getString(cursor.getColumnIndex("linea")).trim();
                        listaFaltantes.add(productosCompensacion);
                    } while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();
            }
        } catch (Exception e) {
            Log.e("Error llenando lista Faltantes compensacion --> ", e.getMessage());
        } finally {
            closeDataBase(db);
        }
    }


    /**
     * Algoritmo  para calcular las compesaciones de faltantes y sobrantes.
     *
     * @param listaFaltantes
     * @param listaDetallesCompensacion
     * @return
     */
    @SuppressLint("NewApi")
    public static boolean iniciarCalculoDeCompensacion(final ArrayList<ProductosCompensacion> listaFaltantes, ArrayList<ProductosCompensacion> listaDetallesCompensacion) {


        boolean ejecucionCompleta = false;
        SQLiteDatabase db = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");


            if (dbFile.exists()) {
                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                /*actualizar el producto faltante a su nuevo valor compensado*/
                String queryUpdateSobrantes = ""
                        + "UPDATE LIQUIDACIONDIFERENCIAS "
                        + "SET diferencia = diferencia - ? "
                        + "WHERE producto = ? AND "
                        + "      tipo = 'PRODUCTO INVENTARIO' AND "
                        + "      diferencia > 0;";
                SQLiteStatement stmtUpdateSobrantes = db.compileStatement(queryUpdateSobrantes);

                /*actualizar el producto sobrante a su nuevo valor compensado*/
                String queryUpdateFaltantes = ""
                        + "UPDATE LIQUIDACIONDIFERENCIAS "
                        + "SET diferencia = diferencia - ? "
                        + "WHERE producto = ? AND "
                        + "      tipo = 'PRODUCTO INVENTARIO' AND "
                        + "      diferencia < 0;";
                SQLiteStatement stmtUpdateFaltantes = db.compileStatement(queryUpdateFaltantes);

                /*referencias para llenar las consultas por cada iteracion del bucle*/
                String querySobrantes = "";
                Cursor cursor = null;

                /*iniciar la transaccion*/
                db.beginTransaction();

                for (ProductosCompensacion productosCompensacion : listaFaltantes) {

                    /*consultar los productos que pueden ser compensados por el faltante de la lista del producto de la iteracion actual*/
                    querySobrantes = ""
                            + "SELECT ld.producto AS producto,"
                            + "       ABS(ld.diferencia) AS sobrante,"
                            + "       ld.diferencia AS diferencia "
                            + "FROM LIQUIDACIONDIFERENCIAS ld "
                            + "     INNER JOIN compensacion c ON c.codigo = ld.producto "
                            + "WHERE ld.diferencia < 0 AND "
                            + "      c.linea = '" + productosCompensacion.linea + "' AND "
                            + "      ld.producto <> '" + productosCompensacion.producto + "' AND "
                            + "      tipo = 'PRODUCTO INVENTARIO' "
                            + "ORDER BY sobrante DESC;";

                    /*llenar cursor con la iteracion actual*/
                    cursor = db.rawQuery(querySobrantes, null);

                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            ProductosCompensacion productoCompensado = new ProductosCompensacion();
                            int sobrante = cursor.getInt(cursor.getColumnIndex("sobrante"));
                            int diferencia = cursor.getInt(cursor.getColumnIndex("diferencia"));
                            productoCompensado.producto = cursor.getString(cursor.getColumnIndex("producto")).trim();
                            productoCompensado.diferencia = diferencia;
                            productoCompensado.linea = productosCompensacion.linea;


                            /*si el producto ya esta compensado continuar con el siguiente*/
                            if (productosCompensacion.diferencia == 0)
                                continue;

                            if (sobrante >= productosCompensacion.diferencia) {
                                ejecucionCompleta = false;
                                /*cargar binds para stmtUpdateSobrantes*/
                                stmtUpdateSobrantes.bindLong(1, productosCompensacion.diferencia);
                                stmtUpdateSobrantes.bindString(2, productosCompensacion.producto);

                                /*cargar binds para stmtUpdateFaltantes*/
                                stmtUpdateFaltantes.bindLong(1, (productosCompensacion.diferencia * -1));
                                stmtUpdateFaltantes.bindString(2, cursor.getString(cursor.getColumnIndex("producto")).trim());
                                stmtUpdateSobrantes.executeUpdateDelete();
                                stmtUpdateFaltantes.executeUpdateDelete();

                                /*la cantidad cambiada es la cantidad que fue compensada, en este caso fue el total de sobrante (diferencia)*/
                                productoCompensado.cantidadCambio = productosCompensacion.diferencia;
                                productosCompensacion.cantidad = Math.abs(productosCompensacion.diferencia);
                                productoCompensado.cantidad = productosCompensacion.cantidad * -1;

                                /*conservar detalle de compensacion*/
                                listaDetallesCompensacion.add((ProductosCompensacion) productosCompensacion.clone());
                                listaDetallesCompensacion.add((ProductosCompensacion) productoCompensado.clone());

                                /*poner en cero la diferencia del faltante porque ya fue compensada*/
                                productosCompensacion.diferencia = 0;
                            } else {
                                /*iniciar el flag para la operacion actual*/
                                ejecucionCompleta = false;
                                /*cargar binds para stmtUpdateSobrantes*/
                                stmtUpdateSobrantes.bindLong(1, sobrante);
                                stmtUpdateSobrantes.bindString(2, productosCompensacion.producto);

                                /*cargar binds para stmtUpdateFaltantes*/
                                stmtUpdateFaltantes.bindLong(1, diferencia);
                                stmtUpdateFaltantes.bindString(2, productoCompensado.producto);
                                stmtUpdateSobrantes.executeUpdateDelete();
                                stmtUpdateFaltantes.executeUpdateDelete();

                                /* para este caso la cantidad sobrante es lo que se compensa respecto al sobrante (diferencia)*/
                                productosCompensacion.diferencia -= sobrante;
                                productoCompensado.cantidadCambio = sobrante;
                                productosCompensacion.cantidad = Math.abs(sobrante);
                                productoCompensado.cantidad = productosCompensacion.cantidad * -1;

                                /*conservar detalle de compensacion*/
                                listaDetallesCompensacion.add((ProductosCompensacion) productosCompensacion.clone());
                                listaDetallesCompensacion.add((ProductosCompensacion) productoCompensado.clone());
                            }

                            /*marcar la ejecucion actual como completa*/
                            ejecucionCompleta = true;
                        } while (cursor.moveToNext());
                    }

                    /*liberar el cursor actual.*/
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                        cursor = null;
                    }
                }

                /*Marcar la transaccion como exitosa*/
                if (ejecucionCompleta) {
                    db.setTransactionSuccessful();
                }
            }
        } catch (Exception e) {
            ejecucionCompleta = false;
            Log.e("Calculo de compensaciones error ---> ", e.getMessage());
        } finally {
            closeDataBase(db);
        }
        return ejecucionCompleta;
    }


    /**
     * Metodo para generar el encabezado y el detalle de una transaccion de compensacion.
     *
     * @param bodega
     * @param codigoVendedor
     * @param numeroDocumento
     * @param listaDetallesCompensacion
     */
    public static void generarEncabezadoYDetallesCompensacion(final String bodega, final String codigoVendedor, final String numeroDocumento,
                                                              final ArrayList<ProductosCompensacion> listaDetallesCompensacion) {

        SQLiteDatabase db = null;
        SQLiteDatabase tmp = null;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            File tmpFile = new File(Util.DirApp(), "Temp.db");

            if (dbFile.exists() && tmpFile.exists()) {
                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                tmp = SQLiteDatabase.openDatabase(tmpFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                /*iniciar la transaccion*/
                db.beginTransaction();
                tmp.beginTransaction();

                /*Contenido del encabezado*/
                ContentValues values = new ContentValues();
                values.put("Codigo", codigoVendedor);
                values.put("NumeroDoc", numeroDocumento);
                values.put("FechaTrans", Util.FechaActual("yyyy-MM-dd HH:mm:ss.SSS"));
                values.put("TipoTrans", 6);
                values.put("MontoFact", 0);
                values.put("Desc1", 0);
                values.put("Desc2", 0);
                values.put("Flete", 0);
                values.put("Costo", 0);
                values.put("Iva", 0);
                values.put("Usuario", codigoVendedor);
                values.put("Sincronizado", 0);
                values.put("Telefono", 0);
                values.put("Observaciones", "COMPENSACIONES");
                values.put("Bodega", bodega);
                values.putNull("OrdenCompra");
                values.putNull("FechaSinc");
                values.put("Tipocliente", 0);
                values.put("Fechainicial", Util.FechaActual("yyyy-MM-dd HH:mm:ss.SSS"));
                values.put("FechaFinal", Util.FechaActual("yyyy-MM-dd HH:mm:ss.SSS"));
                values.putNull("fechareal");
                values.putNull("Fechaentrega");
                values.putNull("entregado");
                values.put("factura", "");
                values.put("sincronizadomapas", 0);
                values.put("FP", "");
                values.put("anulado", 0);
                values.put("impreso", 1);
                values.putNull("liquidador");
                values.putNull("FP2");

                /*insertar encabezado*/
                db.insertOrThrow("encabezadosugerido", null, values);
                tmp.insertOrThrow("encabezadosugerido", null, values);
                values.clear();
                ;

                /*contador de precios para evitar perdida de datos en servidor por precios repetidos en cero*/
                int contPrecios = 0;
                /*si no se produce excepcion en el encabezado, iniciar insercion de detalles*/
                for (ProductosCompensacion productosCompensacion : listaDetallesCompensacion) {
                    values.put("Numdoc", numeroDocumento);
                    values.put("Fecha", Util.FechaActual("yyyy-MM-dd HH:mm:ss.SSS"));
                    values.put("CodigoRef", productosCompensacion.producto);
                    values.put("Precio", contPrecios);
                    values.put("Costo", 0);
                    values.put("TarifaIva", 0);
                    values.put("DescuentoRenglon", 0);
                    values.put("Cantidad", productosCompensacion.cantidad);
                    values.put("Sincronizado", 0);
                    values.put("vendedor", codigoVendedor);
                    values.put("Cantidadcambio", productosCompensacion.cantidadCambio);
                    values.put("Bodega", bodega);
                    values.putNull("Item");
                    values.put("Bodega2", "");
                    values.putNull("OrdenCompra");
                    values.putNull("FechaSinc");
                    values.put("Tipocliente", 0);
                    values.put("Inventario", 0);
                    values.putNull("Fechareal");
                    values.put("Motivo", "");
                    values.putNull("fechaentrega");
                    values.putNull("cantidadentrega");
                    values.putNull("sincronizadomapas");
                    values.put("tipoventa", 0);
                    values.put("anulado", 0);

                    /*insertar el detalle de compensacion*/
                    db.insertOrThrow("detallesugerido", null, values);
                    tmp.insertOrThrow("detallesugerido", null, values);
                    values.clear();
                    /*incrementar los precios en una unidad.*/
                    contPrecios++;
                }

                /*marcar la transaccion como completa exitosamente, si no se ha generado excepcion en el bucle*/
                db.setTransactionSuccessful();
                tmp.setTransactionSuccessful();

                /*vaciar referencias*/
                values.clear();
                values = null;
            }
        } catch (Exception e) {
            Log.e("generarEncabezadoYDetallesCompensacion --> ", e.getMessage());
        } finally {
            closeDataBase(db);
            closeDataBase(tmp);
        }
    }


    public static String obtenerFacturaNroDoc(String numeroDoc) {

        mensaje = "";
        String factura = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT factura FROM encabezado where numeroDoc = '" + numeroDoc + "' ", null);

            if (cursor.moveToFirst()) {

                factura = cursor.getString(cursor.getColumnIndex("factura"));

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaMotivosCambio", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return factura;
    }


    public static boolean existeInformacionEncabezadoSugerido() {

        boolean existe = false;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select * from EncabezadoSugerido where tipotrans=3";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                existe = true;
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return existe;
    }

    public static boolean ExisteCargueFinalizado() {

        mensaje = "";
        int total = 0;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select count(*) as total from encabezadosugerido where TipoTrans = " + Const.TIPO_TRANS_CARGUE;

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Consulta Satisfactoria";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ExistePedidoCliente", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }

    public static boolean eliminarCompensacion(String nroDoc) {

        SQLiteDatabase db = null;
        SQLiteDatabase dbPedido = null;
        int filasAfectadas = 0;
        int filasAfectadas2 = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                File filePedido = new File(Util.DirApp(), "Temp.db");
                dbPedido = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                filasAfectadas = db.delete("encabezadosugerido", "NumeroDoc = ?", new String[]{nroDoc});
                filasAfectadas2 = dbPedido.delete("encabezadosugerido", "NumeroDoc = ?", new String[]{nroDoc});

                if (filasAfectadas > 0 && filasAfectadas2 > 0)
                    filasAfectadas = db.delete("detallesugerido", "NumeroDoc = ?", new String[]{nroDoc});
                filasAfectadas2 = dbPedido.delete("detallesugerido", "NumeroDoc = ?", new String[]{nroDoc});


            } else {

                Log.e(TAG, "eliminarConsignacion: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "eliminarConsignacion: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();

            if (dbPedido != null)
                dbPedido.close();
        }

        if (filasAfectadas2 > 0)
            return true;
        else
            return false;

    }

    public static boolean existeCompensacion() {

        SQLiteDatabase db = null;
        int total = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select count(*) as total from encabezadosugerido where Observaciones = 'COMPENSACIONES' ";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return total > 0;
    }

    public static String ObtenterNroDocCompensacion(String tipo) {

        String numDoc = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            Cursor cursor = db.rawQuery("SELECT NumeroDoc FROM encabezadosugerido WHERE observaciones = '" + tipo + "' ", null);

            if (cursor.moveToFirst()) {

                numDoc = cursor.getString(cursor.getColumnIndex("NumeroDoc"));
            }

            if (cursor != null)
                cursor.close();

            return numDoc;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return "-";

        } finally {

            if (db != null)
                db.close();
        }
    }

    //Inventario de canastas
    public static Vector<RegCanastilla> listaInvCanastilla(String codCliente) {

        Vector<RegCanastilla> listaProducto = new Vector<RegCanastilla>();

        mensaje = "";
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
            String query = "";

            if (dbFile.exists()) {

                query = "SELECT codigo, nombre, cantidad from canastascliente where cliente = '" + codCliente + "' ";

                System.out.println("Query listaCanastilla--> " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {
                        RegCanastilla producto = new RegCanastilla();
                        producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                        producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                        producto.strCantidad = cursor.getString(cursor.getColumnIndex("cantidad"));

                        listaProducto.addElement(producto);
                    } while (cursor.moveToNext());

                }
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            closeDataBase(db);
        }
        return listaProducto;
    }


    public static Vector<Cliente> ListaClientesRuteroAux(String dia) {

        mensaje = "";
        Cliente cliente;
        SQLiteDatabase db = null;

        ItemListView itemListView;
        Vector<Cliente> listaClientes = new Vector<Cliente>();
        Usuario usuario = DataBaseBOJ.ObtenerUsuario();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";

            if (usuario.tipoRutero != 0) {

                int diaRuta2 = Integer.parseInt(dia);

                if (diaRuta2 > 3 && diaRuta2 < 11) {

                    String condicion = "substr(rutanueva, " + diaRuta2 + ",1) = '1'";

                    query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, "
                            + "TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, rutanueva, " +
                            "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad " +
                            "from clientes " +
                            "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
                            + "and Rutero.pendiente = 0 "
                            + "WHERE " + condicion + " "
                            + "AND clientes.bloqueado <> 'S' "
                            + "order by rutero.ordenVisita";

                    System.out.println("Consulta --> " + query);

                } else if (diaRuta2 == 11) {

                    String condicion = "rutanueva = '" + usuario.codigoVendedor + "000000" + "' ";

                    query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, "
                            + "TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, rutanueva, " +
                            "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad " +
                            "from clientes " +
                            "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
                            + "and Rutero.pendiente = 0 "
                            + "and " + condicion + " "
                            + "WHERE clientes.bloqueado <> 'S' "
                            + "order by rutero.ordenVisita";

                }

            } else {


                query = "select Clientes.Codigo, Nombre, RazonSocial, Direccion, Telefono, Nit, Ciudad, CodigoAmarre, Canal, SubCanal, Cupo, TipoCredito, DANE, Bloqueado, tipologia2, diasIngreso, bodega,cliacummesbon, " +
                        "(SELECT COUNT(op.[codigo]) FROM [oportunidadclientes] op WHERE op.[codigo] = Clientes.codigo ) AS oportunidad " +
                        "from clientes " +
                        "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo and Rutero.pendiente = 0 and Rutero.diaVisita = '" + dia + "' "
                        + "WHERE clientes.bloqueado <> 'S' " +
                        "order by rutero.ordenVisita";
            }


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    cliente = new Cliente();

                    listaClientes.addElement(cliente);

                } while (cursor.moveToNext());

                mensaje = "Rutero Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaClientesRutero", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaClientes;
    }


    public static double getValorRecaudoCliente(String codCliente) {

        SQLiteDatabase db = null;
        String sql = "";

        double recaudo = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select ifnull(sum(total),0) as recaudo from encabezadorecaudo where codigocliente = '" + codCliente + "'";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    recaudo = cursor.getDouble(cursor.getColumnIndex("recaudo"));

                } while (cursor.moveToNext());

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }

        return recaudo;
    }


    public static boolean insertarInventarioLiquidacion(String nroDocInforme) {
        SQLiteDatabase db = null;
        SQLiteDatabase tmp = null;
        String sql = "";
        boolean inserto = false;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            File tmpFile = new File(Util.DirApp(), "Temp.db");

            if (dbFile.exists() && tmpFile.exists()) {
                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                tmp = SQLiteDatabase.openDatabase(tmpFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


                sql = "SELECT codigo, '" + nroDocInforme
                        + "', datetime('now','localtime') fechatrans, 5 tipotrans, 0 montofact, 0 desc1,0 iva, codigo usuario,1 sincronizado, "
                        + "'NO LIQUIDACION INV FISICO PRODUCTO BUENO' observacion, bodega, 0 tipocliente, datetime('now','localtime') fechainicial, datetime('now','localtime') fechafinal "
                        + "FROM Vendedor";

                Cursor cursor = db.rawQuery(sql, null);
                ContentValues values = new ContentValues();

                if (cursor.moveToFirst()) {

                    values.put("codigo", cursor.getString(cursor.getColumnIndex("codigo")));
                    values.put("numerodoc", nroDocInforme);
                    values.put("fechatrans", cursor.getString(cursor.getColumnIndex("fechatrans")));
                    values.put("tipotrans", cursor.getString(cursor.getColumnIndex("tipotrans")));
                    values.put("montofact", cursor.getString(cursor.getColumnIndex("montofact")));
                    values.put("desc1", cursor.getString(cursor.getColumnIndex("desc1")));
                    values.put("iva", cursor.getString(cursor.getColumnIndex("iva")));
                    values.put("usuario", cursor.getString(cursor.getColumnIndex("usuario")));
                    values.put("sincronizado", cursor.getString(cursor.getColumnIndex("sincronizado")));
                    values.put("observaciones", cursor.getString(cursor.getColumnIndex("observacion")));
                    values.put("bodega", cursor.getString(cursor.getColumnIndex("bodega")));
                    values.put("tipocliente", cursor.getString(cursor.getColumnIndex("tipocliente")));
                    values.put("fechainicial", cursor.getString(cursor.getColumnIndex("fechainicial")));
                    values.put("fechafinal", cursor.getString(cursor.getColumnIndex("fechafinal")));

                    db.insertOrThrow("encabezadosugerido", null, values);
                    tmp.insertOrThrow("encabezadosugerido", null, values);

                    inserto = true;
                }
                if (cursor != null)
                    cursor.close();
            }

        } catch (Exception e) {
            inserto = false;
            mensaje = e.getMessage();
        } finally {
            if (db != null)
                db.close();

            if (tmp != null)
                tmp.close();
        }
        return inserto;
    }


    public static int getCantClientesRutero(String dia) {

        mensaje = "";
        Cliente cliente;
        SQLiteDatabase db = null;
        int total = 0;

        ItemListView itemListView;
        Vector<Cliente> listaClientes = new Vector<Cliente>();
        Usuario usuario = DataBaseBOJ.ObtenerUsuario();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "";

            if (usuario.tipoRutero != 0) {

                int diaRuta2 = Integer.parseInt(dia);

                if (diaRuta2 > 3 && diaRuta2 < 11) {

                    String condicion = "substr(rutanueva, " + diaRuta2 + ",1) = '1'";

                    query = "select COUNT(*) AS total " +
                            "from clientes " +
                            "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
                            + "WHERE " + condicion + " "
                            + "AND clientes.bloqueado <> 'S' "
                            + "order by rutero.ordenVisita";

                    System.out.println("Consulta --> " + query);

                } else if (diaRuta2 == 11) {

                    String condicion = "rutanueva = '" + usuario.codigoVendedor + "000000" + "' ";

                    query = "select COUNT(*) AS total " +
                            "from clientes " +
                            "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo "
                            + "and Rutero.pendiente = 0 "
                            + "and " + condicion + " "
                            + "WHERE clientes.bloqueado <> 'S' "
                            + "order by rutero.ordenVisita";

                }

            } else {


                query = "select COUNT(*) AS total " +
                        "from clientes " +
                        "INNER JOIN Rutero ON Clientes.codigo = Rutero.codigo and Rutero.pendiente = 0 and Rutero.diaVisita = '" + dia + "' "
                        + "WHERE clientes.bloqueado <> 'S' " +
                        "order by rutero.ordenVisita";
            }


            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                total = cursor.getInt(cursor.getColumnIndex("total"));

                mensaje = "Rutero Cargado Correctamente";

            } else {

                mensaje = "Consulta sin Resultados";
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaClientesRutero", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return total;
    }


    public static ArrayList<CabeceraTirilla> cargarCabeceraTirilla() {

        SQLiteDatabase db = null;
        ArrayList<CabeceraTirilla> listaCabecera = new ArrayList<CabeceraTirilla>();
        CabeceraTirilla cabecera;

        try {

            int i = 0;
            File dbFile = new File(Util.DirApp(), "Database.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "Select prefijo, empresa, nit, direccion, telefono, texto1 FROM encabezadotirilla ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                cabecera = new CabeceraTirilla();

                cabecera.prefijo = cursor.getString(cursor.getColumnIndex("prefijo"));
                cabecera.empresa = cursor.getString(cursor.getColumnIndex("empresa"));
                cabecera.nit = cursor.getString(cursor.getColumnIndex("nit"));
                cabecera.direccion = cursor.getString(cursor.getColumnIndex("direccion"));
                cabecera.telefono = cursor.getString(cursor.getColumnIndex("telefono"));
                cabecera.texto = cursor.getString(cursor.getColumnIndex("texto1"));

                listaCabecera.add(cabecera);

            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "ListaFormasPago:" + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaCabecera;
    }


    /**
     * metodo que permite conocer si existe o no cabecera de tirilla
     *
     * @return, si hay o no cabeceras disponibles.
     */
    public static boolean cabeceraDisponibles() {
        SQLiteDatabase db = null;
        int disponible = 0;

        try {
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT COUNT(*) AS disponible FROM  encabezadotirilla ";

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                disponible = cursor.getInt(cursor.getColumnIndex("disponible"));

            }

            if (cursor != null) {
                cursor.close();
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
        } finally {

            if (db != null)
                db.close();
        }

        return disponible > 0;
    }


    /**
     * @param numeroDoc
     * @return Metodo que retorna un segmento con el detalle del recaudo
     */
    public static Vector<FormaPago> getDetalleRecaudo(String numeroDoc) {

        SQLiteDatabase db = null;
        String sql = "";
        FormaPago formaPago;
        Vector<FormaPago> listaFormasPago = new Vector<FormaPago>();

        try {

            int i = 0;
            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            sql = "select reference,valor_fp valor from detallerecaudo where nrodoc = '" + numeroDoc + "' group by reference ";

            System.out.println("getDetalleRecaudo " + sql);

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {

                do {

                    formaPago = new FormaPago();
                    formaPago.factura = cursor.getString(cursor.getColumnIndex("reference"));
                    formaPago.monto = cursor.getFloat(cursor.getColumnIndex("valor"));
                    listaFormasPago.addElement(formaPago);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
        } catch (Exception e) {

        } finally {

            closeDataBase(db);
        }

        return listaFormasPago;
    }

    public static boolean CargarTotalPedidosSinEnviar() {

        mensaje = "";
        SQLiteDatabase db = null;
        int cantTotal = 0;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT COUNT(*) as total FROM Encabezado " +
                        "INNER JOIN Clientes ON Encabezado.codigo = Clientes.Codigo " +
                        "WHERE tipoTrans = '0'  AND fechaFinal IS NOT NULL AND syncmobile = 0 ORDER BY tipoTrans";
                System.out.println("Consulta total visitas sin enviar: " + query);

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    cantTotal = cursor.getInt(cursor.getColumnIndex("total"));
                }


                if (cursor != null)
                    cursor.close();

            } else {

                Log.e(TAG, "CargarTotalPedidosSinEnviar: No existe la Base de Datos");
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "CargarTotalPedidosSinEnviar: " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return cantTotal >= 5;

    }

    public static Usuario obtenerUsuario() {

        mensaje = "";
        Usuario usuario = null;
        SQLiteDatabase db = null;

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT codigo AS codigo, nombre AS nombre, fechaLabores, fechaConsecutivo FROM Vendedor";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    usuario = new Usuario();
                    usuario.codigoVendedor = cursor.getString(cursor.getColumnIndex("codigo"));
                    usuario.bodega = usuario.codigoVendedor;
                    usuario.nombreVendedor = cursor.getString(cursor.getColumnIndex("nombre"));
                    usuario.fechaLabores = cursor.getString(cursor.getColumnIndex("fechaLabores"));
                    usuario.fechaConsecutivo = cursor.getString(cursor.getColumnIndex("fechaConsecutivo"));
                    //usuario.pedidoMinimo = (long) cursor.getFloat(cursor.getColumnIndex("pedidominimo"));
                }

                if (cursor != null)
                    cursor.close();

            } else {

                // Log.e(TAG, "obtenerUsuario -> " + Msg.NO_EXISTE_BD);
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            // Log.e(TAG, "obtenerUsuario -> " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return usuario;
    }


    public static Vector<Producto> listaProductoEstadisticas(String filtro, Vector<ItemListView> listaItems, String and) {

        mensaje = "";
        SQLiteDatabase db = null;

        Producto producto = new Producto();
        ItemListView itemListView;
        Vector<Producto> listaProducto = new Vector<Producto>();

        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


//			    String	query ="Select f.codigo as codigo, f.nombre, p.precio, p.iva, p.saldo, (p.precio + ((p.precio * p.iva) / 100)) as precioIva, p.unidadesxcaja, p.ean, p.agrupacion, p.grupo, p.linea, p.unidadmedida, p.factorlibras, p.fechalimite, p.devol, p.itf, p.impto1, p.Indice "
//				    + "from filtros f "
//				    + "inner join ProductosTmp p on f.codigo = p.codigo "+and+filtro +" order by p.orden" ;


            String query = "   select d.codigoRef as codigo, MAX(p.nombre) as nombre, SUM(d.cantidad) as cantidad, SUM(d.cantidad*d.Precio) as total, l.nombre as linea, d.cantidadcambio \n" +
                    "from detalle d\n" +
                    "INNER JOIN Productos p ON p.codigo = d.codigoRef      \n" +
                    "INNER JOIN  Lineas l ON p.linea = l.codigo " + filtro + " group by p.codigo ";

            System.out.println("Consulta listaFiltro Productos: " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                do {

                    producto = new Producto();
                    itemListView = new ItemListView();


                    //Se valida que el Saldo no Contenga e+
                    producto.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    producto.descripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    producto.total = cursor.getFloat(cursor.getColumnIndex("total"));
                    producto.cantidad = cursor.getInt(cursor.getColumnIndex("cantidad"));
                    producto.lineaProd = cursor.getString(cursor.getColumnIndex("linea"));
                    // producto.categoria        = cursor.getString(cursor.getColumnIndex("categoria"));
                    producto.cantidadCambio = cursor.getInt(cursor.getColumnIndex("cantidadcambio"));

                    if (producto.cantidadCambio > 0 && producto.cantidad > 0) {
                        itemListView.titulo = producto.codigo + " - " + producto.descripcion;
                        itemListView.subTitulo = "Cantidad: " + producto.cantidad + " Total: " + producto.total
                                + "\n CantDevolucion: " + producto.cantidadCambio;
                    } else if (producto.cantidadCambio > 0 && producto.cantidad == 0) {

                        itemListView.titulo = producto.codigo + " - " + producto.descripcion;
                        itemListView.subTitulo = "CantDevolucion: " + producto.cantidadCambio;
                    } else {
                        itemListView.titulo = producto.codigo + " - " + producto.descripcion;
                        itemListView.subTitulo = "Cantidad: " + producto.cantidad + " Total: " + producto.total;
                    }
                    listaProducto.addElement(producto);
                    listaItems.addElement(itemListView);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "listaCartera - > " + mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaProducto;
    }


    //Filtro Pedidos Clase
    public static Vector<Filtro> listaFiltroClaseEstadistica(Vector<String> items, String filtroFamilia) {
        SQLiteDatabase db = null;
        Filtro clase;
        Vector<Filtro> listadoClase = new Vector<Filtro>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "select categoria from filtros as f inner join Detalle d on f.codigo = d.codigoRef " + filtroFamilia + " GROUP BY categoria";

            System.out.println("Query de Clase---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                clase = new Filtro();

                clase.filtroDescripcion = "TODOS";

                listadoClase.addElement(clase);
                items.addElement(clase.filtroDescripcion);

                do {

                    clase = new Filtro();

                    //								familia.codigo = cursor.getString(cursor.getColumnIndex("codigo"));
                    clase.filtroDescripcion = cursor.getString(cursor.getColumnIndex("categoria"));

                    listadoClase.addElement(clase);
                    items.addElement(clase.filtroDescripcion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaProveedores", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listadoClase;
    }

    //Filtro Pedidos Familia
    public static Vector<Filtro> listaFiltroFamiliaEstadistica(Vector<String> items) {
        SQLiteDatabase db = null;
        Filtro filtro;
        Vector<Filtro> listadoFamilia = new Vector<Filtro>();

        try {

            if (items == null)
                items = new Vector<String>();

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

            String query = "SELECT l.Nombre as nombre, l.Codigo as codigo \n" +
                    " FROM Lineas l \n" +
                    " INNER JOIN Productos p ON l.Codigo = p.linea \n" +
                    "  INNER JOIN Detalle d on p.codigo = d.codigoRef \n" +
                    "   GROUP BY p.codigo";

            System.out.println("Query de Familia---> " + query);

            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {

                filtro = new Filtro();

                filtro.filtroDescripcion = "TODOS";

                listadoFamilia.addElement(filtro);
                items.addElement(filtro.filtroDescripcion);

                do {

                    filtro = new Filtro();

                    filtro.filtroDescripcion = cursor.getString(cursor.getColumnIndex("nombre"));
                    filtro.filtroCodigo = cursor.getString(cursor.getColumnIndex("codigo"));

                    listadoFamilia.addElement(filtro);
                    items.addElement(filtro.filtroDescripcion);

                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("ListaProveedores", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listadoFamilia;
    }


    public static boolean hayCoordenadas(String codigo) {
        mensaje = "";
        SQLiteDatabase db = null;
        int cantTotal = 0;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                String query = "SELECT count(*) as total FROM clientescoordenadas WHERE codigo= '" + codigo + "' ";
                System.out.println("Consulta total visitas sin enviar: " + query);
                Cursor cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    cantTotal = cursor.getInt(cursor.getColumnIndex("total"));
                }

                if (cursor != null)
                    cursor.close();

            } else {
                Log.e(TAG, "CargarTotalPedidosSinEnviar: No existe la Base de Datos");
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "CargarTotalPedidosSinEnviar: " + mensaje, e);
        } finally {
            if (db != null)
                db.close();
        }

        return cantTotal >= 1;

    }

    public static boolean hayCoordenadas2(String codigo) {
        mensaje = "";
        SQLiteDatabase db = null;
        int cantTotal = 0;
        try {

            File dbFile = new File(Util.DirApp(), "DataBase.db");
            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                String query = "SELECT count(*) as total FROM clientescoordenadas WHERE codigo= '" + codigo + "' ";
                System.out.println("Consulta total visitas sin enviar: " + query);
                Cursor cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    cantTotal = cursor.getInt(cursor.getColumnIndex("total"));
                }

                if (cursor != null)
                    cursor.close();

            } else {
                Log.e(TAG, "CargarTotalPedidosSinEnviar: No existe la Base de Datos");
            }

        } catch (Exception e) {
            mensaje = e.getMessage();
            Log.e(TAG, "CargarTotalPedidosSinEnviar: " + mensaje, e);
        } finally {
            if (db != null)
                db.close();
        }

        return cantTotal >= 1;

    }

    public static boolean guardarCoordenadaClienteNuevo(Coordenada coordenada) {

        long rows = 0;
        boolean existe = false;
        SQLiteDatabase dbTemp = null;
        SQLiteDatabase db = null;

        try {

            File filePedido = new File(Util.DirApp(), "Temp.db");
            File filedb = new File(Util.DirApp(), "DataBase.db");

            if (filePedido.exists() && filedb.exists()) {

                if (coordenada.codigoCliente != null) {

                    dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                    db = SQLiteDatabase.openDatabase(filedb.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    ContentValues values = new ContentValues();
                    values = new ContentValues();
                    values.put("codigo", coordenada.codigoCliente);
                    values.put("latitud", coordenada.latitud);
                    values.put("longitud", coordenada.longitud);

                    rows = dbTemp.insertOrThrow("clientescoordenadas", null, values);
                    rows = db.insertOrThrow("clientescoordenadas", null, values);

                    if (coordenada.codigoCliente != null)

                        dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                    db = SQLiteDatabase.openDatabase(filedb.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                
                    values = new ContentValues();
                    values.put("codigo", coordenada.codigoCliente);
                    values.put("latitud", coordenada.latitud);
                    values.put("longitud", coordenada.longitud);

                    rows = dbTemp.insertOrThrow("coordenadas", null, values);
                    rows = db.insertOrThrow("coordenadas", null, values);
                }

            } else {
                Log.e(TAG, "GuardarCoordenadas en clientescoordenadas -> " + Msg.NO_EXISTE_BD);
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarCoordenada -> " + mensaje, e);
            return false;

        } finally {
            if (dbTemp != null)
                dbTemp.close();
            if (db != null)
                db.close();
        }

        return true;

    }

    public static boolean guardarCoordenadaFormPrincipal(Coordenada coordenada) {

        long rows = 0;
        boolean existe = false;
        SQLiteDatabase dbTemp = null;
        SQLiteDatabase db = null;

        try {

            File filePedido = new File(Util.DirApp(), "Temp.db");
            File filedb = new File(Util.DirApp(), "DataBase.db");

            if (filePedido.exists() && filedb.exists()) {

                if (coordenada.codigoCliente != null) {

                    dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                    db = SQLiteDatabase.openDatabase(filedb.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    ContentValues values = new ContentValues();
                    values = new ContentValues();
                    values.put("CodigoVendedor", coordenada.codigoVendedor);

                    values.put("codigo", coordenada.codigoCliente);
                    values.put("latitud", coordenada.latitud);
                    values.put("longitud", coordenada.longitud);

                    rows = dbTemp.insertOrThrow("Coordenadas", null, values);
                    rows = db.insertOrThrow("Coordenadas", null, values);

                }

            } else {
                Log.e(TAG, "GuardarCoordenadas en Coordenadas -> " + Msg.NO_EXISTE_BD);
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarCoordenada -> " + mensaje, e);
            return false;

        } finally {
            if (dbTemp != null)
                dbTemp.close();
            if (db != null)
                db.close();
        }

        return true;

    }


    public static boolean guardarCoordenadaRegistroNovedadesNoCompra(Coordenada coordenada) {

        long rows = 0;
        boolean existe = false;
        SQLiteDatabase dbTemp = null;
        SQLiteDatabase db = null;

        try {

            File filePedido = new File(Util.DirApp(), "Temp.db");
            File filedb = new File(Util.DirApp(), "DataBase.db");

            if (filePedido.exists() && filedb.exists()) {

                if (coordenada.codigoCliente != null) {

                    dbTemp = SQLiteDatabase.openDatabase(filePedido.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                    db = SQLiteDatabase.openDatabase(filedb.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                    ContentValues values = new ContentValues();
                    values = new ContentValues();
                    values.put("CodigoVendedor", coordenada.codigoVendedor);
                    values.put("codigo", coordenada.codigoCliente);
                    values.put("latitud", coordenada.latitud);
                    values.put("longitud", coordenada.longitud);

                    rows = dbTemp.insertOrThrow("Coordenadas", null, values);
                    rows = db.insertOrThrow("Coordenadas", null, values);

                }

            } else {
                Log.e(TAG, "GuardarCoordenadas en Coordenadas Modulo RegistroNovedades -> " + Msg.NO_EXISTE_BD);
                return false;
            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e(TAG, "GuardarCoordenada -> " + mensaje, e);
            return false;

        } finally {
            if (dbTemp != null)
                dbTemp.close();
            if (db != null)
                db.close();
        }

        return true;

    }

}