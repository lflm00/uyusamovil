package celuweb.com.BusinessObject;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;

import celuweb.com.DataObject.Encuestas;
import celuweb.com.DataObject.FotoEncuesta;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.ParametroPregunta;
import celuweb.com.DataObject.Pregunta;
import celuweb.com.DataObject.Respuesta;
import celuweb.com.uyusa.R;
import celuweb.com.uyusa.Util;


public class EncuestasBO {

	public static final String TAG = EncuestasBO.class.getName();

	//TIPO PREGUNTAS
	public final static int ABIERTA = 1;
	public final static int SELECCIONMULTIPLE = 2;
	public final static int SELECCIONUNICA = 3;
	public final static int SELECCIONUNICACONDICIONAL = 4;
	
	//TIPO DATOS PREGUNTAS
	public final static int ALFANUMERICO = 1;
	public final static int FECHA = 2;
	public final static int NUMERICA = 3;
	public final static int FECHAHORA = 4;
	public final static int FOTO = 5;
	
	//CONFIGURACIONES
	public final static int MAXLENGTH_TEXT = 150;
	public final static int MAXLENGTH_NUMBER = 12;
	
	public final static int IDBTNFECHA = 123450;
	public final static int IDBTNFECHAHORA = 123451;
	public final static int IDBTNFOTO = 123452;
	public final static String PREFERENCE = "ENCUESTAFOTO";
	public final static String PREFERENCEDOC = "ENCUESTAFOTO";
	
	public static final int WIDHTFOTO = 800;
	public static final int HEIGHTFOTO = 600;
	
	
	
	public static Vector<Encuestas> listarEncuestas(Vector<ItemListView> listaItems, String codCliente) {

		SQLiteDatabase db = null;
		Vector<Encuestas> listaEncuestas = new Vector<Encuestas>();
		Encuestas encuesta;
		ItemListView itemListView;
		//Cliente cliente = DataBaseBO.CargarClienteSeleccionado();
		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				
				//String filtroProspecto = " WHERE codEncuesta IN (SELECT codEncuesta FROM EsEncuestasSegmento WHERE (codTipoSegmento = 4)) ;";
				//String filtroCorresponsalBancario = " WHERE codEncuesta IN ("
				//		+ "	SELECT codEncuesta FROM EsEncuestasSegmento WHERE (codTipoSegmento >= 1 AND codTipoSegmento < 4) AND idDestino = '" + cliente.clase + "') ;";

				String query = "SELECT codEncuesta, descripcion, " +
						 	   "obligatoria, CASE WHEN unica IS NULL THEN 0 ELSE unica END AS unica, " +
						 	   "codPrimeraPregunta FROM EsEncuestas " ;//+ ((cliente.prospecto == 1)? filtroProspecto : filtroCorresponsalBancario) 
				
				Log.i(TAG, "listarEncuestas->" + query);
				
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					do {
						encuesta = new Encuestas();
						itemListView = new ItemListView();
						
						encuesta.codigo = cursor.getString(cursor.getColumnIndex("codEncuesta")).trim();
						encuesta.titulo = cursor.getString(cursor.getColumnIndex("descripcion"));
						encuesta.codPrimeraPregunta = cursor.getInt(cursor.getColumnIndex("codPrimeraPregunta"));
						encuesta.unica = cursor.getInt(cursor.getColumnIndex("unica"));
						
						if (encuesta.unica==1) {
							if( !validarRespuestasEncuesta(encuesta.codigo, codCliente) ){
								itemListView.titulo    = encuesta.titulo;
								itemListView.subTitulo    = "";
								listaItems.add(itemListView);
								listaEncuestas.addElement( encuesta );
							}
						}else{
							itemListView.titulo    = encuesta.titulo;
							listaItems.add(itemListView);
							listaEncuestas.addElement( encuesta );
						}
					} while (cursor.moveToNext());
				}
				if (cursor != null)
					cursor.close();
			}
		} catch (Exception e) {
			Log.e(TAG, "listarEncuestas->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}
		return listaEncuestas;
	}
	
	public static boolean validarRespuestasEncuesta(String codEncuesta, String codCliente) {

		int total = 0;
		SQLiteDatabase db = null;

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

				/*String query = "SELECT count(*) as total FROM EsRespuestas " +
							   "WHERE codEncuesta = '"+ codEncuesta +"' "+
							   "AND codCliente = '"+ codCliente +"'";*/
				
				String query = "SELECT SUM(total) as total FROM ("+
								"SELECT COUNT(*) as total FROM EsRespuestas "+
								"WHERE codEncuesta = '"+ codEncuesta +"' AND codCliente = '"+ codCliente +"'  "+
								"UNION  "+
								"SELECT COUNT(*) as total FROM EsEncuestasContestadas "+
								"WHERE codEncuesta = '"+ codEncuesta +"' AND codCliente = '"+ codCliente +"')";
				Log.i(TAG, "validarRespuestasEncuesta->"+query);
				
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					total = cursor.getInt( cursor.getColumnIndex( "total" ) );
				}
				if (cursor != null)
					cursor.close();
			}
			
		} catch (Exception e) {
			Log.e(TAG, "validarRespuestasEncuesta->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}
		return total > 0;
	}
	
	public static boolean hayRespuestasEncuestaObligatoria(String codCliente) {

		int total = 0;
		SQLiteDatabase db = null;

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

				boolean encuestaObligatoria = false;
				String query = "SELECT codEncuesta FROM EsEncuestas WHERE obligatoria = '1'";
				Log.i(TAG, "hayEncuestaObligatoria->"+query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.getCount()>0) {
					encuestaObligatoria = true;
				}else{
					total = 1;
				}
						
				if (encuestaObligatoria) {
					query = "SELECT SUM(total) as total FROM ( "+
							   "SELECT COUNT(*) AS total FROM EsRespuestas "+
							   "WHERE codEncuesta IN (SELECT codEncuesta FROM EsEncuestas WHERE obligatoria = '1') AND finalizado = '1' AND codCliente = '"+codCliente+"' "+
						       "UNION "+
						       "SELECT COUNT(*) as total FROM EsEncuestasContestadas WHERE codEncuesta IN (SELECT codEncuesta FROM EsEncuestas WHERE obligatoria = '1') AND codCliente = '"+codCliente+"' "+
						    ")";
					
					Log.i(TAG, "hayRespuestasEncuestaObligatoria->"+query);
					
					cursor = db.rawQuery(query, null);
					if (cursor.moveToFirst()) {
						total = cursor.getInt( cursor.getColumnIndex( "total" ) );
					}
				}
				
				if (cursor != null)
					cursor.close();
			}
			
		} catch (Exception e) {
			Log.e(TAG, "hayRespuestasEncuestaObligatoria->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}
		return total > 0;
	}
	
	
	
	
	public static Pregunta obtenerPregunta(int codEncuesta, int codPregunta, int codPreguntaOrigen) {
		
		SQLiteDatabase db = null;
		Pregunta pregunta = null;
		
		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				
				if (codPreguntaOrigen!=-1 && codPreguntaOrigen!=0) {
					actualizarPregunta(codEncuesta, codPregunta, codPreguntaOrigen);
				}
				
				String where = "";
				if (codPregunta != -1 && codPregunta != 0) {
					where = " AND codPregunta = '"+codPregunta+"' ";
				}
				String query = "SELECT codPregunta, codEncuesta, codTipoPregunta, descripcion, "+
							   "codTipoDatoEncuesta, codPreguntaDestino, codPreguntaOrigen " +
							   "FROM EsPreguntas " + 
							   "WHERE codEncuesta = '"+codEncuesta+"' " + where +
							   "ORDER BY codPregunta ASC LIMIT 1";
				Log.i(TAG, "obtenerPregunta->"+query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					do {
						pregunta = new Pregunta();
						pregunta.codPregunta  = cursor.getInt(cursor.getColumnIndex("codPregunta"));
						pregunta.codEncuesta  = cursor.getInt(cursor.getColumnIndex("codEncuesta"));
						pregunta.codTipoPregunta = cursor.getInt(cursor.getColumnIndex("codTipoPregunta"));
						pregunta.descripcion       = cursor.getString(cursor.getColumnIndex("descripcion"));
						pregunta.codTipoDatoEncuesta  = cursor.getInt(cursor.getColumnIndex("codTipoDatoEncuesta"));					
						pregunta.codPreguntaDestino  = cursor.getInt(cursor.getColumnIndex("codPreguntaDestino"));	
						pregunta.codPreguntaOrigen  = cursor.getInt(cursor.getColumnIndex("codPreguntaOrigen"));	
					} while (cursor.moveToNext());
				} 
				if (cursor != null)
					cursor.close();
			}
		} catch (Exception e) {
			Log.e(TAG, "obtenerPregunta->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}

		return pregunta;
	}
	
	
	
	public static boolean actualizarPregunta(int codEncuesta, int codPregunta, int codPreguntaOrigen) {
		
		SQLiteDatabase db = null;
		boolean estado = false;
		
		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				String where = "codEncuesta = '"+codEncuesta+"' AND "+
						   	   "codPregunta = '"+codPregunta+"' ";
				ContentValues values = new ContentValues(); 
				values.put("codPreguntaOrigen", codPreguntaOrigen);
				db.update("EsPreguntas", values, where, null);	
			}
		} catch (Exception e) {
			Log.e(TAG, "obtenerPregunta->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}

		return estado;
	}
	
	public static Vector<ParametroPregunta> listarParametrosPregunta(int codPregunta) {

		SQLiteDatabase db = null;
		ParametroPregunta parametroPregunta = null;
		Vector<ParametroPregunta> listaParametroPregunta= new Vector<ParametroPregunta>();

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

				String query = "SELECT codParamPreg, codPregunta, descripcion, codPreguntaDestino " +
							   "FROM EsParamPreg " + 
							   "WHERE codPregunta = '"+codPregunta+"' " +
							   "ORDER BY codParamPreg";
				Log.i(TAG, "listarParametrosPregunta->"+query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					do {
						parametroPregunta = new ParametroPregunta();
						parametroPregunta.codParamPreg = cursor.getInt(cursor.getColumnIndex("codParamPreg"));
						parametroPregunta.codPregunta = cursor.getInt(cursor.getColumnIndex("codPregunta"));
						parametroPregunta.descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));
						parametroPregunta.codPreguntaDestino = cursor.getInt(cursor.getColumnIndex("codPreguntaDestino"));
						listaParametroPregunta.addElement(parametroPregunta);
					} while (cursor.moveToNext());
				}
				if (cursor != null)
					cursor.close();
			}
		} catch (Exception e) {
			Log.e(TAG, "listarParametrosPregunta->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}
		return listaParametroPregunta;
	}

	
	public static Vector<Respuesta> listarRespuestasPregunta(int codEncuesta, int codPregunta, String codCliente, String codVendedor) {

		SQLiteDatabase db = null;
		Respuesta respuesta = null;
		Vector<Respuesta> listaRespuestasPregunta= new Vector<Respuesta>();

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

				String query = "SELECT codEncuesta, codPregunta, codParamPreg, descripcionParamPreg, codCliente, codVendedor, " +
							   "numRespuesta, fechaMovil, respuesta, finalizado " +
							   "FROM EsRespuestas " + 
							   "WHERE codEncuesta= '"+codEncuesta+"' AND codPregunta = '"+codPregunta+"' "+
							   "AND codCliente = '"+codCliente+"' AND codVendedor = '"+codVendedor+"' "+
							   "AND finalizado = '0' " +
							   "ORDER BY codParamPreg ASC";
				Log.i(TAG, "listarRespuestasPregunta->"+query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					do {
						respuesta = new Respuesta();
						
						respuesta.codEncuesta = cursor.getInt(cursor.getColumnIndex("codEncuesta"));
						respuesta.codPregunta = cursor.getInt(cursor.getColumnIndex("codPregunta"));
						respuesta.codCliente = cursor.getString(cursor.getColumnIndex("codCliente"));
						respuesta.codVendedor = cursor.getString(cursor.getColumnIndex("codVendedor"));
						respuesta.numRespuesta = cursor.getString(cursor.getColumnIndex("numRespuesta"));
						respuesta.codParamPreg = cursor.getInt(cursor.getColumnIndex("codParamPreg"));
						respuesta.descripcionParamPreg = cursor.getString(cursor.getColumnIndex("descripcionParamPreg"));
						respuesta.respuesta = cursor.getString(cursor.getColumnIndex("respuesta"));
						listaRespuestasPregunta.addElement(respuesta);
					} while (cursor.moveToNext());
				}
				if (cursor != null)
					cursor.close();
			}
		} catch (Exception e) {
			Log.e(TAG, "listarRespuestasPregunta->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}
		return listaRespuestasPregunta;
	}
	
	
	public static boolean guardarRespuesta(Pregunta pregunta, String codCliente, String codVendedor, Vector<Respuesta> respuestas) {

		SQLiteDatabase db = null;
		SQLiteDatabase dbTemp = null;
		boolean finalizado = false;

		try {
			
			
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			File dbFileTemp = new File(Util.DirApp(), "Temp.db");
			
			if (dbFile.exists() && dbFileTemp.exists()) {
				
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				dbTemp = SQLiteDatabase.openDatabase(dbFileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				
				//ELIMINACION DE PREGUNTAS ANTERIORES. PARA EVITAR DIPLICIDAD DE RESPUESTAS
				String where = "codEncuesta = '"+pregunta.codEncuesta+"' AND "+
							   "codPregunta = '"+pregunta.codPregunta+"' AND "+
							   "codVendedor = '"+codVendedor+"' AND "+
							   "codCliente = '" +codCliente+"' ";
			
				System.out.println("guardarRespuesta-> ELIMINACION - where->"+where);
					
				db.delete("EsRespuestas", where, null);			
				dbTemp.delete("EsRespuestas", where, null);
				
				//INSERCION DE RESPUESTAS
				int contadorResAlm = 0;
				String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
				ContentValues values = new ContentValues();
				for (int i = 0; i < respuestas.size(); i++) {
					Respuesta respuestaIn = respuestas.get(i);
					values = new ContentValues(); 
					values.put("codEncuesta", pregunta.codEncuesta);
					values.put("codPregunta", pregunta.codPregunta);
					values.put("codParamPreg", respuestaIn.codParamPreg);
					values.put("descripcionParamPreg", respuestaIn.descripcionParamPreg);
					values.put("codCliente", codCliente);
					values.put("codVendedor", codVendedor);
					values.put("numRespuesta", respuestaIn.numRespuesta);
					values.put("fechaMovil", fechaActual);
					//values.put("fechaSinc", fechaActual);
					values.put("respuesta",    respuestaIn.respuesta);
					values.put("finalizado",  0);
					db.insertOrThrow("EsRespuestas", null, values);
					dbTemp.insertOrThrow("EsRespuestas", null, values);
					contadorResAlm++;
				}
					
				if (respuestas.size() == contadorResAlm) {
					finalizado = true;
				}else{
					finalizado = false;
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "guardarRespuesta->"+e.getMessage());
			finalizado = false;
		} finally {
			if (db != null)
				db.close();

			if (dbTemp != null)
				dbTemp.close();
		}
		return finalizado;
	}
	
	public static boolean borrarRespuestasPregunta(int codEncuesta, int codPregunta, String numDocEncuesta, String codCliente, String codVendedor) {
		SQLiteDatabase db = null;
		SQLiteDatabase dbTemp = null;
		boolean borrado = false;

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			File dbFileTemp = new File(Util.DirApp(), "Temp.db");
			
			if (dbFile.exists() && dbFileTemp.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				dbTemp = SQLiteDatabase.openDatabase(dbFileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				
				//ELIMINACION DE PREGUNTA.
				String where = "codEncuesta = '"+codEncuesta+"' AND "+
							   "codPregunta = '"+codPregunta+"' AND "+
							   "numRespuesta = '"+numDocEncuesta+"' AND "+
							   "codVendedor = '"+codVendedor+"' AND "+
							   "codCliente = '" +codCliente+"' AND "+
							   "finalizado = '0'  ";
			
				System.out.println("borrarRespuestasPregunta-> ELIMINACION - where->"+where);
					
				int deletedb = db.delete("EsRespuestas", where, null);			
				int deletedbtmp = dbTemp.delete("EsRespuestas", where, null);
				
				where = "codEncuesta = '"+codEncuesta+"' AND "+
						"codPregunta = '"+codPregunta+"' AND "+
						"numRespuesta = '"+numDocEncuesta+"' AND "+
						"codVendedor = '"+codVendedor+"' AND "+
					    "codCliente = '" +codCliente+"' AND "+
						"finalizado = '0' ";
				
				db.delete("EsFotosEncuestas", where, null);			
				dbTemp.delete("EsFotosEncuestas", where, null);
				
				if (deletedb==deletedbtmp) {
					borrado = true;
				}
			}			
		}catch (Exception e) {
			Log.e(TAG, "borrarRespuestasPregunta->"+e.getMessage());
		}finally {
			if (db != null)
				db.close();

			if (dbTemp != null)
				dbTemp.close();	
		}
		return borrado;
	}
	
	public static boolean finalizarEncuesta(int codEncuesta, String numRespuesta, String codCliente, String codVendedor) {
		
		SQLiteDatabase db = null;
		SQLiteDatabase dbTemp = null;
		boolean estado = false;
		
		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			File dbFileTemp = new File(Util.DirApp(), "Temp.db");
			if (dbFile.exists() && dbFileTemp.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				dbTemp = SQLiteDatabase.openDatabase(dbFileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
								
				String where = "codEncuesta = '"+codEncuesta+"' AND "+
							   "numRespuesta = '"+numRespuesta+"' AND "+
						   	   "codCliente = '"+codCliente+"' AND "+
						   	   "codVendedor = '"+codVendedor+"' ";
	
				ContentValues values = new ContentValues(); 
				values.put("finalizado", 1);
					
				int dbup = db.update("EsRespuestas", values, where, null);			
				int dbtempup = dbTemp.update("EsRespuestas", values, where, null);
				
				int dbupF = db.update("EsFotosEncuestas", values, where, null);			
				int dbtempupF = dbTemp.update("EsFotosEncuestas", values, where, null);
				
				System.out.println(" finalizar dbup "+dbup+" dbtempup "+dbtempup);
				if (dbup >0 && dbtempup >0 && dbup==dbtempup) {
					estado = true;
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "obtenerPregunta->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
			
			if (dbTemp != null)
				dbTemp.close();
		}

		return estado;
	}
	
	
	
	public static boolean borrarRespuestasEncuesta(int codEncuesta, String numDocEncuesta, String codCliente, String codVendedor) {
		SQLiteDatabase db = null;
		SQLiteDatabase dbTemp = null;
		boolean borrado = false;

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			File dbFileTemp = new File(Util.DirApp(), "Temp.db");
			
			if (dbFile.exists() && dbFileTemp.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				dbTemp = SQLiteDatabase.openDatabase(dbFileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				
				//ELIMINACION DE PREGUNTAS ANTERIORES. PARA EVITAR DIPLICIDAD DE RESPUESTAS
				String where = "codEncuesta = '"+codEncuesta+"' AND "+
							   "codVendedor = '"+codVendedor+"' AND "+
							   "codCliente = '" +codCliente+"' AND "+
							   "finalizado = '0'  ";
			
				System.out.println("borrarRespuestasEncuesta-> ELIMINACION - where->"+where);
					
				int deletedb = db.delete("EsRespuestas", where, null);			
				int deletedbtmp = dbTemp.delete("EsRespuestas", where, null);
				
				where = "codEncuesta = '"+codEncuesta+"' AND "+
						"codVendedor = '"+codVendedor+"' AND "+
					    "codCliente = '" +codCliente+"' AND "+
						"finalizado = '0' ";
				
				db.delete("EsFotosEncuestas", where, null);			
				dbTemp.delete("EsFotosEncuestas", where, null);
				
				if (deletedb==deletedbtmp) {
					borrado = true;
				}
			}			
		}catch (Exception e) {
			Log.e(TAG, "borrarRespuestasEncuesta->"+e.getMessage());
		}finally {
			if (db != null)
				db.close();

			if (dbTemp != null)
				dbTemp.close();	
		}
		return borrado;
	}
	
	public static boolean borrarRespuestasEncuestaNoFinalizadas(String codVendedor) {
		SQLiteDatabase db = null;
		SQLiteDatabase dbTemp = null;
		boolean borrado = false;

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			File dbFileTemp = new File(Util.DirApp(), "Temp.db");
			
			if (dbFile.exists() && dbFileTemp.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				dbTemp = SQLiteDatabase.openDatabase(dbFileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				
				//ELIMINACION DE PREGUNTAS ANTERIORES. PARA EVITAR DIPLICIDAD DE RESPUESTAS
				String where = "finalizado = '0'  ";//"codVendedor = '"+codVendedor+"' AND codCliente = '" +codCliente+"' AND "+
			
				System.out.println("borrarRespuestasEncuestaNoFinalizadas-> ELIMINACION - where->"+where);
					
				int deletedb = db.delete("EsRespuestas", where, null);			
				int deletedbtmp = dbTemp.delete("EsRespuestas", where, null);
				
				where = "finalizado = '0' ";//"codVendedor = '"+codVendedor+"' AND codCliente = '" +codCliente+"' AND "+
				
				db.delete("EsFotosEncuestas", where, null);			
				dbTemp.delete("EsFotosEncuestas", where, null);
				
				if (deletedb==deletedbtmp) {
					borrado = true;
				}
			}			
		}catch (Exception e) {
			Log.e(TAG, "borrarRespuestasEncuesta->"+e.getMessage());
		}finally {
			if (db != null)
				db.close();

			if (dbTemp != null)
				dbTemp.close();	
		}
		return borrado;
	}

	
	public static boolean guardarFotoEncuesta(FotoEncuesta foto) {

		SQLiteDatabase db = null;
		SQLiteDatabase dbTemp = null;

		boolean state = false;

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			File dbFileTP = new File(Util.DirApp(), "Temp.db");

			if (dbFile.exists() && dbFileTP.exists()) {

				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				dbTemp = SQLiteDatabase.openDatabase(dbFileTP.getPath(), null, SQLiteDatabase.OPEN_READWRITE);


				//ELIMINACION DE PREGUNTAS ANTERIORES. PARA EVITAR DIPLICIDAD DE RESPUESTAS
				String where = "codEncuesta = '"+foto.codEncuesta+"' AND "+
							   "codPregunta = '"+foto.codPregunta+"' AND "+
							   "codVendedor = '"+foto.codVendedor+"' AND "+
							   "codCliente = '" +foto.codCliente+"' AND "+
							   "numRespuesta = '" +foto.numRespuesta+"' AND "+
							   "finalizado = '0'  ";
			
				System.out.println("guardarFotoEncuesta-> ELIMINACION - where->"+where);
					
				db.delete("EsFotosEncuestas", where, null);			
				dbTemp.delete("EsFotosEncuestas", where, null);
				
				ContentValues values = new ContentValues();
				values.put("codEncuesta", foto.codEncuesta);
				values.put("codPregunta", foto.codPregunta);
				values.put("codParamPreg", foto.codParamPreg);
				values.put("codCliente", foto.codCliente);
				values.put("codVendedor", foto.codVendedor);
				values.put("numRespuesta", foto.numRespuesta);
				values.put("fechaMovil", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
				values.put("fechaLlegada", Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
				values.put("foto", foto.imagen);
				values.put("numDoc", foto.numDoc);
				values.put("finalizado", 0);
				db.insertOrThrow("EsFotosEncuestas", null, values);
				dbTemp.insertOrThrow("EsFotosEncuestas", null, values);
				state = true;
			} else{
				state = false;
			}
		} catch (Exception e) {
			Log.e(TAG, "guardarFotoEncuesta->"+e.getMessage());
			state = false;
		} finally {
			if (db != null)
				db.close();

			if (dbTemp != null)
				dbTemp.close();	
		}

		return state;
	}

	public static boolean obtenerRegistroFoto(String numDoc, String numRespuesta) {

		SQLiteDatabase db = null;
		boolean hayFoto = false;

		try {
			File dbFile = new File(Util.DirApp(), "DataBase.db");
			if (dbFile.exists()) {
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

				String query = "SELECT * FROM EsFotosEncuestas WHERE numDoc = '"+numDoc+"' AND numRespuesta = '"+numRespuesta+"' ";
				
				Log.i(TAG, "obtenerRegistroFoto->"+query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.getCount()>0) {
					hayFoto = true;
				}
				if (cursor != null)
					cursor.close();
			}
		} catch (Exception e) {
			Log.e(TAG, "obtenerRegistroFoto->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
		}
		return hayFoto;
	}
	
	
	
	
	public static boolean actualizarFechaSincronizacion() {

		SQLiteDatabase db = null;
		SQLiteDatabase dbTemp = null;
		boolean estado = false;

		try {

			File dbFile = new File(Util.DirApp(), "DataBase.db");
			File dbFileTemp = new File(Util.DirApp(), "Temp.db");

			if (dbFile.exists() && dbFileTemp.exists()) {
				
				db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				dbTemp = SQLiteDatabase.openDatabase(dbFileTemp.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
				
				Vector<String> tableNames = new Vector<String>();
				String query = "SELECT tbl_name FROM sqlite_master";
				Cursor cursor = dbTemp.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					do {
						String tableName = cursor.getString(cursor.getColumnIndex("tbl_name"));
						if (tableName.equals("android_metadata")){
							continue;
						}
						tableNames.addElement(tableName);
					} while (cursor.moveToNext());
				}
				if (cursor != null)
					cursor.close();

				
				String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ssSSS");
				ContentValues values = new ContentValues(); 
				values.put("fechaSinc", fechaActual);
				db.update("EsRespuestas", values, null, null);
				dbTemp.update("EsRespuestas", values, null, null);
				
				values = new ContentValues(); 
				values.put("fechaLlegada", fechaActual);
				db.update("EsFotosEncuestas", values, null, null);
				dbTemp.update("EsFotosEncuestas", values, null, null);
				
				
				/*for (String tableName : tableNames) {
					try {
						String fechaActual = Util.FechaActual("yyyy-MM-dd HH:mm:ssSSS");
						//query = "UPDATE "+tableName+" SET fechaSync = '"+fechaActual+"' ";
						ContentValues values = new ContentValues(); 
						values.put("fechaSync", fechaActual);
						db.update(tableName, values, null, null);
						dbTemp.update(tableName, values, null, null);
						Log.i(TAG, "actualizarFechaSincronizacion-> Si campo fechaSync  en -> "+tableName);
					} catch (Exception e) {
						Log.e(TAG, "actualizarFechaSincronizacion-> No campo fechaSync  en -> "+tableName+"\n"+e.getMessage());
					}
				}//end for*/
			}//end if (dbFile.exists() && dbFileTemp.exists())
		} catch (Exception e) {
			Log.e(TAG, "actualizarFechaSincronizacion->"+e.getMessage());
		} finally {
			if (db != null)
				db.close();
			
			if (dbTemp != null)
				dbTemp.close();
		}

		return estado;
	}
	
	
	
	
	// METODO CREA LA FOTO CONMARCA DE AGUA Y LA ALMACENA EN EL DISPOSITIVO
	public static Bitmap resizedImageWaterMark(String fotoRezise, Context context, String marcaAgua) {

		FileInputStream fd = null;
		FileOutputStream fOut = null;
		Bitmap bitmapOriginal = null;
		
		try {

			File fileImg = null;
			String urlPhoto = fotoRezise;
			String[] arrasplit = urlPhoto.split("/");

			if (arrasplit.length > 1) {
				fileImg = new File(urlPhoto);
			} else {
				fileImg = new File(Util.DirApp(), urlPhoto);
			}

			if (!fileImg.exists())
				return null;
			
			if (fileImg.exists()) {

				fd = new FileInputStream(fileImg.getPath());
				bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());
				fd.close();
				fOut = new FileOutputStream(fileImg);
				
				int width = bitmapOriginal.getWidth();
				int height = bitmapOriginal.getHeight();
				float scaleWidth = ((float) EncuestasBO.WIDHTFOTO) / width;
				float scaleHeight = ((float) EncuestasBO.HEIGHTFOTO) / height;

				System.out.println(" urlPhoto "+urlPhoto+
						" width "+width+" height "+height+" scaleWidth "+scaleWidth+" scaleHeight "+scaleHeight);
				 
				Matrix matrix = new Matrix();
				matrix.postScale(scaleWidth, scaleHeight);
				
				//Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, false);
				Bitmap bitmapResized = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
				
				Canvas canvas = new Canvas(bitmapResized);
				
				String marcaAguaTxt = marcaAgua + "\n" + Util.FechaActual("yyyy-MM-dd") + "   "+ Util.FechaActual("HH:mm:ss");
				Paint paint = new Paint();
				paint.setColor(Color.RED);
				paint.setTextSize(18);
				paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
				paint.setTextAlign(Paint.Align.LEFT);
				int yPos = canvas.getHeight();
				canvas.drawText(marcaAguaTxt, 0, yPos, paint);
				
				Bitmap marcaAguaImg = BitmapFactory.decodeResource(context.getResources(), R.drawable.watermark);
				Paint paintImg = new Paint();
				paintImg.setTextAlign(Paint.Align.LEFT);
				canvas.drawBitmap(marcaAguaImg, 0, 0, paintImg);
				
				//marcaAguaImg = BitmapFactory.decodeResource(context.getResources(), R.drawable.logosmall);
				//canvas.drawBitmap(marcaAguaImg, (canvas.getWidth() / 2) - (marcaAguaImg.getWidth() / 2), (canvas.getHeight() / 2) - (marcaAguaImg.getHeight() / 2), paintImg);
				
				canvas.drawBitmap(bitmapResized, 0, 0, paint);
				bitmapResized.compress(Bitmap.CompressFormat.JPEG, 50, fOut);
				fOut.flush();
				fOut.close();
				return bitmapResized;
			}
			return null;
		} catch (Exception e) {
			return null;
		} finally {
			if (fd != null) {
				try {
					fd.close();
				} catch (IOException e) {
				}
			}
			fd = null;
			bitmapOriginal = null;
			System.gc();
		}
	}
	
	
	
	
	
	public static Bitmap resizedImage(String fotoRezise, Context context, float widhtIn, float heightIn) {

		FileInputStream fd = null;
		Bitmap bitmapOriginal = null;
		
		try {

			File fileImg = null;
			String urlPhoto = fotoRezise;
			String[] arrasplit = urlPhoto.split("/");

			if (arrasplit.length > 1) {
				fileImg = new File(urlPhoto);
			} else {
				fileImg = new File(Util.DirApp(), urlPhoto);
			}

			if (!fileImg.exists())
				return null;
			
			if (fileImg.exists()) {

				fd = new FileInputStream(fileImg.getPath());
				bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());
				fd.close();
				
				int width = bitmapOriginal.getWidth();
				int height = bitmapOriginal.getHeight();
				float scaleWidth = ((float) widhtIn) / width;
				float scaleHeight = ((float) heightIn) / height;

				System.out.println(" urlPhoto "+urlPhoto+
						" width "+width+" height "+height+" scaleWidth "+scaleWidth+" scaleHeight "+scaleHeight);
				 
				Matrix matrix = new Matrix();
				matrix.postScale(scaleWidth, scaleHeight);
				
				//Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, false);
				Bitmap bitmapResized = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
		
				return bitmapResized;
			}
			return null;
		} catch (Exception e) {
			return null;
		} finally {
			if (fd != null) {
				try {
					fd.close();
				} catch (IOException e) {
				}
			}
			fd = null;
			bitmapOriginal = null;
			System.gc();
		}
	}
	
	
}