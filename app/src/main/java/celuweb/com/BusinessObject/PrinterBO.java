package celuweb.com.BusinessObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Vector;

import celuweb.com.DataObject.CabeceraTirilla;
import celuweb.com.DataObject.EstadisticaRecorrido;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.DataObject.Impresion;
import celuweb.com.DataObject.ImpresionDetalle;
import celuweb.com.DataObject.ImpresionEncabezado;
import celuweb.com.DataObject.ImpresionNotaCredito;
import celuweb.com.DataObject.ImpresionRecaudo;
import celuweb.com.DataObject.InformeInventario;
import celuweb.com.DataObject.InformeRecaudo;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.RegCanastilla;
import celuweb.com.DataObject.TotalInventarioDia;
import celuweb.com.DataObject.Usuario;
import celuweb.com.uyusa.Const;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.Util;

public class PrinterBO {

    private static StringBuffer strBuffer;

    private static int cantEnter;
    public static final String TAG = "BusinessObject.PrinterBO";

    static String N0 = "! U1 SETBOLD 0";
    static String N2 = "! U1 SETBOLD 2";

    static String SUB_TOTAL = "SubTotal";
    static int contadorSaltosDeLinea = 0;
    /**
     * Cantidad de Caracteres Minimo para la Columna SubTotal.
     **/
    static int MIN_SUB_TOTAL = SUB_TOTAL.length() + 4;

    /**
     * Cantidad de Caracteres Maximo para la Columna Cantidad.
     **/
    static int MAX_CANTIDAD = 8;


    /*Asignacion del formateador para la numeracion de las tirillas.
     * usando punto, como separador de decimales.*/
    private DecimalFormatSymbols simbolos;
    {
	simbolos = new DecimalFormatSymbols();
	simbolos.setDecimalSeparator('.');
    }	
    //	public DecimalFormat format;
    //	{
    //		format = new DecimalFormat("#0.00");
    //	}



    public static String saltoDeLinea(){

	char ret1 = 13;
	char ret2 = 10;
	String ret = String.valueOf(ret1) + String.valueOf(ret2);
	contadorSaltosDeLinea++;
	return ret;

    }




    private static String enter() {

	cantEnter++;
	contadorSaltosDeLinea++;
	return "\r\n";
    }



    public static String formatoDevolucion( String numeroDoc, String copiaPrint) {

	String strPrint; 
	boolean isPedido = false; 
	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	char GS = 29;
	char admiracion = 33;
	char altoLetraNormal = 0;
	String letraNormal = ""+GS + admiracion + altoLetraNormal;
	strPrint = "" + XON;				
	//	Impresion impresion = DataBaseBO.getImpresion();
	if(copiaPrint.equals(""))
	    copiaPrint ="Copia ";

	strPrint += "" + copiaPrint+ enter;
	strPrint += ""+"DEVOLUCION"+enter+enter+enter;

	Impresion impCliente = null;
	Usuario usuario = DataBaseBO.ObtenerUsuario();
	DecimalFormat format = new DecimalFormat("#0.00");


		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		
		if(!cabeceraTirilla.isEmpty()){
			
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
			
		}

	    strPrint += "" + "# DEVOLUCION: "  + " " + numeroDoc + enter;
	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint +=   "" + "COD. VENDEDOR: " + usuario.codigoVendedor+" - "+usuario.nombreVendedor+enter+enter+enter;



	    impCliente = DataBaseBO.getImpresionCliente( numeroDoc );

	    if( impCliente != null ){

		strPrint += letraNormal;
		strPrint += "CLIENTE: " + impCliente.nombre + enter;
		strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
		strPrint += "CODIGO: " + impCliente.codigo + enter;
		strPrint += "RUC: " + impCliente.ruc + enter;
		strPrint += "DIRECCION: " + impCliente.direccion + enter;
		//Falta a�adir el rutero*
		strPrint += Util.lpad("", 42, "-") + enter+enter;


	    }        

	    strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 20, " ")  + Util.rpad("", 5, " ") + enter; 
	    strPrint += Util.rpad("", 7, " ") + Util.rpad("CANTIDAD", 11, " ") + Util.rpad("PRECIO", 12, " ")  + Util.rpad("TOTAL", 5, " ") + enter;
	    strPrint += Util.lpad("", 42, "-") + enter;
	    Vector<ImpresionDetalle> vImpDet = DataBaseBO.getImpresionDetalle( numeroDoc, isPedido, false, false );

	    int totalCant = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;

	    ImpresionDetalle detalle;

	    if ( vImpDet.size() > 0 ) {

		int maxCant = 0;
		float maxSubTotal = 0;
		Enumeration< ImpresionDetalle > elements = vImpDet.elements();

		while (elements.hasMoreElements()) {

		    detalle = elements.nextElement();

		    maxSubTotal = detalle.precio*detalle.cantidad;

		    strPrint += Util.rpad( detalle.codigo, 7, " ") +Util.rpad(detalle.nombre, 33, " ") +enter; //+  Util.rpad(""  , 10, " ") +  Util.lpad( "", 5, " ") + enter;
		    strPrint += Util.rpad( "",9, " ") + Util.rpad("" + Util.round(""+ detalle.cantidad, 2 ), 9, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(detalle.precio)), 2)), 8, " ")  + Util.lpad(Util.SepararMiles(Util.round ( String.valueOf(format.format(maxSubTotal)), 2 ))  , 9, " ") + enter+enter;
		    totalCant += detalle.cantidad;
		    String strCant = "" + detalle.cantidad;
		    strPrint+= detalle.descMotivo+enter+enter;

		    if (strCant.length() > maxCant)
			maxCant = strCant.length();

		    float sub_total       = 0;
		    float valor_descuento = 0;
		    float valor_iva       = 0;    			    			

		    sub_total       = detalle.cantidad * detalle.precio;
		    valor_descuento = sub_total * detalle.descuento / 100;
		    valor_iva       = (sub_total - valor_descuento) * (detalle.iva / 100);

		    subTotalImp  += sub_total;
		    descuentoImp += valor_descuento;
		    ivaImp       += valor_iva;    			
		}
	    }

	    strPrint += Util.lpad("", 42, "-") + enter;

	    String strSubTotal  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( subTotalImp))), 2));
	    String strDescuento = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(descuentoImp))), 2));
	    String strIva       = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( ivaImp))), 2));
	    String strBaseGravable = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( ( subTotalImp - descuentoImp )))), 2));

	    float neto     = subTotalImp + ivaImp - descuentoImp;
	    String str_valor_neto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( neto))), 2));

	    System.out.println("subtotal: "+subTotalImp);
	    System.out.println("tOTAL: "+Util.SepararMiles( Util.round( "" + neto, 2)) );


	    strPrint += Util.rpad( "SUBTOTAL", 23, " ") + Util.lpad( strSubTotal , 12, " ")  + enter;
	    strPrint += Util.rpad( "IVA ", 23, " ") + Util.lpad( strIva , 12, " ")  + enter;
	    strPrint += Util.rpad( "DESCUENTO", 23, " ") + Util.lpad(strDescuento, 12, " ")  + enter;
	    strPrint += Util.rpad( "TOTAL", 23, " ") + Util.lpad( str_valor_neto, 12, " ")  + enter;
	    strPrint += Util.lpad("", 42, "-") + enter;
	    strPrint += Util.lpad("", 42, "-") + enter+enter+enter;  

	    vImpDet = null;



	    strPrint += "Aceptada Cliente." + enter;
	    strPrint += "Firma" + enter;
	    strPrint += "C.C." + enter;

	    for (int i = 0; i < 4; i++) {
		strPrint += enter;
	    }




	
	System.out.println(strPrint);
	return strPrint;

    }



    public static String formatoPrueba() {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);
	
	strPrint = "" + XON;
	strPrint += "" + enter;				
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;



	    strPrint += Util.CentrarLinea( "****IMPRESION DE PRUEBA****" , 42 ) + enter;
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		
		if(!cabeceraTirilla.isEmpty()){
			
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
			
		}


	    strPrint += Util.lpad("", 42, "-") + enter;
	
	System.out.println(strPrint);
	return strPrint;

    }








    public static String formatoFormasDPago( String numDoc) {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char ESC  = 27;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	char GS = 29;
	char admiracion = 33;
	char altoLetraNormal = 0;
	String letraNormal = ""+GS + admiracion + altoLetraNormal;
	strPrint = "" + XON;
	strPrint += "" + enter;				
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	Impresion impCliente = null;
	DecimalFormat format = new DecimalFormat("#0.00");

	Usuario usuario = DataBaseBO.ObtenerUsuario();

	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}


	    strPrint += "" + "RECIBO Nro. "  + " " + numDoc + enter;
	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("dd/MM/yyyy HH:mm:ss") + enter;
	    strPrint += "" + "Codigo Vendedor: " + usuario.codigoVendedor+enter+enter;



	    impCliente = DataBaseBO.getImpresionClienteRecaudo( numDoc );

	    if( impCliente != null ){

		//			strPrint += letraTamano1;
		strPrint += letraNormal;
		strPrint += "CLIENTE: " + impCliente.nombre + enter;
		strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
		strPrint += "CODIGO: " + impCliente.codigo + enter;
		strPrint += "RUC: " + impCliente.ruc +  enter;
		strPrint += "DIRECCION: " + impCliente.direccion + enter;
		//Falta a�adir el rutero*
		strPrint += Util.lpad("", 42, "-") + enter;

		strPrint += Util.CentrarLinea( "FORMAS DE PAGO" , 42 ) + enter;
		strPrint += Util.lpad("", 42, "-") + enter;


	    }        

	    Vector<FormaPago> vFormaPago = DataBaseBO.getImpresionRecaudo( numDoc );

	    double totalMonto = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;

	    FormaPago detalleFPago;

	    System.out.println("Tama�o de lista forma de pago: "+vFormaPago.size());
	    if ( vFormaPago.size() > 0 ) {

		int maxCant = 0;
		int maxSubTotal = 0;
		Enumeration< FormaPago > elements = vFormaPago.elements();

		while (elements.hasMoreElements()) {

		    detalleFPago = elements.nextElement();


		    strPrint += "FORMA DE PAGO: " + enter;
		    strPrint += Util.CentrarLinea( detalleFPago.descripcion+": "+Util.SepararMiles(Util.round(String.valueOf(format.format(detalleFPago.monto)), 2)) , 42 ) + enter;
		    strPrint += Util.lpad("", 42, "-") + enter;


		    totalMonto += detalleFPago.monto;  			
		}
	    }


	    strPrint += Util.rpad( "T. Recaudo:", 25, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format( totalMonto)) , 2)  ), 12, " ")  + enter;
	    strPrint += Util.rpad( "T. Formas:", 25, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format( totalMonto)), 2) ), 12, " ")  + enter;
	    strPrint += Util.lpad("", 42, "-") + enter+enter;
	    strPrint += "Recibo de caja sujeto a aprobacion del "+enter;
	    strPrint += "departamento de cartera" + enter+enter+enter;

	    strPrint += Util.rpad( "Aceptada Cliente.", 27, " ") + Util.lpad( "Aceptada Vend.", 15, " ")  + enter;
	    strPrint += Util.rpad("Firma", 18, " ")+Util.lpad("Firma", 15, " ") + enter;
	    strPrint += Util.rpad("C.C.", 17, " ")+Util.lpad("C.C.", 15, " ") + enter;

	    for (int i = 0; i < 4; i++) {
		strPrint += enter;
	    }




	
	System.out.println(strPrint);
	return strPrint;

    }


    public static String formatoFormasDPagoGeneral() {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	strPrint = "" + XON;
	strPrint += "" + enter;			
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	DecimalFormat format = new DecimalFormat("#0.00");

	Usuario usuario = DataBaseBO.ObtenerUsuario();

	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("dd/MM/yyyy HH:mm:ss") + enter;
	    strPrint += "" + "Codigo Vendedor: " + usuario.codigoVendedor+enter+enter;
	    strPrint += Util.lpad("", 42, "-") + enter;
	    strPrint += ""+ "Cobranza"+ enter;

	    Vector<FormaPago> vFormaPag = DataBaseBO.getAllFormaPago();

	    double totalMonto = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;
	    float efectivo= 0;
	    float cheques = 0;
	    float consignaciones = 0;

	    FormaPago detalleFPago;

	    System.out.println("Tamano de lista forma de pago: "+vFormaPag.size());
	    if ( vFormaPag.size() > 0 ) {

		Enumeration< FormaPago > elements = vFormaPag.elements();

		while (elements.hasMoreElements()) {

		    detalleFPago = elements.nextElement();

		    if(detalleFPago.codigo == 1){
			efectivo+= detalleFPago.monto;
			totalMonto += detalleFPago.monto;  	
		    }else if(detalleFPago.codigo == 3){
			consignaciones+= detalleFPago.monto;
			totalMonto += detalleFPago.monto;  
		    }else {
			cheques+= detalleFPago.monto;
			totalMonto += detalleFPago.monto;  
		    }


		}
	    }

	    String strEfectivo  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( efectivo))), 2));
	    String strCheques = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( cheques))), 2));
	    String strConsignaciones       = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( consignaciones))), 2));

	    strPrint += Util.rpad("", 5, " ")+Util.rpad( "Efectivo:", 25, " ") + Util.rpad(strEfectivo, 12, " ")  + enter;
	    strPrint += Util.rpad("", 5, " ")+Util.rpad( "Cheques del Dia:", 25, " ") + Util.rpad( strCheques, 12, " ")  + enter;
	    strPrint += Util.rpad("", 5, " ")+Util.rpad( "Depositos", 25, " ") + Util.rpad( strConsignaciones , 12, " ")  + enter;
	    strPrint += Util.rpad( "Total:", 30, " ") + Util.rpad( Util.SepararMiles( Util.round( String.valueOf(format.format( totalMonto)) , 2)  ), 12, " ")  + enter+enter;
	    strPrint += "Recaudos"+enter;



	    strPrint += Util.rpad("Cliente", 12, " ") + Util.rpad("Num Doc.", 22, " ")  + Util.rpad("Total", 8, " ") + enter;
	    strPrint += Util.lpad("", 42, "-") + enter;

	    Vector<InformeRecaudo> infRecaudo = DataBaseBO.ListaInformeDeRecaudo();
	    float totalCant = 0;


	    InformeRecaudo informe;

	    if ( infRecaudo.size() > 0 ) {

		int maxCant = 0;

		Enumeration< InformeRecaudo > elements = infRecaudo.elements();

		while (elements.hasMoreElements()) {

		    informe = elements.nextElement();

		    strPrint += Util.rpad( informe.codCliente, 12, " ") +Util.rpad(informe.nroDoc, 22, " ") +Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(informe.valor)), 2)), 8, "")+enter; 
		    totalCant += informe.valor;

		}
	    }

	    strPrint += Util.lpad("", 42, "-") + enter+enter;

	    strPrint += Util.rpad( "", 27, " ") +Util.rpad("Total", 7, " ") +Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(totalCant)), 2)), 8, "")+enter+enter;
	    //-------------
	    strPrint += Util.lpad("", 42, "-") + enter;
	    strPrint += "Recibo de caja sujeto a aprobacion del "+enter;
	    strPrint += "departamento de cartera" + enter+enter+enter;

	    strPrint += "Aceptada Cliente." + enter;
	    strPrint += "Firma" + enter;
	    strPrint += "C.C." + enter;

	    for (int i = 0; i < 4; i++) {
		strPrint += enter;
	    }




	
	System.out.println(strPrint);
	return strPrint;

    }



    public static String formatoVentaPedido( String numeroDoc, String copiaPrint) {

	String strPrint;
	boolean isPedido = true;

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);


	strPrint = "" + XON;		
	//	Impresion impresion = DataBaseBO.getImpresion();

	if(copiaPrint.equals(""))
	    copiaPrint ="Copia ";

	strPrint += "" + enter;
	strPrint += "" + copiaPrint + enter;
	strPrint += "" + "Pedido" + enter+enter+enter;

	Impresion impCliente = null;
	Usuario usuario = DataBaseBO.ObtenerUsuario();

	DecimalFormat format = new DecimalFormat("#0.00");

	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	    strPrint += "" + "# Pedido: "  + " " + numeroDoc + enter;
	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint += "" + "Cod. Vendedor: " + usuario.codigoVendedor+" - "+usuario.nombreVendedor+enter+enter+enter;
	    strPrint += "" + "Cond. de Pago:" + enter;


	    impCliente = DataBaseBO.getImpresionCliente( numeroDoc );

	    if( impCliente != null ){

		//			strPrint += letraTamano1;
		//			strPrint += letraNormal;
		strPrint += "CLIENTE: " + impCliente.nombre + enter;
		strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
		strPrint += "CODIGO: " + impCliente.codigo + enter;
		strPrint += "RUC: " + impCliente.ruc +  enter;
		strPrint += "DIRECCION: " + impCliente.direccion + enter;
		//Falta a�adir el rutero*
		strPrint += Util.lpad("", 42, "-") + enter+enter;


	    }        


	    strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 20, " ")  + Util.rpad("", 5, " ") + enter;  
	    strPrint += Util.rpad("", 7, " ") + Util.rpad("CANTIDAD", 11, " ") + Util.rpad("PRECIO",12, " ")  + Util.rpad("TOTAL", 7, " ") + enter;
	    strPrint += Util.lpad("", 42, "-") + enter;
	    Vector<ImpresionDetalle> vImpDet = DataBaseBO.getImpresionDetalle( numeroDoc, isPedido, false, false );

	    int totalCant = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;

	    ImpresionDetalle detalle;

	    if ( vImpDet.size() > 0 ) {

		int maxCant = 0;

		Enumeration< ImpresionDetalle > elements = vImpDet.elements();

		while (elements.hasMoreElements()) {

		    detalle = elements.nextElement();

		    float maxSubTotal = detalle.precio * detalle.cantidad;

		    strPrint += Util.rpad( detalle.codigo, 7, " ") +Util.rpad(detalle.nombre, 33, " ") +enter; //+  Util.rpad(""  , 10, " ") +  Util.lpad( "", 5, " ") + enter; 
		    strPrint += Util.rpad( "",9, " ") + Util.rpad(Util.SepararMiles("" + Util.round(""+ detalle.cantidad, 2 )), 9, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format (detalle.precio)), 2)), 8, " ")  + Util.lpad(Util.SepararMiles(Util.round ( String.valueOf(format.format (maxSubTotal)), 2 )) , 9, " ") + enter;

		    totalCant += detalle.cantidad;
		    String strCant = "" + detalle.cantidad;

		    //Falta mostrar el motivo de devolucion******

		    if (strCant.length() > maxCant)
			maxCant = strCant.length();

		    float sub_total       = 0;
		    float valor_descuento = 0;
		    float valor_iva       = 0;    			    			

		    sub_total       = detalle.cantidad * detalle.precio;
		    valor_descuento = sub_total * detalle.descuento / 100;
		    valor_iva       = (sub_total - valor_descuento) * (detalle.iva / 100);

		    subTotalImp  += sub_total;
		    descuentoImp += valor_descuento;
		    ivaImp       += valor_iva;    			
		}
	    }

	    strPrint += Util.lpad("", 42, "-") + enter;


	    float neto     = subTotalImp + ivaImp - descuentoImp;


	    strPrint += Util.rpad( "SUBTOTAL", 30, " ") + Util.lpad(  Util.round( String.valueOf(format.format (subTotalImp)) , 2) , 12, " ")  + enter;
	    strPrint += Util.rpad( "IVA ", 30, " ") + Util.lpad(  Util.round(String.valueOf(format.format ( ivaImp)), 2), 12, " ")  + enter;
	    strPrint += Util.rpad( "DESCUENTO", 30, " ") + Util.lpad(  Util.round( String.valueOf(format.format (descuentoImp)), 2), 12, " ")  + enter;
	    //	strPrint += letraAlta; 
	    strPrint += Util.rpad( "TOTAL", 30, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format ( neto)), 2) ), 12, " ")  + enter;
	    //	strPrint += letraNormal;
	    strPrint += Util.lpad("", 42, "-") + enter;
	    strPrint += Util.lpad("", 42, "-") + enter+enter+enter;  

	    vImpDet = null;



	    strPrint += "Aceptada Cliente." + enter;
	    strPrint += "Firma" + enter;
	    strPrint += "C.C." + enter;


	    for (int i = 0; i < 4; i++) {
		strPrint += enter;
	    }




	
	System.out.println(strPrint);
	return strPrint;

    }

    public static String formatoFormasDPagoGeneralItermec() {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	
	strPrint = "" + XON;
	strPrint += "" + enter;				
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;


	DecimalFormat format = new DecimalFormat("#0.00");

	Usuario usuario = DataBaseBO.ObtenerUsuario();

	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("dd/MM/yyyy HH:mm:ss") + enter;
	    strPrint += "" + "Codigo Vendedor: " + usuario.codigoVendedor+enter+enter;
	    strPrint += Util.lpad("", 38, "*") + enter;
	    strPrint += ""+ "Cobranza"+ enter;


	    double totalMonto = 0;


	    float efectivo= 0;
	    float cheques = 0;
	    float consignaciones = 0;
	    Vector<FormaPago> vFormaPag = DataBaseBO.getAllFormaPago();
	    FormaPago detalleFPago;

	    System.out.println("Tama�o de lista forma de pago: "+vFormaPag.size());
	    if ( vFormaPag.size() > 0 ) {

		Enumeration< FormaPago > elements = vFormaPag.elements();

		while (elements.hasMoreElements()) {

		    detalleFPago = elements.nextElement();

		    if(detalleFPago.codigo == 1){
			efectivo+= detalleFPago.monto;
			totalMonto += detalleFPago.monto;  	
		    }else if(detalleFPago.codigo == 3){
			consignaciones+= detalleFPago.monto;
			totalMonto += detalleFPago.monto;  
		    }else {
			cheques+= detalleFPago.monto;
			totalMonto += detalleFPago.monto;  
		    }


		}
	    }

	    String strEfectivo  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(efectivo))), 2));
	    String strCheques = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( cheques))), 2));
	    String strConsignaciones       = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( consignaciones))), 2));

	    strPrint += Util.rpad("", 5, " ")+Util.rpad( "Efectivo:", 21, " ") + Util.rpad( strEfectivo, 12, " ")  + enter;
	    strPrint += Util.rpad("", 5, " ")+Util.rpad( "Cheques del Dia:", 21, " ") + Util.rpad( strCheques, 12, " ")  + enter;
	    strPrint += Util.rpad("", 5, " ")+Util.rpad( "Depositos", 21, " ") + Util.rpad( strConsignaciones , 12, " ")  + enter;
	    strPrint += Util.rpad( "Total:", 26, " ") + Util.rpad( Util.SepararMiles( Util.round( String.valueOf(format.format( totalMonto)) , 2)  ), 12, " ")  + enter+enter;
	    strPrint += "Recaudos"+enter;



	    strPrint += Util.rpad("Cliente", 11, " ") + Util.rpad("Num Doc.", 20, " ")  + Util.rpad("Total", 7, " ") + enter;
	    strPrint += Util.lpad("", 38, "*") + enter;

	    Vector<InformeRecaudo> infRecaudo = DataBaseBO.ListaInformeDeRecaudo();
	    float totalCant = 0;


	    InformeRecaudo informe;

	    if ( infRecaudo.size() > 0 ) {

		int maxCant = 0;

		Enumeration< InformeRecaudo > elements = infRecaudo.elements();

		while (elements.hasMoreElements()) {

		    informe = elements.nextElement();

		    strPrint += Util.rpad( informe.codCliente, 11, " ") +Util.rpad(informe.nroDoc, 20, " ") +Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(informe.valor)), 2)), 7, "")+enter; 
		    totalCant += informe.valor;

		}
	    }

	    strPrint += Util.lpad("", 38, "*") + enter+enter;

	    strPrint += Util.rpad( "", 24, " ") +Util.rpad("Total", 6, " ") +Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(totalCant)), 2)), 8, "")+enter+enter;
	    //-------------
	    strPrint += Util.lpad("", 38, "*") + enter;
	    strPrint += "Recibo de caja sujeto a aprobacion "+enter;
	    strPrint += "del departamento de cartera" + enter+enter+enter;

	    strPrint += "Aceptada Cliente." + enter;
	    strPrint += "Firma" + enter;
	    strPrint += "C.C." + enter;

	    for (int i = 0; i < 4; i++) {
		strPrint += enter;
	    }

	
	System.out.println(strPrint);
	return strPrint;

    }


    public static String formatoDevolucionItermec( String numeroDoc, String copiaPrint, boolean isAnulado, boolean isPedido) {

	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	strPrint = "" + XON;				
	//	Impresion impresion = DataBaseBO.getImpresion();

	if(copiaPrint.equals(""))
	    copiaPrint ="Copia ";

	strPrint += "" + copiaPrint+ enter;
	if(isAnulado)
	    strPrint += ""+"DEVOLUCION ANULADA"+enter+enter+enter;
	else
	    strPrint += ""+"DEVOLUCION"+enter+enter+enter;

	Impresion impCliente = null;

	DecimalFormat format = new DecimalFormat("#0.00");

	Usuario usuario = DataBaseBO.ObtenerUsuario();
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	    strPrint += "" + "# DEVOLUCION: "  + " " + numeroDoc + enter;
	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint +=   "" + "COD. VENDEDOR: " + usuario.codigoVendedor+" - "+usuario.nombreVendedor+enter+enter+enter;



	    impCliente = DataBaseBO.getImpresionCliente( numeroDoc );

	    if( impCliente != null ){

		System.out.println("Tama�o del nombre: "+ impCliente.nombre.length());


		strPrint += "CLIENTE: " + impCliente.nombre + enter;
		if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {

		    strPrint += "CODIGO: " + impCliente.codigo + enter;
		    strPrint += "CONTACTO: " + impCliente.razonSocial + enter;

		} else {
		    strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
		    strPrint += "CODIGO: " + impCliente.codigo + enter;
		}

		strPrint += "RUC: " +  enter;
		strPrint += "DIRECCION: " + impCliente.direccion + enter;

		//Falta a�adir el rutero*
		strPrint += Util.lpad("", 38, "*") + enter+enter;

	    }        

	    strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ") +  enter; 
	    strPrint += Util.rpad("", 3, " ") + Util.rpad("CANTIDAD", 12, " ") + Util.rpad("PRECIO", 11, " ")  + Util.rpad("TOTAL", 7, " ") + enter;
	    strPrint += Util.lpad("", 38, "*") + enter;
	    Vector<ImpresionDetalle> vImpDet = DataBaseBO.getImpresionDetalle( numeroDoc, isPedido, false, false);

	    int totalCant = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;

	    ImpresionDetalle detalle;

	    if ( vImpDet.size() > 0 ) {

		int maxCant = 0;
		float maxSubTotal = 0;
		Enumeration< ImpresionDetalle > elements = vImpDet.elements();

		while (elements.hasMoreElements()) {

		    detalle = elements.nextElement();
		    maxSubTotal = detalle.precio*detalle.cantidad;

		    strPrint += Util.rpad( detalle.codigo, 7, " ") + Util.rpad(detalle.nombre, 30, " ")+ enter; 
		    strPrint += Util.rpad( "", 6, " ") + Util.rpad("" + detalle.cantidad , 10, " ") + Util.rpad( Util.SepararMiles(Util.round ( String.valueOf(format.format(detalle.precio)), 2 )) ,10, " ") + Util.rpad( Util.SepararMiles(Util.round( String.valueOf(format.format(maxSubTotal)), 2)), 12, " ")+enter+enter;

		    totalCant += detalle.cantidad;
		    String strCant = "" + detalle.cantidad;
		    strPrint+= detalle.descMotivo+enter+enter;

		    if (strCant.length() > maxCant)
			maxCant = strCant.length();

		    float sub_total       = 0;
		    float valor_descuento = 0;
		    float valor_iva       = 0;    			    			

		    sub_total       = detalle.cantidad * detalle.precio;
		    valor_descuento = sub_total * detalle.descuento / 100;
		    valor_iva       = (sub_total - valor_descuento) * (detalle.iva / 100);

		    subTotalImp  += sub_total;
		    descuentoImp += valor_descuento;
		    ivaImp       += valor_iva;    			
		}
	    }

	    strPrint += Util.lpad("", 38, "*") + enter;

	    String strSubTotal  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(subTotalImp))), 2));
	    String strDescuento = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( descuentoImp))), 2));
	    String strIva       = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(ivaImp))), 2));



	    double neto =  subTotalImp + ivaImp - descuentoImp;
	    String str_valor_neto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( neto))), 2));


	    strPrint += Util.rpad( "SUBTOTAL", 23, " ") + Util.lpad(strSubTotal , 12, " ")  + enter;
	    strPrint += Util.rpad( "IVA ", 23, " ") + Util.lpad( strIva, 12, " ")  + enter;
	    strPrint += Util.rpad( "DESCUENTO", 23, " ") + Util.lpad( strDescuento, 12, " ")  + enter;
	    strPrint += Util.rpad( "TOTAL", 23, " ") + Util.lpad( str_valor_neto, 12, " ")  + enter;
	    //			strPrint += Util.lpad("", 38, "*") + enter;
	    strPrint += Util.lpad("", 38, "*") + enter+enter+enter;  

	    vImpDet = null;



	    strPrint += "Aceptada Cliente." + enter;
	    strPrint += "Firma" + enter;
	    strPrint += "C.C." + enter;

	    for (int i = 0; i < 6; i++) {
		strPrint += enter;
	    }




	
	System.out.println(strPrint);
	return strPrint;

    }

    public static String formatoFormasDPagoItermec( String numDoc) {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);
	strPrint = "" + XON;
	strPrint += "" + enter;		
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	Impresion impCliente = null;
	Usuario usuario = DataBaseBO.ObtenerUsuario();

	DecimalFormat format = new DecimalFormat("#0.00");
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}


	    strPrint += "" + "RECIBO Nro. "  + " " + numDoc + enter;
	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint +=   "" + "COD. VENDEDOR: " + usuario.codigoVendedor+enter+enter+enter;



	    impCliente = DataBaseBO.getImpresionClienteRecaudo( numDoc );

	    if( impCliente != null ){

		strPrint += "CLIENTE: " + impCliente.nombre + enter;
		strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
		strPrint += "CODIGO: " + impCliente.codigo + enter;
		strPrint += "RUC: " + impCliente.ruc +  enter;
		strPrint += "DIRECCION: " + impCliente.direccion + enter;
		strPrint += Util.lpad("", 38, "*") + enter+enter;
		
			if (usuario.impresionRecaudo.equals("SI")) {

				strPrint += Util.CentrarLinea("DETALLE RECAUDO", 38) + enter;
				strPrint += Util.lpad("", 38, "*") + enter;

				Vector<FormaPago> listaDetalleFP = DataBaseBO.getDetalleRecaudo(numDoc);
				float totalMonto = 0;

				FormaPago detalleFPago;

				System.out.println("Tamano de lista forma de pago: " + listaDetalleFP.size());
				if (listaDetalleFP.size() > 0) {

					Enumeration<FormaPago> elements = listaDetalleFP.elements();
					strPrint += Util.rpad("FACTURA", 24, " ") + Util.lpad("VALOR", 14, " ") + enter;

					while (elements.hasMoreElements()) {

						detalleFPago = elements.nextElement();
						strPrint += Util.rpad(detalleFPago.factura, 24, " ") + Util.lpad(Util.SepararMiles(Util.round(String.valueOf(format.format(detalleFPago.monto)), 2)), 14," ") + enter;


						totalMonto += detalleFPago.monto;
					}
					strPrint+=enter;
				}

				strPrint += Util.rpad("TOTAL RECAUDO", 24, " ") + Util.lpad(Util.SepararMiles(Util.round(String.valueOf(format.format(totalMonto)), 2)), 14," ") + enter;
				strPrint += Util.lpad("", 38, "*") + enter+enter;
			}

		strPrint += Util.CentrarLinea( "FORMAS DE PAGO" , 38 ) + enter;
		strPrint += Util.lpad("", 38, "*") + enter;


	    }        

	    Vector<FormaPago> vFormaPago = DataBaseBO.getImpresionRecaudo( numDoc );

	    float totalMonto = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;

	    FormaPago detalleFPago;

	    if ( vFormaPago.size() > 0 ) {

		int maxCant = 0;
		int maxSubTotal = 0;
		Enumeration< FormaPago > elements = vFormaPago.elements();

		while (elements.hasMoreElements()) {

		    detalleFPago = elements.nextElement();

		    System.out.println("FLOAT SALDOOO--->>> "+ detalleFPago.monto);
		    strPrint += "FORMA DE PAGO: " + enter;
		    strPrint += Util.CentrarLinea( detalleFPago.descripcion+": "+ Util.SepararMiles(Util.round(String.valueOf(format.format(detalleFPago.monto)), 2)), 38 ) + enter+enter;


		    totalMonto += detalleFPago.monto;  			
		}
	    }



	    strPrint += Util.rpad( "T. Recaudo:", 26, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format( totalMonto)) , 2)  ), 12, " ")  + enter;
	    strPrint += Util.rpad( "T. Formas:", 26, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format( totalMonto)), 2) ), 12, " ")  + enter+enter;
	    strPrint += Util.lpad("", 38, "*") + enter+enter;
	    strPrint += "Recibo de caja sujeto a aprobacion del departamento de cartera" + enter+enter+enter;


	    strPrint += Util.rpad( "Aceptada Cliente.", 24, " ") + Util.lpad( "Aceptada Vend.", 14, " ")  + enter;
	    strPrint += Util.rpad("Firma", 16, " ")+Util.lpad("Firma", 13, " ") + enter;
	    strPrint += Util.rpad("C.C.", 15, " ")+Util.lpad("C.C.", 13, " ") + enter;
	    //
	    //	strPrint += "Aceptada Cliente." + enter;
	    //	strPrint += "Firma" + enter;
	    //	strPrint += "C.C." + enter;

	    for (int i = 0; i < 6; i++) {
		strPrint += enter;
	    }


	
	System.out.println(strPrint);
	return strPrint;

    }


    public static String generarTirillaInventarioDeProducto(final String nroDoc, final Usuario liquidador,  int opcionLiquidar){

	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char ESC  = 27;
	char SP = 82;
	char a = 64;
	char neg = 7; // 7 = spain
	char nor = 0x25; // normal 42 CPL

	String initialize = String.valueOf(ESC) + String.valueOf(a);
	String normal   = String.valueOf(ESC) + String.valueOf('w') + String.valueOf(nor);
	String spain = String.valueOf(ESC) + String.valueOf(SP) + String.valueOf(neg);
	String enter = String.valueOf(ret1) + String.valueOf(ret2);


	Usuario user = DataBaseBO.CargarUsuario();

	strPrint = initialize + enter;	
	strPrint += spain + enter + normal + enter;


	Usuario usuario = DataBaseBO.ObtenerUsuario();
	
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	strPrint += "# INVENTARIO DE PRODUCTO: " + nroDoc+ enter;
	strPrint += "FECHA:" + Util.FechaActual("yyyy-MM-dd HH:mm:ss")+enter;
	strPrint += "LIQUIDADOR: "  + enter;
	strPrint += liquidador.codigoVendedor + " - " + liquidador.nombreVendedor+ enter;

	strPrint += "CLIENTE:"+ user.nombreVendedor+ enter;
	strPrint += "NEGOCIO:"+ user.nombreVendedor+ enter;
	strPrint += "CODIGO:"+ user.nombreVendedor+ enter;
	strPrint += "RUC:"+ user.nombreVendedor+ enter;
	strPrint += "DIRECCION:"+enter;

	strPrint += Util.line("*-", 42) + enter;
	strPrint +=  Util.rpad("COD", 12, " ") + Util.rpad("PRODUCTO", 30, " ") + enter;
	strPrint += Util.rpad("TEORICO", 12, " ") + Util.rpad("FISICO", 11, " ")  + Util.rpad("DIF", 7, " ") + enter;
	strPrint += Util.line("*-", 42) + enter;

	Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleLiquidacionInventarioProducto(user.codigoVendedor);

	if ( listaDetalles.size() > 0 ) {

	    /* Dar formato a cada detalle del pedido. */
	    for (ImpresionDetalle detalle : listaDetalles) {

		strPrint += Util.rpad(detalle.codigo, 7, " ") + Util.rpad(detalle.nombre, 30, " ") + enter;
		strPrint += Util.rpad("", 6, " ") + Util.rpad("" + (detalle.cantidadInventario - detalle.cantidadTransaccion), 10, " ")+ Util.rpad("" + detalle.cantidadDigitada, 10, " ")+ Util.rpad("" + detalle.diferencia, 12, " ") + enter+enter;

	    }
	}


	strPrint += Util.line("*-", 42) + enter;
	for (int i = 0; i < 2; i++) {
	    strPrint += enter;
	}
	return strPrint;

    }

    public static String generarEncabezadoTirillaProductos( String numeroDoc,  String copiaPrint, int opcionLiquidar, boolean isFaltante) {

	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	
	strPrint = "" + XON;
	LinkedHashMap<String, Float> detalleIva = new LinkedHashMap<String, Float>();				
	//	Impresion impresion = DataBaseBO.getImpresion();

	if(copiaPrint.equals(""))
	    copiaPrint ="Copia ";

	Impresion impCliente = null;
	impCliente = DataBaseBO.getImpresionClienteLiquidador( numeroDoc );
	String factura = DataBaseBO.obtenerFacturaNroDoc(numeroDoc);
	strPrint+= ""+enter+enter;
	strPrint += "" + copiaPrint+ enter;

	DecimalFormat format = new DecimalFormat("#0.00");

	Usuario usuario = DataBaseBO.ObtenerUsuario();
	
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}


	    if (opcionLiquidar == Const.PRODUCTO_DANADO){

		if(isFaltante)
		    strPrint += "" + "Numero Factura: " + factura + enter;
		else
		    strPrint += "" + "Numero Pedido: " +numeroDoc + enter;

	    } else if(opcionLiquidar == Const.IS_INVENTARIO_LIQUIDACION){
		if(isFaltante)
		    strPrint += "" + "Numero Factura: " + factura + enter;
		else
		    strPrint += "" + "Numero : " +numeroDoc + enter;
	    }

	    else {
		strPrint += "" + "Numero Pedido: " +numeroDoc + enter;
	    }

	    strPrint += "" + "FECHA:         " +Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint += "" + "VENDEDOR:      " + usuario.codigoVendedor+" - "+usuario.nombreVendedor+enter+enter;

	    strPrint += "" + "Cond. de Pago:      " +enter;



	    impCliente = DataBaseBO.getImpresionCliente( numeroDoc );

	    if( impCliente != null ){

		System.out.println("Tamano del nombre: "+ impCliente.nombre.length());


		strPrint += "CLIENTE: " + impCliente.nombre + enter;
		if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {

		    strPrint += "CODIGO: " + impCliente.codigo + enter;
		    strPrint += "CONTACTO: " + impCliente.razonSocial + enter;

		} else {
		    strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
		    strPrint += "CODIGO: " + impCliente.codigo + enter;
		}

		strPrint += "RUC: " + impCliente.ruc +  enter;
		strPrint += "DIRECCION: " + impCliente.direccion + enter;

		//Falta a�adir el rutero*
		strPrint += Util.lpad("", 38, "*") + enter+enter;

	    }        

	    strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ") +  enter; 
	    strPrint += Util.rpad("", 3, " ") + Util.rpad("CANTIDAD", 12, " ") + Util.rpad("PRECIO", 11, " ")  + Util.rpad("TOTAL", 7, " ") + enter;
	    strPrint += Util.lpad("", 38, "*") + enter;

	    Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleLiquidacionProducto(numeroDoc);

	    int totalCant = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;

	    ImpresionDetalle detalle;

	    if ( listaDetalles.size() > 0 ) {

		int maxCant = 0;
		float maxSubTotal = 0;
		Enumeration< ImpresionDetalle > elements = listaDetalles.elements();

		while (elements.hasMoreElements()) {

		    detalle = elements.nextElement();
		    maxSubTotal = detalle.precio*detalle.cantidad;

		    strPrint += Util.rpad( detalle.codigo, 7, " ") + Util.rpad(detalle.nombre, 30, " ")+ enter; 
		    strPrint += Util.rpad( "", 6, " ") + Util.rpad("" + detalle.cantidad , 10, " ") + Util.rpad( Util.SepararMiles(Util.round ( String.valueOf(format.format(detalle.precio)), 2 )) ,10, " ") + Util.rpad( Util.SepararMiles(Util.round( String.valueOf(format.format(maxSubTotal)), 2)), 12, " ")+enter+enter;

		    totalCant += detalle.cantidad;
		    String strCant = "" + detalle.cantidad;
		    //					strPrint+= detalle.descMotivo+enter+enter;

		    if (strCant.length() > maxCant)
			maxCant = strCant.length();

		    float sub_total       = 0;
		    float valor_descuento = 0;
		    float valor_iva       = 0;    			    			

		    sub_total       = detalle.cantidad * detalle.precio;
		    valor_descuento = sub_total * detalle.descuento / 100;
		    valor_iva       = (sub_total - valor_descuento) * (detalle.iva / 100);

		    subTotalImp  += sub_total;
		    descuentoImp += valor_descuento;
		    ivaImp       += valor_iva;    			
		}
	    }

	    strPrint += Util.lpad("", 38, "*") + enter;

	    String strSubTotal  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(subTotalImp))), 2));
	    String strDescuento = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( descuentoImp))), 2));
	    String strIva       = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(ivaImp))), 2));



	    double neto =  subTotalImp + ivaImp - descuentoImp;
	    String str_valor_neto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( neto))), 2));


	    strPrint += Util.rpad( "SUBTOTAL", 23, " ") + Util.lpad(strSubTotal , 12, " ")  + enter;
	    strPrint += Util.rpad( "IVA ", 23, " ") + Util.lpad( strIva, 12, " ")  + enter;
	    strPrint += Util.rpad( "N.CREDITO:", 23, " ") + Util.lpad( "0", 12, " ")   + enter;
	    strPrint += Util.rpad( "DESCUENTO", 23, " ") + Util.lpad( strDescuento, 12, " ")  + enter;
	    strPrint += Util.rpad( "TOTAL", 23, " ") + Util.lpad( str_valor_neto, 12, " ")  + enter;
	    //			strPrint += Util.lpad("", 38, "*") + enter;
	    strPrint += Util.lpad("", 38, "*") + enter+enter+enter;  

	    listaDetalles = null;



	    strPrint += "Aceptada Cliente." + enter;
	    strPrint += "Firma" + enter;
	    strPrint += "C.C." + enter;

	    for (int i = 0; i < 6; i++) {
		strPrint += enter;
	    }




	
	System.out.println(strPrint);
	return strPrint;

    }

    public static String formatoProductoLiquidacion( String numeroDoc, Usuario liquidador , int opcionLiquidar) {

	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);


	strPrint = "" + XON;				
	//	Impresion impresion = DataBaseBO.getImpresion();

	Usuario usuario = DataBaseBO.ObtenerUsuario();
	String titulo = "";

	if(opcionLiquidar == Const.PRODUCTO_DANADO)
	    titulo = "# PRODUCTO DANADO: ";
	else
	    titulo = "# REGISTRO INVENTARIO: ";

	strPrint+= titulo+" - "+numeroDoc + enter + enter + enter;

	if( usuario != null ){

	    strPrint += "FECHA: " +  Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint += "LIQUIDADOR: "+enter;
	    strPrint += liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +enter;
	    strPrint += "CLIENTE: " + usuario.nombreVendedor + enter;
	    strPrint += "NEGOCIO: " + usuario.nombreVendedor + enter;
	    strPrint += "CODIGO: " + usuario.codigoVendedor + enter;
	    strPrint += "RUC: " +  usuario.codigoVendedor +enter;
	    strPrint += "DIRECCION: "  + enter+enter;
	    strPrint += Util.lpad("", 38, "*") + enter;

	}    

	strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ") + enter;
	strPrint += Util.rpad("", 3, " ") + Util.rpad("TEORICO", 12, " ") + Util.rpad("FISICO", 11, " ")+ Util.rpad("DIF", 7, " ") + enter;
	strPrint += Util.lpad("", 38, "*") + enter;

	boolean isDanado = false;
	if(opcionLiquidar == Const.PRODUCTO_DANADO){
	    isDanado = true;
	}

	Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleLiquidacion(usuario.codigoVendedor, isDanado);


	if ( listaDetalles.size() > 0 ) {

	    /* Dar formato a cada detalle del pedido. */
	    for (ImpresionDetalle detalle : listaDetalles) {

		strPrint += Util.rpad(detalle.codigo, 7, " ") + Util.rpad(detalle.nombre, 30, " ") + enter;
		strPrint += Util.rpad("", 6, " ") + Util.rpad("" + detalle.cantidadInventario, 10, " ")+ Util.rpad("" + detalle.cantidadDigitada, 10, " ")+ Util.rpad("" + detalle.diferencia, 12, " ") + enter+enter;

	    }
	}


	strPrint += Util.lpad("", 38, "*") + enter+enter;
	listaDetalles = null;



	strPrint += "Aceptada Cliente." + enter;
	strPrint += "Firma" + enter;
	strPrint += "C.C." + enter;

	for (int i = 0; i < 6; i++) {
	    strPrint += enter;
	}





	System.out.println(strPrint);
	return strPrint;

    }


    public static String formatoImpresionCanastillas( String numDoc,  int opcionLiquidar, Usuario liquidador) {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	strPrint = "" + XON;
	strPrint += "" + enter;
	LinkedHashMap<String, Float> detalleIva = new LinkedHashMap<String, Float>();				
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	Usuario usuario = DataBaseBO.ObtenerUsuario();

	DecimalFormat format = new DecimalFormat("#0.00");

	String subtitulo = cargarTipoLiquidacion(opcionLiquidar) ;

	strPrint+= "#"+subtitulo+" - "+numDoc + enter + enter + enter;

	if( usuario != null ){

	    strPrint += "FECHA: " +  Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint += "LIQUIDADOR: "+enter;
	    strPrint += liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +enter;
	    strPrint += "CLIENTE: " + usuario.nombreVendedor + enter;
	    strPrint += "NEGOCIO: " + usuario.nombreVendedor + enter;
	    strPrint += "CODIGO: " + usuario.codigoVendedor + enter;
	    strPrint += "RUC: " +  usuario.codigoVendedor +enter;
	    strPrint += "DIRECCION: "  + enter;
	    strPrint += Util.lpad("", 38, "*") + enter;

	}        

	if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {


	    if (opcionLiquidar == Const.INVENTARIO_CANASTA) {

		strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ") +  enter; 
		strPrint += Util.rpad("", 3, " ") + Util.rpad("TEORICO", 12, " ") + Util.rpad("FISICO", 11, " ")  + Util.rpad("DIF", 7, " ") + enter;
		strPrint += Util.lpad("", 38, "*") + enter;



	    } else {

		strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ") +  enter; 
		strPrint += Util.rpad("", 3, " ") + Util.rpad("CANT. ENT", 12, " ")  + enter;
		strPrint += Util.lpad("", 38, "*") + enter;


	    }



	    if (opcionLiquidar == Const.INVENTARIO_CANASTA){

		Vector<ImpresionDetalle> vCanastilla;
		vCanastilla = DataBaseBO.getImpresionCanastillasInv(numDoc, true);
		ImpresionDetalle detalle;

		if (vCanastilla.size() > 0) {
		    Enumeration<ImpresionDetalle> elements = vCanastilla.elements();

		    while (elements.hasMoreElements()) {

			detalle = elements.nextElement();

			strPrint += Util.rpad( detalle.codigo, 7, " ") + Util.rpad(detalle.nombre, 30, " ")+ enter; 
			strPrint += Util.rpad( "", 6, " ") + Util.rpad("" + detalle.cantidadInventario , 10, " ") + Util.rpad( ""+detalle.cantidadDigitada ,10, " ") + Util.rpad( ""+detalle.diferencia, 12, " ")+enter;

		    }
		}

	    }else{

		Vector<RegCanastilla> vCanastilla;
		vCanastilla = DataBaseBO.getImpresionCanastillas(numDoc, true);
		RegCanastilla canastilla;

		if (vCanastilla.size() > 0) {
		    Enumeration<RegCanastilla> elements = vCanastilla.elements();

		    while (elements.hasMoreElements()) {

			canastilla = elements.nextElement();

			strPrint += Util.rpad( canastilla.codigo, 7, " ") + Util.rpad(canastilla.descripcion, 30, " ")+ enter; 
			strPrint += Util.rpad( "", 6, " ") + Util.rpad("" + canastilla.nroDevuelto , 10, " ") +enter;


		    }
		}
	    }




	    strPrint += Util.lpad("", 38, "*") + enter+enter+enter;

	    strPrint += Util.rpad( "Aceptada Cliente.", 24, " ")   + enter;
	    strPrint += Util.rpad("Firma", 16, " ")+ enter;
	    strPrint += Util.rpad("C.C.", 15, " ") + enter;

	}




	for (int i = 0; i < 6; i++) {
	    strPrint += enter;
	}



	System.out.println(strPrint);
	return strPrint;

    }

    public static String generarTirillaAveriaLiquidacion( String numDoc,  Usuario liquidador, int opcionLiquidar, boolean isAveria) {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);
	


	strPrint = "" + XON;
	strPrint += "" + enter;			
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	Usuario usuario = DataBaseBO.ObtenerUsuario();

	DecimalFormat format = new DecimalFormat("#0.00");

	if(isAveria)
	    strPrint+= "# Averias Transporte: "+numDoc+enter+enter;
	else
	    strPrint+= "# Compensaciones: "+numDoc+enter+enter;

	if( usuario != null ){

	    strPrint += "FECHA: " +  Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint += "LIQUIDADOR: "+enter;
	    strPrint += liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +enter;
	    strPrint += "CLIENTE: " + usuario.nombreVendedor + enter;
	    strPrint += "NEGOCIO: " + usuario.nombreVendedor + enter;
	    strPrint += "CODIGO: " + usuario.codigoVendedor + enter;
	    strPrint += "RUC: " +  usuario.codigoVendedor +enter;
	    strPrint += "DIRECCION: "  + enter;
	    strPrint += Util.lpad("", 38, "*") + enter;

	}        


	strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ") +  enter; 
	strPrint += Util.rpad("", 3, " ") + Util.rpad("CANTIDAD", 12, " ")  + enter;
	strPrint += Util.lpad("", 38, "*") + enter;



	boolean isDanado = false;
	if(opcionLiquidar == Const.PRODUCTO_DANADO){
	    isDanado = true;
	}	

	Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleAveria(numDoc, usuario.codigoVendedor, isDanado);
	ImpresionDetalle detalle;

	if (listaDetalles.size() > 0) {
	    Enumeration<ImpresionDetalle> elements = listaDetalles.elements();

	    while (elements.hasMoreElements()) {

		detalle = elements.nextElement();

		strPrint += Util.rpad( detalle.codigo, 7, " ") + Util.rpad(detalle.nombre, 30, " ")+ enter; 
		strPrint += Util.rpad( "", 6, " ") + Util.rpad("" + detalle.cantidad , 10, " ") +enter+enter;


	    }
	}





	strPrint += Util.lpad("", 38, "*") + enter+enter+enter;

	strPrint += Util.rpad( "Aceptada Cliente.", 24, " ")   + enter;
	strPrint += Util.rpad("Firma", 16, " ")+ enter;
	strPrint += Util.rpad("C.C.", 15, " ") + enter;



	for (int i = 0; i < 6; i++) {
	    strPrint += enter;
	}



	System.out.println(strPrint);
	return strPrint;

    }

    /**
     * Devuelve el tipo de Liquidacion
     */
    public static String cargarTipoLiquidacion(int tipoLiquidacion) {

	String tipo = "";

	switch (tipoLiquidacion) {
	case Const.CANASTILLA_VACIA:
	    tipo = "CANASTAS VACIAS";
	    break;

	case Const.INVENTARIO_CANASTA:
	    tipo = "INVENTARIO CANASTAS";
	    break;

	case Const.IS_DESCARGA_CANASTA:
	    tipo = "DESCARGA CANASTAS";
	    break;

	default:
	    tipo = "SIN TIPO DE LIQUIDACION";
	    break;
	}

	return tipo;
    }


    public static String formatoVentaPedidoItermerc( String numeroDoc, String copiaPrint, boolean isAnulado, boolean isPedido) {

	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);
	
	strPrint = "" + XON;			
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	String factura = DataBaseBO.obtenerFacturaNroDoc(numeroDoc);

	if(copiaPrint.equals(""))
	    copiaPrint ="Copia ";

	strPrint += "" + copiaPrint + enter;
	
	Usuario usuario = DataBaseBO.ObtenerUsuario();

	if(isAnulado)
	    strPrint += "" + "Pedido Anulado" + enter+enter+enter;
	else
	    strPrint += usuario.tipoVenta.equals(Const.AUTOVENTA) ? "" +"Factura" + enter+enter+enter  :"" + "Pedido" + enter+enter+enter;

	Impresion impCliente = null;

	DecimalFormat format = new DecimalFormat("#0.00");
		
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		
		if(!cabeceraTirilla.isEmpty()){
			
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
			 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
			
		}
		

	    if (usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido) {
		strPrint += "" + "# Factura: "  + " " + factura + enter;
	    }else{
		strPrint += "" + "# PEDIDO: "  + " " + numeroDoc + enter;
	    }

	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint +=   "" + "COD. VENDEDOR: " + usuario.codigoVendedor+" - "+usuario.nombreVendedor+enter+enter+enter;



	    impCliente = DataBaseBO.getImpresionCliente( numeroDoc );

	    if( impCliente != null ){

		strPrint += "CLIENTE: " + impCliente.nombre + enter;

		if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {

		    strPrint += "CODIGO: " + impCliente.codigo + enter;
		    strPrint += "CONTACTO: " + impCliente.razonSocial + enter;

		} else {
		    strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
		    strPrint += "CODIGO: " + impCliente.codigo + enter;
		}


		strPrint += "RUC: " + impCliente.ruc +  enter;
		strPrint += "DIRECCION: " + impCliente.direccion + enter;
		//Falta a�adir el rutero*
		strPrint += Util.lpad(" ", 38, "*") + enter+enter;


	    }       

	    strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ") + enter;   
	    strPrint += Util.rpad("", 3, " ") + Util.rpad("CANTIDAD",12, " ") + Util.rpad("PRECIO", 11, " ")  + Util.rpad("TOTAL", 7, " ") + enter;
	    strPrint += Util.lpad(" ", 38, "*") + enter;
	    Vector<ImpresionDetalle> vImpDet = DataBaseBO.getImpresionDetalle( numeroDoc, isPedido, false, false );

	    int totalCant = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;
	    double valorDevolucion = 0;
	    boolean existeAmarre = false;

	    ImpresionDetalle detalle;

	    if ( vImpDet.size() > 0 ) {

		int maxCant = 0;
		int maxSubTotal = 0;
		Enumeration< ImpresionDetalle > elements = vImpDet.elements();

		while (elements.hasMoreElements()) {

		    detalle = elements.nextElement();


		    strPrint += Util.rpad( detalle.codigo, 7, " ") + Util.rpad(detalle.nombre, 30, " ")+ enter;  
		    strPrint += Util.rpad( "", 6, " ") + Util.rpad("" + detalle.cantidad , 10, " ") + Util.rpad( Util.SepararMiles(Util.round (String.valueOf(format.format(detalle.precio)),2 )) ,10, " ") + Util.rpad(Util.SepararMiles(Util.round ( String.valueOf(format.format(detalle.precio*detalle.cantidad)), 2 )) ,12, " ") + enter+enter;
		    totalCant += detalle.cantidad;
		    String strCant = "" + detalle.cantidad;

		    if (strCant.length() > maxCant)
			maxCant = strCant.length();

		    float sub_total       = 0;
		    float valor_descuento = 0;
		    float valor_iva       = 0;    			    			

		    sub_total       = detalle.cantidad * detalle.precio;
		    valor_descuento = sub_total * detalle.descuento / 100;
		    valor_iva       = (sub_total - valor_descuento) * (detalle.iva / 100);

		    subTotalImp  += sub_total;
		    descuentoImp += valor_descuento;
		    ivaImp       += valor_iva;    			
		}
	    }
	    strPrint += Util.lpad(" ", 38, "*") + enter+enter+enter;


	    //float neto     = subTotalImp + ivaImp - descuentoImp;CC

	    if(usuario.tipoVenta.equals(Const.AUTOVENTA)){
		existeAmarre = DataBaseBO.existeAmarreConDev(numeroDoc, impCliente.codigo, valorDevolucion);
		valorDevolucion = DataBaseBO.obtenerValorDevAmarre(numeroDoc, impCliente.codigo);
		System.out.println("ValorDevolucion por fuera del DB: "+ valorDevolucion);
	    }

	    double neto = usuario.tipoVenta.equals(Const.AUTOVENTA) ? (subTotalImp + ivaImp - descuentoImp) - valorDevolucion : subTotalImp + ivaImp - descuentoImp;

	    if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
		strPrint += Util.lpad(" ", 38, "*") + enter;
		strPrint += Util.rpad("TOTAL A PAGAR:", 23, " ")+ Util.lpad(Util.SepararMiles(Util.round(String.valueOf(format.format(neto)), 2)), 12, " ")+ enter ;
		strPrint += Util.lpad(" ", 38, "*") + enter+ enter + enter;
	    }

	    strPrint += Util.rpad( "SUBTOTAL", 23, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format (subTotalImp)) , 2)  ), 12, " ")  + enter;
	    strPrint += Util.rpad( "IVA ", 23, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format (ivaImp)), 2 ) ), 12, " ")  + enter;
	    strPrint += Util.rpad( "DESCUENTO", 23, " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format (descuentoImp)), 2) ), 12, " ")  + enter;

	    if(usuario.tipoVenta.equals(Const.AUTOVENTA)){
		strPrint += Util.rpad( "Notas Credito", 23 , " ") + Util.lpad( Util.SepararMiles( Util.round( String.valueOf(format.format (valorDevolucion)), 2) ), 12, " ")  + enter;
	    }else{
		strPrint += Util.rpad("TOTAL:", 23, " ")+ Util.lpad(Util.SepararMiles(Util.round(String.valueOf(format.format(neto)), 2)), 12, " ")+ enter ;
	    }

	    strPrint += Util.lpad(" ", 38, "*") + enter+enter+enter;  

	    if(usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido ){

		strPrint += " "+ "C A N A S T I L L A S"+ enter+enter;
		strPrint += Util.rpad("Codigo", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("Descripcion", 21, " ") + enter;   
		strPrint += Util.rpad("", 7, " ") + Util.rpad("Cant. Ent",18, " ") + Util.rpad("Cant. Dev.", 13, " ") + enter;
		strPrint += Util.lpad(" ", 38, "*") + enter;

		Vector<RegCanastilla> vCanastilla = DataBaseBO.getImpresionCanastillas( numeroDoc, false );

		RegCanastilla canastilla = new RegCanastilla();

		if ( vCanastilla.size() > 0 ) {
		    Enumeration< RegCanastilla > elements = vCanastilla.elements();

		    while (elements.hasMoreElements()) {

			canastilla = elements.nextElement();

			//						int canastillaEntre = !canastilla.entregada.equals("") ? Integer.parseInt(canastilla.entregada) : 0;
			//						int canastillaDev = !canastilla.devuelta.equals("") ? Integer.parseInt(canastilla.devuelta) : 0;

			strPrint += Util.rpad( canastilla.codigo, 15, " ") + Util.rpad(canastilla.descripcion, 23, " ")+ enter;  
			strPrint += Util.rpad( "", 12, " ") + Util.rpad(""+canastilla.nroEntrega ,17, " ") + Util.rpad( ""+canastilla.nroDevuelto, 9, " ")  + enter;

		    }
		}
		strPrint += Util.lpad(" ", 38, "*") + enter+enter+enter;
	    }



	    vImpDet = null;



	    strPrint += "Aceptada Cliente." + enter;
	    strPrint += "Firma" + enter;
	    strPrint += "C.C." + enter;

	    for (int i = 0; i < 6; i++) {
		strPrint += enter;
	    }




	
	System.out.println(strPrint);
	return strPrint;

    }

    public static String formatoInventarioNuevoIntermec() {
	
	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char ESC  = 27;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);
	strPrint = "";	
	strPrint += "" + enter;
	strPrint += "Inventario" + enter;
	strPrint += "Vendedor: "+Main.usuario.codigoVendedor + enter;
	strPrint += "Fecha: " + Util.FechaActual("yyyy-MM-dd") + enter;

	strPrint += Util.lpad("", 38, "*") + enter;

	strPrint +=  Util.rpad("COD", 7, " ")+Util.rpad("PRODUCTO", 17, " ")+Util.lpad("ACTUAL", 7, " ")   + Util.lpad("VENTAS", 7, " ")  + enter;

	strPrint += Util.lpad("", 38, "*") + enter;

	Vector<InformeInventario> lInformeInv = DataBaseBO.CargarInformeInventario(false);
	InformeInventario infoInv;

	for( int i = 0; i < lInformeInv.size(); i++ ){

	    infoInv = lInformeInv.elementAt( i );

	    strPrint += Util.rpad( infoInv.codigo, 7, " ") + Util.rpad( "" + infoInv.nombre, 17, " ") + Util.lpad("" + Util.round(infoInv.invInicial+"",0), 6, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),0), 6, " ")  + enter;
	}

	for (int i = 0; i < 4; i++) {
	    strPrint += enter;
	}

	return strPrint;
    }

    ///42

    public static String formatoVentaPedidoSewoo( String numeroDoc, String copiaPrint) {

	String strPrint = "";
	boolean isPedido = true;

	char ret1 = 13;
	char ret2 = 10;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);
	contadorSaltosDeLinea = 0;
	strPrint = "ML 25" + saltoDeLinea() + "TEXT 0 2 10 20" + saltoDeLinea();
	strPrint += "" + saltoDeLinea();
	strPrint += "" + copiaPrint + saltoDeLinea();
	strPrint += "" + "Pedido" + saltoDeLinea()+saltoDeLinea()+saltoDeLinea();

	Impresion impCliente = null;
	Usuario usuario = DataBaseBO.ObtenerUsuario();
	
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	    strPrint += "" + "# Pedido: "  + " " + numeroDoc + saltoDeLinea();
	    strPrint += "" + "FECHA: "  + " " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + saltoDeLinea();
	    strPrint += "" + "Cod. Vendedor: " + usuario.codigoVendedor+" - "+usuario.nombreVendedor+saltoDeLinea()+saltoDeLinea()+saltoDeLinea();
	    strPrint += "" + "Cond. de Pago:" + saltoDeLinea();


	    impCliente = DataBaseBO.getImpresionCliente( numeroDoc );

	    if( impCliente != null ){

		strPrint += "CLIENTE: " + impCliente.nombre + saltoDeLinea();
		strPrint += "NEGOCIO: " + impCliente.razonSocial + saltoDeLinea();
		strPrint += "CODIGO: " + impCliente.codigo + saltoDeLinea();
		strPrint += "RUC: " + impCliente.ruc +  saltoDeLinea();
		strPrint += "DIRECCION: " + impCliente.direccion + saltoDeLinea();
		strPrint += Util.lpad("", 42, "-") + saltoDeLinea()+saltoDeLinea();


	    }        


	    strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 20, " ")  + Util.rpad("", 5, " ") + saltoDeLinea();  
	    strPrint += Util.rpad("", 7, " ") + Util.rpad("CANTIDAD", 11, " ") + Util.rpad("PRECIO",12, " ")  + Util.rpad("TOTAL", 7, " ") + saltoDeLinea();
	    strPrint += Util.lpad("", 42, "-") + saltoDeLinea();
	    Vector<ImpresionDetalle> vImpDet = DataBaseBO.getImpresionDetalle( numeroDoc, isPedido, false, false );

	    int totalCant = 0;

	    float subTotalImp  = 0;
	    float descuentoImp = 0;
	    float ivaImp       = 0;

	    ImpresionDetalle detalle;

	    if ( vImpDet.size() > 0 ) {

		int maxCant = 0;

		Enumeration< ImpresionDetalle > elements = vImpDet.elements();

		while (elements.hasMoreElements()) {

		    detalle = elements.nextElement();

		    float maxSubTotal = detalle.precio * detalle.cantidad;

		    strPrint += Util.rpad( detalle.codigo, 7, " ") +Util.rpad(detalle.nombre, 33, " ") +saltoDeLinea(); //+  Util.rpad(""  , 10, " ") +  Util.lpad( "", 5, " ") + saltoDeLinea(); 
		    strPrint += Util.rpad( "",9, " ") + Util.rpad(Util.SepararMiles("" + Util.round(""+ detalle.cantidad, 2 )), 9, " ") + Util.rpad(Util.SepararMiles(Util.round(""+detalle.precio, 2)), 8, " ")  + Util.lpad(Util.SepararMiles(Util.round ( "" +maxSubTotal, 2 )) , 9, " ") + enter+enter;

		    totalCant += detalle.cantidad;
		    String strCant = "" + detalle.cantidad;

		    //Falta mostrar el motivo de devolucion******

		    if (strCant.length() > maxCant)
			maxCant = strCant.length();

		    float sub_total       = 0;
		    float valor_descuento = 0;
		    float valor_iva       = 0;    			    			

		    sub_total       = detalle.cantidad * detalle.precio;
		    valor_descuento = sub_total * detalle.descuento / 100;
		    valor_iva       = (sub_total - valor_descuento) * (detalle.iva / 100);

		    subTotalImp  += sub_total;
		    descuentoImp += valor_descuento;
		    ivaImp       += valor_iva;    			
		}
	    }

	    strPrint += Util.lpad("", 42, "-") + saltoDeLinea();

	    String strSubTotal  = Util.SepararMilesSin(Util.round(Util.QuitarE("" + subTotalImp), 0));
	    String strDescuento = Util.SepararMilesSin(Util.round(Util.QuitarE("" + descuentoImp), 0));
	    String strIva       = Util.SepararMilesSin(Util.round(Util.QuitarE("" + ivaImp), 0));
	    String strBaseGravable = Util.SepararMilesSin(Util.round(Util.QuitarE("" + ( subTotalImp - descuentoImp )), 0));

	    float neto     = subTotalImp + ivaImp - descuentoImp;
	    String str_valor_neto = Util.SepararMilesSin(Util.round(Util.QuitarE("" + neto), 0));


	    strPrint += Util.rpad( "SUBTOTAL", 30, " ") + Util.lpad(  Util.round( "" + subTotalImp , 2) , 12, " ")  + saltoDeLinea();
	    strPrint += Util.rpad( "IVA ", 30, " ") + Util.lpad(  Util.round("" + ivaImp, 2), 12, " ")  + saltoDeLinea();
	    strPrint += Util.rpad( "DESCUENTO", 30, " ") + Util.lpad(  Util.round( "" + descuentoImp, 2), 12, " ")  + saltoDeLinea();
	    //	strPrint += letraAlta; 
	    strPrint += Util.rpad( "TOTAL", 30, " ") + Util.lpad( Util.SepararMiles( Util.round( "" + neto, 2) ), 12, " ")  + saltoDeLinea();
	    //	strPrint += letraNormal;
	    strPrint += Util.lpad("", 42, "-") + saltoDeLinea();
	    strPrint += Util.lpad("", 42, "-") + saltoDeLinea()+saltoDeLinea()+saltoDeLinea();  

	    vImpDet = null;



	    strPrint += "Aceptada Cliente." + saltoDeLinea();
	    strPrint += "Firma" + saltoDeLinea();
	    strPrint += "C.C." + saltoDeLinea();


	    for (int i = 0; i < 4; i++) {
		strPrint += saltoDeLinea();
	    }



	

	strPrint = "! 0 200 200 " + ((contadorSaltosDeLinea + 5) * 24) + " " + "1" + "\r\n"  +strPrint+"ENDML"+ saltoDeLinea()+"PRINT" + saltoDeLinea();
	//	strPrint += "ENDML"+ enter()+"PRINT" + enter();
	System.out.println(strPrint);
	return strPrint;

    }




    public static String formatoinventarioDiaIntermec(boolean isLiquidacion){

	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char ESC  = 27;
	char SP = 82;
	char a = 64;
	char neg = 7; // 7 = spain
	char nor = 0x25; // normal 42 CPL

	String initialize = String.valueOf(ESC) + String.valueOf(a);
	String normal   = String.valueOf(ESC) + String.valueOf('w') + String.valueOf(nor);
	String spain = String.valueOf(ESC) + String.valueOf(SP) + String.valueOf(neg);
	String enter = String.valueOf(ret1) + String.valueOf(ret2);


	DecimalFormat format = new DecimalFormat("#0.00");
	strPrint = initialize + enter;	
	strPrint += spain + enter + normal + enter;


	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	strPrint += "Inventario" + enter;
	strPrint += "Vendedor: " + Main.usuario.codigoVendedor + enter;
	strPrint += "Fecha: " + Util.FechaActual("yyyy-MM-dd") + enter;

	strPrint += Util.line("*-", 42) + enter;
	strPrint +=  Util.rpad("COD", 12, " ") + Util.rpad("PRODUCTO", 30, " ") + enter;

	if(isLiquidacion)
	    //strPrint +=  Util.rpad("", 12, " ") + Util.rpad("INICIAL", 11, " ")  + Util.rpad("VENTAS", 11, " ")+ Util.rpad("DIF", 10, " ") + enter;
	    strPrint +=  Util.rpad("II", 12, " ") + Util.rpad("V", 11, " ")  + Util.rpad("M", 11, " ")+ Util.rpad("IF", 10, " ") + enter;
	else
	    strPrint +=  Util.rpad("", 12, " ") + Util.rpad("INICIAL", 12, " ")  + Util.rpad("VENTAS", 18, " ") + enter;

	strPrint += Util.line("*-", 42) + enter;

	Vector<InformeInventario> lInformeInv = DataBaseBO.CargarInformeInventario(isLiquidacion);
	/*Agregar las canastas al informe de inventario*/
	DataBaseBO.aregarInformeCanastas(lInformeInv);
	InformeInventario infoInv;
	TotalInventarioDia totalInventarioDia = new TotalInventarioDia();
	DataBaseBO.calcularTotalesInventarioDia(totalInventarioDia);

	for( int i = 0; i < lInformeInv.size(); i++ ){
	    infoInv = lInformeInv.elementAt( i );
	    strPrint += Util.rpad( infoInv.codigo, 12, " ") + Util.rpad( "" + infoInv.nombre, 30, " ") + enter;
	    if(isLiquidacion){
		//strPrint += Util.lpad("" + Util.round(infoInv.invInicial+"",1), 14, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 8, " ")  +Util.lpad("" + Util.round(String.valueOf(infoInv.invInicial - infoInv.cantVentas),1), 14, " ") + enter;
		strPrint += Util.rpad("" + Util.round(infoInv.invInicial+"",1), 12, " ") + Util.rpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 11, " ")  +Util.rpad("" + Util.round(String.valueOf(infoInv.sobrante),1), 11, " ") +Util.rpad("" + Util.round(String.valueOf(infoInv.invActual),1), 10, " ") + enter;
	    }else
		strPrint += Util.lpad("" + Util.round(infoInv.invInicial+"",1), 16, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 12, " ")  + Util.rpad("", 14, " ") + enter;
	}
	strPrint += Util.line("*-", 42) + enter;
	/*Agrear lineas de totalizados*/
	strPrint +=  Util.rpad("Unidades ii: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalUnidadesII()), 20, " ") + enter;
	strPrint +=  Util.rpad("Valor cargue: ", 22, " ") + Util.rpad(Util.SepararMiles(String.valueOf(format.format( totalInventarioDia.getValorCargue()))), 20, " ") + enter;
	strPrint +=  Util.rpad("Total canastillas: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalCanastillas()), 20, " ") + enter;
	strPrint += Util.line("*-", 42) + enter;
	for (int i = 0; i < 2; i++) {
	    strPrint += enter;
	}
	return strPrint;

    }


    public static String formatoInventarioDiaSewoo(boolean isLiquidacion) {
	
	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char ESC  = 27;
	char a = 64;

	/*Character font B(9x17) - 42 caracteres por linea*/
	char neg = 9; //negrilla
	char nor = 3; // normal
	String initialize = String.valueOf(ESC) + String.valueOf(a);
	String normal   = String.valueOf(ESC) + String.valueOf('!') + String.valueOf(nor);
	String negrilla = String.valueOf(ESC) + String.valueOf('!') + String.valueOf(neg);
	String enter = String.valueOf(ret1) + String.valueOf(ret2);


	strPrint = initialize + enter;	
	strPrint += negrilla + enter;
	strPrint += enter + normal + enter;

	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}



	strPrint += "Inventario" + enter;
	strPrint += "Vendedor: " + Main.usuario.codigoVendedor + enter;
	strPrint += "Fecha: " + Util.FechaActual("yyyy-MM-dd") + enter;
	strPrint += normal;
	strPrint += Util.lpad("", 42, "-") + enter;

	strPrint +=  Util.rpad("COD", 12, " ") + Util.rpad("PRODUCTO", 30, " ") + enter;

	if(!isLiquidacion)
	    strPrint +=  Util.rpad("", 12, " ") + Util.rpad("INICIAL", 12, " ")  + Util.rpad("VENTAS", 18, " ") + enter;
	else
	    strPrint +=  Util.rpad("", 12, " ") + Util.rpad("INICIAL", 11, " ")  + Util.rpad("VENTAS", 11, " ")+ Util.rpad("DIF", 10, " ") + enter;

	strPrint += Util.lpad("", 42, "-") + enter;

	Vector<InformeInventario> lInformeInv = DataBaseBO.CargarInformeInventario(isLiquidacion);
	/*Agregar las canastas al informe de inventario*/
	DataBaseBO.aregarInformeCanastas(lInformeInv);
	InformeInventario infoInv;
	TotalInventarioDia totalInventarioDia = new TotalInventarioDia();
	DataBaseBO.calcularTotalesInventarioDia(totalInventarioDia);

	for( int i = 0; i < lInformeInv.size(); i++ ){

	    infoInv = lInformeInv.elementAt( i );
	    strPrint += Util.rpad( infoInv.codigo, 12, " ") + Util.rpad( "" + infoInv.nombre, 30, " ") + enter;

	    if(!isLiquidacion)
		strPrint += Util.lpad("" + Util.round(infoInv.invInicial+"",1), 16, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 12, " ")  + Util.rpad("", 14, " ") + enter;
	    else
		strPrint += Util.lpad("" + Util.round(infoInv.invInicial+"",1), 14, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 8, " ")  +Util.lpad("" + Util.round(String.valueOf(infoInv.invInicial - infoInv.cantVentas),1), 14, " ") + enter;

	}
	strPrint += Util.lpad("", 42, "-") + enter;
	strPrint += negrilla;
	/*Agrear lineas de totalizados*/
	strPrint +=  Util.rpad("Unidades ii: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalUnidadesII()), 20, " ") + enter;
	strPrint +=  Util.rpad("Valor cargue: ", 22, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(totalInventarioDia.getValorCargue()), 2)), 20, " ") + enter;
	strPrint +=  Util.rpad("Total canastillas: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalCanastillas()), 20, " ") + enter;
	strPrint += Util.lpad("", 42, "-") + enter;
	for (int i = 0; i < 2; i++) {
	    strPrint += enter;
	}
	return strPrint;
    }


    public static String formatoCargueSewoo(String numeroDoc) {
	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char ESC  = 27;
	char a = 64;

	/*Character font B(9x17) - 42 caracteres por linea*/
	char neg = 9; //negrilla
	char nor = 1; // normal
	String initialize = String.valueOf(ESC) + String.valueOf(a);
	String normal   = String.valueOf(ESC) + String.valueOf('!') + String.valueOf(nor);
	String negrilla = String.valueOf(ESC) + String.valueOf('!') + String.valueOf(neg);
	String enter = String.valueOf(ret1) + String.valueOf(ret2);


	strPrint = initialize;
	strPrint += initialize;
	strPrint += initialize + enter;
	strPrint += negrilla;
	strPrint += " " + enter;
	strPrint += " " + enter;
	strPrint += " " + enter;
	
	
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	strPrint += Util.lpad("", 42, "-") + enter;

	Impresion impCliente = DataBaseBO.getImpresionClienteSugerido( numeroDoc );

	strPrint += "CLIENTE: " + impCliente.nombre + enter;
	strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
	strPrint += "CODIGO: " + Main.usuario.codigoVendedor + enter;
	strPrint += "RUC: " + Main.usuario.codigoVendedor + enter;
	strPrint += "DIRECCION: " + "" + enter;
	strPrint += normal;
	strPrint += Util.lpad("", 42, "-") + enter;


	strPrint +=  Util.rpad("Codigo", 12, " ") + Util.rpad("Descripcion", 30, " ") + enter;
	strPrint +=  Util.rpad("CJ", 14, " ") + Util.rpad("PR", 14, " ") + Util.rpad("Total", 14, " ") + enter;


	strPrint += Util.lpad("", 42, "-") + enter;

	Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleSugerido( numeroDoc );

	float subTotal = 0;
	float total = 0;

	for( int i = 0; i < listaDetalles.size(); i++ ){


	    ImpresionDetalle infoDet = listaDetalles.elementAt( i );
	    subTotal = infoDet.precio*infoDet.cantidad;
	    total = total+subTotal;
	    strPrint += Util.rpad( infoDet.codigo, 12, " ") + Util.rpad( "" + infoDet.nombre, 29, " ") + enter;
	    strPrint += Util.rpad(infoDet.cantidad+"", 14, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(infoDet.precio),2)), 10, " ") + Util.rpad(Util.SepararMiles(Util.round(subTotal+"",2)), 12, " ") + enter;


	}
	strPrint += Util.lpad("", 42, "-") + enter;
	strPrint += negrilla;
	/*Agrear lineas de totalizados*/
	strPrint +=  Util.rpad("SUBTOTAL: ", 28, " ") + Util.rpad(Util.round(Util.SepararMiles(String.valueOf(total)),2), 10, " ") + enter;
	strPrint +=  Util.rpad("IVA: ", 28, " ") + Util.rpad(Util.SepararMiles(String.valueOf(0)), 10, " ") + enter;
	strPrint +=  Util.rpad("DESCUENTO: ", 28, " ") +  Util.rpad(Util.SepararMiles(String.valueOf(0)), 10, " ") + enter;
	strPrint +=  Util.rpad("TOTAL: ", 28, " ") + Util.rpad(Util.round(Util.SepararMiles(String.valueOf(total)),2), 10, " ") + enter;

	strPrint += Util.lpad("", 42, "-") + enter;
	for (int i = 0; i < 2; i++) {
	    strPrint += enter;
	}
	strPrint += initialize + " " + enter;
	return strPrint;
    }




    public static String formatoCargueImpresoraIntermec(String numeroDoc){
	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char ESC  = 27;
	char SP = 82;
	char a = 64;
	char neg = 7; // 7 = spain
	char nor = 0x25; // normal 42 CPL

	String initialize = String.valueOf(ESC) + String.valueOf(a);
	String normal   = String.valueOf(ESC) + String.valueOf('w') + String.valueOf(nor);
	String spain = String.valueOf(ESC) + String.valueOf(SP) + String.valueOf(neg);
	String enter = String.valueOf(ret1) + String.valueOf(ret2);


	DecimalFormat format = new DecimalFormat("#0.00");

	strPrint = initialize + enter;	
	strPrint += spain + enter + normal + enter;
	strPrint += " " + enter;
	strPrint += " " + enter;
	strPrint += " " + enter;
	strPrint += "SUGERIDO" + enter;
	
	
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}
	strPrint += Util.lpad("", 42, "-") + enter;

	Impresion impCliente = DataBaseBO.getImpresionClienteSugerido( numeroDoc );

	strPrint += "CLIENTE: " + impCliente.nombre + enter;
	strPrint += "NEGOCIO: " + impCliente.razonSocial + enter;
	strPrint += "CODIGO: " + Main.usuario.codigoVendedor + enter;
	strPrint += "RUC: " + Main.usuario.codigoVendedor + enter;
	strPrint += "DIRECCION: " + "" + enter;
	strPrint += normal;
	strPrint += Util.line("*-", 42) + enter;

	strPrint += normal;

	strPrint +=  Util.rpad("Codigo", 12, " ") + Util.rpad("Descripcion", 30, " ") + enter;
	//strPrint +=  Util.rpad("CJ", 14, " ") + Util.rpad("PR", 14, " ") + Util.rpad("Total", 14, " ") + enter;
	strPrint +=  Util.rpad("UN", 10, " ")+ Util.rpad("CJ", 10, " ")  + Util.rpad("PR", 10, " ") + Util.rpad("Total", 10, " ") + enter;

	strPrint += normal;

	strPrint += Util.line("*-", 42) + enter;

	Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleSugerido( numeroDoc );

	float subTotal = 0;
	float total = 0;

	for( int i = 0; i < listaDetalles.size(); i++ ){


	    ImpresionDetalle infoDet = listaDetalles.elementAt( i );
	    subTotal = infoDet.precio*infoDet.cantidad;
	    total = total+subTotal;
	    strPrint += Util.rpad( infoDet.codigo, 12, " ") + Util.rpad( "" + infoDet.nombre, 29, " ") + enter;
	    //strPrint += Util.rpad(infoDet.cantidad+"", 14, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(infoDet.precio)),2)), 10, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(subTotal)) ,2)), 12, " ") + enter;
	    strPrint += Util.rpad(infoDet.cantidad+"", 10, " ") +Util.rpad(infoDet.cantidadCajas+"", 10, " ") +  Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(infoDet.precio)),2)), 10, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(subTotal)) ,2)), 10, " ") + enter+enter;


	}
	strPrint += Util.lpad("", 42, "-") + enter;
	strPrint += normal;
	/*Agrear lineas de totalizados*/
	strPrint +=  Util.rpad("SUBTOTAL: ", 22, " ") + Util.rpad(Util.SepararMiles(String.valueOf(format.format(total))), 20, " ") + enter;
	strPrint +=  Util.rpad("IVA: ", 22, " ") + Util.rpad(Util.SepararMiles(String.valueOf(format.format(0))), 20, " ") + enter;
	strPrint +=  Util.rpad("DESCUENTO: ", 22, " ") +  Util.rpad(Util.SepararMiles(String.valueOf(format.format(0))), 20, " ") + enter;
	strPrint +=  Util.rpad("TOTAL: ", 22, " ") + Util.rpad(Util.SepararMiles(String.valueOf(format.format(total))), 20, " ") + enter;

	strPrint += Util.line("*-", 42) + enter;
	for (int i = 0; i < 2; i++) {
	    strPrint += enter;
	}
	strPrint += initialize + " " + enter;
	return strPrint;
    }


    public static String formatoInformeZetaItermerc(boolean isInformeZeta) {

	String strPrint; 
	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);

	strPrint = "" + XON;			
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	DecimalFormat format = new DecimalFormat("#0.00");

	Usuario usuario = DataBaseBO.ObtenerUsuario();

	

	strPrint += !isInformeZeta ? "" + "Liquidacion Diaria"+ enter+enter : "" ;
	
	ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
	cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
	
	if(!cabeceraTirilla.isEmpty()){
		
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).empresa , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).nit , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).direccion , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).telefono , 38 ) + enter;
		 strPrint += Util.CentrarLinea( cabeceraTirilla.get(0).texto , 38 ) + enter+enter+enter;
		
	}

	strPrint += "" + "Serial: "  + "123" +enter;
	//			strPrint += Util.rpad("Hora: " + Util.FechaActual("HH:mm:ss") , 13, " ") + enter;  
	strPrint+= "Fecha: "+Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;  
	strPrint += isInformeZeta ? "" + "REPORTE Z #"  + "1007"+ enter : "" ;
	strPrint += "" + "Vend: "+ usuario.codigoVendedor+ " " +usuario.nombreVendedor + enter +  enter + enter ;
	strPrint += "RESUMEN DE VENTAS"	+ enter;

	EstadisticaRecorrido estadisticaRecorrido = DataBaseBO.getInformeResumenVenta();

	int totalVisitas = DataBaseBO.obtenerTotalDeVisitas();

	estadisticaRecorrido.str_venta_contado = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.venta_contado))), 2));
	String devolucion_contado = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.dev_contado))), 2));
	String venta_credito = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.venta_credito))), 2));
	String devolucion_credito = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.dev_credito))), 2));
	String totalGlobal = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.totalGlobal))), 2));

	strPrint += Util.rpad("  Ventas Contado", 20, " ") + Util.rpad(""+estadisticaRecorrido.str_venta_contado, 18, " ") +enter;
	strPrint += Util.rpad("  Devs Contado", 20, " ") + Util.rpad(""+devolucion_contado, 18, " ") +enter;
	strPrint += Util.rpad("  Ventas Credito", 20, " ") + Util.rpad(""+venta_credito, 18, " ") +enter; 
	strPrint += Util.rpad("  Devs Credito", 20, " ") + Util.rpad(""+devolucion_credito, 18, " ") +enter; 

	strPrint += Util.rpad("TOTAL VENTAS", 20, " ") + Util.rpad(""+totalGlobal, 18, " ") +enter+enter;

	strPrint += "LIQUIDACION DE VALORES"+ enter+enter;

	Vector<FormaPago> listaFormaPago = DataBaseBO.getImpresionVentaContado();

	strPrint += "VENTAS CONTADO"+ enter;
	float totalVenta = 0;

	FormaPago efectivo = new FormaPago();
	efectivo.descripcion="Efectivo";
	efectivo.monto = estadisticaRecorrido.venta_contado  - estadisticaRecorrido.dev_contado;
	String strEfectivo = 	Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(efectivo.monto))), 2));
	strPrint += Util.rpad(efectivo.descripcion, 20, " ") + Util.rpad(""+strEfectivo, 18, " ") +enter;

	for (FormaPago formaPago : listaFormaPago) {

	    String Str_monto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(formaPago.monto))), 2));
	    strPrint += Util.rpad(formaPago.descripcion, 20, " ") + Util.rpad(""+Str_monto, 18, " ") +enter;
	    totalVenta += formaPago.monto;
	}

	totalVenta += efectivo.monto;
	String Str_Total = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalVenta))), 2));
	strPrint += Util.rpad("TOTAL", 20, " ") + Util.rpad(""+Str_Total, 18, " ") +enter+enter+enter;

	Vector<FormaPago> listaFormaPagoCredito = DataBaseBO.getImpresionVentaCredito();

	strPrint += "COBRANZA"+ enter;
	float totalRecaudo = 0;

	for (FormaPago formaPago : listaFormaPagoCredito) {

	    String Str_monto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(formaPago.monto))), 2));
	    strPrint += Util.rpad(formaPago.descripcion, 20, " ") + Util.rpad(""+Str_monto, 18, " ") +enter;
	    totalRecaudo += formaPago.monto;
	}
	String str_Total_Credito = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalRecaudo))), 2));
	strPrint += Util.rpad("TOTAL", 20, " ") + Util.rpad(""+str_Total_Credito, 18, " ") +enter+enter+enter;

	Vector<FormaPago> listaInformeLiquidacion = DataBaseBO.getImpresionLiquidacion();

	strPrint += "TOTAL DE VALORES A LIQUIDAR"+enter;
	float totalALiquidar= 0;

	float totalCobranza = DataBaseBO.getTotalConbranza();

	FormaPago fp = new FormaPago();
	fp.descripcion="Efectivo";
	fp.monto = (estadisticaRecorrido.venta_contado  - estadisticaRecorrido.dev_contado) + totalCobranza;
	String strMonto = 	Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(fp.monto))), 2));
	strPrint += Util.rpad(fp.descripcion, 20, " ") + Util.rpad(""+strMonto, 18, " ") +enter;

	for (FormaPago formaPago : listaInformeLiquidacion) {

	    String Str_monto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(formaPago.monto))), 2));
	    strPrint += Util.rpad(formaPago.descripcion, 20, " ") + Util.rpad(""+Str_monto, 18, " ") +enter;
	    totalALiquidar += formaPago.monto;
	}
	totalALiquidar+= fp.monto;
	String strTotalALiquidar= Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalALiquidar))), 2));

	strPrint += "TOTAL"+enter;
	strPrint += Util.rpad("A ENTREGAR", 20, " ") + Util.rpad(""+strTotalALiquidar, 18, " ") +enter+enter;

	strPrint +=  Util.rpad("#DOC", 13, " ") +  Util.rpad("GRAV.", 13, " ")+ Util.rpad("NO.GR",12, " ") + enter;
	strPrint +=  Util.rpad("", 13, " ") +  Util.rpad("TP ITEM", 13, " ")+ Util.rpad("ITEM", 12, " ") + enter+enter;	

	strPrint += Util.lpad("", 38, "*") + enter;

	strPrint += "FACTURAS CONTADO"+enter;

	Vector<ImpresionEncabezado> listaEncabezadoFacturaCon = DataBaseBO.getImpresionEncabezadoContado();
	double totalValorDescuento  = 0;
	double totalvalorNeto  = 0;

	if(listaEncabezadoFacturaCon!=null){

	    for (ImpresionEncabezado factura : listaEncabezadoFacturaCon) {

		if(factura.anulado == 0 ){

		    String montoDescuento  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_descuento))), 2));
		    String montoNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_neto))), 2));

		    strPrint +=  Util.rpad(factura.numero_doc, 13, " ") +  Util.rpad("CO "+montoDescuento, 13, " ")+ Util.rpad(""+montoNeto, 12, " ") + enter;

		    totalValorDescuento+= factura.valor_descuento;
		    totalvalorNeto += factura.valor_neto;
		}

	    }

	    strPrint += Util.lpad("", 38, "*") + enter;

	}

	String strTotalDescuento  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalValorDescuento))), 2));
	String strTotalNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
	strPrint +=  Util.rpad("", 13, " ") +  Util.rpad("TOTAL "+strTotalDescuento, 13, " ")+ Util.rpad(""+strTotalNeto, 12, " ") + enter+enter;	

	strPrint += "FACTURAS CREDITO"+enter;

	Vector<ImpresionEncabezado> listaEncabezadoFacturaCre = DataBaseBO.getImpresionEncabezadoCredito();
	totalValorDescuento  = 0;
	totalvalorNeto  = 0;

	if(listaEncabezadoFacturaCre!=null){

	    for (ImpresionEncabezado factura : listaEncabezadoFacturaCre) {

		if(factura.anulado == 0 ){

		    String montoDescuento  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_descuento))), 2));
		    String montoNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_neto))), 2));

		    strPrint +=  Util.rpad(factura.numero_doc, 13, " ") +  Util.rpad("CO "+montoDescuento, 13, " ")+ Util.rpad(""+montoNeto, 12, " ") + enter;

		    totalValorDescuento+= factura.valor_descuento;
		    totalvalorNeto += factura.valor_neto;
		}

	    }

	    strPrint += Util.lpad("", 38, "*") + enter;

	}

	strTotalDescuento  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalValorDescuento))), 2));
	strTotalNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
	strPrint +=  Util.rpad("", 13, " ") +  Util.rpad("TOTAL "+strTotalDescuento, 13, " ")+ Util.rpad(""+strTotalNeto, 12, " ") + enter+enter;	

	
	
	strPrint += "FACTURAS ANULADAS"+enter;

	Vector<ImpresionEncabezado> listaEncabezadoFacturaAnuladas = DataBaseBO.getImpresionEncabezadoAnulado();
	totalValorDescuento = 0;
	totalvalorNeto = 0;

	for (ImpresionEncabezado factura : listaEncabezadoFacturaAnuladas) {

	    if(factura.anulado == 1){

		String montoDescuento  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_descuento))), 2));
		String montoNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_neto))), 2));

		strPrint +=  Util.rpad(factura.numero_doc, 13, " ") +  Util.rpad(""+montoDescuento, 13, " ")+ Util.rpad(""+montoNeto, 12, " ") + enter;
		strPrint +=  Util.rpad("", 13, " ") +  Util.rpad("F "+montoDescuento, 13, " ")+ Util.rpad(""+montoNeto, 12, " ") + enter;

		totalValorDescuento+= factura.valor_descuento;
		totalvalorNeto += factura.valor_neto;
	    }

	}

	String strTotalDescuentoAnulado  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format( totalValorDescuento))), 2));
	String strTotalNetoAnulado = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
	strPrint +=  Util.rpad("", 13, " ") +  Util.rpad("TOTAL "+strTotalDescuentoAnulado, 13, " ")+ Util.rpad(""+strTotalNetoAnulado, 12, " ") + enter+enter;	


	strPrint += "RECAUDOS"+enter;

	strPrint +=  Util.rpad("Cliente", 15, " ") +  Util.rpad("Num Doc.", 17, " ")+ Util.rpad("Total", 8, " ") + enter;

	totalValorDescuento = 0;
	totalvalorNeto = 0;

	Vector<ImpresionRecaudo> listaRecaudo = DataBaseBO.getInformeRecaudo();

	for (ImpresionRecaudo factura : listaRecaudo) {

	    String montoNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.sub_total))), 2));

	    strPrint +=  Util.rpad(factura.numero_doc, 31, " ") + Util.rpad(""+montoNeto, 7, " ") + enter;

	    totalvalorNeto += factura.sub_total;

	}

	String strTotalNetoRecaudo= Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
	strPrint +=  Util.rpad("", 18, " ") +  Util.rpad("", 12, " ")+ Util.rpad(""+strTotalNetoRecaudo, 12, " ") + enter+enter;	


	strPrint += "NOTAS CREDITO"+enter;

	totalValorDescuento = 0;
	totalvalorNeto = 0;

	Vector<ImpresionNotaCredito> listaNotasCredito = DataBaseBO.getInformeNotasCredito();

	for (ImpresionNotaCredito factura : listaNotasCredito) {

	    String montoNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_neto))), 2));

	    strPrint +=  Util.rpad(factura.numero_doc, 31, " ") + Util.rpad(""+montoNeto, 7, " ") + enter;

	    totalValorDescuento += factura.valorSinIva;
	    totalvalorNeto += factura.valor_neto;

	}

	String strTotalNetoNCreditoSinIva= Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalValorDescuento))), 2));
	String strTotalNetoNCredito= Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
	strPrint +=  Util.rpad("", 18, " ") +  Util.rpad("TOTAL", 13, " ")+ Util.rpad(""+strTotalNetoNCredito, 7, " ") + enter+enter;	


	if(isInformeZeta){

	    Vector<FormaPago> listaGranTotal = DataBaseBO.getImpresionGranTotal();

	    strPrint += "ACUMULADO A LA FECHA "+ enter;
	    float granTotalRecaudo = 0;

	    for (FormaPago formaPago : listaGranTotal) {

		String Str_monto = "";

		if(!formaPago.descripcion.equals("PROXIMA FACTURA:"))
		    Str_monto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(formaPago.monto))), 2));
		else
		    Str_monto = String.valueOf((int)formaPago.monto);

		strPrint += Util.rpad(formaPago.descripcion, 20, " ") + Util.rpad(""+Str_monto, 18, " ") +enter;
	    }

	}


	for (int i = 0; i < 6; i++) {
	    strPrint += enter;
	}



	System.out.println(strPrint);
	return strPrint;

    }

    public static String generarImpresionLiquidacion( String numDoc,  int opcionLiquidar, Usuario liquidador, String tipo) {

	String strPrint; 

	char ret1 = 13;
	char ret2 = 10;
	char XON  = 17;
	String enter = String.valueOf(ret1) + String.valueOf(ret2);



	strPrint = "" + XON;
	strPrint += "" + enter;				
	//	Impresion impresion = DataBaseBO.getImpresion();
	strPrint += "" + enter;

	Usuario usuario = DataBaseBO.ObtenerUsuario();
	String subtitulo ="";

	if(opcionLiquidar == Const.IS_PRODUCTO_DANADO)
	    subtitulo = "PRODUCTO DANADO";
	else
	    subtitulo = "INVENTARIO";

	strPrint+= "#"+subtitulo+ enter + enter + enter;

	if( usuario != null ){

	    strPrint += "FECHA: " +  Util.FechaActual("yyyy-MM-dd HH:mm:ss") + enter;
	    strPrint += "LIQUIDADOR: "+enter;
	    strPrint += liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +enter;
	    strPrint += "CLIENTE: " + usuario.nombreVendedor + enter;
	    strPrint += "NEGOCIO: " + usuario.nombreVendedor + enter;
	    strPrint += "CODIGO: " + usuario.codigoVendedor + enter;
	    strPrint += "RUC: " +  usuario.codigoVendedor +enter;
	    strPrint += "DIRECCION: "  + enter;
	    strPrint += Util.lpad("", 38, "*") + enter;

	}        

	if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {

	    String and = "";
	    strPrint += Util.rpad("CODIGO", 7, " ") + Util.rpad("", 10, " ") + Util.rpad("DESCRIPCION", 21, " ")+ enter;
	    strPrint += Util.rpad("", 3, " ") + Util.rpad("FISICO", 12, " ") + Util.rpad("INV. FINAL", 15, " ")+ Util.rpad("DIF", 7, " ") + enter;
	    strPrint += Util.lpad("", 38, "*") + enter;


	    if(opcionLiquidar == Const.IS_PRODUCTO_DANADO || opcionLiquidar == Const.IS_INVENTARIO_LIQUIDACION)
		and = " AND diferencia <> 0 ";

	    Vector<Producto> vCanastilla;

	    vCanastilla = DataBaseBO.listaProductosLiquidados(tipo, and);
	    Producto detalle;

	    if (vCanastilla.size() > 0) {
		Enumeration<Producto> elements = vCanastilla.elements();

		while (elements.hasMoreElements()) {

		    detalle = elements.nextElement();

		    strPrint += Util.rpad(detalle.codigo, 7, " ") + Util.rpad(detalle.descripcion, 30, " ") + enter;
		    strPrint += Util.rpad("", 6, " ") + Util.rpad("" + detalle.cantidadDigitada, 12, " ")+ Util.rpad("" + detalle.cantidadInventario, 12, " ")+ Util.rpad("" + detalle.diferencia, 12, " ") + enter;

		}
	    }



	    strPrint += Util.lpad("", 38, "*") + enter+enter+enter;

	    strPrint += Util.rpad( "Aceptada Cliente.", 24, " ")   + enter;
	    strPrint += Util.rpad("Firma", 16, " ")+ enter;
	    strPrint += Util.rpad("C.C.", 15, " ") + enter;

	}




	for (int i = 0; i < 6; i++) {
	    strPrint += enter;
	}



	System.out.println(strPrint);
	return strPrint;

    }
    
}