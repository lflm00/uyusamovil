package celuweb.com.BusinessObject;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.ConsecutivosRecibos;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.DataObject.RecibosRecaudo;
import celuweb.com.DataObject.Usuario;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.Util;

public class TransaccionBO {

	public static void insertEncabezadoRecaudo(SQLiteDatabase db, Usuario usuario, String nroDoc, String nroRecibo, String deviceId) {
		
		/*
		String sql = "INSERT INTO EncabezadoRecaudo (nroDoc, Total, Fecha_recaudo, Vendedor, Bodega, Tipocliente, CodigoCliente, " +
				     "sincronizadomapas, Nrorecibo, TtlOtrosDescuentos, fecha_consecutivo, imei) " +
				     "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		*/
		
		String sql = "INSERT INTO encabezadorecaudo (nroDoc, Total, Fecha_recaudo, Vendedor, Bodega, Tipocliente, CodigoCliente, " +
			     "sincronizadomapas, Nrorecibo, fechaterminal, sincronizado, marcador ) " +
			     "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		
		
		
		SQLiteStatement insert = db.compileStatement(sql);
		insert.bindString ( 1, nroDoc);
		insert.bindDouble ( 2, Main.total_recaudo);
		insert.bindString ( 3, Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
		insert.bindString ( 4, usuario.codigoVendedor);
		insert.bindString ( 5, usuario.bodega);
		insert.bindLong   ( 6, Main.cliente.tipoCliente);
		insert.bindString ( 7, Main.cliente.codigo);
		insert.bindLong   ( 8, 0);
		insert.bindString ( 9, nroRecibo);
		//insert.bindString (10, "0");
		insert.bindString (10, Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
		insert.bindString (11, "0");
		insert.bindString (12, "1");
		//insert.bindString (12, deviceId);
		insert.executeInsert();
	}
	
	public static void insertDetalleRecaudo(SQLiteDatabase db, Usuario usuario, Cartera cartera, String nroDoc, String nroRecibo, String deviceId) {
		
		//String sql = "INSERT INTO detallerecaudo (Nrodoc, Tiporecaudo, valor, CodigoBanco, vendedor, bodega, Tipocliente, numdoc, Observacion, prontopago, tipopago, numero_fp, valor_fp, formapago, plaza, reference, bancod, fechapostd, valordpp ) " +
			//	      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		String sql = "INSERT INTO detallerecaudo (Nrodoc, Tiporecaudo, valor, CodigoBanco, vendedor, bodega, Tipocliente, numdoc, Observacion, prontopago, tipopago,sincronizado,valordpp) " +
			      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
	
		
		SQLiteStatement insert = db.compileStatement(sql);
		insert.bindString ( 1, nroDoc);
		insert.bindLong   ( 2, 0);
		insert.bindDouble ( 3, cartera.valorARecaudar);
		insert.bindString ( 4, "00");
		insert.bindString ( 5, usuario.codigoVendedor);
		insert.bindString ( 6, usuario.bodega);
		insert.bindLong   ( 7, Main.cliente.tipoCliente);
		insert.bindString ( 8, cartera.documento);
		
		if (cartera.observacion == null) {
			insert.bindNull(9);
		} else {
			insert.bindString (9, cartera.observacion);
		}
		
		if(cartera.prontoPago){
			insert.bindLong   (10, 1);	
		}else{
		    insert.bindLong   (10, 0);
		}
		
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (12, "0");
		
		if(cartera.prontoPago){
			insert.bindDouble   (13, cartera.valorProntoPago);	
		}else{
		    insert.bindDouble   (13, 0);
		}
		
		
		//insert.bindString (12, deviceId);
		
		//numero_fp, valor_fp, formapago, plaza, reference, bancod, fechapostd, valordpp
		/*
		
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		*/
		insert.executeInsert();
		
		if(!cartera.pagoTotal)
		db.execSQL("UPDATE CARTERA SET ABONO=ABONO+" +cartera.valorARecaudar + " WHERE DOCUMENTO='" + cartera.documento+ "'");
		
		
	}
	
	public static void insertDetalleRecaudo2(SQLiteDatabase db, Usuario usuario, Cartera cartera, String nroDoc, String nroRecibo, String deviceId) {
		
		//String sql = "INSERT INTO detallerecaudo (Nrodoc, Tiporecaudo, valor, CodigoBanco, vendedor, bodega, Tipocliente, numdoc, Observacion, prontopago, tipopago, numero_fp, valor_fp, formapago, plaza, reference, bancod, fechapostd, valordpp ) " +
			//	      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		String sql = "INSERT INTO detallerecaudo (Nrodoc, Tiporecaudo, valor, CodigoBanco, vendedor, bodega, Tipocliente, numdoc, Observacion, prontopago, tipopago,sincronizado,valordpp) " +
			      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
	
		
		SQLiteStatement insert = db.compileStatement(sql);
		insert.bindString ( 1, nroDoc);
		insert.bindLong   ( 2, 0);
		insert.bindDouble ( 3, cartera.valorARecaudar);
		insert.bindString ( 4, "00");
		insert.bindString ( 5, usuario.codigoVendedor);
		insert.bindString ( 6, usuario.bodega);
		insert.bindLong   ( 7, Main.cliente.tipoCliente);
		insert.bindString ( 8, cartera.documento);
		
		if (cartera.observacion == null) {
			insert.bindNull(9);
		} else {
			insert.bindString (9, cartera.observacion);
		}
		
		if(cartera.prontoPago){
			insert.bindLong   (10, 1);	
		}else{
		    insert.bindLong   (10, 0);
		}
		
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (12, "0");
		
		if(cartera.prontoPago){
			insert.bindDouble   (13, cartera.valorProntoPago);	
		}else{
		    insert.bindDouble   (13, 0);
		}
		
		
		//insert.bindString (12, deviceId);
		
		//numero_fp, valor_fp, formapago, plaza, reference, bancod, fechapostd, valordpp
		/*
		
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		insert.bindString (11, cartera.pagoTotal ? "T" : "P");
		*/
		insert.executeInsert();
		
		//if(!cartera.pagoTotal)
		//db.execSQL("UPDATE CARTERA SET ABONO=ABONO+" +cartera.valorARecaudar + " WHERE DOCUMENTO='" + cartera.documento+ "'");
		
		
	}

	
	
	public static void insertFormaPago(SQLiteDatabase db, Usuario usuario, FormaPago formaPago, String nroDoc, String deviceId) {
		
		String sql = "INSERT INTO formapago (nroDoc, forma_pago, monto, nro_cheque_cons, banco, fecha_registro, fecha_post, codigoCliente, vendedor, bodega, tipoCliente) " +
			         "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		SQLiteStatement insert = db.compileStatement(sql);
		
		insert.bindString ( 1, nroDoc);
		insert.bindLong   ( 2, formaPago.codigo);
		insert.bindDouble ( 3, formaPago.monto);
		insert.bindString ( 4, formaPago.nro_cheque_cons);
		if(formaPago.codigoBanco.equals("-1"))
		insert.bindString ( 5, "");
		else
		insert.bindString ( 5, formaPago.codigoBanco);
		insert.bindString ( 6, Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
		//insert.bindString ( 7, Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
		if(formaPago.fechaPago == null || formaPago.fechaPago.equals(""))
			insert.bindString ( 7, Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
		else
			insert.bindString ( 7, formaPago.fechaPago);
		
		insert.bindString ( 8, Main.cliente.codigo);
		insert.bindString ( 9, usuario.codigoVendedor);
		insert.bindString (10, usuario.bodega);
		insert.bindLong   (11, Main.cliente.tipoCliente);
		//insert.bindLong   (11, Main.cliente.tipoCliente);
		//insert.bindString (12, "0");
		//insert.bindString (12, deviceId);
		insert.executeInsert();
	}

    
	public static void actualizarConsecutivo(SQLiteDatabase db,RecibosRecaudo recibosRecaudo){
		
		String updateConsecutivo = "update recibos set actual= actual+1 where id ="+recibosRecaudo.id+" ";
		
		db.execSQL(updateConsecutivo);
		
	}
	
	
	
public static void insertConsecutivoRecibos(SQLiteDatabase db,ConsecutivosRecibos consecutivosRecibos ) {
		
		String sql = "INSERT INTO consecutivosrecibos ( id , nrorecibo ,sincronizado , vendedor, sincronizadoandroid ) " +
			         "VALUES (?, ?, ?, ?, ?)";
		
		SQLiteStatement insert = db.compileStatement(sql);
		insert.bindString ( 1, consecutivosRecibos.id+"");
		insert.bindString ( 2, consecutivosRecibos.nrorecibo+"");
		insert.bindString ( 3, consecutivosRecibos.sincronizado+"");
		insert.bindString ( 4, consecutivosRecibos.vendedor+"");
		insert.bindString ( 5, consecutivosRecibos.sincronizadoandroid+"");
		
		insert.executeInsert();
	}

/**
 * Insertar en detallerecaudo temporal
 * @param db
 * @param usuario
 * @param cartera
 * @param nroDoc
 * @param nroRecibo
 * @param deviceId
 */
public static void insertDetalleRecaudoTemporal(SQLiteDatabase db, Usuario usuario, Cartera cartera, String nroDoc, String nroRecibo, String deviceId) {


	String sql = "" +
			"INSERT INTO tmpdetallerecaudo (Nrodoc, " +
											"Tiporecaudo, " +
											"valor, " +
											"CodigoBanco, " +
											"numerointerno, " +
											"observaciones, " +
											"prontopago, " +
											"tipo_pago, " +
											"disponible, " +
											"referencia, " +
											"exercise, " +
											"finnancial, " +
											"IdFoto) " +
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	try {

		SQLiteStatement insert = db.compileStatement(sql);
		insert.bindString ( 1, nroDoc);
		insert.bindLong   ( 2, 1);
		insert.bindDouble ( 3, cartera.valorARecaudar);
		insert.bindString ( 4, "00");
		insert.bindString ( 5, "0"); 

		if (cartera.observacion == null) {
			insert.bindString (6, "");
		} else {
			insert.bindString (6, cartera.observacion);
		}

		if(cartera.prontoPago) {
			insert.bindLong(7, 1);	
		} else {
			insert.bindLong(7, 0);
		}

		insert.bindString(8, cartera.pagoTotal ? "T" : "P");
					
		insert.bindDouble(9, cartera.valorARecaudar);

		//insert.bindString(10, cartera.reference);
		
//		if(cartera.prontoPago) {
//			insert.bindDouble(13, cartera.valorProntoPago);	
//		} else {
//			insert.bindDouble(13, 0);
//		}

		insert.executeInsert();

	} catch (Exception e) {
		Log.e("TransaccionBO", e.getMessage());
	}
}

/**
 * metodo que ejecuta la consulta para Insertar en formapagotemp 
 * @param db
 * @param usuario
 * @param formaPago
 * @param nroDoc
 */
public static void insertFormaPagoTemporal(SQLiteDatabase db, Usuario usuario, FormaPago formaPago, String nroDoc) {

	String sql = "" +
			"INSERT INTO FormaPagoTemp (Nrodoc, forma_pago, monto," +
			" nro_cheque_cons, banco, fecha_registro, fecha_post, codigoCliente, " +
			"vendedor, bodega, tipoCliente, disponible, plaza, IdFoto) " +
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	SQLiteStatement insert = db.compileStatement(sql);

	insert.bindString ( 1, nroDoc);
	insert.bindLong   ( 2, formaPago.codigo);
	insert.bindString ( 3, Util.Redondear(""+formaPago.monto, 2));
	insert.bindString ( 4, formaPago.nro_cheque_cons);
	
	if(formaPago.codigoBanco.equals("-1"))
		insert.bindString ( 5, "-1");
	else
		insert.bindString ( 5, formaPago.codigoBanco);
	
	insert.bindString ( 6, Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
	
	if(formaPago.fechaPago == null || formaPago.fechaPago.equals(""))
		insert.bindString ( 7, Util.FechaActual("yyyy-MM-dd HH:mm:ss"));
	else
		insert.bindString ( 7, formaPago.fechaPago);

	insert.bindString ( 8, Main.cliente.codigo);
	insert.bindString ( 9, usuario.codigoVendedor);
	insert.bindString (10, usuario.bodega);
	insert.bindLong   (11, Main.cliente.tipoCliente); 
	insert.bindString (12, Util.Redondear(""+formaPago.monto, 2)); // disponible
	insert.bindString (13, formaPago.plaza); //plaza
	insert.bindString (14, formaPago.idFoto); //id de la foto por cada cheque

	insert.executeInsert();
}

	

}
