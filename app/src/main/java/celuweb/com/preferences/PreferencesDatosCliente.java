/**
 * 
 */
package celuweb.com.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Clase que ayuda a la insercion y obtencion de datos por medio de
 * SharedPreferences.
 * Conservar los datos del cliente
 * @author JICZ
 *
 */
public class PreferencesDatosCliente {
	
	/**
	 * constante para conservar el nombre del archivo de preferencias
	 */
	private static final String NOMBRE = "DatosCliente";	
	
	/**
	 * representa el key para acceder al codigo cliente
	 */
	private static final String CODIGO_CLIENTE = "codigoCliente";
	
	/**
	 * representa el key para acceder al nombre de cliente
	 */
	private static final String AUX_CANAL = "auxCanal";
	
	/**
	 * subcanal auxiliar
	 */
	private static final String AUX_SUBCANAL = "auxSubCanal";
	
	
	/**
	 * grupo de comosiones al que pertenece
	 */
	private static final String GRUPO_COMISIONES = "grupoComisioines";
	
	/**
	 * iva generado del cliente.
	 */
	private static final String IVA_CLIENTE = "ivaCliente";
	
	/**
	 * bodega del cliente.
	 */
	private static final String BODEGA = "bodega";
	
	/**
	 * razon social del cliente.
	 */
	private static final String RAZON = "razon";
	
	
	/**
	 * CentroSum del cliente.
	 */
	private static final String CENTRO_SUM = "centro_sum";


	

	
	
	
	
	
	
	/**
	 * Permite guardar el numero de documento 
	 * @param context contexto al que se aplica el preference
	 * @param numeroDoc
	 */
	public static void guardarCodigoCliente(Context context, String numeroDoc) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(CODIGO_CLIENTE, numeroDoc);
		editor.commit();
	}
	
	/**
	 * obtener el codigo cliente.
	 * @param context
	 * @return
	 */
	public static String getCodigoCliente(Context context){
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(CODIGO_CLIENTE, null);
	}
	
	
	
	
	
	/**
	 * Permite guardar canal auxiliar
	 * @param context contexto al que se aplica el preference
	 * @param numeroDoc
	 */
	public static void guardarAuxCanal(Context context, String nombre) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(AUX_CANAL, nombre);
		editor.commit();
	}	
	
	/**
	 * obtener canal auxiliar
	 * @param context
	 * @return
	 */
	public static String getAuxCanal(Context context){
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(AUX_CANAL, null);
	}


	
	
	/**
	 * Permite guardar sub canal
	 * @param context contexto al que se aplica el preference
	 * @param numeroDoc
	 */
	public static void guardarAuxSubcanal(Context context, String razonSocial) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(AUX_SUBCANAL, razonSocial);
		editor.commit();
	}

	/**
	 * obtener subcanal
	 * @param context
	 * @return
	 */
	public static String getAuxSubcanal(Context context){
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(AUX_SUBCANAL, null);
	}
	
	
	
	
	
	
	/**
	 * guardar el grupo de comisiones al que pertenece el cliente
	 * @param context
	 * @param razonSocial
	 */
	public static void guardarGrupoComisiones(Context context, String razonSocial) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(GRUPO_COMISIONES, razonSocial);
		editor.commit();
	}
	
	/**
	 * obtener grupo comisiones
	 * @param context
	 * @return
	 */
	public static String getGrupoComisiones(Context context){
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(GRUPO_COMISIONES, null);
	}
	
	
	
	
	
	
	
	/**
	 * guardar el iva generado del cliente.
	 * @param context
	 * @param razonSocial
	 */
	public static void guardarIvaCliente(Context context, String razonSocial) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(IVA_CLIENTE, razonSocial);
		editor.commit();
	}	
	
	/**
	 * Obtener iva de cliente
	 * @param context
	 * @return
	 */
	public static String getIvaCliente(Context context) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(IVA_CLIENTE, null);
	}
	
	
	
	
	
	
	
	/**
	 * guardar bodega del cliente.
	 * @param context
	 * @param razonSocial
	 */
	public static void guardarBodegaCliente(Context context, String bodega) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(BODEGA, bodega);
		editor.commit();
	}	
	
	/**
	 * Obtener bodega del cliente
	 * @param context
	 * @return
	 */
	public static String getBodegaCliente(Context context) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(BODEGA, null);
	}
	
	
	

	/**
	 * guardar razon social del cliente.
	 * @param context
	 * @param razonSocial
	 */
	public static void guardarRazonCliente(Context context, String razon) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(RAZON, razon);
		editor.commit();
	}	
	
	/**
	 * Obtener razon social del cliente
	 * @param context
	 * @return
	 */
	public static String getRazonCliente(Context context) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(RAZON, null);
	}
	
	
	
	
	/**
	 * guardar razon social del cliente.
	 * @param context
	 * @param razonSocial
	 */
	public static void guardarCentroSum(Context context, String centroSum) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(CENTRO_SUM, centroSum);
		editor.commit();
	}	
	
	/**
	 * Obtener razon social del cliente
	 * @param context
	 * @return
	 */
	public static String getCentroSum(Context context) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		return settings.getString(CENTRO_SUM, null);
	}
	
	
	
	/**
	 * vaciar el preference. Remover todos los datos guardados.
	 * @param context
	 */
	public static void vaciarPreferences(Context context) {
		SharedPreferences settings = context.getSharedPreferences(NOMBRE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		//vaciar todo el preference
		editor.clear();
		editor.commit();
	}
}
