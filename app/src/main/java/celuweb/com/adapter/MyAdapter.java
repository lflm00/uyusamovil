package celuweb.com.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Vector;

import celuweb.com.DataObject.Producto;
import celuweb.com.uyusa.R;
import celuweb.com.uyusa.Util;

public class MyAdapter  extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Vector<Producto> productosAlt;
    private ItemClickListener clickListener;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mtexDescripcion;
        public TextView mtextCodigo;
        public TextView mtextPrecio;

        public MyViewHolder(View v) {
            super(v);
            mtextCodigo = (TextView) v.findViewById(R.id.my_text_codigo);
            mtexDescripcion = (TextView) v.findViewById(R.id.my_text_descripcion);
            mtextPrecio = (TextView) v.findViewById(R.id.my_text_precio);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(clickListener != null ) clickListener.onClick(itemView, getAdapterPosition());
        }
    }

    public void clickListener(ItemClickListener itemClickListener){
        this.clickListener = itemClickListener;
    }

    public MyAdapter(Vector<Producto> productoList, Context context){
        this.productosAlt = productoList;
        this.context = context;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_alt, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Producto producto = productosAlt.get(position);
        holder.mtextCodigo.setText(producto.codigo);
        holder.mtexDescripcion.setText(producto.descripcion);
        holder.mtextPrecio.setText(Util.setFormatoCOP(String.valueOf(producto.precio)));

    }

    @Override
    public int getItemCount() {
        return productosAlt == null ? 0 : productosAlt.size();
    }
}
