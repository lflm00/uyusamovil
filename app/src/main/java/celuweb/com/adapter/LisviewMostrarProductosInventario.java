package celuweb.com.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import celuweb.com.DataObject.ItemListView;
import celuweb.com.uyusa.R;

public class LisviewMostrarProductosInventario extends ArrayAdapter<ItemListView> {

    public ItemListView[] itemList;
    public Context context;
    public int[] colors;

    public  LisviewMostrarProductosInventario(Context context,ItemListView[] itemList ){
        super(context, R.layout.list_item_productos, itemList);
        this.itemList = itemList;
        this.context = context;
        colors = new int[] { R.color.colorPrimary, R.color.colorPrimaryDark };
    }

    /**
     * VISUALIZACION DE LA LISTA
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public View getView(final int position, View convertView, ViewGroup parent) {
        View item = convertView;
        LisviewMostrarProductosInventario.ViewHolder holder;

        if(item == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            item = inflater.inflate(R.layout.list_item_seleccione, null);

            holder = new LisviewMostrarProductosInventario.ViewHolder();
            holder.titulo = (TextView)item.findViewById(R.id.lblTitulo);

            item.setTag(holder);

        } else {

            holder =(LisviewMostrarProductosInventario.ViewHolder) item.getTag();
        }

        holder.titulo.setText(itemList[position].titulo);
        //holder.titulo.setTypeface(Const.letraNormal);

        return(item);
    }

    /**
     * VIEWHOLDER
     */
    static class ViewHolder {
        TextView titulo;

    }


}
