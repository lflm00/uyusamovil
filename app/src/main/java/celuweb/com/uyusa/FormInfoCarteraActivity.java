package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.ItemCarteraListView;

public class FormInfoCarteraActivity extends Activity {

	AlertDialog alertDialog;
	Vector<Cartera> listaCartera;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_info_cartera);
		
		CargarCartera();
		//SetListenerListView();
	}
	
	/*public void SetListenerListView() {
		
		ListView listaOpciones = (ListView)findViewById(R.id.listaInfoCartera);
		listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				Cartera cartera = listaCartera.elementAt(position);
				
				if (cartera != null)
					Log.i("Listener Cartera ", "es diferente de null");
				
				Object o = view.getClass();
				
				//Intent formInfoCliente = new Intent(FormInfoCarteraActivity.this, FormInfoClienteActivity.class);
				//startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);
			}
        });
	}*/
	
	public void CargarCartera() {

		ItemCarteraListView[] items;
		Vector<ItemCarteraListView> listaItems = new Vector<ItemCarteraListView>();
		listaCartera = DataBaseBO.ListaCartera(Main.cliente.codigo, listaItems);

		if (listaItems.size() > 0) {

			items = new ItemCarteraListView[listaItems.size()];
			listaItems.copyInto(items);

		} else {

			items = new ItemCarteraListView[] {};

			if (listaCartera != null)
				listaCartera.removeAllElements();
		}

		ListViewCarteraAdapter adapter = new ListViewCarteraAdapter(this, items, R.drawable.cliente/*, 0x2E65AD*/);
		ListView listaInfoCartera = (ListView) findViewById(R.id.listaInfoCartera);
		listaInfoCartera.setAdapter(adapter);
	}
	
	public void OnClikAceptarInfoCartera(View view) {
		
		ListView listaInfoCartera = (ListView) findViewById(R.id.listaInfoCartera);
		ListViewCarteraAdapter adapter = (ListViewCarteraAdapter)listaInfoCartera.getAdapter();
		
		int count = 0;
		float total = 0;
		//Main.cartera.removeAllElements();
		
		for(int i = 0; i < adapter.listItems.length; i++) {
			
			if (adapter.listItems[i].seleccionado) {
				
				Cartera cartera = listaCartera.elementAt(i);
				Main.cartera.addElement(cartera);
				total += cartera.valorARecaudar;
				count++;
				
				//cateraSeleccionada += cartera.referencia + ", " + cartera.descripcion + "\n";
			}
		}
		
		if (count == 1 && total > 0) {
			
			Intent formRegistrarRecaudo = new Intent(this, FormRegistrarRecaudoActivity.class);
			startActivityForResult(formRegistrarRecaudo, Const.RESP_FROM_AGREGAR_CARTERA);
			
		} else {
			
			setResult(RESULT_OK);
			finish();
		}
		
		//if (total > 0) {
			
			
			
		//} else {
			
			//MostrarAlertDialog("El valor a recuadar de ser mayor que cero: " + Util.SepararMiles("" + total));
		//}
		
		
		
		
		/*AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(cateraSeleccionada)
		.setCancelable(false)
		.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				dialog.cancel();				
			}
		});
		
		AlertDialog alert = builder.create();
		alert.show();*/
	}
	
	public void OnClikCancelarInfoCartera(View view) {
		
		setResult(RESULT_CANCELED);
		finish();
	}
	
	public void MostrarAlertDialog(String mensaje) {
    	
    	if (alertDialog == null) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			alertDialog = builder.create();
    	}
    	
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == Const.RESP_FROM_AGREGAR_CARTERA && resultCode == RESULT_OK) {
			
			setResult(RESULT_OK);
			finish();
		}
	}
}
