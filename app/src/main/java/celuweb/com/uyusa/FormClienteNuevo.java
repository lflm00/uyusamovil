package celuweb.com.uyusa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Categoria;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.ClienteNuevo;
import celuweb.com.DataObject.Encabezado;

public class FormClienteNuevo extends Activity implements Sincronizador {
	
	Dialog dialogRuta;
	ProgressDialog progressDialog;
	
	Vector<Categoria> listaCategorias;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_cliente_nuevo);
		
		CargarCiudad();
		CargarCategorias();
	}
	
	public void CargarCiudad() {
		
		String[] items;
		Vector<String> listaCiudad = DataBaseBO.ListaCiudadCliente();

		if (listaCiudad.size() > 0) {

			items = new String[listaCiudad.size()];
			listaCiudad.copyInto(items);

		} else {

			items = new String[] {};

			if (listaCiudad != null)
				listaCiudad.removeAllElements();
		}
		
		Spinner cbCiudadRutero = (Spinner) findViewById(R.id.cbCiudad);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbCiudadRutero.setAdapter(adapter);
	}
	
	public void CargarCategorias() {
		
		String[] items;
		Vector<String> listaItems = new Vector<String>();
		listaCategorias = DataBaseBO.ListaCategorias(listaItems);

		if (listaItems.size() > 0) {

			items = new String[listaItems.size()];
			listaItems.copyInto(items);

		} else {

			items = new String[] {};

			if (listaCategorias != null)
				listaCategorias.removeAllElements();
		}
		
		Spinner cbCategoria = (Spinner) findViewById(R.id.cbCategoria);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbCategoria.setAdapter(adapter);
	}
	
	/*public void CargarDiaParada() {

		String[] items;
		Vector<String> listaDiaParada = DataBaseBO.ListaDiasRutero();

		if (listaDiaParada.size() > 0) {

			items = new String[listaDiaParada.size()];
			listaDiaParada.copyInto(items);

		} else {

			items = new String[] {};

			if (listaDiaParada != null)
				listaDiaParada.removeAllElements();
		}

		Spinner cbCiudadRutero = (Spinner) findViewById(R.id.cbDiaParada);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbCiudadRutero.setAdapter(adapter);
	}*/
	
	public void OnClickSeleccionarRuta(View view) {
		
		MostrardialogRuta();
	}
	
	public void OnClickGuardarCliente(View view) {
		
		ClienteNuevo clienteNuevo = new ClienteNuevo();		
		
		if (Main.cliente == null)
			Main.cliente = new Cliente();
		
		int index = ((Spinner) findViewById(R.id.cbCategoria)).getSelectedItemPosition();;
		Categoria categoria = listaCategorias.elementAt(index);
		
		/*clienteNuevo.codigo      = ((EditText)findViewById(R.id.txtNit)).getText().toString().trim(); 
		Main.cliente.codigo      = clienteNuevo.codigo;
		clienteNuevo.nombre      = ((EditText)findViewById(R.id.txtNombreCliente)).getText().toString().trim();
		Main.cliente.nombre      = clienteNuevo.nombre;
		clienteNuevo.razonSocial = ((EditText)findViewById(R.id.txtRazonSocial)).getText().toString().trim();
		Main.cliente.razonSocial = clienteNuevo.razonSocial;
		clienteNuevo.nit         = clienteNuevo.codigo; 
		Main.cliente.nit         = clienteNuevo.codigo;
		clienteNuevo.direccion   = ((EditText)findViewById(R.id.txtDireccion)).getText().toString().trim();
		Main.cliente.direccion   = clienteNuevo.direccion;
		clienteNuevo.ciudad      = ((Spinner) findViewById(R.id.cbCiudad)).getSelectedItem().toString();
		Main.cliente.ciudad      = clienteNuevo.ciudad; 
		clienteNuevo.telefono    = ((EditText)findViewById(R.id.txtTelefono)).getText().toString().trim();
		Main.cliente.telefono    = clienteNuevo.telefono;
		clienteNuevo.vendedor    = Main.usuario.codigoVendedor;
		
		clienteNuevo.ruta_parada = ObtenerRutaParada();
		Main.cliente.ruta_parada = clienteNuevo.ruta_parada;
		clienteNuevo.actividad   = categoria.codigo;
		
		clienteNuevo.ordenVisita = Util.ToInt(((EditText)findViewById(R.id.txtOrdenVisita)).getText().toString());
		Main.cliente.ordenVisita = clienteNuevo.ordenVisita; 
		clienteNuevo.tipoDoc     = "1";
		
		clienteNuevo.territorio  = ((EditText)findViewById(R.id.txtBarrio)).getText().toString().trim();
		Main.cliente.territorio  = clienteNuevo.territorio;
		clienteNuevo.agencia     = "";
		
		Main.cliente.bloqueado   = "N";
		Main.cliente.tipoCredito = Const.EFECTIVO;
		Main.cliente.listaPrecio = DataBaseBO.ListaPrecioClienteOcasional();*/

		if (clienteNuevo.razonSocial.equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese la Razon Social");
			((EditText)findViewById(R.id.txtRazonSocial)).requestFocus();
			return;
		}
		
		if (clienteNuevo.nit.equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese el Nit");
			((EditText)findViewById(R.id.txtNit)).requestFocus();
			return;
		}
		
		if (clienteNuevo.direccion.equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese la Direccion");
			((EditText)findViewById(R.id.txtDireccion)).requestFocus();
			return;
		}
		
		if (clienteNuevo.ordenVisita == 0) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese el Orden Visita.");
			((EditText)findViewById(R.id.txtOrdenVisita)).requestFocus();
			return;
		}
		
		if (clienteNuevo.ruta_parada.trim().equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese la Ruta");
			return;
		}
		
		boolean ingreso = DataBaseBO.GuardarClienteNuevo(clienteNuevo);

		if (ingreso) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Cliente Registrado con Exito")	
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					progressDialog = ProgressDialog.show(FormClienteNuevo.this, "", "Enviando Informacion Cliente...", true);
					progressDialog.show();
					
					//DataBaseBO.ActualizarSnEnvio(Main.encabezado.id, 1);
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					Sync sync = new Sync(FormClienteNuevo.this, Const.ENVIAR_PEDIDO);
					sync.start();
				}
			});
			/*.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					//DataBaseBO.ActualizarSnEnvio(Main.encabezado.id, 0);
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					FormClienteNuevo.this.setResult(RESULT_OK);
					FormClienteNuevo.this.finish();
				}
			});*/
			
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	public String ObtenerRutaParada() {
		
		String rutaParada = "";
		
		if (dialogRuta != null) {
			
			boolean lunes     = ((CheckBox)dialogRuta.findViewById(R.id.checkLunes)).isChecked();
			boolean martes    = ((CheckBox)dialogRuta.findViewById(R.id.checkMartes)).isChecked();
			boolean miercoles = ((CheckBox)dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
			boolean jueves    = ((CheckBox)dialogRuta.findViewById(R.id.checkJueves)).isChecked();
			boolean viernes   = ((CheckBox)dialogRuta.findViewById(R.id.checkViernes)).isChecked();
			boolean sabado    = ((CheckBox)dialogRuta.findViewById(R.id.checkSabado)).isChecked();
			boolean domingo   = ((CheckBox)dialogRuta.findViewById(R.id.checkDomingo)).isChecked();
			
			rutaParada += lunes ? "1" : "0";
			rutaParada += martes ? "1" : "0";
			rutaParada += miercoles ? "1" : "0";
			rutaParada += jueves ? "1" : "0";
			rutaParada += viernes ? "1" : "0";
			rutaParada += sabado ? "1" : "0";
			rutaParada += domingo ? "1" : "0";
		}

		return rutaParada;
	}
	
	public void MostrardialogRuta() {
		
		if (dialogRuta == null) {
			
			dialogRuta = new Dialog(this);
			dialogRuta.requestWindowFeature(Window.FEATURE_LEFT_ICON);
			dialogRuta.setContentView(R.layout.dialog_dias_ruta);
			dialogRuta.setTitle("Ingresar Ruta");
		}
		
		((Button)dialogRuta.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				boolean lunes     = ((CheckBox)dialogRuta.findViewById(R.id.checkLunes)).isChecked();
				boolean martes    = ((CheckBox)dialogRuta.findViewById(R.id.checkMartes)).isChecked();
				boolean miercoles = ((CheckBox)dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
				boolean jueves    = ((CheckBox)dialogRuta.findViewById(R.id.checkJueves)).isChecked();
				boolean viernes   = ((CheckBox)dialogRuta.findViewById(R.id.checkViernes)).isChecked();
				boolean sabado    = ((CheckBox)dialogRuta.findViewById(R.id.checkSabado)).isChecked();
				boolean domingo   = ((CheckBox)dialogRuta.findViewById(R.id.checkDomingo)).isChecked();
				
				String ruta = "";
				ruta += lunes ? "Lunes" : "";
				ruta += martes ? (ruta.equals("") ? "" : ", " ) + "Martes" : "";
				ruta += miercoles ? (ruta.equals("") ? "" : ", " ) + "Miercoles" : "";
				ruta += jueves ? (ruta.equals("") ? "" : ", " ) + "Jueves" : "";
				ruta += viernes ? (ruta.equals("") ? "" : ", " ) + "Viernes" : "";
				ruta += sabado ? (ruta.equals("") ? "" : ", " ) + "Sabado" : "";
				ruta += domingo ? (ruta.equals("") ? "" : ", " ) + "Domingo" : "";
				
				String rutaParada = Util.SepararPalabrasTextView(ruta, 30);
				(((TextView)findViewById(R.id.lblRutaParada))).setText(Html.fromHtml(rutaParada));
				
				dialogRuta.cancel();
			}
		});
		
		((Button) dialogRuta.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {

				dialogRuta.cancel();
			}
		});
		
		dialogRuta.setCancelable(false);
		dialogRuta.show();
		dialogRuta.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_list);
	}
	
	public void OnClickCancelarPedido(View view) {
		
		finish();
	}

	@Override
	public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
		
		final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : msg;
		
		if (progressDialog != null)
			progressDialog.cancel();
		
		this.runOnUiThread(new Runnable() {
			
			public void run() {
				
				AlertDialog.Builder builder = new AlertDialog.Builder(FormClienteNuevo.this);
				builder.setMessage(mensaje)
						
				.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.cancel();
						
						FormClienteNuevo.this.setResult(RESULT_OK);
						//FormClienteNuevo.this.finish();
						cargarCliente();
						Intent formInfoCliente = new Intent(FormClienteNuevo.this, FormInfoClienteActivity.class);
						startActivityForResult(formInfoCliente,Const.RESP_CLIENTE_NUEVO);
					}
				});
				
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		
		finish();		
	}
	
	private void cargarCliente(){
		
		
		
	}
}
