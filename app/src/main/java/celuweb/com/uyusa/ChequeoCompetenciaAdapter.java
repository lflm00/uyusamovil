package celuweb.com.uyusa;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Vector;

import celuweb.com.DataObject.ChequeoPrecios;


/**
 * Created by Andres Rangel on 25/10/2017.
 */

public class ChequeoCompetenciaAdapter extends RecyclerView.Adapter<ChequeoCompetenciaAdapter.AdapterViewHolder> {

    Context context;
    Vector<ChequeoPrecios> arrayList;

    public ChequeoCompetenciaAdapter(Context context, Vector<ChequeoPrecios> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ChequeoCompetenciaAdapter.AdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate( R.layout.list_items_control3,viewGroup,false);
        AdapterViewHolder adapterViewHolder=new AdapterViewHolder(view);
        return adapterViewHolder;
    }

    @Override
    public void onBindViewHolder(ChequeoCompetenciaAdapter.AdapterViewHolder holder, final int i) {

            holder.txtNombre.setText(arrayList.get(i).getDescripcion());

        if (FormChequeoPreciosPropios.hashMapChequeoCompetencia.containsKey(arrayList.get(i).getCodProductoCompetencia())){

            holder.editNombre.setText
                    (FormChequeoPreciosPropios.hashMapChequeoCompetencia.get(arrayList.get(i).getCodProductoCompetencia()).getPrecio());
        }

            holder.editNombre.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.length() > 0) {
                        arrayList.get(i).setPrecio(editable.toString());
                       FormChequeoPreciosPropios.hashMapChequeoCompetencia.put(arrayList.get(i).getCodProductoCompetencia(), arrayList.get(i));
                    } else {
                        FormChequeoPreciosPropios.hashMapChequeoCompetencia.remove(arrayList.get(i).getCodProductoCompetencia());
                    }
                }
            });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {
        TextView txtNombre;
        EditText editNombre;
        TableRow tableRow;
        public AdapterViewHolder(View itemView) {
            super(itemView);

            txtNombre= (TextView) itemView.findViewById(R.id.txtNombre);
            editNombre= (EditText) itemView.findViewById(R.id.editNombre);
            tableRow= (TableRow) itemView.findViewById(R.id.tableControl);
        }
    }
    public void updateRecords(Vector<ChequeoPrecios> arrayList){
        this.arrayList = arrayList;

        notifyDataSetChanged();
    }
}
