/**
 * 
 */
package celuweb.com.uyusa;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.AgotadoYDevolucion;
import celuweb.com.DataObject.Cliente;

/**
 * Muestra una tabla dinamica que muestra el estado de ventas del vendedor.
 * 
 * @author JICZ
 * 
 */
public class FormAgotadosYDevoluciones extends Activity {


	/*Constantes que se usaran para determinar el ancho de las columnas  de ambas tablas, cabecera y contenido*/
	private TableRow.LayoutParams paramsFecha =  new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT); 
	private TableRow.LayoutParams paramsProducto =  new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
	private TableRow.LayoutParams paramsCantidad =  new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
	private TableRow.LayoutParams paramsMotivo=  new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
	private TableRow.LayoutParams paramsMercaderista =  new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);

	/*Constante, define si la busqueda de contenido se hace para agotados o devoluciones*/
	private static String AGOTADO = "AGOTADO";
	private static String DEVOLUCION = "DEVOLUCION";

	/*Views que representan las tablas que seran usadas para contenido y cabecera y parametrizacion */
	private TableLayout tablaCabecera;
	private TableLayout tablaContenido;
	private TableRow.LayoutParams filaTotal;



	// color de la letra. (Negra)
	int color = R.color.black;

	//usado para definir el cliente al que se hara la consulta de agotados y devoluciones.
	private Cliente cliente;
	
	public static int indice;
	public int maximo;
	public static int indiceInicial;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.agotados_devoluciones);

		// instancias de view necesarias
		tablaCabecera = (TableLayout) findViewById(R.id.tableLayoutCabeceraAgotDev);
		tablaContenido = (TableLayout) findViewById(R.id.tablaContenidoAgotDev);
		// paramtreizacion de tama�o de fila.
		filaTotal = new TableRow.LayoutParams(3000, TableRow.LayoutParams.WRAP_CONTENT);


		cliente = DataBaseBO.CargarClienteSeleccionado();

		TextView titulo = (TextView) findViewById(R.id.textViewTituloAgotDev);		
		titulo.setText(cliente.razonSocial);
		
		indice = indiceInicial = Integer.MIN_VALUE; // valor inicial (minimo valor entero) para ambos indices.
		maximo = 0;
		// crear cabecera
		agregarCabecera();
		// crear contenido, para el cliente inicialmente devolucion
		agregarContenido(cliente.codigo, AGOTADO, true);
	}





	/**
	 * se define el ttulo de las filas de la cabecera de la tabla.
	 */
	private void agregarCabecera() {

		//capturar los nombres de las columnas definidas en strings.xml
		String[] tituloColumnas = getResources().getStringArray(R.array.agotados_devoluciones_nombres_columnas);

		TableRow row = new TableRow(this);
		row.setLayoutParams(filaTotal);
		row.setWeightSum(1f);

		// definicion de columnas
		final TextView txtFecha = new TextView(this);
		final TextView txtProducto = new TextView(this);
		final TextView txtCantidad = new TextView(this);
		final TextView txtMotivo = new TextView(this);
		final TextView txtMercaderista= new TextView(this);


		paramsFecha.weight = 0.10f;
		txtFecha.setText(tituloColumnas[0]);
		txtFecha.setTextColor(color);
		txtFecha.setBackgroundResource(R.drawable.celda_cabecera_tabla);


		paramsProducto.weight = 0.26f;
		txtProducto.setText(tituloColumnas[1]);
		txtProducto.setTextColor(color);
		txtProducto.setBackgroundResource(R.drawable.celda_cabecera_tabla);


		paramsCantidad.weight = 0.06f;
		txtCantidad.setText(tituloColumnas[2]);
		txtCantidad.setTextColor(color);
		txtCantidad.setBackgroundResource(R.drawable.celda_cabecera_tabla);


		paramsMotivo.weight = 0.35f;
		txtMotivo.setText(tituloColumnas[3]);
		txtMotivo.setTextColor(color);
		txtMotivo.setBackgroundResource(R.drawable.celda_cabecera_tabla);


		paramsMercaderista.weight = 0.23f;
		txtMercaderista.setText(tituloColumnas[4]);
		txtMercaderista.setTextColor(color);
		txtMercaderista.setBackgroundResource(R.drawable.celda_cabecera_tabla);

		/*alinear la cuadricula para que se conserven del mismo tama�o que su par.
		 * el valor height de categoria es siempre mas grande que el de total, por esto se elige como referencia*/
		ViewTreeObserver viewTreeObserver = txtMotivo.getViewTreeObserver();
		viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {

				int height = txtMotivo.getMeasuredHeight();
				txtFecha.setHeight(height);
				txtProducto.setHeight(height);
				txtCantidad.setHeight(height);
				txtMercaderista.setHeight(height);
			}
		});

		// insertar en el table contenido.
		row.addView(txtFecha, paramsFecha);
		row.addView(txtProducto, paramsProducto);
		row.addView(txtCantidad, paramsCantidad);
		row.addView(txtMotivo, paramsMotivo);
		row.addView(txtMercaderista, paramsMercaderista);
		tablaCabecera.addView(row);
	}



	/**
	 * se llena el contenido del table con la informacion leida de la tabla
	 * seguimientoventas en database.db
	 * @param tipo 
	 */
	private void agregarContenido(String codigoCliente, String tipo, boolean evento) {

		// lista con los datos a mostrar
		ArrayList<AgotadoYDevolucion> listaContenido = DataBaseBO.cargarAgotadoYDevoluciones(codigoCliente, tipo, evento, indice);	

		for (AgotadoYDevolucion ad : listaContenido) {

			indice = ad.indice; // actualizar el indice.
			maximo = ad.maximo;			
			
			// definicion de una fila que ocupa todo el ancho del padre, es
			// decir de la tabla.
			// el tablerow ocupara todo el ancho del padre y sus hijos pueden
			// usar un peso maximo de 1. (weightsum = 1).
			TableRow row = new TableRow(this);
			row.setLayoutParams(filaTotal);
			row.setWeightSum(1f);

			// definicion de columnas
			final TextView txtFecha = new TextView(this);
			final TextView txtProducto = new TextView(this);
			final TextView txtCantidad = new TextView(this);
			final TextView txtMotivo = new TextView(this);
			final TextView txtMercaderista= new TextView(this);

			txtFecha.setText(ad.fecha);
			txtFecha.setTextColor(color);
			txtFecha.setBackgroundResource(R.drawable.celda_tabla);

			txtProducto.setText(ad.producto);
			txtProducto.setTextColor(color);
			txtProducto.setBackgroundResource(R.drawable.celda_tabla);

			txtCantidad.setText(ad.cantidad);
			txtCantidad.setTextColor(color);
			txtCantidad.setBackgroundResource(R.drawable.celda_tabla);

			txtMotivo.setText(ad.motivo);
			txtMotivo.setTextColor(color);
			txtMotivo.setBackgroundResource(R.drawable.celda_tabla);

			txtMercaderista.setText(ad.mercaderista);
			txtMercaderista.setTextColor(color);
			txtMercaderista.setBackgroundResource(R.drawable.celda_tabla);

			/*alinear la cuadricula para que se conserven del mismo tama�o que su par.
			 * el valor height de categoria es siempre mas grande que el de total, por esto se elige como referencia*/
			ViewTreeObserver viewTreeObserver = txtMotivo.getViewTreeObserver();
			viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {

					int height = txtMotivo.getMeasuredHeight();
					txtFecha.setHeight(height);
					txtProducto.setHeight(height);
					txtCantidad.setHeight(height);
					txtMercaderista.setHeight(height);
				}
			});

			// insertar en el table contenido.
			row.addView(txtFecha, paramsFecha);
			row.addView(txtProducto, paramsProducto);
			row.addView(txtCantidad, paramsCantidad);
			row.addView(txtMotivo, paramsMotivo);
			row.addView(txtMercaderista, paramsMercaderista);
			tablaContenido.addView(row);
		}
		
		//actualizar el valor inicial del indice 
		if(indiceInicial == Integer.MIN_VALUE && indice != Integer.MIN_VALUE){
			indiceInicial = indice;
		}		
	}


	/**
	 * captura evento de radioButtons
	 * @param view
	 */
	public void radioOnclickDevoluciones(View view){

		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch(view.getId()) {
		case R.id.rdDevolucion:
			if (checked) {
				//limpiar la tabla y agregar nuevo contenido con devoluciones
				indice = 0;
				tablaContenido.removeAllViews();
				agregarContenido(cliente.codigo, DEVOLUCION, true);
			}
			break;
		case R.id.rdAgotado:
			if (checked) {
				//limpiar la tabla y agregar nuevo contenido con agotados
				indice = 0;
				tablaContenido.removeAllViews();
				agregarContenido(cliente.codigo, AGOTADO, true);
			}
			break;
		}
	}
	
	/**
	 * pasar a la siguiente pagina
	 * @param view
	 */
	public void onClickSiguiente(View view) {
		if(maximo <= indice){
			return;
		}
		tablaContenido.removeAllViews();
		agregarContenido(cliente.codigo, AGOTADO, true);		
	}
	
	/**
	 * pasar a la siguiente pagina
	 * @param view
	 */
	public void onClickAnterior(View view) {
		if(indiceInicial == indice){
			return;
		}
		tablaContenido.removeAllViews();
		agregarContenido(cliente.codigo, AGOTADO, false);		
	}
	
	
}//final de la clase
