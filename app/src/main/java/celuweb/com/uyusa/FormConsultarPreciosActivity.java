package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Producto;

public class FormConsultarPreciosActivity extends Activity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	Vector<Producto> listaProducto;
	Vector<String> listado;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Put your code here
		
		setContentView(R.layout.consultar_precios);
		
		cargarComboListasPrecios();
	
		mostrarHeader();
	}
	
	
	public void cargarComboListasPrecios(){
		
		
		String[] items;
		listado = DataBaseBO.ListasPrecios();

		if (listado.size() > 0) {

			items = new String[listado.size()];
			listado.copyInto(items);

		} else {

			items = new String[] {};

			if (listado != null)
				listado.removeAllElements();
		}

		Spinner cbListasPrecios= (Spinner) findViewById(R.id.cbListasPrecios);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
    	
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbListasPrecios.setAdapter(adapter);
        
        cbListasPrecios.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		cargarProductos();
        		
        	}

            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });
		
		
	}
	
	
	public void OnClickFormBuscarProductos(View view){

		try {
			cargarProductos();

		}catch(Exception e){}
	
	}
	
	
	public void OnClickBuscarXCodigo(View view){
		
		if(((RadioButton)findViewById(R.id.radioBucarXCodigo)).isChecked()){
			
			((RadioButton)findViewById(R.id.radioBuscarXNombre)).setChecked(false);
			((RadioButton)findViewById(R.id.radioBuscarXTodos)).setChecked(false);
			
			
			((TextView)findViewById(R.id.lblOpBusquedaProduc22)).setVisibility(TextView.VISIBLE);
			((EditText)findViewById(R.id.txtbuscarprecios)).setVisibility(EditText.VISIBLE);
			
			OpcionSeleccionada(getString(R.string.txt_part_of_code), InputType.TYPE_CLASS_NUMBER);

		}
		
	}
	
	
	public void OnClickBuscarXNombre(View view){
		
       if(((RadioButton)findViewById(R.id.radioBuscarXNombre)).isChecked()){
			
			
    		((RadioButton)findViewById(R.id.radioBucarXCodigo)).setChecked(false);
			((RadioButton)findViewById(R.id.radioBuscarXTodos)).setChecked(false);
			
			
			((TextView)findViewById(R.id.lblOpBusquedaProduc22)).setVisibility(TextView.VISIBLE);
			((EditText)findViewById(R.id.txtbuscarprecios)).setVisibility(EditText.VISIBLE);
			
			OpcionSeleccionada(getString(R.string.txt_part_of_name), InputType.TYPE_CLASS_TEXT);
       }
	}
	
	
	public void OnClickBuscarXTodos(View view){
		
		 if(((RadioButton)findViewById(R.id.radioBuscarXTodos)).isChecked()){
			 
				((RadioButton)findViewById(R.id.radioBuscarXNombre)).setChecked(false);
				((RadioButton)findViewById(R.id.radioBucarXCodigo)).setChecked(false);
				
				//OpcionSeleccionada("Ingrese Parte:", InputType.TYPE_CLASS_TEXT);
				
				((TextView)findViewById(R.id.lblOpBusquedaProduc22)).setVisibility(TextView.GONE);
				((EditText)findViewById(R.id.txtbuscarprecios)).setVisibility(EditText.GONE);
				
	    }
	}
	
	
	public void cargarProductos() {
	
		
		//btnBusquedaProduc.setEnabled(false);
		OcultarTeclado((EditText)findViewById(R.id.txtbuscarprecios));
		
		EditText txtOpBusquedaProduc = (EditText) findViewById(R.id.txtbuscarprecios);
		String cadBusqueda = txtOpBusquedaProduc.getText().toString().trim();
		Main.codBusqProductos = cadBusqueda;

		Spinner cbListaPrecios = ((Spinner)(findViewById(R.id.cbListasPrecios)));

		String listaPrecios = cbListaPrecios.getItemAtPosition(cbListaPrecios.getSelectedItemPosition()).toString();

		if(((RadioButton)findViewById(R.id.radioBuscarXTodos)).isChecked()){
			listaProducto = DataBaseBO.BuscarProductos2(3, cadBusqueda, listaPrecios);
		}else if (cadBusqueda.equals("")) {

			
			 Toast toast= Toast.makeText(getApplicationContext(), 
					 getString(R.string.txt_toast_search_alert), Toast.LENGTH_SHORT);  
					 toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
					 toast.show();
			txtOpBusquedaProduc.requestFocus();
			
		} else {
		
	
			if(((RadioButton)findViewById(R.id.radioBucarXCodigo)).isChecked()) {
				listaProducto = DataBaseBO.BuscarProductos2(1, cadBusqueda, listaPrecios);

			}else if(((RadioButton)findViewById(R.id.radioBuscarXNombre)).isChecked()){
				listaProducto = DataBaseBO.BuscarProductos2(2, cadBusqueda, listaPrecios);

			}
		}
		  
		if (listaProducto.size() > 0) {
			
			TableLayout table = new TableLayout(this);
			table.setBackgroundColor(Color.WHITE);
			
			String[] cabecera = { getString(R.string.txt_head_code), getString(R.string.txt_head_name), getString(R.string.txt_head_price) };
			Util.Headers(table, cabecera, this);
			
			HorizontalScrollView scroll = (HorizontalScrollView)findViewById(R.id.scrollAgotados);
			scroll.removeAllViews();
			scroll.addView(table);
			
			for (Producto producto : listaProducto) {
				
				TextView textViewAux;
				TableRow fila = new TableRow(this);
				
				textViewAux = new TextView(this);
				textViewAux.setText(producto.codigo);			
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
				//textViewAux.setTextSize(19);
				//textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
				fila.addView(textViewAux);
				
				textViewAux = new TextView(this);
				textViewAux.setText(producto.descripcion);			
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
				//textViewAux.setTextSize(19);
				//textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
				fila.addView(textViewAux);
				
				textViewAux = new TextView(this);
				textViewAux.setText(Util.Redondear(""+producto.precio, 2));			
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
				//textViewAux.setTextSize(19);
				//textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
				fila.addView(textViewAux);
				
			
				table.addView(fila);
			}
		}
	 }


	
	
	
	public void OpcionSeleccionada(String label, int inputType) {
		
		TextView lblOpBusquedaProduc = (TextView)findViewById(R.id.lblOpBusquedaProduc22);
		EditText txtOpBusquedaProduc = (EditText)findViewById(R.id.txtbuscarprecios);
		
		txtOpBusquedaProduc.setText("");
		lblOpBusquedaProduc.setText(label);
		txtOpBusquedaProduc.setInputType(inputType);
		
		//ListViewAdapter adapter = new ListViewAdapter(this, new ItemListView[]{}, R.drawable.producto, 0);
		//ListView listaBusquedaProductos = (ListView)findViewById(R.id.listaBusquedaProductos);
		//listaBusquedaProductos.setAdapter(adapter);
		
		
	}
	
	
	
  public void OcultarTeclado(EditText editText) {
		
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}
	

  public void mostrarHeader(){
	
	    TableLayout table = new TableLayout(this);
		table.setBackgroundColor(Color.WHITE);
		
		String[] cabecera = { "Codigo", "Nombre", "Precio" };
		Util.Headers(table, cabecera, this);
		
		HorizontalScrollView scroll = (HorizontalScrollView)findViewById(R.id.scrollAgotados);
		scroll.removeAllViews();
		scroll.addView(table);
	
  }
  
	public void onClickRegresar (View view){
		finish();
	}
	

	
	
}
