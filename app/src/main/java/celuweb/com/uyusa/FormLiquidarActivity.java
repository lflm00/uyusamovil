package celuweb.com.uyusa;

import java.util.Enumeration;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.MotivoCambio;
import celuweb.com.DataObject.Producto;

public class FormLiquidarActivity extends Activity implements Sincronizador {

	private long mLastClickTime = 0;

	boolean editar;
	Detalle detalleAct;

	boolean setListener = true;

	Producto producto;
	Detalle detalleEdicion;

	Dialog dialogPedido;
	Dialog dialogEditar;
	Dialog dialogResumen;
	ProgressDialog progressDialog;

	//Por defecto lo toma como pedido
	boolean cambio = false;
	String strOpcion;

	Vector<MotivoCambio> listaMotivosCambio;

	private TextView lblTotalItemsPedido;

	private TextView lblSubTotal;
	private TextView lblTotalIva;
	private TextView lblDescuento;
	private TextView lblValorNetoPedido;
	private TextView txtCodigoSocio;
	private TextView txtNombSocio;
	private boolean isCargue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_liquidar);

		Inicializar();
		cargarBundle();
		CargarLiquidacionPedido();
	}

	private void cargarBundle() {
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null && bundle.containsKey("isCargue"))
			isCargue = bundle.getBoolean("isCargue");
		else
			isCargue = false;
		
	}

	public void Inicializar() {

		lblTotalItemsPedido = ((TextView) findViewById(R.id.lblTotalItemsPedido));
		lblSubTotal         = ((TextView) findViewById(R.id.lblSubTotal));
		lblTotalIva         = ((TextView) findViewById(R.id.lblTotalIva));
		lblDescuento        = ((TextView) findViewById(R.id.lblDescuento));
		lblValorNetoPedido  = ((TextView) findViewById(R.id.lblValorNetoPedido));
		txtCodigoSocio      = ((TextView) findViewById(R.id.txtCodigoCliente));
		txtNombSocio        = ((TextView) findViewById(R.id.txtNombreCliente));
	}


	public void OnClickAceptarLiquidacion(View view) {

		FormLiquidarActivity.this.finish();
	}

	public void OnClickTomarFoto(View view) {

		if (Main.detallePedido.size() > 0) {

			Intent intent = new Intent(this, FormFotos.class);
			intent.putExtra("nroDoc", Main.encabezado.numero_doc);
			intent.putExtra("codCliente", Main.cliente.codigo);
			intent.putExtra("codVendedor", Main.usuario.codigoVendedor);

			startActivity(intent);

		} else {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Para tomar Fotos. Por favor primero ingrese los productos.")
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int id) {

					dialog.cancel();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		}
	}

	public void OnClickOpcionesPedido(View view) {

		/*retardo de 1500ms para evitar eventos de doble click.
		 * esto significa que solo se puede hacer click cada 1500ms.
		 * es decir despues de presionar el boton, se debe esperar que transcurran
		 * 1500ms para que se capture un nuevo evento.*/
		if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
			return;
		}
		mLastClickTime = SystemClock.elapsedRealtime();

		int totalItems;
		EditText txtCodigoProducto;

		switch (view.getId()) {

		case R.id.btnAceptarOpcionesPedido:

			txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
			OcultarTeclado(txtCodigoProducto);

			totalItems = Main.detallePedido.size();

			if (totalItems >= Const.MAX_ITEMS) {

				Util.MostrarAlertDialog(this, "Se ha superado el maximo de Productos: " + Const.MAX_ITEMS + ".");

			} else {

				String codigoProducto = txtCodigoProducto.getText().toString().trim();

				if (codigoProducto.equals("")) {

					txtCodigoProducto.setError("Ingrese el codigo");
					txtCodigoProducto.requestFocus();

				} else {


					txtCodigoProducto.setError(null);
				}
			}
			break;

		case R.id.btnBuscarOpcionesPedido:

			totalItems = Main.detallePedido.size();

			if (totalItems >= Const.MAX_ITEMS) {

				Util.MostrarAlertDialog(this, "Se ha superado el maximo de Productos: " + Const.MAX_ITEMS + ".");

			} else {

				((EditText)findViewById(R.id.txtCodigoProducto)).setError(null);
				Intent formBuscarProducto = new Intent(this, FormBuscarProducto.class);
				startActivityForResult(formBuscarProducto, Const.RESP_BUSQUEDA_PRODUCTO);
			}
			break;

			/*case R.id.btnCancelarPedido:

				txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
				OcultarTeclado(txtCodigoProducto);
				txtCodigoProducto.setError(null);
				CancelarPedido();
				break;

			case R.id.btnTerminarPedido:

				txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
				txtCodigoProducto.setError(null);

				OcultarTeclado(txtCodigoProducto);
				ValidarPedido();
				break;*/
		}
	}



	public void CargarLiquidacionPedido() {				

		Enumeration<Detalle> e = Main.detallePedido.elements();

		float subTotal  = 0;
		float valorDesc = 0;
		float valorIva  = 0;
		float valorNeto = 0;

		txtCodigoSocio.setText( Main.cliente.codigo );
		txtNombSocio.setText( Main.cliente.razonSocial );

		while (e.hasMoreElements()) {
			
			float valorProducto = 0;

			Detalle detalle = e.nextElement();	
			
			if (!isCargue) {
				valorProducto = detalle.cantidad * detalle.precio;
			} else {
				
				detalle.factorCajas = DataBaseBO.ObtenerFactorCanastaProducto(detalle.codProducto);
				valorProducto = (detalle.cantidad*detalle.factorCajas) * detalle.precio;
			}
		
			float valor_descuento = valorProducto * (detalle.descuento / 100f);
			float valor_iva       = (valorProducto - valor_descuento) * (detalle.iva / 100f);

			subTotal  += valorProducto;
			valorIva  += valor_iva;
			valorDesc += valor_descuento;			
		}


		valorNeto = subTotal + valorIva - valorDesc;

		lblTotalItemsPedido.setText("" + Main.detallePedido.size());
		lblSubTotal.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + subTotal), 2)));
		lblTotalIva.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorIva), 2)));
		lblDescuento.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorDesc), 2)));
		lblValorNetoPedido.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorNeto),2)));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {

			if ( (requestCode == Const.RESP_BUSQUEDA_PRODUCTO || requestCode == Const.RESP_ULTIMO_PEDIDO) ) {

				if (data != null) {

					Bundle bundle = data.getExtras();
					Object productoSel = bundle.get("producto"); 

					if (productoSel != null && productoSel instanceof Producto) {

						producto = (Producto)productoSel;
						((EditText)findViewById(R.id.txtCodigoProducto)).setText(producto.codigo);


					}

				} else if (requestCode == Const.RESP_ULTIMO_PEDIDO) {

				}

			} else if (requestCode == Const.RESP_FORMAS_DE_PAGO) {


			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {


			return true;
		}

		return super.onKeyDown(keyCode, event);
	}



	public void SetKeyListener() {

		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		txtCodigoProducto.setOnKeyListener(new OnKeyListener() {

			public boolean onKey(View v, int keyCode, KeyEvent event) {

				if (keyCode == KeyEvent.KEYCODE_ENTER) {

					((Button)findViewById(R.id.btnAceptarOpcionesPedido)).requestFocus();
					return true;
				}

				return false;
			}
		});
	}

	public void SetFocusListener() {

		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		txtCodigoProducto.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				if (!hasFocus) {

					/**
					 * Si NO tiene el foco se quita el mensaje de Error.
					 **/
					((EditText)findViewById(R.id.txtCodigoProducto)).setError(null);
				}
			}
		});
	}

	@Override
	public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

		final String mensaje = ok ?  strOpcion + " Registrado con Exito en el servidor" : msg;

		if (progressDialog != null)
			progressDialog.cancel();

		this.runOnUiThread(new Runnable() {

			public void run() {

				AlertDialog.Builder builder = new AlertDialog.Builder(FormLiquidarActivity.this);
				builder.setMessage(mensaje)

				.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {

						Main.encabezado = new Encabezado();
						Main.detallePedido.clear();

						dialog.cancel();

						//OpcionesPedidoActivity.this.setResult(RESULT_OK);
						FormLiquidarActivity.this.finish();
					}
				});

				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}

	public void OcultarTeclado(EditText editText) {

		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	public String ObtenerVersion() {

		String version;

		try {

			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

		} catch (NameNotFoundException e) {

			version = "0.0";
			Log.e("OpcionesPedidoActivity", e.getMessage(), e);
		}

		return version;
	}

	public String ObtenerImei() {

		TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		return manager.getDeviceId();
	}


	public void OnClickVerDescuentos(View view) {

		Intent intent = new Intent(this, FormDescuentosInformacion.class);
		startActivity(intent);


	}

	/**
	 * ver activity de cartera, para consultar la informacion de cartera.        
	 * @param view
	 */
	public void onClickVerCartera(View view) {
		
		int  tipoDeClienteSeleccionado2 = DataBaseBO.ObtenerTipoClienteSeleccionado();

		if(tipoDeClienteSeleccionado2 == 1) {
			Intent intent = new Intent(this, FormCarteraInformacion.class);
			startActivity(intent);
		} else{
			Util.MostrarAlertDialog(this,"Cliente Nuevo no Tiene Cartera");
		}
	}



}
