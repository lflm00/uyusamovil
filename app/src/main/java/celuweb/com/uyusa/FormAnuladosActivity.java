/**
 * 
 */
package celuweb.com.uyusa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TableRow;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;

/**
 * Lista de pedidos y devoluciones anuladas.
 * 
 * @author JICZ4
 *
 */
public class FormAnuladosActivity extends Activity {

    Vector<Encabezado> listaAnulados;
    public long tiempoClick1 = 0;
    Dialog dialogResumen;
    ProgressDialog progressDialog;
    Encabezado encabezadoSeleccionado;

    /**
     * constante para identificar la opcin para mostrar pedidos.
     */
    private static final boolean MOSTRAR_PEDIDOS = true;

    /**
     * constante para identificar la opcin para mostrar devoluciones.
     */
    private static final boolean MOSTRAR_DEVOLUCIONES = false;
	private Hashtable<String, Detalle> detallePedido;


	@Override
    protected void onCreate(Bundle savedInstanceState) {
	setContentView(R.layout.form_lista_anulados);
	super.onCreate(savedInstanceState);

    }



    @Override
    protected void onResume() {

	listarAnulados(true);
	SetListenerListView();

	super.onResume();
    }



    /**
     * Capturar eventos del radioGroup para listar el pedidos o devoluciones.
     * 
     * @param view
     */
    public void onRadioButtonClick(View view) {
	/* Capturar estado del radioButton */
	boolean checked = ((RadioButton) view).isChecked();

	/* identificar radioButton y manjear el evento */
	switch (view.getId()) {
	case R.id.radioPedidos:
	    if (checked)
		listarAnulados(MOSTRAR_PEDIDOS);
	    break;
	case R.id.radioDevoluciones:
	    if (checked)
		listarAnulados(MOSTRAR_DEVOLUCIONES);
	    break;
	}
    }



    /**
     * Alistar anulados segun el tipo a mostrar.
     * 
     * @param mostrarTipo
     */
    private void listarAnulados(boolean mostrarTipo) {

	ItemListView[] listaItems = null;
	int tipo = 0;

	if (!mostrarTipo)
	    tipo = 2;

	Vector<ItemListView> listaItemsCliente = new Vector<ItemListView>();

	listaAnulados = DataBaseBO.CargarListaAnuladas(Main.usuario.fechaConsecutivo, listaItemsCliente, tipo);

	if (listaItemsCliente.size() > 0) {

	    listaItems = new ItemListView[listaItemsCliente.size()];
	    listaItemsCliente.copyInto(listaItems);

	    ListViewAdapter adapter = new ListViewAdapter(this, listaItems, R.drawable.cliente, 0x2E65AD);
	    ListView listaPedidosRealizados = (ListView) findViewById(R.id.listaAnulados);
	    listaPedidosRealizados.setAdapter(adapter);

	} else {

	    ListViewAdapter adapter = new ListViewAdapter(this, new ItemListView[] {}, R.drawable.cliente, 0x2E65AD);
	    ListView listaPedidosRealizados = (ListView) findViewById(R.id.listaAnulados);
	    listaPedidosRealizados.setAdapter(adapter);
	}
    }



    public void SetListenerListView() {

	ListView listaPedidosRealizados = (ListView) findViewById(R.id.listaAnulados);
	listaPedidosRealizados.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	    @Override
	    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		Date date = new Date();

		long tiempoClick2 = date.getTime();

		long dif1 = tiempoClick2 - tiempoClick1;

		if (dif1 < 0) {

		    dif1 = dif1 * (-1);

		}

		if (dif1 > 3000) {

		    tiempoClick1 = tiempoClick2;

		    Encabezado encabezado = listaAnulados.elementAt(position);
		    encabezadoSeleccionado = encabezado;
		    MostarDialogResumen(encabezado);

		}

	    }
	});
    }

	public boolean validarHoraMaximaEnvio() throws ParseException {

		String pattern = "HH:mm";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String time=Util.FechaActual("HH:mm");
		String endtime=DataBaseBO.ObtenerHoraMaximoEnvio();
		try {
			Date date1 = sdf.parse(time);
			Date date2 = sdf.parse(endtime);

			if(date1.before(date2)) {
				return true;
			} else {

				return false;
			}
		} catch (ParseException e){
			e.printStackTrace();
		}
		return false;
	}

    public void MostarDialogResumen(final Encabezado encabezado) {

	dialogResumen = new Dialog(this);
	dialogResumen.requestWindowFeature(Window.FEATURE_LEFT_ICON);
	dialogResumen.setContentView(R.layout.resumen_pedido);

	((Button) dialogResumen.findViewById(R.id.buttonAnular)).setVisibility(View.GONE);
	((Button) dialogResumen.findViewById(R.id.btnImprimir)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.carrito, 0, 0, 0);

	if (encabezado.tipoTrans == 0)
	    ((Button) dialogResumen.findViewById(R.id.btnImprimir)).setText("N. Pedido");
	else
	    ((Button) dialogResumen.findViewById(R.id.btnImprimir)).setVisibility(View.GONE);
	;

	String titulo = (encabezado.tipoTrans == 2) ? "Devolucion Anulada" : "Venta Anulada";
	dialogResumen.setTitle("Resumen " + titulo);

	encabezado.sub_total = 0;
	encabezado.total_iva = 0;
	encabezado.valor_descuento = 0;

	ItemListView itemListView;
	detallePedido = DataBaseBO.CargarDetallePedido(encabezado.numero_doc);

	Enumeration<Detalle> e = detallePedido.elements();
	Vector<ItemListView> datosPedido = new Vector<ItemListView>();

	while (e.hasMoreElements()) {

	    Detalle detalle = e.nextElement();
	    itemListView = new ItemListView();

	    float sub_total = detalle.cantidad * detalle.precio;
	    float valor_descuento = sub_total * detalle.descuento / 100;
	    float valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100);

	    encabezado.sub_total += sub_total;
	    encabezado.total_iva += valor_iva;
	    encabezado.valor_descuento += valor_descuento;

	    if (encabezado.tipoTrans == 0) {

			if (detalle.cantidadCambio>0 && detalle.cantidad>0){
				itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
				itemListView.subTitulo = "Precio: " + detalle.precio + " - Iva: " + detalle.iva + "% - Cant: " + detalle.cantidad + " - Desc: "
						+ detalle.descuento + "% \nCantDevolucion: "+detalle.cantidadCambio;
				datosPedido.addElement(itemListView);
			}else if (detalle.cantidadCambio>0 && detalle.cantidad==0){
				itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
				itemListView.subTitulo = "Precio: " + detalle.precio + " - Iva: " + detalle.iva + "%  \nCantDevolucion: "+detalle.cantidadCambio;
				datosPedido.addElement(itemListView);
			}else {
				itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
				itemListView.subTitulo = "Precio: " + detalle.precio + " - Iva: " + detalle.iva + "% - Cant: " + detalle.cantidad + " - Desc: "
						+ detalle.descuento + "% ";
				datosPedido.addElement(itemListView);
			}

	    } else {

		if (encabezado.tipoTrans == 2) {

		    itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
		    itemListView.subTitulo = "Cantidad Devolucion: " + detalle.cantidad + " - Motivo: " + DataBaseBO.obtenerMotivo(detalle.motivo);
		    datosPedido.addElement(itemListView);

		}
	    }
	}

	ItemListView[] listaItems = new ItemListView[datosPedido.size()];
	datosPedido.copyInto(listaItems);

	encabezado.valor_neto = encabezado.sub_total + encabezado.total_iva - encabezado.valor_descuento;
	encabezado.str_valor_neto = Util.SepararMiles(Util.Redondear(Util.QuitarE("" + encabezado.valor_neto), 2));

	((TextView) dialogResumen.findViewById(R.id.lblCliente)).setText(encabezado.nombre_cliente);

	if (encabezado.tipoTrans == 0) {
	    ((TextView) dialogResumen.findViewById(R.id.lblValorNetoPedido)).setText(encabezado.str_valor_neto);
	    ((TableRow) dialogResumen.findViewById(R.id.rowTotal)).setVisibility(View.VISIBLE);
	} else if (encabezado.tipoTrans == 2)
	    ((TableRow) dialogResumen.findViewById(R.id.rowTotal)).setVisibility(View.GONE);

	ListViewAdapterPedido adapter = new ListViewAdapterPedido(this, listaItems, R.drawable.compra, 0);
	ListView listaResumenPedido = (ListView) dialogResumen.findViewById(R.id.listaResumenPedido);
	listaResumenPedido.setAdapter(adapter);

	((Button) dialogResumen.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

	    public void onClick(View v) {

		dialogResumen.cancel();
	    }
	});

	((Button) dialogResumen.findViewById(R.id.btnImprimir)).setOnClickListener(new OnClickListener() {

	    public void onClick(View view) {

			try {
				if (validarHoraMaximaEnvio()) {

				if (!DataBaseBO.existeRegistroLiquidacion()) {

					Main.cliente = DataBaseBO.CargarCliente(encabezado.codigo_cliente);

					if (Main.cliente == null) {

						return;
					}

					if (Main.cliente.codigo == null) {

						return;
					}

					if (Main.cliente.codigo.equals("")) {

						return;
					}


					DataBaseBO.organizarTmpDescuentos();
//			DataBaseBO.organizarTmpProductos( cliente.portafolio);


					if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
						DataBaseBO.organizarTmpProductos(Main.cliente.portafolio, Const.AUTOVENTA);
					} else {
						DataBaseBO.organizarTmpProductos(Main.cliente.portafolio, Const.PREVENTA);
					}

					DataBaseBO.GuardarCodPdv(encabezado.codigo_cliente, 1);
					DataBaseBO.ListarProductos(Main.cliente.CodigoAmarre);

					if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {

						if (encabezado.tipoTrans == 0) {

			    /*
			     * Intent formPedidos = new Intent(FormAnuladosActivity.this,
			     * FormPedidoActivity.class); formPedidos.putExtra("encabezadoAnulado",
			     * encabezado); formPedidos.putExtra("cambio", false);
			     * formPedidos.putExtra("pedido", true); startActivity(formPedidos);
			     * dialogResumen.cancel();
			     */

							DataBaseBO.crearVistaProductosAutoventa();

							progressDialog = ProgressDialog.show(FormAnuladosActivity.this, "", "Cargando Productos...", true);
							progressDialog.show();

							CargarProductos cp = new CargarProductos();
							cp.start();

							dialogResumen.cancel();

						} else if (encabezado.tipoTrans == 2) {

							Util.mostrarToast(FormAnuladosActivity.this, "Antes de crear una devolucion, es necesario tener un pedido amarrado.");

			    /*
			     * Intent formPedidos = new Intent(FormAnuladosActivity.this,
			     * FormDevolucionActivity.class);
			     * formPedidos.putExtra("encabezadoAnulado", encabezado);
			     * formPedidos.putExtra("cambio", true); startActivity(formPedidos);
			     * dialogResumen.cancel();
			     */

						}

					} else {

						if (encabezado.tipoTrans == 0) {

			    /*
			     * Intent formPedidos = new Intent(FormAnuladosActivity.this,
			     * FormPedidoActivity.class); formPedidos.putExtra("encabezadoAnulado",
			     * encabezado); formPedidos.putExtra("cambio", false);
			     * formPedidos.putExtra("pedido", true); startActivity(formPedidos);
			     * dialogResumen.cancel();
			     */

							progressDialog = ProgressDialog.show(FormAnuladosActivity.this, "", "Cargando Productos...", true);
							progressDialog.show();

							CargarProductos cp = new CargarProductos();
							cp.start();

							dialogResumen.cancel();

						} else if (encabezado.tipoTrans == 2) {

			    /*
			     * Intent formPedidos = new Intent(FormAnuladosActivity.this,
			     * FormDevolucionActivity.class);
			     * formPedidos.putExtra("encabezadoAnulado", encabezado);
			     * formPedidos.putExtra("cambio", true); startActivity(formPedidos);
			     * dialogResumen.cancel();
			     */

							progressDialog = ProgressDialog.show(FormAnuladosActivity.this, "", "Cargando Productos...", true);
							progressDialog.show();

							CargarProductos2 cp = new CargarProductos2();
							cp.start();

							dialogResumen.cancel();

						}

					}

				} else {
					Util.MostrarAlertDialog(FormAnuladosActivity.this, "Ya ha sido realizada la liquidacion.");
				}

				} else {
					Util.MostrarAlertDialog(FormAnuladosActivity.this, "Esta Fuera De La Hora Permitida Para Tomar Pedidos, Porfavor Termine Labores");
				}
			} catch (ParseException e) {
				e.printStackTrace();
				Log.d("***", e.getMessage());
			}
		}
	});

	dialogResumen.setCancelable(false);
	dialogResumen.show();
	dialogResumen.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.cliente);
    }



    public void onClickRegresar(View view) {

	finish();
    }

    class CargarProductos extends Thread {

	public void run() {

	    cargarProductos();
	}
    }



    public void cargarProductos() {

	Main.encabezado.codigo_novedad = 1;
	Main.encabezado.tipoTrans = 0;
	Main.itemsBusq = new ItemListView[] {};
	Main.codBusqProductos = "";
	Main.posOpBusqProductos = 0;
	Main.posOpLineas = 0;
	Main.primeraVez = true;

	if (Main.usuario == null)
	    DataBaseBO.CargarInfomacionUsuario();

	Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();

	DataBaseBO.organizarTmpDescuentos();

	if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
	    DataBaseBO.organizarTmpProductos(Main.cliente.portafolio, Const.AUTOVENTA);
	} else {
	    DataBaseBO.organizarTmpProductos(Main.cliente.portafolio, Const.PREVENTA);
	}

	DataBaseBO.ListarProductos(Main.cliente.CodigoAmarre);

	if (progressDialog != null)
	    progressDialog.dismiss();

	if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {

	    Intent formPedidos = new Intent(FormAnuladosActivity.this, FormPedidoActivity.class);
	    formPedidos.putExtra("encabezadoAnulado", encabezadoSeleccionado);
	    formPedidos.putExtra("cambio", false);
	    formPedidos.putExtra("pedido", true);
	    startActivity(formPedidos);
	    if (dialogResumen != null)
		dialogResumen.cancel();

	} else {

	    Intent formPedidos = new Intent(FormAnuladosActivity.this, FormPedidoActivity.class);
	    formPedidos.putExtra("encabezadoAnulado", encabezadoSeleccionado);
	    formPedidos.putExtra("cambio", false);
	    formPedidos.putExtra("pedido", true);
	    startActivity(formPedidos);
	    if (dialogResumen != null)
		dialogResumen.cancel();

	}

    }

    class CargarProductos2 extends Thread {

	public void run() {

	    cargarProductos2();
	}
    }



    public void cargarProductos2() {

	Main.encabezado.codigo_novedad = 10;
	Main.encabezado.tipoTrans = 2;

	Main.itemsBusq = new ItemListView[] {};

	Main.codBusqProductos = "";
	Main.posOpBusqProductos = 0;
	Main.posOpLineas = 0;
	Main.primeraVez = true;

	if (Main.usuario == null)
	    DataBaseBO.CargarInfomacionUsuario();

	Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();

	DataBaseBO.organizarTmpDescuentos();
	DataBaseBO.organizarTmpProductos(Main.cliente.portafolio);
	DataBaseBO.ListarProductos(Main.cliente.CodigoAmarre);

	if (progressDialog != null)
	    progressDialog.dismiss();

	Intent formPedidos = new Intent(FormAnuladosActivity.this, FormDevolucionActivity.class);
	formPedidos.putExtra("encabezadoAnulado", encabezadoSeleccionado);
	formPedidos.putExtra("cambio", true);
	formPedidos.putExtra("pedido", false);
	startActivity(formPedidos);
	if (dialogResumen != null)
	    dialogResumen.cancel();
    }

}