package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Filtro;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.Usuario;

public class FormFiltroPedidoActivity extends Activity implements OnClickListener{

	AlertDialog alertDialog;
	Vector<Producto> listaProductos;
	Vector<Filtro> listaFamilia;
	Vector<Filtro> listaClase;
	Filtro filtroFamilia;
	Filtro filtroClase;
	Producto productoSel;
	int positionFamilia;
	int positionClase;
	int posDescClase2;
	int positionProducto;
	int posCodProducto;
	boolean primeraVez=true;
	boolean isAutoVenta = false;
	int tipoTransaccion = -1;
	boolean isPedido = false;
	boolean isCargue = false;
	boolean isProductoDanado = false;
	ListViewAdapterFiltroPedido adapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_filtro_pedido);
		Util.closeTecladoStartActivity(this);
		
//		cargarFamilia();
		cargarTipoClienteActual();
		cargarBundle();
		
		
		SharedPreferences settings = getSharedPreferences("MisPreferenciasFiltro", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		int posDescFamilia = settings.getInt("posFamiliaSel", -1);
		posDescClase2 = settings.getInt("posClaseSel", -1);
		posCodProducto= settings.getInt("posProductoSel", -1);
		
		
			if(posCodProducto != -1 ){
				cargarProductoAnt(posDescFamilia,  posCodProducto);
			}else{
				cargarFamilia();
			}
		
		SetListenerListView();
	}
	
	
	
	private void cargarBundle() {

		Bundle bundle = getIntent().getExtras();
		
		if(bundle != null && bundle.containsKey("tipoTransaccion"))
			tipoTransaccion = bundle.getInt("tipoTransaccion");
		
		if (bundle != null && bundle.containsKey("isCargue"))
			isCargue = bundle.getBoolean("isCargue");
		
		if (bundle != null && bundle.containsKey("isPedido"))
			isPedido = bundle.getBoolean("isPedido");
		
		
		System.out.println("TipoTransaccion --> "+tipoTransaccion);
		System.out.println("?isCargue? --> "+isCargue);
		System.out.println("?isPedido? --> "+isPedido);
		
		
		/*
		 * Facturas, Averias, Toma Inventario  mostrar solo con invnetario
			Devoluciones, Cargues, Danados    mostrar todo
		 */
		
	}



	private void cargarTipoClienteActual() {
		
		Usuario usuario = DataBaseBO.ObtenerUsuario();
		if(usuario.tipoVenta.equals(Const.AUTOVENTA))
			isAutoVenta = true;
	}



	@Override
	protected void onResume() {
		
		pasarGarbageCollector();
		
		super.onResume();
		
	}



	private void cargarProductoAnt(int descFamilia, int codProducto) {
		
		cargarFamilia();

		if (descFamilia != -1)
			((Spinner) findViewById(R.id.spListaFamilia)).setSelection(descFamilia);
		
	}

	private void cargarFamilia() {
		
		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		
		listaFamilia = DataBaseBO.listaFiltroFamilia(listaItems );

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {
			
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinnerFamilia = (Spinner)findViewById(R.id.spListaFamilia);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerFamilia.setAdapter(adapter);

		spinnerFamilia.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
					
				positionFamilia = position;
				
				filtroFamilia =listaFamilia.elementAt(position);
					String where = "";
					
					if(!filtroFamilia.filtroDescripcion.toString().equals("TODOS")){
						where = "WHERE l.linea = '"+filtroFamilia.filtroCodigo+"' ";
					}
					
					//cargaListaClase(where);
				cargarProductos2(where);
				
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	protected void cargaListaClase(final String filtroFamilia) {
		
		
		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		listaClase= DataBaseBO.listaFiltroClase(listaItems,filtroFamilia);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
			((EditText)findViewById(R.id.txtDescProducto)).setText("");
		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinnerClase = (Spinner)findViewById(R.id.spListaClase);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerClase.setAdapter(adapter);
		
		
		spinnerClase.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				
//				if(position!=0){	
				
					String filtro = "";
					filtro = listaClase.elementAt(position).filtroDescripcion;
				
				positionClase =position;
				
					filtroClase = listaClase.elementAt(position);
					String where = "";
					if(!filtroClase.filtroDescripcion.toString().equals("TODOS")){
						
						where = " where categoria = '"+filtroClase.filtroDescripcion+"' ";
					}else{
						
						if(!filtroFamilia.toString().equals("TODOS"))
						where = filtroFamilia;
						
					}
					cargarProductos2(where);
//				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		if(primeraVez && posCodProducto != -1){
			
			if ((posDescClase2!= -1)) {
				((Spinner)findViewById(R.id.spListaClase)).setSelection(posDescClase2);
			}
			
		}

		
	}

	public void cargarProductos2(String filtro){
	ItemListView[] items;
	Vector<ItemListView> listaItems = new Vector<ItemListView>();
	
	String and = "";
	if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)){
		
		if(tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION || tipoTransaccion == Const.IS_AVERIA_TRANSPORTE || isPedido){
			and = " AND saldo > 0 ";
		}
		
	}
	
	listaProductos = DataBaseBO.listaProducto(filtro, listaItems, isAutoVenta, and);

	
	if (listaItems.size() > 0) {

		items = new ItemListView[listaItems.size()];
		listaItems.copyInto(items);
		((EditText)findViewById(R.id.txtDescProducto)).setText("");

	} else {

		items = new ItemListView[] {};
		
		if (listaProductos != null)
			listaProductos.removeAllElements();

		((EditText)findViewById(R.id.txtDescProducto)).setText("");
		Toast.makeText(getApplicationContext(), "Busqueda sin resultados", Toast.LENGTH_SHORT).show();
	}
	
	System.out.println("Precarga el producto seleccionado");
	Main.listaProductos = listaProductos;
	
	adapter = new ListViewAdapterFiltroPedido(this, items, R.drawable.compra, 0);
	ListView listaBusquedaProductos = (ListView) findViewById(R.id.listaHistorialPedidos);
	listaBusquedaProductos.setAdapter(adapter);
	
	if(primeraVez && posCodProducto!= -1){
		
		primeraVez=false;
		listaBusquedaProductos.performItemClick(listaBusquedaProductos.getAdapter().getView(posCodProducto, null, null), 
				posCodProducto, listaBusquedaProductos.getAdapter().getItemId(posCodProducto));
		
		listaBusquedaProductos.getAdapter().getView(posCodProducto, null, null).setSelected(true);
		listaBusquedaProductos.smoothScrollToPosition(posCodProducto);

		positionProducto =posCodProducto;
		
		
		listaBusquedaProductos.invalidateViews();
		
	}
	
	}

	public void SetListenerListView() {
		
		ListView listaProduc = (ListView)findViewById(R.id.listaHistorialPedidos);
		listaProduc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				view.setSelected(true);
				
//				listaBusquedaProductos.getAdapter().getView(posCodProducto, null, null).setSelected(true);
				adapter.seleccionado = position;
				adapter.notifyDataSetChanged();
				
				positionProducto =position;
				productoSel = listaProductos.elementAt(position);
				
				if (productoSel != null)
					((EditText)findViewById(R.id.txtDescProducto)).setText(productoSel.descripcion.toString());
				
				
			}
        });
	}
	
	public void onClickCancelar(View view) {
		
		SharedPreferences settings = getSharedPreferences("MisPreferenciasFiltro", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("posProductoSel", -1);
		editor.commit();
		finish();
		
		
	}
	
	
	
	public void onClickAceptar(View view){
		
		if(productoSel!=null ){
			
			SharedPreferences settings = getSharedPreferences("MisPreferenciasFiltro", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = settings.edit();
			editor.putInt("posFamiliaSel", positionFamilia);
			editor.putInt("posClaseSel", positionClase);
			editor.putInt("posProductoSel", positionProducto);
			editor.commit();
			
			Intent data =  new Intent();
        	data.putExtra("producto", productoSel);
        	setResult(RESULT_OK, data);
        	FormFiltroPedidoActivity.this.finish();
			
		}else{
			Util.mostrarToast(this, "Seleccione un producto");
		}
		
	}

	

	
	public void MostrarAlertDialog(String mensaje) {
    	
    	if (alertDialog == null) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			alertDialog = builder.create();
    	}
    	
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
    }
	
	public void pasarGarbageCollector(){
		 
        Runtime garbage = Runtime.getRuntime();
        System.out.println("Memoria libre antes de limpieza: "+ garbage.freeMemory());
 
        garbage.gc();
 
        System.out.println("Memoria libre tras la limpieza: " +garbage.freeMemory());
    }
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		
		if (requestCode == Const.RESP_FILTRO_PRODUCTO) {

			if (resultCode == RESULT_OK) {

				System.out.println("Entr? al onActivityResult");
				cargarFamilia();
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		
		SharedPreferences settings = getSharedPreferences("MisPreferenciasFiltro", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("posProductoSel", -1);
		editor.commit();
		finish();
	}
	
	public void onClickRegresar(View view){
		
		SharedPreferences settings = getSharedPreferences("MisPreferenciasFiltro", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("posProductoSel", -1);
		editor.commit();
		finish();
	}
}
