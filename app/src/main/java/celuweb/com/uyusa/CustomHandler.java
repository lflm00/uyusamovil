package celuweb.com.uyusa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;

public class CustomHandler {

	private static Activity context;
	private static String message;
	private static boolean finish;
	
	public static void sendEmptyMessage(Activity context, String message, boolean finish) {
    	
    	CustomHandler.context = context;
    	CustomHandler.message = message;
    	CustomHandler.finish = finish;
    	handlerMsg.sendEmptyMessage(0);
    }
    
    private static Handler handlerMsg = new Handler() {
		
    	@Override
		public void handleMessage(Message msg) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
					
					 if (finish) {
						 
						 //Se debe finalizar la Actividad que llamo el Handler!
						 context.finish();
					 }
				}
			});

			AlertDialog alertDialog = builder.create();
	    	alertDialog.setMessage(message);
	    	alertDialog.show();
		}
    };
    
    /*private static Handler handlerConfigOK = new Handler() {
		
    	@Override
		public void handleMessage(Message msg) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(FormPrinters.this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
					finish();
				}
			});

			AlertDialog alertDialog = builder.create();
	    	alertDialog.setMessage(message);
	    	alertDialog.show();
		}
    };
    
    private Handler handlerError = new Handler() {
		
		@Override
		public void handleMessage(Message msg) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(FormPrinters.this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});

			AlertDialog alertDialog = builder.create();
	    	alertDialog.setMessage(message);
	    	alertDialog.show();
		}
    };*/
}
