package celuweb.com.uyusa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.DataObject.Bonificaciones;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.DatePickerFragment;
import celuweb.com.DataObject.DateSetFechaListener;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.MotivoCambio;
import celuweb.com.DataObject.PedidoAmarre;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.Sucursal;
import celuweb.com.DataObject.Usuario;
import celuweb.com.UI.OnClickListenerImpl;
import celuweb.com.printer.sewoo.SewooLKP20;

@SuppressLint("NewApi")
public class FormDevolucionActivity extends Activity implements Sincronizador {

    private long mLastClickTime = 0;
    boolean primerEjecucion = true;
    MotivoCambio motivoCambioSel;
    Encabezado encabezadoAnulado;

    Vector<Bonificaciones> listaBonificaciones;

    boolean primeraVez = true;
    String[] codProductos;

    String codigoProductoAnterior = "";

    Cliente cliente;
    int tipoDeClienteSeleccionado = 1;
    Dialog dialogPedidoRegistrado;
    private String macImpresora;
    boolean devoluciones = false;
    String mensaje;

    protected int _splashTime = 500;
    protected int _splashTimeCopy = 3000;
    Thread splashTread;
    boolean errorBlue = false;
    private static Activity context;
    private static String TAG = FormInformeInventario.class.getName();

    private static String fechaEntrega = "";
    private EditText edFechaEntrega;

    boolean editar;
    Detalle detalleAct;

    boolean setListener = true;

    Producto producto;
    Detalle detalleEdicion;

    Dialog dialogPedido;
    Dialog dialogEditar;
    Dialog dialogResumen;
    Dialog dialogFechaEntrega;
    ProgressDialog progressDialog;

    String valorTotal = "";

    private static final String BS_PACKAGE = "com.google.zxing.client.android";
    boolean primerBarCode = true;
    Dialog dialogEscanear;
    EditText etCodigoEAN;

    /**
     * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
     */
    private SewooLKP20 sewooLKP20;

    // Por defecto lo toma como pedido
    boolean cambio = false;
    String strOpcion;

    Vector<MotivoCambio> listaMotivosCambio;

    private TextView lblTotalItems;
    private TextView lblTotalItemsPedido;

    private TextView txtCodProducto;
    private TextView txtNombProducto;
    private TextView txtInvProducto;
    private TextView txtPrecioProducto;
    private TextView txtIvaProducto;
    private TextView txtDescProducto;
    private TextView txtPrecioIvaProducto;
    private TextView txtCantidadProductoDevolucion;
    private TextView lblPosNavegador;

    public int posNavegadorProductos = 0;
    public String msjPosNavegador;

    public boolean cambioImgDesc = false;

    public boolean fueUlPedido = false;

    public Spinner spMotivosCambios, spSucursal;

    boolean isPedidoAnulado = false;
    PedidoAmarre pedidoAmarre = null;
    private Vector<Sucursal> listaSucursal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.form_pedido);

        cargarBundle();

        Inicializar();
        InicializarDatos();

        cargarCodigosProductos();
        //Util.turnOnBluetooth();
        Util.closeTecladoStartActivity(this);

        txtNombProducto.requestFocus();

        txtCodProducto.setOnFocusChangeListener(new OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    if (!primeraVez) {

                        txtCodProducto.setText("");

                    } else {

                        primeraVez = false;
                        txtCantidadProductoDevolucion.requestFocus();
                    }

                } else {

                }

            }
        });

        //txtCodProducto.addTextChangedListener(textWatcher);

        //((EditText) findViewById(R.id.txtCantidadProducto)).addTextChangedListener(textWatcherCantidad);
        ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).addTextChangedListener(textWatcherCantidadDev);

        if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {

            ((Button) findViewById(R.id.btnCapturarCodigoQR)).setVisibility(Button.GONE);

        }

        SharedPreferences settings = getSharedPreferences("MisPreferenciasFiltro", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("posFamiliaSel", -1);
        editor.putInt("posClaseSel", -1);
        editor.putInt("posProductoSel", -1);
        editor.commit();

    }


    private void cargarBundle() {

        // identificar si se inicia la activity desde devoluciones anuladas
        Bundle bundle = getIntent().getExtras();

        Usuario usuario = DataBaseBO.ObtenerUsuario();

        encabezadoAnulado = (Encabezado) bundle.getSerializable("encabezadoAnulado");

        if (encabezadoAnulado != null) {
            System.out.println("Entr� a Devolucion Anulado");
            isPedidoAnulado = true;
        }
        // cargar los detalles de la devolucion.
        if (isPedidoAnulado) {
            cargarListaDetallesPedidos(encabezadoAnulado);
        }

        if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            pedidoAmarre = (PedidoAmarre) bundle.getSerializable("pedidoAmarre");

            if (pedidoAmarre != null)
                System.out.println(
                        "Carga de ProdcutoAMarre\nCodi: " + pedidoAmarre.codigo_cliente + "\nNroDoc: " + pedidoAmarre.numero_doc + "\nValorTotal: " + pedidoAmarre.valorMonto);

            System.out.println("M�dulo Devoluci�n\nNroDoc Amarre ---> " + pedidoAmarre.numero_doc);

        }

    }


    /**
     * cargar los detalles del pedido que se pretenden modificar.
     */
    private void cargarListaDetallesPedidos(Encabezado encabezado) {
        Main.detallePedido = new Hashtable<String, Detalle>();
        ArrayList<Detalle> listaDetalles = DataBaseBO.cargarDetallesDePedido(encabezado.numero_doc, false);
        for (Detalle detalle : listaDetalles) {
            // se debe capturar el indice el producto. buscar en el vector
            // Main.navegadorProductos el producto por codigo tal, y obtener su
            // indice para asignarlo al detalle.
            for (Producto p : Main.navegadorProductos) {
                if (p.codigo.equals(detalle.codProducto)) {
                    detalle.indice = p.indice;
                    break;
                }
            }
            Main.detallePedido.put(detalle.codProducto, detalle);
        }

    }


    public void Inicializar() {

        fueUlPedido = false;

        posNavegadorProductos = 0;

        txtCodProducto = ((TextView) findViewById(R.id.txtCodigoProducto));
        txtNombProducto = ((TextView) findViewById(R.id.txtNombreProducto));
        txtInvProducto = ((TextView) findViewById(R.id.txtInvProducto));
        txtPrecioProducto = ((TextView) findViewById(R.id.txtPrecioProducto));
        txtIvaProducto = ((TextView) findViewById(R.id.txtIvaProducto));
        txtDescProducto = ((TextView) findViewById(R.id.txtDescProducto));
        txtPrecioIvaProducto = ((TextView) findViewById(R.id.txtPrecioIvaProducto));
        txtCantidadProductoDevolucion = ((TextView) findViewById(R.id.txtCantidadProductoDevolucion));

        lblPosNavegador = ((TextView) findViewById(R.id.lalPosicionNavegado));

        if (Main.navegadorProductos.size() > 0) {

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);

            txtCodProducto.setText(producto.codigo);
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inventario));
            txtPrecioProducto.setText(Util.SepararMiles(String.valueOf(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles( Util.Redondear(String.valueOf(
            // producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f)) * (1 + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            txtCantidadProductoDevolucion.setText("");

            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            cambioImgDesc = false;
            // cambiarImagenDescAutorizado();
        } else {

            txtCodProducto.setText("");
            txtNombProducto.setText("");
            txtInvProducto.setText("");
            txtPrecioProducto.setText("");
            txtIvaProducto.setText("");
            txtDescProducto.setText("");
            txtPrecioIvaProducto.setText("");
            txtCantidadProductoDevolucion.setText("");

            lblPosNavegador.setText("");

            cambioImgDesc = false;
            // cambiarImagenDescAutorizado();
        }
    }


    public void mostrarActualProducto(String cantidad) {

        if (Main.navegadorProductos.size() > 0) {

            if (posNavegadorProductos >= Main.navegadorProductos.size()) {

                posNavegadorProductos = 0;
            }

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);

            txtCodProducto.setText(producto.codigo);
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inventario));
            txtPrecioProducto.setText(Util.SepararMiles(String.valueOf(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles( Util.Redondear(String.valueOf(
            // producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f)) * (1 + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            txtCantidadProductoDevolucion.setText(cantidad);

            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }


    public void mostrarSigProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {

            posNavegadorProductos++;

            if (posNavegadorProductos >= Main.navegadorProductos.size()) {

                posNavegadorProductos = 0;
            }

            txtNombProducto.requestFocus();
            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);

            txtCodProducto.setText(producto.codigo);
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inventario));
            txtPrecioProducto.setText(Util.SepararMiles(String.valueOf(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles( Util.Redondear(String.valueOf(
            // producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f)) * (1 + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            txtCantidadProductoDevolucion.setText("");
            txtCantidadProductoDevolucion.requestFocus();
            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }


    public void mostrarAntProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {

            posNavegadorProductos--;

            if (posNavegadorProductos < 0) {

                posNavegadorProductos = Main.navegadorProductos.size() - 1;
            }

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);

            txtCodProducto.setText(producto.codigo);
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inventario));
            txtPrecioProducto.setText(Util.SepararMiles(String.valueOf(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles( Util.Redondear(String.valueOf(
            // producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f)) * (1 + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            txtCantidadProductoDevolucion.setText("");
            txtCantidadProductoDevolucion.requestFocus();
            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }


    public void mostrarPrimerProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {

            txtNombProducto.requestFocus();
            posNavegadorProductos = 0;

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);

            txtCodProducto.setText(producto.codigo);
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inventario));
            txtPrecioProducto.setText(Util.SepararMiles(String.valueOf(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles( Util.Redondear(String.valueOf(
            // producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f)) * (1 + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            txtCantidadProductoDevolucion.setText("");
            txtCantidadProductoDevolucion.requestFocus();

            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }


    public void mostrarUltimoProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {

            txtNombProducto.requestFocus();
            posNavegadorProductos = Main.navegadorProductos.size() - 1;

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);

            txtCodProducto.setText(producto.codigo);
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inventario));
            txtPrecioProducto.setText(Util.SepararMiles(String.valueOf(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles( Util.Redondear(String.valueOf(
            // producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f)) * (1 + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            txtCantidadProductoDevolucion.setText("");
            txtCantidadProductoDevolucion.requestFocus();
            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }


    public void InicializarDatos() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("cambio"))
            cambio = bundle.getBoolean("cambio");

        strOpcion = cambio ? getString(R.string.devolucion) : getString(R.string.pedidos);
        setTitle("Registrar " + strOpcion);

        if (cambio) {

            ((TextView) findViewById(R.id.tvHeader)).setText(getString(R.string.devolucion));

            LinearLayout lytAccionesPedido = (LinearLayout) findViewById(R.id.lytAccionesPedido);


            LayoutParams parameter = (LinearLayout.LayoutParams) lytAccionesPedido.getLayoutParams();
            parameter.setMargins(parameter.leftMargin, 40, parameter.rightMargin, parameter.bottomMargin);
            lytAccionesPedido.setLayoutParams(parameter);


            TableLayout tblyPedidos = (TableLayout) findViewById(R.id.tblyPedidos);

            tblyPedidos.setVisibility(TableLayout.GONE);

            spMotivosCambios = (Spinner) findViewById(R.id.spMotivosDeCambio);

            String[] items;
            Vector<String> listaItems = new Vector<String>();
            listaMotivosCambio = DataBaseBO.ListaMotivosCambio(listaItems);

            if (listaItems.size() > 0) {

                items = new String[listaItems.size()];
                listaItems.copyInto(items);

            } else {

                items = new String[]{};
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spMotivosCambios.setAdapter(adapter);

        } else {

            TableLayout tblyDevoluciones = (TableLayout) findViewById(R.id.tblyCambios);

            tblyDevoluciones.setVisibility(TableLayout.GONE);

        }

        if (Main.detallePedido == null)
            Main.detallePedido = new Hashtable<String, Detalle>();
        else
            CargarListaPedido();

        if (setListener) {

            SetKeyListener();
            SetFocusListener();
            SetListenerListView();
            setListener = false;
        }

        if (cambio) {

            ((LinearLayout) (findViewById(R.id.lytUltimoPedido))).setVisibility(Button.GONE);
            ((LinearLayout) (findViewById(R.id.lyBotones))).setVisibility(Button.GONE);
            ((LinearLayout) (findViewById(R.id.lyInventario))).setVisibility(Button.GONE);
            ((LinearLayout) (findViewById(R.id.lyMargen))).setVisibility(Button.GONE);
            ((TableLayout) (findViewById(R.id.tlObservacion))).setVisibility(Button.GONE);


//	    ((ImageView) (findViewById(R.id.btnUltimoPedido))).setVisibility(Button.GONE);
            // ((Button)(findViewById(R.id.btnLiquidar))).setVisibility(Button.GONE);

        }

    }

    public void OnClickTerminarPedido(View view) {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        txtCodigoProducto.setError(null);

        OcultarTeclado(txtCodigoProducto);
        ValidarPedido();
    }


    public void OnClickCancelarPedido(View view) {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        OcultarTeclado(txtCodigoProducto);
        txtCodigoProducto.setError(null);
        CancelarPedido();
    }


    public void OnClickLiquidarPedido(View view) {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        OcultarTeclado(txtCodigoProducto);
        txtCodigoProducto.setError(null);

        Intent formLiquidar = new Intent(this, FormLiquidarActivity.class);
        startActivityForResult(formLiquidar, Const.RESP_PEDIDO_EXITOSO);
    }


    public void OnClickTomarFoto(View view) {

        if (Main.detallePedido.size() > 0) {

            Intent intent = new Intent(this, FormFotos.class);
            intent.putExtra("nroDoc", Main.encabezado.numero_doc);
            intent.putExtra("codCliente", Main.cliente.codigo);
            intent.putExtra("codVendedor", Main.usuario.codigoVendedor);

            startActivity(intent);

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Para tomar Fotos. Por favor primero ingrese los productos.").setCancelable(false).setPositiveButton("Aceptar",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }


    public void OnClickOpcionesPedido(View view) {
        //
        // /*retardo de 1500ms para evitar eventos de doble click.
        // * esto significa que solo se puede hacer click cada 1500ms.
        // * es decir despues de presionar el boton, se debe esperar que transcurran
        // * 1500ms para que se capture un nuevo evento.*/
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        int totalItems;
        EditText txtCodigoProducto;

        try {
            Util.playSound2();
        } catch (Exception e) {
        }

        switch (view.getId()) {

            case R.id.btnAceptarOpcionesPedido:

                txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
                OcultarTeclado(txtCodigoProducto);

                totalItems = Main.detallePedido.size();

                /*
                 * if (totalItems >= Const.MAX_ITEMS) {
                 *
                 * Util.MostrarAlertDialog(this, "Se ha superado el maximo de Productos: " +
                 * Const.MAX_ITEMS + ".");
                 *
                 * } else {
                 */

                fueUlPedido = false;

                String codigoProducto = txtCodigoProducto.getText().toString().trim();

                if (codigoProducto.equals("")) {

                    txtCodigoProducto.setError("Ingrese el codigo");
                    txtCodigoProducto.requestFocus();

                } else {
                    codigoProductoAnterior = codigoProducto;
                    CargarProducto_(codigoProducto);
                    txtCodigoProducto.setError(null);
                }
                // }
                break;

            case R.id.btnBuscarOpcionesPedido:

                totalItems = Main.detallePedido.size();

                /*
                 * if (totalItems >= Const.MAX_ITEMS) {
                 *
                 * Util.MostrarAlertDialog(this, "Se ha superado el maximo de Productos: " +
                 * Const.MAX_ITEMS + ".");
                 *
                 * } else {
                 */

                fueUlPedido = false;
                ((EditText) findViewById(R.id.txtCodigoProducto)).setError(null);
                Intent formBuscarProducto = new Intent(this, FormBuscarProducto.class);
                startActivityForResult(formBuscarProducto, Const.RESP_BUSQUEDA_PRODUCTO);
                // }
                break;

            case R.id.btnPrimerProducto:

                mostrarPrimerProducto();
                break;

            case R.id.btnAntProducto:

                mostrarAntProducto();
                break;

            case R.id.btnSigProducto:

                mostrarSigProducto();
                break;

            case R.id.btnUltimoProducto:

                mostrarUltimoProducto();
                break;

            case R.id.btnAgregarProducto:

                RegistrarProducto();

                break;

            case R.id.btnAgregarProductoDevolucion:

                RegistrarProducto();

                break;

            // case R.id.btnDescAutorizado:
            //
            // fueUlPedido = false;
            // String cantidad = txtCantidadProductoDevolucion.getText().toString();
            // String codProducto = producto.codigo;
            // float precio = producto.precio;
            //
            // if(!cantidad.equals("")){
            //
            // Intent formDescAutorizados = new Intent(this, FormDescAutorizadosActivity.class);
            // formDescAutorizados.putExtra("producto", codProducto);
            // formDescAutorizados.putExtra("cantidad", cantidad);
            // formDescAutorizados.putExtra("precio", precio);
            // startActivityForResult(formDescAutorizados, Const.RESP_DESC_AUTORIZADOS);
            //
            // }else{
            //
            // Util.MostrarAlertDialog(this, "Antes de Registrar un Descuento Autorizado Para Este
            // Producto. Por Favor Ingrese la Cantidad", 1);
            //
            //
            // }
            //
            // break;

            /*
             * case R.id.btnCancelarPedido:
             *
             * txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
             * OcultarTeclado(txtCodigoProducto); txtCodigoProducto.setError(null); CancelarPedido();
             * break;
             *
             * case R.id.btnTerminarPedido:
             *
             * txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
             * txtCodigoProducto.setError(null);
             *
             * OcultarTeclado(txtCodigoProducto); ValidarPedido(); break;
             */
        }
    }


    public void OnClickUltimoPedido(View view) {

        ((EditText) findViewById(R.id.txtCodigoProducto)).setError(null);

        fueUlPedido = true;
        Intent intent = new Intent(this, FormUltimoPedidoActivity.class);

        startActivityForResult(intent, Const.RESP_ULTIMO_PEDIDO);

    }


    private void AgregarProductoPedido(int cantidad, float descuento, String codMotivo) {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        String codigoProducto = txtCodigoProducto.getText().toString();

        System.out.println("AgregarProductoPedido LISTADETALLE ---> " + Main.detallePedido.size());

        Detalle detalle = Main.detallePedido.get(codigoProducto);

        if (detalle == null) {

            System.out.println("Entr� a DETALLE IS NULL");
            if (producto != null) {

                tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

                if (tipoDeClienteSeleccionado == 1) {

                    cliente = DataBaseBO.CargarClienteSeleccionado();

                } else {

                    cliente = DataBaseBO.CargarClienteNuevoSeleccionado();

                }

                detalle = new Detalle();
                detalle.codCliente = cliente.codigo;
                detalle.codProducto = producto.codigo;
                detalle.nombProducto = producto.descripcion;
                detalle.precio = producto.precio;
                detalle.iva = producto.iva;
                detalle.descuento = descuento;
                detalle.cantidad = cantidad;
                detalle.tipo_pedido = Const.PEDIDO_VENTA;
                detalle.inventario = producto.inventario;
                detalle.indice = producto.indice;
                detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                detalle.codMotivoCambio = motivoCambioSel.codigo;
                detalle.codMotivo = motivoCambioSel.codigo;
                detalle.motivo = motivoCambioSel.codigo;

            } else {

                AlertResetRegistrarProducto("No se pudo leer la informacion del Producto");
            }

        } else {

            System.out.println("Entr� a DETALLE IS NOT NULL");

            detalle.cantidad = cantidad;
            detalle.descuento = descuento;

            detalle.codMotivoCambio = motivoCambioSel.codigo;
            detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
            detalle.codMotivo = detalle.codMotivoCambio;
            detalle.motivo = motivoCambioSel.codigo;

        }

        int posicion = Main.detallePedido.size();

        detalle.posicion = posicion;
        Main.detallePedido.put(codigoProducto, detalle);
        System.out.println("AgregarProductoPedido LISTADETALLE #2 ---> " + Main.detallePedido.size());

        EditText txtCantidadProc = (EditText) findViewById(R.id.txtCantidadProductoDevolucion);
        txtCantidadProc.setText("");
        OcultarTeclado(txtCantidadProc);

        CargarListaPedido();

        /*
         * boolean agrego = AgregarProducto(detalle);
         *
         * if (agrego) {
         *
         * Main.detallePedido.put(codigoProducto, detalle);
         *
         * EditText txtCantidadProc = (EditText)dialogPedido.findViewById(R.id.txtCantidadProc);
         * txtCantidadProc.setText("");
         *
         * OcultarTeclado(txtCantidadProc);
         *
         * txtCodigoProducto.setText(""); txtCodigoProducto.requestFocus();
         *
         * CargarListaPedido(); dialogPedido.cancel();
         *
         * } else {
         *
         * AlertResetRegistrarProducto("No se pudo Agregar el Producto!"); }
         */
    }


    private void ActualizarProductoPedido(Detalle detalle, int cantidad, float descuento) {

        if (detalle == null) {

            AlertResetRegistrarProducto("No se pudo leer la informacion del Producto!");
            return;
        }

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        String codProducto = txtCodigoProducto.getText().toString();

        boolean actualizo = DataBaseBO.ActualizarProductoPedido(Main.encabezado, codProducto, cantidad);

        if (actualizo) {

            detalle.cantidad = cantidad;
            detalle.descuento = descuento;

            Main.detallePedido.put(codProducto, detalle);

            EditText txtCantidadProc = (EditText) dialogPedido.findViewById(R.id.txtCantidadProc);
            txtCantidadProc.setText("");

            OcultarTeclado(txtCantidadProc);

            txtCodigoProducto.setText("");
            txtCodigoProducto.requestFocus();

            CargarListaPedido();
            dialogPedido.cancel();

        } else {

            AlertResetRegistrarProducto("No se pudo Editar los datos del Producto!");
        }
    }


    private boolean CargarProducto(String codigoProducto) {

        producto = new Producto();
        boolean cargo = DataBaseBO.ProductoXCodigo(codigoProducto, producto);

        if (cargo) {

            posNavegadorProductos = producto.indice - 1;
            mostrarActualProducto("");
            return true;

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("No se encontro el Producto").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
            return false;
        }
    }


    private boolean CargarProducto_(String codigoProducto) {

        producto = new Producto();
        boolean cargo = DataBaseBO.ProductoXCodigo(codigoProducto, producto);
        if (cargo) {

            posNavegadorProductos = producto.indice - 1;
            mostrarActualProducto("");
            return true;

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("No se encontro el Producto de Codigo: " + codigoProducto).setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                    ((EditText) findViewById(R.id.txtCodigoProducto)).setText("");
                    // CargarProducto(codigoProductoAnterior);
                    // mostrarActualProducto("");

                }
            });

            AlertDialog alert = builder.create();
            alert.show();
            return false;
        }
    }


    public boolean MostrarDialogPedido(String codigoProducto) {

        try {

            if (onClickRegistrarProducto != null)
                onClickRegistrarProducto.reset();

            if (dialogPedido == null) {

                dialogPedido = new Dialog(this);
                dialogPedido.setContentView(R.layout.dialog_pedido);
                dialogPedido.setTitle("Registrar Producto");

                String[] items;
                Vector<String> listaItems = new Vector<String>();
                listaMotivosCambio = DataBaseBO.ListaMotivosCambio(listaItems);

                if (listaItems.size() > 0) {

                    items = new String[listaItems.size()];
                    listaItems.copyInto(items);

                } else {

                    items = new String[]{};
                }

                Spinner cbMotivoCambio = (Spinner) dialogPedido.findViewById(R.id.cbMotivoCambio);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                cbMotivoCambio.setAdapter(adapter);

            } else {

                ((EditText) dialogPedido.findViewById(R.id.txtCantidadProc)).requestFocus();
            }

            dialogPedido.onContentChanged();

            if (cambio) {

                ((TableLayout) dialogPedido.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.VISIBLE);

            } else {

                ((TableLayout) dialogPedido.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.GONE);
            }

            if (producto != null) { // Se carga la Informacion del Producto

                ((TextView) dialogPedido.findViewById(R.id.lblDescProducto)).setText(producto.descripcion);
                ((TextView) dialogPedido.findViewById(R.id.lblPrecio)).setText("Precio: " + Util.SepararMiles("" + producto.precio) + " - Iva: " + producto.iva + "%");
                ((TextView) dialogPedido.findViewById(R.id.lblInventario)).setText("Lim Venta: " + String.valueOf(producto.inventario));

                detalleAct = Main.detallePedido.get(codigoProducto);

                if (detalleAct != null) {

                    editar = true;
                    ((EditText) dialogPedido.findViewById(R.id.txtCantidadProc)).setText("" + detalleAct.cantidad);

                    if (cambio)
                        SelecionarMotivoCambio(detalleAct.codMotivo);

                } else {

                    editar = false;
                    ((EditText) dialogPedido.findViewById(R.id.txtCantidadProc)).setText("");
                }

                ((Button) dialogPedido.findViewById(R.id.btnVentaOpcionesPedido)).setOnClickListener(onClickRegistrarProducto);

                ((Button) dialogPedido.findViewById(R.id.btnCancelarOpcionesPedido)).setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {

                        dialogPedido.cancel();
                        onClickRegistrarProducto.reset();
                    }
                });

                dialogPedido.setCancelable(false);
                dialogPedido.show();

                return true;

            } else { // No se Encontro el Producto

                ((TextView) dialogPedido.findViewById(R.id.lblDescProducto)).setText("-");
                ((TextView) dialogPedido.findViewById(R.id.lblPrecio)).setText("-");

                AlertResetRegistrarProducto("No se encontro el Producto!");
                return false;
            }

        } catch (Exception e) {

            onClickRegistrarProducto.reset();
            return false;
        }
    }


    public void RegistrarProducto() {

        if (!cambio) {

            String cant = ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).getText().toString();
            int cantidad = Util.ToInt(cant);

            String desc = ((TextView) findViewById(R.id.txtDescProducto)).getText().toString();
            float descuento = Util.ToFloat(desc);

            // EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
            // String codigoProducto = txtCodigoProducto.getText().toString();

            if (cantidad == 0) {

                if (cambio)
                    MostrarAlertDialog("Debe ingresar la cantidad para la Novedad en entrega");
                else
                    MostrarAlertDialog("Debe Ingresar la cantidad");

            } else if (producto.precio <= 0) {

                MostrarAlertDialog("El producto no tiene precio valido, para la venta");
            } else {

                if (producto.inventario < cantidad) {

                    Toast toast = new Toast(getApplicationContext());

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toask_layout, (ViewGroup) findViewById(R.id.lytLayout));

                    TextView txtMsg = (TextView) layout.findViewById(R.id.txtMensaje);
                    txtMsg.setText("La cantidad supera el inventario disponible");

                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setView(layout);
                    toast.show();
                }

                AgregarProductoPedido(cantidad, descuento, "1");

            }

            EditText txtCantidadProductoDevolucion = (EditText) findViewById(R.id.txtCantidadProductoDevolucion);

            OcultarTeclado(txtCantidadProductoDevolucion);

            if (fueUlPedido) {

                Intent intent = new Intent(this, FormUltimoPedidoActivity.class);
                startActivityForResult(intent, Const.RESP_ULTIMO_PEDIDO);
            }

        } else {

            String cant = ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).getText().toString();
            int cantidad = Util.ToInt(cant);

            String desc = ((TextView) findViewById(R.id.txtDescProducto)).getText().toString();
            float descuento = Util.ToFloat(desc);

            EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
            String codigoProducto = txtCodigoProducto.getText().toString();

            if (codigoProducto == null || codigoProducto.length() < 1) {
                ((EditText) findViewById(R.id.txtCodigoProducto)).requestFocus();
                MostrarAlertDialog("Debe ingresar codigo valido para la Novedad en entrega.");
                return;
            }
            if (!DataBaseBO.validarProducto(codigoProducto)) {
                MostrarAlertDialog("Debe ingresar producto valido para la Novedad en entrega.");
                return;
            }

            CargarProducto_(codigoProducto);

            if (cantidad == 0) {

                if (cambio)
                    MostrarAlertDialog("Debe ingresar la cantidad para la Novedad en enterga");
                else
                    MostrarAlertDialog("Debe Ingresar la cantidad");

            } else {

                int pos = -1;

                pos = spMotivosCambios.getSelectedItemPosition();

                motivoCambioSel = listaMotivosCambio.elementAt(pos);

                AgregarProductoPedido(cantidad, descuento, motivoCambioSel.codigo);

            }

        }

    }


    private void SelecionarMotivoCambio(String codMotivo) {

        int size = listaMotivosCambio.size();
        for (int j = 0; j < size; j++) {

            MotivoCambio motivoCambio = listaMotivosCambio.elementAt(j);

            if (dialogPedido != null && motivoCambio.codigo.equals(codMotivo)) {

                ((Spinner) dialogPedido.findViewById(R.id.cbMotivoCambio)).setSelection(j);
                break;
            }
        }
    }


    /**
     * Verifica si el pedio tiene productos asignados, en caso tal muestra el resumen del Pedido y
     * pide al usuario las observaciones Si el pedido esta Vacio, muestra un mensaje de advertencia
     * al usuario
     **/
    public void ValidarPedido() {

        if (Main.detallePedido.size() > 0) {

            long pedidoMinimo = DataBaseBO.getPedidoMinimo();

            if (Main.usuario.codigoVendedor == null)
                DataBaseBO.CargarInfomacionUsuario();

            if (pedidoMinimo > (long) Main.encabezado.sub_total & !cambio) {

                AlertResetRegistrarProducto("El pedido no supera el valor minimo permitido ( " + pedidoMinimo + " )");
            } else {

                MostrarDialogResumen();
            }
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("La " + strOpcion + " esta vacia. Por favor ingrese los productos.").setCancelable(false).setPositiveButton("Aceptar",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }


    public boolean MostrarDialogResumen() {

        try {

            if (onClickGuardarPedido != null)
                onClickGuardarPedido.reset();

            if (dialogResumen == null) {

                dialogResumen = new Dialog(this);
                dialogResumen.setContentView(R.layout.dialog_resumen_pedido);

                spSucursal = (Spinner) dialogResumen.findViewById(R.id.spSucursal);

                String[] items;
                Vector<String> listaItems = new Vector<String>();
                listaSucursal = DataBaseBO.cargarListaSucursal(listaItems, Main.cliente.codigo);

                if (listaItems.size() > 0) {

                    items = new String[listaItems.size()];
                    listaItems.copyInto(items);

                } else {

                    items = new String[]{};
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spSucursal.setAdapter(adapter);
                /**
                 * Se registran los eventos de los Botones del Formulario Dialog Resumen
                 */
                ((Button) dialogResumen.findViewById(R.id.btnAceptarResumenPedido)).setOnClickListener(onClickGuardarPedido);

                /*
                 * ((Button)dialogResumen.findViewById(R.id.btnAceptarResumenPedido)).
                 * setOnClickListener(new OnClickListener() {
                 *
                 * public void onClick(View v) {
                 *
                 * FinalizarPedido(); } });
                 */

                ((Button) dialogResumen.findViewById(R.id.btnCancelarResumenPedido)).setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {

                        EditText txtObservacionPedido = ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido));
                        OcultarTeclado(txtObservacionPedido);

                        dialogResumen.cancel();
                        onClickGuardarPedido.reset();
                    }
                });

                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                date = diaActualMasUno();

                System.out.println("fecha: " + sdf.format(date));

                edFechaEntrega = (EditText) dialogResumen.findViewById(R.id.lblFechaEntrega);
                edFechaEntrega.setText(sdf.format(date));

                edFechaEntrega.addTextChangedListener(new CustomTextWatcher(edFechaEntrega));

                ((Button) dialogResumen.findViewById(R.id.btnSelecionarFechaEntrega)).setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {

                        // MostrarDialogFechaEntrega();
                        DatePickerFragment datePikker = new DatePickerFragment(new DateSetFechaListener() {

                            @SuppressLint("NewApi")
                            @Override
                            public void getFechaString(String fecha) {
                                edFechaEntrega.setText(fecha);
                            }
                        });
                        datePikker.show(FormDevolucionActivity.this.getFragmentManager(), "datePicker");
                        onClickGuardarPedido.reset();
                    }
                });

            } else {

                ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).setText("");
                ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido)).setText("");
                ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido)).setText("");
                ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).requestFocus();
                // ((Spinner) dialogResumen.findViewById(R.id.cbMotivoCambio)).setSelection(0);
            }

            // Pedido

            Enumeration<Detalle> e = Main.detallePedido.elements();

            float subTotal = 0;
            float valorDesc = 0;
            float valorIva = 0;
            double valorNeto = 0;

            while (e.hasMoreElements()) {

                Detalle detalle = e.nextElement();

                float sub_total = detalle.cantidad * detalle.precio;
                float valor_descuento = sub_total * (detalle.descuento / 100f);
                float valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100f);

                subTotal += sub_total;
                valorIva += valor_iva;
                valorDesc += valor_descuento;

                detalle.strPrecio = Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + detalle.precio), 2));
            }

            valorNeto = (double) (subTotal + valorIva - valorDesc);

            Main.encabezado.sub_total = subTotal;
            Main.encabezado.total_iva = valorIva;
            Main.encabezado.valor_descuento = valorDesc;
            Main.encabezado.valor_neto = (float) valorNeto;

            ((TextView) dialogResumen.findViewById(R.id.lblSubTotalResumenPedido)).setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + subTotal), 2)));
            ((TextView) dialogResumen.findViewById(R.id.lblIVAResumenPedido)).setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorIva), 2)));
            ((TextView) dialogResumen.findViewById(R.id.lblDescuentoResumenPedido)).setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorDesc), 2)));
            ((TextView) dialogResumen.findViewById(R.id.lblTotalResumenPedido)).setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorNeto), 2)));

            valorTotal = Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorNeto), 2));

            ((TableLayout) dialogResumen.findViewById(R.id.tableLayoutResumenCliente)).setVisibility(TableLayout.VISIBLE);

            // ((TableLayout)dialogResumen.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.GONE);

            ((TextView) dialogResumen.findViewById(R.id.lblClienteResumenPedido)).setText(Main.cliente.Nombre);
            dialogResumen.setTitle("Resumen " + strOpcion);

            // ((TextView)dialogResumen.findViewById(R.id.lblFechaEntrega)).setText(Util.DateToStringYYYYMMDD2());

            dialogResumen.setCancelable(false);
            dialogResumen.show();

            return true;

        } catch (Exception e) {

            Log.e("MostrarDialogResumen", e.getMessage(), e);

            if (onClickGuardarPedido != null)
                onClickGuardarPedido.reset();

            return false;
        }
    }


    public void MostrarDialogEdicion() {

        if (dialogEditar == null) {

            dialogEditar = new Dialog(this);
            dialogEditar.setContentView(R.layout.dialog_editar);
            dialogEditar.setTitle("Opciones Edicion");

            ((RadioButton) dialogEditar.findViewById(R.id.radioEditar)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    posNavegadorProductos = detalleEdicion.indice - 1;

                    dialogEditar.cancel();
                    mostrarActualProducto(String.valueOf(detalleEdicion.cantidad));

                }
            });

            ((RadioButton) dialogEditar.findViewById(R.id.radioEliminar)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(FormDevolucionActivity.this);
                    builder.setMessage("Esta Seguro de eliminar el producto " + detalleEdicion.codProducto).setCancelable(false)
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {

                                    Main.detallePedido.remove(detalleEdicion.codProducto);
                                    CargarListaPedido();
                                    dialogEditar.cancel();
                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialogEditar.cancel();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            ((RadioButton) dialogEditar.findViewById(R.id.radioCancelar)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    dialogEditar.cancel();
                }
            });

        } else {

            ((RadioButton) dialogEditar.findViewById(R.id.radioEditar)).requestFocus();
        }

        // ((RadioButton)dialogEditar.findViewById(R.id.radioEditar)).setVisibility(RadioButton.GONE);

        dialogEditar.show();
    }


    public void CargarListaPedido() {

        Main.encabezado.sub_total = 0;
        Main.encabezado.total_iva = 0;
        Main.encabezado.valor_descuento = 0;

        ItemListView itemListView;
        Enumeration<Detalle> e = Main.detallePedido.elements();
        Vector<ItemListView> datosPedido = new Vector<ItemListView>();
        List<Detalle> listaDetalle = new ArrayList<Detalle>();

        while (e.hasMoreElements()) {
            Detalle detalle = (Detalle) e.nextElement();
            listaDetalle.add(detalle);
        }

        Collections.sort(listaDetalle);
        Iterator<Detalle> e2 = listaDetalle.iterator();

        while (e2.hasNext()) {

            Detalle detalle = e2.next();
            itemListView = new ItemListView();

            float sub_total = detalle.cantidad * detalle.precio;
            float valor_descuento = sub_total * 0 / 100;
            float valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100);

            Main.encabezado.sub_total += sub_total;
            Main.encabezado.total_iva += valor_iva;
            Main.encabezado.valor_descuento += valor_descuento;

            if (cambio) {

                itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                // itemListView.subTitulo = "Motivo: "+ detalle.descripcionMotivoCambio +" - Cant.
                // Devolucion: " + detalle.cantidad;
                itemListView.subTitulo = "Precio: $" + detalle.precio + "  Iva: " + detalle.iva + "%  " + "  Monto: $" + Util.Redondear("" + detalle.precio * detalle.cantidad, 2)
                        + "\n" + "Motivo: " + detalle.descripcionMotivoCambio + " - Cant. Devolucion: " + detalle.cantidad;

                datosPedido.addElement(itemListView);
                // isPedidoAnulado ? DataBaseBO.obtenerMotivo(detalle.motivo) :

            } else {

                itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                itemListView.subTitulo = "Precio: $" + detalle.precio + " - Iva: " + detalle.iva + "% - Cant: " + detalle.cantidad;
                datosPedido.addElement(itemListView);

            }
        }

        ItemListView[] listaItems = new ItemListView[datosPedido.size()];
        datosPedido.copyInto(listaItems);

        Main.encabezado.valor_neto = Main.encabezado.sub_total + Main.encabezado.total_iva - Main.encabezado.valor_descuento;
        Main.encabezado.str_valor_neto = Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + Main.encabezado.valor_neto), 2));

        if (cambio) {

            Main.encabezado.sub_total = 0;
            Main.encabezado.total_iva = 0;
            Main.encabezado.valor_descuento = 0;

            // lblTotalItems.setText("" + Main.detallePedido.size());

        } else {

            /*
             * lblTotalItemsPedido.setText("" + Main.detallePedido.size());
             * lblSubTotal.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" +
             * Main.encabezado.sub_total), 0)));
             * lblTotalIva.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" +
             * Main.encabezado.total_iva), 0)));
             * lblDescuento.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" +
             * Main.encabezado.valor_descuento), 0)));
             * lblValorNetoPedido.setText(Main.encabezado.str_valor_neto);
             */
        }

        ListViewAdapterPedido adapter = new ListViewAdapterPedido(this, listaItems, R.drawable.compra, 0);
        ListView listaPedido = (ListView) findViewById(R.id.listaFormPedido);
        listaPedido.setAdapter(adapter);

        // float disponible = Util.ToFloat(Main.cliente.cupo) - Main.cliente.carteraPendiente.total;
        // Main.encabezado.excedeCupo = Main.encabezado.valor_neto > disponible;

        /*
         * if (Main.encabezado.excedeCupo) {
         *
         * Util.MostrarAlertDialog(this, "El valor del pedido excede el cupo disponible: " +
         * Util.SepararMiles(Util.QuitarE("" + disponible))); }
         */
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            if ((requestCode == Const.RESP_BUSQUEDA_PRODUCTO || requestCode == Const.RESP_ULTIMO_PEDIDO || requestCode == Const.RESP_FILTRO_PRODUCTO)) {

                if (data != null) {

                    int cantUltPedido;
                    Bundle bundle = data.getExtras();
                    Object productoSel = bundle.get("producto");

                    if (productoSel != null && productoSel instanceof Producto) {

                        producto = (Producto) productoSel;
                        ((EditText) findViewById(R.id.txtCodigoProducto)).setText(producto.codigo);

                        posNavegadorProductos = producto.indice - 1;

                        if (requestCode == Const.RESP_ULTIMO_PEDIDO) {

                            cantUltPedido = bundle.getInt("cantidad");
                            mostrarActualProducto(String.valueOf(cantUltPedido));
                        } else {

                            mostrarActualProducto("");
                        }

                    }

                } else if (requestCode == Const.RESP_ULTIMO_PEDIDO) {

                    CargarListaPedido();
                }

            } else if (requestCode == Const.RESP_FORMAS_DE_PAGO) {

                MostrarDialogResumen();
            } else if (requestCode == Const.RESP_DESC_AUTORIZADOS) {

                cambioImgDesc = true;
                // cambiarImagenDescAutorizado();
            }
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            CancelarPedido();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    public void CancelarPedido() {

        if (Main.detallePedido.size() > 0) {

            /**
             * Ya ha tomado un Pedido, Se pregunta al Usuario si desea Cancelarlo
             **/
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Esta seguro de cancelar  " + strOpcion + "? La informacion se borrara").setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            // boolean cancelo = DataBaseBO.CancelarPedido(Main.encabezado);

                            boolean cancelo = true;

                            if (cancelo) {

                                Main.encabezado = new Encabezado();
                                Main.detallePedido.clear();

                                dialog.cancel();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_CANCELED, returnIntent);
                                finish();

                            } else {

                                Util.MostrarAlertDialog(FormDevolucionActivity.this, "No se pudo cancelar el " + strOpcion);
                            }
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    moveTaskToBack(false);
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        } else {

            /**
             * El pedido esta vacio. Se verifica si se guardo un Encabezado en caso tal se borra. Se
             * cierra el formulario.
             **/

            if (Main.encabezado.numero_doc != null) {

                DataBaseBO.CancelarPedido(Main.encabezado);
            }

            Main.encabezado = new Encabezado();
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
    }



    /*
     * public void GuardarPedido() {
     *
     * Main.encabezado.codigo_cliente = Main.cliente.codigo; //Codigo del Cliente
     * Main.encabezado.nombre_cliente = Main.cliente.nombre; //Nombre del Cliente
     * Main.encabezado.razon_social = Main.cliente.razonSocial; //Razon Social
     * Main.encabezado.codigo_novedad = 1; //1 -> Venta, para no compra es MotivosCompras.codigo
     * Main.encabezado.lista_precio = Main.cliente.listaPrecio; //Lista de Precio
     *
     * //Main.encabezado.valor_neto; //Valor Neto de la Venta, Esta informacion se va calculando
     * cada vez que se Agrega, Edita o Elimina un Producto del Pedido //Main.encabezado.observacion
     * //Esta informacion se llena en el Dialog Resumen Pedido //Main.encabezado.hora_inicial //Esta
     * informacion se llena en el FormInfoCliente, cuando se da click en Nuevo Pedido
     *
     * Main.encabezado.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " +
     * Util.ObtenerHora(); //Hora en finaliza la Toma del Pedido Main.encabezado.tipo_cliente = "V";
     * //Tipo Cliente (Cliente viejo o Cliente nuevo) Main.encabezado.extra_ruta =
     * Main.cliente.extra_ruta; //Indica si el pedido se hace Sobre un cliente que no esta en el
     * Rutero Main.encabezado.numero_doc = DataBaseBO.ObtenterNumeroDoc(); //Numero Unico por Pedido
     *
     * boolean ingreso = DataBaseBO.GuardarPedido(Main.encabezado, Main.detallePedido);
     *
     * if (ingreso) {
     *
     * //DataBaseBO.ActulizarEstadoRutero(Main.cliente.codigo);
     *
     * AlertDialog.Builder builder = new AlertDialog.Builder(this);
     * builder.setMessage("Pedido Registrado con Exito\n" + "Para el Cliente " + Main.cliente.nombre
     * + "\n\n" + "Desea Enviar el pedido?") .setCancelable(false) .setPositiveButton("SI", new
     * DialogInterface.OnClickListener() {
     *
     * public void onClick(DialogInterface dialog, int id) {
     *
     * progressDialog = ProgressDialog.show(OpcionesPedidoActivity.this, "",
     * "Enviando Informacion Pedido...", true); progressDialog.show();
     *
     * Main.encabezado = new Encabezado(); Main.detallePedido.clear();
     *
     * dialog.cancel();
     *
     * Sync sync = new Sync(OpcionesPedidoActivity.this, Const.ENVIAR_PEDIDO); sync.start(); } })
     * .setNegativeButton("NO", new DialogInterface.OnClickListener() {
     *
     * public void onClick(DialogInterface dialog, int id) {
     *
     * Main.encabezado = new Encabezado(); Main.detallePedido.clear();
     *
     * dialog.cancel();
     *
     * OpcionesPedidoActivity.this.setResult(RESULT_OK); OpcionesPedidoActivity.this.finish(); } });
     *
     * AlertDialog alert = builder.create(); alert.show();
     *
     * dialogResumen.cancel(); } }
     */

    public void FinalizarPedido() {

        Usuario usuario = DataBaseBO.CargarUsuario();
        boolean finalizo = false;

        if (usuario == null) {

            Util.MostrarAlertDialog(this, "No se pudo cargar la informacion del Usuario");

        } else {

            if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {

//			float valorPedidoCliente = Float.parseFloat(Util.SepararMilesSin(Util.QuitarE(Util.Redondear(String.valueOf(pedidoAmarre.valorMonto), 2))));

                System.out.println(" Valor Pedido " + pedidoAmarre.valorMonto);
                long valorDevolucionAct = (long) Main.encabezado.valor_neto;

                System.out.println("Valor Dev " + valorDevolucionAct);

                if (valorDevolucionAct > pedidoAmarre.valorMonto) {
                    Util.MostrarAlertDialog(this, "El valor total de la devolucion no puede ser mayor al total del pedido.");
                    onClickGuardarPedido.reset();
                    return;

                } else {

                    Main.encabezado.codigo_cliente = cliente.codigo;
                    Main.encabezado.observacion = ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).getText().toString().trim();
                    Main.encabezado.observacion2 = ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido)).getText().toString().trim();
                    Main.encabezado.ordenCompra = ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido)).getText().toString().trim();
                    Main.encabezado.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); // Hora
                    // en
                    // finaliza
                    // la
                    // Toma
                    // del
                    // Pedido
                    Main.encabezado.numero_doc = DataBaseBO.ObtenterNumeroDoc(usuario.codigoVendedor);
                    Main.encabezado.fechaEntrega = edFechaEntrega.getText().toString();

                    if (cambio) {

                        Main.encabezado.motivo = 2;
                    } else {
                        Main.encabezado.motivo = 0;
                    }

                    // Main.encabezado.numero_doc = Main.encabezado.numero_doc.substring(0,
                    // Main.encabezado.numero_doc.length()-1);

                    String version = ObtenerVersion();
                    String imei = ObtenerImei();

                    if (isPedidoAnulado)
                        Main.encabezado.tipoTrans = 2;

                    if (!Main.encabezado.fechaEntrega.equals("")) {

                        finalizo = DataBaseBO.RegistrarPedido(Main.encabezado, Main.detallePedido, version, imei, true, pedidoAmarre.numero_doc);

                        if (finalizo) {

                            String complemento = "Registrado con exito\n" + "para el cliente " + cliente.Nombre;
                            mostrarDialogPedidoRegistrado(strOpcion + complemento);

                            dialogResumen.cancel();

                        } else {
                            DataBaseBO.BorrarPedidoIncompleto(Main.encabezado.numero_doc);
                            MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
                        }

                    }
                }
            } else {

                Main.encabezado.codigo_cliente = cliente.codigo;
                Main.encabezado.observacion = ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).getText().toString().trim();
                Main.encabezado.observacion2 = ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido)).getText().toString().trim();
                Main.encabezado.ordenCompra = ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido)).getText().toString().trim();
                Main.encabezado.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); // Hora
                // en
                // finaliza
                // la
                // Toma
                // del
                // Pedido
                Main.encabezado.numero_doc = DataBaseBO.ObtenterNumeroDoc(usuario.codigoVendedor);
                Main.encabezado.fechaEntrega = edFechaEntrega.getText().toString();

                int posSucursal = spSucursal.getSelectedItemPosition();
                Main.encabezado.sucursal = listaSucursal.get(posSucursal).codigodir;

                String valor = ((EditText) dialogResumen.findViewById(R.id.txtPrecioFlete))
                        .getText().toString().trim();
                //Condicion para el felete determina si esta vacio o ingresaron algun valor
                Main.encabezado.flete = (valor.equals("")) ? 0f : Float.parseFloat(valor);

                /*if (cambio) {
                    Main.encabezado.motivo = 10;
                } else {
                    Main.encabezado.motivo = 0;
                }*/

                // Main.encabezado.numero_doc = Main.encabezado.numero_doc.substring(0,
                // Main.encabezado.numero_doc.length()-1);

                String version = ObtenerVersion();
                String imei = ObtenerImei();

                if (isPedidoAnulado)
                    Main.encabezado.tipoTrans = 2;

                if (!Main.encabezado.fechaEntrega.equals("")) {

                    if (usuario.equals(Const.AUTOVENTA))
                        finalizo = DataBaseBO.RegistrarPedido(Main.encabezado, Main.detallePedido, version, imei, false, pedidoAmarre.numero_doc);
                    else
                        finalizo = DataBaseBO.RegistrarPedido(Main.encabezado, Main.detallePedido, version, imei, false, "");

                    if (finalizo) {

                        String complemento = "Registrado con Exito\n" + "Para el Cliente " + cliente.Nombre;
                        mostrarDialogPedidoRegistrado(strOpcion + complemento);

                        dialogResumen.cancel();

                    } else {

                        DataBaseBO.BorrarPedidoIncompleto(Main.encabezado.numero_doc);
                        MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
                    }

                }

            }

        }

    }


    public void mostrarDialogPedidoRegistrado(String mensaje) {

        Usuario usuario = DataBaseBO.ObtenerUsuario();

        if (dialogPedidoRegistrado == null) {

            dialogPedidoRegistrado = new Dialog(this);
            dialogPedidoRegistrado.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogPedidoRegistrado.setContentView(R.layout.dialog_pedido_registrado);

//	    dialogPedidoRegistrado.setTitle(strOpcion);

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

                public void onClick(View view) {

                    if (Main.usuario.codigoVendedor == null)
                        DataBaseBO.CargarInfomacionUsuario();

                    DataBaseBO.ActualizaEstadoRutero(Main.cliente.codigo);

                    // progressDialog = ProgressDialog.show(FormDevolucionActivity.this, "",
                    // "Enviando Informacion ...", true);
                    // progressDialog.show();

                    // Sync sync = new Sync(FormDevolucionActivity.this, Const.ENVIAR_PEDIDO);
                    // sync.start();
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", true);
                    returnIntent.putExtra("nroDocDev", Main.encabezado.numero_doc);

                    Main.encabezado = new Encabezado();
                    dialogPedidoRegistrado.cancel();
                    onClickGuardarPedido.reset();
                    Main.detallePedido.clear();

                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            });

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setOnClickListener(new OnClickListener() {

                public void onClick(View view) {

                    boolean isPedido = false;

                    String copiaPrint = "";
                    if (primerEjecucion) {
                        primerEjecucion = false;
                        copiaPrint = "ORIGINAL";
                    } else {
                        copiaPrint = "COPIA";
                    }

                    progressDialog = ProgressDialog.show(FormDevolucionActivity.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
                    progressDialog.show();

                    SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                    macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

                    SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                    String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");

                    if (macImpresora.equals("-")) {

                        Util.MostrarAlertDialog(FormDevolucionActivity.this, "");//"Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");
                        if (progressDialog != null)
                            progressDialog.cancel();

                    } else {

                        if (!tipoImpresora.equals("Intermec")) {
                            sewooLKP20 = new SewooLKP20(FormDevolucionActivity.this);
                            imprimirSewooLKP20(macImpresora, Main.encabezado.numero_doc, copiaPrint, isPedido);
                        } else {
                            imprimirTirillaGeneral(macImpresora, Main.encabezado.numero_doc, copiaPrint);
                        }
                    }
                }
            });
        }

        SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
        String macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

        if (macImpresora.equals("-")) {

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.GONE);

            String msg = mensaje; //+ "<br /><br />" + "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!";
            ((TextView) dialogPedidoRegistrado.findViewById(R.id.lblMsg)).setText(Html.fromHtml(msg));

        } else {

            if (!usuario.tipoVenta.equals(Const.AUTOVENTA))
                ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.VISIBLE);
            else
                ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.GONE);

            ((TextView) dialogPedidoRegistrado.findViewById(R.id.lblMsg)).setText(mensaje);

        }

        ((TextView) dialogPedidoRegistrado.findViewById(R.id.tvTituloDialog)).setText("Devolucion");
        dialogPedidoRegistrado.setCancelable(false);
        dialogPedidoRegistrado.show();
    }


    private void imprimirTirillaGeneral(final String macAddress, final String numeroDoc, final String copiaPrint) {

        System.out.println("Entr� a imprimirTirillaGeneral");

        new Thread(new Runnable() {

            public void run() {

                System.out.println("Entr� al hilo");

                mensaje = "";
                BluetoothSocket socket = null;

                try {

                    Looper.prepare();

                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                    if (bluetoothAdapter == null) {

                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else if (!bluetoothAdapter.isEnabled()) {

                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else {

                        BluetoothDevice printer = null;

                        printer = bluetoothAdapter.getRemoteDevice(macAddress);

                        if (printer == null) {

                            mensaje = "No se pudo establecer la conexion con la Impresora.";

                        } else {

                            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

                            SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                            String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");

                            if (tipoImpresora.equals("Intermec")) {

                                socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);
                            } else {
                                socket = printer.createRfcommSocketToServiceRecord(uuid);
                            }

                            if (socket != null) {

                                socket.connect();

                                Thread.sleep(3500);

                                if (tipoImpresora.equals("Intermec")) {
                                    System.out.println("Entr� TipoImpresora: " + tipoImpresora);
                                    ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoDevolucionItermec(numeroDoc, copiaPrint, false, false));
                                } else {
                                    System.out.println("Entr� TipoImpresora: " + tipoImpresora);
                                    ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoDevolucion(numeroDoc, copiaPrint));
                                }

                                handlerFinish.sendEmptyMessage(0);

                            } else {

                                mensaje = "No se pudo abrir la conexion con la Impresora.\n\nPor Favor intente de nuevo.";
                            }

                        }
                    }

                    if (!mensaje.equals("")) {

                        context = FormDevolucionActivity.this;
                        handlerMensaje.sendEmptyMessage(0);
                    }

                    Looper.myLooper().quit();

                } catch (Exception e) {

                    String motivo = e.getMessage();
                    Log.e(TAG, "imprimirPruebaExtech -> " + motivo, e);

                    mensaje = "No se pudo ejecutar la Impresion.";

                    if (motivo != null) {
                        mensaje += "\n\n" + motivo;
                    }

                    context = FormDevolucionActivity.this;
                    handlerMensaje.sendEmptyMessage(0);

                } finally {

                    try {

                        // if (socket != null) {
                        // socket.close();
                        // }

                    } catch (Exception e) {

                    }
                }
            }

        }).start();

    }

    //
    // public void Imprimiendo(final BluetoothSocket socket,
    // final String textoImprimir) {
    //
    // splashTread = new Thread() {
    // @Override
    // public void run() {
    //
    // try {
    // synchronized (this) {
    //
    // wait(_splashTime);
    //
    // }
    //
    // } catch (InterruptedException e) {
    // } finally {
    //
    // try {
    //
    // OutputStream stream = socket.getOutputStream();
    // String strPrint = textoImprimir;
    // String aux=".";
    // stream.write(strPrint.getBytes());
    // stream.write(aux.getBytes());
    // stream.flush();
    // stream.close();
    //
    // handlerFinish.sendEmptyMessage(0);
    //
    // if (progressDialog != null) {
    // progressDialog.dismiss();
    // }
    // try {
    //
    // if (socket != null)
    // socket.close();
    //
    // } catch (Exception e) {
    // }
    //
    //
    // } catch (Exception e2) {
    //
    // }
    //
    // }
    // }
    // };
    //
    // splashTread.start();
    //
    // }
    //
    // private Handler handlerError = new Handler() {
    //
    // @Override
    // public void handleMessage(Message msg) {
    //
    // Util.MostrarAlertDialog(FormDevolucionActivity.this, mensaje);
    //
    // if (progressDialog != null) {
    // progressDialog.cancel();
    //
    //
    // }
    // }
    // };

    private Handler handlerFinish = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (progressDialog != null)
                progressDialog.cancel();

        }
    };

    private Handler handlerMensaje = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (progressDialog != null) {
                progressDialog.cancel();
            }

            if (context != null) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.setMessage(mensaje);
                alertDialog.show();
            }
        }
    };


    public void ImprimirCopia() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Desea imprimir una copia?").setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                progressDialog = ProgressDialog.show(FormDevolucionActivity.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
                progressDialog.show();

                dialog.cancel();
                // imprimirTirillaGeneral(macImpresora,Main.encabezado.numero_doc, );

            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }


    public void SetListenerListView() {

        ListView listaOpciones = (ListView) findViewById(R.id.listaFormPedido);
        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ListViewAdapterPedido adapter = (ListViewAdapterPedido) parent.getAdapter();
                ItemListView itemListView = adapter.listItems[position];

                String[] data = itemListView.titulo.split("-");
                String codigoProducto = data[0].trim();
                detalleEdicion = Main.detallePedido.get(codigoProducto);

                MostrarDialogEdicion();

            }
        });
    }


    public void SetKeyListener() {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        txtCodigoProducto.setOnKeyListener(new OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    ((Button) findViewById(R.id.btnAceptarOpcionesPedido)).requestFocus();
                    return true;
                }

                return false;
            }
        });
    }


    public void SetFocusListener() {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        txtCodigoProducto.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {

                    /**
                     * Si NO tiene el foco se quita el mensaje de Error.
                     **/
                    ((EditText) findViewById(R.id.txtCodigoProducto)).setError(null);
                }
            }
        });
    }


    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

        final String mensaje = ok ? strOpcion + " Registrado con Exito en el servidor" : msg;

        if (progressDialog != null)
            progressDialog.cancel();

        this.runOnUiThread(new Runnable() {

            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(FormDevolucionActivity.this);
                builder.setMessage(mensaje)

                        .setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Main.encabezado = new Encabezado();
                        Main.detallePedido.clear();

                        dialog.cancel();

                        // OpcionesPedidoActivity.this.setResult(RESULT_OK);
                        FormDevolucionActivity.this.finish();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }


    public void OcultarTeclado(EditText editText) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }


    public String ObtenerVersion() {

        String version;

        try {

            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

        } catch (NameNotFoundException e) {

            version = "0.0";
            Log.e("OpcionesPedidoActivity", e.getMessage(), e);
        }

        return version;
    }


    public String ObtenerImei() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
    }


    public void MostrarAlertDialog(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
                onClickGuardarPedido.reset();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }


    public void AlertResetRegistrarProducto(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
                onClickRegistrarProducto.reset();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }


    private void setMsjPosNavegador() {

        int posAux = String.valueOf((posNavegadorProductos + 1)).length();
        msjPosNavegador = Util.replicate("0", 3 - posAux) + String.valueOf((posNavegadorProductos + 1));
        msjPosNavegador += " de ";

        posAux = String.valueOf(Main.navegadorProductos.size()).length();
        msjPosNavegador += Util.replicate("0", 3 - posAux) + String.valueOf(Main.navegadorProductos.size());
    }

    private OnClickListenerImpl onClickTerminar = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            // OcultarMensajeYTeclado();
            ValidarPedido();
        }
    };

    private OnClickListenerImpl onClickGuardarPedido = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            onClickTerminar.reset();
            FinalizarPedido();
        }
    };

    private OnClickListenerImpl onClickRegistrarProducto = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            RegistrarProducto();
        }
    };

    private OnClickListenerImpl onClickFechaEntrega = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            MostrarDialogFechaEntrega();
        }
    };


    // private void cambiarImagenDescAutorizado( ){
    //
    // Button btnDesc;
    //
    // if( cambioImgDesc ){
    //
    // cambioImgDesc = false;
    //
    // btnDesc = (Button)findViewById(R.id.btnDescAutorizado);
    // btnDesc.setCompoundDrawablesWithIntrinsicBounds(
    // R.drawable.desc_mod, //izquierda
    // 0, //arriba
    // 0, //derecha
    // 0); //abajo
    // }
    // else{
    //
    // btnDesc = (Button)findViewById(R.id.btnDescAutorizado);
    // btnDesc.setCompoundDrawablesWithIntrinsicBounds(
    // R.drawable.desc_add, //izquierda
    // 0, //arriba
    // 0, //derecha
    // 0); //abajo
    // }
    //
    // }

    public boolean MostrarDialogFechaEntrega() {

        DatePicker dpFechaEntrega = null;

        Calendar c = Calendar.getInstance(Locale.US);

        String[] days = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

        String dday = days[c.get(Calendar.DAY_OF_WEEK) - 1];

        if (dday.equals("SATURDAY")) {

            c.add(Calendar.DAY_OF_MONTH, 3);

        } else {

            c.add(Calendar.DAY_OF_MONTH, 2);
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Main.ano = year;
        Main.day = day;
        Main.mes = month + 1;

        try {

            if (dialogFechaEntrega == null) {

                dialogFechaEntrega = new Dialog(this);
                dialogFechaEntrega.setContentView(R.layout.dialog_fecha_entrega_pedido);
                dialogFechaEntrega.setTitle("Fecha Entrega");

                dpFechaEntrega = (DatePicker) dialogFechaEntrega.findViewById(R.id.dpFechaEntrega);

                // set current date into datepicker
                dpFechaEntrega.init(year, month, day, null);

                /**
                 * Se registran los eventos de los Botones del Formulario Dialog Resumen
                 */
                ((Button) dialogFechaEntrega.findViewById(R.id.btnAceptarFechaEntrega)).setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {

                        String fechaEntrega = getFechaEntrega();

                        if (fechaEsValida()) {

                            ((TextView) dialogResumen.findViewById(R.id.lblFechaEntrega)).setText(fechaEntrega);
                            dialogFechaEntrega.cancel();

                        } else {

                            Util.MostrarAlertDialog(FormDevolucionActivity.this, "La Fecha No Es Valida", 1);

                        }

                    }
                });

            } else {

                dpFechaEntrega = (DatePicker) dialogFechaEntrega.findViewById(R.id.dpFechaEntrega);
                dpFechaEntrega.init(year, month, day, null);
            }

            dialogFechaEntrega.setCancelable(false);
            dialogFechaEntrega.show();

            return true;

        } catch (Exception e) {

            return false;
        }
    }


    private String getFechaEntrega() {

        int dia = 0, mes = 0, ano = 0;
        String diaTxt = "0", mesTxt = "0", anoTxt = "0", resultado = "";

        // Fechas
        DatePicker fechaPicker = (DatePicker) dialogFechaEntrega.findViewById(R.id.dpFechaEntrega);
        dia = fechaPicker.getDayOfMonth();
        mes = fechaPicker.getMonth() + 1;
        ano = fechaPicker.getYear();

        Main.ano1 = ano;
        Main.day1 = dia;
        Main.mes1 = mes;
        fechaEntrega = ano + "-" + ((mes < 10) ? ("0" + mes) : mes) + "-" + ((dia < 10) ? ("0" + dia) : dia);

        if (dia < 10)
            diaTxt = "0" + String.valueOf(dia);
        else
            diaTxt = String.valueOf(dia);

        if (mes < 10)
            mesTxt = "0" + String.valueOf(mes);
        else
            mesTxt = String.valueOf(mes);

        anoTxt = String.valueOf(ano);

        resultado = anoTxt + mesTxt + diaTxt;

        return resultado;
    }


    public void cargarCodigosProductos() {

        if (Main.navegadorProductos != null)
            if (Main.navegadorProductos.size() > 0) {

                codProductos = new String[Main.navegadorProductos.size()];

                for (int i = 0; i < codProductos.length; i++) {

                    codProductos[i] = Main.navegadorProductos.get(i).codigo;

                }

                AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.txtCodigoProducto);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        R.layout.auto_complete_text_view, R.id.autoCompleteItem, codProductos);
                textView.setAdapter(adapter);

                textView.setOnItemSelectedListener(new OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        // ... your stuff

                        CargarProducto_(((AutoCompleteTextView) findViewById(R.id.txtCodigoProducto)).getText().toString());

                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // ... your stuff
                    }

                });

                textView.setOnItemClickListener(new OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // ... your stuff

                        CargarProducto_(((AutoCompleteTextView) findViewById(R.id.txtCodigoProducto)).getText().toString());

                        OcultarTeclado(((AutoCompleteTextView) findViewById(R.id.txtCodigoProducto)));

                        ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).requestFocus();
                    }

                });

            }

    }


    public boolean fechaEsValida() {

        Date d1 = new Date(Main.ano, Main.mes, Main.day);

        Date d2 = new Date(Main.ano1, Main.mes1, Main.day1);

        if (d2.after(d1) || d2.toString().equals(d1.toString())) {

            return true;

        } else {

            return false;

        }

    }


    @Override
    protected void onResume() {

        super.onResume();

        // Main.usuario = null;
        // Main.cliente = null;

        if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {

            DataBaseBO.CargarInfomacionUsuario();

        }

        if (Main.cliente == null || Main.cliente.codigo == null) {

            int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;

            if (tipoDeClienteSelec == 1) {

                clienteSel = DataBaseBO.CargarClienteSeleccionado();

            } else {

                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();

            }

            if (clienteSel != null)
                Main.cliente = clienteSel;

        }

    }


    public Date diaActualMasUno() {

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("DIA:" + date.getDay() + "\nDATE: " + date.getDate());

        int dateActual = date.getDate();

        if (date.getDay() == 6) {

            dateActual = dateActual + 2;

        } else {
            dateActual = dateActual + 1;
        }

        date.setDate(dateActual);

        return date;
    }

    private class CustomTextWatcher implements TextWatcher {

        private EditText mEditText;


        public CustomTextWatcher(EditText e) {
            this.mEditText = e;
        }


        public synchronized void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }


        public synchronized void onTextChanged(CharSequence s, int start, int before, int count) {
        }


        public synchronized void afterTextChanged(Editable s) {

            if (mEditText != null) {

                mEditText.removeTextChangedListener(this);

                if (mEditText.getId() == R.id.lblFechaEntrega) {

                    String cadenaFecha = mEditText.getText().toString().trim();
                    Date dateFecha = Util.converFecha(cadenaFecha);

                    String fechaActual = Util.FechaActual("yyyy-MM-dd");
                    Date dateFechaActual = Util.converFecha(fechaActual);

                    long diff = dateFecha.getTime() - dateFechaActual.getTime();

                    if (diff < 0) {

                        Util.MostrarAlertDialog(FormDevolucionActivity.this, "La fecha ingresada debe ser superior a la actual.");
                        Date date = diaActualMasUno();

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                        edFechaEntrega.setText(sdf.format(date));

                    }
                }

                mEditText.addTextChangedListener(this);
            }
        }
    }


    /**
     * Imprimir la factura del pedido.
     *
     * @param macImpresora
     * @param numero_doc
     * @param copiaPrint
     */
    protected void imprimirSewooLKP20(final String macImpresora, final String numero_doc, final String copiaPrint, final boolean isPedido) {

        final Usuario usuario = DataBaseBO.ObtenerUsuario();

        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();

                if (macImpresora.equals("-")) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(FormDevolucionActivity.this, " ", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });
                } else {

                    if (sewooLKP20 == null) {
                        sewooLKP20 = new SewooLKP20(FormDevolucionActivity.this);
                    }
                    int conect = sewooLKP20.conectarImpresora(macImpresora);

                    switch (conect) {
                        case 1:
                            sewooLKP20.generarEncabezadoTirilla(numero_doc, usuario, copiaPrint, isPedido, false, false, 0, false);
                            sewooLKP20.imprimirBuffer(true);
                            break;

                        case -2:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormDevolucionActivity.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;

                        case -8:
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Util.MostrarAlertDialog(FormDevolucionActivity.this, "Bluetooth apagado. Por favor habilite el bluetoth para imprimir.");
                                }
                            });
                            break;

                        default:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormDevolucionActivity.this, "Error desconocido, intente nuevamente.", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                    }

                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (sewooLKP20 != null) {
                        sewooLKP20.desconectarImpresora();
                        if (progressDialog != null)
                            progressDialog.dismiss();
                    }
                }
                Looper.myLooper().quit();
            }
        }).start();
    }


    public void onClickFiltros(View view) {
        Intent intent = new Intent(this, FormFiltroPedidoActivity.class);
        intent.putExtra("isPedido", 1);
        startActivityForResult(intent, Const.RESP_FILTRO_PRODUCTO);
    }


    public void onClickCapturarCodigoQR(View view) {

        Intent intentScan = new Intent(BS_PACKAGE + ".SCAN");
        intentScan.putExtra("PROMPT_MESSAGE", "Enfoque entre 9 y 11 cm.viendo s�lo el c�digo de barras");
        String targetAppPackage = findTargetAppPackage(intentScan);
        if (targetAppPackage == null) {
            showDownloadDialog();
        } else {
            startActivityForResult(intentScan, Const.RESP_CODIGOBARRAS);
        }

    }


    private String findTargetAppPackage(Intent intent) {

        PackageManager pm = this.getPackageManager();
        List<ResolveInfo> availableApps = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (availableApps != null) {
            for (ResolveInfo availableApp : availableApps) {
                String packageName = availableApp.activityInfo.packageName;
                if (BS_PACKAGE.contains(packageName)) {
                    return packageName;
                }
            }
        }
        return null;
    }


    private AlertDialog showDownloadDialog() {
        final String DEFAULT_TITLE = "Instalar Barcode Scanner?";
        final String DEFAULT_MESSAGE = "Esta aplicaci�n necesita Barcode Scanner. �Desea instalarla?";
        final String DEFAULT_YES = "Si";
        final String DEFAULT_NO = "No";

        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
        downloadDialog.setTitle(DEFAULT_TITLE);
        downloadDialog.setMessage(DEFAULT_MESSAGE);
        downloadDialog.setPositiveButton(DEFAULT_YES, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://details?id=" + BS_PACKAGE);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    FormDevolucionActivity.this.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                    // Hmm, market is not installed
                    Toast.makeText(FormDevolucionActivity.this, "Android market no esta instalado,no puedo instalar Barcode Scanner", Toast.LENGTH_LONG).show();
                }
            }
        });
        downloadDialog.setNegativeButton(DEFAULT_NO, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }


    public void mostrarDialogoEscanearCodigoBarras() {

        if (dialogEscanear != null) {
            if (dialogEscanear.isShowing())
                dialogEscanear.dismiss();
            dialogEscanear.cancel();
        }

        dialogEscanear = new Dialog(this);
        dialogEscanear.setContentView(R.layout.escanear_codigo_de_barras2);
        dialogEscanear.setTitle("Lector");

        etCodigoEAN = (EditText) dialogEscanear.findViewById(R.id.etCodigoEAN);
        etCodigoEAN.setText("");
        etCodigoEAN.requestFocus();
        etCodigoEAN.addTextChangedListener(textWatcher);

        Button dialogButton = (Button) dialogEscanear.findViewById(R.id.btnSalir);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogEscanear.dismiss();
            }
        });

        dialogEscanear.show();

    }

    TextWatcher textWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {

            if (s.toString().length() >= 8 || s.toString().length() > 15) {

                enter();

            }

        }


        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }


        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };


    public void enter() {

        String codProducto = DataBaseBO.ExisteCodigoEnProductos(txtCodProducto.getText().toString());

        if (!codProducto.equals("")) {

            CargarProducto(codProducto);
            txtCodProducto.setText(codProducto);

            ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).requestFocus();

        } else {
            Util.mostrarToast(this, "No existe un producto registrado con ese EAN");

            txtCodProducto.setText("");

        }

    }

    TextWatcher textWatcherCantidad = new TextWatcher() {

        public void afterTextChanged(Editable s) {

            if (s.toString().length() >= 8 || s.toString().length() > 15) {

                //((EditText) findViewById(R.id.txtCantidadProducto)).setText("");

            }

        }


        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }


        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher textWatcherCantidadDev = new TextWatcher() {

        public void afterTextChanged(Editable s) {

            if (s.toString().length() >= 8 || s.toString().length() > 15) {

                ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).setText("");

            }

        }


        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }


        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };


    public void OnClickRegresar(View view) {
        finish();

    }


}
