package celuweb.com.uyusa;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import celuweb.com.DataObject.ItemCarteraListView;

public class ListViewDescuentoAdapter extends ArrayAdapter<ItemCarteraListView> {
	
	int layout_list_item;
	
	Activity context;
	ItemCarteraListView[] listItems;
	
	ListViewDescuentoAdapter(Activity context, ItemCarteraListView[] listItems) {
		
		super(context, R.layout.item_descuento, listItems);
		
		this.layout_list_item = R.layout.item_descuento;
		this.listItems = listItems; 
		this.context = context; 
	}
	
	ListViewDescuentoAdapter(Activity context, ItemCarteraListView[] listItems, int layout_list_item) {
		
		super(context, layout_list_item, listItems);
		
		this.layout_list_item = layout_list_item;
		this.listItems = listItems; 
		this.context = context; 
	}
	
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = context.getLayoutInflater();
		View item = inflater.inflate(layout_list_item, null);
		
		TextView lblTitulo = (TextView)item.findViewById(R.id.lblTitulo);
		lblTitulo.setText(listItems[position].titulo);
		
		return item;
    }
	
	@Override
	public ItemCarteraListView getItem(int position) {
		
		return listItems[position];
	}
}
