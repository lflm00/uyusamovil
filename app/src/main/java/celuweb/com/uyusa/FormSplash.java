package celuweb.com.uyusa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

public class FormSplash extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
    	
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    	setContentView(R.layout.form_splash);
    	
    	Handler handler = new Handler();
    	handler.postDelayed(getRunnableStartApp(), 2 * 1000);
    }
    
	/*
	
	
	/**
	* Metodo en el cual se debe incluir dentro de run(){Tu codigo} el codigo que se quiere realizar una
	* vez ha finalizado el tiempo que se desea mostrar la actividad de splash
	* @return
	*/
	private Runnable getRunnableStartApp() {
		
		Runnable runnable = new Runnable() {
			
			public void run() {
				
				//al interior valida si deben insertarse registros e inserta las estaciones si no existen
				//EstacionesMetroBO estacionesMetroBO = BusinessContext.getBean(EstacionesMetroBO.class);
				//estacionesMetroBO.insertRecordsEstacionesMetro();
				
				Intent intent = new Intent(FormSplash.this, FormLoginActivity.class);
				startActivity(intent);
				finish();
			}
	};
	return runnable;
	}
}
