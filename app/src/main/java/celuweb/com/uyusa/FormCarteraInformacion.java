package celuweb.com.uyusa;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.Cliente;

public class FormCarteraInformacion extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_cartera_informacion);

        cargarCartera();
    }

    public void cargarCartera() {

        celuweb.com.BusinessObject.FileBO.validarCliente(this);
        Vector<celuweb.com.DataObject.Cartera> listaCartera = DataBaseBO.listaCartera(Main.cliente.codigo);

        double total = 0;
        double totalVencido = 0;
        double saldoVencido = 0;
        String cupoCliente = "";

        if (listaCartera.size() > 0) {

            TableLayout table = new TableLayout(this);
            table.setBackgroundColor(Color.WHITE);

            //String[] cabecera = { getString(R.string.documento), getString(R.string.saldo), getString(R.string.abono),getString(R.string.prompt_concepto), getString(R.string.fecha), getString(R.string.vencimiento), getString(R.string.dias), getString(R.string.nro_fiscal) };
            String[] cabecera = {getString(R.string.documento),
                    getString(R.string.saldo), getString(R.string.saldo_vencido), getString(R.string.abono), getString(R.string.prompt_concepto),
                    getString(R.string.fecha), getString(R.string.vencimiento), getString(R.string.dias)};
            Util.Headers(table, cabecera, this);

            HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollCartera);
            scroll.removeAllViews();
            scroll.addView(table);

            for (Cartera cartera : listaCartera) {

                TextView textViewAux;
                TableRow fila = new TableRow(this);

                textViewAux = new TextView(this);
                textViewAux.setText(cartera.documento + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(Util.SepararMiles(Util.getDecimalFormatCartera(cartera.saldo)) + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(Util.SepararMiles(Util.getDecimalFormatCartera(cartera.saldoVencido)) + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(Util.SepararMiles(Util.Redondear("" + cartera.abono, 0)) + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(cartera.concepto + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                textViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(cartera.fecha + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(cartera.FechaVecto + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(cartera.dias + "\n");
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                //textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                fila.addView(textViewAux);
				
				/*textViewAux = new TextView(this);
				textViewAux.setText( cartera.nroFiscal+"\n");			
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
				//textViewAux.setTextSize(19);
				//textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
				fila.addView(textViewAux);*/

                total += (cartera.saldo - cartera.abono);
                //saldoVencido += cartera.saldoVencido;
                cupoCliente = cartera.cupoCliente;
                if (cartera.dias > 0)
                    totalVencido = totalVencido + (cartera.saldo - cartera.abono);


                table.addView(fila);
            }

            //((EditText)(findViewById(R.id.txtSaldoCartera))).setText(Util.SepararMiles(Util.Redondear(total+"", 0)));
            //((EditText)(findViewById(R.id.txtSaldoCarteraVencida))).setText(Util.SepararMiles(Util.Redondear(totalVencido+"", 0)));


            ((EditText) (findViewById(R.id.txtCupoCartera))).setText(Util.SepararMiles((Util.getDecimalFormatCartera(Double.parseDouble(cupoCliente)))));
            ((EditText) (findViewById(R.id.txtSaldoCartera))).setText(Util.SepararMiles(Util.getDecimalFormatCartera(total)));
            ((EditText) (findViewById(R.id.txtSaldoCarteraVencida))).setText(Util.SepararMiles(Util.getDecimalFormatCartera(totalVencido)));


        }
    }

    public void OnClickSalir(View view) {

        finish();

    }


    @Override
    protected void onResume() {

        super.onResume();

        // Main.usuario = null;
        // Main.cliente = null;

        if (Main.usuario == null || Main.usuario.codigoVendedor == null
                || Main.usuario.bodega == null) {

            DataBaseBO.CargarInfomacionUsuario();

        }

        if (Main.cliente == null || Main.cliente.codigo == null) {

            int tipoDeClienteSelec = DataBaseBO
                    .ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;

            if (tipoDeClienteSelec == 1) {

                clienteSel = DataBaseBO.CargarClienteSeleccionado();

            } else {

                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();

            }

            if (clienteSel != null)
                Main.cliente = clienteSel;

        }

    }

    public void onclickPdf(View view){
        // TODO Auto-generated method stub
        try {
            createPDF();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public void createPDF() throws FileNotFoundException, DocumentException {
         PdfPCell cell;
         String textAnswer;
         Image bgImage;

         String path = null;
         File dir = null;
         final File file;

        //use to set background color
        BaseColor myColor = WebColors.getRGBColor("#9E9E9E");
        BaseColor myColor1 = WebColors.getRGBColor("#9E9E9E");
        //create document file
        Document doc = new Document();
        //creating new file path
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PDF Files";
        dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {

            Log.e("PDFCreator", "PDF Path: " + path);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            file = new File(dir, Main.cliente.codigo+"-"+ sdf.format( Calendar.getInstance().getTime()) + ".pdf");
            if ( file.exists() ){
                file.delete();
            }
            FileOutputStream fOut = new FileOutputStream(file);
            PdfWriter writer = PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();
            //create table
            PdfPTable pt = new PdfPTable(3);
            pt.setWidthPercentage(100);
            float[] fl = new float[]{20, 75, 35};
            pt.setWidths(fl);
            cell = new PdfPCell();
            cell.setBorder( Rectangle.NO_BORDER);

            //set drawable in cell
            Drawable myImage = FormCarteraInformacion.this.getResources().getDrawable(R.drawable.logo_menu);
            Bitmap bitmap = ((BitmapDrawable) myImage).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bitmapdata = stream.toByteArray();
            try {
                bgImage = Image.getInstance(bitmapdata);
                bgImage.setAbsolutePosition(330f, 642f);
                cell.addElement(bgImage);
                pt.addCell(cell);
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                String header=Main.cliente.codigo+"\n"+Main.cliente.Nombre+"\nDireccion: "
                        +Main.cliente.direccion+"\nTelefono: "+Main.cliente.Telefono+"\nCiudad: "
                        +Main.cliente.Ciudad+"\nVendedor: "+Main.usuario.codigoVendedor+"-"+Main.usuario.nombreVendedor;

                Paragraph p = new Paragraph(
                        header,
                        new Font(Font.FontFamily.HELVETICA, 12));
                cell.addElement(new Paragraph(p));

                cell.addElement(new Paragraph(""));
                cell.addElement(new Paragraph(""));
                pt.addCell(cell);
                cell = new PdfPCell(new Paragraph(""));
                cell.setBorder(Rectangle.NO_BORDER);
                pt.addCell(cell);

                PdfPTable pTable = new PdfPTable(1);
                pTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.setColspan(1);
                cell.addElement(pt);
                pTable.addCell(cell);
                PdfPTable table = new PdfPTable(9);

                float[] columnWidth = new float[]{11,16, 30, 30, 30, 60, 40,40,15};
                table.setWidths(columnWidth);


                cell = new PdfPCell();


                cell.setBackgroundColor(myColor);
                cell.setColspan(9);
                cell.addElement(pTable);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(" "));
                cell.setColspan(9);
                table.addCell(cell);
                cell = new PdfPCell();
                cell.setColspan(9);

                cell.setBackgroundColor(myColor1);

                 p = new Paragraph(
                        "#",
                        new Font(Font.FontFamily.HELVETICA, 8));

                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);

                p = new Paragraph(
                        getString(R.string.documento),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);

                p = new Paragraph(
                        getString(R.string.saldo),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);


                p = new Paragraph(
                        getString(R.string.saldo_vencido),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);


                p = new Paragraph(
                        getString(R.string.abono),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);


                p = new Paragraph(
                        getString(R.string.prompt_concepto),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);


                p = new Paragraph(
                        getString(R.string.fecha),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);


                p = new Paragraph(
                        getString(R.string.vencimiento),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);


                p = new Paragraph(
                        getString(R.string.dias),
                        new Font(Font.FontFamily.HELVETICA, 8));
                cell = new PdfPCell(p);
                cell.setBackgroundColor(myColor1);
                table.addCell(cell);

                //table.setHeaderRows(3);
                cell = new PdfPCell();
                cell.setColspan(9);

                long total=0;
                Vector<Cartera> listaCartera = DataBaseBO.listaCartera(Main.cliente.codigo);
                int i=1;
                for (Cartera cartera:listaCartera) {
                    p = new Paragraph(
                            String.valueOf(i),
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            cartera.documento + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            Util.SepararMiles(Util.getDecimalFormatCartera(cartera.saldo)) + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            Util.SepararMiles(Util.getDecimalFormatCartera(cartera.saldoVencido)) + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            Util.SepararMiles(Util.getDecimalFormatCartera(cartera.abono)) + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            cartera.concepto + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            cartera.fecha + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            cartera.FechaVecto + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);

                    p = new Paragraph(
                            cartera.dias + "\n",
                            new Font(Font.FontFamily.HELVETICA, 8));
                    table.addCell(p);


                    total += (cartera.saldo - cartera.abono);
                    i++;

                }

                PdfPTable ftable = new PdfPTable(9);
                ftable.setWidthPercentage(100);
                float[] columnWidthaa = new float[]{30, 30, 30, 10, 30, 10,10,10,10};
                ftable.setWidths(columnWidthaa);
                cell = new PdfPCell();
                cell.setColspan(9);
                cell.setBackgroundColor(myColor1);

                p = new Paragraph(
                        "Total Cartera",
                        new Font(Font.FontFamily.HELVETICA, 12));

                cell = new PdfPCell(p);
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase(""));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);

                p = new Paragraph(
                        Util.SepararMiles(Util.getDecimalFormatCartera(total)),
                        new Font(Font.FontFamily.HELVETICA, 13));

                cell = new PdfPCell(p);
                cell.setColspan(9);
                ftable.addCell(cell);
                cell = new PdfPCell();
                cell.setColspan(9);
                cell.addElement(ftable);
                table.addCell(cell);
                doc.add(table);


                AlertDialog alertDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(FormCarteraInformacion.this);
                builder.setCancelable(false).setPositiveButton("Abrir", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType( Uri.fromFile(file), "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getApplicationContext(), "No existe una aplicaci?n para abrir el PDF", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                alertDialog = builder.create();
                alertDialog.setTitle( "PDF" );
                alertDialog.setMessage("Pdf Creado Correctamente, Abrir Pdf?");
                alertDialog.setIcon(R.drawable.inform);
                alertDialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                alertDialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.inform);
                alertDialog.show();

            } catch (DocumentException de) {
                Log.e("PDFCreator", "DocumentException:" + de);
                Util.MostrarAlertDialog( FormCarteraInformacion.this,"Error Creando el PDF" );
            } catch (IOException e) {
                Log.e("PDFCreator", "ioException:" + e);
                Util.MostrarAlertDialog( FormCarteraInformacion.this,"Error Creando el PDF" );
            } finally {
                doc.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
