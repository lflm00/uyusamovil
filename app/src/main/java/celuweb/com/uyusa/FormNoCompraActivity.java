package celuweb.com.uyusa;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.MotivoCompra;
import celuweb.com.UI.OnClickListenerImpl;

public class FormNoCompraActivity extends Activity implements Sincronizador {

    private Button btnAceptarFormNoCompra;

    int anchoImg, altoImg;

    ProgressDialog progressDialog;
    Vector<MotivoCompra> listaMotivosNoCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_no_compra);

        Inicializar();
        CargarMotivosNoCompra();
    }

    public void Inicializar() {

        this.btnAceptarFormNoCompra = (Button) findViewById(R.id.btnAceptarFormNoCompra);
        this.btnAceptarFormNoCompra.setOnClickListener(onClickGuardarNoCompra);
		
		/*Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int ancho = display.getWidth();
		int alto = display.getHeight();
		
		anchoImg = (ancho * 120) / 240;
		altoImg = (alto * 150) / 320;
		
		if (Main.fotoNoCompra != null) {
			
			((ImageView)findViewById(R.id.imageFoto)).setImageDrawable(Main.fotoNoCompra);
			
		} else {
			
			SetPhotoDefault();
		}*/
    }

    public void SetPhotoDefault() {

        Drawable fotoVacia = getResources().getDrawable(R.drawable.foto_vacia);
        Drawable img = Util.ResizedImage(fotoVacia, anchoImg, altoImg);

        if (img != null)
            ((ImageView) findViewById(R.id.imageFoto)).setImageDrawable(img);
    }

    public void OnClickFormNoCompra(View view) {

        switch (view.getId()) {

            case R.id.btnAceptarFormNoCompra:
                GuardarMotivoNoCompra();
                break;

            case R.id.btnCancelarFormNoCompra:
                Finalizar();
                break;
        }
    }

    public void Finalizar() {

        Main.fotoNoCompra = null;
        //BorrarFotoCapturada();
        finish();
    }

    public void OnClickTomarFoto(View view) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String filePath = Util.DirApp().getPath() + "/foto.jpg";

        Uri output = Uri.fromFile(new File(filePath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
        startActivityForResult(intent, Const.RESP_TOMAR_FOTO);
    }

    public void OnClickEliminarFoto(View view) {

        if (Main.fotoNoCompra != null) {

            Main.fotoNoCompra = null;
            SetPhotoDefault();
        }
    }
	
	/*public void OnClickTomarFotos(View view) {
		
		if (Main.encabezado.numero_doc == null)
			Main.encabezado.numero_doc = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor);
		
		Intent intent = new Intent(this, FormFotos.class);
		intent.putExtra("nroDoc", Main.encabezado.numero_doc);
		intent.putExtra("codCliente", Main.cliente.codigo);
		intent.putExtra("codVendedor", Main.usuario.codigoVendedor);
		
		startActivity(intent);
	}*/

    public void CargarMotivosNoCompra() {

        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();
        listaMotivosNoCompra = DataBaseBO.ListaMotivosNoCompra(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbNoCompra);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public boolean GuardarMotivoNoCompra() {

        //if (Main.fotoNoCompra == null) {
//		if( false ){
//			
//			MostrarAlertDialog("Para Registar la No compra, Debe primero Capturar la Foto!");
//			return false;
//			
//		} else {

        Spinner spinner = (Spinner) findViewById(R.id.cbNoCompra);
        int position = spinner.getSelectedItemPosition();

        if (position != AdapterView.INVALID_POSITION && listaMotivosNoCompra.size() > 0) {

            //Drawable imgFoto = ResizedImage(320, 480);
				
				/*if (imgFoto != null) {
					
					Bitmap bitmap = ((BitmapDrawable)imgFoto).getBitmap();
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
					byte[] byteArray = stream.toByteArray();*/

            MotivoCompra motivoCompra = listaMotivosNoCompra.elementAt(position);
					
					/*Main.encabezado.codigo_cliente  = Main.cliente.codigo;
					Main.encabezado.nombre_cliente  = Main.cliente.nombre;
					Main.encabezado.razon_social    = Main.cliente.razonSocial;
					Main.encabezado.codigo_novedad  = motivoCompra.codigo;
					Main.encabezado.lista_precio    = Main.cliente.listaPrecio;
					Main.encabezado.valor_neto      = 0;			
					Main.encabezado.hora_final      = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
					Main.encabezado.tipo_cliente    = "V";
					Main.encabezado.observacion     = "";*/

            if (Main.encabezado.numero_doc == null)
                Main.encabezado.numero_doc = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor);  //Numero Unico

            String version = ObtenerVersion();
            String imei = ObtenerImei();

            Main.encabezado.codigo_novedad = motivoCompra.codigo;
            Main.encabezado.codigo_cliente = Main.cliente.codigo;
            Main.encabezado.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); //Hora en finaliza la Toma del Pedido


            Coordenada coordenada = Coordenada.get(FormNoCompraActivity.this);

            if (coordenada == null) {

                //El GPS esta ON y aun no ha capturado coordenada.
                DataBaseBO.validarUsuario();

                if (Main.usuario != null && Main.usuario.codigoVendedor != null) {

                    coordenada = new Coordenada();
                    coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                    coordenada.codigoCliente = Main.cliente.codigo;
                    coordenada.latitud = 0;
                    coordenada.longitud = 0;
                    coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                    coordenada.estado = Coordenada.ESTADO_GPS_SIN_RESPUESTA;
                    coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                    coordenada.fecha = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

                }
            }


            //if (DataBaseBO.GuardarNoCompra(Main.encabezado, version, imei, byteArray)) {
            if (DataBaseBO.GuardarNoCompra(Main.encabezado)) {

                DataBaseBO.guardarCoordenada(coordenada, Main.encabezado.numero_doc);


                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Novedad registrada con exito " + "para el cliente " + Main.cliente.Nombre)
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                DataBaseBO.ActualizaEstadoRutero(Main.cliente.codigo);
//								
                                dialog.cancel();
                                onClickGuardarNoCompra.reset();

                                FormNoCompraActivity.this.setResult(RESULT_OK);
                                Finalizar();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

                return true;

            } else {

                MostrarAlertDialog("No se pudo Registrar la No Compra!");
                return false;
            }

        } else {

            MostrarAlertDialog("No se pudo Procesar la Imagen. Por Favor intente nuevamente!");
            return false;
        }
				
			/*} else {
				
				MostrarAlertDialog("Para Registrar la No compra, Debe primero Capturar la Foto!");
				return false;
			}*/
//		}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Const.RESP_TOMAR_FOTO && resultCode == RESULT_OK) {

            Main.fotoNoCompra = ResizedImage(anchoImg, altoImg);

            if (Main.fotoNoCompra != null) {

                //ImageView imgFoto = (ImageView)findViewById(R.id.imageFoto);
                //imgFoto.setImageDrawable(Main.fotoNoCompra);
            }

            //BorrarFotoCapturada();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Finalizar();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
	
	/*public void CaptureImage() {
		
		int rotation =-1;
		File fileImg = new File(Util.DirApp(), "foto.jpg");
	    long fileSize = fileImg.length();

	    Cursor mediaCursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, 
	    								  new String[] {MediaStore.Images.ImageColumns.ORIENTATION, MediaStore.MediaColumns.SIZE }, 
	    								  MediaStore.MediaColumns.DATE_ADDED + ">=?", 
	    								  new String[]{String.valueOf(captureTime/1000 - 1)}, 
	    								  MediaStore.MediaColumns.DATE_ADDED + " desc");

	    if (mediaCursor != null && captureTime != 0 && mediaCursor.getCount() !=0 ) {
	    	
	        while(mediaCursor.moveToNext()){
	            long size = mediaCursor.getLong(1);
	            //Extra check to make sure that we are getting the orientation from the proper file
	            if(size == fileSize){
	                rotation = mediaCursor.getInt(0);
	                break;
	            }
	        }
	    }

	}*/

    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

        final String mensaje = ok ? "Novedad Registrada con Exito en el servidor" : msg;

        if (progressDialog != null)
            progressDialog.cancel();

        this.runOnUiThread(new Runnable() {

            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(FormNoCompraActivity.this);
                builder.setMessage(mensaje)

                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                                Main.encabezado = new Encabezado();

                                FormNoCompraActivity.this.setResult(RESULT_OK);
                                Finalizar();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public Drawable ResizedImage(int newWidth, int newHeight) {

        Matrix matrix;
        FileInputStream fd = null;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;

        try {

            File fileImg = new File(Util.DirApp(), "foto.jpg");

            if (fileImg.exists()) {

                fd = new FileInputStream(fileImg.getPath());
                bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());

                int width = bitmapOriginal.getWidth();
                int height = bitmapOriginal.getHeight();

                //Matrix mtx = new Matrix();

                /**
                 * Si la Orientacion es 90 Grados, se rota la Imagen
                 **/
                //int orientacion = ObtenerOrientacionImg();
                //if (orientacion == 90)
                //	mtx.postRotate(90);

                //Bitmap rotatedBMP = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, mtx, true);

                //width = rotatedBMP.getWidth();
                //height = rotatedBMP.getHeight();

                if (width == newWidth && height == newHeight) {

                    //return new BitmapDrawable(rotatedBMP);
                    return new BitmapDrawable(bitmapOriginal);
                }

                // Reescala el Ancho y el Alto de la Imagen
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                // Crea la Imagen con el nuevo Tamano
                resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);

                //width = resizedBitmap.getWidth();
                //height = resizedBitmap.getHeight();

                //Matrix mtx = new Matrix();
                //int orientacion = ObtenerOrientacionImg();
                //if (orientacion == 90)
                //	mtx.postRotate(90);

                //Bitmap rotatedBMP = Bitmap.createBitmap(resizedBitmap, 0, 0, width, height, mtx, true);

                return new BitmapDrawable(resizedBitmap);
            }

            return null;

        } catch (Exception e) {

            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
            return null;

        } finally {

            if (fd != null) {

                try {

                    fd.close();

                } catch (IOException e) {
                }
            }

            fd = null;
            matrix = null;
            resizedBitmap = null;
            bitmapOriginal = null;
            System.gc();
        }
    }

    public int ObtenerOrientacionImg() {

        String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};

        int orientacion = 0;
        Cursor cursor = null;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        try {

            if (uri != null) {

                cursor = managedQuery(uri, projection, null, null, null);
            }

            if (cursor != null && cursor.moveToLast()) {

                orientacion = cursor.getInt(0);
                Log.i("ObtenerOrientacionImg", "ORIENTATION = " + orientacion);
            }

        } finally {

            if (cursor != null)
                cursor.close();
        }

        return orientacion;
    }

    public void BorrarFotoCapturada() {

        String[] projection = {MediaStore.Images.ImageColumns.SIZE,
                MediaStore.Images.ImageColumns.DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATA,
                BaseColumns._ID,
        };

        Cursor cursor = null;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        try {

            if (uri != null) {

                cursor = managedQuery(uri, projection, null, null, null);
            }

            if (cursor != null && cursor.moveToLast()) {

                ContentResolver contentResolver = getContentResolver();
                int rows = contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, BaseColumns._ID + "=" + cursor.getString(3), null);

                Log.i("BorrarFotoCapturada", "Numero de filas eliminadas : " + rows);
            }

        } finally {

            if (cursor != null)
                cursor.close();
        }
    }

    public String ObtenerVersion() {

        String version;

        try {

            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

        } catch (NameNotFoundException e) {

            version = "0.0";
            Log.e("FormNoCompraActivity", e.getMessage(), e);
        }

        return version;
    }

    public String ObtenerImei() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
    }

    public void MostrarAlertDialog(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
                onClickGuardarNoCompra.reset();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    private OnClickListenerImpl onClickGuardarNoCompra = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            GuardarMotivoNoCompra();
        }
    };


    @Override
    protected void onResume() {

        super.onResume();

        //Main.usuario = null;
        //Main.cliente = null;

        if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {

            DataBaseBO.CargarInfomacionUsuario();

        }


        if (Main.cliente == null || Main.cliente.codigo == null) {

            int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;

            if (tipoDeClienteSelec == 1) {

                clienteSel = DataBaseBO.CargarClienteSeleccionado();

            } else {


                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();


            }

            if (clienteSel != null)
                Main.cliente = clienteSel;


        }


    }

    public void onClickRegresar(View view) {

        finish();

    }


}
