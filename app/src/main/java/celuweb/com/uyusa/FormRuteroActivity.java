package celuweb.com.uyusa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;

import java.io.ByteArrayOutputStream;
import java.util.Locale;
import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
//import celuweb.com.DataObject.CicloVisita;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Foto;
import celuweb.com.DataObject.HabeasData;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;

public class FormRuteroActivity extends CustomActivity /*implements GPS*/ implements Sincronizador, View.OnClickListener {
  
  int ciudadSeleccionada;
  boolean primerEjecucion = true;
  
  Vector<String> listaDiasRutero;
  Vector<Cliente> listaClientes;
  Vector<String> listaDiasRutero2;
  Spinner spDiasRutero;
  private long mLastClickTime = 0;
  Dialog dialogEnviarInfo;
  ProgressDialog progressDialog;
  int posicionActual = 0;
  private EditText edtBusqueda;
  Dialog dialogoFormularioTerminos;
  Dialog dialogoTerminos;
  private byte[] firma;
  Bitmap bitmap = null;
  Cliente cliente = null;
  Usuario usuario = null;
  //Vector<CicloVisita> listaCiclos;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.form_rutero);
    DataBaseBO.CargarInfomacionUsuario();
    DataBaseBO.eliminarClientePropio(Main.usuario.codigoVendedor);
    cargarAccionBusqueda();
    //cargarCicloVisita();
    SetListenerListView();
    //LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
    //HandlerCoordenadas handlerCoor = new HandlerCoordenadas(locationManager, this);
    //Thread thread = new Thread(handlerCoor);
    //thread.start();
  }
  
  private void cargarAccionBusqueda() {
    edtBusqueda = (EditText) findViewById(R.id.edtBusqueda);
    edtBusqueda.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      }
      
      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() > 2) {
          validarTipoRutero();
          
        } else if (charSequence.length() == 0) {
          validarTipoRutero();
        }
        
      }
      
      @Override
      public void afterTextChanged(Editable editable) {
        //				if (editable.length() > 2) {
        //					validarTipoRutero();
        //
        //				} else if (editable.length() == 0) {
        //					validarTipoRutero();
        //				}
      }
    });
  }
  
  private void validarTipoRutero() {
    Usuario usuario = DataBaseBO.ObtenerUsuario();
    if (usuario.tipoRutero != 0) {
      Log.d("TIPORUTERO","!0");
      CargarDiasRutero2();
      int diaActual = Util.ObtenerDiaSemanaNumeroEntero();
      spDiasRutero = (Spinner) findViewById(R.id.cbDiasRutero);
      spDiasRutero.setSelection(diaActual);
      CargarClientesRutero2();
      
    } else {
      Log.d("TIPORUTERO","0");
      CargarDiasRutero();
      
    }
  }
  
  @Override
  protected void onResume() {
    // TODO Auto-generated method stub
    super.onResume();
    validarTipoRutero();
    Util.cargarIconoEditText(this, edtBusqueda, FontAwesome.Icon.faw_search, R.color.gray, R.color.gray, Const.SIZEICON1);
  }
  
  private void inicializarSpinnerDia(int diaSemanaRutero) {
    spDiasRutero = (Spinner) findViewById(R.id.cbDiasRutero);
    spDiasRutero.setSelection(diaSemanaRutero);
  }
  
  //@Override
  public void respGPS(Location location) {
    Toast.makeText(getBaseContext(), "Coor: " + location.getLatitude() + " - " + location.getLongitude(), Toast.LENGTH_LONG).show();
  }

  /*
  public void cargarCicloVisita(){

    listaCiclos = DataBaseBOJF.listaCiclosVisita("");

    if (listaCiclos.size() > 0) {

      LinearLayout linerCuerpo = (LinearLayout) findViewById(R.id.linerCuerpo);
      linerCuerpo.removeAllViews();

      for (int i = 0; i < listaCiclos.size(); i++) {

        final CicloVisita c = listaCiclos.get(i);
        final int pos = i;

        final TableRow headerView = (TableRow) LayoutInflater.from(this).inflate(R.layout.fila_doc, linerCuerpo, false);

////        headerView.setBackgroundResource(R.drawable.circulo);
        Button btn=((Button) headerView.findViewById(R.id.btn));
//        GradientDrawable gd = (GradientDrawable) btn.getBackground().getCurrent();
//        gd.setColor(Color.parseColor(c.color));

        SomeDrawable drawable = new SomeDrawable(Color.parseColor(c.color),Color.parseColor(c.color),Color.parseColor(c.color),1,Color.BLACK,00);
        btn.setBackgroundDrawable(drawable);

        ((TextView) headerView.findViewById(R.id.txtDesc)).setText(c.rango);
        ((TextView) headerView.findViewById(R.id.txtDesc)).setMaxLines(3);
        ((TextView) headerView.findViewById(R.id.txtDesc)).setSingleLine(false);

        ((TextView) headerView.findViewById(R.id.txtCant)).setText(c.cantidad);
        ((TextView) headerView.findViewById(R.id.txtCant)).setMaxLines(3);
        ((TextView) headerView.findViewById(R.id.txtCant)).setSingleLine(false);

        linerCuerpo.addView(headerView);
      }
    }
  }
   */

  public void CargarDiasRutero() {
    System.out.println("Entro A CargarDiasRutero");
    String[] items;
    listaDiasRutero = DataBaseBO.ListaDiasRutero();
    if (listaDiasRutero.size() > 0) {
      items = new String[listaDiasRutero.size()];
      listaDiasRutero.copyInto(items);
      
    } else {
      items = new String[]{};
      if (listaDiasRutero != null) listaDiasRutero.removeAllElements();
    }
    spDiasRutero = (Spinner) findViewById(R.id.cbDiasRutero);
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spDiasRutero.setAdapter(adapter);
    spDiasRutero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      
      @Override
      public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        //        		if (primerEjecucion) {
        //
        //        			primerEjecucion = false;
        //
        //        		} else {
        //ciudadSeleccionada = position;
        CargarClientesRutero();
        //        		}
      }
      
      @Override
      public void onNothingSelected(AdapterView<?> parentView) {
      }
    });
  }
  
  public void CargarDiasRutero2() {
    String[] items;
    listaDiasRutero2 = new Vector<String>();
    listaDiasRutero2.add("DOMINGO");
    listaDiasRutero2.add("LUNES");
    listaDiasRutero2.add("MARTES");
    listaDiasRutero2.add("MIERCOLES");
    listaDiasRutero2.add("JUEVES");
    listaDiasRutero2.add("VIERNES");
    listaDiasRutero2.add("SABADO");
    listaDiasRutero2.add("SIN RUTA");
    if (listaDiasRutero2.size() > 0) {
      items = new String[listaDiasRutero2.size()];
      listaDiasRutero2.copyInto(items);
      
    } else {
      items = new String[]{};
      if (listaDiasRutero2 != null) listaDiasRutero2.removeAllElements();
    }
    spDiasRutero = (Spinner) findViewById(R.id.cbDiasRutero);
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spDiasRutero.setAdapter(adapter);
    spDiasRutero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      
      @Override
      public void onItemSelected(AdapterView<?> parent, View selectedItemView, int position, long id) {
        if (primerEjecucion) {
          primerEjecucion = false;
          
        } else {
          CargarClientesRutero2();
        }
      }
      
      @Override
      public void onNothingSelected(AdapterView<?> parentView) {
      }
    });
    
  }
  
  private void CargarClientesRutero() {
    ItemListView[] items;
    String busqueda = edtBusqueda.getText().toString();
    String dia = ((Spinner) findViewById(R.id.cbDiasRutero)).getSelectedItem().toString();
    Main.rutaEscogidaVendedor = dia;
    String dia2 = ((Spinner) findViewById(R.id.cbDiasRutero)).getSelectedItemPosition() + "";
    Vector<ItemListView> listaItems = new Vector<ItemListView>();
    listaClientes = DataBaseBO.ListaClientesRutero(listaItems, dia, busqueda);
    if (listaItems.size() > 0) {
      items = new ItemListView[listaItems.size()];
      listaItems.copyInto(items);
      
    } else {
      items = new ItemListView[]{};
      if (listaClientes != null) listaClientes.removeAllElements();
    }
    ListViewAdapterRemarca adapter = new ListViewAdapterRemarca(this, items, R.drawable.cliente, 0x2E65AD);
    ListView listaClienteRutero = (ListView) findViewById(R.id.listaClienteRutero);
    listaClienteRutero.setAdapter(adapter);
    ((TextView) findViewById(R.id.lalNumClientesRutero)).setText(getString(R.string.no_clientes_) + String.valueOf(listaClientes.size()));
  }
  
  private void CargarClientesRutero2() {
    ItemListView[] items;
    int posDia = ((Spinner) findViewById(R.id.cbDiasRutero)).getSelectedItemPosition();
    String busqueda = edtBusqueda.getText().toString();
    String diaRuta2 = String.valueOf(cargaRuta(posDia));
    Vector<ItemListView> listaItems = new Vector<ItemListView>();
    listaClientes = DataBaseBO.ListaClientesRutero(listaItems, diaRuta2, busqueda);
    if (listaItems.size() > 0) {
      items = new ItemListView[listaItems.size()];
      listaItems.copyInto(items);
      
    } else {
      items = new ItemListView[]{};
      if (listaClientes != null) listaClientes.removeAllElements();
    }
    ListViewAdapterRemarca adapter = new ListViewAdapterRemarca(this, items, R.drawable.drw_op_cliente, 0x2E65AD);
    ListView listaClienteRutero = (ListView) findViewById(R.id.listaClienteRutero);
    listaClienteRutero.setAdapter(adapter);
    ((TextView) findViewById(R.id.lalNumClientesRutero)).setText(getString(R.string.no_clientes_) + String.valueOf(listaClientes.size()));
  }
  
  private String cargaRuta(int position) {
    String strDia = "";
    switch (position) {
      case 0:
        strDia = "DOMINGO";
        break;
      case 1:
        strDia = "LUNES";
        break;
      case 2:
        strDia = "MARTES";
        break;
      case 3:
        strDia = "MIERCOLES";
        break;
      case 4:
        strDia = "JUEVES";
        break;
      case 5:
        strDia = "VIERNES";
        break;
      case 6:
        strDia = "SABADO";
        break;
      default:
        strDia = "SIN RUTA";
    }
    return strDia;
  }
  
  public void SetListenerListView() {
    ListView listaOpciones = (ListView) findViewById(R.id.listaClienteRutero);
    listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      
      @Override
      public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        
				/* Evitar evento de doble click */
        if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
          return;
        }
        posicionActual = position;
        mLastClickTime = SystemClock.elapsedRealtime();
        if (DataBaseBO.HayInformacionXEnviar1()) {
          cliente = listaClientes.elementAt(position);
          progressDialog = ProgressDialog.show(FormRuteroActivity.this, "", "Enviando Informacion...", true);
          progressDialog.show();
          Sync sync = new Sync(FormRuteroActivity.this, Const.ENVIAR_PEDIDO);
          sync.start();
          //					Util.MostrarAlertDialog(FormRuteroActivity.this,"Hay informacion pendiente por Enviar.\nAntes de ingresar al cliente es necesario enviar Informacion");
        } else {
          cliente = listaClientes.elementAt(position);
          if (!DataBaseBO.validarHabeasData(cliente.codigo)){
            AlertDialog alertDialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(FormRuteroActivity.this);
            builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){
              public void onClick(DialogInterface dialog, int id){
                dialog.cancel();
                mostrarDialogoFormularioTerminos();
              }
            }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener(){
              public void onClick(DialogInterface dialog, int id){
                dialog.cancel();
                CargarInformacionCliente(cliente);
              }
            });
            alertDialog = builder.create();
            alertDialog.setMessage("El cliente no posee tratamiento de datos.\nDesea realizar la autorizacion del tratamiento de datos?");
            alertDialog.show();
          }else{
            CargarInformacionCliente(cliente);
          }
        }
      }
    });
  }
  
  
  
  public void mostrarDialogoFormularioTerminos(){
    
    if (dialogoFormularioTerminos == null){
      dialogoFormularioTerminos = new Dialog(this);
      dialogoFormularioTerminos.setContentView(R.layout.dialog_formulario_terminos);
      dialogoFormularioTerminos.setTitle("Formulario Terminos y Condiciones");
      dialogoFormularioTerminos.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      //String resumen = DataBaseBO.obtenerTerminosHabeasData().substring(0,30);
      String resumen = DataBaseBO.obtenerTerminosHabeasData();
      WebView myWebViewRDT = (WebView) dialogoFormularioTerminos.findViewById(R.id.webViewResumenDescripcionTerminos);
      myWebViewRDT.loadData(resumen, "text/html; charset=UTF-8", "UTF-8");
    }
    EditText edtxObservaciones = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxObservaciones);
    edtxObservaciones.setText("");
    EditText edtxEmail = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxEmail);
    edtxEmail.setText("");
    String fecha = Util.FechaActual("yyyy-MM-dd");
    TextView txtFecha = (TextView) dialogoFormularioTerminos.findViewById(R.id.txtFecha);
    txtFecha.setText(fecha);
    EditText edtxCiudad = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCiudad);
    edtxCiudad.setText(cliente.codCiudad + " - " + cliente.Ciudad);
    EditText edtxCedula = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCedula);
    edtxCedula.setText(cliente.codigo);
    String nombre = cliente.Nombre.toUpperCase(Locale.getDefault()).replace(".", " ");
    if (nombre.equals("")){
      nombre = cliente.razonSocial.toUpperCase(Locale.getDefault()).replace(".", " ");
    }
    EditText edtxNombre = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxNombre);
    edtxNombre.setText(nombre);
    TextView txtLeerTerminos = (TextView) dialogoFormularioTerminos.findViewById(R.id.txtLeerTerminos);
    txtLeerTerminos.setOnClickListener(this);
    Button btnAceptarFormularioTerminos = (Button) dialogoFormularioTerminos.findViewById(R.id.btnAceptarFormularioTerminos);
    btnAceptarFormularioTerminos.setTag(cliente);
    btnAceptarFormularioTerminos.setOnClickListener(this);
    Button btnCancelarFormularioTerminos = (Button) dialogoFormularioTerminos.findViewById(R.id.btnCancelarFormularioTerminos);
    btnCancelarFormularioTerminos.setOnClickListener(this);
    Button btnFotoFormularioTerminos = (Button) dialogoFormularioTerminos.findViewById(R.id.btnFotoFormularioTerminos);
    btnFotoFormularioTerminos.setTag(cliente);
    btnFotoFormularioTerminos.setOnClickListener(this);
    dialogoFormularioTerminos.setCancelable(false);
    dialogoFormularioTerminos.show();
    Util.closeTecladoStartActivity(FormRuteroActivity.this);
  }
  
  public void mostrarDialogoTerminos(){
    if (dialogoTerminos == null){
      dialogoTerminos = new Dialog(this);
      dialogoTerminos.setContentView(R.layout.dialog_terminos);
      dialogoTerminos.setTitle(getResources().getString(R.string.terminos));
      dialogoTerminos.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      Button btnAceptarTerminos = (Button) dialogoTerminos.findViewById(R.id.btnAceptarTerminos);
      btnAceptarTerminos.setOnClickListener(this);
      Button btnCancelarTerminos = (Button) dialogoTerminos.findViewById(R.id.btnCancelarTerminos);
      btnCancelarTerminos.setOnClickListener(this);
      String descipcion = DataBaseBO.obtenerTerminosHabeasData();
      WebView myWebViewDT = (WebView) dialogoTerminos.findViewById(R.id.webViewDescripcionTerminos);
      myWebViewDT.loadData(descipcion, "text/html; charset=UTF-8", "UTF-8");
    }
    dialogoTerminos.setCancelable(false);
    dialogoTerminos.show();
  }
  
  @Override
  public void onClick(final View v){
    switch (v.getId()){
      case R.id.txtLeerTerminos:
        mostrarDialogoTerminos();
        break;
      case R.id.btnAceptarFormularioTerminos:
        boolean habheasDataAceptado = false;
        RadioButton rdbAceptaTerminosSi = (RadioButton) dialogoFormularioTerminos.findViewById(R.id.rdbAceptaTerminosSi);
        RadioButton rdbAceptaTerminoNo = (RadioButton) dialogoFormularioTerminos.findViewById(R.id.rdbAceptaTerminosSi);
        RadioGroup rdgAceptaTerminos = (RadioGroup) dialogoFormularioTerminos.findViewById(R.id.rdgAceptaTerminos);
        int rdSeleccionado = rdgAceptaTerminos.getCheckedRadioButtonId();
        System.out.println("+++++++++++++++++++++ " + rdSeleccionado);
        if (rdSeleccionado <= 0){
          Util.MostrarAlertDialog(FormRuteroActivity.this, "Por favor seleccione si acepta o no la manipulacion de datos");
          return;
        }
        if (bitmap == null && rdbAceptaTerminosSi.isChecked()){
          Util.MostrarAlertDialog(FormRuteroActivity.this, "Por favor tome la firma");
          return;
        }
        if (rdbAceptaTerminosSi.isChecked()){
          habheasDataAceptado = true;
        }else if (rdbAceptaTerminoNo.isChecked()){
          habheasDataAceptado = false;
        }
        if (cliente == null){
          cliente = (Cliente) v.getTag();
        }
        gestionHabbeasData(habheasDataAceptado, cliente);
        break;
      case R.id.btnFotoFormularioTerminos:
        if (cliente == null){
          cliente = (Cliente) v.getTag();
        }
        if (firma != null || bitmap != null){
          AlertDialog alertDialog;
          AlertDialog.Builder builder = new AlertDialog.Builder(this);
          builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
              dialog.cancel();
              tomarFirma();
            }
          }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){
              dialog.cancel();
            }
          });
          alertDialog = builder.create();
          alertDialog.setMessage("Ya ha tomado una firma.\n?Desea firmar nuevamente?");
          alertDialog.show();
        }else{
          tomarFirma();
        }
        break;
      case R.id.btnCancelarFormularioTerminos:
        dialogoFormularioTerminos.cancel();
        break;
      case R.id.btnAceptarTerminos:
        dialogoTerminos.cancel();
        break;
      case R.id.btnCancelarTerminos:
        dialogoTerminos.cancel();
        break;
      default:
        break;
    }
  }
  
  
  public void tomarFirma(){
    String nombre = cliente.Nombre.toUpperCase(Locale.getDefault()).replace(".", " ");
    if (nombre.equals("")){
      nombre = cliente.razonSocial.toUpperCase(Locale.getDefault()).replace(".", " ");
    }
    Util.clearPrefence(FormRuteroActivity.this, Const.PREFERENCEFIRMA);
    String keysre[] = { "CODIGO", "NOMBRE" };
    String datosre[] = { cliente.codigo, nombre };
    Util.putPrefence(FormRuteroActivity.this, Const.PREFERENCEFIRMA, keysre, datosre);
    Intent formDibujoLibre = new Intent(this, FormDibujoLibre.class);
    startActivityForResult(formDibujoLibre, Const.RESP_FIRMA);
  }
  
  
  public void gestionHabbeasData(final boolean habheasDataAceptado, final Cliente cliente){
    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
    builder1.setMessage("?Desea almacenar la autorizacion?").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){
      public void onClick(DialogInterface dialog, int id){
        dialog.cancel();
        guardarHabbeasData(habheasDataAceptado, cliente);
      }
    }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener(){
      public void onClick(DialogInterface dialog, int id){
        dialog.cancel();
      }
    });
    AlertDialog alert1 = builder1.create();
    alert1.show();
		/*
		 * if (habheasDataAceptado) { guardarHabbeasData(habheasDataAceptado,
		 * cliente); }else{ }
		 */
  }
  
  public void guardarHabbeasData(final boolean habheasDataAceptado, final Cliente clienteIn){
    if (usuario == null){
      usuario = DataBaseBO.CargarUsuario();
    }
    EditText edtxCedula = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCedula);
    String cedula = edtxCedula.getText().toString();
    EditText edtxNombre = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxNombre);
    String nombre = edtxNombre.getText().toString();
    EditText edtxObservaciones = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxObservaciones);
    String observacion = edtxObservaciones.getText().toString();
    EditText edtxEmail = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxEmail);
    String email = edtxEmail.getText().toString();
    String numDoc = "A" + usuario.codigoVendedor + clienteIn.codigo + Util.FechaActual("yyyyMMddHHmmssSSS");;
    HabeasData habeasData = new HabeasData();
    habeasData.numDoc = numDoc;
    habeasData.codigoCliente = cedula;
    habeasData.nit = cedula;
    habeasData.nombreCliente = nombre;
    habeasData.codigoCiudad = cliente.Ciudad;
    habeasData.email = email;
    habeasData.autorizo = habheasDataAceptado == true ? 1 : 0;
    habeasData.nuevo = "0";
    habeasData.observacion = observacion;
    habeasData.codigoVendedor = usuario.codigoVendedor;
    boolean estadoHab = DataBaseBO.guardarHabeasData(habeasData);
    if (estadoHab){
      if (habheasDataAceptado){
        if (bitmap == null){
          Util.MostrarAlertDialog(FormRuteroActivity.this, "No se pudo almacenar la firma.\nPor favor intentelo nuevamente");
          return;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] byteArray = stream.toByteArray();
        if (byteArray == null){
          Util.MostrarAlertDialog(FormRuteroActivity.this, "No se pudo almacenar la firma.\nPor favor intentelo nuevamente");
          return;
        }
        Foto foto = new Foto();
        foto.imagen = byteArray;
        foto.nroDoc = numDoc;
        estadoHab = DataBaseBO.guardarFirmaHabeasData(foto);
      }
      if (estadoHab){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Autorizacion almacenada correctamente").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener(){
          public void onClick(DialogInterface dialog, int id){
            dialog.cancel();
            resetValoresDialogoFormularioTerminos();
            if (habheasDataAceptado){
              CargarInformacionCliente(clienteIn);
              dialogoFormularioTerminos.cancel();
            }else{
              dialogoFormularioTerminos.cancel();
            }
          }
        });
        AlertDialog alert1 = builder1.create();
        alert1.show();
      }else{
        Util.MostrarAlertDialog(FormRuteroActivity.this, "Ocurrio un error al almacenar la firma de uso de datos");
      }
    }else{
      Util.MostrarAlertDialog(FormRuteroActivity.this, "Ocurrio un error al almacenar la autorizacion de uso de datos");
    }
  }
  
  public void resetValoresDialogoFormularioTerminos(){
    bitmap = null;
    firma = null;
    RadioGroup rdgAceptaTerminos = (RadioGroup) dialogoFormularioTerminos.findViewById(R.id.rdgAceptaTerminos);
    rdgAceptaTerminos.clearCheck();
    EditText edtxCedula = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCedula);
    edtxCedula.setText("");
    EditText edtxNombre = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxNombre);
    edtxNombre.setText("");
    EditText edtxObservaciones = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxObservaciones);
    edtxObservaciones.setText("");
    EditText edtxEmail = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxEmail);
    edtxEmail.setText("");
  }
  
  public void CargarInformacionCliente(Cliente cliente) {
    Cliente clienteSel = cliente;
    boolean tienePedido = DataBaseBO.ExistePedidoCliente(clienteSel.codigo);
  
    Util.clearPrefence(this, Const.PREFERENCERE);
    String keysre[] = {"RUTERO", "CODIGOCLIENTE"};
    String datosre[] = {Const.RUTERO + "", clienteSel.codigo + ""};
    Util.putPrefence(this, Const.PREFERENCERE, keysre, datosre);
    
    
    if (tienePedido) {
      mensajeTienePedidos(clienteSel);
      
    } else {
      /*****
       Main.cliente = listaClientes.elementAt(position);
       
       Intent formInfoCliente = new Intent(FormRuteroActivity.this, FormInfoClienteActivity.class);
       startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);****/
      Usuario usuario = DataBaseBO.CargarUsuario();
      if (usuario == null) {
        //Util.MostrarAlertDialog(this, "No se pudo cargar la informacion del Usuario");
        Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del Usuario", Toast.LENGTH_LONG).show();
        
      } else {
        //Guarda la Referencia al Cliente seleccionado.
        Main.cliente = cliente;
        DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
        Intent formInfoCliente = new Intent(FormRuteroActivity.this, FormInfoClienteActivity.class);
        startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);
      }
    }
  }
  
  public void mensajeTienePedidos(final Cliente clienteSel) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage("El cliente " + clienteSel.razonSocial + " ya tiene un pedido para el dia de hoy.\n\nDesea tomar un nuevo pedido?");
    builder.setCancelable(false);
    builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
      
      public void onClick(DialogInterface dialog, int id) {
        Usuario usuario = DataBaseBO.CargarUsuario();
        if (usuario == null) {
          //Util.MostrarAlertDialog(this, "No se pudo cargar la informacion del Usuario");
          Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del Usuario", Toast.LENGTH_LONG).show();
          
        } else {
          if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            Main.cliente = clienteSel;
            DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
            if (DataBaseBO.GuardarSesionCliente(Main.cliente.codigo, Const.RUTERO)) {
              Intent formInfoCliente = new Intent(FormRuteroActivity.this, FormInfoClienteActivity.class);
              startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);
            } else {
              Util.MostrarAlertDialog(FormRuteroActivity.this, "Ocurrio un problema al cargar el cliente");
              
            }
            
          } else {
            Main.cliente = clienteSel;
            DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
            Intent formInfoCliente = new Intent(FormRuteroActivity.this, FormInfoClienteActivity.class);
            startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);
          }
          
        }
        dialog.dismiss();
        
      }
    });
    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
      
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });
    builder.create();
    builder.show();
  }
  
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
        case Const.RESP_PEDIDO_EXITOSO:
          CargarClientesRutero();
          break;
        case Const.RESP_FIRMA:
          procesarFirma(data);
          break;
      }
    }
  }
  
  public void procesarFirma(Intent data){
    try{
      String cedula = "";
      String nombre = "";
      String keysre[] = { "CODIGO", "NOMBRE" };
      String datosre[] = Util.getPrefence(FormRuteroActivity.this, Const.PREFERENCEFIRMA, keysre);
      if (datosre.length > 0){
        if (!datosre[0].equals("")){
          cedula = datosre[0];
        }
        if (!datosre[1].equals("")){
          nombre = datosre[1];
        }
      }
      if (cliente == null){
        cedula = cliente.codigo;
        nombre = cliente.Nombre.toUpperCase(Locale.getDefault()).replace(".", " ");
        if (nombre.equals("")){
          nombre = cliente.razonSocial.toUpperCase(Locale.getDefault()).replace(".", " ");
        }
      }
      String nombreMarcaAgua = cedula + " - " + nombre;
      firma = data.getByteArrayExtra("imagen");
      if (firma != null){
        bitmap = BitmapFactory.decodeByteArray(firma, 0, firma.length);
        if (bitmap != null){
          System.out.println("captura foto watermark 1");
          bitmap = Util.ResizedImageWaterMark(bitmap, nombreMarcaAgua, FormRuteroActivity.this);
          System.out.println("captura foto watermark 2");
        }
      }
    }
    catch (Exception e){
      System.out.println("Error escalando la Imagen: " + e.toString());
    }
    finally{}
  }
  
  public void onClickRegresar(View view) {
    finish();
  }
  
  @Override
  public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
    switch (codeRequest) {
      case Const.ENVIAR_PEDIDO:
        RespuestaEnviarInfo(ok, respuestaServer, msg);
        break;
    }
    
  }
  
  public void RespuestaEnviarInfo(boolean ok, String respuestaServer, String msg) {
    final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : "No se pudo registrar informacion.\nMotivo: Fallo de conexion.";
    if (progressDialog != null) progressDialog.cancel();
    this.runOnUiThread(new Runnable() {
      
      public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormRuteroActivity.this);
        builder.setMessage(mensaje).setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
          
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
            CargarInformacionCliente(cliente);
          }
        });
        AlertDialog alert = builder.create();
        alert.show();
      }
    });
  }
  
  public void OnClickMapa(View view) {
    Intent intentHome = new Intent(FormRuteroActivity.this, MapsActivity.class);
    int posDia = ((Spinner) findViewById(R.id.cbDiasRutero)).getSelectedItemPosition();
//    String diaRuta2 = String.valueOf(cargaRuta(posDia));
//    intentHome.putExtra("dia", diaRuta2);
    intentHome.putExtra("dia", posDia);

    startActivity(intentHome);
    
  }

  public class SomeDrawable extends GradientDrawable {

    public SomeDrawable(int pStartColor, int pCenterColor, int pEndColor, int pStrokeWidth, int pStrokeColor, float cornerRadius) {
      super(Orientation.BOTTOM_TOP,new int[]{pStartColor,pCenterColor,pEndColor});
      setShape(GradientDrawable.OVAL);
    }
  }
  
}
