package celuweb.com.uyusa;
import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import celuweb.com.DataObject.ItemListView;


/**
 * Adapter para llenar la lista de clientes que tienen agotados o devoluciones.
 * @author JICZ
 *
 */
public class ListViewClientesAgotyDevAdapter extends ArrayAdapter<ItemListView> {

	private Activity activity;
	private ArrayList<ItemListView> arrayItems;


	/**
	 * constructor
	 * @param activity
	 * @param arrayItems
	 */
	public ListViewClientesAgotyDevAdapter(Activity activity, ArrayList<ItemListView> arrayItems) {

		super(activity, R.layout.items_lista_clientes_agotadosydev, arrayItems);
		this.activity = activity;
		this.arrayItems = arrayItems;
	}


	/**
	 * getView optimizado para cargar gran numero de opciones encontradas, minimizando el consumo de recursos.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if(convertView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.items_lista_clientes_agotadosydev, null);

			//conifurar viewHolder (optimizacion)
			Row rowHolder = new Row();			
			rowHolder.codigo = (TextView) convertView.findViewById(R.id.textViewCodigo);			
			rowHolder.razonSocial = (TextView) convertView.findViewById(R.id.textViewRazonSocial);
			convertView.setTag(rowHolder);
		}

		//llenar datos
		Row holder = (Row) convertView.getTag();
		ItemListView item = arrayItems.get(position);
		if(item != null){
			holder.codigo.setText(item.titulo);
			holder.razonSocial.setText(item.subTitulo);
		}
		return convertView;
	}
	
	
	/**
	 * representa una fila, (ViewHolder).
	 * @author JICZ
	 *
	 */
	public static class Row {
		TextView codigo;
		TextView razonSocial;
	}
}
