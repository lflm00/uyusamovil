package celuweb.com.uyusa;

import java.util.Calendar;
import java.util.Locale;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.UI.OnClickListenerImpl;

public class FormCarteraFormasPago2 extends Activity implements OnClickListener {

	private Button btnTerminarFormaPago;

	private Spinner cbFormasDePago;

	private TextView lblDiferenciaFormasPago;

	String nroDoc = "";

	double totalRecuado = 0;

	Dialog dialogFormasPago;

	Vector<celuweb.com.DataObject.Banco> listaBancos;



	@Override
	protected void onCreate (Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_cartera_formas_pago);

		inicializar();
		CargarInformacion();
		CargarFormaPago();
		CargarTablaFormasPago();

	}



	public void inicializar () {

		this.btnTerminarFormaPago = (Button) findViewById(R.id.btnTerminarFormaPago);
		this.btnTerminarFormaPago.setOnClickListener(onClickTerminarRecaudo);

		//this.cbFormasDePago = (Spinner) findViewById(R.id.cbFormasDePago);
		//this.lblDiferenciaFormasPago = (TextView)findViewById(R.id.lblDiferenciaFormasPago);
	}



	public void CargarInformacion () {

		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {

			if (bundle.containsKey("totalRecuado"))
				totalRecuado = bundle.getDouble("totalRecuado");

			if (bundle.containsKey("nroDoc"))
				nroDoc = bundle.getString("nroDoc");
		}

		String cadena = "Para Ingresar las Formas de Pago, Por favor Seleccione una Forma de Pago y haga Click en el Boton Agregar.";
		String mensaje = Util.SepararPalabrasTextView(cadena, 50);

		TextView lblFormaPago = (TextView) findViewById(R.id.lblFormaPago);
		lblFormaPago.setText(Html.fromHtml(mensaje));

		String strTotalRecaudo = "" + totalRecuado;
		strTotalRecaudo = Util.QuitarE(strTotalRecaudo.replace("e+", "E"));

		TextView lblTotalRecaudo = (TextView) findViewById(R.id.lblTotalRecaudo);
		lblTotalRecaudo.setText(Util.SepararMilesSin(Util.Redondear(strTotalRecaudo, 0) /*(Main.total_recaudo - Main.total_descuento)*/));

		String strTotalFormaPago = "" + Main.total_forma_pago;
		strTotalFormaPago = Util.QuitarE(strTotalFormaPago.replace("e+", "E"));

		TextView lblTotalFormasPago = (TextView) findViewById(R.id.lblTotalFormasPago);
		lblTotalFormasPago.setText(Util.SepararMiles(strTotalFormaPago));

		String strDiferencia = "" + (Main.total_recaudo - (Main.total_descuento + Main.total_forma_pago));
		strDiferencia = Util.QuitarE(strDiferencia.replace("e+", "E"));

		lblDiferenciaFormasPago.setText(Util.SepararMilesSin(strDiferencia));
	}

	/*public void CargarListaFormaPago() {

		ItemListView[] listaItems;
		ItemListView itemListView;
		Vector<ItemListView> items = new Vector<ItemListView>();

		Main.total_forma_pago = 0;
		Enumeration<FormaPago> e = Main.listaFormaPago.elements(); 

		while(e.hasMoreElements()) {

			FormaPago formaPago = e.nextElement();
			Main.total_forma_pago += formaPago.valor;

			itemListView = new ItemListView();
			itemListView.titulo = formaPago.descripcion + "  " + Util.SepararMiles("" + formaPago.valor);
			items.addElement(itemListView);
		}

		if (items.size() > 0) {

			listaItems = new ItemListView[items.size()];
			items.copyInto(listaItems);

			ListAdapter adapter = new ListAdapter(this, listaItems, 0);
			ListView listaPedido = (ListView)findViewById(R.id.listaFormasDePago);
			listaPedido.setAdapter(adapter);

		} else {

			ListAdapter adapter = new ListAdapter(this, new ItemListView[]{}, 0);
			ListView listaPedido = (ListView)findViewById(R.id.listaFormasDePago);
			listaPedido.setAdapter(adapter);
		}

		TextView lblTotalFormasPago = (TextView)findViewById(R.id.lblTotalFormasPago);
		lblTotalFormasPago.setText(Util.SepararMiles("" + Main.total_forma_pago));

		TextView lblDiferenciaFormasPago = (TextView)findViewById(R.id.lblDiferenciaFormasPago);
		lblDiferenciaFormasPago.setText(Util.SepararMiles("" + (Main.total_recaudo - (Main.total_descuento + Main.total_forma_pago))));
	}*/



	public void CargarTablaFormasPago () {

		//listaFormaPago = DataBaseBO.CargarFormasPago(nroDoc);

		TableLayout table = new TableLayout(this);
		table.setBackgroundColor(Color.WHITE);

		HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollFormasDePago);
		scroll.removeAllViews();
		scroll.addView(table);

		if (Main.listaFormasPago.size() > 0) {

			String[] headers = {" ", " VALOR ", " FORMA DE PAGO "};
			Util.Headers(table, headers, this);

			TextView textViewAux;
			ImageView imageViewAux;

			int i = 0;

			for (FormaPago formaPago : Main.listaFormasPago) {

				TableRow fila = new TableRow(this);

				int ico_estado = R.drawable.op_borrar;
				imageViewAux = new ImageView(this);
				imageViewAux.setTag(i++ /*formaPago.position*/);
				imageViewAux.setImageResource(ico_estado);
				imageViewAux.setOnClickListener(this);
				//imageViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				imageViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
				imageViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
				fila.addView(imageViewAux);

				String strMonto = "" + formaPago.monto;
				strMonto = Util.QuitarE(strMonto.replace("e+", "E"));

				textViewAux = new TextView(this);
				textViewAux.setText(Util.SepararMiles(strMonto));
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
				textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
				textViewAux.setTextSize(19);
				//textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
				textViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
				fila.addView(textViewAux);

				textViewAux = new TextView(this);
				textViewAux.setText(formaPago.descripcion);
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
				textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
				textViewAux.setTextSize(19);
				//textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
				textViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
				fila.addView(textViewAux);

				table.addView(fila);
			}
		}

		long totalPagos = TotalFormasPago();
		String strTotalPagos = "" + totalPagos;
		strTotalPagos = Util.QuitarE(strTotalPagos.replace("e+", "E"));

		TextView lblTotalFormasPago = (TextView) findViewById(R.id.lblTotalFormasPago);
		lblTotalFormasPago.setText(Util.SepararMilesSin(Util.Redondear(strTotalPagos, 0)));

		String strDiferencia = "" + (totalRecuado - totalPagos);
		strDiferencia = Util.QuitarE(strDiferencia.replace("e+", "E"));

		TextView lblDiferenciaFormasPago = (TextView) findViewById(R.id.lblDiferenciaFormasPago);
		lblDiferenciaFormasPago.setText(Util.SepararMilesSin(Util.Redondear(strDiferencia, 0)));
	}



	@Override
	public void onClick (View view) {

		String posTag = ((ImageView) view).getTag().toString();
		final int position = Util.ToInt(posTag);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Esta seguro de Eliminar la Forma de Pago?").setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {

			public void onClick (DialogInterface dialog, int id) {

				Main.listaFormasPago.remove(position);
				CargarTablaFormasPago();

				//FormaPago formaPago = listaFormaPago.elementAt(position);
				/*boolean elimino = false; //DataBaseBO.EliminarFormaPago(formaPago.serial);

				if (elimino) {

					CargarTablaFormasPago();

				} else {

					Util.MostrarAlertDialog(FormCarteraFormasPago.this, "No se pudo eliminar la Forma de Pagp");
				}*/
			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {

			public void onClick (DialogInterface dialog, int id) {

				dialog.cancel();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}



	public void CargarFormaPago () {

		String[] items = new String[]{"Efectivo", "Cheque", "Cheque Postfechado", "Consignacion"};
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cbFormasDePago.setAdapter(adapter);
	}

	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == Const.RESP_FORM_OPCIONES_PAGO && resultCode == RESULT_OK) {

			CargarListaFormaPago();
		}
	}*/

	/*

	public void CargarBancos() {

		Vector<String> listaItems = new Vector<String>();
		listaBancos = DataBaseBO.ListaBancos(listaItems);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);

			Spinner cbBancosFormaPago = (Spinner) dialogOpcionesPago.findViewById(R.id.cbBancosFormaPago);
	    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        cbBancosFormaPago.setAdapter(adapter);
		}
	}*/



	public void OnClickAgregar (View view) {

		mostrarDialogFormasPago(null);

		if (onClickTerminarRecaudo != null)
			onClickTerminarRecaudo.reset();
	}



	public void OnClikCancelarFormaPago (View view) {

		finish();
	}



	public void terminarRecaudo () {

		long totalPagos = TotalFormasPago();

		if (totalPagos > (Main.total_recaudo - Main.total_descuento)) {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {

				public void onClick (DialogInterface dialog, int id) {

					//Guardar Recaudo
					dialog.cancel();
					guardarRecaudo();
					onClickTerminarRecaudo.reset();
				}
			}).setNegativeButton("No", new DialogInterface.OnClickListener() {

				public void onClick (DialogInterface dialog, int id) {

					dialog.cancel();
					onClickTerminarRecaudo.reset();
				}
			});

			AlertDialog alertDialog = builder.create();
			alertDialog.setMessage("La sumatoria de las formas de pago es superior al valor total de las facturas esta seguro de terminar el recaudo?");
			alertDialog.show();

		} else if (totalPagos == (Main.total_recaudo - Main.total_descuento)) {

			guardarRecaudo();

		} else {

			alertTerminarRecaudo("La sumatoria de las formas de pago es inferior al valor total de las facturas.");
		}
	}



	public void alertTerminarRecaudo (String mensaje) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick (DialogInterface dialog, int id) {

				dialog.cancel();
				onClickTerminarRecaudo.reset();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.setMessage(mensaje);
		alertDialog.show();
	}



	public long TotalFormasPago () {

		long total = 0;
		for (FormaPago formaPago : Main.listaFormasPago) {

			total += formaPago.monto;
		}

		return total;
	}



	public boolean mostrarDialogFormasPago (FormaPago formaPago) {

		if (dialogFormasPago == null) {

			dialogFormasPago = new Dialog(this);
			dialogFormasPago.requestWindowFeature(Window.FEATURE_LEFT_ICON);
			dialogFormasPago.setContentView(R.layout.dialog_formas_pago);
			dialogFormasPago.setTitle("Registrar Forma de Pago");

			initDialog();
			cargarBancos();
			cargarPlazas();
		}

		String msgFormaPago = "";
		String strTotal = "" + totalRecuado;
		strTotal = Util.QuitarE(strTotal.replace("e+", "E"));

		final String strTotalRecuado = Util.SepararMilesSin(Util.Redondear(strTotal, 0));
		String valorARecuadar = "<b>Valor A Recaudar $" + strTotalRecuado + "</b>";

		String tipoPago = cbFormasDePago.getSelectedItem().toString();

		if (tipoPago.equals("Efectivo")) { //Forma Pago Efectivo

			msgFormaPago = "Pago en Efectivo";
			(((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.GONE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.GONE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.GONE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.GONE);

		} else if (tipoPago.equals("Cheque")) { //Forma Pago Cheque

			msgFormaPago = "Pago Cheque";
			(((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.VISIBLE);

			if (formaPago != null) {

				((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText(formaPago.nroCheque);

			} else {

				((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText("");
			}

		} else if (tipoPago.equals("Cheque Postfechado")) { //Forma Pago Cheque Postfechado

			msgFormaPago = "Pago Cheque Postfechado";
			(((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.VISIBLE);

			if (formaPago != null) {

				((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText(formaPago.nroCheque);

			} else {

				((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText("");
			}

		} else if (tipoPago.equals("Consignacion")) { //Forma Pago Consignacion

			msgFormaPago = "Pago Consignacion";
			(((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.VISIBLE);
			(((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.GONE);

			if (formaPago != null) {

				((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText(formaPago.nroCheque);

			} else {

				((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText("");
			}
		}

		((TextView) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).setText("");
		((TextView) dialogFormasPago.findViewById(R.id.lblFormaPago)).setText(msgFormaPago);
		((TextView) dialogFormasPago.findViewById(R.id.lblValorRecaudo)).setText(Html.fromHtml(valorARecuadar));

		String diferencia = lblDiferenciaFormasPago.getText().toString().replace(",", "");
		((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).setText(diferencia);

		((Button) dialogFormasPago.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

			public void onClick (View v) {

				String cant = ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).getText().toString();
				int monto = Util.ToInt(cant);

				if (monto == 0) {

					AlertDialog.Builder builder = new AlertDialog.Builder(dialogFormasPago.getContext());
					builder.setMessage("Debe ingresar la cantidad").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

						public void onClick (DialogInterface dialog, int id) {

							((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).requestFocus();
							dialog.cancel();
						}
					});

					AlertDialog alert = builder.create();
					alert.show();

				} else {

					long totalPagos = TotalFormasPago();

					if (monto + totalPagos <= totalRecuado) {

						AgregarFormaPago();

					} else {

						/*
						AlertDialog.Builder builder = new AlertDialog.Builder(dialogFormasPago.getContext());
						builder.setMessage("La sumatoria de las formas de Pago supera el Valor a Recaudar: $" + strTotalRecuado)
						.setCancelable(false)
						.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {

								((EditText)dialogFormasPago.findViewById(R.id.txtValorFormaPago )).requestFocus();
								dialog.cancel();
							}
						});

						AlertDialog alert = builder.create();
						alert.show(); */

						AgregarFormaPago();

					}
				}
			}
		});

		((Button) dialogFormasPago.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {

			public void onClick (View v) {

				dialogFormasPago.cancel();
			}
		});

		((ImageButton) dialogFormasPago.findViewById(R.id.btnCalendar)).setOnClickListener(new OnClickListener() {

			public void onClick (View v) {

				DatePickerDialog.OnDateSetListener datePickerDialogListener = new DatePickerDialog.OnDateSetListener() {

					public void onDateSet (DatePicker view, int year, int monthOfYear, int dayOfMonth) {

						MostrarFecha(year, monthOfYear + 1, dayOfMonth);
					}
				};

				Calendar calendar = Calendar.getInstance();
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH);
				int day = calendar.get(Calendar.DAY_OF_MONTH);

				DatePickerDialog datePickerDialog = new DatePickerDialog(FormCarteraFormasPago2.this, datePickerDialogListener, year, month, day);
				datePickerDialog.show();
			}
		});

		dialogFormasPago.setCancelable(false);
		dialogFormasPago.show();
		dialogFormasPago.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_agregar);

		return true;
	}



	public void MostrarFecha (int anio, int mes, int dia) {

		StringBuilder sb = new StringBuilder();

		sb.append(anio);
		sb.append("-");
		sb.append(mes < 10 ? "0" + mes : mes);
		sb.append("-");
		sb.append(dia < 10 ? "0" + dia : dia);

		((TextView) dialogFormasPago.findViewById(R.id.txtFecha)).setText(sb);
	}



	public void initDialog () {

		Util.aplicarFuente(this, dialogFormasPago.findViewById(android.R.id.content), Const.BUTTON);
		final EditText txtFechaEntrega = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);

		txtFechaEntrega.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		txtFechaEntrega.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
		txtFechaEntrega.setHint("Fecha Documento");

		txtFechaEntrega.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged (Editable e) {

			}



			@Override
			public void beforeTextChanged (CharSequence s, int start, int before, int count) {

			}



			@Override
			public void onTextChanged (CharSequence s, int start, int before, int count) {

				if (s.length() == 0) {

					txtFechaEntrega.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

				} else {

					txtFechaEntrega.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
				}
			}
		});
	}



	public void cargarBancos () {

		Vector<String> listaItems = new Vector<String>();
		listaBancos = DataBaseBO.ListaBancos(listaItems);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);

			Spinner cbBancosFormaPago = (Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			cbBancosFormaPago.setAdapter(adapter);
		}
	}



	public void cargarPlazas () {

		String[] items = new String[]{"Local", "Otra"};
		Spinner cbPlazaFormasPago = (Spinner) dialogFormasPago.findViewById(R.id.cbPlazaFormasPago);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cbPlazaFormasPago.setAdapter(adapter);
	}



	public void guardarRecaudo () {

		String nroRecibo = null;
		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {

			if (bundle.containsKey("nroRecibo"))
				nroRecibo = bundle.getString("nroRecibo");
		}

		if (nroRecibo != null) {

			String imei = obtenerImei();
			//			int consecutivo = obtenerConsecutivo();



			if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {

				DataBaseBO.CargarInfomacionUsuario();

			}

			String consecutivo = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor);

			boolean guardo = DataBaseBO.guardarRecaudo(imei, nroRecibo, consecutivo);

			if (guardo) {

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

					public void onClick (DialogInterface dialog, int id) {

						dialog.cancel();

						Main.total_recaudo = 0;
						Main.total_descuento = 0;
						Main.total_forma_pago = 0;

						Main.cartera.removeAllElements();
						Main.listaDescuentos.clear();
						//Main.listaFormaPago.clear();
						Main.listaFormasPago.clear();
						onClickTerminarRecaudo.reset();

						setResult(RESULT_OK);
						finish();
					}
				});

				AlertDialog alertDialog = builder.create();
				alertDialog.setMessage("Recaudo Guardado con exito para el cliente " + Main.cliente.razonSocial);
				alertDialog.show();

			} else {

				alertTerminarRecaudo("No se pudo guardar Recaudo: " + DataBaseBO.mensaje);
			}

		} else {

			alertTerminarRecaudo("No se pudo guardar el Recaudo. No se pudo leer el Numero de Recibo!");
		}
	}



	public String obtenerImei () {

		TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			return "NoPermissionToRead";
		}
		String deviceId = manager.getDeviceId();

		if (deviceId == null) {

			WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

			if (wifiManager != null) {

				WifiInfo wifiInfo = wifiManager.getConnectionInfo();

				if (wifiInfo != null) {

					String mac = wifiInfo.getMacAddress();

					if (mac != null) {

						deviceId = mac.replace(":", "").toUpperCase(Locale.getDefault());
					}
				}
			}
		}

		return deviceId;
	}

	public int obtenerConsecutivo() {

		SharedPreferences settings = getSharedPreferences(Const.SETTINGS_RECAUDO, 0);
		SharedPreferences.Editor editor = settings.edit();
		int consecutivo = settings.getInt(Const.CONSECUTIVO_RECAUDO, 0);
		editor.putInt(Const.CONSECUTIVO_RECAUDO, (consecutivo + 1) % 9999);
		editor.commit();

		return consecutivo;
	}

	public void AgregarFormaPago() {

		EditText txtValorFormaPago = (EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago);
		float valor = Util.ToFloat(txtValorFormaPago.getText().toString());

		if (valor > 0) {

			int codigoPago = 0;
			String codigoBanco = "0";
			String nroDocumento = ""; //Este valor aplica para Cheque, Cheque Posfechado y Consigancion

			int position = cbFormasDePago.getSelectedItemPosition();
			String descFormaPago = cbFormasDePago.getSelectedItem().toString();

			int indexBanco;
			EditText txtFecha = null;
			TextView txtNroDocumento = null;
			Spinner cbPlazaFormasPago = null;

			switch (position) {

			case 0: //Efectivo
				codigoPago = 1;
				codigoBanco = "-1";
				break;

			case 1: //Cheque
				codigoPago = 2;
				txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
				cbPlazaFormasPago = ((Spinner)findViewById(R.id.cbPlazaFormasPago));
				indexBanco = ((Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();					
				codigoBanco = listaBancos.elementAt(indexBanco).codigo;
				break;

			case 2: //Cheque Posfechado
				codigoPago = 4;
				txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
				cbPlazaFormasPago = ((Spinner)findViewById(R.id.cbPlazaFormasPago));
				indexBanco = ((Spinner)dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
				codigoBanco = listaBancos.elementAt(indexBanco).codigo;
				break;

			case 3: //Consignacion
				codigoPago = 3; 
				txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
				indexBanco = ((Spinner)dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
				codigoBanco = listaBancos.elementAt(indexBanco).codigo;
				break;
			}

			if (position == 1 || position == 2) { //Si es Cheque o Cheque Posfechado

				txtNroDocumento = (TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento);
				nroDocumento = txtNroDocumento.getText().toString().trim();

				if (nroDocumento.equals("")) {

					Util.MostrarAlertDialog(this, "Por favor ingrese el numero de cheque");
					txtNroDocumento.requestFocus();
					return;
				} 

				if (txtFecha != null) {

					String fechaPago = txtFecha.getText().toString();

					if (fechaPago.equals("")) {

						Util.MostrarAlertDialog(this, "Por Favor Ingrese la Fecha.");
						return;
					}

					/**
					 * Si dias = 0. Indica que CURRENT_DATE = fechaPago
					 * Si dias < 0. Indica que CURRENT_DATE < fechaPago (Fecha Acutal menor que la Fecha de Pago)
					 * Si dias > 0. Indica que CURRENT_DATE > fechaPago (Fecha Actual mayor que la Fecha de Pago)
					 **/
					int dias = DataBaseBO.DiferenciaDias(fechaPago);

					if (dias < 0 && position == 1) {

						Util.MostrarAlertDialog(this, "La fecha que ha ingresado no corresponde a la forma de Pago 'Cheque'. Debe ingresarlo como Cheque Postfechado.");
						return;

					} else if (dias >= 0 && position == 2) {

						Util.MostrarAlertDialog(this, "La fecha que ha ingresado no corresponde a la forma de Pago 'Cheque Posfechado'. Debe ingresarlo como Cheque.");
						return;
					}

				} else {

					Util.MostrarAlertDialog(this, "No se pudo leer la Fecha.");
					return;
				}

			} else if (position == 3) { //Consignacion

				txtNroDocumento = (TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento);
				nroDocumento = txtNroDocumento.getText().toString().trim();

				if (nroDocumento.equals("")) {

					Util.MostrarAlertDialog(this, "Por favor ingrese el numero de la Consigancion");
					txtNroDocumento.requestFocus();
					return;
				}

				if (txtFecha != null) {

					String fechaPago = txtFecha.getText().toString();

					if (fechaPago.equals("")) {

						Util.MostrarAlertDialog(this, "Por Favor Ingrese la Fecha.");
						return;
					}

					/**
					 * Si dias = 0. Indica que CURRENT_DATE = fechaPago
					 * Si dias < 0. Indica que CURRENT_DATE < fechaPago (Fecha Acutal menor que la Fecha de Pago)
					 * Si dias > 0. Indica que CURRENT_DATE > fechaPago (Fecha Actual mayor que la Fecha de Pago)
					 **/
					int dias = DataBaseBO.DiferenciaDias(fechaPago);
					if (dias < 0) {

						Util.MostrarAlertDialog(this, "La fecha que ha ingresado es Invalida.");
						return;
					}

				}  else {

					Util.MostrarAlertDialog(this, "No se pudo leer la Fecha.");
					return;
				}
			}

			FormaPago formaPago = new FormaPago();
			formaPago.monto           = valor;
			formaPago.codigo          = codigoPago;
			formaPago.nro_cheque_cons = txtNroDocumento != null ? txtNroDocumento.getText().toString() : "";
			formaPago.fechaPago       = txtFecha != null ? txtFecha.getText().toString() : "";
			formaPago.codigoBanco     = codigoBanco;
			formaPago.descripcion     = descFormaPago;

			if (cbPlazaFormasPago != null) {

				String plaza = cbPlazaFormasPago.getSelectedItem().toString();

				if (plaza.equals("Local"))
					formaPago.plaza = "L";
				else if (plaza.equals("Otra"))
					formaPago.plaza = "O";
			}

			//DatePicker datePickerFormaPago = (DatePicker) findViewById(R.id.datePickerFormaPago);
			//String fechaPago = datePickerFormaPago.getYear() + "-" + (datePickerFormaPago.getMonth() + 1) + "-" + datePickerFormaPago.getDayOfMonth();


			//MostrarAlertDialog("Fecha Pago = " + fechaPago + " dif dias: " + dias);
			/*
			if (position == 1 || position == 2) { //Si es cheque o cheque posfechado

				TextView txtNroDocumento = (TextView) findViewById(R.id.txtNroDocumento);
				nroDocumento = txtNroDocumento.getText().toString().trim();

				if (nroDocumento.equals("")) {

					MostrarAlertDialog("Por favor ingrese el numero de cheque");
					((TextView) findViewById(R.id.txtNroDocumento)).requestFocus();
					return;

				} else {

					if (dias > 0 && position == 1) {

						MostrarAlertDialog("La fecha que ha ingresado no corresponde a la forma de Pago 'Cheque'. Debe ingresarlo como Cheque Postfechado.");
						return;

					} else if (dias <= 0 && position == 2) {

						MostrarAlertDialog("La fecha que ha ingresado no corresponde a la forma de Pago 'Cheque Posfechado'. Debe ingresarlo como Cheque.");
						return;
					}
				}

			} else if (position == 3) { //Consignacion

				TextView txtNroDocumento = (TextView) findViewById(R.id.txtNroDocumento);
				nroDocumento = txtNroDocumento.getText().toString().trim();

				if (nroDocumento.equals("")) {

					MostrarAlertDialog("Por favor ingrese el numero de la consigancion");
					((TextView) findViewById(R.id.txtNroDocumento)).requestFocus();
					return;
				}
			}

			if (dias > 0 && position == 3) {

				MostrarAlertDialog("La fecha que ha ingresado es Invalida.");
				return;
			}*/

			/*float totalPagos = 0;
			boolean existePago = false;
			Enumeration<FormaPago> e = Main.listaFormaPago.elements();

			while (e.hasMoreElements()) {

				FormaPago formaPagoAux = e.nextElement();

				if (formaPagoAux.codigo == codigoPago) { //La forma de Pago ya fue agregada

					totalPagos += valor;
					existePago = true;

				} else {

					totalPagos += formaPagoAux.valor; 
				}
			}

			if (!existePago)
				totalPagos += valor;

			if (totalPagos > Main.total_recaudo - Main.total_descuento) {


			} else {


			}*/

			Main.listaFormasPago.addElement(formaPago);
			CargarTablaFormasPago();
			dialogFormasPago.cancel();

			//setResult(RESULT_OK);
			//finish();

		} else {

			Util.MostrarAlertDialog(this, "Por favor ingrese el valor del Recaudo");
			txtValorFormaPago.requestFocus();
			return;
		}
	}

	private OnClickListenerImpl onClickTerminarRecaudo = new OnClickListenerImpl() {

		@Override
		public void onOneClick(View view) {	    	
			terminarRecaudo();
		}
	};
}
