package celuweb.com.uyusa;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ChequeoPrecios;

/**
 * Created by Andres Rangel on 18/01/2018.
 */

public class DialogChequeoPreciosCompetencia extends Dialog {

    ChequeoPrecios propios;
    Button btnAceptar;
    Vector<ChequeoPrecios> listaChequeo;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ChequeoCompetenciaAdapter adapter;
    public DialogChequeoPreciosCompetencia(@NonNull Context context,ChequeoPrecios propios) {
        super(context);
        this.propios=propios;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_chequeo_competencia);
        btnAceptar= (Button) findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        listaChequeo=new Vector<>();
        recyclerView= (RecyclerView) findViewById(R.id.recicler);
        adapter=new ChequeoCompetenciaAdapter(getContext(),listaChequeo);
        layoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        CargarChequeosCompetencia();
    }

    private void CargarChequeosCompetencia() {
        listaChequeo = DataBaseBO.ListaChequeosCompetencia(propios);

        for (int i=0;i<listaChequeo.size();i++){
            recyclerView.setAdapter(adapter);

        }
        adapter.updateRecords(listaChequeo);
        recyclerView.setItemViewCacheSize(listaChequeo.size());

    }
}
