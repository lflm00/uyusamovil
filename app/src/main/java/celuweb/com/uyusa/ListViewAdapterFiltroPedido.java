package celuweb.com.uyusa;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import celuweb.com.DataObject.ItemListView;

public class ListViewAdapterFiltroPedido extends ArrayAdapter<ItemListView> {
	
	int icono;
	int colorTitulo;
	int layout_list_item;

	public int seleccionado = -1;
	
	Activity context;
	ItemListView[] listItems;
	
	ListViewAdapterFiltroPedido(Activity context, ItemListView[] listItems, int icono, int colorTitulo) {
		
		super(context, R.layout.list_item, listItems);
		
		this.layout_list_item = R.layout.list_item;
		this.listItems = listItems; 
		this.context = context;
		
		this.icono = icono;
		this.colorTitulo = colorTitulo; 
	}
	
//	public View getView(int position, View convertView, ViewGroup parent) {
//		
//		LayoutInflater inflater = context.getLayoutInflater();
//		View item = inflater.inflate(layout_list_item, null);
//		
//		TextView lblTitulo = (TextView)item.findViewById(R.id.lblTitulo);
//		lblTitulo.setText(listItems[position].titulo);
//		
//		TextView lblSubtitulo = (TextView)item.findViewById(R.id.lblSubTitulo);
//		lblSubtitulo.setText(listItems[position].subTitulo);
//		
//		if (listItems[position].icono > 0)
//			((ImageView)item.findViewById(R.id.iconListView)).setImageResource(listItems[position].icono);
//		else if (icono > 0)
//			((ImageView)item.findViewById(R.id.iconListView)).setImageResource(icono);
//		
//		if (item != null) {
//			if (position == seleccionado) {
////				item.setBackgroundColor(Color.RED);
//				lblTitulo.setTextColor(Color.RED);
//			} else {
//				lblTitulo.setTextColor(Color.parseColor("#2E65AD"));
////				item.setBackgroundColor(Color.TRANSPARENT);
//			}
//		}
//		
//		return item;
//    }

	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder viewHolder;
	
		if(convertView==null){

		 LayoutInflater inflater = context.getLayoutInflater();
		 convertView = inflater.inflate(R.layout.list_item, null);
		
		 viewHolder = new ViewHolder();
		 viewHolder.lblTitulo = (TextView)convertView.findViewById(R.id.lblTitulo);
		 viewHolder.lblSubtitulo = (TextView)convertView.findViewById(R.id.lblSubTitulo);
		 viewHolder.iconListView = (ImageView)convertView.findViewById(R.id.iconListView);
		 convertView.setTag(viewHolder);	
		
		}else{
	        viewHolder = (ViewHolder) convertView.getTag();

	     }
		
		
		
			
		if (listItems[position].icono > 0)
			viewHolder.iconListView.setImageResource(listItems[position].icono);
//			((ImageView)convertView.findViewById(R.id.iconListView)).setImageResource(listItems[position].icono);
		else {
			((ImageView)convertView.findViewById(R.id.iconListView)).setImageResource(icono);
		
//			viewHolder.iconListView = (ImageView) convertView.findViewById(R.id.iconListView);
		}
		
		  if (viewHolder != null) {
		  
				if (position == seleccionado) {
					viewHolder.lblTitulo.setTextColor(Color.RED);
				} else {
					viewHolder.lblTitulo.setTextColor(Color.parseColor("#2E65AD"));
				}
			}
	 
		viewHolder.lblTitulo.setText(listItems[position].titulo);
		viewHolder.lblSubtitulo.setTextColor(Color.RED);
		viewHolder.lblSubtitulo.setText(listItems[position].subTitulo);	
		
		viewHolder.iconListView.setImageResource(icono);
			
		return(convertView);
    }
	
	
	@Override
	public ItemListView getItem(int position) {
		
		return listItems[position];
	}
	
	static class ViewHolder {

		TextView lblTitulo;
		TextView lblSubtitulo;
		ImageView iconListView;

	}
	
	
	
}
