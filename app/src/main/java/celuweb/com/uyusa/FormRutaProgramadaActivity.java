package celuweb.com.uyusa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import celuweb.com.BusinessObject.ConfigBO;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Config;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;

public class FormRutaProgramadaActivity extends CustomActivity {

    Dialog dialogSalir;
    AlertDialog alert;
    private Dialog dialogAgotados;
    private ArrayList<ItemListView> items;
    ProgressDialog progressDialog;
    LinearLayout lyInfo, lyMas;
    TableLayout tbletInfo, tbleMas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_ruta_programada);

        Inicializar();
        validarClickInfoMas();

    }


    public void Inicializar() {

        int cantClientes, visitas, sinVisitaRuta, pedidos, novedades, visitasRuta;
        double valorVenta, cumplimiento;
        float efectRutero, efectVisitas;
        String strCumplimiento;

        cantClientes = cargarTotalClientesRuta();
        visitas = (int) DataBaseBO.obtenerTotalDeVisitas();
        pedidos = DataBaseBO.getCantPedidos();
        novedades = DataBaseBO.getCantNovedades();

        valorVenta = DataBaseBO.getTotalVentas();

        visitasRuta = DataBaseBO.getCantVisitasRuta();

        sinVisitaRuta = cantClientes - visitas;

        if (cantClientes > 0) {

            efectRutero = (visitas * 100) / cantClientes;

            if (visitas > 0)
                efectVisitas = (pedidos * 100) / visitas;
            else
                efectVisitas = 0;
        } else {

            efectRutero = 0;
            efectVisitas = 0;
        }

        cumplimiento = 0;

        float presupuestoRuta = Float.parseFloat(Util.Redondear(Util.QuitarE("" + Main.presupuesto), 2));

        if (presupuestoRuta > 0) {

            cumplimiento = (valorVenta / presupuestoRuta) * 100f;
            strCumplimiento = Util.Redondear(String.valueOf(cumplimiento), 2);
        } else {

            strCumplimiento = "0";
        }


        TextView lblClientesRuta = (TextView) findViewById(R.id.lblClientesRuta);
        lblClientesRuta.setText(String.valueOf(cantClientes));

        TextView lblEfectRutero = (TextView) findViewById(R.id.lblEfecRutero);
        lblEfectRutero.setText(Util.Redondear(String.valueOf(efectRutero), 2) + " %");

        TextView lblVisitas = (TextView) findViewById(R.id.lblVisitas);
        lblVisitas.setText(String.valueOf(visitas));

        TextView lblEfecVisitas = (TextView) findViewById(R.id.lblEfectVisitas);
        lblEfecVisitas.setText(Util.Redondear(String.valueOf(efectVisitas), 2) + " %");

        TextView lblSinVisita = (TextView) findViewById(R.id.lblSinVisita);
        lblSinVisita.setText(String.valueOf(sinVisitaRuta));

        TextView lblNovedades = (TextView) findViewById(R.id.lblNovedades);
        lblNovedades.setText(String.valueOf(novedades));

        TextView lblPedidos = (TextView) findViewById(R.id.lblPedidos);
        lblPedidos.setText(String.valueOf(pedidos));

        TextView lblTotalVentas = (TextView) findViewById(R.id.lblTotalVentas);
        lblTotalVentas.setText(Util.SepararMiles(Util.Redondear(String.valueOf(valorVenta), 2)));

        TextView lblPresupuesto = (TextView) findViewById(R.id.lblPresupuesto);
        lblPresupuesto.setText(Util.SepararMiles(Util.Redondear(Util.QuitarE("" + Main.presupuesto), 0)));

        TextView lblCumplimiento = (TextView) findViewById(R.id.lblCumplimineto);
        lblCumplimiento.setText(Util.Redondear("" + Main.porcentaje, 2) + "%");

        Cartera datosMes = DataBaseBO.consultarDatosMes();

        TextView lblFacturadoMes = (TextView) findViewById(R.id.lblFacturadoMes);
        lblFacturadoMes.setText(datosMes.facturadoMes + "    Periodo: " + datosMes.periodoFacturado);

        TextView lblRecaudadoMes = (TextView) findViewById(R.id.lblRecaudadoMes);
        lblRecaudadoMes.setText(datosMes.recaudadoMes + "    Periodo: " + datosMes.periodoRecaudado);
		
		/*ConfigBO.CrearConfigDB();
		//DataBaseBO.ActualizarSnEnvio();
		DataBaseBO.BorrarPedidosSinTerminar();
		
		try {
        	
        	Main.versionApp = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			Log.i("FormPrincipalActivity", "Version = " + Main.versionApp);
			
		} catch (NameNotFoundException e) {
			
			Main.versionApp = "0.0.0";
			Log.e("FormPrincipalActivity", e.getMessage(), e);
		}
		
		setTitle("ZZVende " + Main.versionApp + Const.TITULO);
		
		TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        Main.deviceId = manager.getDeviceId();
        Log.i("FormPrincipalActivity", "deviceId = " + Main.deviceId);*/
    }


    private int cargarTotalClientesRuta() {

        int diaActual = Util.ObtenerDiaSemanaNumeroEntero();
        String diaRuta2 = String.valueOf(cargaRuta2(diaActual));
        int total = DataBaseBO.getCantClientesRutero(diaRuta2);

        return total;
    }

    public void OnClickClienteNuevo(View view) {


//		if (DataBaseBO.ExisteInformacion("Clientes")) {
//			
//			Intent intent = new Intent(this, FormClienteNuevo.class);
//			startActivity(intent);
//			
//		} else {
//			
//			MostrarAlert("Para Registrar un Cliente Nuevo, Por Favor Inicie Dia.");
//		}
    }

    public void OnClickRuteroRP(View view) {

//		if(Util.estaGPSEncendido(this)){

        if (DataBaseBO.ExisteDataBase()) {

            if (DataBaseBO.TerminoLabores()) {

                Util.MostrarAlertDialog(this, "No se pueden Registrar mas Pedidos debido a que ya termino Labores");

            } else if (DataBaseBO.ExisteInformacion("Rutero") || DataBaseBO.ExisteInformacionClientes()) {

                Config config = ConfigBO.ObtenerConfigUsuario();

                if (config == null) {

                    MostrarAlert("Por favor Ingrese la Configuracion de Usuario e Inicie Dia");

                } else {


                    if (Main.usuario.viajero.equals("SI")) {

                        if (!DataBaseBO.existeRegistroLiquidacion()) {


                            Intent formRutero = new Intent(this, FormRuteroActivity.class);
                            startActivity(formRutero);
                        } else {
                            MostrarAlert("Ya ha sido realizada la liquidacion.");
                        }


                    } else {

                        if (Util.estaActualLaFecha()) {

                            if (!DataBaseBO.existeRegistroLiquidacion()) {

                                Intent formRutero = new Intent(this, FormRuteroActivity.class);
                                startActivity(formRutero);

                            } else {
                                MostrarAlert("Ya ha sido realizada la liquidacion.");
                            }

                        } else {

                            Util.MostrarAlertDialog(this,
                                    "La fecha de la labor no coincide con la fecha de celular, por favor iniciar dia",
                                    1);
                        }
                    }

                }

            } else {

                MostrarAlert("No hay informacion de rutero.\n\nVerifique con el administrador si tiene rutero asignado");
            }

        } else {

            MostrarAlert("No hay informacion de Rutero.\nPor favor inicie dia");
        }


//		}else{
//			mostrarMensajeActivarGPS();
//		}
    }

    private void mostrarMensajeActivarGPS() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("El GPS esta desactivado. Por favor enciendalo antes de continuar.")
                .setCancelable(false)

                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(myIntent, Const.ENABLE_GPS);
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();

    }


    public void OnClickBusquedaRP(View view) {

        if (DataBaseBO.TerminoLabores()) {

            Util.MostrarAlertDialog(this, "No se pueden Registrar mas Pedidos debido a que ya termino Labores");

        } else if (DataBaseBO.ExisteInformacion("Clientes")) {

            if (Main.usuario.viajero.equals("SI")) {

                if (!DataBaseBO.existeRegistroLiquidacion()) {

                    Intent formBuscarCliente = new Intent(this, FormBuscarClienteActivity.class);
                    startActivity(formBuscarCliente);

                } else {
                    MostrarAlert("Ya ha sido realizada la liquidacion.");
                }

            } else {

                if (Util.estaActualLaFecha()) {

                    if (!DataBaseBO.existeRegistroLiquidacion()) {

                        Intent formBuscarCliente = new Intent(this, FormBuscarClienteActivity.class);
                        startActivity(formBuscarCliente);

                    } else
                        MostrarAlert("Ya ha sido realizada la liquidacion.");

                } else {

                    Util.MostrarAlertDialog(this,
                            "La fecha de la labor no coincide con la fecha de celular, por favor iniciar dia", 1);

                }

            }


        } else {

            MostrarAlert("No hay informacion de Clientes. Por Favor Inicie Dia.");
        }

    }

    public void OnClickRegresar(View view) {

        CerrarAplicacion();
    }


    public void CerrarAplicacion() {

        setResult(RESULT_OK);
        finish();
    }

    public void MostrarAlert(String mensaje) {

        if (alert == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                }
            });

            alert = builder.create();
        }

        alert.setMessage(mensaje);
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Const.RESP_FORM_INICIAR_DIA && resultCode == RESULT_OK) {

            //CargarInfoUsuario();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            CerrarAplicacion();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    public void OnClickClientesNuevos(View v) {

        if (DataBaseBO.ExisteInformacion("Clientes")) {
            Intent intent = new Intent(this, FormClienteNuevo1.class);
            startActivity(intent);
        } else {
            MostrarAlert("No hay informacion disponible. Por Favor Inicie Dia.");
        }


//		if (DataBaseBO.ExisteDataBase()) {
//		
//			
//
//           if( Util.estaActualLaFecha() ){						
//				
//     		  Intent intent = new Intent(this, FormClienteNuevo1.class);
//    		  startActivity(intent);
//        	   
//						
//            }
//           else{
//        	   
//            	Util.MostrarAlertDialog(this,"La fecha de la labor no coincide con la fecha de celular, por favor iniciar dia",1);
//           }
//		}
//		else {			
//			
//			MostrarAlert("Por Favor Inicie Dia.");	
//		}

    }


    @Override
    protected void onResume() {

        super.onResume();

        // Main.usuario = null;
        // Main.cliente = null;

        if (Main.usuario == null || Main.usuario.codigoVendedor == null
                || Main.usuario.bodega == null) {

            DataBaseBO.CargarInfomacionUsuario();

        }

        Inicializar();
        cargarInfoPedido();

    }


    /**
     * mostrar el dialog de clientes con agotados y devoluciones
     *
     * @param view
     */
    public void onClickAgotadosYDevoluciones(View view) {
        mostrarDialogAgotadosYdevolucionesClientes();
    }

    public void onClickConsultarCartera(View view) {

        this.runOnUiThread(new Runnable() {

            public void run() {

                Main.pdialog = ProgressDialog.show(FormRutaProgramadaActivity.this,
                        "", "Cargando Informacion....", true);

                Main.pdialog.show();

            }
        });


        Intent intent55 = new Intent(this, FormSociosCarteraActivity.class);
        startActivity(intent55);
    }


    public void mostrarDialogAgotadosYdevolucionesClientes() {

        dialogAgotados = new Dialog(this, R.style.AppTheme);
        dialogAgotados.setContentView(R.layout.dialog_agotydev_clientes);
        dialogAgotados.setCancelable(true);
        ListView listaViewClientes = (ListView) dialogAgotados.findViewById(R.id.listaClientesAgotDev);
        cargarListaClientesConAgotadosYDevoluciones(listaViewClientes);
        dialogAgotados.show();
    }


    /**
     * cargar la lista de pedidos no sincronizados.
     */
    private void cargarListaClientesConAgotadosYDevoluciones(ListView listaViewClientes) {
        //lista de productos (referencias) cargados.
        ArrayList<Cliente> listaClientes = DataBaseBO.clientesConAgotadosYDevoluciones();
        items = new ArrayList<ItemListView>();

        for (Cliente cliente : listaClientes) {
            ItemListView item = new ItemListView();
            item.titulo = cliente.codigo;
            item.subTitulo = cliente.razonSocial;
            items.add(item);
        }
        //cargar el adapter en el listView
        ListViewClientesAgotyDevAdapter adapter = new ListViewClientesAgotyDevAdapter(FormRutaProgramadaActivity.this, items);
        listaViewClientes.setAdapter(adapter);
        listaViewClientes.setOnItemClickListener(listenerListView);
    }

    private OnItemClickListener listenerListView = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {

            if (DataBaseBO.ExisteDataBase()) {
                if (DataBaseBO.TerminoLabores()) {
                    Util.MostrarAlertDialog(FormRutaProgramadaActivity.this, "No se pueden Registrar mas Pedidos debido a que ya termino Labores");

                } else if (DataBaseBO.ExisteInformacion("Rutero") || DataBaseBO.ExisteInformacionClientes()) {
                    Config config = ConfigBO.ObtenerConfigUsuario();

                    if (config == null) {
                        MostrarAlert("Por favor Ingrese la Configuracion de Usuario e Inicie Dia");
                    } else {
                        if (Util.estaActualLaFecha()) {
                            CargarInformacionCliente(position);
                            dialogAgotados.dismiss();
                        } else {
                            Util.MostrarAlertDialog(FormRutaProgramadaActivity.this, "La fecha de la labor no coincide con la fecha de celular, por favor iniciar dia", 1);
                        }
                    }
                } else {
                    MostrarAlert("No hay Informacion de Rutero.\n\nVerifique con el Adminstrador si tiene Rutero Asignado");
                }
            } else {
                MostrarAlert("No hay Informacion de Rutero.\nPor Favor Inicie Dia");
            }
        }
    };


    /**
     * cargar informacion del cliente seleccionado para mostrar su informacion
     *
     * @param position
     */
    public void CargarInformacionCliente(final int position) {

        ItemListView item = items.get(position);
        final Cliente clienteSel = DataBaseBO.BuscarCliente(item.titulo);
        boolean tienePedido = DataBaseBO.ExistePedidoCliente(clienteSel.codigo);

        if (tienePedido) {

            AlertDialog alertDialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    Usuario usuario = DataBaseBO.CargarUsuario();

                    if (usuario == null) {
                        Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del Usuario", Toast.LENGTH_LONG).show();
                    } else {
                        Main.cliente = clienteSel;
                        DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
                        Intent formInfoCliente = new Intent(FormRutaProgramadaActivity.this, FormInfoClienteActivity.class);
                        startActivity(formInfoCliente);
                    }
                    dialog.cancel();
                }
            }).
                    setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            alertDialog = builder.create();
            alertDialog.setMessage("El cliente " + clienteSel.razonSocial + " ya tiene un pedido para el dia de hoy.\n\nDesea tomar un nuevo pedido?");
            alertDialog.show();

        } else {
            Usuario usuario = DataBaseBO.CargarUsuario();
            if (usuario == null) {
                Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del Usuario", Toast.LENGTH_LONG).show();
            } else {
                //Guarda la Referencia al Cliente seleccionado.
                Main.cliente = clienteSel;
                DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);

                Intent formInfoCliente = new Intent(FormRutaProgramadaActivity.this, FormInfoClienteActivity.class);
                startActivity(formInfoCliente);
            }
        }
    }

    private String cargaRuta2(int position) {

        String strDia = "";

        switch (position) {

            case 0:
                strDia = "DOMINGO";
                break;
            case 1:
                strDia = "LUNES";
                break;
            case 2:
                strDia = "MARTES";
                break;
            case 3:
                strDia = "MIERCOLES";
                break;
            case 4:
                strDia = "JUEVES";
                break;
            case 5:
                strDia = "VIERNES";
                break;
            case 6:
                strDia = "SABADO";
                break;
            default:
                strDia = "SIN RUTA";
        }

        return strDia;
    }


    private void cargarInfoPedido() {

        final ProgressDialog progress = ProgressDialog.show(this, "Validando", "Validando información sin enviar...", true);
        progress.show();

        new Thread(new Runnable() {

            @Override
            public void run() {

                /*generar las acciones en hilo principal de views*/
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (progress != null) {
                            progress.dismiss();
                        }

                        new Task().execute("");
//						
                    }
                });
            }
        }).start();

    }

    private class Task extends AsyncTask<String, Integer, Long> {

        protected Long doInBackground(String... urls) {

            try {
                Thread.sleep(300);
            } catch (Exception e) {
            }


            FormRutaProgramadaActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (DataBaseBO.CargarTotalPedidosSinEnviar()) {

                        Util.mostrarDialogGeneral(FormRutaProgramadaActivity.this, "ATENCIÓN", "Existen 5 o más pedidos por enviar.\nSe recomienda enviar información.");

                    }
                }
            });

            return 0l;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {

            FormRutaProgramadaActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (progressDialog != null)
                        progressDialog.dismiss();

                }
            });

        }
    }


    public void validarClickInfoMas() {

        lyMas = (LinearLayout) findViewById(R.id.lyMas);
        lyInfo = (LinearLayout) findViewById(R.id.lyInformacion);
        tbleMas = (TableLayout) findViewById(R.id.tblLayoutInformacionClienteInferior);
        tbletInfo = (TableLayout) findViewById(R.id.tblLayoutInformacionCliente);
        lyInfo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                tbletInfo.setVisibility(View.VISIBLE);
                tbleMas.setVisibility(View.GONE);
                lyInfo.setBackgroundResource(R.color.blue_focus);
                lyMas.setBackgroundResource(R.color.blue_onfocus);

            }
        });
        lyMas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                tbletInfo.setVisibility(View.GONE);
                tbleMas.setVisibility(View.VISIBLE);
                lyInfo.setBackgroundResource(R.color.blue_onfocus);
                lyMas.setBackgroundResource(R.color.blue_focus);

            }
        });

    }


}
