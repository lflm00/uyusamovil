package celuweb.com.uyusa;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import celuweb.com.DataObject.ItemCarteraListView;

public class ListViewCarteraAdapter extends ArrayAdapter<ItemCarteraListView> {
	
	int icono;
	//int colorTitulo;
	Activity context;
	ItemCarteraListView[] listItems;
	
	ListViewCarteraAdapter(Activity context, ItemCarteraListView[] listItems, int icono/*, int colorTitulo*/) {
		
		super(context, R.layout.item_cartera, listItems);
		this.listItems = listItems; 
		this.context = context;
		
		this.icono = icono;
		//this.colorTitulo = colorTitulo; 
	}
	
	
	
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = context.getLayoutInflater();
		View item = inflater.inflate(R.layout.item_cartera, null);
			
		CheckBox checkListaCartera = (CheckBox)item.findViewById(R.id.checkListaCartera);
		checkListaCartera.setChecked(listItems[position].seleccionado);
		
		checkListaCartera.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
				listItems[position].seleccionado = isChecked;				
			}
		});
		
		TextView lblTitulo = (TextView)item.findViewById(R.id.lblTitulo);
		lblTitulo.setText(listItems[position].titulo);
		
		TextView lblSubtitulo = (TextView)item.findViewById(R.id.lblSubTitulo);
		lblSubtitulo.setText(listItems[position].subTitulo);
		
		return(item);
    }
	
	@Override
	public ItemCarteraListView getItem(int position) {
		
		return listItems[position];
	}
}
