package celuweb.com.uyusa;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.DataBaseBOJ;
import celuweb.com.BusinessObject.DataBaseBOJF;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.MotivoCompra;
import celuweb.com.UI.OnClickListenerImpl;

import android.view.WindowManager;

import androidx.core.app.ActivityCompat;

public class FormNoCompraActivity2 extends Activity implements Sincronizador, TextWatcher {

    private Button btnAceptarFormNoCompra;
    int anchoImg, altoImg;

    ProgressDialog progressDialog;
    Vector<MotivoCompra> listaMotivosNoCompra;
    private LinearLayout lyOpcionVenta;
    private EditText lblObservacionCompra;
    private EditText txtValorNoCompra, txtDescripcion;

    private DecimalFormat df;
    private DecimalFormat dfnd;
    private boolean hasFractionalPart;

    /**
     * Variables Coordenadas
     **/
    ProgressDialog progressDialog2;
    AlertDialog alertDialog;

    AlertDialog alert;
    public double latitud;
    public double longitud;
    private Handler timer = new Handler();

    LocationManager locationManager;
    Location currentLocation = null;
    Location currentLocation2 = null;
    int guardar = 0;
    private Dialog dialogoCoordenadas;
    TaskGPS taskGPS;
    public long time = 2 * 60 * 1000; //Cada 2 minutos lanza la captura de Coordenadas

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_no_compra2);

        iniciarServicioGPS();
        lanzarTimerGPS(LocationManager.NETWORK_PROVIDER);
        Inicializar();

        CargarMotivosNoCompra();

        if (estaGPSEncendido(getApplicationContext())) {


            obtenerCoordenada();

        } else {
            mostrarMensajeActivarGPS();
        }
    }

    public void iniciarServicioGPS() {
        if (!isServiceGPSRunning()) {
            Intent intent = new Intent(this, ServiceGPS.class);
            startService(intent);
        }
    }

    protected boolean isServiceGPSRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            String nameSevice = service.service.getClassName();
            if (ServiceGPS.class.getName().equals(nameSevice)) {
                return true;
            }
        }
        return false;
    }

    public void FormNoCompraActivity(EditText txtValorNoCompra) {
        df = new DecimalFormat("#,###.##");
        df.setDecimalSeparatorAlwaysShown(true);
        dfnd = new DecimalFormat("#,###");
        this.txtValorNoCompra = txtValorNoCompra;
        hasFractionalPart = false;
    }

    @SuppressWarnings("unused")
    private static final String TAG = "FormNoCompraActivity";

    public void Inicializar() {
        Main.fotoNoCompra = null;
        this.btnAceptarFormNoCompra = (Button) findViewById(R.id.btnAceptarFormNoCompra);
        this.btnAceptarFormNoCompra.setOnClickListener(onClickGuardarNoCompra);

        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int ancho = display.getWidth();
        int alto = display.getHeight();

        anchoImg = (ancho * 120) / 240;
        altoImg = (alto * 150) / 320;

        if (Main.fotoNoCompra != null) {
            ((ImageView) findViewById(R.id.imageFoto)).setImageDrawable(Main.fotoNoCompra);
        } else {
            SetPhotoDefault();
        }
    }

    public void SetPhotoDefault() {
        Drawable fotoVacia = getResources().getDrawable(R.drawable.foto_vacia);
        Drawable img = Util.ResizedImage(fotoVacia, anchoImg, altoImg);
        if (img != null)
            ((ImageView) findViewById(R.id.imageFoto)).setImageDrawable(img);
    }

    public void OnClickFormNoCompra(View view) {
        switch (view.getId()) {
            case R.id.btnAceptarFormNoCompra:
                GuardarMotivoNoCompra();
                break;
            case R.id.btnCancelarFormNoCompra:
                Finalizar();
                break;
        }
    }

    public void Finalizar() {
        Main.fotoNoCompra = null;
        //BorrarFotoCapturada();
        finish();
    }

    public void OnClickTomarFoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String filePath = Util.DirApp().getPath() + "/foto.jpg";
        Uri output = Uri.fromFile(new File(filePath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
        startActivityForResult(intent, Const.RESP_TOMAR_FOTO);
    }

    public void OnClickEliminarFoto(View view) {
        if (Main.fotoNoCompra != null) {
            Main.fotoNoCompra = null;
            SetPhotoDefault();
        }
    }

    public void CargarMotivosNoCompra() {

        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();
        listaMotivosNoCompra = DataBaseBO.ListaMotivosNoCompra(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbNoCompra);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                lyOpcionVenta = (LinearLayout) findViewById(R.id.lyOpcionVenta);
                if (listaMotivosNoCompra.get(position).codigo == 99) {

                    lyOpcionVenta.setVisibility(View.VISIBLE);
                    txtValorNoCompra = (EditText) findViewById(R.id.txtValorNoCompra);
                    txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);


                    //    txtValorNoCompra.addTextChangedListener(new NumberTextWatcher(txtValorNoCompra));
                } else {
                    lyOpcionVenta.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public boolean GuardarMotivoNoCompra() {

        /*boolean yaHayVisita = DataBaseBO.consultarPrimeraVisita();
        if (!yaHayVisita) {
            if (Main.fotoNoCompra == null) {
                MostrarAlertDialog("Para registar la no compra en la primera visita, debe primero capturar la foto!");
                return false;
            }
        }*/

        Spinner spinner = (Spinner) findViewById(R.id.cbNoCompra);
        int position = spinner.getSelectedItemPosition();

        if (position != AdapterView.INVALID_POSITION && listaMotivosNoCompra.size() > 0) {

            MotivoCompra motivoCompra = listaMotivosNoCompra.elementAt(position);

            if (Main.encabezado.numero_doc == null)
                Main.encabezado.numero_doc = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor);  //Numero Unico

            String version = ObtenerVersion();
            String imei = ObtenerImei();

            Main.encabezado.codigo_novedad = motivoCompra.codigo;
            Main.encabezado.version = version;
            Main.encabezado.codigo_cliente = Main.cliente.codigo;
            Main.encabezado.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); //Hora en finaliza la Toma del Pedido
            lblObservacionCompra = (EditText) findViewById(R.id.lblObservacionCompra);
            Main.encabezado.observacion = lblObservacionCompra.getText().toString().trim();

            Coordenada coordenada = Coordenada.get(FormNoCompraActivity2.this);
            if (coordenada == null) {

                //El GPS esta ON y aun no ha capturado coordenada.
                DataBaseBO.validarUsuario();

                if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
                    coordenada = new Coordenada();
                    coordenada.codigoVendedor = Main.encabezado.numero_doc;
                    coordenada.codigoCliente = Main.cliente.codigo;
                    coordenada.latitud = 0;
                    coordenada.longitud = 0;
                    coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                    coordenada.estado = Coordenada.ESTADO_GPS_SIN_RESPUESTA;
                    coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                    coordenada.fecha = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
                }
            }

            byte[] byteArray = null;
            if (Main.fotoNoCompra != null) {
                Drawable imgFoto = ResizedImage(320, 480);
                if (imgFoto != null) {
                    Bitmap bitmap = ((BitmapDrawable) imgFoto).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byteArray = stream.toByteArray();
                }
            }

            int opcionVenta = 99;
            lyOpcionVenta = (LinearLayout) findViewById(R.id.lyOpcionVenta);
            if (listaMotivosNoCompra.get(position).codigo == opcionVenta) {
                Main.encabezado.esVenta = 1;
                txtValorNoCompra = (EditText) findViewById(R.id.txtValorNoCompra);
                txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);


                Main.encabezado.valorVenta = txtValorNoCompra.getText().toString().trim();
                Main.encabezado.descripcion = txtDescripcion.getText().toString().trim();

            } else {
                Main.encabezado.esVenta = 0;
                Main.encabezado.valorVenta = "0";
            }

            if (DataBaseBO.GuardarNoCompra(Main.encabezado)) {

                DataBaseBO.guardarCoordenada(coordenada, Main.encabezado.numero_doc);

                if (byteArray != null && Main.fotoNoCompra != null)
                    DataBaseBOJF.GuardarFotoNoCompra(Main.cliente.codigo, Main.usuario.codigoVendedor, Main.encabezado.numero_doc, byteArray);

                Main.fotoNoCompra = null;

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Novedad registrada con exito " + "para el cliente " + Main.cliente.Nombre)
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                DataBaseBO.ActualizaEstadoRutero(Main.cliente.codigo);
                                dialog.cancel();
                                onClickGuardarNoCompra.reset();
                                FormNoCompraActivity2.this.setResult(RESULT_OK);
                                Finalizar();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

                return true;

            } else {
                MostrarAlertDialog("No se pudo registrar la no compra!");
                return false;
            }

        } else {
            MostrarAlertDialog("No se pudo procesar la imagen. Por favor intente nuevamente!");
            return false;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Const.RESP_TOMAR_FOTO && resultCode == RESULT_OK) {

            Main.fotoNoCompra = ResizedImage(anchoImg, altoImg);
            if (Main.fotoNoCompra != null) {
                ImageView imgFoto = (ImageView) findViewById(R.id.imageFoto);
                imgFoto.setImageDrawable(Main.fotoNoCompra);
            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Finalizar();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

        final String mensaje = ok ? "Novedad registrada con exito en el servidor" : msg;

        if (progressDialog != null)
            progressDialog.cancel();

        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormNoCompraActivity2.this);
                builder.setMessage(mensaje)

                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                Main.encabezado = new Encabezado();
                                FormNoCompraActivity2.this.setResult(RESULT_OK);
                                Finalizar();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public Drawable ResizedImage(int newWidth, int newHeight) {

        Matrix matrix;
        FileInputStream fd = null;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;

        try {

            File fileImg = new File(Util.DirApp(), "foto.jpg");

            if (fileImg.exists()) {

                fd = new FileInputStream(fileImg.getPath());
                bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());

                int width = bitmapOriginal.getWidth();
                int height = bitmapOriginal.getHeight();

                //Matrix mtx = new Matrix();

                /**
                 * Si la Orientacion es 90 Grados, se rota la Imagen
                 **/
                //int orientacion = ObtenerOrientacionImg();
                //if (orientacion == 90)
                //	mtx.postRotate(90);

                //Bitmap rotatedBMP = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, mtx, true);

                //width = rotatedBMP.getWidth();
                //height = rotatedBMP.getHeight();

                if (width == newWidth && height == newHeight) {

                    //return new BitmapDrawable(rotatedBMP);
                    return new BitmapDrawable(bitmapOriginal);
                }

                // Reescala el Ancho y el Alto de la Imagen
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                // Crea la Imagen con el nuevo Tamano
                resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);

                //width = resizedBitmap.getWidth();
                //height = resizedBitmap.getHeight();

                //Matrix mtx = new Matrix();
                //int orientacion = ObtenerOrientacionImg();
                //if (orientacion == 90)
                //	mtx.postRotate(90);

                //Bitmap rotatedBMP = Bitmap.createBitmap(resizedBitmap, 0, 0, width, height, mtx, true);

                return new BitmapDrawable(resizedBitmap);
            }

            return null;

        } catch (Exception e) {

            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
            return null;

        } finally {

            if (fd != null) {

                try {

                    fd.close();

                } catch (IOException e) {
                }
            }

            fd = null;
            matrix = null;
            resizedBitmap = null;
            bitmapOriginal = null;
            System.gc();
        }
    }

    public int ObtenerOrientacionImg() {

        String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};

        int orientacion = 0;
        Cursor cursor = null;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        try {

            if (uri != null) {

                cursor = managedQuery(uri, projection, null, null, null);
            }

            if (cursor != null && cursor.moveToLast()) {

                orientacion = cursor.getInt(0);
                Log.i("ObtenerOrientacionImg", "ORIENTATION = " + orientacion);
            }

        } finally {

            if (cursor != null)
                cursor.close();
        }

        return orientacion;
    }

    public void BorrarFotoCapturada() {

        String[] projection = {MediaStore.Images.ImageColumns.SIZE,
                MediaStore.Images.ImageColumns.DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATA,
                BaseColumns._ID,
        };

        Cursor cursor = null;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        try {

            if (uri != null) {

                cursor = managedQuery(uri, projection, null, null, null);
            }

            if (cursor != null && cursor.moveToLast()) {

                ContentResolver contentResolver = getContentResolver();
                int rows = contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, BaseColumns._ID + "=" + cursor.getString(3), null);

                Log.i("BorrarFotoCapturada", "Numero de filas eliminadas : " + rows);
            }

        } finally {

            if (cursor != null)
                cursor.close();
        }
    }

    public String ObtenerVersion() {

        String version;

        try {

            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

        } catch (NameNotFoundException e) {

            version = "0.0";
            Log.e("FormNoCompraActivity", e.getMessage(), e);
        }

        return version;
    }

    public String ObtenerImei() {
        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
    }

    public void MostrarAlertDialog(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
                onClickGuardarNoCompra.reset();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    private OnClickListenerImpl onClickGuardarNoCompra = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {
            GuardarMotivoNoCompra();
        }
    };


    @Override
    protected void onResume() {

        super.onResume();

        if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {
            DataBaseBO.CargarInfomacionUsuario();
        }

        if (Main.cliente == null || Main.cliente.codigo == null) {
            int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;

            if (tipoDeClienteSelec == 1) {
                clienteSel = DataBaseBO.CargarClienteSeleccionado();
            } else {
                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();
            }

            if (clienteSel != null)
                Main.cliente = clienteSel;

        }

    }

    public void onClickRegresar(View view) {
        finish();
    }


    /**
     * EditText  TextWacther
     */

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        txtValorNoCompra.removeTextChangedListener(this);

        try {
            int inilen, endlen;
            inilen = txtValorNoCompra.getText().length();

            String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
            Number n = df.parse(v);
            int cp = txtValorNoCompra.getSelectionStart();
            if (hasFractionalPart) {
                txtValorNoCompra.setText(df.format(n));
            } else {
                txtValorNoCompra.setText(dfnd.format(n));
            }
            endlen = txtValorNoCompra.getText().length();
            int sel = (cp + (endlen - inilen));
            if (sel > 0 && sel <= txtValorNoCompra.getText().length()) {
                txtValorNoCompra.setSelection(sel);
            } else {
                // place cursor at the end?
                txtValorNoCompra.setSelection(txtValorNoCompra.getText().length() - 1);
            }
        } catch (NumberFormatException nfe) {
            // do nothing?
        } catch (ParseException e) {
            // do nothing?
        }

        txtValorNoCompra.addTextChangedListener(this);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()))) {
            hasFractionalPart = true;
        } else {
            hasFractionalPart = false;
        }
    }

    /**
     * Metodos Para guardar coordenadas
     */

    public boolean obtenerCoordenada() {

        boolean estado = false;
        GPSTracker gpsTracker = new GPSTracker(this);
        Location location = gpsTracker.getLocation();
        boolean estadoGPS = gpsTracker.isEstadoGPS();
        double lati = 0;
        double longi = 0;
        //String direccionCapturada = "Direccion Aprox Capturada: ";
        //String ciudadCapturada = "Ciudad Capturada: ";
        if (location != null && estadoGPS) {
            lati = location.getLatitude();
            longi = location.getLongitude();
            //direccionCapturada += gpsTracker.getAddress(location);
            //ciudadCapturada += gpsTracker.getCity(location);
        }

        Log.i("***", " coordenadas capturada LAT: " + lati + " LONG: " + longi);

        if (estadoGPS) {
            if (lati != 0 && longi != 0) {
                Coordenada coordenada = new Coordenada();
                coordenada.codigoCliente = Main.cliente.codigo;
                coordenada.latitud = latitud;
                coordenada.longitud = longitud;
                coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
                coordenada.id = Coordenada.obtenerId(Main.cliente.codigo);
                latitud = lati;
                longitud = longi;
                boolean guardo = Coordenada.save(FormNoCompraActivity2.this, coordenada);
                if (guardo) {
                    Log.i("GUARDO", "OK");
                  /*  Toast.makeText(getApplicationContext(), "Coordenadas Obtenidas con exito \n"+
                            lati+"\n"+longi, Toast.LENGTH_LONG).show();*/
                }
                estado = true;
            } else {

                estado = false;
            }
        } else {
            Util.mostrarAlertDialog(FormNoCompraActivity2.this, "Por favor encienda el GPS");
        }

        return estado;
    }

    public static boolean estaGPSEncendido(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return statusOfGPS;
    }

    private void mostrarMensajeActivarGPS() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormNoCompraActivity2.this);
        builder.setMessage("El GPS est? desactivado. Por favor act?velo").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, 1100);
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void MostrarAlertDialogSinCoordenada() {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                String mensaje = "";
                String bton = "";

                mensaje = "No se obtuvieron las Coordenadas,Desea Continuar sin Coordenadas";
                bton = "Continuar Sin Coordenada";

                AlertDialog.Builder builder = new AlertDialog.Builder(FormNoCompraActivity2.this);
                builder.setCancelable(false).setPositiveButton(bton, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();

                    }
                });

                alertDialog = builder.create();


                alertDialog.setMessage(mensaje);
                alertDialog.show();

            }
        });


    }

    public void lanzarTimerGPS(String provider) {

        //Se guarda el Provider para Captura de Coordenadas
        setGPSProvider(provider);

        Timer timer = new Timer();
        taskGPS = new FormNoCompraActivity2.TaskGPS();
        timer.schedule(taskGPS, 0, time);
    }

    public void setGPSProvider(String provider) {
        SharedPreferences settings = getSharedPreferences("settings_gps", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("provider", provider);
        editor.commit();
    }

    private class TaskGPS extends TimerTask {

        public void run() {

            if (handlerGPS != null)
                handlerGPS.sendEmptyMessage(0);
        }
    }

    private Handler handlerGPS = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (latitud == 0 && longitud == 0) {
                MostrarAlertDialogSinCoordenada();
            } else {
                obtenerCoordenada();
            }
        }
    };

}
