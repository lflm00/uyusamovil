package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.CondicionesBonificacion;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Linea;

public class FormBonificaciones extends Activity {

	Button btnBusquedaProduc;
	
	Vector<Linea> listaLineas;
	Vector<CondicionesBonificacion> listaProductos;
	
	boolean primerEjecucion = true;
	boolean isPedido ;
	int tipoTransaccion;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_bonificaciones);
		
		cargarBundle();
		SetListenerListView();
		buscarBonificaciones();
	}
	private void cargarBundle() {
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			isPedido = bundle.getBoolean("isPedido");
			tipoTransaccion = bundle.getInt("tipoTransaccion");
		}
		
		
		
	}

	
	public void buscarBonificaciones(){

		ItemListView[] items;
		Vector<ItemListView> listaItems = new Vector<ItemListView>();
		listaProductos= DataBaseBO.Bonificaciones(Main.usuario.codigoVendedor,Main.cliente.codigo,Main.cliente.Canal,Main.cliente.bodega,listaItems);

		
		if (listaItems.size() > 0) {

			items = new ItemListView[listaItems.size()];
			Main.itemsBusq = items;
			listaItems.copyInto(items);

		} else {

			items = new ItemListView[] {};
			Main.itemsBusq = items;
			
			if (listaProductos != null)
				listaProductos.removeAllElements();

			Toast.makeText(getApplicationContext(), "Busqueda sin resultados", Toast.LENGTH_SHORT).show();
		}
		
		
		
		ListViewAdapter adapter = new ListViewAdapter(this, items, R.drawable.compra, 0);
		ListView listaBusquedaProductos = (ListView) findViewById(R.id.listaHistorialPedidos);
		listaBusquedaProductos.setAdapter(adapter);
	}
	
	
	public void SetListenerListView() {
		
		ListView listaOpciones = (ListView)findViewById(R.id.listaHistorialPedidos);
		
		listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               
                try {
                	
                	/*Producto producto = listaProductos.elementAt(position);
		
					Intent data =  new Intent();
                	data.putExtra("producto", producto);
                	
                	setResult(RESULT_OK, data);
                	FormBonificaciones.this.finish();*/
                			
				} catch (Exception e) {
					
					String msg = e.getMessage();
					Toast.makeText(getBaseContext(), "Error seleccionado Producto: " + msg, Toast.LENGTH_LONG).show();
				}
            }
        });
	}
	
	
	public void OnClickRegresar (View view){
		
		finish();
	}
	
	
}
