package celuweb.com.uyusa;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Vector;

import celuweb.com.DataObject.ChequeoPrecios;


/**
 * Created by Andres Rangel on 25/10/2017.
 */

public class ChequeoAdapter extends RecyclerView.Adapter<ChequeoAdapter.AdapterViewHolder> {

    Activity context;
    Vector<ChequeoPrecios> arrayList;

    public ChequeoAdapter(Context context, Vector<ChequeoPrecios> arrayList) {
        this.context = (Activity) context;
        this.arrayList = arrayList;
    }

    @Override
    public ChequeoAdapter.AdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate( R.layout.list_items_control2,viewGroup,false);
        AdapterViewHolder adapterViewHolder=new AdapterViewHolder(view);
        return adapterViewHolder;
    }

    @Override
    public void onBindViewHolder(final ChequeoAdapter.AdapterViewHolder holder, final int i) {

            holder.txtNombre.setText(arrayList.get(i).getDescripcion());
            if (arrayList.get(i).getTieneCompetencia()==0){
                holder.btnCompetencia.setVisibility(View.INVISIBLE);
            }else {
                holder.btnCompetencia.setVisibility(View.VISIBLE);
            }
            holder.editNombre.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (editable.length() > 0) {
                        arrayList.get(i).setPrecio(editable.toString());
                       FormChequeoPreciosPropios.hashMapChequeoPropio.put(arrayList.get(i).getCodProducto(), arrayList.get(i));
                    } else {
                        FormChequeoPreciosPropios.hashMapChequeoPropio.remove(arrayList.get(i).getCodProducto());
                    }
                }
            });

        holder.btnCompetencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogChequeoPreciosCompetencia dialogChequeoPreciosCompetencia=
                        new DialogChequeoPreciosCompetencia(context,arrayList.get(i));
                dialogChequeoPreciosCompetencia.setCancelable(false);
                dialogChequeoPreciosCompetencia.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogChequeoPreciosCompetencia.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {
        TextView txtNombre;
        EditText editNombre;
        TableRow tableRow;
        ImageButton btnCompetencia;
        public AdapterViewHolder(View itemView) {
            super(itemView);

            txtNombre= (TextView) itemView.findViewById(R.id.txtNombre);
            editNombre= (EditText) itemView.findViewById(R.id.editNombre);
            tableRow= (TableRow) itemView.findViewById(R.id.tableControl);
            btnCompetencia= (ImageButton) itemView.findViewById(R.id.btnCompetencia);
        }
    }
    public void updateRecords(Vector<ChequeoPrecios> arrayList){
        this.arrayList = arrayList;

        notifyDataSetChanged();
    }
}
