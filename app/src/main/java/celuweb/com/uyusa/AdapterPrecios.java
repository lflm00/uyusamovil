package celuweb.com.uyusa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import celuweb.com.DataObject.InformeInventario;
/**
 * Created by JICZ on 12/02/2019.
 */

public class AdapterPrecios extends BaseAdapter {

    Context context;
    ArrayList<InformeInventario> arrayList;



    public AdapterPrecios (Context context, ArrayList<InformeInventario> arrayList) {

        this.context = context;
        this.arrayList = arrayList;
    }



    @Override
    public int getCount () {

        return arrayList.size();
    }



    @Override
    public Object getItem (int i) {

        return arrayList.get( i );
    }



    @Override
    public long getItemId (int i) {

        return i;
    }

    public void update(ArrayList<InformeInventario> arrayList){
        this.arrayList=arrayList;
        notifyDataSetChanged();
    }

    @Override
    public View getView (int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = LayoutInflater.from(context).
                    inflate(R.layout.layout_list_view_row_items, viewGroup, false);
        }

        InformeInventario informeInventario=arrayList.get( i );


        float promedio = informeInventario.promedio;
        float margen = (float) (1 - (promedio / informeInventario.precio));

        TextView txtTitulo=view.findViewById( R.id.txtTitulo );
        TextView subTitulo=view.findViewById( R.id.txtSubtitulo );
        TextView subTitulo2=view.findViewById( R.id.txtSubtitulo2 );
        txtTitulo.setText( informeInventario.nombre );
        subTitulo.setText( Util.SepararMiles(Util.getDecimalFormatMargen(informeInventario.precio )));
        subTitulo2.setText("   " + Util.getDecimalFormatMargen(margen * 100));

        return view;
    }
}
