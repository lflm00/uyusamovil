package celuweb.com.uyusa;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
public class GPSTracker extends Service implements LocationListener {

	public static final String TAG = GPSTracker.class.getName();

	private Context mContext;

	boolean isGPSEnabled = false;

	boolean isNetworkEnabled = false;

	boolean canGetLocation = false;

	private Location location = null; // location
	private double latitude; // latitude
	private double longitude; // longitude
	private boolean estadoGPS = false;

	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

	protected LocationManager locationManager;

	public GPSTracker(Context context) {
		this.mContext = context;
	}

	public void mostrarToastTop(String mensaje) {
		Toast toast = Toast.makeText(mContext, mensaje, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 0);
		toast.show();
	}


	@SuppressLint("MissingPermission")
	public Location getLocation() {

		try {

			//location = Main.location;
			locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			if (isGPSEnabled || isNetworkEnabled) {
				estadoGPS = true;
			}
			if (!isGPSEnabled && !isNetworkEnabled) {
				return null;
			} else {
				this.canGetLocation = true;


				if (isGPSEnabled) {
					locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
					if (locationManager != null) {
						//location = Main.location;
						location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						}
					}else{
						mostrarToastTop("Hubo un error al obtener las Coordenadas.");
					}
				}

				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
					if (locationManager != null) {
						//location = Main.location;
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							System.out.println("obtuvo coordenadas getLocation isNetworkEnabled");
							System.out.println(latitude + " - " + longitude);
						}

					}else{
						mostrarToastTop("Hubo un error al obtener las Coordenadas.");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "getLocation -> location  "+e.getMessage());
		}
		return location;
	}

	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(GPSTracker.this);
		}
	}

	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			locationManager.removeUpdates(this);
		} else
			System.out.println("GPS TRACKER: location fue null");
	}

	public String getAllAddress(Location location) {
		String direccionCompleta = "";
		if (location != null) {
			try {
				Geocoder geocoder;
				List<Address> addresses;
				geocoder = new Geocoder(this, Locale.getDefault());
				addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // obtiene
				// la
				// direccion
				// de
				// la
				// coordenada

				String direccion = addresses.get(0).getAddressLine(0); // si es
				// null
				// probar
				// getMaxAddressLineIndex()
				String ciudad = addresses.get(0).getLocality();
				String municipio = addresses.get(0).getAdminArea();
				String pais = addresses.get(0).getCountryName();
				String codigopostal = addresses.get(0).getPostalCode();
				String knownName = addresses.get(0).getFeatureName();
				direccionCompleta = direccion + " " + ciudad + " " + municipio + " " + pais;

				System.out.println("COORDENADAS -> latitud: " + location.getLatitude() + " - longitud: " + location.getLongitude());
				System.out.println("DIRECCION -> direccion: " + direccion + " - " + "ciudad: " + ciudad + "estado: " + municipio + "pais: " + pais + "codigopostal: " + codigopostal + "knownName: " + knownName);

			} catch (IOException e) {
				Log.i(TAG, "getAllAddress-> " + e.getMessage());
			}
		}
		return direccionCompleta;
	}

	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public boolean isEstadoGPS() {
		return estadoGPS;
	}

	public void setEstadoGPS(boolean estadoGPS) {
		this.estadoGPS = estadoGPS;
	}

}