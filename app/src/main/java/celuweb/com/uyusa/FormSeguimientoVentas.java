/**
 * 
 */
package celuweb.com.uyusa;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.SeguimientoVenta;
import celuweb.com.DataObject.Usuario;

/**
 * Muestra una tabla dinamica que muestra el estado de ventas del vendedor.
 * 
 * @author JICZ
 * 
 */
public class FormSeguimientoVentas extends Activity {

	private TableLayout tablaCabecera;
	private TableLayout tablaContenido;
	private TableRow.LayoutParams filaTotal;
	private TextView zonaVendedor;

	// defina parametrizacion de una fila de la tabla de contenidos
	private TableRow.LayoutParams params;

	// color de la letra. (Negra)
	int color = R.color.black;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.seguimiento_ventas);

		// instancias de view necesarias
		tablaCabecera = (TableLayout) findViewById(R.id.tableLayoutCabecera);
		tablaContenido = (TableLayout) findViewById(R.id.tablaContenido);
		zonaVendedor = (TextView) findViewById(R.id.textViewSubTituloSeguimiento);

		//mostrar informacion de zona (codigo vendedor)
		Usuario usuario = DataBaseBO.ObtenerUsuario();
		zonaVendedor.setText("Vendedor Zona " + usuario.codigoVendedor.trim());

		// paramtreizacion de tama�o de fila.
		filaTotal = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT);

		// se define para que cada columna ocupe el 50% del peso de su padre
		params = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
		params.weight = 0.5f;

		// crear cabecera
		agregarCabecera();
		// crear contenido
		agregarContenido();
	}

	/**
	 * se define el titulo de las filas de la cabecera de la tabla.
	 */
	private void agregarCabecera() {

		// el tablerow ocupara todo el ancho del padre y sus hijos pueden usar
		// un peso maximo de 1. (weightsum = 1).
		TableRow row = new TableRow(this);
		row.setLayoutParams(filaTotal);
		row.setWeightSum(1f);

		// definicion de columnas
		TextView txtCategoria = new TextView(this);
		TextView txtTotal = new TextView(this);

		txtCategoria.setText("Categoria");
		txtCategoria.setTextColor(color);
		txtCategoria.setBackgroundResource(R.drawable.celda_cabecera_tabla);

		txtTotal.setText("Total ($)");
		txtTotal.setTextColor(color);
		txtTotal.setBackgroundResource(R.drawable.celda_cabecera_tabla);

		// insertar en el table cabecera.
		row.addView(txtCategoria, params);
		row.addView(txtTotal, params);
		tablaCabecera.addView(row);
	}

	/**
	 * se llena el contenido del table con la informacion leida de la tabla
	 * seguimientoventas en database.db
	 */
	private void agregarContenido() {

		// lista con los datos a mostrar
		ArrayList<SeguimientoVenta> listaContenido = DataBaseBO.cargarSeguimientoVenta();

		for (SeguimientoVenta seguimiento : listaContenido) {

			// definicion de una fila que ocupa todo el ancho del padre, es
			// decir de la tabla.
			// el tablerow ocupara todo el ancho del padre y sus hijos pueden
			// usar un peso maximo de 1. (weightsum = 1).
			TableRow row = new TableRow(this);
			row.setLayoutParams(filaTotal);
			row.setWeightSum(1f);

			// definicion de columnas
			final TextView txtCategoria = new TextView(this);
			final TextView txtTotal = new TextView(this);

			// atributos
			txtCategoria.setText(seguimiento.categoria);
			txtCategoria.setTextColor(color);
			txtCategoria.setBackgroundResource(R.drawable.celda_tabla);

			txtTotal.setText(seguimiento.total);
			txtTotal.setTextColor(color);
			txtTotal.setBackgroundResource(R.drawable.celda_tabla);

			/*alinear la cuadricula para que se conserven del mismo tama�o que su par.
			 * el valor height de categoria es siempre mas grande que el de total, por esto se elige como referencia*/
			ViewTreeObserver viewTreeObserver = txtCategoria.getViewTreeObserver();
			viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {

					int height = txtCategoria.getMeasuredHeight();
					txtTotal.setHeight(height);
				}
			});

			// insertar en el table contenido.
			row.addView(txtCategoria, params);
			row.addView(txtTotal, params);
			tablaContenido.addView(row);
		}
	}

}
