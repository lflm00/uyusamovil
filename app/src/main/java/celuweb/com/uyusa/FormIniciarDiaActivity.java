package celuweb.com.uyusa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import celuweb.com.Conexion.Sync;

public class FormIniciarDiaActivity extends Activity implements Sincronizador {
	
	int diaActual;
	int diaSeleccionado;
	
	AlertDialog alertDialog;
	ProgressDialog progressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_iniciar_dia);
		
		((TextView) findViewById(R.id.lblUsuarioIniciarDia)).setText(Main.usuario.nombreVendedor);
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	
    	switch (requestCode) {
    	
			case Const.RESP_FORM_RESUMEN:
				
				setResult(RESULT_OK);
				finish();
				break;
		}
    }
	
	/*public void CargarDatosDiaRuta() {
    	
		String[] items = new String[] {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
    	Spinner cbDiaRuta = (Spinner) findViewById(R.id.cbDiaRuta);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbDiaRuta.setAdapter(adapter);
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        
        diaActual = calendar.get(Calendar.DAY_OF_WEEK);
        diaActual = diaActual == Calendar.SUNDAY ? Const.DOMINGO : diaActual - 2;
        
        cbDiaRuta.setSelection(diaActual);
        diaSeleccionado = diaActual + 1;
    }*/
	
	public void OnClickFormIniciarDia(View view) {
		
		switch (view.getId()) {

			case R.id.btnAceptarIniciarDia:
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("Esta seguro de Iniciar Dia? Iniciar Dia actualizara la informacion. Toda la informacion se borrara.")
				.setCancelable(false)
				.setPositiveButton("Si", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						progressDialog = ProgressDialog.show(FormIniciarDiaActivity.this, "", "Decargando Base de Datos...", true);
						progressDialog.show();
						
						Sync sync = new Sync(FormIniciarDiaActivity.this, Const.DOWNLOAD_DATA_BASE);
						sync.start();
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.cancel();
					}
				});
				
				AlertDialog alert = builder.create();
				alert.show();
				
				
				break;
				
			case R.id.btnCancelarIniciarDia:
				
				finish();
				break;
		}
	}
	
	public void CerrarFormulario() {
		
		setResult(RESULT_OK);
		finish();
	}
	
	public void MostrarAlertDialog(String mensaje) {
    	
    	if (alertDialog == null) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			alertDialog = builder.create();
    	}
    	
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
    }

	@Override
	public void RespSync(boolean ok, final String respuestaServer, String msg, int codeRequest) {
		
		if (progressDialog != null)
			progressDialog.cancel();
		
		if (ok) {
			
			//Intent formResumen = new Intent(this, FormResumenActivity.class);
			//startActivityForResult(formResumen, Const.RESP_FORM_RESUMEN);
			
			this.runOnUiThread(new Runnable() {
				
				public void run() {
					
					AlertDialog.Builder builder = new AlertDialog.Builder(FormIniciarDiaActivity.this);
					builder.setMessage("Sincronizacion Finalizada");
					builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
							dialog.cancel();
							CerrarFormulario();
						}
					});
					
					AlertDialog alert = builder.create();
					alert.show();
				}
			});
			
		} else {
			
			this.runOnUiThread(new Runnable() {
				
				public void run() {

					MostrarAlertDialog(respuestaServer);
				}
			});
		}
	}
}
