package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;

public class FormEstadisticaLineaProductos extends CustomActivity {

	Dialog dialogResumen;
	Vector<Detalle> listaPedidos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_estadistica_linea_productos);
		
//		CargarInfoVendedor();
		CargarResumenPedidos();
		//SetListenerListView();
		
	}
	
	
	public void CargarResumenPedidos() {
		
		Usuario usuario = DataBaseBO.CargarUsuario(); 
		
		ItemListView[] listaItems = null;
		Vector<ItemListView> listaItemsPedido = new Vector<ItemListView>(); 
		listaPedidos = DataBaseBO.CargarPedidosLineaProductos(usuario.fechaConsecutivo, listaItemsPedido);	
		
		if (listaItemsPedido.size() > 0) {
			
			listaItems = new ItemListView[listaItemsPedido.size()];
			listaItemsPedido.copyInto(listaItems);
			
			ListViewAdapter adapter = new ListViewAdapter(this, listaItems, R.drawable.cliente, 0x2E65AD);
			ListView listaPedidosRealizados = (ListView)findViewById(R.id.listaPedidosProducto);
			listaPedidosRealizados.setAdapter(adapter);
			
		} else {
			
			ListViewAdapter adapter = new ListViewAdapter(this, new ItemListView[]{}, R.drawable.cliente, 0x2E65AD);
			ListView listaClientesNoCompra = (ListView)findViewById(R.id.listaPedidosProducto);
			listaClientesNoCompra.setAdapter(adapter);					
		}
		
		int size = listaItemsPedido.size();
		
		String msg = "<b>Ventas por Linea: " + size + "</b>";
		((TextView)findViewById(R.id.lblTitulo)).setText(Html.fromHtml(msg));
	}
	
//	public void CargarInfoVendedor() 
//	{	
//		Usuario usuario = DataBaseBOJB.CargarUsuario(); 
//		Config config = ConfigBO.ObtenerConfigUsuario();
//		if (usuario != null && usuario.codigoVendedor != null) {
//			
//			((TextView) findViewById(R.id.lblVendedorAct)).setText(usuario.codigoVendedor+" - "+usuario.nombreVendedor);
//			
//			
//		} else {
//			
//			((TextView) findViewById(R.id.lblVendedorAct)).setText("Vendedor");
//			
//			
//		}
//	}

	
//	public void mostrarTablaproductosPedidos(){
//	
//		LinearLayout scroll = (LinearLayout)findViewById(R.id.scrollInfoProductosPedidos);
//		scroll.removeAllViews();
//		
//		
//		if (listaPedidos.size() > 0) 
//		{
//			
//			Hashtable<String, String> htUnidades = new Hashtable<String, String>();
//	        Vector<String> listadoUnidades = new Vector<String>();
//			
//			for (Detalle detalle : listaPedidos) 
//			{	
//				if(!detalle.codUnidad1.equals(""))
//				if(!htUnidades.containsKey(detalle.codUnidad1)){
//						htUnidades.put(detalle.codUnidad1,detalle.codUnidad1);
//						listadoUnidades.add(detalle.codUnidad1);
//				}
//				
//				if(!detalle.codUnidad2.equals(""))
//				if(!htUnidades.containsKey(detalle.codUnidad2)){
//					htUnidades.put(detalle.codUnidad2,detalle.codUnidad2);
//					listadoUnidades.add(detalle.codUnidad2);
//				}
//						
//				
//			}
//			
//			
//			TableLayout table = new TableLayout(this);
//				
//			table.setBackgroundColor(Color.WHITE);
//			String[] cabecera = new String[listadoUnidades.size()+3];
//			cabecera[0] = "Producto";
//			cabecera[listadoUnidades.size()+1] = "Valor";
//			cabecera[listadoUnidades.size()+2] = "Nombre";
//			float[] cantidadUnidades = new float[listadoUnidades.size()];
//			float valorTotal = 0;
//			
//			
//			for(int i=0; i<listadoUnidades.size(); i++)
//				cabecera[i+1] = listadoUnidades.elementAt(i);
//			
//			
//			Util.Headers2(table, cabecera, this);
//			scroll.removeAllViews();
//			scroll.addView(table);	
//			TextView textViewAux;
//
//			
//			for (Detalle detalle : listaPedidos) 
//			{	
//				TableRow fila = new TableRow(this);
//
//				textViewAux = new TextView(this);
//				textViewAux.setText("" + detalle.codProducto+ "");		
//				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//				textViewAux.setTextColor(Color.BLUE);
//				textViewAux.setTextSize(10);
//				textViewAux.setGravity(Gravity.RIGHT);
//				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//				fila.addView(textViewAux);
//				
//				for(int i=0; i<listadoUnidades.size(); i++){
//				
//				textViewAux = new TextView(this);
//				if(listadoUnidades.elementAt(i).equals(detalle.codUnidad1))
//				textViewAux.setText("" +detalle.cantidad);	
//				else
//				if(listadoUnidades.elementAt(i).equals(detalle.codUnidad2))
//				textViewAux.setText("" +detalle.cantidad2);	
//				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//				textViewAux.setTextColor(Color.BLUE);	
//				textViewAux.setTextSize(10);
//				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//				fila.addView(textViewAux);
//				
//				
//				if(listadoUnidades.elementAt(i).equals(detalle.codUnidad1))
//				cantidadUnidades[i] = cantidadUnidades[i]+detalle.cantidad;	
//				else
//				if(listadoUnidades.elementAt(i).equals(detalle.codUnidad2))
//				cantidadUnidades[i] = cantidadUnidades[i]+detalle.cantidad2;	
//			
//				
//				}
//				
//				textViewAux = new TextView(this);
//				textViewAux.setText("" + Util.SepararMiles(Util.Redondear(String.valueOf(detalle.precio+detalle.iva-detalle.descuento_autorizado),0)));			
//				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//				textViewAux.setTextColor(Color.BLUE);	
//				textViewAux.setTextSize(10);
//				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//				fila.addView(textViewAux);
//				
//				valorTotal = valorTotal+detalle.precio+detalle.iva-detalle.descuento_autorizado;
//				
//				
//				textViewAux = new TextView(this);
//				textViewAux.setText("" + detalle.desc_producto+ "");		
//				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//				textViewAux.setTextColor(Color.BLUE);
//				textViewAux.setTextSize(10);
//				textViewAux.setGravity(Gravity.RIGHT);
//				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//				fila.addView(textViewAux);
//				
//				
//				
//			
//				table.addView(fila);
//			}
//			
//			
//			TableRow fila = new TableRow(this);
//
//			textViewAux = new TextView(this);
//			textViewAux.setText("Total");		
//			textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//			textViewAux.setTextColor(Color.BLACK);
//			textViewAux.setTextSize(10);
//			textViewAux.setGravity(Gravity.RIGHT);
//			textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//			fila.addView(textViewAux);
//			
//			for(int i=0; i<listadoUnidades.size(); i++){
//			
//			textViewAux = new TextView(this);
//			textViewAux.setText(Util.Redondear("" +cantidadUnidades[i],2));	
//			textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//			textViewAux.setTextColor(Color.BLACK);	
//			textViewAux.setTextSize(10);
//			textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//			fila.addView(textViewAux);
//			
//			}
//			
//			textViewAux = new TextView(this);
//			textViewAux.setText("" + Util.SepararMiles(Util.Redondear(valorTotal+"",0)));			
//			textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//			textViewAux.setTextColor(Color.BLACK);	
//			textViewAux.setTextSize(10);
//			textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//			fila.addView(textViewAux);
//			
//			
//			textViewAux = new TextView(this);
//			textViewAux.setText("");		
//			textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//			textViewAux.setTextColor(Color.BLACK);
//			textViewAux.setTextSize(10);
//			textViewAux.setGravity(Gravity.RIGHT);
//			textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
//			fila.addView(textViewAux);
//		
//			table.addView(fila);
//			
//			
//			
//			
//			
//		} else {
//			
//			
//				
//			
//		}
//		
//		
//	}
	
	public void onClickRegresar (View view){
		
		finish();
	}
	

	
	
}
