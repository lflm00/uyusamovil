package celuweb.com.uyusa;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class FormDibujoLibre extends Activity {

	private DibujoLibreView canvas;

	private final int ANCHO = Const.WIDHTFOTO;
	private final int ALTO = Const.HEIGHTFOTO;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_dibujo_libre);

		inicializar();
	}

	private void inicializar() {

		canvas = (DibujoLibreView) findViewById(R.id.canvas);

		canvas.setAlto(1024);
		canvas.setAncho(1024);

		Bundle extras = getIntent().getExtras();

		if (extras != null && extras.containsKey("firma")) {

			try {

				byte[] firma = extras.getByteArray("firma");

				if (firma != null)
					canvas.setFirma(firma);
			} catch (OutOfMemoryError error) {
				error.printStackTrace();
			}
		}
	}

	public void onClickBorrar(View view) {

		if (canvas != null)
			canvas.borrarDibujo();
	}

	public void onCLickCerrar(View view) {

		Intent intent = new Intent();
		byte[] bytesImg = null;
		if (canvas != null) {
			Bitmap imagen = canvas.getBitmap();
			ByteArrayOutputStream bOut = new ByteArrayOutputStream();
			try {
				if (imagen != null) {
					
					imagen.compress(CompressFormat.JPEG, 80, bOut);
					bytesImg = bOut.toByteArray();
					bOut.flush();
					bOut.close();
				} else {
					setResult(RESULT_CANCELED, intent);
					finish();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (imagen != null) {
					imagen.recycle();
					imagen = null;
				}
			}
		}
		intent.putExtra("imagen", bytesImg);
		setResult(RESULT_OK, intent);
		finish();
	}

}
