package celuweb.com.uyusa;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Foto;

public class FormFotos extends Activity {
	
	Foto fotoSel;
	int posFotoSel;
	int anchoImg, altoImg;
	
	String nroDoc = "";
	String codCliente = "";
	String codVendedor = "";
	AlertDialog alertDialog;
	
	boolean estaGuardando = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_fotos);
		
		CargarExtras();
		Inicializar();		
	}
	
	public void CargarExtras() {
		
		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			
			if (extras.containsKey("nroDoc"))
				nroDoc = extras.getString("nroDoc");
			
			if (extras.containsKey("codCliente"))
				codCliente = extras.getString("codCliente");
			
			if (extras.containsKey("codVendedor"))
				codVendedor = extras.getString("codVendedor");
		}
		
		DataBaseBO.FotosXCliente(nroDoc, codCliente, codVendedor);
		
		Log.i("FormFotos", "nroDoc = " + nroDoc);
		Log.i("FormFotos", "codCliente = " + codCliente);
		Log.i("FormFotos", "codVendedor = " + codVendedor);
	}
	
	public void Inicializar() {
		
		Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int ancho = display.getWidth();
		int alto = display.getHeight();
		
		anchoImg = (ancho * 100) / 240;
		altoImg = (alto * 130) / 320;
		
		IncializarCatalogo();
		
		if (Main.fotoActual != null) {
			
			((ImageView)findViewById(R.id.imageFoto)).setImageDrawable(Main.fotoActual);
			
		} else {
			
			SetPhotoDefault();
		}
	}
	
	public void OnClickTomarFoto(View view) {
		
		if (Main.listaInfoFotos.size() >= 10) {
			
			MostrarAlertDialog("Se ha alcanzado el limite de fotos almacenables para esta medicion");
			return;
		}
		
		fotoSel = null;
		posFotoSel = -1;
		
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String filePath = Util.DirApp().getPath() + "/foto.jpg";
		
		Uri output = Uri.fromFile(new File(filePath));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
		startActivityForResult(intent, Const.RESP_TOMAR_FOTO);
	}
	
	public void OnClickGuardarFoto(View view) {
		
		if (estaGuardando == true) {
			
			estaGuardando = false;
			if (Main.listaInfoFotos.size() >= 10) {
				
				MostrarAlertDialog("Se ha alcanzado el limite de fotos almacenables para esta medicion");
				return;
			}
			
			if (Main.guardarFoto) {
				
				fotoSel = null;
				posFotoSel = -1;
				
				String mensaje;
				Drawable imgFoto = ResizedImage(320, 480);
				
				if (imgFoto != null) {
					
					Bitmap bitmap = ((BitmapDrawable)imgFoto).getBitmap();
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
					byte[] byteArray = stream.toByteArray();
					
					if (byteArray != null && byteArray.length > 0) {
						
						Foto foto = new Foto();
						foto.id          = Util.ObtenerFechaId();
						foto.codCliente  = codCliente;
						foto.codVendedor = codVendedor;
						foto.modulo      = 1;
						foto.nroDoc      = nroDoc;
						
						if (DataBaseBO.GuardarImagen(foto, byteArray)) {
							
							Main.guardarFoto = false;
							imgFoto = ResizedImage(anchoImg, altoImg);
							
							if (imgFoto != null) {
								
								Drawable imgGaleria = ResizedImage(30, 30);
								Main.fotosGaleria.addElement(imgGaleria);
								Main.listaInfoFotos.addElement(foto);
								
								mensaje = "Foto Guardada con Exito";
								Main.fotoActual = null;
								System.gc();
								
							} else {
								
								mensaje = "Foto Guardada con Exito, Error Visualizando la Img.";
							}
							
						} else {
							
							mensaje = "Error guardando la Imagen: " + DataBaseBO.mensaje;
						}
						
					} else {
						
						mensaje = "Error procesando la Imagen 2";
					}
					
				} else {
					
					mensaje = "Error procesando la Imagen 1";
				}
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.cancel();
						SetPhotoDefault();
						
						((ImageAdapter)(((Gallery) findViewById(R.id.galleryFotos))).getAdapter()).notifyDataSetChanged();
						
						fotoSel = null;
						posFotoSel = -1;
					}
				});
				
				AlertDialog alert = builder.create();
				alert.setMessage(mensaje);
				alert.show();
				
			} else {
				
				MostrarAlertDialog("Debe capturar la imagen antes de guardarla");
			}
			
			estaGuardando = true;
		}
	}
	
	public void OnClickEliminarFoto(View view) {
		
		if (fotoSel != null) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this)
			.setMessage("Esta seguro de Eliminar la foto?");
			builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
					
					if (DataBaseBO.BorrarImagen(fotoSel.id)) {
						
						Main.fotosGaleria.removeElementAt(posFotoSel);
						Main.listaInfoFotos.removeElementAt(posFotoSel);
						
						AlertDialog.Builder builder = new AlertDialog.Builder(FormFotos.this);
						builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
							
							public void onClick(DialogInterface dialog, int id) {
								
								dialog.cancel();								
								SetPhotoDefault();
								
								((ImageAdapter)(((Gallery) findViewById(R.id.galleryFotos))).getAdapter()).notifyDataSetChanged();
								
								fotoSel = null;
								posFotoSel = -1;
							}
						});
						
						AlertDialog alert = builder.create();
						alert.setMessage("Foto eliminada con exito");
						alert.show();
						
					} else { 
						
						MostrarAlertDialog("Error eliminando la foto " + DataBaseBO.mensaje);
					}
				}
			}).setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			
		} else {
			
			MostrarAlertDialog("Para eliminar, debe seleccionar una Foto");
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == Const.RESP_TOMAR_FOTO && resultCode == RESULT_OK) {
			
			Main.fotoActual = ResizedImage(anchoImg, altoImg);
			
			if (Main.fotoActual != null) {
				
				Main.guardarFoto = true;
				ImageView imgFoto = (ImageView)findViewById(R.id.imageFoto);
				imgFoto.setImageDrawable(Main.fotoActual);
			}
			
			BorrarFotoCapturada();
		}
	}
	
	public void BorrarFotoCapturada() {

		String[] projection = { MediaStore.Images.ImageColumns.SIZE, 
        						MediaStore.Images.ImageColumns.DISPLAY_NAME, 
        						MediaStore.Images.ImageColumns.DATA, 
        						BaseColumns._ID, };

        Cursor cursor = null;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        try {
        	
            if (uri != null) {
            	
            	cursor = managedQuery(uri, projection, null, null, null);
            }
            
            if (cursor != null && cursor.moveToLast()) {
            	
            	ContentResolver contentResolver = getContentResolver();
                int rows = contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, BaseColumns._ID + "=" + cursor.getString(3), null);

                Log.i("BorrarFotoCapturada", "Numero de filas eliminadas : " + rows);
            }
            
        } finally {
        	
            if (cursor != null)
            	cursor.close();
        }
    }
	
	public Drawable ResizedImage(int newWidth, int newHeight) {
		
		Matrix matrix;
		FileInputStream fd = null;
		Bitmap resizedBitmap = null;
		Bitmap bitmapOriginal = null;
		
		try {
			
			File fileImg = new File(Util.DirApp(), "foto.jpg");
			
			if (fileImg.exists()) {
				
				fd = new FileInputStream(fileImg.getPath());
				bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());

	            int width = bitmapOriginal.getWidth();
	            int height = bitmapOriginal.getHeight();
	            
	            if (width == newWidth && height == newHeight) {
	            	
	            	return new BitmapDrawable(bitmapOriginal);
	            }
	            
	            // Reescala el Ancho y el Alto de la Imagen
	            float scaleWidth = ((float) newWidth) / width;
	            float scaleHeight = ((float) newHeight) / height;

	            matrix = new Matrix();
	            matrix.postScale(scaleWidth, scaleHeight);
	            
	            // Crea la Imagen con el nuevo Tamano
	            resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
	            
	            return new BitmapDrawable(resizedBitmap);
			}
			
			return null;

		} catch(Exception e) {
			
			Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
			return null;
			
		} finally {
			
			if (fd != null) {
				
				try {
					
					fd.close();
					
				} catch (IOException e) { }
			}
			
			fd = null;
			matrix = null;
			resizedBitmap = null;
			bitmapOriginal = null;
			System.gc();
		}
	}
	
	public void IncializarCatalogo() {
		
		Gallery galleryFotos = (Gallery) findViewById(R.id.galleryFotos);
		galleryFotos.setAdapter(new ImageAdapter(this));
		galleryFotos.setSpacing(6);
		
		galleryFotos.setOnItemClickListener(new OnItemClickListener() {
			
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				
				posFotoSel = position;
				fotoSel = Main.listaInfoFotos.elementAt(position);
				CargarFotoSeleccionada();
			}
		});
	}
	
	public void CargarFotoSeleccionada() {
		
		Main.guardarFoto = false;
		byte[] image = DataBaseBO.CargarImagen(fotoSel.id);
		
		if (image != null && image.length > 0) {
			
			Main.fotoActual = Util.ResizedImage(image, anchoImg, altoImg);
			ImageView imgFoto = (ImageView)findViewById(R.id.imageFoto);
			imgFoto.setImageDrawable(Main.fotoActual);
			
		} else {
			
			MostrarAlertDialog("No se pudo cargar la Foto: " + DataBaseBO.mensaje);
		}
	}
	
	public void SetPhotoDefault() {
		
		Drawable fotoVacia = getResources().getDrawable(R.drawable.foto_vacia);
		Drawable img = Util.ResizedImage(fotoVacia, anchoImg, altoImg);
		
		if (img != null)
			((ImageView) findViewById(R.id.imageFoto)).setImageDrawable(img);
	}
	
	public void MostrarAlertDialog(String mensaje) {
    	
    	if (alertDialog == null) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			alertDialog = builder.create();
    	}
    	
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	
	    	Main.fotoActual = null;
	    	Main.fotosGaleria.removeAllElements();
			Main.listaInfoFotos.removeAllElements();
			
			System.gc();
	    }
	    
	    return super.onKeyDown(keyCode, event);
	}
	
	
	public class ImageAdapter extends BaseAdapter {
		
		private Context context;
		int galleryItemBackground;

		public ImageAdapter(Context context) {
			
			this.context = context;
			TypedArray typedArray = obtainStyledAttributes(R.styleable.GalleryFotos);
			galleryItemBackground = typedArray.getResourceId(R.styleable.GalleryFotos_android_galleryItemBackground, 0);
			typedArray.recycle();
		}
		
		public int getCount() {
			
			return Main.fotosGaleria.size();
		}

		public Object getItem(int position) {
			
			return position;
		}
		
		public long getItemId(int position) {
			
			return position;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ImageView i = new ImageView(context);
			i.setImageDrawable(Main.fotosGaleria.elementAt(position));
	        i.setScaleType(ImageView.ScaleType.FIT_XY);
			i.setBackgroundResource(galleryItemBackground);			
			return i;
		}
	}
}
