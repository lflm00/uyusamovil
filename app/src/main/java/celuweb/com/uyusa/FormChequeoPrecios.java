package celuweb.com.uyusa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ChequeoPrecios;
import celuweb.com.DataObject.ItemListView;

public class FormChequeoPrecios extends CustomActivity /*implements GPS*/ {

    boolean primerEjecucion = true;

    Vector<ChequeoPrecios> listaChequeo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_chequeo);
        CargarChequeos();
        SetListenerListView();

    }

    public void CargarChequeos() {

        Vector<ItemListView> listaItems = new Vector<ItemListView>();
        listaChequeo = DataBaseBO.ListaChequeos(listaItems);

        if (listaChequeo.isEmpty()) {
            ItemListView itemListView = new ItemListView();
            listaItems.removeAllElements();
            itemListView.titulo = "!No hay Chequeo de Precios disponibles!";
            listaItems.add(itemListView);
        }

        ItemListView[] items;

        if (listaItems.size() > 0) {

            items = new ItemListView[listaItems.size()];
            listaItems.copyInto(items);

        } else {

            items = new ItemListView[]{};

            if (listaChequeo != null)
                listaChequeo.removeAllElements();
        }

        ListViewAdapter adapter = new ListViewAdapter(this, items, R.drawable.op_informes, 0x2E65AD);
        ListView list = (ListView) findViewById(R.id.listaChequeo);
        list.setAdapter(adapter);
    }

    public void SetListenerListView() {

        ListView listaOpciones = (ListView) findViewById(R.id.listaChequeo);
        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listaChequeo.isEmpty()) {
                    try {
                        finish();
                    } catch (Throwable e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    boolean hayRegistro = DataBaseBO.consultarRegistroChequeo(Main.cliente.codigo);
                    if (hayRegistro) {
                        Util.MostrarAlertDialog(FormChequeoPrecios.this, "Ya ha realizado un chequeo de precios para este cliente el dia de hoy");
                    } else {
                        ChequeoPrecios chequeoPrecios = listaChequeo.elementAt(position);
                        Intent intent = new Intent(getApplicationContext(), FormChequeoPreciosPropios.class);
                        intent.putExtra("id", chequeoPrecios.getId());
                        startActivity(intent);
                    }

                }
            }
        });
    }


    public void OnClickRegresar(View view) {
        finish();
    }


}
