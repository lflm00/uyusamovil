package celuweb.com.uyusa;


import java.io.Serializable;

import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolder implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public TextView text;
    public ImageView image;
    
    
    public TextView titulo;
    public TextView subtitulo;
    
    /**
     * Maneja diferentes estados
     * 0 -> imgDefault (Imagen de Fondo Gris)
     * 1 -> Imagen No Disponible. (No se encuentra la imagen Fisica del producto)
     * 2 -> Debe cargar la Imagen del producto
     **/
    public int img;
    
    /*public String toString() {
    	
    	return "ViewHolder: img = " + img;
    }*/
}
