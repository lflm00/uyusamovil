package celuweb.com.uyusa;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.Catalogo.CatalogoActivity;
import celuweb.com.DataObject.Bonificaciones;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.CondicionesBonificacion;
import celuweb.com.DataObject.ConsecutivoFactura;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.Correo;
import celuweb.com.DataObject.DatePickerFragment;
import celuweb.com.DataObject.DateSetFechaListener;
import celuweb.com.DataObject.DescripsionXProducto;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.ListaPrecios;
import celuweb.com.DataObject.MotivoCambio;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.RegistroRango;
import celuweb.com.DataObject.RegistroVisita;
import celuweb.com.DataObject.Sucursal;
import celuweb.com.DataObject.TopeBonificacion;
import celuweb.com.DataObject.Usuario;
import celuweb.com.DataObject.VendedorBonificacion;
import celuweb.com.UI.OnClickListenerImpl;
import celuweb.com.adapter.ItemClickListener;
import celuweb.com.adapter.MyAdapter;
import celuweb.com.printer.sewoo.SewooLKP20;

@SuppressLint("NewApi")
public class FormPedidoActivity extends Activity implements Sincronizador, ItemClickListener {

    public static final String TAG = FormPedidoActivity.class.getName();

    /**
     * CONSTANTE PARA IDENTIFICAR EL TIPO DE TRANSACCION AUTOVENTA PARA EL
     * PEDIDO ACTUAL.
     */
    private static final boolean AUTOVENTA = true;

    /**
     * CONSTANTE PARA IDENTIFICAR EL TIPO DE TRANSACCION PREVENTA PARA EL PEDIDO
     * ACTUAL.
     */
    private static final boolean PREVENTA = false;

    /**
     * Flag para identificar el tipo de transaccion actual, preventa o
     * autoventa. Inicialmente es de tipo Preventa.
     */
    private boolean tipoTransaccionActual = PREVENTA;
    private float saldoValido;
    //Permite saber si se cambia de bodega o no.
    private int cambiarBodega = 0;

    private long mLastClickTime = 0;
    private long mLastClickTimetTerminar = 0;
    private long mLastClickTimeDialog = 0;

    String version = "";
    String imei = "";
    private String macImpresora;
    String mensaje;
    Dialog dialogPedidoRegistrado;
    private EditText fechaEntrega;
    boolean primerEjecucion = true;
    boolean registro = false;

    Vector<Correo> listaCorreos;
    Vector<Correo> listaCorreosSel;

    public double acomuladoClienteBonificaciones = 0;

    public boolean vendedorTieneBonificacion = false;
    public boolean clienteCumpleBonificacion = false;
    public VendedorBonificacion vendedorBonificacion = null;
    public TopeBonificacion topeBonificacion = null;

    MotivoCambio motivoCambioSel;

    boolean primeraVez = true;
    String[] codProductos;

    String codigoProductoAnterior = "";

    Cliente cliente;
    int tipoDeClienteSeleccionado = 1;

    boolean editar;
    Detalle detalleAct;

    boolean setListener = true;

    Producto producto;
    Detalle detalleEdicion;

    Dialog dialogPedido;
    Dialog dialogEditar;
    Dialog dialogResumen;
    Dialog dialogFechaEntrega;
    ProgressDialog progressDialog;
    Dialog dialogProductosBono, dialogTrasladoBodega;

    // Por defecto lo toma como pedido
    boolean cambio = false;
    String strOpcion;

    Vector<MotivoCambio> listaMotivosCambio;
    Vector<Bonificaciones> listaBonificaciones;
    Vector<Bonificaciones> listaCodigoBono;
    Vector<Detalle> listaDetalle;
    Vector<Detalle> listaDetalleBono;
    Vector<ListaPrecios> listaPrecios;
    Vector<Sucursal> listaSucursal;
    private TextView lblTotalItems;
    private TextView lblTotalItemsPedido;
    Spinner spinnerPrecios;
    private TextView txtCodProducto;
    private TextView txtNombProducto;
    private TextView txtInvProducto, txtInvProducto1, txtInvProducto2;
    private TextView txtPrecioProducto;
    private TextView txtIvaProducto;
    private TextView txtDescProducto;
    private TextView txtPrecioIvaProducto;
    private TextView txtCantidadProducto;
    private TextView txtCantidadCambioProducto;
    private TextView lblPosNavegador;
    private EditText lblPrecioFlete;

    public int posNavegadorProductos = 0;
    public String msjPosNavegador;

    public boolean cambioImgDesc = false;

    public boolean fueUlPedido = false;

    public Spinner spMotivosCambios, spSucursal;

    private boolean modificarPedido = false, estado = true;
    ;
    private boolean isPedidoAnulado = false;
    private Encabezado encabezadoModificar;
    private Encabezado encabezadoAnulado;

    Dialog dialogAlertaLiquidacion;

    private static final String BS_PACKAGE = "com.google.zxing.client.android";

    /**
     * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
     */
    private SewooLKP20 sewooLKP20;

    boolean primerBarCode = true;
    Dialog dialogEscanear;
    private EditText etCodigoEAN;
    boolean esCargue = false; // Por defecto es pedido no cargue
    boolean isAveriaTransporte = false;
    boolean isProductoDanado = false;
    boolean isInventarioLiquidacion = false;

    /**
     * Cuando viene del mod. Estadistica/liquidacion se obtiene el tipo de
     * transaccion
     */
    int tipoTransaccion = 0;
    String tipo = "";
    Usuario liquidador;
    boolean isPedido;

    int cantidadGlobal;
    float descuentoGlobal;
    String codMotivoGlobal;
    private TextView txtCantidadSugerida, lblMargen;
    boolean pedidoRegistrado = false;
    private EditText lblObservacionPedido, lblPrecioSugerido;
    private Button btnMasContenido, btnAlternaLista;
    boolean presionado = false, presionador = false;
    private String codigoProducto;
    private float valor_promedio;
    //Para identificar si esta o no en el detalle
    private Detalle detalle;
    private Vector<Producto> productosAlt;
    //Sirve para la lista de productos alternos
    private RecyclerView listaDatosAlternos;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.form_pedido);

        // identificar si se inicia la activity desde la modificacion de
        // pedidos.
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            encabezadoModificar = (Encabezado) bundle.getSerializable("encabezado");
            encabezadoAnulado = (Encabezado) bundle.getSerializable("encabezadoAnulado");
            isPedido = bundle.getBoolean("pedido");
        }

        if (encabezadoModificar != null) {
            modificarPedido = true;
        } else if (encabezadoAnulado != null) {
            isPedidoAnulado = true;
        }
        // cargar los detalles del pedido.
        if (modificarPedido) {
            cargarListaDetallesPedidos(encabezadoModificar);
        } else if (isPedidoAnulado) {
            cargarListaDetallesPedidos(encabezadoAnulado);
        }

        /*
         * Inicialmente identificar si la transaccion actual sera para Autoventa
         * o para Preventa
         */
        identificarTipoTransaccion();
        Inicializar();
        InicializarDatos();
        cargarInformacionBonificaciones();
        cargarCodigosProductos();

        Util.closeTecladoStartActivity(this);

        //Util.turnOnBluetooth();

        txtNombProducto.requestFocus();

        txtCodProducto.setOnFocusChangeListener(new OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (!primeraVez) {
                        txtCodProducto.setText("");
                    } else {
                        primeraVez = false;
                        txtCantidadProducto.requestFocus();
                    }
                }
            }
        });

        txtCodProducto.addTextChangedListener(textWatcher);

        ((EditText) findViewById(R.id.txtCantidadProducto)).addTextChangedListener(textWatcherCantidad);
        ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).addTextChangedListener(textWatcherCantidadDev);

        if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {

            ((Button) findViewById(R.id.btnCapturarCodigoQR)).setVisibility(Button.GONE);

        }

        SharedPreferences settings = getSharedPreferences("MisPreferenciasFiltro", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("posFamiliaSel", -1);
        editor.putInt("posClaseSel", -1);
        editor.putInt("posProductoSel", -1);
        editor.commit();
    }

    /**
     * metodo para definir el tipo de transaccion actual. por defecto la
     * transaccion es preventa.
     */
    private void identificarTipoTransaccion() {
        Usuario usuario = DataBaseBO.ObtenerUsuario();
        if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            tipoTransaccionActual = AUTOVENTA;
        }
    }

    public void Inicializar() {

        fueUlPedido = false;

        posNavegadorProductos = 0;
        spinnerPrecios = (Spinner) findViewById(R.id.spinnerPrecios);
        txtCodProducto = ((TextView) findViewById(R.id.txtCodigoProducto));
        txtNombProducto = ((TextView) findViewById(R.id.txtNombreProducto));
        txtInvProducto = ((TextView) findViewById(R.id.txtInvProducto));
        txtInvProducto1 = ((TextView) findViewById(R.id.txtInvProducto1));
        txtInvProducto2 = ((TextView) findViewById(R.id.txtInvProducto2));
        lblObservacionPedido = ((EditText) findViewById(R.id.lblObservacionPedido));
        txtPrecioProducto = ((TextView) findViewById(R.id.txtPrecioProducto));
        txtIvaProducto = ((TextView) findViewById(R.id.txtIvaProducto));
        txtDescProducto = ((TextView) findViewById(R.id.txtDescProducto));
        txtPrecioIvaProducto = ((TextView) findViewById(R.id.txtPrecioIvaProducto));
        txtCantidadProducto = ((TextView) findViewById(R.id.txtCantidadProducto));
        txtCantidadCambioProducto = ((TextView) findViewById(R.id.txtCantidadCambio));
        txtCantidadSugerida = ((EditText) findViewById(R.id.txtCantidadSugerida));
        lblPosNavegador = ((TextView) findViewById(R.id.lalPosicionNavegado));
        btnMasContenido = ((Button) findViewById(R.id.btnMasContenido));
        btnAlternaLista = ((Button) findViewById(R.id.btnAlternaLista));
        lblPrecioSugerido = ((EditText) findViewById(R.id.lblPrecioSugerido));
        lblMargen = ((TextView) findViewById(R.id.lblMargen));

        if (Main.navegadorProductos.size() > 0) {

            producto = Main.navegadorProductos.get(posNavegadorProductos);
            codigoProducto = producto.codigo;
            int cantidad = DataBaseBO.consultarCantidadSugerida(producto.codigo, Main.cliente.codigo);
            SpinnerPrecios(producto.codigo, Main.cliente.codigo);
            txtCantidadSugerida.setText(cantidad + "");
            txtCodProducto.setText(producto.codigo);
            txtNombProducto.setText(producto.descripcion);

            final float promedio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            final float precio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).precio + "");
            double precioConDescuentoIva = (precio - (precio * producto.descuento / 100f))
                    * (1 + producto.iva / 100f);

            float margen = (float) (1 - (promedio / precio));

            lblMargen.setText(Util.getDecimalFormatMargen(margen * 100));
            txtInvProducto.setText(String.valueOf(producto.inv1));
            txtInvProducto1.setText(String.valueOf(producto.inv2));
            txtInvProducto2.setText(String.valueOf(producto.inv3));
            txtPrecioProducto.setText(Util.SepararMiles(Util.getDecimalFormatMargen(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            valor_promedio = producto.promedio_fijo;
            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f))
                    * (1 + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            // txtPrecioIvaProducto.setText( Util.SepararMiles(
            // Util.Redondear(String.valueOf( producto.precioIva ), 0) ) );
            txtCantidadProducto.setText("");

            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);
            cambioImgDesc = false;
            // cambiarImagenDescAutorizado();
        } else {

            txtCodProducto.setText("");
            txtNombProducto.setText("");
            txtInvProducto.setText("");
            txtInvProducto1.setText("");
            txtInvProducto2.setText("");
            txtPrecioProducto.setText("");
            txtIvaProducto.setText("");
            txtDescProducto.setText("");
            txtPrecioIvaProducto.setText("");
            txtCantidadProducto.setText("");
            txtCantidadCambioProducto.setText("");
            txtCantidadSugerida.setText("");
            lblPosNavegador.setText("");
            lblMargen.setText("");

            cambioImgDesc = false;
            // cambiarImagenDescAutorizado();
        }


        lblPrecioSugerido.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String valor = lblPrecioSugerido.getText().toString().trim();
                if (!valor.equals("")) {
                    Float valorLimpio = Util.quitarComasYsignoPeso(valor);

                    NumberFormat numberFormat = NumberFormat.getNumberInstance();

                    lblPrecioSugerido.removeTextChangedListener(this);

                    lblPrecioSugerido.setText(lblPrecioSugerido.getText().toString().
                            replace(lblPrecioSugerido.getText().toString(),
                                    numberFormat.format(valorLimpio)));
                    lblPrecioSugerido.setSelection(lblPrecioSugerido.getText().length());

                    lblPrecioSugerido.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void mostrarActualProducto(String cantidad, String cantidadCambio, String observacion) {

        if (Main.navegadorProductos.size() > 0) {

            if (posNavegadorProductos >= Main.navegadorProductos.size()) {

                posNavegadorProductos = 0;
            }

            producto = Main.navegadorProductos.get(posNavegadorProductos);// xxx

            DataBaseBO.setDescProducto(producto);
            int cantidadSugerida = DataBaseBO.consultarCantidadSugerida(producto.codigo, Main.cliente.codigo);
            SpinnerPrecios(producto.codigo, Main.cliente.codigo);
            txtCantidadSugerida.setText(cantidadSugerida + "");
            codigoProducto = producto.codigo;
            txtCodProducto.setText(producto.codigo);


            final float promedio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            final float precio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            double precioConDescuentoIva = (precio - (precio * producto.descuento / 100f))
                    * (1 + producto.iva / 100f);

            float margen = (float) (1 - (promedio / precio));

            lblMargen.setText(Util.getDecimalFormatMargen(margen * 100));
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inv1));
            txtInvProducto1.setText(String.valueOf(producto.inv2));
            txtInvProducto2.setText(String.valueOf(producto.inv3));
            txtPrecioProducto.setText(Util.SepararMiles(Util.getDecimalFormatMargen(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            valor_promedio = producto.promedio_fijo;
            // txtPrecioIvaProducto.setText( Util.SepararMiles(
            // Util.Redondear(String.valueOf( producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f))
                    * (1f + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            txtCantidadProducto.setText(cantidad);
            lblObservacionPedido.setText(observacion);
            txtCantidadCambioProducto.setText(cantidadCambio);
            lblPrecioSugerido.setText("");
            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }

    public void mostrarSigProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {


            posNavegadorProductos++;

            if (posNavegadorProductos >= Main.navegadorProductos.size()) {

                posNavegadorProductos = 0;
            }

            txtNombProducto.requestFocus();
            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);
            int cantidadSugerida = DataBaseBO.consultarCantidadSugerida(producto.codigo, Main.cliente.codigo);
            SpinnerPrecios(producto.codigo, Main.cliente.codigo);
            txtCantidadSugerida.setText(cantidadSugerida + "");
            codigoProducto = producto.codigo;
            txtCodProducto.setText(producto.codigo);


            final float promedio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            final float precio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            double precioConDescuentoIva = (precio - (precio * producto.descuento / 100f))
                    * (1 + producto.iva / 100f);

            float margen = (float) (1 - (promedio / precio));

            lblMargen.setText(Util.getDecimalFormatMargen(margen * 100));
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inv1));
            txtInvProducto1.setText(String.valueOf(producto.inv2));
            txtInvProducto2.setText(String.valueOf(producto.inv3));
            txtPrecioProducto.setText(Util.SepararMiles(Util.getDecimalFormatMargen(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            valor_promedio = producto.promedio_fijo;
            // txtPrecioIvaProducto.setText( Util.SepararMiles(
            // Util.Redondear(String.valueOf( producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f))
                    * (1f + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));
            txtCantidadCambioProducto.setText("");
            lblPrecioSugerido.setText("");
            txtCantidadProducto.setText("");
            txtCantidadProducto.requestFocus();
            setMsjPosNavegador();

            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }

    public void mostrarAntProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {

            posNavegadorProductos--;

            if (posNavegadorProductos < 0) {

                posNavegadorProductos = Main.navegadorProductos.size() - 1;
            }

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);
            int cantidadSugerida = DataBaseBO.consultarCantidadSugerida(producto.codigo, Main.cliente.codigo);
            SpinnerPrecios(producto.codigo, Main.cliente.codigo);
            txtCantidadSugerida.setText(cantidadSugerida + "");
            codigoProducto = producto.codigo;
            txtCodProducto.setText(producto.codigo);


            final float promedio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            final float precio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            double precioConDescuentoIva = (precio - (precio * producto.descuento / 100f))
                    * (1 + producto.iva / 100f);

            float margen = (float) (1 - (promedio / precio));

            lblMargen.setText(Util.getDecimalFormatMargen(margen * 100));
            valor_promedio = producto.promedio_fijo;
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inv1));
            txtInvProducto1.setText(String.valueOf(producto.inv2));
            txtInvProducto2.setText(String.valueOf(producto.inv3));
            txtPrecioProducto.setText(Util.SepararMiles(Util.getDecimalFormatMargen(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles(
            // Util.Redondear(String.valueOf( producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f))
                    * (1f + producto.iva / 100f);
            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));
            txtCantidadCambioProducto.setText("");
            txtCantidadProducto.setText("");
            txtCantidadProducto.requestFocus();
            lblPrecioSugerido.setText("");
            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }

    public void mostrarPrimerProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {

            txtNombProducto.requestFocus();
            posNavegadorProductos = 0;

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);
            int cantidadSugerida = DataBaseBO.consultarCantidadSugerida(producto.codigo, Main.cliente.codigo);
            SpinnerPrecios(producto.codigo, Main.cliente.codigo);
            codigoProducto = producto.codigo;
            txtCantidadSugerida.setText(cantidadSugerida + "");
            txtCodProducto.setText(producto.codigo);

            final float promedio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            final float precio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            double precioConDescuentoIva = (precio - (precio * producto.descuento / 100f))
                    * (1 + producto.iva / 100f);

            float margen = (float) (1 - (promedio / precio));

            lblMargen.setText(Util.getDecimalFormatMargen(margen * 100));
            valor_promedio = producto.promedio_fijo;
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inv1));
            txtInvProducto1.setText(String.valueOf(producto.inv2));
            txtInvProducto2.setText(String.valueOf(producto.inv3));
            txtPrecioProducto.setText(Util.SepararMiles(Util.getDecimalFormatMargen(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));
            // txtPrecioIvaProducto.setText( Util.SepararMiles(
            // Util.Redondear(String.valueOf( producto.precioIva ), 0) ) );

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f))
                    * (1f + producto.iva / 100f);

            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));
            txtCantidadCambioProducto.setText("");
            txtCantidadProducto.setText("");
            txtCantidadProducto.requestFocus();
            lblPrecioSugerido.setText("");

            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }

    public void mostrarUltimoProducto() {

        fueUlPedido = false;

        if (Main.navegadorProductos.size() > 0) {

            txtNombProducto.requestFocus();
            posNavegadorProductos = Main.navegadorProductos.size() - 1;

            producto = Main.navegadorProductos.get(posNavegadorProductos);

            DataBaseBO.setDescProducto(producto);
            int cantidadSugerida = DataBaseBO.consultarCantidadSugerida(producto.codigo, Main.cliente.codigo);
            SpinnerPrecios(producto.codigo, Main.cliente.codigo);
            txtCantidadSugerida.setText(cantidadSugerida + "");
            codigoProducto = producto.codigo;

            final float promedio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            final float precio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            double precioConDescuentoIva = (precio - (precio * producto.descuento / 100f))
                    * (1 + producto.iva / 100f);

            float margen = (float) (1 - (promedio / precio));

            lblMargen.setText(Util.getDecimalFormatMargen(margen * 100));
            txtCodProducto.setText(producto.codigo);
            valor_promedio = producto.promedio_fijo;
            txtNombProducto.setText(producto.descripcion);
            txtInvProducto.setText(String.valueOf(producto.inv1));
            txtInvProducto1.setText(String.valueOf(producto.inv2));
            txtInvProducto2.setText(String.valueOf(producto.inv3));
            txtPrecioProducto.setText(Util.SepararMiles(Util.getDecimalFormatMargen(producto.precio)));
            txtIvaProducto.setText(Util.Redondear(String.valueOf(producto.iva), 2));
            txtDescProducto.setText(Util.Redondear(String.valueOf(producto.descuento), 2));

            double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f))
                    * (1f + producto.iva / 100f);

            txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 2)));

            // txtPrecioIvaProducto.setText( Util.SepararMiles(
            // Util.Redondear(String.valueOf( producto.precioIva ), 0) ) );
            txtCantidadProducto.setText("");
            txtCantidadProducto.requestFocus();
            txtCantidadCambioProducto.setText("");
            lblPrecioSugerido.setText("");
            setMsjPosNavegador();
            lblPosNavegador.setText(msjPosNavegador);

            // cambiarImagenDescAutorizado();
        }
    }

    public void InicializarDatos() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("cambio"))
            cambio = bundle.getBoolean("cambio");

        if (bundle != null && bundle.containsKey("esCargue"))
            esCargue = bundle.getBoolean("esCargue");

        String textHeader = "";

        if (isPedido)
            textHeader = getString(R.string.pedido);
        else if (esCargue)
            textHeader = getString(R.string.cargue);
        else if (cambio)
            textHeader = getString(R.string.devolucion);

        ((TextView) findViewById(R.id.tvHeader)).setText(textHeader);

        // if(esCargue)
        // ((LinearLayout)findViewById(R.id.lytLiquidar)).setVisibility(View.GONE);
        //

        strOpcion = cambio ? getString(R.string.devolucion) : getString(R.string.pedidos);

        if (tipoTransaccionActual) {

            if (bundle != null && bundle.containsKey("tipoTransaccion"))
                tipoTransaccion = bundle.getInt("tipoTransaccion");

            liquidador = new Usuario();
            if (bundle != null && bundle.containsKey("liquidador"))
                liquidador = (Usuario) bundle.getSerializable("liquidador");

            if (liquidador != null)
                System.out.println("Liquidador:\n\n" + liquidador.codigoVendedor + " - " + liquidador.nombreVendedor);

            switch (tipoTransaccion) {
                case Const.IS_PRODUCTO_DANADO:

                    isProductoDanado = true;
                    tipo = "de productos da�ados.";
                    strOpcion = "registro de productos da�ados";

                    break;

                case Const.IS_AVERIA_TRANSPORTE:

                    isAveriaTransporte = true;
                    tipo = "de averias de transporte.";
                    strOpcion = "registro de averias";

                    break;
                case Const.IS_INVENTARIO_LIQUIDACION:

                    isInventarioLiquidacion = true;
                    tipo = "para realizar el inventario.";
                    strOpcion = "registro de inventario";

                    break;

                default:
                    break;
            }

        }

        if (!esCargue || !isAveriaTransporte || !isInventarioLiquidacion || !isProductoDanado) {// si
            // no
            // es
            // cargue
            // eliminamos
            // el
            // cliente
            // propio

            DataBaseBO.CargarInfomacionUsuario();
            DataBaseBO.eliminarClientePropio(Main.usuario.codigoVendedor);

        }

        if (esCargue) {
            strOpcion = "Cargue";
        }

        setTitle("Registrar " + strOpcion);

        if (cambio) {

            TableLayout tblyPedidos = (TableLayout) findViewById(R.id.tblyPedidos);

            tblyPedidos.setVisibility(TableLayout.GONE);

            spMotivosCambios = (Spinner) findViewById(R.id.spMotivosDeCambio);

            String[] items;
            Vector<String> listaItems = new Vector<String>();
            listaMotivosCambio = DataBaseBO.ListaMotivosCambio(listaItems);

            if (listaItems.size() > 0) {

                items = new String[listaItems.size()];
                listaItems.copyInto(items);

            } else {

                items = new String[]{};
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spMotivosCambios.setAdapter(adapter);

        } else {

            TableLayout tblyDevoluciones = (TableLayout) findViewById(R.id.tblyCambios);

            tblyDevoluciones.setVisibility(TableLayout.GONE);

        }

        if (Main.detallePedido == null)
            Main.detallePedido = new Hashtable<String, Detalle>();
        else
            CargarListaPedido();

        if (setListener) {

            SetKeyListener();
            SetFocusListener();
            SetListenerListView();
            setListener = false;
        }
        if (cambio) {

            ((ImageView) (findViewById(R.id.btnUltimoPedido))).setVisibility(Button.GONE);
            ((Button) (findViewById(R.id.btnLiquidar))).setVisibility(Button.GONE);
        }
    }

    public void OnClickTerminarPedido(View view) {
        /*
         * retardo de 1500ms para evitar eventos de doble click. esto significa
         * que solo se puede hacer click cada 1500ms. es decir despues de
         * presionar el boton, se debe esperar que transcurran 1500ms para que
         * se capture un nuevo evento.
         */

        if (SystemClock.elapsedRealtime() - mLastClickTimetTerminar < 3000) {
            return;
        }
        mLastClickTimetTerminar = SystemClock.elapsedRealtime();

        if (pedidoRegistrado) {

            AlertDialog alertDialog;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false)

                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                            Main.encabezado = new Encabezado();
                            Main.detallePedido.clear();
                        }
                    });

            alertDialog = builder.create();
            alertDialog.setMessage(getString(R.string.ya_registro_pedido_en_este_formulario_));
            alertDialog.show();

        } else {

            AlertDialog alertDialog;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

//                    if (tipoTransaccion == 0)
//                        ((ListView) findViewById(R.id.listaFormPedido)).setEnabled(false);

                    dialog.cancel();
                    continuarTransaccion();
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                }
            });

            alertDialog = builder.create();
            alertDialog.setMessage("Esta Seguro de Terminar?");
            alertDialog.show();

        }

    }

    protected void cargarPromocionCliente() {

        boolean successful = false;

        successful = CalcularBonificacion();

        if (successful) {

            this.runOnUiThread(new Runnable() {

                public void run() {

                    dialogProductosBono = null;

                    if (dialogProductosBono == null) {
                        dialogProductosBono = new Dialog(FormPedidoActivity.this);
                        dialogProductosBono.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogProductosBono.setContentView(R.layout.activity_dialog_bonificacion);

                        ((Button) dialogProductosBono.findViewById(R.id.btnSalir))
                                .setOnClickListener(new OnClickListener() {

                                    public void onClick(View v) {

                                       // ((ListView) findViewById(R.id.listaFormPedido)).setEnabled(false);

                                        if (progressDialog != null)
                                            progressDialog.cancel();

                                        dialogProductosBono.cancel();
                                        dialogProductosBono = null;
                                    }
                                });

                        ((Button) dialogProductosBono.findViewById(R.id.btnAceptar))
                                .setOnClickListener(new OnClickListener() {

                                    public void onClick(View v) {

                                        if (progressDialog != null)
                                            progressDialog.cancel();

                                        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
                                        txtCodigoProducto.setError(null);

                                        OcultarTeclado(txtCodigoProducto);
                                        ValidarPedido();

                                        dialogProductosBono.cancel();
                                        dialogProductosBono = null;
                                    }
                                });

                        dialogProductosBono.setCancelable(false);
                        dialogProductosBono.show();
                        BuscarProductosBonoTemp();

                    }
                }
            });

        } else {

            if (progressDialog != null)
                progressDialog.cancel();

            this.runOnUiThread(new Runnable() {

                public void run() {

                    EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
                    txtCodigoProducto.setError(null);

                    OcultarTeclado(txtCodigoProducto);
                    ValidarPedido();
                }
            });

        }

    }

    private void eliminarTemporal() {

        int tipoTrans = 0;
        String tipoReg = "";
        if (tipoTransaccion == Const.IS_PRODUCTO_DANADO) {
            tipoReg = "DANADO";
            tipoTrans = Const.TIPO_TRANS_PRODUCTO_DANADO;
        } else if (tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION) {
            tipoReg = "PRODUCTO INVENTARIO";
            tipoTrans = Const.TIPO_TRANS_INVENTARIO_LIQUIDACION;
        }

        boolean isFinalizado = DataBaseBO.isFinalizadoRegistro(tipoTrans);

        if (!isFinalizado)
            DataBaseBO.eliminarRegistroTemporal(tipoReg);

    }

    public void OnClickCancelarPedido(View view) {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        OcultarTeclado(txtCodigoProducto);
        txtCodigoProducto.setError(null);
        CancelarPedido();
    }

    public void OnClickLiquidarPedido(View view) {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        OcultarTeclado(txtCodigoProducto);
        txtCodigoProducto.setError(null);

        Intent formLiquidar = new Intent(this, FormLiquidarActivity.class);
        if (esCargue)
            formLiquidar.putExtra("isCargue", true);
        startActivityForResult(formLiquidar, Const.RESP_PEDIDO_EXITOSO);
    }

    public void OnClickTomarFoto(View view) {

        if (Main.detallePedido.size() > 0) {

            Intent intent = new Intent(this, FormFotos.class);
            intent.putExtra("nroDoc", Main.encabezado.numero_doc);
            intent.putExtra("codCliente", Main.cliente.codigo);
            intent.putExtra("codVendedor", Main.usuario.codigoVendedor);

            startActivity(intent);

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Para tomar Fotos. Por favor primero ingrese los productos.").setCancelable(false)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void OnClickOpcionesPedido(View view) {

            int totalItems;
            EditText txtCodigoProducto;

            // try {
            // Util.playSound2();
            // } catch (Exception e) {
            // }

            switch (view.getId()) {

                case R.id.btnAceptarOpcionesPedido:
                    presionado = true;
                    presionador = true;
                    onClickMas();
                    onClickMasReferencia();
                    /*
                     * retardo de 1500ms para evitar eventos de doble click. esto
                     * significa que solo se puede hacer click cada 1500ms. es decir
                     * despues de presionar el boton, se debe esperar que transcurran
                     * 1500ms para que se capture un nuevo evento.
                     */
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
                    OcultarTeclado(txtCodigoProducto);

                    String codigoBusqueda = txtCodigoProducto.getText().toString().trim();
                    String posibleCodigo = DataBaseBO.buscarProductoPorCodigo(codigoBusqueda);
                    totalItems = Main.detallePedido.size();

                    /*
                     * if (totalItems >= Const.MAX_ITEMS) {
                     *
                     * Util.MostrarAlertDialog(this,
                     * "Se ha superado el maximo de Productos: " + Const.MAX_ITEMS +
                     * ".");
                     *
                     * } else {
                     */

                    fueUlPedido = false;

                    String codigoProducto = txtCodigoProducto.getText().toString().trim();

                    if (codigoProducto.equals("")) {

                        txtCodigoProducto.setError("Ingrese el codigo");
                        txtCodigoProducto.requestFocus();

                    } else {
                        codigoProductoAnterior = codigoProducto;
                        CargarProducto_(posibleCodigo);
                        txtCodigoProducto.setError(null);
                    }
                    // }
                    break;

                case R.id.btnBuscarOpcionesPedido:
                    presionado = true;
                    presionador = true;
                    onClickMas();
                    onClickMasReferencia();

                    totalItems = Main.detallePedido.size();

                    /*
                     * if (totalItems >= Const.MAX_ITEMS) {
                     *
                     * Util.MostrarAlertDialog(this,
                     * "Se ha superado el maximo de Productos: " + Const.MAX_ITEMS +
                     * ".");
                     *
                     * } else {
                     */

                    fueUlPedido = false;
                    ((EditText) findViewById(R.id.txtCodigoProducto)).setError(null);
                    Intent formBuscarProducto = new Intent(this, FormBuscarProducto.class);
                    formBuscarProducto.putExtra("isPedido", isPedido);
                    formBuscarProducto.putExtra("tipoTransaccion", tipoTransaccion);
                    startActivityForResult(formBuscarProducto, Const.RESP_BUSQUEDA_PRODUCTO);
                    // }
                    break;

                case R.id.btnPrimerProducto:
                    presionado = true;
                    presionador = true;
                    onClickMas();
                    onClickMasReferencia();
                    mostrarPrimerProducto();
                    break;

                case R.id.btnAntProducto:
                    presionado = true;
                    presionador = true;
                    onClickMas();
                    onClickMasReferencia();
                    mostrarAntProducto();
                    break;

                case R.id.btnSigProducto:
                    presionado = true;
                    presionador = true;
                    onClickMas();
                    onClickMasReferencia();
                    mostrarSigProducto();
                    break;

                case R.id.btnUltimoProducto:
                    presionado = true;
                    presionador = true;
                    onClickMas();
                    onClickMasReferencia();
                    mostrarUltimoProducto();
                    break;

                case R.id.btnAgregarProducto:
                    int cerrado= DataBaseBO.permisosParaPedido();

                    if (cerrado == 1){
                        MostrarAlertDialog("Usuario no autorizado para toma de pedidos!");
                    }else {


                        presionado = true;
                        presionador = true;
                        onClickMas();
                        onClickMasReferencia();
                        /*
                         * retardo de 1500ms para evitar eventos de doble click. esto
                         * significa que solo se puede hacer click cada 1500ms. es decir
                         * despues de presionar el boton, se debe esperar que transcurran
                         * 1500ms para que se capture un nuevo evento.
                         */
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        RegistrarProducto();
                    }
                    break;

                case R.id.btnMasContenido:
                    onClickMas();
                    break;
                case R.id.btnAlternaLista:
                    onClickMasReferencia();
                    break;

                // case R.id.btnDescAutorizado:
                //
                // fueUlPedido = false;
                // String cantidad = txtCantidadProducto.getText().toString();
                // String codProducto = producto.codigo;
                // float precio = producto.precio;
                //
                // if (!cantidad.equals("")) {
                //
                // Intent formDescAutorizados = new Intent(this,
                // FormDescAutorizadosActivity.class);
                // formDescAutorizados.putExtra("producto", codProducto);
                // formDescAutorizados.putExtra("cantidad", cantidad);
                // formDescAutorizados.putExtra("precio", precio);
                // startActivityForResult(formDescAutorizados,
                // Const.RESP_DESC_AUTORIZADOS);
                //
                // } else {
                //
                // Util.MostrarAlertDialog(this, "Antes de Registrar un Descuento
                // Autorizado Para Este
                // Producto. Por Favor Ingrese la Cantidad", 1);
                //
                // }
                //
                // break;

                /*
                 * case R.id.btnCancelarPedido:
                 *
                 * txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
                 * OcultarTeclado(txtCodigoProducto); txtCodigoProducto.setError(null);
                 * CancelarPedido(); break;
                 *
                 * case R.id.btnTerminarPedido:
                 *
                 * txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
                 * txtCodigoProducto.setError(null);
                 *
                 * OcultarTeclado(txtCodigoProducto); ValidarPedido(); break;
                 */
            }

    }

    public void OnClickUltimoPedido(View view) {

        ((EditText) findViewById(R.id.txtCodigoProducto)).setError(null);

        fueUlPedido = true;
        Intent intent = new Intent(this, FormUltimoPedidoActivity.class);

        startActivityForResult(intent, Const.RESP_ULTIMO_PEDIDO);
    }

    private void AgregarProductoPedido(int cantidad, float descuento, String codMotivo, int cantidadCambio, String observacion) {

        if (tipoTransaccion == Const.IS_PRODUCTO_DANADO || tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION) {

            AgregarProductoPedido2(cantidad, descuento, codMotivo);
            return;

        }

        // EditText txtCodigoProducto =
        // (EditText)findViewById(R.id.txtCodigoProducto);
        // String codigoProducto = txtCodigoProducto.getText().toString();

        detalle = Main.detallePedido.get(producto.codigo);
        String precioProducto = "";

        if (detalle == null) {

            if (producto != null) {

                tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

                if (tipoDeClienteSeleccionado == 1) {

                    cliente = DataBaseBO.CargarClienteSeleccionado();

                } else {

                    cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
                }

                if (tipoTransaccionActual == AUTOVENTA) {

                    if (tipoTransaccion > 0 || esCargue) {

                        cliente = new Cliente();
                        cliente.codigo = "0";
                    }

                }

                detalle = new Detalle();
                detalle.codCliente = cliente.codigo; // tipoTransaccionActual ==
                // AUTOVENTA &&
                // isAveriaTransporte ? "0" : cliente.codigo;
                detalle.codProducto = producto.codigo;
                detalle.nombProducto = producto.descripcion;
                detalle.precio = producto.precio;
                detalle.iva = producto.iva;
                detalle.descuento = descuento;
                detalle.cantidad = cantidad;
                detalle.tipo_pedido = Const.PEDIDO_VENTA;
                detalle.codMotivo = codMotivo;
                detalle.inventario = producto.inventario;
                detalle.indice = producto.indice;
                detalle.cantidadCambio = cantidadCambio;
                detalle.codGrupo = producto.grupo;
                detalle.observacion = observacion;

                // detalle.tipoGrupo = DataBaseBO.tipoGrupo(producto.grupo);

                if (cambio) {

                    detalle.codMotivoCambio = motivoCambioSel.codigo;
                    detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                    detalle.codMotivo = detalle.codMotivoCambio;
                }
            } else {

                AlertResetRegistrarProducto(getString(R.string.no_se_pudo_leer_la_informacion_del_producto));
            }
        } else {

            detalle.cantidad = cantidad;
            detalle.descuento = descuento;
            detalle.observacion = observacion;
            detalle.precio = producto.precio;
            detalle.cantidadCambio = cantidadCambio;

            if (cambio) {

                detalle.codMotivoCambio = motivoCambioSel.codigo;
                detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                detalle.codMotivo = detalle.codMotivoCambio;

            }
        }
        saldoValido = DataBaseBO.obtenerCantidad(producto.codigo);

        if (saldoValido < cantidad) {
            detalle.detalleVinventario = 1;
        }

        if (!lblPrecioSugerido.getText().toString().trim().equals("")) {
            String valor = lblPrecioSugerido.getText().toString().trim();
            Float valorLimpio = Util.quitarComasYsignoPeso(valor);


            final float promedio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).promedio + "");
            final float precio = Float.parseFloat(listaPrecios.get(spinnerPrecios.getSelectedItemPosition()).precio + "");

            String valorModificado = (lblPrecioSugerido.getText().toString().trim());
            float valorModificadoLimpio = Util.quitarComasYsignoPeso(valorModificado);
            producto.precio = valorLimpio;
            detalle.precio = producto.precio;

            AlertDialog.Builder builder = new AlertDialog.Builder(FormPedidoActivity.this);
            builder.setMessage("Desea aplicar el cambio de precio cuando el margen es del\n " + Util.getDecimalFormat3(1 - (promedio / valorModificadoLimpio)) + "%?")
                    .setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    guardarDatos();
                    dialog.dismiss();
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();


            if ((1 - (promedio / valorModificadoLimpio)) < 0.10f) {

                /*AlertDialog.Builder builder = new AlertDialog.Builder(FormPedidoActivity.this);
                builder.setMessage("Desea aplicar el cambio de precio cuando el margen es del\n " + Util.getDecimalFormat((1 - (promedio / valorModificadoLimpio)) * 100) + "%?")
                        .setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        guardarDatos();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();*/


            } else {
                //guardarDatos();
            }
        } else {
            guardarDatos();
        }

    }

    public void guardarDatos() {
        lblObservacionPedido.setText("");
        int posicion = Main.detallePedido.size();
        detalle.posicion = posicion;
        detalle.inven1 = producto.inv1;
        detalle.inven2 = producto.inv2;
        detalle.inven3 = producto.inv3;
        detalle.invenVendedor = String.valueOf(saldoValido);
        if (detalle.cantidad > saldoValido) {
            mostrarDialogoTrasladoBodega(detalle.cantidad);
        }
        Main.detallePedido.put(producto.codigo, detalle);
        EditText txtCantidadProc = (EditText) findViewById(R.id.txtCantidadProducto);
        txtCantidadProc.setText("");
        txtCantidadCambioProducto.setText("");
        OcultarTeclado(txtCantidadProc);
        EditText txtPrecio = (EditText) findViewById(R.id.lblPrecioSugerido);
        txtPrecio.setText("");
        OcultarTeclado(txtPrecio);
        CargarListaPedido();
    }


    private void ActualizarProductoPedido(Detalle detalle, int cantidad, float descuento) {

        if (detalle == null) {

            AlertResetRegistrarProducto("No se pudo leer la informacion del Producto!");
            return;
        }

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        String codProducto = txtCodigoProducto.getText().toString();

        boolean actualizo = DataBaseBO.ActualizarProductoPedido(Main.encabezado, codProducto, cantidad);

        if (actualizo) {

            detalle.cantidad = cantidad;
            detalle.descuento = descuento;

            Main.detallePedido.put(codProducto, detalle);

            EditText txtCantidadProc = (EditText) dialogPedido.findViewById(R.id.txtCantidadProc);
            txtCantidadProc.setText("");

            OcultarTeclado(txtCantidadProc);

            txtCodigoProducto.setText("");
            txtCodigoProducto.requestFocus();

            CargarListaPedido();
            dialogPedido.cancel();

        } else {

            AlertResetRegistrarProducto("No se pudo Editar los datos del Producto!");
        }
    }

    private boolean CargarProducto(String codigoProducto) {

        producto = new Producto();
        boolean cargo = DataBaseBO.ProductoXCodigo(codigoProducto, producto);

        if (cargo) {

            posNavegadorProductos = producto.indice - 1;
            mostrarActualProducto("", "", "");
            return true;

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("No se encontro el Producto").setCancelable(false).setPositiveButton(R.string.accept,
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
            return false;
        }
    }

    private boolean CargarProducto_(String codigoProducto) {

        producto = new Producto();
        boolean cargo = DataBaseBO.ProductoXCodigo(codigoProducto, producto);

        if (cargo) {

            posNavegadorProductos = producto.indice - 1;
            mostrarActualProducto("", "", "");
            return true;

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("No se encontro el Producto de Codigo: " + codigoProducto).setCancelable(false)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                            // CargarProducto(codigoProductoAnterior);
                            mostrarActualProducto("", "", "");

                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
            return false;
        }
    }

    public boolean MostrarDialogPedido(String codigoProducto) {

        try {

            if (onClickRegistrarProducto != null)
                onClickRegistrarProducto.reset();

            if (dialogPedido == null) {

                dialogPedido = new Dialog(this);
                dialogPedido.setContentView(R.layout.dialog_pedido);
                dialogPedido.setTitle("Registrar Producto");

                String[] items;
                Vector<String> listaItems = new Vector<String>();
                listaMotivosCambio = DataBaseBO.ListaMotivosCambio(listaItems);

                if (listaItems.size() > 0) {

                    items = new String[listaItems.size()];
                    listaItems.copyInto(items);

                } else {

                    items = new String[]{};
                }

                Spinner cbMotivoCambio = (Spinner) dialogPedido.findViewById(R.id.cbMotivoCambio);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                        items);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                cbMotivoCambio.setAdapter(adapter);

            } else {

                ((EditText) dialogPedido.findViewById(R.id.txtCantidadProc)).requestFocus();
            }

            dialogPedido.onContentChanged();

            if (cambio) {

                ((TableLayout) dialogPedido.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.VISIBLE);

            } else {

                ((TableLayout) dialogPedido.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.GONE);
            }

            if (producto != null) { // Se carga la Informacion del Producto

                ((TextView) dialogPedido.findViewById(R.id.lblDescProducto)).setText(producto.descripcion);
                ((TextView) dialogPedido.findViewById(R.id.lblPrecio)).setText(
                        "Precio: " + Util.SepararMiles("" + producto.precio) + " - Iva: " + producto.iva + "%");
                ((TextView) dialogPedido.findViewById(R.id.lblInventario))
                        .setText("Lim Venta: " + String.valueOf(producto.inventario));

                detalleAct = Main.detallePedido.get(codigoProducto);

                if (detalleAct != null) {

                    editar = true;
                    ((EditText) dialogPedido.findViewById(R.id.txtCantidadProc)).setText("" + detalleAct.cantidad);

                    if (cambio)
                        SelecionarMotivoCambio(detalleAct.codMotivo);

                } else {

                    editar = false;
                    ((EditText) dialogPedido.findViewById(R.id.txtCantidadProc)).setText("");
                }

                ((Button) dialogPedido.findViewById(R.id.btnVentaOpcionesPedido))
                        .setOnClickListener(onClickRegistrarProducto);

                ((Button) dialogPedido.findViewById(R.id.btnCancelarOpcionesPedido))
                        .setOnClickListener(new OnClickListener() {

                            public void onClick(View v) {

                                dialogPedido.cancel();
                                onClickRegistrarProducto.reset();
                            }
                        });

                dialogPedido.setCancelable(false);
                dialogPedido.show();

                return true;

            } else { // No se Encontro el Producto

                ((TextView) dialogPedido.findViewById(R.id.lblDescProducto)).setText("-");
                ((TextView) dialogPedido.findViewById(R.id.lblPrecio)).setText("-");

                AlertResetRegistrarProducto("No se encontro el Producto!");
                return false;
            }

        } catch (Exception e) {

            onClickRegistrarProducto.reset();
            return false;
        }
    }

    public void RegistrarProducto() {

        boolean bloqueado = DataBaseBO.consultarProductoBloqueado(producto.codigo, Main.cliente.codigo);
        if (bloqueado) {
            MostrarAlertDialog("El producto se encuentra bloqueado por el area Administrativa.");
            return;
        }

        if (!cambio) {

            String cant = ((EditText) findViewById(R.id.txtCantidadProducto)).getText().toString().trim();
            int cantidad = Util.ToInt(cant);

            String cantCambio = ((EditText) findViewById(R.id.txtCantidadCambio)).getText().toString().trim();
            int cantidadCambio = Util.ToInt(cantCambio);


            String desc = ((TextView) findViewById(R.id.txtDescProducto)).getText().toString();
            float descuento = Util.ToFloat(desc);

            EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
            String codigoProducto = txtCodigoProducto.getText().toString();

            String observacion = lblObservacionPedido.getText().toString().toString().trim();

            if (cantidad == 0 && cantidadCambio == 0) {

                if (cambio) {
                    MostrarAlertDialog("Debe Ingresar la cantidad para la Devolucion");
                } else if (tipoTransaccionActual == AUTOVENTA) {

                    if (isInventarioLiquidacion || isProductoDanado || isAveriaTransporte) {
                        MostrarAlertDialog("Debe Ingresar la cantidad " + tipo);
                        return;
                    }
                } else {
                    MostrarAlertDialog("Debe Ingresar la cantidad para la Venta");
                }

            }

            // else if(tipoTransaccionActual == AUTOVENTA && (isProductoDanado
            // ||
            // isAveriaTransporte)){
            // AgregarProductoPedido(cantidad, descuento, "0");
            // return;
            // }

            else if (cantidad > 5 && isAveriaTransporte) {

                MostrarAlertDialog("Solo puede ingresar un maximo de 5 unidades de averias.");

            } else if (producto.precio <= 0) {

                MostrarAlertDialog("El producto no tiene precio valido, para la venta");
            } else {

                if (producto.inventario < cantidad && tipoTransaccionActual == AUTOVENTA && !esCargue
                        && !isInventarioLiquidacion && !isProductoDanado) {// si
                    // es
                    // cargue
                    // no
                    // validar
                    // inventario

                    Toast toast = new Toast(getApplicationContext());

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toask_layout, (ViewGroup) findViewById(R.id.lytLayout));

                    TextView txtMsg = (TextView) layout.findViewById(R.id.txtMensaje);
                    txtMsg.setText("La cantidad supera el inventario disponible");

                    toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setView(layout);
                    toast.show();
                    return;
                }

                int unidadesCajaProducto = DataBaseBO.ObtenerUnidadesCajaProducto(Main.cliente.Canal, producto.codigo);

                if (unidadesCajaProducto == 0) {

                    AgregarProductoPedido(cantidad, descuento, "0", cantidadCambio, observacion);

                } else {

                    if (cantidad % unidadesCajaProducto == 0) {

                        AgregarProductoPedido(cantidad, descuento, "0", cantidadCambio, observacion);

                    } else {

                        Util.MostrarAlertDialog(FormPedidoActivity.this,
                                "La Cantidad Ingresada Debe Ser Multiplo De " + unidadesCajaProducto, 1);

                    }

                }

            }

            EditText txtCantidadProducto = (EditText) findViewById(R.id.txtCantidadProducto);

            OcultarTeclado(txtCantidadProducto);

           /* if (fueUlPedido) {

                Intent intent = new Intent(this, FormUltimoPedidoActivity.class);
                startActivityForResult(intent, Const.RESP_ULTIMO_PEDIDO);
            }*/

        } else {

            String cant = ((EditText) findViewById(R.id.txtCantidadProducto)).getText().toString();
            int cantidad = Util.ToInt(cant);

            String desc = ((TextView) findViewById(R.id.txtDescProducto)).getText().toString();
            float descuento = Util.ToFloat(desc);

            EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
            String codigoProducto = txtCodigoProducto.getText().toString();

            String observacion = lblObservacionPedido.getText().toString().trim();

            if (cantidad == 0) {
                if (cambio) {
                    MostrarAlertDialog("Debe Ingresar la cantidad para la Devolucion");
                } else if (tipoTransaccionActual == AUTOVENTA) {

                    if (isInventarioLiquidacion) {
                        MostrarAlertDialog("Debe Ingresar la cantidad " + tipo);
                    } else if (isProductoDanado || isAveriaTransporte) {

                        int pos = -1;

                        pos = spMotivosCambios.getSelectedItemPosition();
                        motivoCambioSel = listaMotivosCambio.elementAt(pos);
                        AgregarProductoPedido(cantidad, descuento, "1", 0, observacion);

                    }
                } else {
                    MostrarAlertDialog("Debe Ingresar la cantidad para la Venta");
                }

            } else if (cantidad > 5 && isAveriaTransporte) {

                MostrarAlertDialog("Solo puede ingresar un maximo de 5 unidades de averias.");

            } else {

                int pos = -1;

                pos = spMotivosCambios.getSelectedItemPosition();

                motivoCambioSel = listaMotivosCambio.elementAt(pos);

                AgregarProductoPedido(cantidad, descuento, "1", 0, observacion);

            }

        }

    }

    private void SelecionarMotivoCambio(String codMotivo) {

        int size = listaMotivosCambio.size();
        for (int j = 0; j < size; j++) {

            MotivoCambio motivoCambio = listaMotivosCambio.elementAt(j);

            if (dialogPedido != null && motivoCambio.codigo.equals(codMotivo)) {

                ((Spinner) dialogPedido.findViewById(R.id.cbMotivoCambio)).setSelection(j);
                break;
            }
        }
    }

    /**
     * Verifica si el pedio tiene productos asignados, en caso tal muestra el
     * resumen del Pedido y pide al usuario las observaciones Si el pedido esta
     * Vacio, muestra un mensaje de advertencia al usuario
     **/
    public void ValidarPedido() {

        if (Main.detallePedido.size() > 0) {

            long pedidoMinimo = DataBaseBO.getPedidoMinimo();

            if (Main.usuario.codigoVendedor == null) {
                DataBaseBO.CargarInfomacionUsuario();
            }

            if (pedidoMinimo > (long) Main.encabezado.sub_total && !cambio) {

                AlertResetRegistrarProducto(getString(R.string.valor_minimo_permitido_) + pedidoMinimo + " )");
            } else {
                MostrarDialogResumen();
            }
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.el_) + strOpcion + getString(R.string._esta_vacio_)).setCancelable(false)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void MostrarDialogResumen() {

        if (isPedido) {

            if (!Main.cliente.formaPago.equals("FPA2")) {

                long cupo = (long) (Main.cliente.Cupo + DataBaseBO.getValorRecaudoCliente(Main.cliente.codigo));
                if (tipoDeClienteSeleccionado == 1) {

                    if (Main.encabezado.sub_total > cupo) {

                        //((ListView) findViewById(R.id.listaFormPedido)).setEnabled(false);

                        final Dialog dialog = new Dialog(this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_generico);
                        ((TextView) dialog.findViewById(R.id.txtMensajeGeneral)).setText("El Valor del pedido Supera El Cupo");
                        ((Button) dialog.findViewById(R.id.btnAceptarGenerico)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogoResumen();
                                dialog.cancel();
                            }
                        });
                        dialog.setCancelable(false);
                        dialog.show();
                    }else{
                        dialogoResumen();
                    }
                }

            }
        }


    }

    public void dialogoResumen(){
        try {

            if (onClickGuardarPedido != null)
                onClickGuardarPedido.reset();

            if (dialogResumen == null) {
                dialogResumen = new Dialog(this);
                dialogResumen.setContentView(R.layout.dialog_resumen_pedido);

                spSucursal = (Spinner) dialogResumen.findViewById(R.id.spSucursal);

                CheckBox checkPromocion = (CheckBox) dialogResumen.findViewById(R.id.checkPromocion);
                setCheckedPromocion(checkPromocion);

                CheckBox checkCotizacion = (CheckBox) dialogResumen.findViewById(R.id.checkCotizacion);
                setCheckedCotizacion(checkCotizacion);

                String[] items;
                Vector<String> listaItems = new Vector<String>();
                listaSucursal = DataBaseBO.cargarListaSucursal(listaItems, Main.cliente.codigo);

                if (listaItems.size() > 0) {

                    items = new String[listaItems.size()];
                    listaItems.copyInto(items);

                } else {

                    items = new String[]{};
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spSucursal.setAdapter(adapter);

                lblPrecioFlete = ((EditText) dialogResumen.findViewById(R.id.txtPrecioFlete));

                //Condicion para el felete determina si esta vacio o ingresaron algun valor

                lblPrecioFlete.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String valor = lblPrecioFlete.getText().toString().trim();
                        if (!valor.equals("")) {
                            Float valorLimpio = Util.quitarComasYsignoPeso(valor);

                            NumberFormat numberFormat = NumberFormat.getNumberInstance();

                            lblPrecioFlete.removeTextChangedListener(this);

                            lblPrecioFlete.setText(lblPrecioFlete.getText().toString().
                                    replace(lblPrecioFlete.getText().toString(),
                                            numberFormat.format(valorLimpio)));
                            lblPrecioFlete.setSelection(lblPrecioFlete.getText().length());

                            lblPrecioFlete.addTextChangedListener(this);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


                /**
                 * Se registran los eventos de los Botones del Formulario Dialog
                 * Resumen
                 */
                ((Button) dialogResumen.findViewById(R.id.btnAceptarResumenPedido))
                        .setOnClickListener(onClickGuardarPedido);

                ((Button) dialogResumen.findViewById(R.id.btnCancelarResumenPedido))
                        .setOnClickListener(new OnClickListener() {

                            public void onClick(View v) {

//                                if (tipoTransaccion == 0)
//                                    ((ListView) findViewById(R.id.listaFormPedido)).setEnabled(false);

                                EditText txtObservacionPedido = ((EditText) dialogResumen
                                        .findViewById(R.id.txtObservacionPedido));
                                OcultarTeclado(txtObservacionPedido);

                                dialogResumen.cancel();
                                onClickGuardarPedido.reset();
                            }
                        });

                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                date = diaActualMasUno();

                System.out.println("fecha: " + sdf.format(date));

                fechaEntrega = (EditText) dialogResumen.findViewById(R.id.lblFechaEntrega);
                fechaEntrega.setText(sdf.format(date));

                fechaEntrega.addTextChangedListener(new CustomTextWatcher(fechaEntrega));

                ((Button) dialogResumen.findViewById(R.id.btnSelecionarFechaEntrega))
                        .setOnClickListener(new OnClickListener() {

                            public void onClick(View v) {
                                // MostrarDialogFechaEntrega();

                                DatePickerFragment datePikker = new DatePickerFragment(new DateSetFechaListener() {

                                    @SuppressLint("NewApi")
                                    @Override
                                    public void getFechaString(String fecha) {
                                        fechaEntrega.setText(fecha);
                                    }
                                });
                                datePikker.show(FormPedidoActivity.this.getFragmentManager(), "datePicker");
                                onClickGuardarPedido.reset();
                            }
                        });

            } else {
                ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).setText("");
                ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido)).setText("");
                ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido)).setText("");
                ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).requestFocus();
            }

            // Pedido

            Enumeration<Detalle> e = Main.detallePedido.elements();

            float subTotal = 0;
            float valorDesc = 0;
            float valorIva = 0;
            float valorNeto = 0;

            while (e.hasMoreElements()) {

                Detalle detalle = e.nextElement();

                float sub_total = 0;

                if (esCargue) {

                    sub_total = detalle.cantidad * detalle.precio * detalle.factorCajas;

                } else {

                    sub_total = detalle.cantidad * detalle.precio;

                }

                float valor_iva = 0;
                float valor_descuento = sub_total * (detalle.descuento / 100f);

                if (sub_total > 0.1)
                    valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100f);

                subTotal += sub_total;
                valorIva += valor_iva;
                valorDesc += valor_descuento;

                // detalle.strPrecio =
                // Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" +
                // detalle.precio), 0));
            }

            valorNeto = subTotal + valorIva - valorDesc;

            Main.encabezado.sub_total = subTotal;
            Main.encabezado.total_iva = valorIva;
            Main.encabezado.valor_descuento = valorDesc;
            Main.encabezado.valor_neto = valorNeto;

            if (cambio) {

                ((TextView) dialogResumen.findViewById(R.id.lblSubTotalResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + 0), 0)));
                ((TextView) dialogResumen.findViewById(R.id.lblIVAResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + 0), 0)));
                ((TextView) dialogResumen.findViewById(R.id.lblDescuentoResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + 0), 0)));
                ((TextView) dialogResumen.findViewById(R.id.lblTotalResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + 0), 0)));
                ((TableLayout) dialogResumen.findViewById(R.id.tableLayoutResumenCliente))
                        .setVisibility(TableLayout.VISIBLE);

            } else {
                ((TextView) dialogResumen.findViewById(R.id.lblSubTotalResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + subTotal), 0)));
                ((TextView) dialogResumen.findViewById(R.id.lblIVAResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorIva), 0)));
                ((TextView) dialogResumen.findViewById(R.id.lblDescuentoResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorDesc), 0)));
                ((TextView) dialogResumen.findViewById(R.id.lblTotalResumenPedido))
                        .setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + valorNeto), 0)));

                ((TableLayout) dialogResumen.findViewById(R.id.tableLayoutResumenCliente))
                        .setVisibility(TableLayout.VISIBLE);
                // TextView fechaEntrega = ((TextView)
                // dialogResumen.findViewById(R.id.lblFechaEntrega));
                //// fechaEntrega.setText(Util.DateToStringYYYYMMDD2());

                if (modificarPedido) {
                    // mostrar datos insertados anteriormente,
                    ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido))
                            .setText(encabezadoModificar.observacion);
                    ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido))
                            .setText(encabezadoModificar.observacion2);
                    ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido))
                            .setText(encabezadoModificar.ordenCompra);
                    fechaEntrega.setText(encabezadoModificar.fechaEntrega.substring(0, 10).replace("-", "").trim());
                    ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).requestFocus();
                }
            }

            ((TextView) dialogResumen.findViewById(R.id.lblClienteResumenPedido)).setText(Main.cliente.Nombre);

            if (esCargue)
                dialogResumen.setTitle("Resumen Cargue");
            else
                dialogResumen.setTitle("Resumen " + strOpcion);
            dialogResumen.setCancelable(false);
            dialogResumen.show();



        } catch (Exception e) {

            Log.e("MostrarDialogResumen", e.getMessage(), e);

            if (onClickGuardarPedido != null)
                onClickGuardarPedido.reset();


        }
    }

    public void MostrarDialogEdicion() {

        if (dialogEditar == null) {

            dialogEditar = new Dialog(this);
            dialogEditar.setContentView(R.layout.dialog_editar);
            dialogEditar.setTitle("Opciones Edicion");

            // if(modificarPedido){
            // ((RadioButton)
            // dialogEditar.findViewById(R.id.radioEditar)).setVisibility(View.GONE);
            // }

            ((RadioButton) dialogEditar.findViewById(R.id.radioEditar)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    posNavegadorProductos = detalleEdicion.indice - 1;

                    dialogEditar.cancel();
                    mostrarActualProducto(String.valueOf(detalleEdicion.cantidad), String.valueOf(detalleEdicion.cantidadCambio)
                            , String.valueOf(detalleEdicion.observacion));
                }
            });

            ((RadioButton) dialogEditar.findViewById(R.id.radioEliminar)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(FormPedidoActivity.this);
                    builder.setMessage("Esta Seguro de eliminar el producto " + detalleEdicion.codProducto)
                            .setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            Main.detallePedido.remove(detalleEdicion.codProducto);
                            CargarListaPedido();
                            dialogEditar.cancel();
                        }
                    }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialogEditar.cancel();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            ((RadioButton) dialogEditar.findViewById(R.id.radioCancelar)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    dialogEditar.cancel();
                }
            });

        } else {

            ((RadioButton) dialogEditar.findViewById(R.id.radioEditar)).requestFocus();
        }

        dialogEditar.show();
    }

    public void CargarListaPedido() {

        Main.encabezado.sub_total = 0;
        Main.encabezado.total_iva = 0;
        Main.encabezado.valor_descuento = 0;

        ItemListView itemListView;

        List<Detalle> listaDetalle = new ArrayList<Detalle>();
        Enumeration<Detalle> e = Main.detallePedido.elements();
        Vector<ItemListView> datosPedido = new Vector<ItemListView>();

        while (e.hasMoreElements()) {
            Detalle detalle = (Detalle) e.nextElement();

            if (esCargue && detalle.factorCajas == 0) {

                detalle.factorCajas = DataBaseBO.ObtenerFactorCanastaProducto(detalle.codProducto);

            }

            listaDetalle.add(detalle);
        }

        // Se ordena de tal forma que se conserve el orden en que se han
        // ingresado los productos al pedido.
        Collections.sort(listaDetalle);
        Iterator<Detalle> e2 = listaDetalle.iterator();

        while (e2.hasNext()) {

            Detalle detalle = e2.next();
            itemListView = new ItemListView();

            float sub_total = 0;
            if (esCargue) {

                sub_total = (detalle.cantidad * detalle.factorCajas) * detalle.precio;

            } else {

                sub_total = detalle.cantidad * detalle.precio;

            }
            float valor_descuento = sub_total * (detalle.descuento) / 100f;
            float valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100f);

            Main.encabezado.sub_total += sub_total;
            Main.encabezado.total_iva += valor_iva;
            Main.encabezado.valor_descuento += valor_descuento;

            if (esCargue) {

                itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                itemListView.subTitulo = "Precio: $"
                        + Util.SepararMilesSin(Util.Redondear(Util.getDecimalFormat(detalle.precio), 0)) + "  Iva: "
                        + detalle.iva + "%  Cajas: " + detalle.cantidad + "  Unidades: "
                        + String.valueOf(detalle.cantidad * detalle.factorCajas) + "  Monto: $"
                        + Util.SepararMilesSin(Util.Redondear(
                        Util.getDecimalFormat(detalle.precio * detalle.cantidad * detalle.factorCajas), 0));
                datosPedido.addElement(itemListView);

            } else {

                if (cambio) {

                    itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                    itemListView.subTitulo = "- Cantidad Devolucion: " + detalle.cantidad + " - Motivo: "
                            + detalle.descripcionMotivoCambio;
                    datosPedido.addElement(itemListView);

                } else {
                    if (detalle.cantidadCambio > 0 && detalle.cantidad > 0) {

                        itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                        itemListView.subTitulo = "Precio: $"
                                + Util.SepararMilesSin(Util.Redondear("" + detalle.precio, 2)) + "  Iva: "
                                + detalle.iva + "%  Cant: " + detalle.cantidad + "  Monto: $" + Util.SepararMilesSin(
                                Util.Redondear("" + detalle.precio * detalle.cantidad, 2)) + "\nCantDevolucion: " + detalle.cantidadCambio;
                        datosPedido.addElement(itemListView);
                    } else if (detalle.cantidadCambio > 0 && detalle.cantidad == 0) {
                        itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                        itemListView.subTitulo = "Precio: $"
                                + Util.SepararMilesSin(Util.Redondear("" + detalle.precio, 2)) + "  Iva: "
                                + detalle.iva + "% " + "\nCantDevolucion: " + detalle.cantidadCambio;
                        datosPedido.addElement(itemListView);

                    } else {
                        itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                        itemListView.subTitulo = "Precio: $"
                                + Util.SepararMilesSin(Util.Redondear("" + detalle.precio, 2)) + "  Iva: "
                                + detalle.iva + "%  Cant: " + detalle.cantidad + "  Monto: $" + Util.SepararMilesSin(
                                Util.Redondear("" + detalle.precio * detalle.cantidad, 2));
                        datosPedido.addElement(itemListView);
                    }
                }

            }
        }

        ItemListView[] listaItems = new ItemListView[datosPedido.size()];
        datosPedido.copyInto(listaItems);

        Main.encabezado.valor_neto = Main.encabezado.sub_total - Main.encabezado.valor_descuento
                + Main.encabezado.total_iva;
        Main.encabezado.str_valor_neto = Util
                .SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + Main.encabezado.valor_neto), 0));
        Main.encabezado.str_valor_neto_producto = Util
                .SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + Main.encabezado.valor_neto), 0));

        if (cambio) {

            Main.encabezado.sub_total = 0;
            Main.encabezado.total_iva = 0;
            Main.encabezado.valor_descuento = 0;
        }

        ListViewAdapterPedido adapter = new ListViewAdapterPedido(this, listaItems, R.drawable.compra, 0);
        ListView listaPedido = (ListView) findViewById(R.id.listaFormPedido);
        listaPedido.setAdapter(adapter);

        /** SE PROCEDE A CARGAR EN LA TABLA TEMPORAL LOS TEMPORALES **/
        DataBaseBO.RegistrarPedidoTemp(Main.detallePedido);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            if ((requestCode == Const.RESP_BUSQUEDA_PRODUCTO || requestCode == Const.RESP_ULTIMO_PEDIDO)
                    || requestCode == Const.RESP_FILTRO_PRODUCTO) {

                if (data != null) {

                    int cantUltPedido;
                    Bundle bundle = data.getExtras();
                    Object productoSel = bundle.get("producto");

                    if (productoSel != null && productoSel instanceof Producto) {

                        producto = (Producto) productoSel;
                        ((EditText) findViewById(R.id.txtCodigoProducto)).setText(producto.codigo);

                        posNavegadorProductos = producto.indice - 1;

                        if (requestCode == Const.RESP_ULTIMO_PEDIDO) {

                            cantUltPedido = bundle.getInt("cantidad");
                            mostrarActualProducto(String.valueOf(cantUltPedido), "", "");
                        } else {

                            mostrarActualProducto("", "", "");
                        }

                    }

                } else if (requestCode == Const.RESP_ULTIMO_PEDIDO) {

                    CargarListaPedido();
                }

            } else if (requestCode == Const.RESP_FORMAS_DE_PAGO) {

                MostrarDialogResumen();
            } else if (requestCode == Const.RESP_DESC_AUTORIZADOS) {

                cambioImgDesc = true;
                // cambiarImagenDescAutorizado();
            } else if (requestCode == Const.RESP_CODIGOBARRAS) {

                // if (resultCode == Activity.RESULT_OK) {

                String contents = data.getStringExtra("SCAN_RESULT");

                String codProducto = DataBaseBO.ExisteCodigoEnProductos(contents);

                if (!codProducto.equals("")) {

                    CargarProducto(codProducto);

                } else {
                    Util.mostrarToast(this, "No existe un producto registrado con ese codigo");
                }

                // }
            } else if (requestCode == Const.RESP_LIQUID_EXITOSO) {

                FinalizarPedido();
                // MostrarDialogResumen();
            }
        } else if (resultCode == RESULT_CANCELED) {

            switch (requestCode) {

                case Const.RESP_LIQUID_EXITOSO:
                    tipoTransaccionActual = AUTOVENTA;
                    System.out.println("Entr� al cancel desde liquidacion");
                    tipoTransaccion = data.getIntExtra("liquidacionOpciones", tipoTransaccion);
                    liquidador = (Usuario) data.getSerializableExtra("liquidador");

                    System.out.println("TipoTrans --> " + tipoTransaccion);
                    System.out.println("liquidador --> " + liquidador);
                    break;

            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            CancelarPedido();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void CancelarPedido() {

        if (Main.detallePedido.size() > 0) {

            /**
             * Ya ha tomado un Pedido, Se pregunta al Usuario si desea
             * Cancelarlo
             **/
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Esta Seguro de cancelar el " + strOpcion + "? La informacion se Borrara")
                    .setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    // boolean cancelo =
                    // DataBaseBO.CancelarPedido(Main.encabezado);

                    boolean cancelo = true;

                    if (cancelo) {

                        Main.encabezado = new Encabezado();
                        Main.detallePedido.clear();

                        dialog.cancel();
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED, returnIntent);
                        finish();

                    } else {

                        Util.MostrarAlertDialog(FormPedidoActivity.this, "No se pudo cancelar el " + strOpcion);
                    }
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    moveTaskToBack(false);
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        } else {

            /**
             * El pedido esta vacio. Se verifica si se guardo un Encabezado en
             * caso tal se borra. Se cierra el formulario.
             **/

            if (Main.encabezado.numero_doc != null) {

                DataBaseBO.CancelarPedido(Main.encabezado);
            }

            Main.encabezado = new Encabezado();

            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
    }

    public void FinalizarPedido() {

        if (tipoTransaccion == 0)
            ((Button) dialogResumen.findViewById(R.id.btnAceptarResumenPedido)).setEnabled(false);

        /* Evitar evento de doble click */
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        Usuario usuario = DataBaseBO.CargarUsuario();

        if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            tipoTransaccionActual = AUTOVENTA;

        }

        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

        if (tipoDeClienteSeleccionado == 1) {

            cliente = DataBaseBO.CargarClienteSeleccionado();

        } else {
            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
        }

        if (Main.usuario.codigoVendedor == null)
            DataBaseBO.CargarInfomacionUsuario();

        if (tipoTransaccionActual == AUTOVENTA) {
            if (tipoTransaccion > 0 || esCargue) {
                Main.encabezado.codigo_cliente = usuario.codigoVendedor;
                Main.encabezado.observacion = "";
                Main.encabezado.observacion2 = "";
                Main.encabezado.ordenCompra = "";
            } else {
                Main.encabezado.codigo_cliente = cliente.codigo;
                Main.encabezado.observacion = ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido))
                        .getText().toString().trim();
                Main.encabezado.observacion2 = ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido))
                        .getText().toString().trim();
                Main.encabezado.ordenCompra = ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido))
                        .getText().toString().trim();
            }
        } else {
            Main.encabezado.codigo_cliente = cliente.codigo;
            Main.encabezado.observacion = ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).getText()
                    .toString().trim();
            Main.encabezado.observacion2 = ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido)).getText()
                    .toString().trim();
            Main.encabezado.ordenCompra = ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido)).getText()
                    .toString().trim();

            int posSucursal = spSucursal.getSelectedItemPosition();
            Main.encabezado.sucursal = listaSucursal.get(posSucursal).codigodir;


            String valor = lblPrecioFlete.getText().toString().trim();
            if (!valor.equals("")) {
                Float valorLimpio = Util.quitarComasYsignoPeso(valor);
                Main.encabezado.flete = valorLimpio;
            } else
                Main.encabezado.flete = 0f;
        }

        // Main.encabezado.codigo_cliente = tipoTransaccionActual == AUTOVENTA ?
        // usuario.codigoVendedor : cliente.codigo;xx

        Main.encabezado.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
        Main.encabezado.numero_doc = DataBaseBO.ObtenterNumeroDoc(usuario.codigoVendedor);

        if (tipoTransaccionActual == AUTOVENTA && tipoTransaccion > 0) {

            Main.encabezado.fechaEntrega = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
        } else {
            Main.encabezado.fechaEntrega = fechaEntrega.getText().toString();
        }

        if (cambio)
            Main.encabezado.motivo = 10;
        else
            Main.encabezado.motivo = 0;

        version = ObtenerVersion();
        imei = ObtenerImei();

        if (!Main.encabezado.fechaEntrega.equals("")) {

            String alerta = obtenerAlertaPromocion();

            if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {

                boolean existeConsecutivo = DataBaseBO.existeConsecutivoAutoventa();

                if (existeConsecutivo) {

                    if (Main.encabezado.tipoTrans == 0) {

                        ConsecutivoFactura consecutivo = DataBaseBO.obtenerConsecutivoFactura();

                        Main.encabezado.strFactura = consecutivo.StrConsecutivo;
                        Main.encabezado.idConsecutivo = consecutivo.codigoConsecutivo;
                        Main.encabezado.prefijoConsecutivo = consecutivo.prefijo;
                        Main.encabezado.nroConsecutivo = consecutivo.nroConsecutivo;

                    } else {

                        Main.encabezado.strFactura = "0";
                        Main.encabezado.idConsecutivo = "0";
                        Main.encabezado.prefijoConsecutivo = "0";
                        Main.encabezado.nroConsecutivo = 0;

                    }

                    if (alerta.equals("")) {

                        boolean finalizo = false;

                        if (esCargue || tipoTransaccion > 0) {
                            finalizo = DataBaseBO.RegistrarPedidoSugerido(Main.encabezado, Main.detallePedido, version,
                                    imei, false, "", tipoTransaccion, liquidador.codigoVendedor);
                        } else {

                            finalizo = DataBaseBO.RegistrarPedido(Main.encabezado, Main.detallePedido, version, imei,
                                    false, "");
                        }

                        AgregarProductoPedidoBono(Main.encabezado.numero_doc);

                        Coordenada coordenada = Coordenada.get(FormPedidoActivity.this);

                        if (finalizo) {


                            registrarVisita();

                            pedidoRegistrado = true;

                            String complemento = "";

                            if (coordenada != null)
                                DataBaseBO.guardarCoordenada(coordenada, Main.encabezado.numero_doc);

                            if (tipoTransaccionActual == AUTOVENTA) {

                                complemento = " guardo con Exito";
                            } else if (tipoTransaccionActual == AUTOVENTA && !isAveriaTransporte) {

                                complemento = " Registrado con Exito\n" + "Para el Cliente " + cliente.Nombre;
                            } else {
                                complemento = " Registrado con Exito\n" + "Para el Cliente " + cliente.Nombre;
                                //+ "<br> &#191;Desea imprimir una copia?Desea imprimir una copia?";
                            }

                            mostrarDialogPedidoRegistrado(strOpcion + complemento);

                        } else {

                            DataBaseBO.BorrarPedidoIncompleto(Main.encabezado.numero_doc);
                            MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
                        }

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage(alerta + " Desea Terminar el Pedido de Todas Formas?").setCancelable(false)
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();

                                        boolean finalizo = false;

                                        if (esCargue || tipoTransaccion > 0) {
                                            finalizo = DataBaseBO.RegistrarPedidoSugerido(Main.encabezado,
                                                    Main.detallePedido, version, imei, false, "", tipoTransaccion,
                                                    liquidador.codigoVendedor);
                                        } else {
                                            finalizo = DataBaseBO.RegistrarPedido(Main.encabezado, Main.detallePedido,
                                                    version, imei, false, "");
                                        }
                                        AgregarProductoPedidoBono(Main.encabezado.numero_doc);

                                        Coordenada coordenada = Coordenada.get(FormPedidoActivity.this);

                                        if (coordenada == null) {

                                            // El GPS esta ON y aun no
                                            // ha capturado coordenada.
                                            DataBaseBO.validarUsuario();

                                            if (Main.usuario != null && Main.usuario.codigoVendedor != null) {

                                                coordenada = new Coordenada();
                                                coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                                                coordenada.codigoCliente = Main.cliente.codigo;
                                                coordenada.latitud = 0;
                                                coordenada.longitud = 0;
                                                coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                                                coordenada.estado = Coordenada.ESTADO_GPS_SIN_RESPUESTA;
                                                coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                                                coordenada.fecha = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

                                            }
                                        }

                                        if (finalizo) {

                                            registrarVisita();

                                            pedidoRegistrado = true;

                                            DataBaseBO.guardarCoordenada(coordenada, Main.encabezado.numero_doc);

                                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                                    FormPedidoActivity.this);
                                            builder.setMessage(strOpcion + " Registrado con Exito\n"
                                                    + "Para el Cliente " + cliente.Nombre + "\n"
                                                    + "Desea Enviar la Informacion en este Momento?")
                                                    .setCancelable(false).setPositiveButton(R.string.accept,
                                                    new DialogInterface.OnClickListener() {

                                                        public void onClick(DialogInterface dialog, int id) {

                                                            if (Main.usuario.codigoVendedor == null)
                                                                DataBaseBO.CargarInfomacionUsuario();

                                                            DataBaseBO.ActualizaEstadoRutero(Main.cliente.codigo);

                                                            // progressDialog
                                                            // =
                                                            // ProgressDialog.show(FormPedidoActivity.this,
                                                            // "",
                                                            // "Enviando
                                                            // Informacion
                                                            // ...",
                                                            // true);
                                                            // progressDialog.show();

                                                            // DataBaseBO.ActualizarSnEnvio(Main.encabezado.id,
                                                            // 1);
                                                            Main.encabezado = new Encabezado();
                                                            Main.detallePedido.clear();

                                                            dialog.cancel();
                                                            onClickGuardarPedido.reset();

                                                            // Sync sync
                                                            // = new
                                                            // Sync(FormPedidoActivity.this,
                                                            // Const.ENVIAR_PEDIDO);
                                                            // sync.start();

                                                            // OpcionesPedidoActivity.this.setResult(RESULT_OK);
                                                            FormPedidoActivity.this.finish();
                                                        }
                                                    })

                                                    .setNegativeButton("Cancelar",
                                                            new DialogInterface.OnClickListener() {

                                                                public void onClick(DialogInterface dialog, int id) {

                                                                    if (Main.usuario.codigoVendedor == null)

                                                                        DataBaseBO.CargarInfomacionUsuario();
                                                                    dialog.cancel();
                                                                    onClickGuardarPedido.reset();
                                                                    Main.encabezado = new Encabezado();
                                                                    Main.detallePedido.clear();// OpcionesPedidoActivity.this.setResult(RESULT_OK);
                                                                    FormPedidoActivity.this.finish();

                                                                }
                                                            });

                                            AlertDialog alert = builder.create();
                                            alert.show();

                                            dialogResumen.cancel();

                                        } else {

                                            DataBaseBO.BorrarPedidoIncompleto(Main.encabezado.numero_doc);
                                            MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
                                        }
                                    }
                                })

                                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                        dialogResumen.cancel();

                                    }

                                });

                        AlertDialog alert = builder.create();
                        alert.show();
                    }

                } else {

                    Util.MostrarAlertDialog(this,
                            "No hay consecutivo para facturacion, consulte con su administrador.");
                    return;

                }

            } else {

                if (alerta.equals("")) {

                    boolean finalizo = false;

                    if (esCargue || isAveriaTransporte || isProductoDanado || isInventarioLiquidacion) {
                        finalizo = DataBaseBO.RegistrarPedidoSugerido(Main.encabezado, Main.detallePedido, version,
                                imei, false, "", tipoTransaccion, liquidador.codigoVendedor);
                    } else {

                        finalizo = DataBaseBO.RegistrarPedido(Main.encabezado, Main.detallePedido, version, imei, false,
                                "");
                    }

                    AgregarProductoPedidoBono(Main.encabezado.numero_doc);

                    Coordenada coordenada = Coordenada.get(FormPedidoActivity.this);

                    if (coordenada == null) {

                        // El GPS esta ON y aun no ha capturado coordenada.
                        DataBaseBO.validarUsuario();

                        // if (Main.usuario != null &&
                        // Main.usuario.codigoVendedor != null) {
                        //
                        // coordenada = new Coordenada();
                        // coordenada.codigoVendedor =
                        // Main.usuario.codigoVendedor;
                        // coordenada.codigoCliente = Main.cliente.codigo;
                        // coordenada.latitud = 0;
                        // coordenada.longitud = 0;
                        // coordenada.horaCoordenada =
                        // Util.FechaActual("HH:mm:ss");
                        // coordenada.estado =
                        // Coordenada.ESTADO_GPS_SIN_RESPUESTA;
                        // coordenada.id =
                        // Coordenada.obtenerId(Main.usuario.codigoVendedor);
                        // coordenada.fecha = Util.FechaActual("yyyy-MM-dd
                        // HH:mm:ss");
                        //
                        // }
                    }

                    if (finalizo) {

                        registrarVisita();

                        // DataBaseBO.guardarCoordenada(coordenada);

                        if (coordenada != null)
                            DataBaseBO.guardarCoordenada(coordenada, Main.encabezado.numero_doc);

                        String complemento = usuario.tipoVenta.equals(Const.AUTOVENTA)
                                ? " Registrado con Exito\n" + "Para el Cliente " + cliente.Nombre
                                : " Registrado con Exito <br>" + "Para el Cliente " + cliente.Nombre;
                        //+ "<br> &#191;Desea imprimir una copia?";

                        mostrarDialogPedidoRegistrado(strOpcion + complemento);

                    } else {

                        DataBaseBO.BorrarPedidoIncompleto(Main.encabezado.numero_doc);
                        MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
                    }

                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(alerta + " Desea Terminar el Pedido de Todas Formas?").setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();

                                    boolean finalizo = false;

                                    if (esCargue || isAveriaTransporte || isProductoDanado || isInventarioLiquidacion) {
                                        finalizo = DataBaseBO.RegistrarPedidoSugerido(Main.encabezado,
                                                Main.detallePedido, version, imei, false, "", tipoTransaccion,
                                                liquidador.codigoVendedor);
                                    } else {
                                        finalizo = DataBaseBO.RegistrarPedido(Main.encabezado, Main.detallePedido,
                                                version, imei, false, "");
                                    }
                                    AgregarProductoPedidoBono(Main.encabezado.numero_doc);

                                    Coordenada coordenada = Coordenada.get(FormPedidoActivity.this);

                                    if (coordenada == null) {

                                        // El GPS esta ON y aun no
                                        // ha capturado coordenada.
                                        DataBaseBO.validarUsuario();

                                        if (Main.usuario != null && Main.usuario.codigoVendedor != null) {

                                            coordenada = new Coordenada();
                                            coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                                            coordenada.codigoCliente = Main.cliente.codigo;
                                            coordenada.latitud = 0;
                                            coordenada.longitud = 0;
                                            coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                                            coordenada.estado = Coordenada.ESTADO_GPS_SIN_RESPUESTA;
                                            coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                                            coordenada.fecha = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

                                        }
                                    }

                                    if (finalizo) {

                                        registrarVisita();

                                        DataBaseBO.guardarCoordenada(coordenada, Main.encabezado.numero_doc);

                                        AlertDialog.Builder builder = new AlertDialog.Builder(FormPedidoActivity.this);
                                        builder.setMessage(strOpcion + " Registrado con Exito\n" + "Para el Cliente "
                                                + cliente.Nombre + "\n"
                                                + "Desea Enviar la Informacion en este Momento?").setCancelable(false)
                                                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface dialog, int id) {

                                                        if (Main.usuario.codigoVendedor == null)
                                                            DataBaseBO.CargarInfomacionUsuario();

                                                        DataBaseBO.ActualizaEstadoRutero(Main.cliente.codigo);

                                                        // progressDialog =
                                                        // ProgressDialog.show(FormPedidoActivity.this,
                                                        // "",
                                                        // "Enviando Informacion
                                                        // ...", true);
                                                        // progressDialog.show();

                                                        // DataBaseBO.ActualizarSnEnvio(Main.encabezado.id,
                                                        // 1);
                                                        Main.encabezado = new Encabezado();
                                                        Main.detallePedido.clear();

                                                        dialog.cancel();
                                                        onClickGuardarPedido.reset();

                                                        // Sync sync = new
                                                        // Sync(FormPedidoActivity.this,
                                                        // Const.ENVIAR_PEDIDO);
                                                        // sync.start();

                                                        // OpcionesPedidoActivity.this.setResult(RESULT_OK);
                                                        FormPedidoActivity.this.finish();
                                                    }
                                                })

                                                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface dialog, int id) {

                                                        if (Main.usuario.codigoVendedor == null)

                                                            DataBaseBO.CargarInfomacionUsuario();
                                                        dialog.cancel();
                                                        onClickGuardarPedido.reset();
                                                        Main.encabezado = new Encabezado();
                                                        Main.detallePedido.clear();// OpcionesPedidoActivity.this.setResult(RESULT_OK);
                                                        FormPedidoActivity.this.finish();

                                                    }
                                                });

                                        AlertDialog alert = builder.create();
                                        alert.show();

                                        dialogResumen.cancel();

                                    } else {

                                        DataBaseBO.BorrarPedidoIncompleto(Main.encabezado.numero_doc);
                                        MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
                                    }
                                }
                            })

                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                    dialogResumen.cancel();

                                }

                            });

                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }

        } else {

            MostrarAlertDialog("Por Favor Ingrese la Fecha de Entrega" + strOpcion);

        }

    }

    public void mostrarDialogPedidoRegistrado(String mensaje) {

        Usuario usuario = DataBaseBO.ObtenerUsuario();
        if (tipoTransaccion == 0)
            ((Button) dialogResumen.findViewById(R.id.btnAceptarResumenPedido)).setEnabled(true);

        if (dialogPedidoRegistrado == null) {

            dialogPedidoRegistrado = new Dialog(this);
            // dialogPedidoRegistrado.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialogPedidoRegistrado.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogPedidoRegistrado.setContentView(R.layout.dialog_pedido_registrado);

            // dialogPedidoRegistrado.setTitle(strOpcion);

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnAceptar)).
                    setOnClickListener(new OnClickListener() {

                        public void onClick(View view) {

                            /* Evitar evento de doble click */
                            if (SystemClock.elapsedRealtime() - mLastClickTimeDialog < 1000) {
                                return;
                            }
                            mLastClickTimeDialog = SystemClock.elapsedRealtime();

                            if (Main.usuario.codigoVendedor == null)
                                DataBaseBO.CargarInfomacionUsuario();

                            DataBaseBO.ActualizaEstadoRutero(Main.cliente.codigo);

                            Intent returnIntent = null;

                            if (isPedidoAnulado) {
                                returnIntent = new Intent(FormPedidoActivity.this, FormInfoClienteActivity.class);
                                returnIntent.putExtra("result", true);
                                returnIntent.putExtra("nroDocSesion", Main.encabezado.numero_doc);
                                returnIntent.putExtra("isAnulado", true);

                                Main.encabezado = new Encabezado();
                                Main.detallePedido.clear();

                                dialogPedidoRegistrado.cancel();
                                onClickGuardarPedido.reset();
                                setResult(Activity.RESULT_OK, returnIntent);
                                startActivityForResult(returnIntent, Const.RESP_PEDIDO_EXITOSO);
                                finish();
                            } else {

                                returnIntent = new Intent();
                                returnIntent.putExtra("result", true);
                                returnIntent.putExtra("nroDocSesion", Main.encabezado.numero_doc);
                                Main.encabezado = new Encabezado();
                                Main.detallePedido.clear();

                                dialogPedidoRegistrado.cancel();
                                onClickGuardarPedido.reset();

                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }

                        }
                    });

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setOnClickListener(new OnClickListener() {

                public void onClick(View view) {

                    boolean isPedido = true;
                    String copiaPrint = "";
                    if (primerEjecucion) {
                        primerEjecucion = false;
                        copiaPrint = "ORIGINAL";
                    } else {
                        copiaPrint = "COPIA";
                    }

                    progressDialog = ProgressDialog.show(FormPedidoActivity.this, "",
                            "Por Favor Espere...\n\nProcesando Informacion!", true);
                    progressDialog.show();

                    SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                    macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

                    SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                    String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");

                    if (macImpresora.equals("-")) {

                        Util.MostrarAlertDialog(FormPedidoActivity.this,
                                "");//"Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");
                        if (progressDialog != null)
                            progressDialog.cancel();

                    } else {

                        if (esCargue) {
                            if (!tipoImpresora.equals("Intermec")) {
                                sewooLKP20 = new SewooLKP20(FormPedidoActivity.this);
                                imprimirSewooLKP20(macImpresora, Main.encabezado.numero_doc, copiaPrint, isPedido,
                                        esCargue);

                            } else {

                                imprimirTirillaGeneral(macImpresora, Main.encabezado.numero_doc, copiaPrint, isPedido,
                                        esCargue);

                            }

                        } else {

                            if (!tipoImpresora.equals("Intermec")) {
                                sewooLKP20 = new SewooLKP20(FormPedidoActivity.this);
                                imprimirSewooLKP20(macImpresora, Main.encabezado.numero_doc, copiaPrint, isPedido,
                                        esCargue);
                            } else {
                                imprimirTirillaGeneral(macImpresora, Main.encabezado.numero_doc, copiaPrint, isPedido,
                                        esCargue);
                            }

                        }

                    }
                }
            });
        }

        SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
        String macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

        if (macImpresora.equals("-")) {

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.GONE);

            String msg = mensaje; /* + "<br /><br />"
					+ "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!";*/
            ((TextView) dialogPedidoRegistrado.findViewById(R.id.lblMsg)).setText(Html.fromHtml(msg));

        } else {

            if (!usuario.tipoVenta.equals(Const.AUTOVENTA))
                ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.VISIBLE);
            else {

                if (esCargue)
                    ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.VISIBLE);
                else
                    ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.GONE);
            }

            ((TextView) dialogPedidoRegistrado.findViewById(R.id.lblMsg)).setText(mensaje);
        }
        ((TextView) dialogPedidoRegistrado.findViewById(R.id.tvTituloDialog)).setText("Pedido");
        dialogPedidoRegistrado.setCancelable(false);
        dialogPedidoRegistrado.show();
    }

    /**
     * Imprimir la factura del pedido.
     *
     * @param macImpresora
     * @param numero_doc
     * @param copiaPrint
     */
    protected void imprimirSewooLKP20(final String macImpresora, final String numero_doc, final String copiaPrint,
                                      final boolean isPedido, final boolean isCargue) {

        final Usuario usuario = DataBaseBO.ObtenerUsuario();

        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();

                if (macImpresora.equals("-")) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(FormPedidoActivity.this,
                                    "", Toast.LENGTH_SHORT).show();//"Aun no hay Impresora Predeterminada.\n\nPor Favor primero Configure la Impresora!",

                        }
                    });
                } else {

                    if (sewooLKP20 == null) {
                        sewooLKP20 = new SewooLKP20(FormPedidoActivity.this);
                    }
                    int conect = sewooLKP20.conectarImpresora(macImpresora);

                    switch (conect) {
                        case 1:

                            sewooLKP20.generarEncabezadoTirilla(numero_doc, usuario, copiaPrint, isPedido, false, isCargue,
                                    0, false);
                            sewooLKP20.imprimirBuffer(true);

                            break;

                        case -2:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormPedidoActivity.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;

                        case -8:
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Util.MostrarAlertDialog(FormPedidoActivity.this,
                                            "Bluetooth apagado. Por favor habilite el bluetoth para imprimir.");
                                }
                            });
                            break;

                        default:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormPedidoActivity.this, "Error desconocido, intente nuevamente.",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                    }

                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (sewooLKP20 != null) {
                        sewooLKP20.desconectarImpresora();
                        if (progressDialog != null)
                            progressDialog.dismiss();
                    }
                }
                Looper.myLooper().quit();
            }
        }).start();
    }

    /**
     * @param copiaPrint
     */
    // protected void imprimirSewooLKP20(final String macImpresora, final String
    // numero_doc, final
    // String copiaPrint) {
    //
    // new Thread(new Runnable() {
    //
    // @Override
    // public void run() {
    // Looper.prepare();
    //
    // if (macImpresora.equals("-")) {
    // progressDialog.dismiss();
    // runOnUiThread(new Runnable() {
    //
    // @Override
    // public void run() {
    // Toast.makeText(FormPedidoActivity.this, "Aun no hay Impresora
    // Predeterminada.\n\nPor Favor
    // primero Configure la Impresora!",
    // Toast.LENGTH_SHORT).show();
    // }
    // });
    // } else {
    //
    // if (wR240 == null) {
    // wR240 = new WoosimR240(FormPedidosAutoventa.this);
    // }
    // int conect = wR240.conectarImpresora(macImpresora);
    //
    // switch (conect) {
    // case 1:
    // ControlImpresion control = new ControlImpresion();
    // /*
    // * lista que contendra los detalles que seran impresos
    // */
    // DetalleImprimir detalleImprimir = new DetalleImprimir();
    // ArrayList<DetalleProducto> listaDetalleProductos = new
    // ArrayList<DetalleProducto>();
    // String numeroFactura = DataBaseBO.cargarDetallesImprimir(numeroDoc,
    // detalleImprimir,
    // listaDetalleProductos, control);
    //
    // if (detalleImprimir != null && detalleImprimir.getEncabezado() != null &&
    // listaDetalleProductos != null
    // && !listaDetalleProductos.isEmpty()) {
    //
    // for (int i = 0; i < numCopias; i++) {
    // if (i > 0) {
    // control.original = false;
    // }
    // wR240.generarEncabezadoTirilla(numeroFactura, clienteEncabezado,
    // control.fechaVenta,
    // control.original);
    // wR240.generarDetalleTirilla(detalleImprimir);
    // int succes = wR240.imprimirBuffer(true);
    // if ((control.original) && succes == 1) {
    // DataBaseBO.marcarComoCopiaProximaImpresion(numeroDoc, usuario);
    // }
    // try {
    // Thread.sleep(2000);
    // } catch (InterruptedException e) {
    // e.printStackTrace();
    // }
    // }
    // } else {
    // progressDialog.dismiss();
    // runOnUiThread(new Runnable() {
    //
    // @Override
    // public void run() {
    // Toast.makeText(FormPedidosAutoventa.this, "Aun no hay datos para
    // imprimir, revise el
    // pedido.", Toast.LENGTH_SHORT).show();
    // }
    // });
    // }
    // break;
    //
    // case -2:
    // runOnUiThread(new Runnable() {
    //
    // @Override
    // public void run() {
    // Toast.makeText(FormPedidosAutoventa.this, "-2 fallo conexion",
    // Toast.LENGTH_SHORT).show();
    // }
    // });
    // break;
    //
    // case -8:
    // progressDialog.dismiss();
    // runOnUiThread(new Runnable() {
    //
    // @Override
    // public void run() {
    // Util.MostrarAlertDialog(FormPedidosAutoventa.this, "Bluetooth apagado.
    // Por favor habilite el
    // bluetoth para imprimir.");
    // }
    // });
    // break;
    //
    // default:
    // runOnUiThread(new Runnable() {
    //
    // @Override
    // public void run() {
    // Toast.makeText(FormPedidosAutoventa.this, "error desconocido",
    // Toast.LENGTH_SHORT).show();
    // }
    // });
    // break;
    // }
    //
    // try {
    // Thread.sleep(2500);
    // } catch (InterruptedException e) {
    // e.printStackTrace();
    // }
    //
    // if (wR240 != null) {
    // wR240.desconectarImpresora();
    // progressDialog.dismiss();
    // handlerFinish.sendEmptyMessage(0);
    // }
    // }
    // Looper.myLooper().quit();
    // }
    // }).start();
    //
    // }
    private void imprimirTirillaGeneral(final String macAddress, final String numeroDoc, final String copiaPrint,
                                        final boolean isPedido, final boolean isCargue) {

        new Thread(new Runnable() {

            public void run() {

                mensaje = "";
                BluetoothSocket socket = null;

                try {

                    Looper.prepare();

                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                    if (bluetoothAdapter == null) {

                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else if (!bluetoothAdapter.isEnabled()) {

                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else {

                        BluetoothDevice printer = null;

                        printer = bluetoothAdapter.getRemoteDevice(macAddress);

                        if (printer == null) {

                            mensaje = "No se pudo establecer la conexion con la Impresora.";

                        } else {

                            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

                            SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                            String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");

                            if (tipoImpresora.equals("Intermec")) {

                                socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);

                            } else {

                                socket = printer.createRfcommSocketToServiceRecord(uuid);

                            }

                            if (socket != null) {

                                socket.connect();

                                Thread.sleep(3500);

                                if (esCargue) {

                                    if (tipoImpresora.equals("Intermec")) {

                                        ReporstPrinter.ImprimiendoPrinter(socket,
                                                PrinterBO.formatoCargueImpresoraIntermec(numeroDoc));

                                    }
                                    // else if(tipoImpresora.equals("Sewoo -
                                    // LK-P20")){
                                    //
                                    // ReporstPrinter.ImprimiendoPrinter(socket,
                                    // PrinterBO.formatoCargueSewoo(numeroDoc));
                                    //
                                    // }

                                } else {

                                    if (tipoImpresora.equals("Intermec")) {

                                        ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO
                                                .formatoVentaPedidoItermerc(numeroDoc, copiaPrint, false, isPedido));

                                    }

                                }

                                handlerFinish.sendEmptyMessage(0);

                            } else {

                                mensaje = "No se pudo abrir la conexion con la Impresora.\n\nPor Favor intente de nuevo.";
                            }

                        }

                    }

                    if (!mensaje.equals("")) {

                        handlerFinish.sendEmptyMessage(0);
                    }

                    Looper.myLooper().quit();

                } catch (Exception e) {

                    String motivo = e.getMessage();

                    mensaje = "No se pudo ejecutar la Impresion.";

                    if (motivo != null) {
                        mensaje += "\n\n" + motivo;
                    }

                    handlerFinish.sendEmptyMessage(0);

                } finally {

                }
            }

        }).start();

    }

    private Handler handlerFinish = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (progressDialog != null)
                progressDialog.cancel();

            // progressDialog = ProgressDialog.show(FormPedidoActivity.this, "",
            // "Enviando
            // Informacion Pedido...", true);
            // progressDialog.show();
            //
            // Sync sync = new Sync(FormPedidoActivity.this,
            // Const.ENVIAR_PEDIDO);
            // sync.start();
        }
    };

    public void SetListenerListView() {

        ListView listaOpciones = (ListView) findViewById(R.id.listaFormPedido);
        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ListViewAdapterPedido adapter = (ListViewAdapterPedido) parent.getAdapter();
                ItemListView itemListView = adapter.listItems[position];

                String[] data = itemListView.titulo.split("-");
                String codigoProducto = data[0].trim();
                detalleEdicion = Main.detallePedido.get(codigoProducto);

                MostrarDialogEdicion();

            }
        });
    }

    public void SetKeyListener() {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        txtCodigoProducto.setOnKeyListener(new OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    ((ImageView) findViewById(R.id.btnAceptarOpcionesPedido)).requestFocus();
                    return true;
                }

                return false;
            }
        });
    }

    public void SetFocusListener() {

        EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        txtCodigoProducto.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {

                    ((EditText) findViewById(R.id.txtCodigoProducto)).setError(null);
                }
            }
        });
    }

    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

        final String mensaje = ok ? strOpcion + " Registrado con Exito en el servidor" : msg;

        if (progressDialog != null)
            progressDialog.cancel();

        this.runOnUiThread(new Runnable() {

            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(FormPedidoActivity.this);
                builder.setMessage(mensaje)

                        .setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Main.encabezado = new Encabezado();
                        Main.detallePedido.clear();

                        dialog.cancel();

                        // OpcionesPedidoActivity.this.setResult(RESULT_OK);
                        FormPedidoActivity.this.finish();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public void OcultarTeclado(EditText editText) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public String ObtenerVersion() {

        String version;

        try {

            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

        } catch (NameNotFoundException e) {

            version = "0.0";
            Log.e("OpcionesPedidoActivity", e.getMessage(), e);
        }

        return version;
    }

    public String ObtenerImei() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
    }

    public void MostrarAlertDialog(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
                onClickGuardarPedido.reset();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    public void AlertResetRegistrarProducto(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
                onClickRegistrarProducto.reset();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    private void setMsjPosNavegador() {

        int posAux = String.valueOf((posNavegadorProductos + 1)).length();
        msjPosNavegador = Util.replicate("0", 3 - posAux) + String.valueOf((posNavegadorProductos + 1));
        msjPosNavegador += getString(R.string._de_);

        posAux = String.valueOf(Main.navegadorProductos.size()).length();
        msjPosNavegador += Util.replicate("0", 3 - posAux) + String.valueOf(Main.navegadorProductos.size());
    }

    private OnClickListenerImpl onClickTerminar = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {
            // OcultarMensajeYTeclado();
            ValidarPedido();
        }
    };

    private OnClickListenerImpl onClickGuardarPedido = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            dialogResumen.cancel();
            onClickTerminar.reset();

            if (modificarPedido) {
                cargarNuevaInformacionParaModificar();
            } else {
                FinalizarPedido();
            }
        }
    };

    private OnClickListenerImpl onClickRegistrarProducto = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            RegistrarProducto();
        }
    };

    private OnClickListenerImpl onClickFechaEntrega = new OnClickListenerImpl() {

        @Override
        public void onOneClick(View view) {

            MostrarDialogFechaEntrega();
        }
    };

    // private void cambiarImagenDescAutorizado() {
    //
    // Button btnDesc;
    //
    // if (cambioImgDesc) {
    //
    // cambioImgDesc = false;
    //
    // btnDesc = (Button) findViewById(R.id.btnDescAutorizado);
    // btnDesc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.desc_mod, //
    // izquierda
    // 0, // arriba
    // 0, // derecha
    // 0); // abajo
    // } else {
    //
    // btnDesc = (Button) findViewById(R.id.btnDescAutorizado);
    // btnDesc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.desc_add, //
    // izquierda
    // 0, // arriba
    // 0, // derecha
    // 0); // abajo
    // }
    //
    // }

    public boolean MostrarDialogFechaEntrega() {

        DatePicker dpFechaEntrega = null;

        Calendar c = Calendar.getInstance();

        String[] days = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

        String dday = days[c.get(Calendar.DAY_OF_WEEK) - 1];

        if (dday.equals("SATURDAY")) {

            c.add(Calendar.DAY_OF_MONTH, 3);

        } else {

            c.add(Calendar.DAY_OF_MONTH, 2);
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Main.ano = year;
        Main.day = day;
        Main.mes = month + 1;

        try {

            if (dialogFechaEntrega == null) {

                dialogFechaEntrega = new Dialog(this);
                dialogFechaEntrega.setContentView(R.layout.dialog_fecha_entrega_pedido);
                dialogFechaEntrega.setTitle("Fecha Entrega");

                dpFechaEntrega = (DatePicker) dialogFechaEntrega.findViewById(R.id.dpFechaEntrega);

                // set current date into datepicker
                dpFechaEntrega.init(year, month, day, null);

                /**
                 * Se registran los eventos de los Botones del Formulario Dialog
                 * Resumen
                 */
                ((Button) dialogFechaEntrega.findViewById(R.id.btnAceptarFechaEntrega))
                        .setOnClickListener(new OnClickListener() {

                            public void onClick(View v) {

                                String fechaEntrega = getFechaEntrega();

                                if (fechaEsValida()) {

                                    ((TextView) dialogResumen.findViewById(R.id.lblFechaEntrega)).setText(fechaEntrega);
                                    dialogFechaEntrega.cancel();

                                } else {
                                    Util.MostrarAlertDialog(FormPedidoActivity.this, "La Fecha No Es Valida", 1);
                                }
                            }
                        });

            } else {

                dpFechaEntrega = (DatePicker) dialogFechaEntrega.findViewById(R.id.dpFechaEntrega);
                dpFechaEntrega.init(year, month, day, null);
            }

            dialogFechaEntrega.setCancelable(false);
            dialogFechaEntrega.show();

            return true;

        } catch (Exception e) {

            return false;
        }
    }

    protected void mostrarDetalleLiquidacion() {

        Intent formProductosLiq = new Intent(this, FormProductosLiquidacionActivity.class);
        formProductosLiq.putExtra("liquidador", liquidador);
        formProductosLiq.putExtra("liquidacionOpciones", tipoTransaccion);
        startActivityForResult(formProductosLiq, Const.RESP_LIQUID_EXITOSO);
    }

    private String getFechaEntrega() {

        int dia = 0, mes = 0, ano = 0;
        String diaTxt = "0", mesTxt = "0", anoTxt = "0", resultado = "";

        // Fechas
        DatePicker fechaPicker = (DatePicker) dialogFechaEntrega.findViewById(R.id.dpFechaEntrega);
        dia = fechaPicker.getDayOfMonth();
        mes = fechaPicker.getMonth() + 1;
        ano = fechaPicker.getYear();

        Main.ano1 = ano;
        Main.day1 = dia;
        Main.mes1 = mes;

        if (dia < 10)
            diaTxt = "0" + String.valueOf(dia);
        else
            diaTxt = String.valueOf(dia);

        if (mes < 10)
            mesTxt = "0" + String.valueOf(mes);
        else
            mesTxt = String.valueOf(mes);

        anoTxt = String.valueOf(ano);

        resultado = anoTxt + mesTxt + diaTxt;

        return resultado;
    }

    public void cargarCodigosProductos() {

        if (Main.navegadorProductos != null)
            if (Main.navegadorProductos.size() > 0) {

                codProductos = new String[Main.navegadorProductos.size()];

                for (int i = 0; i < codProductos.length; i++) {

                    Producto p = Main.navegadorProductos.get(i);
                    String codigo = p.codigo;
                    codProductos[i] = codigo;
                }

                AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.txtCodigoProducto);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        R.layout.auto_complete_text_view, R.id.autoCompleteItem, codProductos);
                textView.setAdapter(adapter);

                Point pointSize = new Point();//ampliar textview dentron del campo de texto

                getWindowManager().getDefaultDisplay().getSize(pointSize);

                textView.setDropDownWidth(pointSize.x);


                textView.setOnItemSelectedListener(new OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        // ... your stuff

                        String codigoBusqueda = ((AutoCompleteTextView) findViewById(R.id.autoCompleteItem)).getText()
                                .toString();

                        CargarProducto_(codigoBusqueda);
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // ... your stuff
                    }

                });

                textView.setOnItemClickListener(new OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // ... your stuff

                        CargarProducto_(
                                ((AutoCompleteTextView) findViewById(R.id.txtCodigoProducto)).getText().toString());

                        OcultarTeclado(((AutoCompleteTextView) findViewById(R.id.txtCodigoProducto)));

                        ((EditText) findViewById(R.id.txtCantidadProducto)).requestFocus();
                    }

                });

            }

    }

    public boolean fechaEsValida() {

        Date d1 = new Date(Main.ano, Main.mes, Main.day);

        Date d2 = new Date(Main.ano1, Main.mes1, Main.day1);

        if (d2.after(d1) || d2.toString().equals(d1.toString())) {

            return true;

        } else {

            return false;

        }

    }

    public void cargarInformacionBonificaciones() {

        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

        if (tipoDeClienteSeleccionado == 1) {

            cliente = DataBaseBO.CargarClienteSeleccionado();

        } else {

            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();

        }

        vendedorBonificacion = DataBaseBO.ExisteBonificacionVendedor();

        System.out.println("IS TRUE OR FALSE\nAveria: " + isAveriaTransporte + "\nInventario: "
                + isInventarioLiquidacion + "\nProducto Dañado: " + isProductoDanado);
        if (!isAveriaTransporte && !isInventarioLiquidacion && !isProductoDanado && !esCargue)
            topeBonificacion = DataBaseBO.aplicaBonificacionVendedor(cliente.tipologia);

        double tope = 0;

        if (vendedorBonificacion != null)
            if (topeBonificacion != null) {
                tope = topeBonificacion.tope;
            }
        if (tope > 0) {
            Main.sticker = 1;
        } else {

            Main.sticker = 0;

        }

    }

    public boolean aplicarDevolucion() {

        Enumeration<Detalle> e = Main.detallePedido.elements();

        float subTotal = 0;
        float valorDesc = 0;
        float valorIva = 0;
        float valorNeto = 0;

        Hashtable<String, String> htTipos = new Hashtable<String, String>();

        while (e.hasMoreElements()) {

            Detalle detalle = e.nextElement();

            float sub_total = detalle.cantidad * detalle.precio;
            float valor_descuento = sub_total * 0 / 100;
            float valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100);

            subTotal += sub_total;
            valorIva += valor_iva;
            valorDesc += valor_descuento;

            htTipos.put(detalle.tipoGrupo, detalle.codProducto);

        }

        if (topeBonificacion != null) {

            if (acomuladoClienteBonificaciones + subTotal >= topeBonificacion.tope && htTipos.size() >= 3) {

                return true;

            } else {

                return false;

            }

        } else {

            return false;

        }

    }

    public void aplicarRegalo() {

        if (Main.usuario == null)
            Main.usuario = DataBaseBO.CargarUsuario();

        CondicionesBonificacion condicionesBonificaciones = DataBaseBO.condicionesBonificaciones(
                Main.usuario.codigoVendedor, Main.encabezado.codigo_cliente, cliente.Canal, Main.usuario.bodega);

        if (cliente == null) {

            tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

            if (tipoDeClienteSeleccionado == 1) {

                cliente = DataBaseBO.CargarClienteSeleccionado();

            } else {

                cliente = DataBaseBO.CargarClienteNuevoSeleccionado();

            }

        }

        if (condicionesBonificaciones != null) {

            Detalle detalle = new Detalle();
            detalle.codCliente = cliente.codigo;
            detalle.codProducto = condicionesBonificaciones.codigo;
            detalle.nombProducto = "";
            detalle.precio = 0;
            detalle.iva = 0;
            detalle.descuento = 0;
            detalle.cantidad = 1;
            detalle.tipo_pedido = Const.PEDIDO_VENTA;
            detalle.codMotivo = "1";
            detalle.inventario = 0;
            detalle.indice = 0;

            detalle.codGrupo = "";
            detalle.tipoGrupo = "";

            Main.detallePedido.put("r" + condicionesBonificaciones.codigo, detalle);

        }

    }

    public String obtenerAlertaPromocion() {

        String alerta = "";

        Enumeration<Detalle> e = Main.detallePedido.elements();

        float subTotal = 0;
        float valorDesc = 0;
        float valorIva = 0;
        float valorNeto = 0;

        Hashtable<String, String> htTipos = new Hashtable<String, String>();

        while (e.hasMoreElements()) {

            Detalle detalle = e.nextElement();

            float sub_total = detalle.cantidad * detalle.precio;
            float valor_descuento = sub_total * 0 / 100;
            float valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100);

            subTotal += sub_total;
            valorIva += valor_iva;
            valorDesc += valor_descuento;

            htTipos.put(detalle.tipoGrupo, detalle.codProducto);

        }

        if (topeBonificacion != null) {

            if (htTipos.size() < 3) {

                if (acomuladoClienteBonificaciones + subTotal < topeBonificacion.tope) {

                    return "Para poder Agregar El Regalo necesita adicionar  " + String.valueOf(3 - htTipos.size())
                            + "  Categoria Adicionales al Pedido y el pedido superar el valor de $"
                            + topeBonificacion.tope;

                } else {

                    return "Para poder Agregar El Regalo necesita Adicionar " + String.valueOf(3 - htTipos.size())
                            + "  Categorias Adicionales al Pedido Actual";

                }

                // ojo otra decision

            } else {

                if (acomuladoClienteBonificaciones + subTotal < topeBonificacion.tope) {

                    return "Para poder Agregar el Regalo necesita que el pedido sea mayor a $" + topeBonificacion.tope;

                } else {

                    return "";

                }

            }

        } else {

            return "";

        }

    }

    @Override
    protected void onResume() {

        super.onResume();

        // Main.usuario = null;
        // Main.cliente = null;

        if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {

            DataBaseBO.CargarInfomacionUsuario();

        }

        if (Main.cliente == null || Main.cliente.codigo == null) {

            int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;

            if (tipoDeClienteSelec == 1) {

                clienteSel = DataBaseBO.CargarClienteSeleccionado();

            } else {

                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();

            }

            if (clienteSel != null)
                Main.cliente = clienteSel;

        }
        CargarListaPedido();
    }

    /**
     * cargar los detalles del pedido que se pretenden modificar.
     */
    private void cargarListaDetallesPedidos(Encabezado encabezado) {
        Main.detallePedido = new Hashtable<String, Detalle>();
        ArrayList<Detalle> listaDetalles = DataBaseBO.cargarDetallesDePedido(encabezado.numero_doc, isPedidoAnulado);

        for (Detalle detalle : listaDetalles) {
            // se debe capturar el indice el producto. buscar en el vector
            // Main.navegadorProductos el producto por codigo tal, y obtener su
            // indice para asignarlo al detalle.
            for (Producto p : Main.navegadorProductos) {
                if (p.codigo.equals(detalle.codProducto)) {
                    detalle.indice = p.indice;
                    break;
                }
            }
            Main.detallePedido.put(detalle.codProducto, detalle);
        }
    }

    /**
     * cargar los cambios que pueden ser modificados del pedido.
     */
    protected void cargarNuevaInformacionParaModificar() {

        /* Evitar evento de doble click */
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        Usuario usuario = DataBaseBO.CargarUsuario();

        if (usuario == null) {

            Util.MostrarAlertDialog(this, "No se pudo cargar la informacion del Usuario");

        } else {

            tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

            if (tipoDeClienteSeleccionado == 1) {

                cliente = DataBaseBO.CargarClienteSeleccionado();

            } else {

                cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
            }

            if (Main.usuario.codigoVendedor == null)
                DataBaseBO.CargarInfomacionUsuario();

            Main.encabezado.observacion = ((EditText) dialogResumen.findViewById(R.id.txtObservacionPedido)).getText()
                    .toString().trim();
            Main.encabezado.observacion2 = ((EditText) dialogResumen.findViewById(R.id.txtObservacion2Pedido)).getText()
                    .toString().trim();
            Main.encabezado.ordenCompra = ((EditText) dialogResumen.findViewById(R.id.txtOrdenCompraPedido)).getText()
                    .toString().trim();
            Main.encabezado.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); // Hora
            // en
            // finaliza
            // la
            // Toma
            // del
            // Pedido

            String fechaSinFormato = ((TextView) dialogResumen.findViewById(R.id.lblFechaEntrega)).getText().toString();
            String fechaString = fechaSinFormato.substring(0, 4) + "-" + fechaSinFormato.substring(4, 6) + "-"
                    + fechaSinFormato.substring(6, 8) + " 12:00:00.000";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date fechaDate = null;
            try {
                fechaDate = sdf.parse(fechaString);
                Main.encabezado.fechaEntrega = sdf.format(fechaDate);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Main.encabezado.motivo = 0;

            if (!Main.encabezado.fechaEntrega.equals("")) {
                modificarPedido();
            } else {
                MostrarAlertDialog("Por Favor Ingrese la Fecha de Entrega del pedido");
            }
        }
    }

    /**
     * actualizar el detalle y el encabezado con los nuevos datos.
     */
    protected void modificarPedido() {

        Usuario usuario = DataBaseBO.CargarUsuario();

        if (usuario == null) {

            Util.MostrarAlertDialog(this, "No se pudo cargar la informacion del Usuario");

        } else {

            tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

            if (tipoDeClienteSeleccionado == 1) {

                cliente = DataBaseBO.CargarClienteSeleccionado();
            } else {

                cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
            }

            if (Main.usuario.codigoVendedor == null)
                DataBaseBO.CargarInfomacionUsuario();

            encabezadoModificar.hora_final = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); // Hora
            // en
            // Pedido
            String alerta = obtenerAlertaPromocion();

            if (alerta.equals("")) {

                // actualizar nuevos datos modificados.
                encabezadoModificar.sub_total = Main.encabezado.sub_total;
                encabezadoModificar.total_iva = Main.encabezado.total_iva;
                encabezadoModificar.valor_descuento = Main.encabezado.valor_descuento;
                encabezadoModificar.valor_neto = Main.encabezado.valor_neto;
                encabezadoModificar.observacion = Main.encabezado.observacion;
                encabezadoModificar.observacion2 = Main.encabezado.observacion2;
                encabezadoModificar.fechaEntrega = Main.encabezado.fechaEntrega;
                encabezadoModificar.hora_final = Main.encabezado.hora_final;
                encabezadoModificar.ordenCompra = Main.encabezado.ordenCompra;

                boolean finalizo = DataBaseBO.actualizarPedido(encabezadoModificar, Main.detallePedido);

                Coordenada coordenada = Coordenada.get(FormPedidoActivity.this);

                if (coordenada == null) {

                    // El GPS esta ON y aun no ha capturado coordenada.
                    DataBaseBO.validarUsuario();

                    if (Main.usuario != null && Main.usuario.codigoVendedor != null) {

                        coordenada = new Coordenada();
                        coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                        coordenada.codigoCliente = Main.cliente.codigo;
                        coordenada.latitud = 0;
                        coordenada.longitud = 0;
                        coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                        coordenada.estado = Coordenada.ESTADO_GPS_SIN_RESPUESTA;
                        coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                        coordenada.fecha = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
                    }
                }

                if (finalizo) {

                    DataBaseBO.guardarCoordenada(coordenada, Main.encabezado.numero_doc);


                    registrarVisita();

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Se ha modificado el pedido correctamente").setCancelable(false)
                            .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {

                                    if (Main.usuario.codigoVendedor == null)
                                        DataBaseBO.CargarInfomacionUsuario();

                                    dialog.cancel();
                                    onClickGuardarPedido.reset();
                                    Main.encabezado = new Encabezado();
                                    Main.detallePedido.clear();
                                    if (dialogResumen != null) {
                                        dialogResumen.dismiss();
                                    }
                                    FormPedidoActivity.this.finish();
                                }
                            });

                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
                }
            }
        }
    }

    public Date diaActualMasUno() {

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("DIA:" + date.getDay() + "\nDATE: " + date.getDate());

        int dateActual = date.getDate();

        if (date.getDay() == 6) {

            dateActual = dateActual + 2;

        } else {
            dateActual = dateActual + 1;
        }

        date.setDate(dateActual);

        return date;
    }

    public Date diaActua() {

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("DIA:" + date.getDay() + "\nDATE: " + date.getDate());

        int dateActual = date.getDate();

        if (date.getDay() == 6) {

            dateActual = dateActual + 2;

        } else {
            dateActual = dateActual;
        }

        date.setDate(dateActual);

        return date;
    }


    private class CustomTextWatcher implements TextWatcher {

        private EditText mEditText;

        public CustomTextWatcher(EditText e) {
            this.mEditText = e;
        }

        public synchronized void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public synchronized void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public synchronized void afterTextChanged(Editable s) {

            if (mEditText != null) {

                mEditText.removeTextChangedListener(this);

                if (mEditText.getId() == R.id.lblFechaEntrega) {

                    String cadenaFecha = mEditText.getText().toString().trim();
                    Date dateFecha = Util.converFecha(cadenaFecha);

                    String fechaActual = Util.FechaActual("yyyy-MM-dd");
                    Date dateFechaActual = Util.converFecha(fechaActual);

                    long diff = dateFecha.getTime() - dateFechaActual.getTime();

                    if (diff < 0) {

                        Util.MostrarAlertDialog(FormPedidoActivity.this,
                                "La fecha de entrega debe ser a partir del dia de hoy!.");
                        Date date = diaActua();

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                        fechaEntrega.setText(sdf.format(date));

                    }
                }

                mEditText.addTextChangedListener(this);
            }
        }
    }

    public void onClickFiltros(View view) {

        fueUlPedido = false;
        Intent intent = new Intent(this, FormFiltroPedidoActivity.class);
        int tipo = 0;

        if (tipoTransaccion > 0)
            tipo = tipoTransaccion;

        intent.putExtra("tipoTransaccion", tipo);
        intent.putExtra("isCargue", esCargue);
        intent.putExtra("isPedido", isPedido);
        startActivityForResult(intent, Const.RESP_FILTRO_PRODUCTO);

    }

    public void onClickBonificacion(View view) {

        Intent intent = new Intent(this, FormBonificaciones.class);
        startActivity(intent);

    }

    public void onClickCapturarCodigoQR(View view) {

        // Usuario user = DataBaseBO.ObtenerUsuario();

        // if(user.tipoVenta.equals(Const.AUTOVENTA)){
        // mostrarDialogoEscanearCodigoBarras();
        // }else{

        Intent intentScan = new Intent(BS_PACKAGE + ".SCAN");
        intentScan.putExtra("PROMPT_MESSAGE", getString(R.string.enfoque_qr));
        String targetAppPackage = findTargetAppPackage(intentScan);
        if (targetAppPackage == null) {
            showDownloadDialog();
        } else {
            startActivityForResult(intentScan, Const.RESP_CODIGOBARRAS);

        }
        // }

    }

    private String findTargetAppPackage(Intent intent) {

        PackageManager pm = this.getPackageManager();
        List<ResolveInfo> availableApps = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (availableApps != null) {
            for (ResolveInfo availableApp : availableApps) {
                String packageName = availableApp.activityInfo.packageName;
                if (BS_PACKAGE.contains(packageName)) {
                    return packageName;
                }
            }
        }
        return null;
    }

    private AlertDialog showDownloadDialog() {
        final String DEFAULT_TITLE = getString(R.string.instalar_barcode_scanner_);
        final String DEFAULT_MESSAGE = getString(R.string.app_necesita_barcode_);
        final String DEFAULT_YES = getString(R.string.yes);
        ;
        final String DEFAULT_NO = getString(R.string.no);
        ;

        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
        downloadDialog.setTitle(DEFAULT_TITLE);
        downloadDialog.setMessage(DEFAULT_MESSAGE);
        downloadDialog.setPositiveButton(DEFAULT_YES, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://details?id=" + BS_PACKAGE);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    FormPedidoActivity.this.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {
                    // Hmm, market is not installed
                    Toast.makeText(FormPedidoActivity.this,
                            R.string.play_store_no_esta_instalado, Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
        downloadDialog.setNegativeButton(DEFAULT_NO, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    public void mostrarDialogoEscanearCodigoBarras() {

        if (dialogEscanear != null) {
            if (dialogEscanear.isShowing())
                dialogEscanear.dismiss();
            dialogEscanear.cancel();
        }

        dialogEscanear = new Dialog(this);
        dialogEscanear.setContentView(R.layout.escanear_codigo_de_barras2);
        dialogEscanear.setTitle("Lector");

        etCodigoEAN = (EditText) dialogEscanear.findViewById(R.id.etCodigoEAN);
        etCodigoEAN.setText("");
        etCodigoEAN.requestFocus();
        etCodigoEAN.addTextChangedListener(textWatcher);

        Button dialogButton = (Button) dialogEscanear.findViewById(R.id.btnSalir);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogEscanear.dismiss();
            }
        });

        dialogEscanear.show();

    }

    TextWatcher textWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {

            if (s.toString().length() >= 8 || s.toString().length() > 15) {

                //enter();

            }

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    public void enter() {

        String codProducto = DataBaseBO.ExisteCodigoEnProductos(txtCodProducto.getText().toString());

        if (!codProducto.equals("")) {

            CargarProducto(codProducto);
            txtCodProducto.setText(codProducto);

            ((EditText) findViewById(R.id.txtCantidadProducto)).requestFocus();

        } else {
            Util.mostrarToast(this, getString(R.string.no_existe_un_producto_ean));

            txtCodProducto.setText("");

            /*
             * if(dialogEscanear!=null){ if(dialogEscanear.isShowing())
             * dialogEscanear.dismiss(); dialogEscanear.cancel(); }
             */

        }

    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    protected boolean CalcularBonificacion() {

        boolean successful = false;

        // CONSULTA 1
        DataBaseBO.BorraPedidoCero();

        int cantidadPedida = 0;
        int cantidad = 0;
        int cantidadRegalo = 0;
        String codigoRegalo = "";
        int cantidadInventario = 0;
        String codigoBono = "";
        String descriRegalo = "";
        String tipoBonoSel = "";
        String[] listaTipoBonos = {"E", "A", "Z", "P"};
        String codigoBonoTem = "";

        int size = listaBonificaciones.size();

        for (int j = 0; j < size; j++) {

            Bonificaciones bonificaciones = listaBonificaciones.elementAt(j);

            codigoRegalo = bonificaciones.codigoRegalo;
            descriRegalo = bonificaciones.descripcionRegalo;
            codigoBono = bonificaciones.codigoBono;
            listaCodigoBono = DataBaseBO.ObtenerTipoBonificacion(codigoBono);

            int sizeBono = listaCodigoBono.size();
            int sizeTipo = listaTipoBonos.length;

            for (int jBono = 0; jBono < sizeBono; jBono++) {

                Bonificaciones bono = listaCodigoBono.elementAt(jBono);

                String tipoBonos = bono.tipoBono;

                for (int jtipo = 0; jtipo < sizeTipo; jtipo++) {
                    tipoBonoSel = listaTipoBonos[jtipo];

                    if (tipoBonoSel.equals(tipoBonos)) {

                        listaDetalleBono = DataBaseBO.ObtenerCantidadPedida(codigoBono, tipoBonos);

                        Detalle detalleBono = listaDetalleBono.elementAt(0);

                        cantidadPedida = detalleBono.cantidadBonos;
                        codigoBonoTem = detalleBono.codigoBonos;

                        cantidad = DataBaseBO.ObtenerCantidad(codigoBono, tipoBonos, cantidadPedida, codigoBonoTem);

                        if (cantidad > 0) {
                            cantidadRegalo = DataBaseBO.ObtenerCantidadRegalo(tipoBonos, codigoRegalo);
                            cantidadInventario = DataBaseBO.ObtenerCantidadInventario(tipoBonos, codigoRegalo);

                            if (cantidad <= cantidadInventario) {
                                // INSERT BONO TEMPORAL
                                DataBaseBO.RegistrarBonoTemp(tipoBonos, codigoRegalo, descriRegalo, cantidad,
                                        codigoBono);
                                successful = true;

                            } else if (cantidadInventario > 0) {
                                // INSERT BONO TEMPORAL
                                DataBaseBO.RegistrarBonoTemp(tipoBonos, codigoRegalo, descriRegalo, cantidadInventario,
                                        codigoBono);
                                successful = true;
                            }
                        }
                    }
                }
            }
        }

        return successful;

    }

    /************************************
     * MODULO DE BONIFICACIONES JAUM
     ************************************/
    private void AgregarProductoPedidoBono(String numeroServicio) {
        listaDetalle = DataBaseBO.ObtenerDetalleBono2();
        DataBaseBO.RegistrarPedidoDetalle(this.listaDetalle, version, imei, numeroServicio);
    }

    public void BuscarProductosBonoTemp() {

        ItemListView[] items;
        Vector<ItemListView> listaItems = new Vector<ItemListView>();

        listaDetalleBono = DataBaseBO.ObtenerDetalleBono(listaItems);

        if (listaItems.size() > 0) {

            items = new ItemListView[listaItems.size()];
            listaItems.copyInto(items);

        } else {

            items = new ItemListView[]{};

            if (listaDetalleBono != null)
                listaDetalleBono.removeAllElements();

            Toast.makeText(getApplicationContext(), getString(R.string.txt_busqueda_sin_resultados), Toast.LENGTH_SHORT).show();
        }

        final ListViewAdapter adapter = new ListViewAdapter(FormPedidoActivity.this, items, 0, 0);
        final ListView ListViewAdapter = (ListView) dialogProductosBono.findViewById(R.id.ltsDetalleBono);

        ListViewAdapter.setAdapter(adapter);
        ListViewAdapter.post(new Runnable() {

            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                ListViewAdapter.setSelection(adapter.getCount() - 1);
            }
        });

    }

    private Handler handlerFinishRegistro = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (progressDialog != null)
                progressDialog.cancel();

            if (registro) {
                mostrarDetalleLiquidacion();
            } else {
                Util.MostrarAlertDialog(FormPedidoActivity.this, getString(R.string.problema_al_registrar_la_liquidacion_));
            }

        }
    };

    private Handler handlerMensajeRegistro = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (progressDialog != null) {
                progressDialog.cancel();
            }

            // if (context != null) {
            //
            // AlertDialog.Builder builder = new AlertDialog.Builder(context);
            // builder.setCancelable(false).setPositiveButton("Aceptar", new
            // DialogInterface.OnClickListener() {
            //
            // public void onClick(DialogInterface dialog, int id) {
            //
            // dialog.cancel();
            // }
            // });
            //
            // AlertDialog alertDialog = builder.create();
            // alertDialog.setMessage(mensaje);
            // alertDialog.show();
            // }
        }
    };

    TextWatcher textWatcherCantidad = new TextWatcher() {

        public void afterTextChanged(Editable s) {

            if (s.toString().length() >= 8 || s.toString().length() > 15) {

                ((EditText) findViewById(R.id.txtCantidadProducto)).setText("");

            }

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher textWatcherCantidadDev = new TextWatcher() {

        public void afterTextChanged(Editable s) {

            if (s.toString().length() >= 8 || s.toString().length() > 15) {

                ((EditText) findViewById(R.id.txtCantidadProductoDevolucion)).setText("");

            }

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    public void continuarTransaccion() {

        if (tipoTransaccionActual) {

            if (tipoTransaccion == Const.IS_PRODUCTO_DANADO || tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION) {

                if (Main.detallePedido.size() > 0) {

                    boolean existeConsecutivo = DataBaseBO.existeConsecutivoAutoventa();
                    boolean existenTiposClientesValidacion = DataBaseBO.existeTipoClienteLiquidacion();

                    if (!existeConsecutivo) {
                        Util.mostrarToast(this, getString(R.string.sin_consecutivo_liquidacion));
                        return;

                    } else if (!existenTiposClientesValidacion) {

                        Util.mostrarToast(this,
                                getString(R.string.faltan_clientes_faltantes_sobrantes));
                        return;

                    } else {

                        eliminarTemporal();

                        progressDialog = ProgressDialog.show(FormPedidoActivity.this, "", getString(R.string.registrando_informacion_),
                                true);
                        progressDialog.show();

                        new Thread() {

                            public void run() {

                                registro = DataBaseBO.RegistrarLiquidacionComparacion(Main.detallePedido,
                                        tipoTransaccion);

                                if (registro) {
                                    handlerFinishRegistro.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                } else {

                    if (tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage(getString(R.string.el_) + strOpcion + getString(R.string._esta_vacio_))
                                .setCancelable(false)
                                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();

                    } else if (tipoTransaccion == Const.IS_PRODUCTO_DANADO) {

                        boolean existeConsecutivo = DataBaseBO.existeConsecutivoAutoventa();
                        boolean existenTiposClientesValidacion = DataBaseBO.existeTipoClienteLiquidacion();

                        if (!existeConsecutivo) {
                            Util.mostrarToast(this, getString(R.string.sin_consecutivo_liquidacion));
                            return;

                        } else if (!existenTiposClientesValidacion) {

                            Util.mostrarToast(this,
                                    getString(R.string.faltan_clientes_faltantes_sobrantes));
                            return;

                        } else {

                            eliminarTemporal();

                            progressDialog = ProgressDialog.show(FormPedidoActivity.this, "",
                                    getString(R.string.registrando_informacion_), true);
                            progressDialog.show();

                            new Thread() {

                                public void run() {

                                    registro = DataBaseBO.RegistrarLiquidacionComparacion(Main.detallePedido,
                                            tipoTransaccion);

                                    if (registro) {
                                        handlerFinishRegistro.sendEmptyMessage(0);
                                    }
                                }
                            }.start();
                        }

                    }

                }

            } else if (tipoTransaccion == Const.IS_AVERIA_TRANSPORTE) {

                long pedidoMinimo = DataBaseBO.getPedidoMinimo();

                if (Main.usuario.codigoVendedor == null) {
                    DataBaseBO.CargarInfomacionUsuario();
                }

                if (pedidoMinimo > (long) Main.encabezado.sub_total && !cambio) {

                    AlertResetRegistrarProducto(
                            getString(R.string.valor_minimo_permitido_) + pedidoMinimo + " )");
                } else {
                    MostrarDialogResumen();
                }

            } else if (isPedido) {

                /************************************
                 * MODULO DE BONIFICACIONES JAUM
                 ************************************/

                // CONSULTA 2
                listaBonificaciones = DataBaseBO.CalcularBonificacion(Main.usuario.codigoVendedor, Main.cliente.codigo,
                        Main.cliente.Canal, Main.usuario.bodega, Main.cliente.SubCanal);

                if (listaBonificaciones.size() > 0) {

                    progressDialog = ProgressDialog.show(FormPedidoActivity.this, "", getString(R.string.cargando_informacion_), true);
                    progressDialog.show();

                    new Thread() {

                        public void run() {

                            cargarPromocionCliente();
                        }
                    }.start();

                } else {

                    EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
                    txtCodigoProducto.setError(null);

                    OcultarTeclado(txtCodigoProducto);
                    ValidarPedido();
                }

            } else {

                EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
                txtCodigoProducto.setError(null);

                OcultarTeclado(txtCodigoProducto);
                ValidarPedido();
            }

        } else {

            EditText txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
            txtCodigoProducto.setError(null);

            OcultarTeclado(txtCodigoProducto);
            ValidarPedido();

        }

    }

    private void AgregarProductoPedido2(int cantidad, float descuento, String codMotivo) {

        Detalle detalle = Main.detallePedido.get(producto.codigo);
        String precioProducto = "";

        if (detalle == null) {

            if (producto != null) {

                tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

                if (tipoDeClienteSeleccionado == 1) {

                    cliente = DataBaseBO.CargarClienteSeleccionado();

                } else {

                    cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
                }

                if (tipoTransaccionActual == AUTOVENTA) {

                    if (tipoTransaccion > 0 || esCargue) {

                        cliente = new Cliente();
                        cliente.codigo = "0";
                    }

                }

                detalle = new Detalle();
                detalle.codCliente = cliente.codigo; // tipoTransaccionActual ==
                // AUTOVENTA &&
                // isAveriaTransporte ?
                // "0" : cliente.codigo;
                detalle.codProducto = producto.codigo;
                detalle.nombProducto = producto.descripcion;
                detalle.precio = producto.precio;
                detalle.iva = producto.iva;
                detalle.descuento = descuento;
                detalle.cantidad = cantidad;
                detalle.tipo_pedido = Const.PEDIDO_VENTA;
                detalle.codMotivo = codMotivo;
                detalle.inventario = producto.inventario;
                detalle.indice = producto.indice;

                detalle.codGrupo = producto.grupo;
                // detalle.tipoGrupo = DataBaseBO.tipoGrupo(producto.grupo);

                if (cambio) {

                    detalle.codMotivoCambio = motivoCambioSel.codigo;
                    detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                    detalle.codMotivo = detalle.codMotivoCambio;
                }
            } else {

                AlertResetRegistrarProducto(getString(R.string.no_se_pudo_leer_la_informacion_del_producto));
            }

            int posicion = Main.detallePedido.size();
            detalle.posicion = posicion;
            Main.detallePedido.put(producto.codigo, detalle);

            EditText txtCantidadProc = (EditText) findViewById(R.id.txtCantidadProducto);
            txtCantidadProc.setText("");
            OcultarTeclado(txtCantidadProc);

            CargarListaPedido();

        } else {

            this.cantidadGlobal = cantidad;
            this.descuentoGlobal = descuento;
            this.codMotivoGlobal = codMotivo;

            mostrarMensajeAdicionarCantidad();

        }

    }

    public void mostrarMensajeAdicionarCantidad() {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)

                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Detalle detalle = Main.detallePedido.get(producto.codigo);
                        detalle.cantidad = detalle.cantidad + FormPedidoActivity.this.cantidadGlobal;
                        detalle.descuento = descuentoGlobal;
                        int posicion = Main.detallePedido.size();
                        detalle.posicion = posicion;
                        Main.detallePedido.put(producto.codigo, detalle);
                        EditText txtCantidadProc = (EditText) findViewById(R.id.txtCantidadProducto);
                        txtCantidadProc.setText("");
                        OcultarTeclado(txtCantidadProc);
                        CargarListaPedido();
                        dialog.cancel();

                    }
                })

                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Detalle detalle = Main.detallePedido.get(producto.codigo);
                        detalle.cantidad = FormPedidoActivity.this.cantidadGlobal;
                        detalle.descuento = FormPedidoActivity.this.descuentoGlobal;
                        int posicion = Main.detallePedido.size();
                        detalle.posicion = posicion;
                        Main.detallePedido.put(producto.codigo, detalle);
                        EditText txtCantidadProc = (EditText) findViewById(R.id.txtCantidadProducto);
                        txtCantidadProc.setText("");
                        OcultarTeclado(txtCantidadProc);
                        CargarListaPedido();
                        dialog.cancel();
                    }
                });

        alertDialog = builder.create();
        alertDialog.setMessage(getString(R.string.desea_adicionar_) + cantidadGlobal + " " + getString(R.string.a_la_cantidad_actual));
        alertDialog.show();

    }

    public void OnClickRegresar(View view) {

        finish();
    }


    public boolean registrarVisita() {

        Usuario usuario = DataBaseBO.obtenerUsuario();
        String numDocRegVisita = "";
        int rutero = -1;
        boolean estado = false;
        boolean wifi = Util.isWifiOn(this);
        boolean mobnet = Util.isMobileNetworkOn(this);
        if (!wifi && !mobnet) {
            Util.MostrarAlertDialog(this, "Por favor encienda los medios de conexion a internet para continuar.");
        } else {

            boolean estadoGPS = Util.gpsIsOn(this);
            if (estadoGPS) {

                tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
                if (tipoDeClienteSeleccionado == 1) {
                    cliente = DataBaseBO.CargarClienteSeleccionado();
                } else {
                    cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
                }

                if (cliente != null) {

                    RegistroRango registroRango = Util.validarRegistroRango(this, cliente);
                    if (registroRango == null) {
                        Util.MostrarAlertDialog(this, "No se pudo registrar la visita.\nPor favor verifique que su GPS este encendedi y reinicie la visita.");
                        return estado;
                    }
                    if (rutero == -1) {
                        String keysre[] = {"RUTERO", "CODIGOCLIENTE"};
                        String datosre[] = Util.getPrefence(this, Const.PREFERENCERE, keysre);
                        if (datosre.length > 0) {
                            if (!datosre[0].equals("")) {
                                rutero = Util.ToInt(datosre[0].toString());
                            }
                        }
                    }

                    final RegistroVisita registroVisitaAlm = DataBaseBO.hayRegistroVisita(usuario.codigoVendedor, cliente.codigo);
                    if (registroVisitaAlm == null) {

                        int codigoTipoEjecucionRuta = 1;
                        if (rutero == Const.RUTA_VISITA) {
                            codigoTipoEjecucionRuta = Const.RUTA_VISITA;
                        } else {
                            codigoTipoEjecucionRuta = Const.RUTA_EXTRA_RUTA;
                        }
                        String seccion = Const.MODULO_REGISTRO_VISITA;
                        if (usuario != null) {
                            seccion += usuario.codigoVendedor;
                        }
                        if (cliente != null) {
                            seccion += cliente.codigo;
                        }
                        String numDoc = Util.obtenerNumdoc(seccion);
                        String fechaMovil = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
                        RegistroVisita registroVisita = new RegistroVisita();
                        registroVisita.numDoc = numDoc;
                        registroVisita.codigoVendedor = usuario.codigoVendedor;
                        registroVisita.codigoCliente = cliente.codigo;
                        registroVisita.fechaInicial = fechaMovil;
                        registroVisita.fechaFinal = fechaMovil;
                        registroVisita.ruta = rutero + "";
                        registroVisita.codigoTipoEjecucionRuta = codigoTipoEjecucionRuta + "";
                        registroVisita.finalizado = "0";
                        registroVisita.radio = registroRango.radio;
                        registroVisita.distancia = registroRango.distancia;
                        registroVisita.perimetro = registroRango.perimetro;
                        registroVisita.latitud = registroRango.latitud;
                        registroVisita.longitud = registroRango.longitud;
                        registroVisita.finalizado = "0";
                        registroVisita.id = Util.FechaActual("yyyyMMddHHmmss");
                        registroVisita.numDocGestion = Main.encabezado.numero_doc;
                        registroVisita.diaVisita = 0;//DataBaseBO.obtenerDiaVisita(usuario.codigo, cliente.codigo);
                        numDocRegVisita = numDoc;
                        estado = DataBaseBO.guardarRegistroVisita(registroVisita);
                        if (estado) {
                            estado = registrarCoordenada(numDoc, fechaMovil);
                            //DataBaseBO.actualizarRegistroCoordenadaCliente(cliente.codigo, numDocRegVisita);
                            Util.clearPrefence(this, Const.PREFERENCERENUMDOCREGVISITA);
                            String keysrereg[] = {"REGIVISITA"};
                            String datosrereg[] = {numDoc + ""};
                            Util.putPrefence(this, Const.PREFERENCERENUMDOCREGVISITA, keysrereg, datosrereg);
                        } else {
                            Log.e(TAG, "guardarRegistroVisita-> No se pudo almacenar la visita");
                        }
                    } else {
                        Log.e(TAG, "registroVisitaAlm diferente null -> ");
                    }
                } else {
                    Util.mostrarAlertDialog(this, "No se pudo cargar el cliente seleccionado.\nPor favor vuelva a intentarlo");
                    Log.e(TAG, "cliente es null ---------------------------");
                }
            } else {
                Util.mostrarAlertDialog(this, "Por favor encienda el GPS para continuar.");
            }
        }
        return estado;
    }

    public boolean registrarCoordenada(String numDoc, String fechaMovil) {
        boolean estado = false;
        try {
            GPSTracker gpsTracker = new GPSTracker(this);
            Location location = gpsTracker.getLocation();
            boolean estadoGPS = gpsTracker.isEstadoGPS();
            double latitud = 0;
            double longitud = 0;
            if (estadoGPS) {
                if (location != null) {
                    Usuario usuario = DataBaseBO.obtenerUsuario();
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();
                    Coordenada coordenada = new Coordenada();
                    coordenada.numDoc = numDoc;
                    coordenada.codigoVendedor = usuario.codigoVendedor;
                    coordenada.codigoCliente = cliente.codigo;
                    coordenada.latitud = latitud;
                    coordenada.longitud = longitud;
                    coordenada.fechaMovil = fechaMovil;
                    coordenada.finalizado = "0";
                    coordenada.numDocRegVisita = numDoc;
                    estado = DataBaseBO.guardarCoordenadaRegVisita(coordenada);
                    if (estado) {
                        Log.i(TAG, "guardarCoordenada-> " + estado + " latitud: " + latitud + " - longitud: " + longitud);
                    } else {
                        Log.i(TAG, "guardarCoordenada-> No se pudo almacenar la coordenada");
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "registrarCoordenada -> " + e.getMessage());
        }
        return estado;
    }

    public void onClickCatalogo(View view) {
        Intent intent = new Intent(FormPedidoActivity.this, CatalogoActivity.class);
        intent.putExtra("codTienda", 1);
        intent.putExtra("tipoTransaccion", tipoTransaccion);
        intent.putExtra("tipoClienteSeleccionado", tipoDeClienteSeleccionado);
        intent.putExtra("tipoTransaccionActual", tipoTransaccionActual);
        intent.putExtra("esCargue", esCargue);
        intent.putExtra("cambio", cambio);
        startActivity(intent);
    }

    public void SpinnerPrecios(final String codProducto, final String codCliente) {

        String[] items;
        Vector<String> listaItems = new Vector<String>();
        listaPrecios = DataBaseBO.ListaPrecios(listaItems, codProducto, codCliente);

        if (listaItems.size() > 0) {

            items = new String[listaItems.size()];
            listaItems.copyInto(items);

        } else {

            items = new String[]{};
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPrecios.setAdapter(adapter);

        for (int i = 0; i < spinnerPrecios.getCount(); i++) {
            if (spinnerPrecios.getItemAtPosition(i).toString().equalsIgnoreCase(Util.SepararMiles(Util.RedondearFit("" + producto.precio, 0)))) {
                spinnerPrecios.setSelection(i);
            }
        }
        spinnerPrecios.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                producto.precio = listaPrecios.get(i).precio;
                double precioDescuentoIva = (producto.precio - (producto.precio * producto.descuento / 100f))
                        * (1 + producto.iva / 100f);

                float promedio = listaPrecios.get(i).promedio;

                float margen = (float) (1 - (promedio / producto.precio));

                lblMargen.setText(Util.getDecimalFormatMargen(margen * 100));
                txtPrecioProducto.setText(Util.SepararMiles(String.valueOf(Util.getDecimalFormatMargen(producto.precio))));
                txtPrecioIvaProducto.setText(Util.SepararMiles(Util.Redondear(String.valueOf(precioDescuentoIva), 0)));
                DataBaseBO.actualizarPrecioProducto(codProducto, producto.precio);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void onClickMas() {

        TableLayout tlDescripcion = (TableLayout) findViewById(R.id.tlDescripcion);
        DescripsionXProducto dxp = new DescripsionXProducto();
        dxp = DataBaseBO.descripsionXProducto(codigoProducto);
        TextView lblDescripcion1 = (TextView) findViewById(R.id.lblDescripcion1);
        TextView lblDescripcion2 = (TextView) findViewById(R.id.lblDescripcion2);


        if (!presionado) {
            tlDescripcion.setVisibility(View.VISIBLE);
            presionado = true;
            btnMasContenido.setBackground(ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher_arriba, null));
            lblDescripcion1.setText(dxp.desc1);
            lblDescripcion2.setText(dxp.desc2);
        } else {
            tlDescripcion.setVisibility(View.GONE);
            presionado = false;
            btnMasContenido.setBackground(ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher_abajo_uno, null));
        }

    }

    public void mostrarDialogoTrasladoBodega(int cantidad) {

        if (dialogTrasladoBodega == null) {

            dialogTrasladoBodega = new Dialog(this);
            dialogTrasladoBodega.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogTrasladoBodega.setContentView(R.layout.dialog_traslado_bodegas);
        }

        //cargarCorreos();

        LinearLayout lytCorreos = (LinearLayout) dialogTrasladoBodega.findViewById(R.id.tablaCorreosElectronicos);
        lytCorreos.removeAllViews();

        listaCorreos = DataBaseBO.listarCorreos();

        for (int pos = 0; pos < listaCorreos.size(); pos++) {
            Correo correo = listaCorreos.get(pos);
            correo.checkeado = false;

            pintarCorreos(lytCorreos, correo, pos);

        }

        final TextView lblCodidoProducto = (TextView) dialogTrasladoBodega.findViewById(R.id.lblCodidoProducto);
        final TextView txtInvProducto = (TextView) dialogTrasladoBodega.findViewById(R.id.txtInvProducto);
        final TextView txtInvProducto1 = (TextView) dialogTrasladoBodega.findViewById(R.id.txtInvProducto1);
        final TextView txtInvProducto2 = (TextView) dialogTrasladoBodega.findViewById(R.id.txtInvProducto2);
        final TextView txtInvCantidad = (TextView) dialogTrasladoBodega.findViewById(R.id.txtInvCantidad);
        Button btnSi = (Button) dialogTrasladoBodega.findViewById(R.id.btnSi);
        Button btnNo = (Button) dialogTrasladoBodega.findViewById(R.id.btnNo);

        lblCodidoProducto.setText(producto.codigo);
        txtInvProducto.setText(producto.inv1);
        txtInvProducto1.setText(producto.inv2);
        txtInvProducto2.setText(producto.inv3);
        txtInvCantidad.setText(String.valueOf(cantidad));

        btnSi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                detalle.trasferenciaBodega = 1;
                lblCodidoProducto.setText("");
                txtInvProducto.setText("");
                txtInvProducto1.setText("");
                txtInvProducto2.setText("");
                txtInvCantidad.setText("");
                dialogTrasladoBodega.dismiss();
            }
        });
        btnNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                lblCodidoProducto.setText("");
                txtInvProducto.setText("");
                txtInvProducto1.setText("");
                txtInvProducto2.setText("");
                txtInvCantidad.setText("");
                dialogTrasladoBodega.dismiss();
            }
        });

        dialogTrasladoBodega.setCancelable(false);
        dialogTrasladoBodega.show();
        dialogTrasladoBodega.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void onClickMasReferencia() {

        productosAlt = DataBaseBO.buscarProductoAlt(producto.codigo);
        LinearLayout tlDescripcion = (LinearLayout) findViewById(R.id.lyListaAlterna);
        listaDatosAlternos = (RecyclerView) findViewById(R.id.listaDatosAlternos);

        listaDatosAlternos.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        listaDatosAlternos.setLayoutManager(mLayoutManager);

        mAdapter = new MyAdapter(productosAlt, this);
        listaDatosAlternos.setAdapter(mAdapter);
        mAdapter.clickListener(this);

        if (!presionador) {
            tlDescripcion.setVisibility(View.VISIBLE);
            presionador = true;
            btnAlternaLista.setBackground(ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher_arriba, null));

        } else {
            tlDescripcion.setVisibility(View.GONE);
            presionador = false;
            btnAlternaLista.setBackground(ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher_abajo_uno, null));
        }

    }

    //El de la interfez
    @Override
    public void onClick(View view, int position) {
        presionado = true;
        onClickMas();
        onClickMasReferencia();
        final Producto producto = productosAlt.get(position);
        CargarProducto(producto.codigo);


    }

    public void pintarCorreos(LinearLayout tablaCorreos, Correo correo, int pos) {

        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.list_item_table_correos, null);
        CheckBox checkCorreo = (CheckBox) view.findViewById(R.id.checkAgotado);
        //checkCorreo.setTag(correo);
        checkCorreo.setTag(pos + "");

        setCheckedCorreo(checkCorreo);
        setDataCorreos(correo, checkCorreo);
        String[] datos = {correo.nombre};
        for (int i = 0; i < datos.length; i++) {
            TextView textView = (TextView) view.findViewWithTag("column" + i);
            textView.setText(datos[i]);
            textView.setVisibility(View.VISIBLE);
        }
        tablaCorreos.addView(view);
    }

    public void setCheckedCorreo(CheckBox checkCorreo) {
        //final Correo correo = (Correo) checkCorreo.getTag();
        checkCorreo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                int posSelected = Integer.parseInt(buttonView.getTag().toString());

                Correo correoAux = listaCorreos.elementAt(posSelected);

                if (isChecked) {

                    correoAux.checkeado = true;

                } else {

                    correoAux.checkeado = false;

                }

                String s = "";

                listaCorreosSel= new Vector<Correo>();

                for (int i = 0; i < listaCorreos.size(); i++) {

                    if (listaCorreos.elementAt(0).checkeado) {
                       // s = s + listaCorreos.elementAt(i).correo + "\n";
                        //listaCorreosSel.add(listaCorreos.elementAt(i));
                        detalle.transfCorreo1 = listaCorreos.elementAt(0).correo;

                    }
                    if (listaCorreos.elementAt(1).checkeado) {

                        detalle.transfCorreo2 = listaCorreos.elementAt(1).correo;

                    }
                    if (listaCorreos.elementAt(2).checkeado) {

                        detalle.transfCorreo3 = listaCorreos.elementAt(2).correo;

                    }
                    if (listaCorreos.elementAt(3).checkeado) {

                        detalle.transfCorreo4 = listaCorreos.elementAt(3).correo;

                    }
                    if (listaCorreos.elementAt(4).checkeado) {

                        detalle.transfCorreo5 = listaCorreos.elementAt(4).correo;

                    }
                }
               // Util.mostrarToast1(FormPedidoActivity.this, s);

            }
        });
    }

    public void setCheckedPromocion(CheckBox checkPromocion) {
        //final Correo correo = (Correo) checkPromocion.getTag();
        checkPromocion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                   Main.encabezado.checkPromocion= 1;

                } else {

                    Main.encabezado.checkPromocion= 0;

                }

            }
        });
    }

    public void setCheckedCotizacion(CheckBox checkCotizacion) {

        checkCotizacion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    //Es cotizacion
                    Main.encabezado.checkCotizacion= 1;

                } else {

                    Main.encabezado.checkCotizacion= 0;

                }

            }
        });
    }

    public void setItemCorreo(int correoBodega, Correo correo) {
        if (correo != null) {
            for (int pos = 0; pos < listaCorreos.size(); pos++) {
                Correo correoElectronico = listaCorreos.get(pos);

            }
        }
    }

    public void setDataCorreos(Correo correo, CheckBox checkCorreo) {
        /*if (correo != null && checkCorreo != null) {
            if (!correo.nombre.equals("")) {
                checkCorreo.setChecked(true);
            }
        }*/
    }

}