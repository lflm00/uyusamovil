package celuweb.com.uyusa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.util.UUID;
import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.DataBaseBOJ;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.Encuestas;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Oportunidad;
import celuweb.com.DataObject.PedidoAmarre;
import celuweb.com.DataObject.RangoCliente;
import celuweb.com.DataObject.TopeBonificacion;
import celuweb.com.DataObject.Usuario;
import celuweb.com.DataObject.VendedorBonificacion;
import celuweb.com.printer.sewoo.SewooLKP20;

public class FormInfoClienteActivity extends CustomActivity implements Sincronizador {

    public static final String TAG = FormInfoClienteActivity.class.getName();

    Cliente cliente;

    int tipoDeClienteSeleccionado = 1;

    GPSListener gpsListener;

    AlertDialog alertDialog;

    LocationManager locationManager;

    Location currentLocation = null;

    Location currentLocation2 = null;

    ProgressDialog progressDialog, progressDialog2;

    private Handler timer = new Handler();

    private long mLastClickTime = 0;

    boolean isCambio = false;

    public double acomuladoClienteBonificaciones = 0;

    public boolean vendedorTieneBonificacion = false;

    public boolean clienteCumpleBonificacion = false;

    public VendedorBonificacion vendedorBonificacion = null;

    public TopeBonificacion topeBonificacion = null;

    public Usuario vendedorActual = null;

    PedidoAmarre pedidoAmarre = null;

    int guardar = 0;

    boolean gps = false;

    double lat;

    double longi;

    boolean regPedExitoso = false;

    boolean regDevExitoso = false;

    public double latitud;

    public double longitud;

    boolean isClienteNuevo = false;

    /**
     * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
     */
    private SewooLKP20 sewooLKP20;

    boolean primerEjecucion = true;

    String nroDocSesion = "";

    String nroDocDevolucion = "";

    private String macImpresora;

    String mensaje;

    boolean isAnulate = false;

    LinearLayout lyInfo, lyMas;

    TableLayout tbletInfo, tbleMas;

    Vector<Cliente> listaClientes;
    Vector<Cliente> listaClientesPotenciales;
    Dialog dialogoClientesPotenciales;
    String listaclientesstr;

    private Dialog dialogoCoordenadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_info_cliente);
        listaclientesstr = "";
        validarClickInfoMas();
        cargarVistaProductosAutoventa();
        CargarDatosCliente();
        cargarInformacionCartera();
        //verificarRegalo();


        iniciarGPS2();
        //verificar si el cliente actual tiene agotados o devoluciones.
        verificarAgotadosYDevoluciones();
        //verificar si hay oportunidad
        if (Main.cliente.oportunidad > 0) {
            mostrarAlertConProductosOportunidad(Main.cliente.codigo);
        }
        vendedorActual = DataBaseBO.ObtenerUsuario();
        cargarBundle();
        IniciarGPS();
        if (!DataBaseBOJ.hayCoordenadas(Main.cliente.codigo)) {
            guardarCoordenada();
        }
    }

    public void validarClickInfoMas() {
        lyMas = (LinearLayout) findViewById(R.id.lyMas);
        lyInfo = (LinearLayout) findViewById(R.id.lyInformacion);
        tbleMas = (TableLayout) findViewById(R.id.tblLayoutInformacionClienteInferior);
        tbletInfo = (TableLayout) findViewById(R.id.tblLayoutInformacionCliente);
        lyInfo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                tbletInfo.setVisibility(View.VISIBLE);
                tbleMas.setVisibility(View.GONE);
                lyInfo.setBackgroundResource(R.color.blue_focus);
                lyMas.setBackgroundResource(R.color.blue_onfocus);

            }
        });
        lyMas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                tbletInfo.setVisibility(View.GONE);
                tbleMas.setVisibility(View.VISIBLE);
                lyInfo.setBackgroundResource(R.color.blue_onfocus);
                lyMas.setBackgroundResource(R.color.blue_focus);

            }
        });

    }

    private void cargarInfoPedido() {
        final ProgressDialog progress = ProgressDialog.show(this, "Validando", "Validando información sin enviar...", true);
        progress.show();
        new Thread(new Runnable() {

            @Override
            public void run() {

                /*generar las acciones en hilo principal de views*/
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (progress != null) {
                            progress.dismiss();
                        }
                        new Task().execute("");
                        //
                    }
                });
            }
        }).start();

    }

    private void cargarBundle() {
        Bundle bundle = getIntent().getExtras();
        if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            if (bundle != null) {
                isAnulate = bundle.getBoolean("isAnulado");
                if (isAnulate) {
                    regPedExitoso = getIntent().getBooleanExtra("result", true);
                    nroDocSesion = getIntent().getStringExtra("nroDocSesion");
                }
            }

        }
        if (bundle != null) isClienteNuevo = bundle.getBoolean("clienteNuevo");

    }

    /**
     * Si el vendedor es de tipo autoventa, se crea la vista de productos.
     */
    private void cargarVistaProductosAutoventa() {
        DataBaseBO.CargarInfomacionUsuario();
        if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            DataBaseBO.crearVistaProductosAutoventa();
            //((Button) findViewById(R.id.btnSalir2)).setVisibility(View.VISIBLE);
            // ((LinearLayout) findViewById(R.id.btnCanastilla)).setVisibility(View.VISIBLE);
            ((LinearLayout) findViewById(R.id.btnInvCanastas)).setVisibility(View.VISIBLE);
            ((LinearLayout) findViewById(R.id.btnInvCliente)).setVisibility(View.VISIBLE);

        } else {
            ((ImageButton) findViewById(R.id.btnImprimir)).setVisibility(View.GONE);
            //			((Button)findViewById(R.id.btnSalir)).setVisibility(View.VISIBLE);
            ((LinearLayout) findViewById(R.id.btnCanastilla)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.btnInvCanastas)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.btnPrqCliente)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.btnInvCliente)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.btnPrqClientePreventa)).setVisibility(View.VISIBLE);
            ((LinearLayout) findViewById(R.id.btnInvClientePreventa)).setVisibility(View.VISIBLE);
        }
    }

    public void CargarDatosCliente() {
        TextView lblCodigo = (TextView) findViewById(R.id.lblCodigo);
        lblCodigo.setText("\t\t\t" + Main.cliente.codigo);
        TextView lblNombre = (TextView) findViewById(R.id.lblNombre);
        lblNombre.setText(Main.cliente.Nombre);
        TextView lblRazonSocial = (TextView) findViewById(R.id.lblRazonSocial);
        lblRazonSocial.setText(Main.cliente.razonSocial);
        TextView lblDireccion = (TextView) findViewById(R.id.lblDireccion);
        lblDireccion.setText(Main.cliente.direccion);
        TextView lblTelefono = (TextView) findViewById(R.id.lblTelefono);
        lblTelefono.setText(Main.cliente.Telefono);
        TextView lblCiudad = (TextView) findViewById(R.id.lblCiudad);
        lblCiudad.setText(Main.cliente.Ciudad);
        TextView lblNit = (TextView) findViewById(R.id.lblNit);
        lblNit.setText(String.valueOf(Main.cliente.Nit));
        TextView lblDC = (TextView) findViewById(R.id.lblDiasCreacion);
        lblDC.setText(String.valueOf(Main.cliente.diasCreacion));
        TextView lblBloqueado = (TextView) findViewById(R.id.lblBloqueado);
        lblBloqueado.setText(Main.cliente.Bloqueado);
        double cartera = DataBaseBO.getValorCartera(Main.cliente.codigo);
        TextView lblCartera = (TextView) findViewById(R.id.lblCartera);
        lblCartera.setText(Util.SepararMiles(String.valueOf(Util.getDecimalFormatCartera(cartera))));
        TextView lblCupo = (TextView) findViewById(R.id.lblCupo);
        lblCupo.setText(Util.SepararMiles(Util.getDecimalFormatCartera(Main.cliente.Cupo + DataBaseBO.getValorRecaudoCliente(Main.cliente.codigo))));
        TextView lblTipologia = (TextView) findViewById(R.id.lblTipologia);
        lblTipologia.setText(Main.cliente.tipologia);
        String EC = DataBaseBO.getExperienciaCrediticia(Main.cliente.codigo);
        TextView lblEC = (TextView) findViewById(R.id.lblEC);
        lblEC.setText(EC);
        TextView lblFormaPago = (TextView) findViewById(R.id.lblFormaPago);
        lblFormaPago.setText(Main.cliente.formaPago);
        TextView lblAcomBonif = (TextView) findViewById(R.id.lblAcumBon);
        lblAcomBonif.setText(Main.cliente.AcomuladoBonif);

    }

	/*public void CargarInformacionCartera() {

		Main.cliente.carteraPendiente = DataBaseBO.CarteraPendiente(Main.cliente.codigo);

		TextView lblCartera = (TextView)findViewById(R.id.lblCartera);
		lblCartera.setText("" + Util.SepararMiles(Util.QuitarE("" + Main.cliente.carteraPendiente.total)));

		Main.encabezado.carteraVencida  = Main.cliente.carteraPendiente.diasVencido > 0;
		if (Main.encabezado.carteraVencida) {

			MostrarAlertDialog("Tiene cartera vencida superior a diez dias " + Main.cliente.carteraPendiente.diasVencido +  ".\n\n" +
					           "Los Pedidos que se tomen seran bajo responsabilidad del vendedor");

		}
	}*/

    public void OnClickPedidoCliente(View view) {

        /* Evitar evento de doble click */
        if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        if (Util.estaGPSEncendido(this)) {
            // validar que no hay encuestas disponibles.
            boolean encuestaDisponible = validarEncuestasDosponibleObligatorias();
            if (encuestaDisponible) {
                MostrarAlertDialog("Hay encuesta disponibles Obligatorias, debe realizar primero la encuesta.");
            } else {
                progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Cargando Productos...", true);
                progressDialog.show();
                CargarProductos cp = new CargarProductos();
                cp.start();
            }

        } else {
            mostrarMensajeActivarGPS();
        }


    }

    private void mostrarMensajeActivarGPS() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("El GPS esta desactivado. Por favor activelo antes de continuar.").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, Const.ENABLE_GPS);
                dialog.cancel();
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void OnClickFinalizarCliente(View view) {


        /* Evitar evento de doble click */
        if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        if (Util.estaGPSEncendido(this)) {
            // validar que no hay encuestas disponibles.
            boolean encuestaDisponible = validarEncuestasDosponible();
            if (encuestaDisponible) {
                MostrarAlertDialog("Hay encuesta disponible, debe realizar primero la encuesta.");
            } else {
                progressDialog = ProgressDialog.show(this, "", "Enviando Informacion...", true);
                progressDialog.show();
                Sync sync = new Sync(this, Const.ENVIAR_PEDIDO);
                sync.start();

            }

        } else {
            mostrarMensajeActivarGPS();
        }
    }

    /**
     * validar que hay enuestas disponibles para el cliente. si es asi no se debe permitir el pedido.
     *
     * @return
     */
    private boolean validarEncuestasDosponible() {
        Vector<ItemListView> listaItems = new Vector<ItemListView>();
        Vector<Encuestas> listaEncuestas;
        listaEncuestas = DataBaseBO.ListaEncuestas(listaItems, 0, Main.cliente.Canal);
        int disponibles = 0;
        for (Encuestas encuestas : listaEncuestas) {
            //disponibles = DataBaseBO.encuestasDisponibles(encuestas.codigo, Main.cliente.Canal);
            disponibles = listaEncuestas.size();

        }
        if (disponibles > 0) {
            return true;
        }
        return false;
    }

    private boolean validarEncuestasDosponibleObligatorias() {
        Vector<ItemListView> listaItems = new Vector<ItemListView>();
        Vector<Encuestas> listaEncuestas;
        int obligatorias = 0;
        listaEncuestas = DataBaseBO.ListaEncuestas(listaItems, 0, Main.cliente.codigo);
        for (Encuestas encuestas : listaEncuestas) {
            if (encuestas.obligatoria == 1) {
                obligatorias += encuestas.obligatoria;
            }
        }
        if (obligatorias > 0) {
            return true;
        }
        return false;
    }

    /**
     * cargar productos para modulo de pedidos
     */
    public void cargarProductos() {
        Main.encabezado.codigo_novedad = 1;
        Main.encabezado.tipoTrans = 0;
        Main.itemsBusq = new ItemListView[]{};
        Main.codBusqProductos = "";
        Main.posOpBusqProductos = 0;
        Main.posOpLineas = 0;
        Main.primeraVez = true;
        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado == 1) {
            cliente = DataBaseBO.CargarClienteSeleccionado();
            Log.i("Cliente", "ENtro");
        } else {
            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
            Log.i("Cliente Nuevo", "ENtro");
        }
        Main.cliente = cliente;
        if (tipoDeClienteSeleccionado == 1) {
            if (Main.cliente.bodega == null)
                Main.cliente = DataBaseBO.CargarCliente(Main.cliente.codigo);
        }
        if (Main.usuario == null) DataBaseBO.CargarInfomacionUsuario();
        // verificar que el cliente no este bloqueado.
        if (cliente.Bloqueado.equals("S")) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FormInfoClienteActivity.this);
                    builder.setTitle("Cliente Bloqueado");
                    builder.setMessage("El cliente esta bloqueado, no se permite hacer el pedido");
                    builder.setPositiveButton("OK", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (progressDialog != null) progressDialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
        } else {
            Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
            DataBaseBO.organizarTmpDescuentos();
            if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
                DataBaseBO.organizarTmpProductos(cliente.CodigoAmarre, Const.AUTOVENTA);
                Log.i("AUTOVENTA", "ENTRO");
            } else {
                DataBaseBO.organizarTmpProductos(cliente.CodigoAmarre, Const.PREVENTA);
                Log.i("PREVENTA", "ENTRO");
            }
            DataBaseBO.ListarProductos(cliente.CodigoAmarre);
            if (progressDialog != null) progressDialog.dismiss();
            if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
                if (!regPedExitoso) {
                    Intent formPedido = new Intent(this, FormPedidoActivity.class);
                    formPedido.putExtra("cambio", false);
                    formPedido.putExtra("pedido", true);
                    startActivityForResult(formPedido, Const.RESP_PEDIDO_EXITOSO);

                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(FormInfoClienteActivity.this);
                            builder.setTitle("Cliente Pausado");
                            builder.setMessage("El cliente tiene una sesion abierta, no se permite hacer otro pedido.");
                            builder.setPositiveButton("OK", new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    if (progressDialog != null) progressDialog.dismiss();
                                }
                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });
                }

            } else {
                Intent formPedido = new Intent(this, FormPedidoActivity.class);
                formPedido.putExtra("cambio", false);
                formPedido.putExtra("pedido", true);
                startActivityForResult(formPedido, Const.RESP_PEDIDO_EXITOSO);
            }

        }
    }

    /**
     * cargar productos para modulo de devoluciones
     */
    public void cargarProductos2() {
        Main.encabezado.codigo_novedad = 10;
        Main.encabezado.tipoTrans = 2;
        Main.itemsBusq = new ItemListView[]{};
        Main.codBusqProductos = "";
        Main.posOpBusqProductos = 0;
        Main.posOpLineas = 0;
        Main.primeraVez = true;
        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado == 1) {
            cliente = DataBaseBO.CargarClienteSeleccionado();
        } else {
            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
        }
        Main.cliente = cliente;
        if (Main.cliente.bodega == null)
            Main.cliente = DataBaseBO.CargarCliente(Main.cliente.codigo);
        if (Main.usuario == null) DataBaseBO.CargarInfomacionUsuario();
        Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
        DataBaseBO.organizarTmpDescuentos();
        //		DataBaseBO.organizarTmpProductos( cliente.portafolio);
        if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            DataBaseBO.organizarTmpProductos(cliente.CodigoAmarre, Const.AUTOVENTA);
        } else {
            DataBaseBO.organizarTmpProductos(cliente.CodigoAmarre, Const.PREVENTA);
        }
        DataBaseBO.ListarProductos(cliente.CodigoAmarre);
        if (progressDialog != null) progressDialog.dismiss();
        Intent formPedido = new Intent(this, FormDevolucionActivity.class);
        formPedido.putExtra("cambio", true);
        formPedido.putExtra("pedidoAmarre", pedidoAmarre);
        startActivityForResult(formPedido, Const.RESP_DEVOLUCION_EXITOSA);
    }

    class CargarProductos extends Thread {

        public void run() {
            cargarProductos();
        }
    }

    class CargarProductos2 extends Thread {

        public void run() {
            cargarProductos2();
        }
    }

    class CargarProductos3 extends Thread {

        public void run() {
            cargarProductos3();
        }
    }

    public void OnClickNoCompraCliente(View view) {

        /*retardo de 2000ms para evitar eventos de doble click.
         * esto significa que solo se puede hacer click cada 2000ms.
         * es decir despues de presionar el boton, se debe esperar que transcurran
         * 1500ms para que se capture un nuevo evento.*/
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();//inicializa esta variable en 0

        /*si el cliente tiene factura, deshabilitar opcion de devoluciones*/
    /*    if (DataBaseBO.tieneFacturasCliente(Main.cliente.codigo)) {
            Util.MostrarAlertDialog(this, "No se permite registrar no compra a un cliente con factura.");
            return;
        }  */
        Main.encabezado = new Encabezado();
        Main.encabezado.codigo_novedad = 0;
        Main.encabezado.tipoTrans = 0;
        Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
        Intent formNoCompra = new Intent(this, FormNoCompraActivity2.class);
        startActivityForResult(formNoCompra, Const.RESP_NO_COMPRA_EXITOSO);
    }

    public void OnClickCambiosCliente(View view) {
        if (Main.cliente.Bloqueado.equals("N")) {
            if (Main.formOpcionesPedido == null)
                Main.formOpcionesPedido = new Intent(this, OpcionesPedidoActivity.class);
            Main.formOpcionesPedido.putExtra("cambio", true);
            Main.encabezado.motivo = 10;
            Main.encabezado.tipoTrans = 2;
            Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
            startActivityForResult(Main.formOpcionesPedido, Const.RESP_PEDIDO_EXITOSO);

        } else {
            Util.MostrarAlertDialog(this, "Este Cliente Esta Bloqueado. No se puede Ingresar PNC");
        }
    }

    public void OnClickEditarCliente(View view) {
        Intent formEditarCliente = new Intent(this, FormEditarCliente.class);
        startActivity(formEditarCliente);
    }

    /**
     * metodo para capturar evento del boton de encuesta en el formulario de info cliente.
     *
     * @param view
     * @by JICZ
     */
    public void OnClickEncuestar(View view) {
        Intent intent = new Intent(this, FormEncuestasActivity.class);
        startActivity(intent);
    }

    /**
     * metodo para capturar evento del boton de PQR en el formulario de info cliente. para redirigir a la web
     *
     * @param view
     * @by JICZ
     */
    public void OnClickWebPqr(View view) {
        Intent intent = new Intent(this, FormWebPqrActivity.class);
        startActivity(intent);
    }

    public void OnClickRegresar(View view) {
        if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            if (regPedExitoso) {
                if (DataBaseBO.existeRegCanastilla(nroDocSesion)) {
                    salir();
                    return;
                } else {
                    Util.MostrarAlertDialog(FormInfoClienteActivity.this, "Es necesario ingresar las Canastillas despachadas en la factura.");
                }

            } else {
                salir();
                return;
            }

        } else {
            salir();
            return;
        }

    }

    public void salir() {
        if (locationManager != null) locationManager.removeUpdates(gpsListener);
        setResult(RESULT_OK);
        finish();
    }

    public void Finalizar() {
        if (locationManager != null) locationManager.removeUpdates(gpsListener);
        setResult(RESULT_OK);
        //finish();
    }

	/*public void OnClikFormInfoCliente(View view) {

		switch (view.getId()) {

			case R.id.btnPedidos:

				if (Main.cliente.bloqueado.equals("N")) {

					//Si ya tiene un pedio del dia, mostrar un alert al usuario.

					if (Main.formOpcionesPedido == null)
						Main.formOpcionesPedido = new Intent(this, OpcionesPedidoActivity.class);

					Main.encabezado.no_compra = 0;
					Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
					startActivityForResult(Main.formOpcionesPedido, Const.RESP_PEDIDO_EXITOSO);

				} else {

					Util.MostrarAlertDialog(this, "Este Cliente Esta Bloqueado. No se puede tomar pedido");
				}
				break;

			case R.id.btnRecaudo:

				//Intent formCarteraRecaudo = new Intent(this, FormCarteraRecaudo.class);
				//startActivity(formCarteraRecaudo);

				Intent formTabsOpcionesCartera = new Intent(this, FormTabsOpcionesCartera.class);
				startActivityForResult(formTabsOpcionesCartera, Const.RESP_RECAUDO_EXITOSO);
				break;

			case R.id.btnNoCompra:

				Main.encabezado.no_compra = 1;
				Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();

				Intent formNoCompra = new Intent(this, FormNoCompraActivity.class);
				startActivityForResult(formNoCompra, Const.RESP_NO_COMPRA_EXITOSO);
				break;

			case R.id.btnAtras:
				finish();
				break;
		}
	}*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Entro al onActivityResult");
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Const.RESP_PEDIDO_EXITOSO:

                    if (DataBaseBO.HayInformacionXEnviar1()) {
                        progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Enviando Informacion...", true);
                        progressDialog.show();
                        Sync sync = new Sync(FormInfoClienteActivity.this, Const.ENVIAR_PEDIDO);
                        sync.start();

                    }

                    break;
                case Const.RESP_RECAUDO_EXITOSO:
                    regPedExitoso = data.getBooleanExtra("result", true);
                    nroDocSesion = data.getStringExtra("nroDocSesion");
                    Finalizar();
                    break;
                case Const.RESP_DEVOLUCION_EXITOSA:
                    regDevExitoso = data.getBooleanExtra("result", true);
                    nroDocDevolucion = data.getStringExtra("nroDocDev");
                    break;
                case Const.RESP_CANASTILLA_SIN_VENTA_EXITOSA:
                    salir();
                    break;
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            switch (requestCode) {
                case Const.RESP_RECAUDO_EXITOSO:
                    regPedExitoso = data.getBooleanExtra("result", false);
                    break;
                case Const.RESP_DEVOLUCION_EXITOSA:
                    if (data != null) regDevExitoso = data.getBooleanExtra("result", false);
                    break;
            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
                if (regPedExitoso) {
                    if (DataBaseBO.existeRegCanastilla(nroDocSesion)) {
                        salir();
                        return true;
                    } else {
                        Util.MostrarAlertDialog(FormInfoClienteActivity.this, "Es necesario ingresar las Canastillas despachadas.");
                    }

                } else {
                    salir();
                    return true;
                }

            } else {
                salir();
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    public void MostrarAlertDialog(String mensaje) {
        if (alertDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alertDialog = builder.create();
        }
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    @SuppressLint("MissingPermission")
    public void IniciarGPS() {

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(criteria, true);

        if (provider == null)
            provider = LocationManager.GPS_PROVIDER;

        //List<String> all = locationManager.getAllProviders();
        //Log.w("IniciarGPS", "All available location providers: " + all.toString() );

        gpsListener = new GPSListener();
        locationManager.requestLocationUpdates(provider, 1 * 60 * 1000, 0, gpsListener);
    }


    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (locationManager != null && gpsListener != null) {
                locationManager.removeUpdates(gpsListener);
            }

            if (currentLocation != null && guardar == 0) {

                DataBaseBO.validarUsuario();

                if (Main.usuario != null && Main.usuario.codigoVendedor != null) {

                    Coordenada coordenada = new Coordenada();
                    coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                    coordenada.codigoCliente = Main.cliente.codigo;
                    coordenada.latitud = currentLocation.getLatitude();
                    coordenada.longitud = currentLocation.getLongitude();
                    coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                    coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
                    coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                    latitud = currentLocation.getLatitude();
                    longitud = currentLocation.getLongitude();
                    Log.i("COORDENADA CLIENTE", currentLocation.getLatitude() + "--" + currentLocation.getLongitude());
                    //if (progressDialog != null) progressDialog.dismiss();
                    //progressDialog.cancel();
                    boolean guardo = Coordenada.save(FormInfoClienteActivity.this, coordenada);

                    if (guardo) {
                        Log.i("GUARDO", "OK");
                        guardar = -1;
                    }
                }
            }
        }
    };

    /*private class GPSListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            if (location != null) {

                currentLocation = location;
                handler.sendEmptyMessage(0);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }*/

    /*private class TaskGPS extends TimerTask {

        private int i = 0;
        private int min = 9;
        private int max = 10;

        @Override
        public void run() {

            if (i == min) {

                if (gpsListener != null)
                    gpsListener.CapturarCoordenada();
            }

            i = (i + 1) % max;
        }

        public void ResetTime() {

            i = 0;
        }
    }*/

    public void onClickCoordenadas(View view) {
        guardarCoordenada();
    }

    private void guardarCoordenada() {
        if (!DataBaseBOJ.hayCoordenadas(Main.cliente.codigo)) {
            mostrarDialogoCoordenadasCliente();
        } else {
            MostrarAlertDialog("El cliente ya cuenta con coordenadas, si desea modificarla por favor contacte a su administrador!");
        }
    }

    private void mostrarDialogoCoordenadasCliente() {

        if (dialogoCoordenadas == null) {
            dialogoCoordenadas = new Dialog(this);
            dialogoCoordenadas.setContentView(R.layout.dialog_coordenadas_cliente);
            ((ImageView) dialogoCoordenadas.findViewById(R.id.btnCoordenadas)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    capturarCoordenada();
                }
            });
            ((Button) dialogoCoordenadas.findViewById(R.id.btnAceptarResumenPedido)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Coordenada coordenada = new Coordenada();
                    coordenada.codigoCliente = Main.cliente.codigo;
                    coordenada.latitud = latitud;
                    coordenada.longitud = longitud;
                    if (coordenada.latitud == 0 && coordenada.longitud == 0) {
                        MostrarAlertDialog("Pulse el icono para capturar coordenadas!");
                    } else {

                        if (DataBaseBOJ.guardarCoordenadaClienteNuevo(coordenada)) {
                            MostrarAlertDialog("Coordenadas almacenadas con exito");
                            dialogoCoordenadas.dismiss();
                        } else {
                            MostrarAlertDialog("Error al registrar informacion");
                        }

                    }
                }
            });
            ((Button) dialogoCoordenadas.findViewById(R.id.btnCancelarResumenPedido)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogoCoordenadas.dismiss();
                    // finish();
                }
            });

        }

        dialogoCoordenadas.setCancelable(false);
        dialogoCoordenadas.show();

    }

    public void progressCoordenada() {
        progressDialog2 = new ProgressDialog(FormInfoClienteActivity.this);
        progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog2.setMessage("Obteniendo coordenadas...");
        progressDialog2.setCancelable(false);
        progressDialog2.show();

        timer.postDelayed(forzarCierre, 1000 * 50); // 90 segundos (1.5 minutos) antes de ejecutarse el cierre. (tiempo en milisegundos)
    }

    public Runnable forzarCierre = new Runnable() {

        public boolean killMe = false;

        public void run() {

            if (killMe)
                return;

            if (locationManager != null && gpsListener != null)
                locationManager.removeUpdates(gpsListener); // remover el listener para evitar capturas basura.

            progressDialog2.dismiss();

            if (latitud == 0 && longitud == 0) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "No se pudo obtener la coordenada", Toast.LENGTH_SHORT).show();
                        progressDialog2.dismiss();
                    }
                });
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

            } else {

                currentLocation = currentLocation2;
                handler.sendEmptyMessage(0);
                ((TextView) dialogoCoordenadas.findViewById(R.id.labLatitud)).setText("" + latitud);
                ((TextView) dialogoCoordenadas.findViewById(R.id.lblLongitud)).setText("" + longitud);

            }
        }

        void killRunnable() {
            killMe = true;
        }

    };


    /**
     * private Handler handler = new Handler() {
     *
     * @Override public void handleMessage(Message msg) {
     * if (locationManager != null && gpsListener != null) {
     * locationManager.removeUpdates(gpsListener);
     * }
     * if (currentLocation != null && guardar == 0) {
     * DataBaseBO.validarUsuario();
     * if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
     * Coordenada coordenada = new Coordenada();
     * coordenada.codigoVendedor = Main.usuario.codigoVendedor;
     * coordenada.codigoCliente = Main.cliente.codigo;
     * coordenada.latitud = currentLocation.getLatitude();
     * coordenada.longitud = currentLocation.getLongitude();
     * coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
     * coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
     * coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
     * latitud = currentLocation.getLatitude();
     * longitud = currentLocation.getLongitude();
     * Log.i("COORDENADA CLIENTE", currentLocation.getLatitude() + "--" + currentLocation.getLongitude());
     * //progressDialog2.cancel();
     * boolean guardo = Coordenada.save(FormInfoClienteActivity.this, coordenada);
     * if (guardo) {
     * Log.i("GUARDO", "OK");
     * guardar = -1;
     * }
     * }
     * }
     * }
     * };
     */

    public void OnClickHistorialPedido(View view) {
        Intent intent = new Intent(this, FormHistorialPedidos.class);
        startActivity(intent);
    }

    public void onClickCartera(View view) {

        /*retardo de 2000ms para evitar eventos de doble click.
         * esto significa que solo se puede hacer click cada 2000ms.
         * es decir despues de presionar el boton, se debe esperar que transcurran
         * 1500ms para que se capture un nuevo evento.*/
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();//inicializa esta variable en 0

        int tipoDeClienteSeleccionado2 = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado2 == 1) {
            if (DataBaseBO.existeCarteraCliente(cliente.codigo)) {
                Intent intent = new Intent(this, FormCarteraInformacion.class);
                startActivity(intent);

            } else {
                Util.mostrarToast(this, "El cliente no tiene cartera disponible");
            }

        } else {
            Util.MostrarAlertDialog(this, "Cliente Nuevo no Tiene Cartera");

        }

    }

    public void onClickRecaudo(View view) {

        /*retardo de 2000ms para evitar eventos de doble click.
         * esto significa que solo se puede hacer click cada 2000ms.
         * es decir despues de presionar el boton, se debe esperar que transcurran
         * 1500ms para que se capture un nuevo evento.*/
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();//inicializa esta variable en 0

        int tipoDeClienteSeleccionado2 = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado2 == 1) {
            borrarDatosTemporalesCartera();
            Util.guardarDatosDeRecaudo();
            Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
            Intent intent = new Intent(this, FormCarteraFacturas.class);
            //					startActivityForResult(intent, Const.RESP_RECAUDO_EXITOSO);
            startActivity(intent);

        } else {
            Util.MostrarAlertDialog(this, "Cliente Nuevo no Tiene Cartera");

        }

    }

    public void borrarDatosTemporalesCartera() {
        Main.consignacion = false;
        Main.total_recaudo = 0;
        Main.total_descuento = 0;
        Main.total_forma_pago = 0;
        //Main.cartera.clear();
        Main.listaDescuentos.clear();
        Main.listaFormasPago.clear();
        if (Main.cartera != null) Main.cartera.clear();
        if (Main.listaFormasPago != null) Main.listaFormasPago.clear();
        if (Main.listaDescuentos != null) Main.listaDescuentos.clear();

    }

    public void cargarInformacionCartera() {
        int tipoDeClienteSeleccionado4 = DataBaseBO.ObtenerTipoClienteSeleccionado();
        boolean tieneCarteraVencida = false;
        if (tipoDeClienteSeleccionado4 == 1) {
            cliente = DataBaseBO.CargarClienteSeleccionado();
            Vector<Cartera> listaCartera = DataBaseBO.listaCartera(Main.cliente.codigo);
            for (Cartera cartera : listaCartera) {
                if (cartera.dias > 0) {
                    tieneCarteraVencida = true;
                    break;

                }

            }
            if (tieneCarteraVencida)
                Util.MostrarAlertDialog(this, getString(R.string.msg_cliente_cartera_vencida), 2);

        }
    }

    /**
     * evento para iniciar devoluciones
     *
     * @param view
     */
    public void OnClickCambioCliente(View view) {
        /* Evitar evento de doble click */
        if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        if (vendedorActual.tipoVenta.equals(Const.AUTOVENTA)) {
            pedidoAmarre = DataBaseBO.existePedidoSinAmarre(cliente.codigo, vendedorActual.codigoVendedor);
            if (pedidoAmarre != null) {
                progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Cargando Productos...", true);
                progressDialog.show();
                CargarProductos cp = new CargarProductos();
                isCambio = true;
                cp.start();

            } else {
                Util.MostrarAlertDialog(this, "Antes de hacer una novedad en entrega, es necesario tener registrado un pedido.");
            }

        } else {
            progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Cargando Productos...", true);
            progressDialog.show();
            CargarProductos2 cp = new CargarProductos2();
            cp.start();
        }
    }

    public void verificarRegalo() {
        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado == 1) {
            cliente = DataBaseBO.CargarClienteSeleccionado();

        } else {
            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();

        }
        vendedorBonificacion = DataBaseBO.ExisteBonificacionVendedor();
        topeBonificacion = DataBaseBO.aplicaBonificacionVendedor(cliente.tipologia);
        double tope = 0;
        if (vendedorBonificacion != null) if (topeBonificacion != null) {
            tope = topeBonificacion.tope;

        }
        if (tope > 0) {
            Main.sticker = 1;
            Util.MostrarAlertDialog(this, "Hay una promocion Activa Para Este Cliente Por la Compra Mayor a " + tope + " y 3 Categorias " + "\n" + "Acomulado Mes Bonific: " + Util.SepararMiles(cliente.AcomuladoBonif), 0);

        } else {
            Main.sticker = 0;

        }

    }

    public void iniciarGPS2() {
        Coordenada.delete(this);
        turnOnGPS();
        if (isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Verifica si GPS_PROVIDER esta Activo. En caso tal Registra el Listener de Captura de Coordenadas con GPS_PROVIDER
            registrarListenerGPS(LocationManager.NETWORK_PROVIDER);

        } else if (isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Verifica si NETWORK_PROVIDER esta Activo. En caso tal Registra el Listener de Captura de Coordenadas con NETWORK_PROVIDER
            registrarListenerGPS(LocationManager.GPS_PROVIDER);

        } else {
            /**
             * Se guarda seguimiento indicando que el GPS esta apagado!
             **/
            DataBaseBO.validarUsuario();
            if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
                Coordenada coordenada = new Coordenada();
                coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                coordenada.codigoCliente = Main.cliente.codigo;
                coordenada.latitud = 0;
                coordenada.longitud = 0;
                coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                coordenada.estado = Coordenada.ESTADO_GPS_APAGADO;
                coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                Coordenada.save(this, coordenada);
            }
        }
    }

    protected void turnOnGPS() {
        //Se valida si se puede Activar el GPS
        boolean turnOn = canToggleGPS();
        if (turnOn) {
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (provider != null && !provider.contains("gps")) {
                final Intent intent = new Intent();
                intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
                intent.setData(Uri.parse("3"));
                sendBroadcast(intent);
            }
        }
    }

    private boolean canToggleGPS() {
        PackageManager packageManager = getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);

        } catch (NameNotFoundException e) {
            //Paquete no encontrado
            return false;
        }
        if (packageInfo != null) {
            for (ActivityInfo actInfo : packageInfo.receivers) {
                String name = actInfo.name;
                boolean exported = actInfo.exported;
                //Verifica si el receiver es exported. En caso tal se puede actiar el GPS.
                if (name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && exported) {
                    return true;
                }
            }
        }
        return false;
    }

	/*public void registrarListenerGPS(String provider) {

		try {

			//String provider = getGPSProvider();

			if (provider != null && !provider.equals("")) {

				if (locationManager == null)
					locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

				if (gpsListener == null)
					gpsListener = new GPSListener();

				//Registra el Listener de Coordenadas.
				locationManager.requestLocationUpdates(provider, 0, 0, gpsListener);

			} else {

				//Log.e(TAG, "registrarListenerGPS -> No se encontro el Provider");
			}

		} catch (Exception e) {

			//Log.e(TAG, "registrarListenerGPS -> " + e.getMessage(), e);
		}
	}*/

	/*public String getGPSProvider() {

		SharedPreferences settings = getSharedPreferences(Const.SETTINGS_GPS, MODE_PRIVATE);
		return settings.getString(Const.PROVIDER, "");
	}

	public void setGPSProvider(String provider) {

		SharedPreferences settings = getSharedPreferences(Const.SETTINGS_GPS, MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(Const.PROVIDER, provider);
		editor.commit();
	}

	public void removeGPSProvider() {

		SharedPreferences settings = getSharedPreferences(Const.SETTINGS_GPS, MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(Const.PROVIDER);
		editor.commit();
	}	
*/

	/*public boolean isProviderEnabled(String provider) {

		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		if (manager != null) {

			return manager.isProviderEnabled(provider);
		}

		return false;
	}    */

    @Override
    protected void onResume() {
        super.onResume();
        //Main.usuario = null;
        //Main.cliente = null;
        if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {
            DataBaseBO.CargarInfomacionUsuario();

        }
        if (Main.cliente == null || Main.cliente.codigo == null) {
            int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;
            if (tipoDeClienteSelec == 1) {
                clienteSel = DataBaseBO.CargarClienteSeleccionado();

            } else {
                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();

            }
            if (clienteSel != null) Main.cliente = clienteSel;

        }
        CargarDatosCliente();
        cargarInfoPedido();
        if (validarEncuestasDosponible()) {
            MostrarAlertDialog("Hay encuesta disponibles.");
        }
    }

    private void mostrarAlertConProductosOportunidad(String codigo) {
        //capturar informacion de la oportunidad para mostrar al cliente.
        Oportunidad op = DataBaseBO.obtenerOportunidad(codigo);
        String oportunidad = (!op.laBuena.equals("-") ? op.laBuena + "\n" : "") + (!op.campi.equals("-") ? op.campi + "\n" : "") + (!op.gourmet.equals("-") ? op.gourmet + "\n" : "") + (!op.olioSoya.equals("-") ? op.olioSoya + "\n" : "") + (!op.sol.equals("-") ? op.sol + "\n" : "") + (!op.chocoExpress.equals("-") ? op.chocoExpress + "\n" : "") + (!op.lukafe.equals("-") ? op.lukafe + "\n" : "") + (!op.aromaPulverizado.equals("-") ? op.aromaPulverizado + "\n" : "") + (!op.fluocardent.equals("-") ? op.fluocardent + "\n" : "") + (!op.tarritoRojo.equals("-") ? op.tarritoRojo + "\n" : "");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Cliente con oportunidad de venta");
        builder.setMessage("Se pueden ofrecer los siguientes productos: \n" + oportunidad);
        builder.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * inicia activity para mostrar agotados o devoluciones
     *
     * @param view
     */
    public void onClickAgotadosYDevoluciones(View view) {
        Intent i = new Intent(FormInfoClienteActivity.this, FormAgotadosYDevoluciones.class);
        startActivity(i);
    }

    /**
     * verifica si hay devoluciones o agotados para mostrar, si es asi muestra un alert que dirige al actvity que muestra el reporte.
     */
    private void verificarAgotadosYDevoluciones() {
        int tiene = DataBaseBO.verificarAgotasYdevoluciones(Main.cliente.codigo);
        if (tiene > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(FormInfoClienteActivity.this);
            builder.setTitle("Cliente con devoluciones o agotados");
            builder.setMessage("Presione OK para ver el reporte");
            builder.setPositiveButton("OK", new OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    Intent i = new Intent(FormInfoClienteActivity.this, FormAgotadosYDevoluciones.class);
                    startActivity(i);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    /**
     * iniciar activity para modificar el cliente actual.
     *
     * @param view
     */
    public void onClickModificarCliente(View view) {
        Intent intent = new Intent(FormInfoClienteActivity.this, FormModificarCliente.class);
        intent.putExtra("codigoCliente", Main.cliente.codigo);
        intent.putExtra("clienteNuevo", isClienteNuevo);
        startActivity(intent);
    }

    /**
     * iniciar activity para ingresar a la canastilla de productos.
     *
     * @param view
     */
    public void onClickCanastilla(View view) {
        if (nroDocSesion.equals("") && nroDocDevolucion.equals(""))
            nroDocSesion = Util.generarNumdoc("REGC", Main.cliente.codigo, Main.usuario.codigoVendedor);
        Intent intent = new Intent(FormInfoClienteActivity.this, FormCanastillasActivity.class);
        intent.putExtra("nroDocSesion", nroDocSesion);
        intent.putExtra("isPedido", regPedExitoso);
        intent.putExtra("clienteNuevo", isClienteNuevo);
        intent.putExtra("codigoCliente", Main.cliente.codigo);
        startActivityForResult(intent, Const.RESP_CANASTILLA_SIN_VENTA_EXITOSA);
    }

    /**
     * iniciar dialogo de impresion de la sesion del cliente.
     *
     * @param view
     */
    public void OnClickImprimir(View view) {
        if (regPedExitoso) {
            if (DataBaseBO.existeRegCanastilla(nroDocSesion)) {
                impresionPedido();
            } else {
                Util.MostrarAlertDialog(FormInfoClienteActivity.this, "Es necesario ingresar las Canastillas despachadas.");
            }

        } else {
            Util.mostrarToast(this, "No existen facturas por imprimir");
        }
    }

    /**
     * iniciar activity para ingresar l inv de canastas por cliente.
     *
     * @param view
     */
    public void onClickInvCanastas(View view) {
        Intent intent = new Intent(FormInfoClienteActivity.this, FormInvCanastillasActivity.class);
        intent.putExtra("codigoCliente", Main.cliente.codigo);
        intent.putExtra("clienteNuevo", isClienteNuevo);
        startActivity(intent);
    }

    private void impresionPedido() {
        boolean isPedido = true;
        String copiaPrint = "";
        if (primerEjecucion) {
            primerEjecucion = false;
            copiaPrint = "ORIGINAL";
        } else {
            copiaPrint = "COPIA";
        }
        progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
        progressDialog.show();
        SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
        macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");
        SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
        String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");
        if (macImpresora.equals("-")) {
            Util.MostrarAlertDialog(FormInfoClienteActivity.this, "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");
            if (progressDialog != null) progressDialog.cancel();

        } else {
            if (!tipoImpresora.equals("Intermec")) {
                sewooLKP20 = new SewooLKP20(FormInfoClienteActivity.this);
                imprimirSewooLKP20(macImpresora, nroDocSesion, copiaPrint, isPedido);
            } else {
                imprimirTirillaGeneral(macImpresora, nroDocSesion, copiaPrint, isPedido);
            }
        }

    }

    /**
     * Imprimir la factura del pedido.
     *
     * @param macImpresora
     * @param numero_doc
     * @param copiaPrint
     */
    protected void imprimirSewooLKP20(final String macImpresora, final String numero_doc, final String copiaPrint, final boolean isPedido) {
        final Usuario usuario = DataBaseBO.ObtenerUsuario();
        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                if (macImpresora.equals("-")) {
                    if (progressDialog != null) progressDialog.dismiss();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(FormInfoClienteActivity.this, "Aun no hay Impresora Predeterminada.\n\nPor Favor primero Configure la " + "Impresora!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    if (sewooLKP20 == null) {
                        sewooLKP20 = new SewooLKP20(FormInfoClienteActivity.this);
                    }
                    int conect = sewooLKP20.conectarImpresora(macImpresora);
                    switch (conect) {
                        case 1:
                            sewooLKP20.generarEncabezadoTirilla(numero_doc, usuario, copiaPrint, isPedido, false, false, 0, false);
                            if (regDevExitoso)
                                sewooLKP20.generarEncabezadoTirilla(nroDocDevolucion, usuario, copiaPrint, false, false, false, 1, false);
                            sewooLKP20.imprimirBuffer(true);
                            break;
                        case -2:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormInfoClienteActivity.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                        case -8:
                            if (progressDialog != null) progressDialog.dismiss();
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Util.MostrarAlertDialog(FormInfoClienteActivity.this, "Bluetooth apagado. Por favor habilite el bluetoth para " + "imprimir.");

                                }
                            });
                            break;
                        default:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormInfoClienteActivity.this, "Error desconocido, intente nuevamente.", Toast.LENGTH_SHORT).show();

                                }
                            });
                            break;
                    }
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (sewooLKP20 != null) {
                        sewooLKP20.desconectarImpresora();
                        if (progressDialog != null) progressDialog.dismiss();
                    }
                }
                Looper.myLooper().quit();
            }
        }).start();
    }

    private void imprimirTirillaGeneral(final String macAddress, final String numeroDoc, final String copiaPrint, final boolean isPedido) {
        new Thread(new Runnable() {

            public void run() {
                mensaje = "";
                BluetoothSocket socket = null;
                try {
                    Looper.prepare();
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (bluetoothAdapter == null) {
                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else if (!bluetoothAdapter.isEnabled()) {
                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else {
                        BluetoothDevice printer = null;
                        printer = bluetoothAdapter.getRemoteDevice(macAddress);
                        if (printer == null) {
                            mensaje = "No se pudo establecer la conexion con la Impresora.";

                        } else {
                            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                            SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                            String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");
                            if (tipoImpresora.equals("Intermec")) {
                                socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);

                            } else {
                                socket = printer.createRfcommSocketToServiceRecord(uuid);

                            }
                            if (socket != null) {
                                socket.connect();
                                Thread.sleep(3500);
                                if (tipoImpresora.equals("Intermec")) {
                                    ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoVentaPedidoItermerc(numeroDoc, copiaPrint, false, isPedido));
                                    if (regDevExitoso) {
                                        ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoDevolucionItermec(nroDocDevolucion, copiaPrint, false, false));
                                    }
                                }
                                handlerFinish.sendEmptyMessage(0);

                            } else {
                                mensaje = "No se pudo abrir la conexion con la Impresora.\n\nPor Favor intente de nuevo.";
                            }

                        }

                    }
                    if (!mensaje.equals("")) {
                        handlerFinish.sendEmptyMessage(0);
                    }
                    Looper.myLooper().quit();

                } catch (Exception e) {
                    String motivo = e.getMessage();
                    mensaje = "No se pudo ejecutar la Impresion.";
                    if (motivo != null) {
                        mensaje += "\n\n" + motivo;
                    }
                    handlerFinish.sendEmptyMessage(0);

                } finally {
                }
            }

        }).start();

    }

    private Handler handlerFinish = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (progressDialog != null) progressDialog.cancel();
            if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
        
			/*	progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Enviando Informacion Pedido...", true);
        progressDialog.show();
		
				Sync sync = new Sync(FormInfoClienteActivity.this, Const.ENVIAR_PEDIDO);
				sync.start();  */
            }

        }
    };

    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
        switch (codeRequest) {
            case Const.ENVIAR_PEDIDO:
                RespuestaEnviarInfo(ok, respuestaServer, msg);
                break;
        }
    }

    public void RespuestaEnviarInfo(boolean ok, String respuestaServer, String msg) {
        final String mensaje = ok ? "Informacion Registrada con exito en el servidor" : msg;
        if (progressDialog != null) progressDialog.cancel();
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormInfoClienteActivity.this);
                builder.setMessage(mensaje).setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //validarClientesPotenciales();
                        salir();

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });
    }

    private void validarClientesPotenciales() {
        boolean wifi = Util.isWifiOn(this);
        boolean mobnet = Util.isMobileNetworkOn(this);
        if (!wifi && !mobnet) {
            AlertDialog.Builder builder = new AlertDialog.Builder(FormInfoClienteActivity.this);
            builder.setMessage("Por favor encienda los medios de conexion a internet para continuar.").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    validarClientesPotenciales();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            boolean estadoGPS = false;
            estadoGPS = Util.gpsIsOn(this);
            if (estadoGPS) {
                progressDialog = ProgressDialog.show(this, "", "Por favor espere...\n\nValidando clientes potenciales...", true);
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            synchronized (this) {
                                wait(2000);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (cargarInformacion()) {
                                            handlerFinishP.sendEmptyMessage(0);
                                        } else {
                                            handlerErrorP.sendEmptyMessage(0);
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormInfoClienteActivity.this);
                builder.setMessage("EL GPS ESTA DESACTIVADO, DEBE ACTIVARLO PARA CONTINUAR.").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        validarClientesPotenciales();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }

    }

    private Handler handlerFinishP = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (progressDialog != null) progressDialog.cancel();
            mostrarDialogoClientesPotenciales();
        }
    };
    private Handler handlerErrorP = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (progressDialog != null) progressDialog.cancel();
            Util.MostrarAlertDialog(FormInfoClienteActivity.this, "No se encontraron clientes potenciales disponibles.");
        }
    };

    public boolean cargarInformacion() {
        boolean estado = false;
        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado == 1) {
            cliente = DataBaseBO.CargarClienteSeleccionado();
        } else {
            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
        }
        listaClientes = DataBaseBO.buscarClientesPotenciales();
        listarClientesPotenciales();
        if (listaClientesPotenciales.size() > 0) {
            estado = true;
        } else {
            estado = false;
        }
        return estado;
    }

    public void listarClientesPotenciales() {
        listaClientesPotenciales = new Vector<Cliente>();
        for (Cliente clientepotencial : listaClientes) {
            Log.e(TAG, " clientepotencial.latitud -> " + clientepotencial.latitud);
            Log.e(TAG, " clientepotencial.longitud -> " + clientepotencial.longitud);
            RangoCliente rangoCliente = validarRangoClientePotencial(clientepotencial);
            if (rangoCliente != null) {
                if (rangoCliente.estado) {
                    Cliente clientepotencialIn = new Cliente();
                    clientepotencialIn.codigo = clientepotencial.codigo;
                    clientepotencialIn.cedula = clientepotencial.cedula;
                    clientepotencialIn.razonSocial = clientepotencial.razonSocial;
                    clientepotencialIn.Nombre = clientepotencial.Nombre;
                    clientepotencialIn.Ciudad = clientepotencial.Ciudad;
                    clientepotencialIn.barrio = clientepotencial.barrio;
                    clientepotencialIn.direccion = clientepotencial.direccion;
                    clientepotencialIn.latitud = clientepotencial.latitud;
                    clientepotencialIn.longitud = clientepotencial.longitud;
                    clientepotencialIn.diff = rangoCliente.diferencia;
                    Log.e(TAG, "  clientepotencialIn.Nombre-> " + clientepotencialIn.Nombre + " diff " + clientepotencialIn.diff);
                    listaClientesPotenciales.addElement(clientepotencialIn);
                }
            }
        }
    }

    public RangoCliente validarRangoClientePotencial(Cliente clientePotencial) {
        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado == 1) {
            cliente = DataBaseBO.CargarClienteSeleccionado();
        } else {
            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
        }
        RangoCliente rangoCliente = null;
        GPSTracker gpsTracker = new GPSTracker(this);
        Location locationActual = null;
        if (gpsTracker != null) {
            locationActual = gpsTracker.getLocation();
        }
        double diff = -1;
        if (locationActual != null) {
            Location location = new Location("pointcurrent");
            location.setLatitude(cliente.latitud);
            location.setLongitude(cliente.longitud);
            diff = Util.validarDiferenciaRango(location, clientePotencial);
            double distMax = DataBaseBO.obtenerDistanciaMaximaClientesPotenciales();
            if (diff <= distMax) {
                rangoCliente = new RangoCliente();
                rangoCliente.diferencia = diff;
                rangoCliente.estado = true;
            }
        }
        return rangoCliente;
    }

    public void mostrarDialogoClientesPotenciales() {
        Log.e(TAG, " mostrarDialogoClientesPotenciales -> ");

        try {
            if (dialogoClientesPotenciales == null) {
                dialogoClientesPotenciales = new Dialog(this);
                dialogoClientesPotenciales.setContentView(R.layout.dialog_clientes_potenciales);
                dialogoClientesPotenciales.setTitle("Clientes Potenciales");
            }
            ((Button) dialogoClientesPotenciales.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    dialogoClientesPotenciales.cancel();
                }
            });
            ((Button) dialogoClientesPotenciales.findViewById(R.id.btnCancelar)).setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    dialogoClientesPotenciales.cancel();
                }
            });
            Log.e(TAG, " listaClientesPotenciales -> " + listaClientesPotenciales.size());
            if (listaClientesPotenciales != null) {
                if (listaClientesPotenciales.size() > 0) {
                    ItemListView[] listaItems = null;
                    listaItems = new ItemListView[listaClientesPotenciales.size()];
                    for (int i = 0; i < listaClientesPotenciales.size(); i++) {
                        Cliente clie = listaClientesPotenciales.get(i);
                        Log.e(TAG, "  clie.Nombre-> " + clie.Nombre + " diff " + Util.Redondear(clie.diff + "", 1));

                        ItemListView itemListView = new ItemListView();
                        itemListView.titulo = clie.codigo + " - " + clie.Nombre;
                        itemListView.subTitulo = clie.direccion + "\nDistancia: " + Util.Redondear(clie.diff + "", 1) + " metros";
                        listaItems[i] = itemListView;

                        if (listaclientesstr.equals("")) {
                            listaclientesstr += clie.codigo + ":" + Util.Redondear(clie.diff + "", 1);
                        } else {
                            listaclientesstr += "@" + clie.codigo + ":" + Util.Redondear(clie.diff + "", 1);
                        }

                    }

                    ListViewAdapterRemarca adapter = new ListViewAdapterRemarca(this, listaItems, R.drawable.drw_op_cliente, 0x2E65AD);
                    ListView lvwClientes = (ListView) dialogoClientesPotenciales.findViewById(R.id.lvwClientes);
                    lvwClientes.setAdapter(adapter);
                    lvwClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                            Cliente clienteSel = listaClientesPotenciales.elementAt(position);
                            if (clienteSel != null) {

                                Util.clearPrefence(FormInfoClienteActivity.this, Const.PREFERENCELISTA);
                                String keysre[] = {"LISTACLIENTES"};
                                String datosre[] = {listaclientesstr};
                                Util.putPrefence(FormInfoClienteActivity.this, Const.PREFERENCELISTA, keysre, datosre);


                                Intent intentHome = new Intent(FormInfoClienteActivity.this, MapsActivityClientesPotenciales.class);
                                intentHome.putExtra("clienteSel", clienteSel);
                                intentHome.putExtra("listaClientesPotenciales", listaclientesstr);
                                startActivity(intentHome);

                            }
                        }
                    });
                } else {
                    ListViewAdapterRemarca adapter = new ListViewAdapterRemarca(this, new ItemListView[]{}, R.drawable.cliente, 0x2E65AD);
                    ListView lvwClientes = (ListView) dialogoClientesPotenciales.findViewById(R.id.lvwClientes);
                    lvwClientes.setAdapter(adapter);
                    if (listaClientes != null) listaClientesPotenciales.removeAllElements();
                    Toast.makeText(getApplicationContext(), "Busqueda sin resultados", Toast.LENGTH_SHORT).show();
                }
                dialogoClientesPotenciales.setCancelable(false);
                dialogoClientesPotenciales.show();
            }

        } catch (Exception e) {
            Log.e(TAG, " mostrarDialogoClientesPotenciales -> " + e.getMessage());
        }
    }


    private class Task extends AsyncTask<String, Integer, Long> {

        protected Long doInBackground(String... urls) {
            try {
                Thread.sleep(300);
            } catch (Exception e) {
            }
            FormInfoClienteActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (DataBaseBO.CargarTotalPedidosSinEnviar()) {
                        Util.mostrarDialogGeneral(FormInfoClienteActivity.this, "ATENCION", "Existen 5 o mas pedidos por enviar.\nSe recomienda " + "enviar informacion.");
                    }
                }
            });
            return 0l;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
            FormInfoClienteActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (progressDialog != null) progressDialog.dismiss();

                }
            });

        }
    }
    //COORDENADAS NUEVA


    /**
     * Coordenadas
     ***/
    private class GPSListener implements LocationListener {

        public void onLocationChanged(Location location) {
            if (location != null) {
                location.getLatitude();
                location.getLongitude();
                currentLocation = location;
                handler.sendEmptyMessage(0);
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    /*private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (locationManager != null && gpsListener != null) {
                locationManager.removeUpdates(gpsListener);
            }
            if (currentLocation != null && guardar == 0) {
                DataBaseBO.validarUsuario();
                if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
                    Coordenada coordenada = new Coordenada();
                    coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                    coordenada.codigoCliente = Main.cliente.codigo;
                    coordenada.latitud = currentLocation.getLatitude();
                    coordenada.longitud = currentLocation.getLongitude();
                    coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                    coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
                    coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                    latitud = currentLocation.getLatitude();
                    longitud = currentLocation.getLongitude();
                    Log.i("COORDENADA CLIENTE", currentLocation.getLatitude() + "--" + currentLocation.getLongitude());
                    //progressDialog2.cancel();
                    boolean guardo = Coordenada.save(FormInfoClienteActivity.this, coordenada);
                    if (guardo) {
                        Log.i("GUARDO", "OK");
                        guardar = -1;
                    }
                }
            }
        }
    };*/

    public static boolean estaGPSEncendido(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return statusOfGPS;
    }

    public boolean isProviderEnabled(String provider) {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager != null) {
            return manager.isProviderEnabled(provider);
        }
        return false;
    }

    public void capturarCoordenada() {
        Coordenada.delete(this);
        EncenderServGps();
        progressCoordenada();
        if (isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            //Verifica si NETWORK_PROVIDER esta Activo. En caso tal Registra el
            // Listener de Captura de Coordenadas con NETWORK_PROVIDER
            registrarListenerGPS(LocationManager.NETWORK_PROVIDER);

        } else if (isProviderEnabled(LocationManager.GPS_PROVIDER)) { //
            // Verifica si GPS_PROVIDER esta Activo. En caso tal Registra el
            // Listener de Captura de Coordenadas con GPS_PROVIDER
            registrarListenerGPS(LocationManager.GPS_PROVIDER);

        } else {
            /**
             * Se guarda seguimiento indicando que el GPS esta apagado!
             **/
            DataBaseBO.validarUsuario();
            if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
                Coordenada coordenada = new Coordenada();
                coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                coordenada.codigoCliente = Main.cliente.codigo;
                coordenada.latitud = currentLocation.getLatitude();
                coordenada.longitud = currentLocation.getLongitude();
                coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                coordenada.estado = Coordenada.ESTADO_GPS_APAGADO;
                coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                Coordenada.save(this, coordenada);
            }
        }
    }


    protected void EncenderServGps() {
        // Se valida si se puede Activar el GPS
        boolean encender = validarDisponibilidad();
        if (encender) {
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (provider != null && !provider.contains("gps")) {
                final Intent intent = new Intent();
                intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
                intent.setData(Uri.parse("3"));
                sendBroadcast(intent);
            }
        }
    }

    private boolean validarDisponibilidad() {
        PackageManager packageManager = getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);

        } catch (PackageManager.NameNotFoundException e) {
            // Paquete no encontrado
            return false;
        }
        if (packageInfo != null) {
            for (ActivityInfo actInfo : packageInfo.receivers) {
                String name = actInfo.name;
                boolean exported = actInfo.exported;
                // Verifica si el receiver es exported. En caso tal se puede
                // actiar el GPS.
                if (name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && exported) {
                    return true;
                }
            }
        }
        return false;
    }

    public void registrarListenerGPS(String provider) {
        try {
            setGPSProvider(provider);
            //				String provider = getGPSProvider();
            if (provider != null && !provider.equals("")) {
                if (locationManager == null)
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (gpsListener == null) gpsListener = new GPSListener();
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(provider, 0, 0, gpsListener);

            } else {
                Log.e("****", "registrarListenerGPS -> No se encontro el Provider");
            }

        } catch (Exception e) {
            Log.e("***", "registrarListenerGPS -> " + e.getMessage(), e);
        }
    }

    public void setGPSProvider(String provider) {
        SharedPreferences settings = getSharedPreferences("settings_gps", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("provider", provider);
        editor.commit();
    }

    /**
     *
     **/


    public void onClickInvCliente(View view) {

        /* Evitar evento de doble click */
        if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Cargando Inv...", true);
        progressDialog.show();
        CargarProductos3 cp = new CargarProductos3();
        cp.start();

    }


    /**
     * Cargar productos para modulo de inventario.
     */
    public void cargarProductos3() {
        Main.encabezado.codigo_novedad = 0;
        Main.encabezado.tipoTrans = 4;
        Main.itemsBusq = new ItemListView[]{};
        Main.codBusqProductos = "";
        Main.posOpBusqProductos = 0;
        Main.posOpLineas = 0;
        Main.primeraVez = true;
        tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
        if (tipoDeClienteSeleccionado == 1) {
            cliente = DataBaseBO.CargarClienteSeleccionado();
        } else {
            cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
        }
        Main.cliente = cliente;
        if (Main.cliente.bodega == null)
            Main.cliente = DataBaseBO.CargarCliente(Main.cliente.codigo);
        if (Main.usuario == null) DataBaseBO.CargarInfomacionUsuario();
        Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
        DataBaseBO.organizarTmpDescuentos();
        //		DataBaseBO.organizarTmpProductos( cliente.portafolio);
        if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            DataBaseBO.organizarTmpProductos(cliente.CodigoAmarre, Const.AUTOVENTA);
        } else {
            DataBaseBO.organizarTmpProductos(cliente.CodigoAmarre, Const.PREVENTA);
        }
        DataBaseBO.ListarProductos(cliente.CodigoAmarre);
        if (progressDialog != null) progressDialog.dismiss();
        Intent formInv = new Intent(this, FormInventarioClienteActivity.class);
        formInv.putExtra("inventario", true);
        formInv.putExtra("pedidoAmarre", pedidoAmarre);
        startActivityForResult(formInv, Const.RESP_DEVOLUCION_EXITOSA);
    }

    public void onClickPqrCliente(View view) {
        Intent formPqr = new Intent(this, FormPqrActivity.class);
        startActivity(formPqr);
    }

    public void onClickFormPlanogramaActivity(View view) {
        Intent formPlanogramaActivity = new Intent(this, FormPlanogramaActivity.class);
        startActivity(formPlanogramaActivity);
    }

    public void onClickForms(View view) {

        /*retardo de 2000ms para evitar eventos de doble click.
         * esto significa que solo se puede hacer click cada 2000ms.
         * es decir despues de presionar el boton, se debe esperar que transcurran
         * 1500ms para que se capture un nuevo evento.*/
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();//inicializa esta variable en 0

        Intent intent = new Intent(getApplicationContext(), FormEncuestasActivity.class);
        startActivity(intent);
    }

    public void onClickChequeoPrecios(View view) {

        /*retardo de 2000ms para evitar eventos de doble click.
         * esto significa que solo se puede hacer click cada 2000ms.
         * es decir despues de presionar el boton, se debe esperar que transcurran
         * 1500ms para que se capture un nuevo evento.*/
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();//inicializa esta variable en 0

        Intent intent = new Intent(getApplicationContext(), FormChequeoPrecios.class);
        startActivity(intent);
    }


}//final de la clase.


