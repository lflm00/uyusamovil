package celuweb.com.uyusa;

public class Const {

    public static final String URL_IMGS = "http://66.33.94.203/LAREPUESTERA/Sync//catalogo/";

    public final static String TITULO;
    public final static String URL_SYNC;
    public final static String URL_DOWNLOAD_NEW_VERSION;

    private final static int PRUEBAS = 1;
    private final static int PRODUCCION = 2;

    public final static int timeWait = 5 * 1000;

    /**
     * IMPORTANTE: Indica que aplicacion se va a generar
     **/
    private final static int APLICACION = PRODUCCION;

    static {

        switch (APLICACION) {

            case PRUEBAS:

                TITULO = " - Pruebas";
                URL_SYNC = "http://66.33.94.203/UYUSA/Sync/";
                URL_DOWNLOAD_NEW_VERSION = "http://64.239.115.11/Sync/App/uyusa.apk";
                break;

            case PRODUCCION:

                TITULO = " - Produccion";
                URL_SYNC = "http://66.33.94.203/UYUSA/Sync/";
                URL_DOWNLOAD_NEW_VERSION = "http://64.239.115.11/Sync/App/uyusa.apk";
                break;

            default:
                TITULO = " - Sin Definir";
                URL_SYNC = "Sin_Definir";
                URL_DOWNLOAD_NEW_VERSION = "Sin_Definir";
                break;

        }

    }


    /**
     * DEFINCION DE LAS CONSTANTES DE LA APLICACION
     **/
    public final static int PEDIDO_VENTA = 1;

    public final static int RESP_CERRAR_SESION = 1000;

    public final static int RESP_PEDIDO_EXITOSO = 1001;

    public final static int RESP_NO_COMPRA_EXITOSO = 1002;

    public final static int RESP_BUSQUEDA_PRODUCTO = 1003;

    public final static int RESP_FORM_RESUMEN = 1004;

    public final static int RESP_FROM_AGREGAR_CARTERA = 1005;

    public final static int RESP_FORM_OPCIONES_PAGO = 1006;

    public final static int RESP_RECAUDO_EXITOSO = 1007;

    public final static int RESP_ULTIMO_PEDIDO = 1008;

    public final static int RESP_FORM_INICIAR_DIA = 1009;

    public final static int RESP_ACTUALIZAR_VERSION = 1010;

    public final static int RESP_TOMAR_FOTO = 1011;

    public final static int RESP_FORMAS_DE_PAGO = 1012;

    public final static int RESP_EDITAR_CLIENTE = 1013;

    public final static int RESP_CLIENTE_NUEVO = 1014;

    public final static int RESP_FORM_LOGIN = 1015;

    public final static int RESP_DESC_AUTORIZADOS = 1016;

    public final static int RESP_FORM_DETALLE_GASTOS = 1017;

    public final static int RESP_FORM_DETALLE_TIEMPOS = 1018;

    public final static int RESP_FORM_PREGUNTAS = 1019;

    public final static int REQUEST_DISCOVERABLE_CODE = 1016;

    public final static int RESP_FILTRO_PRODUCTO = 1020;

    public final static int RESP_CODIGOBARRAS = 1000;

    public final static int RESP_ELIMINAR_REGISTRO_RECAUDO = 1029;

    public final static int RESP_DEVOLUCION_EXITOSA = 1030;

    public final static int RESP_CANASTILLA_SIN_VENTA_EXITOSA = 1031;

    public final static int RESP_LIQUID_EXITOSO = 1032;

    public final static int RESP_LEER_CEDULA = 9875432;

    public final static int LOGIN = 1;
    public final static int LOGOUT = 2;
    public final static int DOWNLOAD_DATA_BASE = 3;
    public final static int ENVIAR_PEDIDO = 4;
    public final static int ENVIAR_PEDIDO_TERMINAR = 5;
    public final static int DOWNLOAD_VERSION_APP = 6;
    public final static int TERMINAR_LABORES = 7;
    public final static int VERIFICAR_CLIENTE = 8;
    public final static int ENVIAR_COORDENADAS = 11;
    public final static int ACTUALIZAR_INVENTARIO = 12;

    public final static String CLIENTE_VIEJO = "V";

    public final static String TODOS = "Todos";

    public final static String POR_NOMBRE = "Parte Nombre";

    public final static String POR_CODIGO = "Parte Codigo";

    public final static String POR_RAZON_SOCIAL = "Parte Razon Social";

    public final static int MAX_ITEMS = 33;

    public final static int LUNES = 0; // Calendar.MONDAY (2)
    public final static int MARTES = 1; // Calendar.TUESDAY (3)
    public final static int MIERCOLES = 2; // Calendar.WEDNESDAY (4)
    public final static int JUEVES = 3; // Calendar.THURSDAY (5)
    public final static int VIERNES = 4; // Calendar.FRIDAY (6)
    public final static int SABADO = 5; // Calendar.SATURDAY (7)
    public final static int DOMINGO = 6; // Calendar.SUNDAY (1)

    public final static String nameDirApp = "uyusa";
    public final static String fileNameApk = "uyusa.apk";
    public final static String EFECTIVO = "EF";
    public final static String CREDITO = "CR";

    public final static int BUTTON = 1;
    public final static int TEXTVIEW = 2;

    public final static String SETTINGS_RECAUDO = "settings_recaudo";
    public final static String CONSECUTIVO_RECAUDO = "consecutivo_recaudo";

    public final static String SETTINGS_GPS = "settings_gps";
    public final static String PROVIDER = "provider";

    public final static String CONFIG_IMPRESORA = "PRINTER";
    public final static String MAC_IMPRESORA = "MAC";
    public final static String LABEL_IMPRESORA = "LABEL";
    public final static String TIPO_IMPRESORA = "TIPO";

    public static final int OP_PEDIDO = 0;
    public static final int OP_CAMBIO = 1;
    public static final int OP_DEVOLUCION = 2;

    public static final int ENABLE_GPS = 1100;

    public static final String AUTOVENTA = "A";
    public static final String PREVENTA = "1";
    public static final boolean IS_AUTOVENTA = true;
    public static final boolean IS_PREVENTA = false;

    public static final int RUTERO = 1;
    public static final int EXTRARUTA = 0;


    public static final int SIN_LIQUIDACION = 0;
    public static final int CANASTILLA_VACIA = 1;
    public static final int INVENTARIO_CANASTA = 2;
    public static final int DESCARGA_CANASTA = 7;
    public static final int PRODUCTO_DANADO = 4;
    public static final int AVERIA_TRANSPORTE = 5;
    public static final int INF_INVENTARIO = 6;

    //TipoTrans Liquidacion Canastillas
    public static final int TIPO_TRANS_CANASTILLA_VACIA = 2;
    public static final int TIPO_TRANS_INVENTARIO_CANASTA = 4;
    public static final int TIPO_TRANS_ESCARGA_CANASTA = 10;

    //TipoTrans sugeridos
    public static final int TIPO_TRANS_CARGUE = 3;
    public static final int TIPO_TRANS_AVERIA_TRANSPORTE = 8;
    public static final int TIPO_TRANS_INVENTARIO_LIQUIDACION = 5;
    public static final int TIPO_TRANS_PRODUCTO_DANADO = 4;
    public static final int TIPO_TRANS_INVENTARIO_COMPENSACION = 6;

    // TipoTransaccion
    public static final int IS_AVERIA_TRANSPORTE = 2;
    public static final int IS_INVENTARIO_LIQUIDACION = 3;
    public static final int IS_PRODUCTO_DANADO = 1;
    public static final int IS_DESCARGA_CANASTA = 7;
    public static final int IS_SOBRANTE = 100;

    public final static int RESP_PERMISOS_STORAGE = 2003;
    public final static int RESP_PERMISOS_PHONE = 2004;
    public final static int RESP_PERMISOS_LOCATION = 2005;
    public final static int RESP_FINALIZA_PEDIDO = 2008;
    public final static int RESP_PERMISOS_CAMERA = 2006;
    public final static int RESP_PERMISOS_CALL_PHONE = 2007;

    public static final int SIZEICON1 = 20;
    public static final int SIZEICON2 = 24;
    public static final int SIZEICON3 = 30;

    public static final int ID_COLUMN = 1;
    public static final int ID_COLUMN_TWO = 2;

    public static final String MODULO_REGISTRO_VISITA = "AREGV";
    public final static double DISTANCIA_MAX = 50;
    public final static double RANGO_MAX = 100;
    public final static String PREFERENCERE = "RUTERO";
    public final static String PREFERENCERENUMDOCREGVISITA = "REGVISITA";
    public static final int RUTA_VISITA = 1;
    public static final int RUTA_EXTRA_RUTA = 2;
    public static final String FORMATO_FECHA = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMATO_FECHA_ID = "yyyyMMddHHmmssSSS";

    public final static int DESCARGAR_ARCHIVO_PDF = 9989;
    public static final String PAQUETES_POLARIS = "com.infraware.office.link&hl=es";
    public static final String FILE_NAME = "planograma.pdf";
    public static final String DIR_NAME = "downloadfiles";
    public static final String RUTA_ARCHIVOS = "/" + DIR_NAME + "/";
    public final static String PREFERENCEFIRMA = "FIRMA";
    public final static String PREFERENCELISTA = "LISTA";
    public final static int RESP_FIRMA = 1020;
    public static final int WIDHTFOTO = 1000;
    public static final int HEIGHTFOTO = 800;

    //public static final AssetManager am = Main.contexto.getAssets();
    //public static final Typeface letraNormal = Typeface.createFromAsset(am, String.format(Locale.US, "fonts/%s", "Roboto-Light.ttf"));

}