package celuweb.com.uyusa;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import celuweb.com.DataObject.ItemListView;

public class ListAdapter extends ArrayAdapter<ItemListView> {
	
	int colorTitulo;
	int layout_list_item;
	
	Activity context;
	ItemListView[] listItems;
	
	ListAdapter(Activity context, ItemListView[] listItems, int colorTitulo) {
		
		super(context, R.layout.item, listItems);
		
		this.layout_list_item = R.layout.list_item;
		this.listItems = listItems; 
		this.context = context;
			
		this.colorTitulo = colorTitulo; 
	}
	
	ListAdapter(Activity context, ItemListView[] listItems, int colorTitulo, int layout_list_item) {
		
		super(context, layout_list_item, listItems);
		
		this.layout_list_item = layout_list_item;
		this.listItems = listItems; 
		this.context = context;
			
		this.colorTitulo = colorTitulo; 
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = context.getLayoutInflater();
		View item = inflater.inflate(layout_list_item, null);
		
		TextView lblTitulo = (TextView)item.findViewById(R.id.lblTitulo);
		lblTitulo.setText(listItems[position].titulo);
		
		TextView lblSubtitulo = (TextView)item.findViewById(R.id.lblSubTitulo);
		lblSubtitulo.setText(listItems[position].subTitulo);
		
		return item;
    }
	
	@Override
	public ItemListView getItem(int position) {
		
		return listItems[position];
	}
}
