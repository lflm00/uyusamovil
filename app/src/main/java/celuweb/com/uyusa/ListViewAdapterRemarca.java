package celuweb.com.uyusa;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import celuweb.com.DataObject.ItemListView;

public class ListViewAdapterRemarca extends ArrayAdapter<ItemListView> {
  
  int icono;
  int colorTitulo;
  Activity context;
  ItemListView[] listItems;
  //	ArrayList<ItemListView> listItems;
  
  ListViewAdapterRemarca(Activity context, ItemListView[] listItems, int icono, int colorTitulo) {
    super(context, R.layout.list_item_card, listItems);
    this.listItems = listItems;
    this.context = context;
    this.icono = icono;
    this.colorTitulo = colorTitulo;
  }
  
  @Override
  public int getCount() {
    return listItems.length;
  }
  
  @Override
  public ItemListView getItem(int position) {
    return listItems[position];
  }
  
  @Override
  public long getItemId(int position) {
    return position;
  }
  
  /**
   metodo implementado con optimizacion de recursos.
   evita en gran medida errores relacionados con recursos de memoria, porque evita crear objetos por cada vez que se mueva el scroll.
   @param position
   @param convertView
   @param parent
   @return
   @by JICZ
   */
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      LayoutInflater inflater = context.getLayoutInflater();
      convertView = inflater.inflate(R.layout.list_item_card, null);
      Row holder = new Row();
      holder.tituloItem = (TextView) convertView.findViewById(R.id.lblTitulo);
      holder.subtitulo = (TextView) convertView.findViewById(R.id.lblSubTitulo);
      holder.icono = (ImageView) convertView.findViewById(R.id.iconListView);
      convertView.setTag(holder);

    }
    //llenar datos
    Row holder = (Row) convertView.getTag();
    holder.tituloItem.setText(listItems[position].titulo);
    holder.subtitulo.setText(listItems[position].subTitulo);

    if (icono > 0) {
      holder.icono.setImageResource(icono);
    }

    if (listItems[position].isProspecto.equals("1")){
      holder.tituloItem.setTextColor(context.getResources().getColor(R.color.dark_red));
      holder.subtitulo.setTextColor(context.getResources().getColor(R.color.dark_red));
    }else{
      holder.tituloItem.setTextColor(context.getResources().getColor(R.color.blue_semilight));
      holder.subtitulo.setTextColor(context.getResources().getColor(R.color.blue_semilight));
    }

    return (convertView);
  }
  
  /**
   representa una fila, (ViewHolder).
   @author JICZ
   */
  public static class Row {
    TextView tituloItem;
    TextView subtitulo;
    ImageView icono;
  }
  
}
