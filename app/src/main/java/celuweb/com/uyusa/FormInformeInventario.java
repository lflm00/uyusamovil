package celuweb.com.uyusa;

import java.util.ArrayList;
import java.util.UUID;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.DataObject.InformeInventario;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.adapter.LisviewMostrarProductosInventario;
import celuweb.com.printer.sewoo.SewooLKP20;

@SuppressLint("NewApi")
public class FormInformeInventario extends CustomActivity {

	Dialog dialogInventario;
	Vector<InformeInventario> listaInformeInv;
	ProgressDialog progressDialog;
	private String macImpresora;
	String mensaje;
	boolean isLiquidacion = false;
	private static Activity context;
	private ListView lvSeleccione;
	private EditText textvalorBuscar;
	private long mLastClickTime = 0;
	int posicionActual = 0;
	/**
	 * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
	 */
	private SewooLKP20 sewooLKP20;



	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_productos);
		
		cargarBundle();
		
		context = FormInformeInventario.this;
		lvSeleccione = (ListView) findViewById(R.id.lvSeleccione);
		textvalorBuscar = (EditText) findViewById(R.id.textvalorBuscar);

		progressDialog = ProgressDialog.show(FormInformeInventario.this, "", "Cargando Informacion...", true);
		progressDialog.show();

		//new Thread() {

		//	public void run() {

				//CargarInformeInv();
				cargarInformeInventarioLisView("");
		//	}
		//}.start();

		textvalorBuscar.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				String buscarProducto = textvalorBuscar.getText().toString().trim();
				if(buscarProducto.length()>0){
					cargarInformeInventarioLisView(textvalorBuscar.getText().toString().trim());
				}else{
					cargarInformeInventarioLisView("");
				}
			}
		});


	}




	@Override
	protected void onResume() {
		cargarBundle();
		super.onResume();
	}





	private void cargarBundle() {
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null && bundle.containsKey("isLiquidacion"))
			isLiquidacion = (bundle.getBoolean("isLiquidacion"));
		
		Button btnAceptar = (Button)findViewById(R.id.btnAceptar);
		Button btnImprimir= (Button)findViewById(R.id.btnImprimir);
		
		btnAceptar.setOnClickListener(onClickButton);
		btnImprimir.setOnClickListener(onClickButton);
		
		if(isLiquidacion){
			btnAceptar.setVisibility(View.VISIBLE);
			btnImprimir.setVisibility(View.GONE);
		}else {
			btnImprimir.setVisibility(View.VISIBLE);
			btnAceptar.setVisibility(View.GONE);
		}
	}
	
	View.OnClickListener onClickButton = new View.OnClickListener() {
		  public void onClick(View v) {
		      switch(v.getId()) {
			case R.id.btnAceptar:
				finish();
				break;
			case R.id.btnImprimir:
				System.out.println("Entro a imprimir Informe.");
				impresionPedido();
				break;
			default:
				System.out.println("No toco a ningun boton.");
				break;
		      }
		  }
		};

	public void buscarProducto(View view){
		String buscarProducto = textvalorBuscar.getText().toString().trim();
		cargarInformeInventarioLisView(buscarProducto);
	}


	public void cargarInformeInventarioLisView(String buscarProducto){
		ItemListView[] items;
		Vector<ItemListView> listaItems = new Vector<ItemListView>();
		//listaSucursal = DataBaseBO.listaClientesSucursal(listaItems, codigo);
		listaInformeInv = DataBaseBO.CargarInformeInventarioLisView(listaItems ,isLiquidacion, buscarProducto);

		if (listaItems.size() > 0) {

			items = new ItemListView[listaItems.size()];
			listaItems.copyInto(items);

		} else {

			items = new ItemListView[]{};
		}

		LisviewMostrarProductosInventario adapter = new LisviewMostrarProductosInventario(this, items);
		lvSeleccione.setAdapter(adapter);

		lvSeleccione.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				InformeInventario informeInventario = listaInformeInv.elementAt(position);
				DialogDetalleInform dialogDetalleInform=new DialogDetalleInform( FormInformeInventario.this,informeInventario );
				dialogDetalleInform.setCancelable( false );
				dialogDetalleInform.requestWindowFeature( Window.FEATURE_NO_TITLE );
				dialogDetalleInform.show();

			}
		});

		if (progressDialog != null)
			progressDialog.dismiss();

	}
/**
	public void CargarInformeInv() {

		listaInformeInv = DataBaseBO.CargarInformeInventario(isLiquidacion);

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				TableLayout table = new TableLayout(FormInformeInventario.this);
				table.setBackgroundColor(Color.WHITE);

				HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollInventario);
				scroll.removeAllViews();
				scroll.addView(table);

				if (listaInformeInv.size() > 0) {

					
					if(isLiquidacion){
						String[] cabecera = { "CODIGO", "NOMBRE", "II", "V", "M" , "IF"};
						Util.Headers(table, cabecera, FormInformeInventario.this);
					}else{
						String[] cabecera = { "CODIGO", "NOMBRE", "II", "V" };
						Util.Headers(table, cabecera, FormInformeInventario.this);
						}
					

					TextView textViewAux;

					for (InformeInventario informeInv : listaInformeInv) {

						TableRow fila = new TableRow(FormInformeInventario.this);

						textViewAux = new TextView(FormInformeInventario.this);
						textViewAux.setText(informeInv.codigo);
						textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
						textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
						textViewAux.setTextSize(17);
						textViewAux
								.setBackgroundDrawable(FormInformeInventario.this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
						fila.addView(textViewAux);

						textViewAux = new TextView(FormInformeInventario.this);
						textViewAux.setText("" + informeInv.nombre);
						textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
						textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
						textViewAux.setTextSize(17);
						textViewAux
								.setBackgroundDrawable(FormInformeInventario.this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
						fila.addView(textViewAux);

						textViewAux = new TextView(FormInformeInventario.this);
						textViewAux.setText("" + informeInv.invInicial);
						textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
						textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
						textViewAux.setTextSize(18);
						textViewAux
								.setBackgroundDrawable(FormInformeInventario.this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
						fila.addView(textViewAux);

						textViewAux = new TextView(FormInformeInventario.this);
						textViewAux.setText("" + informeInv.cantVentas);
						textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
						textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
						textViewAux.setTextSize(17);
						textViewAux
								.setBackgroundDrawable(FormInformeInventario.this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
						fila.addView(textViewAux);
						
						if(isLiquidacion){
							
							
							
							textViewAux = new TextView(FormInformeInventario.this);
							textViewAux.setText("" + (informeInv.sobrante));
							textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
							textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
							textViewAux.setTextSize(17);
							textViewAux
									.setBackgroundDrawable(FormInformeInventario.this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
							fila.addView(textViewAux);
							
							
							textViewAux = new TextView(FormInformeInventario.this);
							//textViewAux.setText("" + (informeInv.invInicial - informeInv.cantVentas));
							textViewAux.setText("" + (informeInv.invActual));
							textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
							textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
							textViewAux.setTextSize(17);
							textViewAux
									.setBackgroundDrawable(FormInformeInventario.this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
							fila.addView(textViewAux);
							
							
						}

						table.addView(fila);
					}

				} else {

					Toast.makeText(getApplicationContext(), "Busqueda sin resultados", Toast.LENGTH_SHORT).show();
				}

				if (progressDialog != null)
					progressDialog.dismiss();

			}
		});

	}
*/


	
	private void impresionPedido() {
		
		progressDialog = ProgressDialog.show(FormInformeInventario.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
		progressDialog.show();
		
		
		SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
		macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

		SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
		String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");

		if (macImpresora.equals("-")) {
			
			Util.MostrarAlertDialog(FormInformeInventario.this, "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");
			if (progressDialog != null)
				progressDialog.cancel();
			
			
		} else {
			if (!tipoImpresora.equals("Intermec")) {
				sewooLKP20 = new SewooLKP20(FormInformeInventario.this);
				imprimirSewooLKP20(macImpresora);
			} else {
				imprimirTirillaGeneral(macImpresora);
			}
		}
		
	}


	private void imprimirTirillaGeneral(final String macAddress) {

		new Thread(new Runnable() {

			public void run() {

				mensaje = "";
				BluetoothSocket socket = null;

				try {

					Looper.prepare();

					BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

					if (bluetoothAdapter == null) {

						mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

					} else if (!bluetoothAdapter.isEnabled()) {

						mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

					} else {

						BluetoothDevice printer = null;

						printer = bluetoothAdapter.getRemoteDevice(macAddress);

						if (printer == null) {

							mensaje = "No se pudo establecer la conexion con la Impresora.";

						} else {

							UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

							SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
							String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");

							if (tipoImpresora.equals("Intermec")) {

								socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);
							} else {
								socket = printer.createRfcommSocketToServiceRecord(uuid);
							}

							if (socket != null) {

								socket.connect();

								Thread.sleep(3500);

								if (tipoImpresora.equals("Intermec")) {

									ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoinventarioDiaIntermec(false));

								} else {
									imprimirSewooLKP20(macImpresora);
								}


								handlerFinish.sendEmptyMessage(0);

							} else {

								mensaje = "No se pudo abrir la conexion con la Impresora.\n\nPor Favor intente de nuevo.";
							}

						}
					}

					if (!mensaje.equals("")) {

						handlerFinish.sendEmptyMessage(0);
					}

					Looper.myLooper().quit();

				} catch (Exception e) {

					String motivo = e.getMessage();

					mensaje = "No se pudo ejecutar la Impresion.";

					if (motivo != null) {
						mensaje += "\n\n" + motivo;
					}

					handlerFinish.sendEmptyMessage(0);

				} finally {

				}
			}

		}).start();

	}

	private Handler handlerFinish = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			if (progressDialog != null)
				progressDialog.cancel();
			
			
			if (!mensaje.equals("")) {

				if (context != null) {

					AlertDialog.Builder builder = new AlertDialog.Builder(context);
					builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int id) {

							dialog.cancel();
						}
					});

					AlertDialog alertDialog = builder.create();
					alertDialog.setMessage(mensaje);
					alertDialog.show();
				}

			}

		}
	};
	
	public void onClickRegresar(View view){
		finish();
	}
	

	/**
	 * Imprimir el inventario .
	 * @param macImpresora
	 * @param numero_doc
	 * @param copiaPrint
	 */
	protected void imprimirSewooLKP20(final String macImpresora) {
		
		
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				Looper.prepare();

				if (macImpresora.equals("-")) {
					if (progressDialog != null)
						progressDialog.dismiss();
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Toast.makeText(FormInformeInventario.this, "Aun no hay Impresora Predeterminada.\n\nPor Favor primero Configure la Impresora!",
									Toast.LENGTH_SHORT).show();
						}
					});
				} else {

					if (sewooLKP20 == null) {
						sewooLKP20 = new SewooLKP20(FormInformeInventario.this);
					}
					int conect = sewooLKP20.conectarImpresora(macImpresora);

					switch (conect) {
					case 1:
						sewooLKP20.formatoInventarioDiaSewoo(isLiquidacion);
						sewooLKP20.imprimirBuffer(true);
						break;

					case -2:
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(FormInformeInventario.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
							}
						});
						break;

					case -8:
						if (progressDialog != null)
							progressDialog.dismiss();
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Util.MostrarAlertDialog(FormInformeInventario.this, "Bluetooth apagado. Por favor habilite el bluetoth para imprimir.");
							}
						});
						break;

					default:
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(FormInformeInventario.this, "Error desconocido, intente nuevamente.", Toast.LENGTH_SHORT).show();
							}
						});
						break;
					}

					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (sewooLKP20 != null) {
						sewooLKP20.desconectarImpresora();
						if (progressDialog != null)
							progressDialog.dismiss();
					}
				}
				Looper.myLooper().quit();
			}
		}).start();
	}

	public class DialogDetalleInform extends Dialog{


		InformeInventario informeInventario;
		public DialogDetalleInform (@NonNull Context context,InformeInventario informeInventario) {

			super( context );
			this.informeInventario=informeInventario;
		}


		ListView lista;
		ArrayList<InformeInventario> arrayList;
		AdapterInventario adapterInventario;
		AdapterPrecios adapterPrecios;
		Button btn;
		TextView textView;
		ListView listaPrecios;
		ArrayList<InformeInventario> arrayListPrecio;

		@Override
		protected void onCreate (Bundle savedInstanceState) {
			super.onCreate( savedInstanceState );
			setContentView( R.layout.dialog_detalle_inventario );

			lista=(ListView) findViewById( R.id.listaInventario );
			listaPrecios=(ListView) findViewById( R.id.listaPrecios );
			btn=(Button) findViewById( R.id.btnAceptar );
			textView=(TextView) findViewById( R.id.txtTitulo ) ;
			textView.setText( informeInventario.nombre );
			arrayList=new ArrayList<>(  );
			arrayListPrecio=new ArrayList<>(  );
			arrayList=DataBaseBO.consultarInventario(informeInventario.codigo);
			arrayListPrecio=DataBaseBO.consultarPrecios(informeInventario.codigo);
			adapterInventario=new AdapterInventario( getContext(),arrayList );
			adapterPrecios=new AdapterPrecios( getContext(),arrayListPrecio );
			listaPrecios.setAdapter( adapterPrecios );
			lista.setAdapter( adapterInventario );
			adapterInventario.update( arrayList );
			adapterPrecios.update( arrayListPrecio );
			btn.setOnClickListener( new View.OnClickListener() {
				@Override
				public void onClick (View view) {
					cancel();
				}
			} );
		}
	}


}
