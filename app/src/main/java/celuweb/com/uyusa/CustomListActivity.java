package celuweb.com.uyusa;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CustomListActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
    }
    
    @Override
    public void setContentView(int layoutResID) {
    	
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    	requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
    	
    	super.setContentView(layoutResID);
    	
    	getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
    	setTitle(getTitle());
    }
    
    @Override
    public void setTitle(CharSequence title) {
    	
    	TextView lblTitle = (TextView) findViewById(R.id.lblTitle);
    	
    	if (lblTitle != null) {
    		
    		lblTitle.setText(title);
    		
    	} else {
    		
    		super.setTitle(title);
    	}
    }
    
    public void setFeatureIndeterminateProgress(boolean visible) {
    	
    	ProgressBar progress_bar = (ProgressBar)findViewById(R.id.progress_bar);
    	
    	if (progress_bar != null) {
    		
    		int VISIBLE = visible ? View.VISIBLE : View.GONE;
    		progress_bar.setVisibility(VISIBLE);
    	}
    }
}
