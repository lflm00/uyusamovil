package celuweb.com.uyusa;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;

public class FormEstadisticasActivity extends TabActivity {

	TabHost tabHost;
	ImageView btnAtras;
	ImageView btnPlusMas;
	ImageView btnSearch;
	LinearLayout lybtnAtras;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_estadisticas);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		DataBaseBO.CargarInfomacionUsuario();
		DataBaseBO.eliminarClientePropio(Main.usuario.codigoVendedor);
		//CargarOpcionesReportes();
		
		CargarTabs();
		
		
		TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
		mTitle.setText("Efectividad");
		
		btnAtras = (ImageView) toolbar.findViewById(R.id.btnAtras);
		btnAtras.setOnClickListener(botonAtrasListener);
	
		lybtnAtras = (LinearLayout) toolbar.findViewById(R.id.lybtnAtras);
		lybtnAtras.setOnClickListener(lybotonAtrasListener);
		
		
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			    getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			    getWindow().setStatusBarColor(getResources().getColor(R.color.blue_dark));
			}
	}
	
	public void CargarTabs() {
		
		try {
			
			Intent intent;
			tabHost = getTabHost();
			tabHost.getTabWidget().setDividerDrawable(R.drawable.tab_divider);
			
			//Pestana Recorrido
			intent = new Intent().setClass(this, FormEstadisticaRecorrido.class);
			SetupTab(new TextView(this), "recorrido", getString(R.string.recorrido), intent);
			
			//Pestana Visitas Realizadas 
			intent = new Intent().setClass(this, FormEstadisticaPedidos.class);
			intent.putExtra("pedido", 1);
			SetupTab(new TextView(this), "visitasRealizadas", getString(R.string.visitas_realizadas), intent);
			
			//Pestana Sin Sincronizar
			intent = new Intent().setClass(this, FormEstadisticaPedidos.class);
			intent.putExtra("pedido", 0);
			SetupTab(new TextView(this), "sinSincronizar", getString(R.string.sin_sincronizar), intent);
			
			//Pestana Venta por linea
//			intent = new Intent().setClass(this, FormEstadisticaLineaProductos.class);
//			SetupTab(new TextView(this), "lineaProducto", "Ventas por Linea", intent);
			
			//Pestana Venta por productos
			intent = new Intent().setClass(this, FormEstadisticaProductosFamilia.class);
			SetupTab(new TextView(this), "familiaProducto", getString(R.string.ventas_por_linea), intent);
			
//			//Pesta�a Cliente Nuevo
//			intent = new Intent().setClass(this, FormEstadisticaPedidos.class);
//			intent.putExtra("pedido", -1);
//			SetupTab(new TextView(this), "Cli. Nuevos", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pedidos C.Nuevos", intent);
			
			
			//Pestana No Compras
			intent = new Intent().setClass(this, FormEstadisticasNoCompra.class);
			intent.putExtra("noCompra", false);
			SetupTab(new TextView(this), "noCompra", getString(R.string.no_compra), intent);
			
			/*Anulados*//*
			intent = new Intent().setClass(this, FormAnuladosActivity.class);
			SetupTab(new TextView(this), "Anulados", getString(R.string.anulados), intent);*/
			
			tabHost.setCurrentTab(0);
			
		} catch (Exception e) {
			String msg = e.getMessage();
			Log.e("Cargando Tabs", msg, e);
		}
	}
	
	
	
	
	private void SetupTab(final View view, final String tag, final String titulo, Intent intent) {
		
		View tabView = CreateTabView(tabHost.getContext(), titulo);
		TabSpec setContent = tabHost.newTabSpec(tag).setIndicator(tabView).setContent(intent);
		tabHost.addTab(setContent);
	}
	
	
	
	
	private static View CreateTabView(final Context context, final String titulo) {

		View view = LayoutInflater.from(context).inflate(R.layout.tab_bg, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(Html.fromHtml(titulo.trim().replace(" ", "<br />")));
		return view;
	}
	
	
	OnClickListener botonAtrasListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			finish();
			
		}
	};
	
	
	

	OnClickListener lybotonAtrasListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			finish();
			
		}
	};
	
	
}