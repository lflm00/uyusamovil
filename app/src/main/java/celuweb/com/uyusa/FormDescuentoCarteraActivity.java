package celuweb.com.uyusa;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Descuento;
import celuweb.com.DataObject.ItemCarteraListView;
import celuweb.com.DataObject.MotivoDescuento;

public class FormDescuentoCarteraActivity extends Activity{

	boolean primerEjecucion = true;
	
	AlertDialog alertDialog;
	Vector<MotivoDescuento> listaMotivosDescuentos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_descuento_cartera);
		
		CargarMotivosDecuentos();
	}
	
	public void CargarMotivosDecuentos() {
		
		Vector<String> listItems = new Vector<String>();
		listaMotivosDescuentos = DataBaseBO.ListaMotivosDescuentos(listItems);
		
		if (listItems.size() > 0) {
			
			String[] items = new String[listItems.size()];
			listItems.copyInto(items);
			
			Spinner cbDescuentoCartera = (Spinner) findViewById(R.id.cbDescuentoCartera);
	    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
	        
	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        cbDescuentoCartera.setAdapter(adapter);
	        
	        cbDescuentoCartera.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
	        	
	        	@Override
	            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	        		
	        		if (primerEjecucion) {
	        			
	        			primerEjecucion = false;
	        			
	        		} else {
	        			
	        			MotivoDescuento motivoDescuento = listaMotivosDescuentos.elementAt(position);
	        			Descuento descuento = Main.listaDescuentos.get("" + motivoDescuento.CodMotivo);
	        			
	        			if (descuento != null) {
	        				
	        				((TextView) findViewById(R.id.txtValorDescuento)).setText("" + descuento.valor);
	        				
	        			} else {
	        				
	        				((TextView) findViewById(R.id.txtValorDescuento)).setText("");
	        			}
	        		}
	        	}

	            @Override
	            public void onNothingSelected(AdapterView<?> parentView) { }
	        });
	        
		} else {
			
			Spinner cbDescuentoCartera = (Spinner) findViewById(R.id.cbDescuentoCartera);
	    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
	        
	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        cbDescuentoCartera.setAdapter(adapter);
		}
	}
	
	public void OnClickAceptarDescuentoCartera(View view) {
		
		TextView txtValorDescuento = (TextView) findViewById(R.id.txtValorDescuento);
		String valor = txtValorDescuento.getText().toString();
		
		if (valor.equals("")) {
			
			MostrarAlertDialog("Debe ingresar el valor del descuento");
			
		} else {
			
			if (Main.total_recaudo == 0) {
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.cancel();
						
						TabActivity ta = (TabActivity) FormDescuentoCarteraActivity.this.getParent();
					    ta.getTabHost().setCurrentTab(0);
					}
				});
				
				AlertDialog alert = builder.create();
				alert.setMessage("Para definir los descuentos, por favor ingrese primero las Facturas");
		    	alert.show();
				
			} else {
				
				AgregarDescuento(valor);
			}
		}
	}
	
	public void AgregarDescuento(String valor) {

		Spinner cbDescuentoCartera = (Spinner) findViewById(R.id.cbDescuentoCartera);
		int index = cbDescuentoCartera.getSelectedItemPosition();
		
		if (index == Spinner.INVALID_POSITION) {
			
			MostrarAlertDialog("Debe seleccionar un descuento valido");
			
			TabActivity ta = (TabActivity) this.getParent();
			ta.getTabHost().setCurrentTab(0);
			
		} else {

			if (Main.listaDescuentos == null)
				Main.listaDescuentos = new Hashtable<String, Descuento>();
			
			MotivoDescuento motivoDescuento = listaMotivosDescuentos.elementAt(index);
			Descuento descuento = Main.listaDescuentos.get("" + motivoDescuento.CodMotivo);
			
			boolean existeDesc = false;
			float totalDescuentos = 0;
			Enumeration<Descuento> e = Main.listaDescuentos.elements();
			
			while (e.hasMoreElements()) {
				
				Descuento descuentoAux = e.nextElement();
				
				if (descuento != null) {
					
					if (descuento.codigo == descuentoAux.codigo) {
						
						totalDescuentos += Util.ToFloat(valor);
						existeDesc = true;
						
					} else {
						
						totalDescuentos += Util.ToFloat(descuentoAux.valor);
					}
					
				} else {
					
					totalDescuentos += Util.ToFloat(descuentoAux.valor);
				}
			}
			
			if (!existeDesc)
				totalDescuentos += Util.ToFloat(valor);
				
			
			if (Main.total_recaudo < totalDescuentos) {

				MostrarAlertDialog("Los descuentos son superiores al recaudo." + "\n\n" + "Agregue mas facturas, modifique el valor del descuento, o elimine algun descuento");
				return;
			}
				
			if (descuento == null) {
				
				descuento = new Descuento();
				descuento.codigo      = motivoDescuento.CodMotivo;
				descuento.descripcion = motivoDescuento.Descripcion;
				descuento.valor       = valor;
				
				Main.listaDescuentos.put("" + motivoDescuento.CodMotivo, descuento);
				
			} else {
				
				descuento.valor = valor;
				Main.listaDescuentos.put("" + motivoDescuento.CodMotivo, descuento);
			}
			
			ItemCarteraListView[] items;
			e = Main.listaDescuentos.elements();
			Vector<ItemCarteraListView> listaItems = new Vector<ItemCarteraListView>();
			
			while (e.hasMoreElements()) {
				
				Descuento descuentoAux = e.nextElement();
				
				ItemCarteraListView item = new ItemCarteraListView();
				item.titulo = descuentoAux.descripcion + " " + Util.SepararMiles(descuentoAux.valor);
				listaItems.addElement(item);				
			}
			
			if (listaItems.size() > 0) {
				
				items = new ItemCarteraListView[listaItems.size()];
				listaItems.copyInto(items);
				
			} else {
				
				items = new ItemCarteraListView[] {};
			}
			
			ListViewDescuentoAdapter adapter = new ListViewDescuentoAdapter(this, items);
			ListView listaCarteraRecaudo = (ListView) findViewById(R.id.listaDescuentoCartera);
			listaCarteraRecaudo.setAdapter(adapter);
			
			
			////////////////////////////////////////////////////////////////////////////////////////
			
			FormCarteraRecaudo.SetTotalDescuento(totalDescuentos);
			
			//Intent i = this.getParent().getIntent();
		    //i.putExtra("totalDescuentos", totalDescuentos);
		    
		    
		    
		    //TextView lblOtrosDescuentos = (TextView)findViewById(R.id.lblOtrosDescuentos);
		    //lblOtrosDescuentos.setText("Otros Descuentos: " + Util.SepararMiles("" + totalDescuentos));
		    
		    //TextView lblOtrosDescuentos = (TextView)ta.getTabHost().findViewWithTag("facturas").findViewById(R.id.lblOtrosDescuentos);
		    //lblOtrosDescuentos.setText("Otros Descuentos: " + Util.SepararMiles("" + totalDescuentos));
		    
		    //
		}
	}
	
	
	
	public void MostrarAlertDialog(String mensaje) {
    	
    	if (alertDialog == null) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			alertDialog = builder.create();
    	}
    	
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
    }
}
