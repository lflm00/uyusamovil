package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Banco;
import celuweb.com.DataObject.FormaPago;

public class FormOpcionesPagosActivity extends Activity {

	boolean primerEjecucion = true;
	Vector<Banco> listaBancos;
	AlertDialog alertDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_formas_pago);
		
		CargarBancos();
		//CargarOpcionesFormaPago();
		
		//((TextView)findViewById(R.id.lblValorRecaudoFormaPago)).setText(Util.SepararMiles("" + (Main.total_recaudo - Main.total_descuento)));
	}
	
	public void CargarBancos() {
		
		Vector<String> listaItems = new Vector<String>();
		listaBancos = DataBaseBO.ListaBancos(listaItems);
		
		if (listaItems.size() > 0) {
			
			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			
			Spinner cbBancosFormaPago = (Spinner) findViewById(R.id.cbBancosFormaPago);
	    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
	        
	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        cbBancosFormaPago.setAdapter(adapter);
		}
	}
	
	/*public void CargarPlazas() {
		
		String[] items = new String[] { "Local", "Otra" };
    	Spinner cbPlazaFormasPago = (Spinner) findViewById(R.id.cbPlazaFormasPago);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbPlazaFormasPago.setAdapter(adapter);
	}*/
	
	/*public void CargarOpcionesFormaPago() {
    	
		String[] items = new String[] {"Efectivo", "Cheque"};
    	Spinner cbFormasDePago = (Spinner) findViewById(R.id.cbFormasDePago);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbFormasDePago.setAdapter(adapter);
        
        cbFormasDePago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        	
        	@Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        		
        		if (primerEjecucion) {
        			
        			primerEjecucion = false;
        			
        		} else {
        			
        			MostrarOpcionesFormaPago();
        		}
        	}

            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });
        
        FormaPago formPago = Main.listaFormaPago.get("1");
        
        if (formPago != null) {
        	
        	//((TextView) findViewById(R.id.txtNroDocumento)).setText(formPago.nroDocumento);
        	((TextView) findViewById(R.id.txtValorFormaPago)).setText("" + formPago.valor);
        }
	}*/
	
	/*public void MostrarOpcionesFormaPago() {
		
		String op = ((Spinner) findViewById(R.id.cbFormasDePago)).getSelectedItem().toString();
		int position = ((Spinner) findViewById(R.id.cbFormasDePago)).getSelectedItemPosition();
		int codigoPago = 0;
		
		switch (position) {
		
			case 0:
				codigoPago = 1; //Efectivo
				break;
				
			case 1:
				codigoPago = 2; //Cheque
				break;
				
			case 2:
				codigoPago = 4; //Cheque Posfechado
				break;
				
			case 3:
				codigoPago = 3; //Consignacion
				break;
		}
		
		FormaPago formPago = Main.listaFormaPago.get("" + codigoPago);
		
		if (formPago != null) {
			
			//((TextView) findViewById(R.id.txtNroDocumento)).setText(formPago.nroDocumento);
			((TextView) findViewById(R.id.txtValorFormaPago)).setText("" + formPago.valor);
			//((TextView) findViewById(R.id.txtNroChequeFormaPago)).setText("" + formPago.nroCheque);
		
		} else {
			
			//((TextView) findViewById(R.id.txtNroDocumento)).setText("");
			((TextView) findViewById(R.id.txtValorFormaPago)).setText("");
			//((TextView) findViewById(R.id.txtNroChequeFormaPago)).setText("");
		}
		
		if (op.equals("Efectivo")) {
			
			((Spinner)findViewById(R.id.cbBancosFormaPago)).setVisibility(Spinner.INVISIBLE);
			//((TableLayout)findViewById(R.id.tblLayoutPlazaFormaPago)).setVisibility(TableLayout.INVISIBLE);
			//((TableLayout)findViewById(R.id.tblLayoutNumCheque)).setVisibility(TableLayout.INVISIBLE);
					
		} else if (op.equals("Consignacion")) {
			
			((Spinner)findViewById(R.id.cbBancosFormaPago)).setVisibility(Spinner.VISIBLE);
			//((TableLayout)findViewById(R.id.tblLayoutPlazaFormaPago)).setVisibility(TableLayout.VISIBLE);
			//((TableLayout)findViewById(R.id.tblLayoutNumCheque)).setVisibility(TableLayout.INVISIBLE);
			
		} else {
			
			((Spinner)findViewById(R.id.cbBancosFormaPago)).setVisibility(Spinner.VISIBLE);
			//((TableLayout)findViewById(R.id.tblLayoutPlazaFormaPago)).setVisibility(TableLayout.VISIBLE);
			//((TableLayout)findViewById(R.id.tblLayoutNumCheque)).setVisibility(TableLayout.VISIBLE);
		}
	}*/
	
	public void OnClickAceptarOpcionesPago(View view) {
		
		float valor = Util.ToFloat(((TextView) findViewById(R.id.txtValorFormaPago)).getText().toString());
		
		if (valor > 0) {
			
			int codigoPago = 0;
			String nroCheque = "";
			String nroCuenta = "";
			String codigoBanco = "0";
			
			int position = 0;//((Spinner) findViewById(R.id.cbFormasDePago)).getSelectedItemPosition();
			String op = "";//((Spinner) findViewById(R.id.cbFormasDePago)).getSelectedItem().toString();
			
			int idBanco = ((Spinner)findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
			Banco banco = listaBancos.elementAt(idBanco);
			
			switch (position) {
				
				case 0:
					codigoPago = 1; //Efectivo
					codigoBanco = "-1";
					break;
					
				case 1:
					codigoPago = 2; //Cheque
					codigoBanco = banco.codigo;
					break;
					
				case 2:
					codigoPago = 4; //Cheque Posfechado
					codigoBanco = banco.codigo;
					break;
					
				case 3:
					codigoPago = 3; //Consignacion
					codigoBanco = banco.codigo;
					break;
			}
			
			//DatePicker datePickerFormaPago = (DatePicker) findViewById(R.id.datePickerFormaPago);
			//String fechaPago = datePickerFormaPago.getYear() + "-" + (datePickerFormaPago.getMonth() + 1) + "-" + datePickerFormaPago.getDayOfMonth();
			//int dias = DataBaseBO.DiferenciaDias(fechaPago);
			
			//MostrarAlertDialog("Fecha Pago = " + fechaPago + " dif dias: " + dias);
			
			if (position == 1) { //Si es cheque
				
				TextView txtNroCheque = (TextView) findViewById(R.id.txtNroCheque);
				nroCheque = txtNroCheque.getText().toString().trim();
				
				if (nroCheque.equals("")) {
					
					MostrarAlertDialog("Por favor ingrese el numero de cheque");
					txtNroCheque.requestFocus();
					return;
					
				} 
				
				TextView txtNroCuenta = (TextView) findViewById(R.id.txtNroCuenta);
				nroCuenta = txtNroCuenta.getText().toString().trim();
				
			}
				
				/*else {
					
					if (dias > 0 && position == 1) {
						
						MostrarAlertDialog("La fecha que ha ingresado no corresponde a la forma de Pago 'Cheque'. Debe ingresarlo como Cheque Postfechado.");
						return;
						
					} else if (dias <= 0 && position == 2) {
						
						MostrarAlertDialog("La fecha que ha ingresado no corresponde a la forma de Pago 'Cheque Posfechado'. Debe ingresarlo como Cheque.");
						return;
					}
				}
				
			}/* else if (position == 3) { //Consignacion
				
				TextView txtNroDocumento = (TextView) findViewById(R.id.txtNroDocumento);
				nroDocumento = txtNroDocumento.getText().toString().trim();
				
				if (nroDocumento.equals("")) {
					
					MostrarAlertDialog("Por favor ingrese el numero de la consigancion");
					((TextView) findViewById(R.id.txtNroDocumento)).requestFocus();
					return;
				}
			}*/
			
			/*if (dias > 0 && position == 3) {
				
				MostrarAlertDialog("La fecha que ha ingresado es Invalida.");
				return;
			}*/
			
			/*float totalPagos = 0;
			boolean existePago = false;
			Enumeration<FormaPago> e = Main.listaFormaPago.elements();
			
			while (e.hasMoreElements()) {
				
				FormaPago formaPagoAux = e.nextElement();
				
				if (formaPagoAux.codigo == codigoPago) { //La forma de Pago ya fue agregada
					
					totalPagos += valor;
					existePago = true;
					
				} else {
					
					totalPagos += formaPagoAux.valor; 
				}
			}
			
			if (!existePago)
				totalPagos += valor;
			
			if (totalPagos > Main.total_recaudo - Main.total_descuento) {
				
				
			} else {
				
				
			}*/
			
			
			FormaPago formaPago;
			formaPago = Main.listaFormaPago.get("" + codigoPago);
			
			if (formaPago == null)
				formaPago = new FormaPago();
			
			//formaPago.nroCheque = nroCheque;
			//formaPago.nroCuenta = nroCuenta;
			
			//formaPago.codigo       = codigoPago;
			//formaPago.descripcion  = op;
			//formaPago.valor        = valor;
			//formaPago.fechaPago    = Util.FechaActual("yyyy-MM-dd HH:mm:ss");
			//formaPago.codigoBanco  = codigoBanco;
			
			/*String plazaSel = ((Spinner)findViewById(R.id.cbPlazaFormasPago)).getSelectedItem().toString();
			
			if (plazaSel.equals("Local"))
				formaPago.plaza = "L";
			else if (plazaSel.equals("Otra"))
				formaPago.plaza = "O";*/
			
			Main.listaFormaPago.put("" + codigoPago, formaPago);
			
			setResult(RESULT_OK);
			finish();
			
		} else {
			
			MostrarAlertDialog("Por favor ingrese el valor del Recaudo");
			((TextView) findViewById(R.id.txtValorFormaPago)).requestFocus();
			return;
		}
	}
	
	public void OnClickCancelarOpcionesPago(View view) {
		
		setResult(RESULT_CANCELED);
		finish();
	}
	
	public void MostrarAlertDialog(String mensaje) {
    	
    	if (alertDialog == null) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			alertDialog = builder.create();
    	}
    	
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
    }
}
