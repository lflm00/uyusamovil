package celuweb.com.uyusa;

import java.util.Date;
import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.MotivoAbono;

public class FormRegistrarRecaudoActivity extends Activity {

	Vector<MotivoAbono> listaMotivosAbono;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_registrar_recaudo);
		
		CargarDatosRecaudo();
	}
	
	public void CargarDatosRecaudo() {
		
		if (Main.cartera.size() > 0) {

			((RadioButton) findViewById(R.id.radioPagoTotal)).setChecked(true);
			
			Cartera cartera = Main.cartera.lastElement();
			((TextView)findViewById(R.id.lblDocRegistroRecaudo)).setText(cartera.codCliente);
			((TextView)findViewById(R.id.lblFechaRegistroRecaudo)).setText(Util.DateToStringMM_DD_YYYY(new Date()));
			((TextView)findViewById(R.id.lblVencimientoRegistroRecaudo)).setText(cartera.FechaVecto);
			((TextView)findViewById(R.id.lblDiasVencRegistroRecaudo)).setText("" + cartera.dias);
			((TextView)findViewById(R.id.lblValorRegistroRecaudo)).setText(Util.SepararMiles("" + cartera.saldo));
			
			EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
			txtValorARecaudar.setText("" + cartera.saldo);
			txtValorARecaudar.setEnabled(false);
			
			Vector<String> listaItems = new Vector<String>();
			listaMotivosAbono =  DataBaseBO.ListaMotivoAbono(listaItems);
			
			if (listaItems.size() > 0) {
				
				String[] items = new String[listaItems.size()];
				listaItems.copyInto(items);
				
				Spinner cbMotivoPagoParcial = (Spinner) findViewById(R.id.cbMotivoPagoParcial);
		    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
		        
		        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		        cbMotivoPagoParcial.setAdapter(adapter);
			}
		}
	}
	
	public void OnClickPagoTotal(View view) {
		
		Cartera cartera = Main.cartera.lastElement();
		
		EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
		txtValorARecaudar.setText("" + cartera.saldo);
		txtValorARecaudar.setEnabled(false);
		
		((TableLayout) findViewById(R.id.tblLayoutMotivoAbono)).setVisibility(TableLayout.INVISIBLE);
	}
	
	public void OnClickPagoParcial(View view) {
		
		EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
		txtValorARecaudar.setText("0");
		txtValorARecaudar.setEnabled(true);
		
		((TableLayout) findViewById(R.id.tblLayoutMotivoAbono)).setVisibility(TableLayout.VISIBLE);
	}
	
	public void OnClickAceptarRegisrarRecaudo(View view) {
		
		String valor = ((EditText)findViewById(R.id.txtValorARecaudar)).getText().toString();
		float valorARecaudar = Util.ToFloat(valor);
		
		if (valorARecaudar == 0) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese el valor del Recaudo");
			((EditText)findViewById(R.id.txtValorARecaudar)).requestFocus();
			
		} else { 
			
			Main.cartera.lastElement().valorARecaudar = valorARecaudar;
			Main.cartera.lastElement().pagoTotal = ((RadioButton) findViewById(R.id.radioPagoTotal)).isChecked();
			Main.cartera.lastElement().observacion = ((EditText)findViewById(R.id.txtObservacionRegistroRecaudo)).getText().toString();
			
			if (Main.cartera.lastElement().pagoTotal) {
				
				Main.cartera.lastElement().codigoMotivoAbono = -1;
				
			} else {
				
				int index = ((Spinner)findViewById(R.id.cbMotivoPagoParcial)).getSelectedItemPosition();
				MotivoAbono motivoAbono = listaMotivosAbono.elementAt(index);
				
				Main.cartera.lastElement().codigoMotivoAbono = motivoAbono.codigo;
			}
			
			setResult(RESULT_OK);
			finish();
		}
	}
	
	public void OnClickCancelarRegisrarRecaudo(View view) {
		
		setResult(RESULT_CANCELED);
		finish();
	}
}
