package celuweb.com.uyusa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Producto;

public class FormUltimoPedidoDetalleActivity extends Activity {

	private Button btnCargarPedido;

	Dialog dialogMensaje;
	Dialog dialogCargarPedido;

	String numeroDoc = "";
	Vector<Producto> listaUltimoPedido;

	ProgressDialog progressDialog;
	LinearLayout lyBtnCargar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.from_ultimo_pedido);

		Inicializar();
		CargarUltimoPedido();
		SetListenerListView();
	}

	public void Inicializar() {
		lyBtnCargar = (LinearLayout) findViewById(R.id.lyBtnCargar);
		lyBtnCargar.setVisibility(View.VISIBLE);
		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {

			if (bundle.containsKey("NUMDOC"))
				numeroDoc = bundle.getString("NUMDOC");
		}
	}

	public void OnClickCargarPedido(View view) {

	}

	public void OnClickRegresar(View view) {

		finish();
	}

	public void CargarUltimoPedido() {

		ItemListView[] listaItems;
		Vector<ItemListView> items = new Vector<ItemListView>();
		listaUltimoPedido =  DataBaseBO.UltimoPedidoDetalle(items, numeroDoc);

		if (items.size() > 0) {

			listaItems = new ItemListView[items.size()];
			items.copyInto(listaItems);

			ListAdapterUltimo adapter = new ListAdapterUltimo(this, listaItems);
			ListView listaPedido = (ListView)findViewById(R.id.listaUltimoPedido);
			listaPedido.setAdapter(adapter);

		} else {

			ListAdapterUltimo adapter = new ListAdapterUltimo(this, new ItemListView[]{});
			ListView listaPedido = (ListView)findViewById(R.id.listaUltimoPedido);
			listaPedido.setAdapter(adapter);
		}

	}

	public void SetListenerListView() {

		ListView listaOpciones = (ListView)findViewById(R.id.listaUltimoPedido);

		listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {


				} catch (Exception e) {
					
					String msg = e.getMessage();
					Toast.makeText(getBaseContext(), "No se pudo Cargar la Informacion del Producto: " + msg, Toast.LENGTH_LONG).show();
				}
            }
        });
	}
	


	
	public void AgregarUltimoPedido(View view) {

		AlertDialog alertDialog;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				registrarMain();
				dialog.dismiss();
			}
		});
		builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		alertDialog = builder.create();
		alertDialog.setMessage("Esta seguro de cargar el ultimo pedido al pedido actual?\n Se cargaran los productos al pedido actual");
		alertDialog.show();


	}

	public void registrarMain(){
		Detalle detalle;
		int cant = 0;
		int cant1 = 1;

		for (Producto producto : listaUltimoPedido) {

			detalle = new Detalle();
			detalle.codCliente       = Main.cliente.codigo;
			detalle.codProducto      = producto.codigo;
			detalle.nombProducto        = producto.descripcion;
			detalle.precio               = producto.precio;
			detalle.iva                  = producto.iva;
			detalle.descuento_autorizado = 0;
			detalle.cantidad             = producto.cantidad;
			detalle.tipo_pedido          = Const.PEDIDO_VENTA;
			detalle.codMotivo            = "";
			detalle.indice 				= cant1;

			//boolean agrego = AgregarProducto(detalle);

			//if (agrego) {

			cant++;
			Main.detallePedido.put(producto.codigo, detalle);

//				} else {
//
//					Log.e("Agregar Ultimo Pedido", "no se puedo agregar el producto : " + producto.codigo);
//				}
		}
		//}

		AlertDialog alertDialog;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
				FormUltimoPedidoDetalleActivity.this.setResult(RESULT_OK);
				FormUltimoPedidoDetalleActivity.this.finish();
			}
		});

		if (progressDialog != null)
			progressDialog.cancel();

		alertDialog = builder.create();
		alertDialog.setMessage("Se agregaron " + cant + " al Pedido Actual");
		alertDialog.show();
	}



	public boolean AgregarProducto(Detalle detalle) {
		
		boolean registro;
		
		if (Main.encabezado == null)
			Main.encabezado = new Encabezado();
		
		if (Main.encabezado.numero_doc == null) {
			
			/*Main.encabezado.codigo_cliente = Main.cliente.codigo;      //Codigo del Cliente
			Main.encabezado.nombre_cliente = Main.cliente.nombre;      //Nombre del Cliente
			Main.encabezado.razon_social   = Main.cliente.razonSocial; //Razon Social
			Main.encabezado.codigo_novedad = 1;                        //1 -> Venta, para no compra es MotivosCompras.codigo
			Main.encabezado.lista_precio   = Main.cliente.listaPrecio; //Lista de Precio
			
			Main.encabezado.tipo_cliente   = "V";                            //Tipo Cliente (Cliente viejo o Cliente nuevo)
			Main.encabezado.extra_ruta     = Main.cliente.extra_ruta;        //Indica si el pedido se hace Sobre un cliente que no esta en el Rutero
			Main.encabezado.numero_doc     = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor); //Numero Unico por Pedido*/
			
			String version = ObtenerVersion();
			String imei = ObtenerImei();
			
			String tipoTrans = "0";
			registro = DataBaseBO.RegistrarEncabezado(Main.encabezado, detalle, version, imei, tipoTrans);
			
		} else {
			
			registro = DataBaseBO.RegistrarProductoPedido(Main.encabezado, detalle);
		}
		
		return registro;
	}

	public String ObtenerVersion() {
		
		String version;
		
		try {
        	
        	version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			
		} catch (NameNotFoundException e) {
			
			version = "0.0";
			Log.e("Pedido", e.getMessage(), e);
		}
		
		return version;
	}
	
	public String ObtenerImei() {
		
		TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
	}
	
	public void onClickRegresar(View view){
		
		finish();
	}
}
