package celuweb.com.uyusa;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Descuento;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.DataObject.Foto;
import celuweb.com.DataObject.HistorialPedido;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.Usuario;

public final class Main {

    public static boolean consignacion = false;

    public static int sticker = 0;
    public static float desc1 = 0;
    public static float desc2 = 0;
    public static float desc3 = 0;


    public static int day = 0;
    public static int mes = 0;
    public static int ano = 0;

    public static int day1 = 0;
    public static int mes1 = 0;
    public static int ano1 = 0;

    public static ProgressDialog pdialog;

    public static double total_recaudo = 0;
    public static double total_descuento = 0;
    public static float total_forma_pago = 0;

    public static Vector<Drawable> fotosGaleria = new Vector<Drawable>();
    public static Vector<Foto> listaInfoFotos = new Vector<Foto>();

    public static Vector<Cartera> cartera;
    public static Hashtable<String, Descuento> listaDescuentos = new Hashtable<String, Descuento>();
    public static Hashtable<String, FormaPago> listaFormaPago = new Hashtable<String, FormaPago>();

    public static Vector<Cartera> carterasRelacionadas;
    public static FormaPago formaPagoAct;


    public static Vector<FormaPago> listaFormasPago = new Vector<FormaPago>();

    //public static Vector<Drawable> imgCatago = new Vector<Drawable>();

    //public static String descAct = "";

    public static String versionApp;
    public static String deviceId;

    //Referencia a los Formularios (Activity)
    public static Intent formOpcionesPedido;

    public static Drawable fotoNoCompra;
    public static Drawable fotoActual;
    public static boolean guardarFoto = false;

    /**
     * Guarda la informacion que el Usuario Ingresa en el Formulario de Configuracion
     * Esta informacion se utiliza para las Acciones: Iniciar Dia, Terminar Dia, Enviar Informmacion
     **/
    //public static SharedPreferences configUser;

    /**
     * Guarda la informacion del Vendedor que se Loguea
     **/
    public static Usuario usuario;

    /**
     * Cliente al cual se le va hacer le pedio
     **/
    public static Cliente cliente;

    /**
     * Guarda el Encabezado del Pedido
     **/
    public static Encabezado encabezado;


    /**
     * Guarda el detalle del Pedido de un Cliente.
     * El detalle se identifica con el codigo del Producto
     **/
    public static Hashtable<String, Detalle> detallePedido;


    //Variables para conservar ultima busqueda
    public static Vector<Producto> listaProductos;
    public static Vector<HistorialPedido> listaHistorial;
    public static String codBusqProductos;
    public static String codBusqHistorial;
    public static int posOpBusqProductos;
    public static int posOpLineas;
    public static boolean primeraVez;
    public static ItemListView[] itemsBusq = new ItemListView[]{};

    public static float presupuesto = 0;
    public static String rutaProgramada = "";
    public static int posRutaProgRutero = 0;

    public static String rutaEscogidaVendedor = "";

    public static ArrayList<Producto> navegadorProductos;
    public static Context contexto;

    public static String codBusqProductos2;
    public static String codBusqHistorial2;
    public static float ventames;
    public static float porcentaje;

    /**
     * Se define un contexto estatico donde se incializan las varibles
     * Este contexto estatico se ejecuta una sola vez duarante
     * la ejecucion de aplicacion.
     **/
    static {

        usuario = new Usuario();
        cliente = new Cliente();
        encabezado = new Encabezado();
        cartera = new Vector<Cartera>();
        detallePedido = new Hashtable<String, Detalle>();
        navegadorProductos = new ArrayList<Producto>();
    }
}