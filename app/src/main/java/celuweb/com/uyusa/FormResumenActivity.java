package celuweb.com.uyusa;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ResumenSync;

public class FormResumenActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_resumen);
		
		CargarResumenSync();
	}
	
	public void OnClikFormResumen(View view) {
		
		setResult(RESULT_OK);
		finish();
	}
	
	public void CargarResumenSync() {
		
		ResumenSync resumenSync = DataBaseBO.CagarResumenSincronizacion();
		
		if (resumenSync != null) {
			
			((TextView) findViewById(R.id.lblResumenTotalClientes)).setText("" + resumenSync.totalClientes);
			((TextView) findViewById(R.id.lblResumenTotalReferencias)).setText("" + resumenSync.totalReferencias);
			((TextView) findViewById(R.id.lblResumenTotalRutero)).setText("" + resumenSync.totalRutero);
			((TextView) findViewById(R.id.lblResumenTotalComplementos)).setText("" + resumenSync.totalComplementos);		
		}			
	}
}
