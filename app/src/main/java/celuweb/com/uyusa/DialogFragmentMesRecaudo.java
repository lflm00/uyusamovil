package celuweb.com.uyusa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ResumenMesRecaudo;

public class DialogFragmentMesRecaudo extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View view = getActivity().getLayoutInflater().inflate(R.layout.template_dialog_mes_recaudo_vendedor, null);
        TextView tipoUno = view.findViewById(R.id.tipoUnoMesRecaudo);
        TextView tipoDos = view.findViewById(R.id.tipoDosMesRecaudo);
        TextView tipoTres = view.findViewById(R.id.tipoTresMesRecaudo);
        TextView tipoCuatro = view.findViewById(R.id.tipoCuatroMesRecaudo);
        TextView tipoCinco = view.findViewById(R.id.tipoCincoMesRecaudo);
        TextView valorUno = view.findViewById(R.id.valorUnoMesRecaudo);
        TextView valorDos = view.findViewById(R.id.valorDosMesRecaudo);
        TextView valorTres = view.findViewById(R.id.valorTresMesRecaudo);
        TextView valorCuatro = view.findViewById(R.id.valorCuatroMesRecaudo);
        TextView valorCinco = view.findViewById(R.id.valorCincoMesRecaudo);

        ArrayList<ResumenMesRecaudo> resumenes = DataBaseBO.obtenerMesRecaudoVendedor();

        for(int i = 0; i < resumenes.size(); i++){
            if(i == 0){
                tipoUno.setText(resumenes.get(0).tipo_pago);
                valorUno.setText(resumenes.get(0).total.toString());
                tipoUno.setVisibility(View.VISIBLE);
                valorUno.setVisibility(View.VISIBLE);
            }
            if(i == 1){
                tipoDos.setText(resumenes.get(1).tipo_pago);
                valorDos.setText(resumenes.get(1).total.toString());
                tipoDos.setVisibility(View.VISIBLE);
                valorDos.setVisibility(View.VISIBLE);
            }
            if(i == 2){
                tipoTres.setText(resumenes.get(2).tipo_pago);
                valorTres.setText(resumenes.get(2).total.toString());
                tipoTres.setVisibility(View.VISIBLE);
                valorTres.setVisibility(View.VISIBLE);
            }
            if(i == 3){
                tipoCuatro.setText(resumenes.get(3).tipo_pago);
                valorCuatro.setText(resumenes.get(3).total.toString());
                tipoCuatro.setVisibility(View.VISIBLE);
                valorCuatro.setVisibility(View.VISIBLE);
            }
            if(i == 4){
                tipoCinco.setText(resumenes.get(4).tipo_pago);
                valorCinco.setText(resumenes.get(4).total.toString());
                tipoCinco.setVisibility(View.VISIBLE);
                valorCinco.setVisibility(View.VISIBLE);
            }
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

        alert.setTitle("Resumen mes recaudos")
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        alert.setView(view);

        return alert.create();
    }
}






