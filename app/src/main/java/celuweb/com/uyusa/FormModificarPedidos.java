/**
 * 
 */
package celuweb.com.uyusa;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import celuweb.com.BusinessObject.ConfigBO;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Config;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;


/**
 * Permite modificar pedidos que aun no han sido soncronizados. 
 * si eun pedido ya fue sincronizado, no puede ser modificado, pero si mostrado.
 * @author JICZ
 *
 */
public class FormModificarPedidos extends Activity {


	/**
	 * listView que muestra los pedidos realizados hasta el momento.
	 */
	private ListView listViewPedidos;

	/**
	 * lista de detalles mostrados en dialogDetalles
	 */
	private ListView listViewDetalles;

	/**
	 * text para mostrar un mensaje en el formulario.
	 */
	private TextView titulo;

	/**
	 * muestra el valor neto del pedido
	 */
	private EditText editTextNeto;

	/**
	 * muestra el iva generado por el pedido
	 */
	private EditText editTextIva;

	/**
	 * muestra el total del pedido.
	 */
	private EditText editTextTotal;

	/**
	 * lista de pedidos que se han realizado hasta el momento.
	 */
	private ArrayList<Encabezado> listaPedidosActual;

	/**
	 * lista de detalles que le pertenecen a un encabezado (pedido).
	 */
	private ArrayList<Detalle> listaDetalle;

	/**
	 * lista de items a mostrar en listView
	 */
	private ArrayList<ItemListView> items;

	/**
	 * lista de items detalles a mostrar en listView
	 */
	private ArrayList<ItemListView> itemsDetalles;

	/**
	 * dialog para mostrar los detalles.
	 */
	private Dialog dialogDetalles;

	/**
	 * boton salir del dialog
	 */
	private Button botonSalir; 

	/**
	 * boton ingresar a modificacion
	 */
	private Button botonModificar; 
	
	private boolean sincronizado;




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_modificar_pedido);

		//identificar si los pedidos a mostrar son los sincronizados o los no sincronizados.
		sincronizado = getIntent().getExtras().getBoolean("sincronizado");

		//instancia del listView
		listViewPedidos = (ListView) this.findViewById(R.id.listaPedidosModificar);		
		titulo = (TextView) this.findViewById(R.id.textViewTiutloModificar);

		//definir un mensaje para mostrar en el layout
		String mensaje = sincronizado? "Lista de pedidos sincronizados:" : "Lista de pedidos No sincronizados:";		
		titulo.setText(mensaje);

		
		
	}	

	@Override
	protected void onResume() {
		super.onResume();
		//cargar pedidos disponibles.
		cargarListaPedidosRealizados(sincronizado);
	}


	/**
	 * cargar en el listView la lista de pedidos que se han realizado hasta un momento determinado.
	 * @param sincronizado 
	 */
	private void cargarListaPedidosRealizados(boolean sincronizado) {

		listaPedidosActual = DataBaseBO.cargarListaPedidos(sincronizado);
		items = new ArrayList<ItemListView>();

		for (Encabezado e : listaPedidosActual) {
			ItemListView item = new ItemListView();
			item.titulo = e.codigo_cliente + "\n" + e.razon_social; 
			item.subTitulo = "Pedido: " + e.numero_doc;
			items.add(item);
		}
		//cargar el adapter en el listView
		ListViewModificarPedidoAdapter adapter = new ListViewModificarPedidoAdapter(FormModificarPedidos.this, items);
		listViewPedidos.setAdapter(adapter);
		listViewPedidos.setOnItemClickListener(listenerListViewPedidos);
	}



	/**
	 * listener para determinar el pedido seleccionado por el usuario.
	 */
	private OnItemClickListener listenerListViewPedidos = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
			Encabezado e = listaPedidosActual.get(position);			
			listaDetalle = DataBaseBO.cargarDetallesDePedido(e.numero_doc, false);			
			mostrarDialogDetallesDelPedido(e);
		}
	};




	/**
	 * mostrar el dialog con los detalles del pedido
	 * @param e 
	 */
	protected void mostrarDialogDetallesDelPedido(final Encabezado encabezado) {		

		if(dialogDetalles == null){
			dialogDetalles = new Dialog(FormModificarPedidos.this, R.style.AppTheme);
			dialogDetalles.setContentView(R.layout.dialog_detalles_pedidos_modificar);
			dialogDetalles.setTitle("Detalles");		
			listViewDetalles = (ListView) dialogDetalles.findViewById(R.id.listViewDetalles);
			editTextNeto = (EditText) dialogDetalles.findViewById(R.id.editTextNeto);
			editTextIva = (EditText) dialogDetalles.findViewById(R.id.editTextIva);
			editTextTotal = (EditText) dialogDetalles.findViewById(R.id.editTextTotal);
			botonSalir = (Button) dialogDetalles.findViewById(R.id.buttonSalirDialogModificar);
			botonModificar = (Button) dialogDetalles.findViewById(R.id.buttonModificarDialogModificar);		
			if(sincronizado){
				botonModificar.setVisibility(View.GONE);
			}
		}	
		
		//definir el texto adecuado para el boton.
		String textoBoton = sincronizado? "Ver" : "Modificar";
		botonModificar.setText(textoBoton);

		//asignar los montos.
		editTextNeto.setText("$ " + String.valueOf(encabezado.valor_neto - encabezado.total_iva));
		editTextNeto.setEnabled(false);
		editTextIva.setText("$ " + String.valueOf(encabezado.total_iva));
		editTextIva.setEnabled(false);
		editTextTotal.setText("$ " + String.valueOf(encabezado.valor_neto));
		editTextTotal.setEnabled(false);
		
		//asignar los detalles a la lista.
		cargarListaDetalles();

		//salir del dialog
		botonSalir.setOnClickListener(new OnClickListener() {			
			public void onClick(View v) {
				dialogDetalles.dismiss();
			}
		});	

		//iniciar activity pedido para modificar el pedido actual seleccionado.
		botonModificar.setOnClickListener(new OnClickListener() {			
			public void onClick(View v) {
				if(encabezado.syncmobile == 1){
					Util.MostrarAlertDialog(FormModificarPedidos.this, "El pedido ya fue sincronizado, no se permite modificar");
				}
				else {					
					modificarPedidos(encabezado);					
				}
			}
		});	
		dialogDetalles.show();		
	}



	/**
	 * cargar informacion necesario para iniciar la activity de pedidos, y poder modificarlo.
	 * @param encabezado
	 */
	protected void modificarPedidos(Encabezado encabezado) {

		if (DataBaseBO.ExisteDataBase()) {
			if (DataBaseBO.TerminoLabores()) {
				Util.MostrarAlertDialog(this, "No se pueden Registrar mas Pedidos debido a que ya termino Labores");
			} 

			Config config = ConfigBO.ObtenerConfigUsuario();

			if (config == null) {
				Util.MostrarAlertDialog(FormModificarPedidos.this, "Por favor Ingrese la Configuracion de Usuario e Inicie Dia");	
			} 
			else {
				if( Util.estaActualLaFecha() ){					

					Main.cliente = DataBaseBO.BuscarCliente(encabezado.codigo_cliente);
					DataBaseBO.GuardarCodPdv(Main.cliente.codigo,1); 
					DataBaseBO.organizarTmpProductos(Main.cliente.CodigoAmarre);
					DataBaseBO.ListarProductos(Main.cliente.CodigoAmarre); // cargar productos.

					// iniciar el activity de pedidos, para modificar.
					Intent i =  new Intent(FormModificarPedidos.this, FormPedidoActivity.class);
					i.putExtra("encabezado", encabezado);
					startActivity(i);
					dialogDetalles.dismiss(); // cerrar el dialog.
				}
				else {
					Util.MostrarAlertDialog(this,"La fecha de la labor no coincide con la fecha de celular, por favor iniciar dia",1);
				}
			}
		}
	}

	
	
	

	/**
	 * lista de detalles que se muestraen en el dialog. 
	 */
	private void cargarListaDetalles() {		

		itemsDetalles = new ArrayList<ItemListView>();

		for (Detalle d : listaDetalle) {
			ItemListView item = new ItemListView();
			item.titulo = d.descripcion;
			item.subTitulo = "Codigo: " + d.codProducto + "\nCantidad: " + d.cantidad + " -Precio Unit: " + d.precio + " -Iva: " + d.iva;
			itemsDetalles.add(item);
		}

		ItemListView[] item = new ItemListView[itemsDetalles.size()];
		item = itemsDetalles.toArray(item);

		//cargar el adapter en el listView
		ListViewAdapter adapter = new ListViewAdapter(FormModificarPedidos.this, item, R.drawable.recaudo, 0);
		listViewDetalles.setAdapter(adapter);
	}	
}














