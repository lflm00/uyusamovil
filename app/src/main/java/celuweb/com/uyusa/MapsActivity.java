/**
 *
 */
package celuweb.com.uyusa;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBOJF;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, OnMarkerClickListener {

	Vector<Cliente> listaClientes;
	int dia;
	Dialog dialogAbastecimiento;
	long mLastClickTime = 0;
	Vector<Marker> listaMarcadores = new Vector<Marker>();
	PolylineOptions rectOptions;
	Polyline polyline;
	Vector<Encabezado> listaPedidos;
	private GoogleMap mMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {

			try {
				if (bundle.containsKey("dia"))
//					dia = Integer.parseInt(bundle.getString("dia"));
				dia = bundle.getInt("dia");
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		Vector<ItemListView> listaItems = new Vector<ItemListView>();
		listaClientes = DataBaseBOJF.ListaClientesRutero(listaItems, dia + "");

	}

	private Bitmap createStoreMarker(String orden) {
		View markerLayout = getLayoutInflater().inflate(R.layout.store_marker_layout, null);

		ImageView markerImage = (ImageView) markerLayout.findViewById(R.id.marker_image);
		TextView markerRating = (TextView) markerLayout.findViewById(R.id.marker_text);
		markerImage.setImageResource(R.drawable.marker);
		markerRating.setText(orden);

		markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
				View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
		markerLayout.layout(0, 0, markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight());

		final Bitmap bitmap = Bitmap.createBitmap(markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		markerLayout.draw(canvas);
		return bitmap;
	}

	@Override
	public void onMapReady(GoogleMap map) {

		if (listaClientes != null && !listaClientes.isEmpty()) {
			LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

			Location myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
			double longitude = myLocation.getLongitude();
			double latitude = myLocation.getLatitude();
			LatLng sydney = new LatLng(latitude, longitude);
			Marker marker = map.addMarker(
					new MarkerOptions().position(sydney).title("").snippet("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

			listaMarcadores.add(marker);
			map.animateCamera(
					CameraUpdateFactory.newLatLngZoom(new LatLng(sydney.latitude, sydney.longitude), 12.0f));

			for (Cliente cliente : listaClientes) {

				if (cliente.latitud != 0 && cliente.longitud != 0) {

					sydney = new LatLng(cliente.latitud, cliente.longitud);

					marker = map.addMarker(
							new MarkerOptions().position(sydney).title("").snippet("").icon(BitmapDescriptorFactory
									.fromBitmap(createStoreMarker(cliente.contadorRutero + " - " + cliente.Nombre))));

					listaMarcadores.add(marker);
					map.animateCamera(
							CameraUpdateFactory.newLatLngZoom(new LatLng(sydney.latitude, sydney.longitude), 12.0f));

				}

			}

			/*rectOptions = new PolylineOptions();

			for (Marker mar : listaMarcadores) {

				rectOptions.add(mar.getPosition()).color(Color.parseColor("#009FDF"));
			}

			polyline = map.addPolyline(rectOptions);*/
			
			CameraPosition cameraPosition =
			            new CameraPosition.Builder().target(new LatLng(listaClientes.elementAt(0).latitud, listaClientes.elementAt(0).longitud))
	                    .zoom(17.5f)
	                    .bearing(300)
	                    .tilt(50)
	                    .build();
			
			try {
				mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//				changeCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
			}catch (Exception e)
			{
				String m= e.getMessage();
			}


		}

		map.setOnMarkerClickListener(this);

	}

	/*
	 * public void CargarInformacionCliente(final int position) {
	 * 
	 * Usuario usuario = DataBaseBO.ObtenerUsuario();
	 * 
	 * if (usuario == null) {
	 * 
	 * Util.MostrarAlertDialog(this,
	 * "No se pudo cargar la informacion del Usuario");
	 * 
	 * } else {
	 * 
	 * Main.cliente = listaClientes.elementAt(position); Cliente.save(this,
	 * Main.cliente); Intent formInfoCliente = new Intent(this,
	 * FormInfoClienteActivity.class); startActivityForResult(formInfoCliente,
	 * Const.RESP_PEDIDO_EXITOSO);
	 * 
	 * this.finish(); }
	 * 
	 * }
	 */

	public void cargarInformacionDePedidos() {

		/*
		 * listaPedidos =
		 * DataBaseBOJF.CargarPedidosRealizadosMaMapa(Main.cliente.codigo);
		 * 
		 * if (listaPedidos.size() > 0) {
		 * 
		 * TableLayout table = new TableLayout(this);
		 * table.setBackgroundColor(Color.WHITE);
		 * 
		 * String[] cabecera = {"Producto", "Cantidad"};
		 * Util.HeadersMapas(table, cabecera, this);
		 * 
		 * HorizontalScrollView scroll = (HorizontalScrollView)
		 * dialogAbastecimiento.findViewById(R.id.scrollPedidosRealizados);
		 * scroll.removeAllViews(); scroll.addView(table); int pos = 0;
		 * 
		 * for (Encabezado encabezado : listaPedidos) {
		 * 
		 * pos++;
		 * 
		 * TextView textViewAux; TableRow fila = new TableRow(this);
		 * 
		 * 
		 * textViewAux = new TextView(this);
		 * textViewAux.setText(encabezado.nombre_cliente + "\n");
		 * textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
		 * textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
		 * textViewAux.setTextSize(8); //
		 * textViewAux.setBackgroundDrawable(this.getResources().getDrawable(
		 * android.R.drawable.editbox_dropdown_light_frame));
		 * textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.
		 * drawable.table_cell_row_1)); fila.addView(textViewAux);
		 * 
		 * textViewAux = new TextView(this);
		 * textViewAux.setText(encabezado.codigo_cliente + "\n");
		 * textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
		 * textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
		 * textViewAux.setTextSize(8);
		 * textViewAux.setGravity(View.TEXT_ALIGNMENT_CENTER); //
		 * textViewAux.setBackgroundDrawable(this.getResources().getDrawable(
		 * android.R.drawable.editbox_dropdown_light_frame));
		 * textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.
		 * drawable.table_cell_row_1)); fila.addView(textViewAux);
		 * 
		 * 
		 * table.addView(fila); } }
		 */
	}

	protected void mostrarDialogAbastecimiento(Cliente cliente, final int position) {

		/*
		 * dialogAbastecimiento = null;
		 * 
		 * if (dialogAbastecimiento == null) {
		 * 
		 * dialogAbastecimiento = new Dialog(this);
		 * dialogAbastecimiento.setContentView(R.layout.dialog_mapa);
		 * dialogAbastecimiento.setTitle("CLIENTE: " + cliente.razonSocial);
		 * 
		 * // mostrar el dialog en pantalla dialogAbastecimiento.show();
		 * 
		 * } else {
		 * 
		 * dialogAbastecimiento.show(); }
		 * 
		 * String contenido = "CODIGO: " + cliente.codigo + "\n" + "CEDULA: " +
		 * cliente.cedula + "\n" + "TIPO COMERCIO: " + cliente.tiponegociodes +
		 * "\n" + "DIRECCION: " + cliente.direccion + "\n" + "TEL�FONO: " +
		 * cliente.indicativofijodes + cliente.telefono + "\n" + "CELULAR: " +
		 * cliente.indicativoceldes + cliente.telefonoCel + "\n";
		 * 
		 * TextView mensaje = (TextView)
		 * dialogAbastecimiento.findViewById(R.id.mensaje);
		 * 
		 * mensaje.setText(contenido);
		 * 
		 * Button buttonAceptarDialogResumenPedidoAv = (Button)
		 * dialogAbastecimiento
		 * .findViewById(R.id.buttonAceptarDialogResumenPedidoAv);
		 * buttonAceptarDialogResumenPedidoAv.setText("Ir");
		 * buttonAceptarDialogResumenPedidoAv.setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) { return;
		 * } mLastClickTime = SystemClock.elapsedRealtime();
		 * 
		 * CargarInformacionCliente(position);
		 * 
		 * }
		 * 
		 * });
		 * 
		 * Button Btncancelar = (Button)
		 * dialogAbastecimiento.findViewById(R.id.Btncancelar); LinearLayout
		 * adsa = (LinearLayout)
		 * dialogAbastecimiento.findViewById(R.id.botoncancelar);
		 * adsa.setVisibility(View.VISIBLE); Btncancelar.setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) { return;
		 * } mLastClickTime = SystemClock.elapsedRealtime();
		 * 
		 * if (dialogAbastecimiento != null) { dialogAbastecimiento.cancel();
		 * dialogAbastecimiento = null; }
		 * 
		 * }
		 * 
		 * });
		 * 
		 * cargarInformacionDePedidos();
		 */

	}

	@Override
	public boolean onMarkerClick(Marker c) {

		/*
		 * int position = 0;
		 * 
		 * if (listaClientes != null && !listaClientes.isEmpty()) { for (Cliente
		 * cliente : listaClientes) {
		 * 
		 * if (cliente.razonSocial.equalsIgnoreCase(c.getTitle())) { position =
		 * listaClientes.indexOf(cliente); } }
		 * 
		 * // Util.MostrarAlertDialog(this, listaMarcadores.size()+""); }
		 * Main.cliente = listaClientes.elementAt(position); Cliente.save(this,
		 * Main.cliente); mostrarDialogAbastecimiento(Main.cliente, position);
		 */

		return true;
	}

	/**
	 * Called when the zoom in button (the one with the +) is clicked.
	 */
	public void onZoomIn(View view) {

		changeCamera(CameraUpdateFactory.zoomIn());
	}

	/**
	 * Called when the zoom out button (the one with the -) is clicked.
	 */
	public void onZoomOut(View view) {

		changeCamera(CameraUpdateFactory.zoomOut());
	}

	private void changeCamera(CameraUpdate update) {
		changeCamera(update, null);
	}

	/**
	 * Change the camera position by moving or animating the camera depending on
	 * the state of the animate toggle button.
	 */
	private void changeCamera(CameraUpdate update, CancelableCallback callback) {
		// boolean animated = ((CompoundButton)
		// findViewById(R.id.animate)).isChecked();
		// if (animated) {
		// mMap.getUiSettings().setMyLocationButtonEnabled(true);

		mMap.animateCamera(update, callback);

		// } else {
		// mMap.getUiSettings().setMyLocationButtonEnabled(true);

		// mMap.moveCamera(update);

		// }

	}

}