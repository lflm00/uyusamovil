package celuweb.com.uyusa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Planograma;
import celuweb.com.DataObject.Usuario;

public class FormPlanogramaActivity extends Activity implements Sincronizador {
  ProgressDialog progressDialog;
  Cliente cliente = null;
  Usuario usuario = null;
  String urlDownloadFile = "";
  String nombreArchivo = Const.FILE_NAME;
  boolean enviadoAlServidor = false;
  Vector<Planograma> listaManuales;
  Planograma PlanogramaSelected = null;
  int tipoDeClienteSeleccionado = 1;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.form_planograma);
    
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    initFormPlanogramas();
    crearCarpetaArchivosManuales();
  }
  
  public void crearCarpetaArchivosManuales() {
    File directory;
    try {
      directory = new File(Util.DirApp().getPath() + "/Manuales");
      if (!directory.exists()) {
        // NOTHING
        directory.mkdirs();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  
  public void initFormPlanogramas() {
    
    cliente = Main.cliente;
    usuario = Main.usuario;
    if (cliente==null) {
      tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
  
      if (tipoDeClienteSeleccionado == 1) {
    
        cliente = DataBaseBO.CargarClienteSeleccionado();
    
      } else {
    
        cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
      }
    }
    if (usuario==null) {
      //usuario = DataBaseBO.CargarUsuario();
      usuario = DataBaseBO.ObtenerUsuario();
    }
    
    listaManuales = DataBaseBO.obtenerPlanograma(cliente.Canal);
    
    final LinearLayout lytpdfs = (LinearLayout) findViewById(R.id.lytpdfs);
    lytpdfs.removeAllViews();
    
    LayoutInflater inflater1 = this.getLayoutInflater();
    View viewhead = inflater1.inflate(R.layout.list_item_tabla_manual_head, null);
    TextView textViewSel = (TextView) viewhead.findViewById(R.id.columnx0);
    textViewSel.setVisibility(View.VISIBLE);
    textViewSel.setText("Ver");
    String[] head = { "Codigo", "Nombre" };
    for (int i = 0; i < head.length; i++) {
      TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
      textView.setText(head[i]);
      textView.setVisibility(View.VISIBLE);
    }
    lytpdfs.addView(viewhead);
    
    if (listaManuales != null) {
      for (Planograma Planograma : listaManuales) {
        if (Planograma != null) {
          System.out.println(" ----------- " + Planograma.url);
          agregarItemManual(Planograma, lytpdfs);
        }
      }
    }

		/*if(Planograma!= null){
			urlDownloadFile = Planograma.url;
		}else{
			AlertDialog alertDialog;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
					finish();
				}
			});

			alertDialog = builder.create();
	    	alertDialog.setMessage("No se encontr? el PDF, verifique que est? alojado en el servidor.");
	    	alertDialog.show();
		}
		String[] arrayName = urlDownloadFile.split("/");
		String nombreSubUrl = arrayName[arrayName.length - 1];
		if (!nombreSubUrl.equals("")) {
			nombreArchivo = nombreSubUrl;
		}*/
  }
  
  
  public void agregarItemManual(Planograma Planograma, LinearLayout lytpdfs) {
    
    if (lytpdfs != null) {
      
      LayoutInflater inflater = this.getLayoutInflater();
      View view = inflater.inflate(R.layout.list_item_tabla_manual, null);
      
      String[] data = { Planograma.codigo+"", Planograma.descripcion };
      
      TextView textViewverdetalle = (TextView) view.findViewById(R.id.columnx0);
      textViewverdetalle.setTag(Planograma);
      textViewverdetalle.setText("Ver");
      textViewverdetalle.setVisibility(View.VISIBLE);
      textViewverdetalle.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Planograma Planograma = (Planograma) view.getTag();
          System.out.println("ver manual " + Planograma.nombre);
          if (Planograma != null) {
            PlanogramaSelected = Planograma;
            verManual(Planograma);
          } else {
            Util.MostrarAlertDialog(FormPlanogramaActivity.this, "Error en el manual");
          }
          
        }
      });
      
      for (int i = 0; i < data.length; i++) {
        TextView textView = (TextView) view.findViewWithTag("column" + i);
        textView.setText(data[i]);
        textView.setVisibility(View.VISIBLE);
      }
      lytpdfs.addView(view);
      
    } else {
      Util.MostrarAlertDialog(FormPlanogramaActivity.this, "Error al cargar el modulo");
    }
    
  }
  
  public void verManual(final Planograma Planograma) {
    
    boolean esta = Util.estaArchivo("/Manuales/" + Planograma.nombre);
    
    if (esta) {
      
      // SE MUESTRA ARCHIVO
      Log.i("El Archivo ", "Esta");
      
      AlertDialog.Builder builder = new AlertDialog.Builder(FormPlanogramaActivity.this);
      builder.setMessage("Desea Abrir el Archivo").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
        
        public void onClick(DialogInterface dialog, int id) {
          abrirArchivo(Planograma);
        }
      }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
        
        @Override
        public void onClick(DialogInterface dialog, int which) {
          
          dialog.cancel();
        }
      });
      
      AlertDialog alert = builder.create();
      alert.show();
      
    } else {
      
      // SE DESCARGA Y LUEGO SE MUESTRA
      Log.i("El Archivo ", "No Esta");
      
      AlertDialog.Builder builder = new AlertDialog.Builder(FormPlanogramaActivity.this);
      builder.setMessage("Desea Descargar el Archivo").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
        
        public void onClick(DialogInterface dialog, int id) {
          
          descargarArchivo(Planograma);
          
        }
      }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
        
        @Override
        public void onClick(DialogInterface dialog, int which) {
          
          dialog.cancel();
        }
      });
      
      AlertDialog alert = builder.create();
      alert.show();
    }
  }
  
  
  public void abrirArchivo(Planograma Planograma) {
    
    Intent intent = new Intent();
    intent.setAction(android.content.Intent.ACTION_VIEW);
    File file = new File(Util.DirApp().getPath(), "/Manuales/" + Planograma.nombre);
    MimeTypeMap mime = MimeTypeMap.getSingleton();
    String ext = file.getName().substring(file.getName().indexOf(".") + 1);
    String type = mime.getMimeTypeFromExtension(ext);
    intent.setDataAndType(Uri.fromFile(file), type);
    startActivity(intent);
  }
  
  public void descargarArchivo(Planograma Planograma) {
    
    progressDialog = ProgressDialog.show(FormPlanogramaActivity.this, "", "Descargando Archivo " + Planograma.nombre + "...", true);
    progressDialog.show();
    
    Sync sync = new Sync(FormPlanogramaActivity.this, Const.DESCARGAR_ARCHIVO_PDF);
    sync.URL = Planograma.url;
    sync.nombreArchivo = Planograma.nombre;
    sync.start();
  }
  
  
  
  
  
  
  
  
  public void OnClickFormPlanogramas(View view) {
    
    switch (view.getId()) {
      case R.id.btnAtras:
        finish();
        break;
		/*case R.id.btnDescargar:
			abrirArchivo();
			break;*/
      default:
        break;
    }
  }
  
  
  
  
  
  
  
  public void abrirArchivoXXX() {
    
    File file = new File(Util.DirApp(), Const.RUTA_ARCHIVOS + nombreArchivo);
    
    if (file.exists()) {
      
      Intent verArchivo = new Intent();
      
      verArchivo.setAction(android.content.Intent.ACTION_VIEW);
      
      MimeTypeMap mime = MimeTypeMap.getSingleton();
      String[] arrayExtension = nombreArchivo.split("\\.");
      
      if (arrayExtension.length == 2) {
        
        try {
          String extension = arrayExtension[1];
          String type = mime.getMimeTypeFromExtension(extension);
          verArchivo.setDataAndType(Uri.fromFile(file), type);
          startActivity(verArchivo);
        } catch (Exception e) {
          alertAplicacionInstalada("Debe instalar la aplicaci?n para visualizar el contenido");
        }
      } else {
        Util.MostrarAlertDialog(FormPlanogramaActivity.this,
                                "No se puede abrir el archivo por que esta da?ado");
      }
      
    } else {
      //descargrPDF();
    }
    
  }
  
  public void alertAplicacionInstalada(String mensaje) {
    AlertDialog alertDialog;
    AlertDialog.Builder builder = new AlertDialog.Builder(
                                                           FormPlanogramaActivity.this);
    builder.setCancelable(false).setPositiveButton("Aceptar",
                                                   new DialogInterface.OnClickListener() {
                                                     public void onClick(DialogInterface dialog, int id) {
                                                       dialog.cancel();
                                                       abrirPlayStorePolaris(Const.PAQUETES_POLARIS);
                                                     }
                                                   });
    alertDialog = builder.create();
    alertDialog.setMessage(mensaje);
    alertDialog.show();
  }
  
  public void abrirPlayStorePolaris(String paquete) {
    String url = "";
    try {
      // Check whether Google Play store is installed or not:
      this.getPackageManager().getPackageInfo("com.android.vending", 0);
      url = "market://details?id=" + paquete;
    } catch (final Exception e) {
      url = "https://play.google.com/store/apps/details?id=" + paquete;
    }
    // Open the app page in Google Play store:
    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                      | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    startActivity(intent);
  }
  
  public void descargarPDF() {
    progressDialog = ProgressDialog.show(this, "", "Descargando Archivo...", true);
    progressDialog.show();
    Sync sync = new Sync(FormPlanogramaActivity.this, Const.DESCARGAR_ARCHIVO_PDF);
    sync.URL = urlDownloadFile;
    sync.nombreArchivo = nombreArchivo;
    sync.start();
  }
  
  
  
  @Override
  public void RespSync(final boolean ok, String respuestaServer, final String msg,
                       final int codeRequest) {
    
    switch (codeRequest) {
      
      case Const.DESCARGAR_ARCHIVO_PDF:
        
        if (progressDialog != null)
          progressDialog.cancel();
        
        this.runOnUiThread(new Runnable() {
          
          @Override
          public void run() {
            if (ok && codeRequest == Const.DESCARGAR_ARCHIVO_PDF) {
              AlertDialog alertDialog;
              AlertDialog.Builder builder = new AlertDialog.Builder(
                                                                     FormPlanogramaActivity.this);
              builder.setCancelable(false).setPositiveButton("Si",
                                                             new DialogInterface.OnClickListener() {
                                                               public void onClick(DialogInterface dialog,
                                                                                   int id) {
                                                                 abrirArchivo(PlanogramaSelected);
                                                                 dialog.cancel();
                                                               }
                                                             });
              builder.setCancelable(false).setNegativeButton("No",
                                                             new DialogInterface.OnClickListener() {
                                                               public void onClick(DialogInterface dialog,
                                                                                   int id) {
                                                                 dialog.cancel();
                                                               }
                                                             });
              alertDialog = builder.create();
              alertDialog
                .setMessage("Se descarg? el archivo exitosamente\n?Desea abrir el archivo?");
              alertDialog.show();
            } else if (!ok) {
              Util.MostrarAlertDialog(FormPlanogramaActivity.this,
                                      msg);
            }
          }
        });
        break;
    }
    
  }
  
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      finish();
      return true;
    }
    return super.onKeyDown(keyCode, event);
  }
}
