package celuweb.com.uyusa;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;


/**
 * Clase encargada de contener los metodos los cuales generan las tirillas de
 * impresion
 * 
 * @author Juan Carlos Hidalgo <Celuweb>
 * 
 */
public class ReporstPrinter {

	// atribuctos necesarios para la impresion
	ProgressDialog progressDialog;
	protected static int _splashTime = 1000;
	protected static int _splashTime2 = 4000;

	static Thread splashTread;

	public static float valorRecogido = 0;
	public static float valorNoRecogido = 0;
	public static float valorDevolucionParcial = 0;



	// constructor de la clase
	public ReporstPrinter() {
	}



	/**
	 * Metodo encargado de generar la tirilla de prueba (Establecer Impresora)
	 * 
	 * @return
	 */
	public static String formatoPrueba() {

		char ret1 = 13;
		char ret2 = 10;

		String ret = String.valueOf(ret1) + String.valueOf(ret2);
		String ESC = "\u001B";
		String fontSmall = ESC + "w" + "\u0025";// font char small block MF226
												// %(25H)
		String formato = fontSmall;
		formato += "";
		formato += fontSmall + Util.CentrarLinea("Pepsico Rutas Directas Autoventa", 42) + ret;
		formato += Util.CentrarLinea("Fecha: " + Util.FechaActual("yyyy-MM-dd HH:mm:ss"), 42) + ret + ret;
		formato += Util.CentrarLinea("TIRILLA DE PRUEBAS", 42) + ret + ret;
		formato += Util.lpad("", 42, "-") + ret + ret;
		
		return formato;
	}



	/**
	 * Metodo encargado de armar el formato de devolucion
	 * 
	 * @return
	 */
	public static String formatoDevolucion(boolean isCheckIn) {
		return "";
	}



	/**
	 * Metodo encargado de calcular el tamano maximo de un vector para cargar
	 * los espacios
	 * 
	 * @param cadenas
	 * @return
	 */
	public static int calcularEspacios(Vector<String> cadenas) {
		int espacio = 0;
		int auxiliar = 0;
		for (String cadena : cadenas) {
			auxiliar = cadena.length();
			if (espacio < auxiliar)
				espacio = auxiliar;
		}
		return espacio;
	}



	/**
	 * Metodo encargado de agregar espacios a las una cadena
	 * 
	 * @param cadena
	 * @param espacio
	 * @return
	 */
	public static String agregarEspacioCadena(String cadena, int espacio) {
		String cadenaEspacio = "";
		if (espacio > cadena.length()) {
			int numeroEspacios = espacio - cadena.length();
			for (int i = 0; i < numeroEspacios; i++) {
				cadenaEspacio += " ";
			}
		}
		return (cadenaEspacio + cadena);
	}



	/**
	 * Metodo encargado de imprimir las tirillas de la impresora Apex
	 * 
	 * @param socket
	 * @param textoImprimir
	 */
	public static void ImprimiendoPrinter(final BluetoothSocket socket, final String textoImprimir) {

		splashTread = new Thread() {
			@Override
			public void run() {

				try {
					synchronized (this) {
						wait(_splashTime);
					}

				} catch (InterruptedException e) {
				} finally {
					OutputStream salida = null;
					try {
						
						
						char ret1 = 13;
						char ret2 = 10;
						String enter = String.valueOf(ret1) + String.valueOf(ret2);
						
						String textoImprimirfinal = textoImprimir + enter+enter+enter;
						
						salida = socket.getOutputStream();
						
						synchronized (this) {

							wait(_splashTime);
						}
						
						salida.write(textoImprimirfinal.getBytes());

						synchronized (this) {

							wait(_splashTime2);
						}

						salida.flush();
						
						

					} catch (Exception e2) {

//						String e = e2.getMessage();
						e2.printStackTrace();

					} finally {

						try {

							if (socket != null) {
								socket.close();
							}
							salida.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		};
		splashTread.start();
	}
}