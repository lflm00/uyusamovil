package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.RegCanastilla;
import celuweb.com.DataObject.Usuario;

public class FormCanastillasActivity extends Activity  { 

	public static final String TAG = FormCanastillasActivity.class.getName();
	
	Typeface fontMM;
	Typeface font;
	String cod_cliente;
	ProgressDialog progressDialog;
	boolean salir;
	private Vector<RegCanastilla> listaProductos;
	Cliente cliente = null;
	Usuario usuario = null;
	Dialog dialogEnviarInfo;
	String nroDocSesion = "";
	boolean tienePedido = false;
	private long mLastClickTime = 0;
	Cliente Cliente;
	boolean isClienteNuevo = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_canastillas);
		
		Util.closeTecladoStartActivity(this);
		cargarBundle();
	}
	
	
	private void cargarBundle() {
		
		String codigoCliente="";
		Bundle bundle = getIntent().getExtras();
		nroDocSesion = bundle.getString("nroDocSesion");
		tienePedido = bundle.getBoolean("isPedido");
		
		if (bundle != null) {
			isClienteNuevo = bundle.getBoolean("clienteNuevo");
			codigoCliente = bundle.getString("codigoCliente");
		}
		
		if(codigoCliente != null) {
			if(isClienteNuevo)
				cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
			else
				cliente = DataBaseBO.BuscarCliente(codigoCliente);
		}
		
		System.out.println("Cargo NroDoc --> "+ nroDocSesion);
		
	}


	@Override
	public void onResume() {
		super.onResume();
		usuario = Main.usuario;
		
		if (cliente==null) {
			cliente = DataBaseBO.CargarClienteSeleccionado();
		}
		if (usuario==null) {
			usuario = DataBaseBO.CargarUsuario();
		}
		
		cargarListaCanastilla();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	


	public boolean cargarListaCanastilla(){

		boolean state = false;
	
		listaProductos = DataBaseBO.listaCanastilla(cliente.codigo, nroDocSesion, tienePedido, Const.SIN_LIQUIDACION);
		
		if (listaProductos != null) {

			TableLayout tablaHeadListaProductosInv = (TableLayout) findViewById(R.id.tablaHeadListaClientesPadre);
			tablaHeadListaProductosInv.removeAllViews();

			LayoutInflater inflater1 = this.getLayoutInflater();
			View viewhead = inflater1.inflate(R.layout.list_item_tabla_head_canastilla, null);
			
			if(tienePedido){
				
				String[] head = { "Codigo", "Nombre", "Entregada", "Devuelta" };
				
				for (int i = 0; i < head.length; i++) {
					TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
					textView.setText(head[i]);
					textView.setVisibility(View.VISIBLE);	
				}
				
			}else{
				
				String[] head = { "Codigo", "Nombre",  "Devuelta" };
				
				for (int i = 0; i < head.length; i++) {
					TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
					textView.setText(head[i]);
					textView.setVisibility(View.VISIBLE);	
				}
			}

			
			tablaHeadListaProductosInv.addView(viewhead);

			TableLayout tablaBodyListaProductosInventario = (TableLayout) findViewById(R.id.tablaBodyListaClientesPadre);
			tablaBodyListaProductosInventario.removeAllViews();
			int position = 0;
			for (RegCanastilla clPadre : listaProductos) {
				if(pintarDetalleChequeo(clPadre, tablaBodyListaProductosInventario, position)){
					state = true;
				}
				position++;
			}
		} else {

			TableLayout tablaListaAgendaEjecutiva = (TableLayout) findViewById(R.id.tablaHeadListaClientesPadre);
			tablaListaAgendaEjecutiva.removeAllViews();
			state = false;
			Util.mostrarToast(FormCanastillasActivity.this, "No existen Canastillas.");
		}
		return state;
	}
	
	public boolean pintarDetalleChequeo(final RegCanastilla item, LinearLayout tablaHeadListaProductosInv, int position) {

		LayoutInflater inflater = this.getLayoutInflater();
		View view = inflater.inflate(R.layout.list_item_tabla_canastilla, null);

		TableRow rowTableElemento = (TableRow)view.findViewById(R.id.rowTableElemento);
		rowTableElemento.setTag(item);
		
		TextView codigo = (TextView) view.findViewById(R.id.column0);
		codigo.setText(" "+item.codigo);
		codigo.setTag(item);
		codigo.setVisibility(View.VISIBLE);
		if ( position % 2 == 1 ) {
			codigo.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		TextView descripcion = (TextView) view.findViewById(R.id.column1);
		descripcion.setText(item.descripcion);
		descripcion.setTag(item);
		descripcion.setVisibility(View.VISIBLE);
		if ( position % 2 == 1 ) {
			descripcion.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		
		if (tienePedido) {
			
			boolean tieneRegistros = DataBaseBO.tieneRegistroCanastilla(item.codigo, nroDocSesion);
			
			LinearLayout columnCantidad = (LinearLayout) view.findViewById(R.id.column2);
			columnCantidad.setVisibility(View.VISIBLE);

			EditText edtEntregada = (EditText) view.findViewById(R.id.edTxtEntregado);
			if (tieneRegistros) {
				edtEntregada.setText(item.entregada);
			} else {
				edtEntregada.setText("0");
				edtEntregada.setEnabled(false);
			}
			edtEntregada.setTag(item);
			if (position % 2 == 1) {
				columnCantidad
						.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
			}
			
		}
		
		LinearLayout columnRecibida = (LinearLayout) view.findViewById(R.id.column3);
		columnRecibida.setVisibility(View.VISIBLE);
		
		EditText edtRecibida = (EditText) view.findViewById(R.id.edTxtRecibido);
		edtRecibida.setText(item.devuelta);
		edtRecibida.setTag(item);
		
		if ( position % 2 == 1 ) {
			columnRecibida.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		//rowTableElemento.setOnClickListener(this);
		
		tablaHeadListaProductosInv.addView(view);
		return true;

	}


	
	public void salir() {
		
		if(tienePedido)
			finish(); 
		else{
			setResult(Activity.RESULT_OK);
			finish();
			}
	}
	
	public void cancelar() {
		finish();
	}
	
	public void onClickGuardar(View view) {
		
		if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
            return;
        }
		
        mLastClickTime = SystemClock.elapsedRealtime();
		
		
		Usuario user = DataBaseBO.ObtenerUsuario();
		
		Vector<RegCanastilla> listaItemCanastillaToSave = new Vector<RegCanastilla>();
		
		TableLayout tablaBodyListaContactos= (TableLayout) findViewById(R.id.tablaBodyListaClientesPadre);

		for (int i = 0; i < tablaBodyListaContactos.getChildCount(); i++) {

			TableRow rowTableElemento = (TableRow) tablaBodyListaContactos.getChildAt(i);
			RegCanastilla item = (RegCanastilla) rowTableElemento.getTag();
			
			EditText edtEntregado = null;
			EditText edtRecibida =(EditText) rowTableElemento.findViewById(R.id.edTxtRecibido);
			
			if (tienePedido) {
				
				edtEntregado = (EditText) rowTableElemento.findViewById(R.id.edTxtEntregado);
				item.entregada = edtEntregado.getText().toString();
			}
			
			item.devuelta = edtRecibida.getText().toString();

			RegCanastilla regCanastilla = new RegCanastilla();

			regCanastilla.codigo = item.codigo;
			regCanastilla.nroDoc = nroDocSesion;
			regCanastilla.devuelta = item.devuelta != null ? item.devuelta: "0" ;
			regCanastilla.codCliente = cliente.codigo;
			regCanastilla.vendedor = user.codigoVendedor;
			
			if (tienePedido) {
				
				regCanastilla.entregada = item.entregada;
				RegCanastilla canasta = DataBaseBO.obtenerCantidadMinimaCanastillas(regCanastilla.codigo, nroDocSesion);
				
				
				if (regCanastilla.entregada.equals("")) {
					
					Util.MostrarAlertDialog(this, "Por favor ingrese una cantidad para " + item.descripcion+".");
					return;
					
				} else {
					
					int cantEntrega = Integer.parseInt(regCanastilla.entregada);
					
					
					if(canasta!= null){
						
						System.out.println("-- RegCanastaAGuardar --\nCodigo:"+regCanastilla.codigo+"\nEntregados: "+cantEntrega+"\n-----------");
						System.out.println("-- Canasta Minima que debe ingresar --\nCodigo:"+canasta.codigo+"\nEntregados: "+canasta.entregaMin+"\n-----------\n");
						
						if (regCanastilla.codigo.equals(canasta.codigo)) {

							if (cantEntrega > canasta.entregaMin) {

								Util.MostrarAlertDialog(this, "La cantidad de canastillas que ingreso, es mayor al necesario. \n\n"+ item.descripcion + ": "+canasta.entregaMin);
								return;

							} else if (cantEntrega < canasta.entregaMin) {

								Util.MostrarAlertDialog(this,"La cantidad de canastillas que ingreso, es menor al necesario. \n\n" + item.descripcion + ": "+canasta.entregaMin);
								return;

							} else {

								listaItemCanastillaToSave.addElement(regCanastilla);
							}
						}
					}else{
						System.out.println("Cod."+regCanastilla.codigo + " Cant:"+regCanastilla.cantidad);
						listaItemCanastillaToSave.addElement(regCanastilla);
					}
					
				}
			}else{
				
				listaItemCanastillaToSave.addElement(regCanastilla);
			}
			

		}
		
		if (listaItemCanastillaToSave.size() > 0) {

			if (DataBaseBO.guardarRegCanastilla(listaItemCanastillaToSave, tienePedido, Const.SIN_LIQUIDACION, cliente.codigo )) {
				mensajeGuardar("Guardo el registro exitosamente.");
			} else {
				Util.MostrarAlertDialog(FormCanastillasActivity.this,
						"Ocurrio un error al guardar el registro.");
				return;
			}
		}else{
			Util.mostrarToast(FormCanastillasActivity.this, "No tiene registros por guardar.");
			return;
		}
		

	}
	
	protected void mensajeGuardar(String mensaje) {
		
		AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(FormCanastillasActivity.this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				
				
				dialog.cancel();
				salir();
			}
		});
		alertDialog = builder.create();
		alertDialog.setMessage(mensaje);
		alertDialog.show();
		
	}


	public void onClickSalir(View view) {
		cancelar();  	
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cancelar(); 
		}
		return true;
	}

	
	public void onClickRegresar(View view) {

		finish();

	}

	
	
}
