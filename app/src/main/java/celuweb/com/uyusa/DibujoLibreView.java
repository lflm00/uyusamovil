package celuweb.com.uyusa;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout.LayoutParams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DibujoLibreView extends View implements OnTouchListener {

	private Path mPath;
	private Paint mPaint;
	
	private Paint p = new Paint();

	private float mX, mY;
	private static final float TOUCH_TOLERANCE = 4;

	private Bitmap mBitmap;

	private int ancho;
	private int alto;

	public boolean isDraw = true;

	public boolean clear = false;

	public DibujoLibreView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		initPaint();
		setOnTouchListener(this);
	}

	public DibujoLibreView(Context context, AttributeSet attrs) {
		super(context, attrs);

		initPaint();
		setOnTouchListener(this);
	}

	public DibujoLibreView(Context context) {
		super(context);

		initPaint();
		setOnTouchListener(this);
	}

	private void initPaint() {

		mPath = new Path();

		mPaint = new Paint();

		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(Color.BLACK);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(3);
	}

	public void setLayoutParams(LayoutParams parametros) {

		super.setLayoutParams(parametros);

		ancho = parametros.width;
		alto = parametros.height;
	}

	@Override
	public void onDraw(Canvas canvas) {

		p.setColor(Color.BLACK);

		canvas.drawColor(Color.WHITE);

		if (mBitmap != null) {
			canvas.drawBitmap(mBitmap, 0, 0, p);
		}

		if (clear) {
			mBitmap = null;

			Paint p = new Paint();
			p.setColor(Color.WHITE);

			canvas.drawRect(0, 0, ancho, alto, p);
			clear = false;
		}

		if (mPath != null)
			canvas.drawPath(mPath, mPaint);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		int x = (int) event.getX();
		int y = (int) event.getY();

		switch (event.getAction()) {

			case MotionEvent.ACTION_DOWN:

				if (isDraw)
					touch_start(x, y);

				break;
			case MotionEvent.ACTION_UP:

				if (isDraw)
					touch_up();

				break;
			case MotionEvent.ACTION_MOVE:

				if (isDraw)
					touch_move(x, y);

				break;
		}
		invalidate();
		return true;
	}

	private void touch_start(float x, float y) {

		if (mPath != null) {
			mPath.moveTo(x, y);
			mX = x;
			mY = y;
		}
	}

	private void touch_move(float x, float y) {

		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);

		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {

			mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
			mX = x;
			mY = y;
		}
	}

	private void touch_up() {

		if (mPath != null) {
			mPath.lineTo(mX, mY);
		}
	}

	public void borrarDibujo() {

		if (mPath != null) {
			mPath.reset();

			clear = true;
			invalidate();
		}
	}

	public boolean dibujar() {

		if (isDraw)
			isDraw = false;
		else
			isDraw = true;

		return isDraw;
	}

	public void guardarDibujo(String name) throws IOException {

		FileOutputStream fOut = null;

		try {

			Bitmap bitmap = Bitmap.createBitmap(ancho, alto, Bitmap.Config.ARGB_8888);

			Canvas canvas = new Canvas(bitmap);
			this.draw(canvas);

			File dir = new File(Util.DirApp() + "/");

			if (!dir.exists())
				dir.mkdirs();

			File file = new File(dir, name + ".jpg");

			fOut = new FileOutputStream(file);

			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);

		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		} finally {

			if (fOut != null) {
				fOut.flush();
				fOut.close();
			}
		}
	}

	public void setFirma(byte[] firma) {

		mBitmap = Bitmap.createBitmap(ancho, alto, Bitmap.Config.ARGB_8888);
		mBitmap = BitmapFactory.decodeByteArray(firma, 0, firma.length).copy(Bitmap.Config.ARGB_8888, true);
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		return false;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public Bitmap getBitmap() {
		
		/*if(mPath.isEmpty())
			return null;*/

		Bitmap bitmap = null;

		try {

			bitmap = Bitmap.createBitmap(ancho, alto, Bitmap.Config.RGB_565);
			
			Canvas canvas = new Canvas(bitmap);
			this.draw(canvas);
			
		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		}

		return bitmap;
	}

	public Bitmap getBitmap(int ancho, int alto) {
		
		if(mPath.isEmpty())
			return null;

		Bitmap bitmap = Bitmap.createBitmap(ancho, alto, Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		this.draw(canvas);

		return bitmap;
	}

	public void setBitmap(Bitmap mBitmap) {
		this.mBitmap = mBitmap;
	}

}