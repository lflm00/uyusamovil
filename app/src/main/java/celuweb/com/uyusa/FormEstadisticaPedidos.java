package celuweb.com.uyusa;

import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;
import celuweb.com.printer.sewoo.SewooLKP20;

@SuppressLint("NewApi")
public class FormEstadisticaPedidos extends CustomActivity {

    Dialog dialogResumen;
    Vector<Encabezado> listaPedidos;
    ProgressDialog progressDialog;
    private String macImpresora;
    String mensaje;
    int tipoTranns;
    int pedido = 1;
    boolean fallo = false;

    public long tiempoClick1 = 0;

    /**
     * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
     */
    private SewooLKP20 sewooLKP20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_estadistica_pedidos);

        Inicializar();
        CargarResumenPedidos();
        SetListenerListView();
        //Util.BlueetoothON();
    }

    public void Inicializar() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            if (bundle.containsKey("pedido")) {

                pedido = bundle.getInt("pedido");
            } else {

                pedido = 1;
            }
        }
    }


    public void CargarResumenPedidos() {

        int sync = 1;

        if (pedido == 1)
            sync = 1;
        else if (pedido == 0)
            sync = 0;
        else if (pedido == -1)
            sync = 1;

        ItemListView[] listaItems = null;

        Vector<ItemListView> listaItemsCliente = new Vector<ItemListView>();

        if (pedido == 1)
            listaPedidos = DataBaseBO.CargarVisitasRealizadas(Main.usuario.fechaConsecutivo, listaItemsCliente, sync);
        else {
            if (pedido == 0)
                listaPedidos = DataBaseBO.CargarPedidosRealizados(Main.usuario.fechaConsecutivo, listaItemsCliente, 0);
            else if (pedido == -1)
                listaPedidos = DataBaseBO.CargarPedidosRealizadosCN(Main.usuario.fechaConsecutivo, listaItemsCliente, 0);

        }
        if (listaItemsCliente.size() > 0) {

            listaItems = new ItemListView[listaItemsCliente.size()];
            listaItemsCliente.copyInto(listaItems);

            ListViewAdapter adapter = new ListViewAdapter(this, listaItems, R.drawable.cliente, 0x2E65AD);
            ListView listaPedidosRealizados = (ListView) findViewById(R.id.listaPedidosRealizados);
            listaPedidosRealizados.setAdapter(adapter);

        } else {

            ListViewAdapter adapter = new ListViewAdapter(this, new ItemListView[]{}, R.drawable.cliente, 0x2E65AD);
            ListView listaPedidosRealizados = (ListView) findViewById(R.id.listaPedidosRealizados);
            listaPedidosRealizados.setAdapter(adapter);
        }

        int size = listaPedidos.size();

        if (pedido == 1) {

            String msg = "<b>Visitas realizadas: " + size + "</b>";
            ((TextView) findViewById(R.id.lblTitulo)).setText(Html.fromHtml(msg));

        } else {

            if (pedido == 0) {

                String msg = "<b>Pedidos sin sincronizar: " + size + "</b>";
                ((TextView) findViewById(R.id.lblTitulo)).setText(Html.fromHtml(msg));

            } else {

                if (pedido == -1) {

                    String msg = "<b>Pedidos clientes nuevos: " + size + "</b>";
                    ((TextView) findViewById(R.id.lblTitulo)).setText(Html.fromHtml(msg));

                }

            }

        }
    }


    public void SetListenerListView() {

        ListView listaPedidosRealizados = (ListView) findViewById(R.id.listaPedidosRealizados);
        listaPedidosRealizados.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Date date = new Date();

                long tiempoClick2 = date.getTime();

                long dif1 = tiempoClick2 - tiempoClick1;

                if (dif1 < 0) {

                    dif1 = dif1 * (-1);

                }

                if (dif1 > 3000) {

                    tiempoClick1 = tiempoClick2;

                    Encabezado encabezado = listaPedidos.elementAt(position);
                    MostarDialogResumen(encabezado);

                }

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Const.RESP_PEDIDO_EXITOSO) {
            CargarResumenPedidos();
        }
    }


    public void MostarDialogResumen(final Encabezado encabezado) {

        dialogResumen = new Dialog(this);
        dialogResumen.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialogResumen.setContentView(R.layout.resumen_pedido);

        Vector<Detalle> listaDetalle = new Vector<Detalle>();

        if (encabezado.sincronizado == 0)
            ((Button) dialogResumen.findViewById(R.id.buttonAnular)).setVisibility(View.VISIBLE);
        else
            ((Button) dialogResumen.findViewById(R.id.buttonAnular)).setVisibility(View.VISIBLE);

        String titulo = (encabezado.tipoTrans == 2) ? "Devolucion" : "Venta";
        dialogResumen.setTitle("Resumen " + titulo);

        encabezado.sub_total = 0;
        encabezado.total_iva = 0;
        encabezado.valor_descuento = 0;


        ItemListView itemListView;
//		Hashtable<String, Detalle> detallePedido = DataBaseBO.CargarDetallePedido(encabezado.numero_doc);
        listaDetalle = DataBaseBO.CargarDetallePedidoVector(encabezado.numero_doc);

        Vector<ItemListView> datosPedido = new Vector<ItemListView>();

        int i = 0;

        for (Detalle detalle : listaDetalle) {

            i = i + 1;
            itemListView = new ItemListView();

            float sub_total = detalle.cantidad * detalle.precio;
            float valor_descuento = sub_total * detalle.descuento / 100f;
            float valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100f);

            encabezado.sub_total += sub_total;
            encabezado.total_iva += valor_iva;
            encabezado.valor_descuento += valor_descuento;

            if (encabezado.tipoTrans == 0) {

                if (detalle.cantidadCambio > 0 && detalle.cantidad > 0) {
                    itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                    itemListView.subTitulo = "Precio: " + detalle.precio + " - Iva: " + detalle.iva + "% - Cant: " + detalle.cantidad + " - Desc: "
                            + detalle.descuento + "% \nCantDevolucion: " + detalle.cantidadCambio;
                    datosPedido.addElement(itemListView);
                } else if (detalle.cantidadCambio > 0 && detalle.cantidad == 0) {
                    itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                    itemListView.subTitulo = "Precio: " + detalle.precio + " - Iva: " + detalle.iva + "%  \nCantDevolucion: " + detalle.cantidadCambio;
                    datosPedido.addElement(itemListView);
                } else {
                    itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                    itemListView.subTitulo = "Precio: " + detalle.precio + " - Iva: " + detalle.iva + "% - Cant: " + detalle.cantidad + " - Desc: "
                            + detalle.descuento + "% ";
                    datosPedido.addElement(itemListView);
                }


            } else {

                if (encabezado.tipoTrans == 2) {

                    itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
                    itemListView.subTitulo = "Precio: " + detalle.precio + " - Cant. Devolucion: " + detalle.cantidad;
                    ;
                    datosPedido.addElement(itemListView);

                }
            }

        }


        ItemListView[] listaItems = new ItemListView[datosPedido.size()];
        datosPedido.copyInto(listaItems);

        encabezado.valor_neto = encabezado.sub_total + encabezado.total_iva - encabezado.valor_descuento;

        System.out.println("Total productos --> " + i);
        System.out.println("Subtotal---> " + encabezado.sub_total);
        System.out.println("IVA---> " + encabezado.total_iva);
        System.out.println("Descuento---> " + encabezado.valor_descuento);
        System.out.println("ValorNeto---> " + encabezado.valor_neto);

        encabezado.str_sub_total = Util.SepararMiles(Util.Redondear(Util.QuitarE(Util.getDecimalFormat(encabezado.sub_total)), 0));
        encabezado.str_valor_neto = Util.SepararMiles(Util.Redondear(Util.QuitarE(Util.getDecimalFormat(encabezado.valor_neto)), 0));

        ((TextView) dialogResumen.findViewById(R.id.lblCliente)).setText(encabezado.nombre_cliente);

        if (encabezado.tipoTrans == 0)
            ((TextView) dialogResumen.findViewById(R.id.lblValorNetoPedido)).setText(encabezado.str_valor_neto);
        else if (encabezado.tipoTrans == 2)
            ((TextView) dialogResumen.findViewById(R.id.lblValorNetoPedido)).setText(encabezado.str_valor_neto);
        else
            ((TextView) dialogResumen.findViewById(R.id.lblValorNetoPedido)).setText("0");

        ListViewAdapterPedido adapter = new ListViewAdapterPedido(this, listaItems, R.drawable.compra, 0);
        ListView listaResumenPedido = (ListView) dialogResumen.findViewById(R.id.listaResumenPedido);
        listaResumenPedido.setAdapter(adapter);

        ((Button) dialogResumen.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                dialogResumen.cancel();
            }
        });

        ((Button) dialogResumen.findViewById(R.id.buttonAnular)).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                dialogResumen.cancel();

                if (!DataBaseBO.existeRegistroLiquidacion()) {

                    if (encabezado.anulado == 1) {
                        Toast.makeText(FormEstadisticaPedidos.this, "Ya esta anulado.", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        mostrarAlertAnular(encabezado);
                        return;
                    }

                } else {
                    Util.MostrarAlertDialog(FormEstadisticaPedidos.this, "Ya ha sido realizada la liquidacion.");
                }


            }
        });

        ((Button) dialogResumen.findViewById(R.id.btnImprimir)).setOnClickListener(new OnClickListener() {

            public void onClick(View view) {


                boolean isPedido = true;

                if (!(encabezado.tipoTrans == 0))
                    isPedido = false;


                progressDialog = ProgressDialog.show(FormEstadisticaPedidos.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
                progressDialog.show();

                SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

                SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");

                if (macImpresora.equals("-")) {

                    if (progressDialog != null)
                        progressDialog.cancel();
                    Util.MostrarAlertDialog(FormEstadisticaPedidos.this, "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");


                } else {
                    boolean isAnulado = false;

                    if (encabezado.anulado == 1)
                        isAnulado = true;

                    if (!tipoImpresora.equals("Intermec")) {
                        sewooLKP20 = new SewooLKP20(FormEstadisticaPedidos.this);
                        imprimirSewooLKP20(macImpresora, encabezado.numero_doc, "", isPedido, isAnulado, encabezado.codigo_cliente);


                    } else {
                        imprimirTirillaGeneral(macImpresora, encabezado.numero_doc, encabezado.tipoTrans, isAnulado, encabezado.codigo_cliente);
                    }


                }
            }
        });

        dialogResumen.setCancelable(false);
        dialogResumen.show();
        dialogResumen.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.cliente);
    }


    /**
     * Preguntar al usuario si realmente desea anular la venta o la devolucion.
     *
     * @param encabezado
     */
    protected void mostrarAlertAnular(final Encabezado encabezado) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormEstadisticaPedidos.this);
        builder.setIcon(R.drawable.cliente_anulado);
        builder.setTitle("Anular?");
        builder.setMessage("Aceptar para anular el registro seleccionado.");

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                anularRegistroSeleccionado(encabezado);
                return;
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                return;
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    /**
     * Anular el encabezado seleccionado por el usuario.
     * Cambiar el estado del campo a anulado = 1;
     *
     * @param encabezado
     */
    protected void anularRegistroSeleccionado(Encabezado encabezado) {

        boolean anulado = DataBaseBO.anularEncabezado(encabezado);
        if (anulado) {
            Util.MostrarAlertDialog(FormEstadisticaPedidos.this, "Anulado Correcto.");
            CargarResumenPedidos();
        } else {
            Util.MostrarAlertDialog(FormEstadisticaPedidos.this, "Error!\nIntente nuevamente.");
        }
    }


    @Override
    protected void onResume() {

        super.onResume();

        Inicializar();
        CargarResumenPedidos();
    }


    private void imprimirTirillaGeneral(final String macAddress, final String numeroDoc, final int tipoTrans, final boolean isAnulado, final String codCliente) {

        new Thread(new Runnable() {

            public void run() {

                mensaje = "";
                BluetoothSocket socket = null;


                try {

                    Looper.prepare();

                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                    if (bluetoothAdapter == null) {

                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else if (!bluetoothAdapter.isEnabled()) {

                        mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

                    } else {

                        BluetoothDevice printer = null;

                        printer = bluetoothAdapter.getRemoteDevice(macAddress);

                        if (printer == null) {

                            mensaje = "No se pudo establecer la conexion con la Impresora.";

                        } else {

                            fallo = true;

                            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

                            SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                            String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");

                            if (tipoImpresora.equals("Intermec")) {

                                socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);
                            } else {
                                socket = printer.createRfcommSocketToServiceRecord(uuid);
                            }

                            if (socket != null) {

                                socket.connect();

                                Thread.sleep(3500);
                                System.out.println("TipoTrans PEDIDO O DEV?-->: " + tipoTrans + "\n tipoImpresora: " + tipoImpresora);

                                if (tipoImpresora.equals("Intermec")) {

                                    if (tipoTrans == 0) {

                                        System.out.println("Imprimir Pedido INTERMEC....");
                                        ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoVentaPedidoItermerc(numeroDoc, "", isAnulado, true));


                                        String nroDocDevolucion = DataBaseBO.obtenerNroDocDevAmarre(numeroDoc, codCliente);

                                        if (!nroDocDevolucion.equals("")) {
                                            ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoDevolucionItermec(nroDocDevolucion, "", false, false));
                                        }

                                    } else if (tipoTrans == 2) {
                                        System.out.println("Imprimir Devolucion INTERMEC...");
                                        ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoDevolucionItermec(numeroDoc, "", isAnulado, false));
                                    }

                                } else {

                                    if (tipoTrans == 0) {
                                        System.out.println("Imprimir Pedido OTRA IMPRESORA....");
                                        ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoVentaPedido(numeroDoc, ""));
                                    } else if (tipoTrans == 2) {
                                        System.out.println("Imprimir Devolucion OTRA IMPRESORA....");
                                        ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoDevolucion(numeroDoc, ""));
                                    }

                                }

                                // ReporstPrinter.ImprimiendoPrinter(socket,
                                // PrinterBO.formatoVentaPedido( numeroDoc));

                                handlerFinish.sendEmptyMessage(0);

                            } else {

                                mensaje = "No se pudo abrir la conexion con la Impresora.\n\nPor Favor intente de nuevo.";
                            }

                        }
                    }

                    if (!mensaje.equals("")) {

                        handlerFinish.sendEmptyMessage(0);
                    }

                    Looper.myLooper().quit();

                } catch (Exception e) {

                    String motivo = e.getMessage();

                    mensaje = "No se pudo ejecutar la Impresion.";

                    if (motivo != null) {
                        mensaje += "\n\n" + motivo;
                    }

                    handlerFinish.sendEmptyMessage(0);

                } finally {

                }
            }

        }).start();

    }

    private Handler handlerFinish = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (progressDialog != null)
                progressDialog.cancel();

            if (!fallo)
                Util.MostrarAlertDialog(FormEstadisticaPedidos.this, mensaje);

        }
    };

    /**
     * Imprimir la factura del pedido.
     *
     * @param macImpresora
     * @param numero_doc
     * @param copiaPrint
     */
    protected void imprimirSewooLKP20(final String macImpresora, final String numero_doc, final String copiaPrint, final boolean isPedido, final boolean isAnulado, final String codCliente) {

        final Usuario usuario = DataBaseBO.ObtenerUsuario();


        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();

                if (macImpresora.equals("-")) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(FormEstadisticaPedidos.this, "Aun no hay Impresora Predeterminada.\n\nPor Favor primero Configure la Impresora!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {

                    if (sewooLKP20 == null) {
                        sewooLKP20 = new SewooLKP20(FormEstadisticaPedidos.this);
                    }
                    int conect = sewooLKP20.conectarImpresora(macImpresora);

                    switch (conect) {
                        case 1:
                            String nroDocDevolucion = DataBaseBO.obtenerNroDocDevAmarre(numero_doc, codCliente);
                            sewooLKP20.generarEncabezadoTirilla(numero_doc, usuario, copiaPrint, isPedido, isAnulado, false, 0, false);

                            if (!nroDocDevolucion.equals(""))
                                sewooLKP20.generarEncabezadoTirilla(nroDocDevolucion, usuario, copiaPrint, false, false, false, 1, false);

                            sewooLKP20.imprimirBuffer(true);
                            break;

                        case -2:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormEstadisticaPedidos.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;

                        case -8:
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Util.MostrarAlertDialog(FormEstadisticaPedidos.this, "Bluetooth apagado. Por favor habilite el bluetoth para imprimir.");
                                }
                            });
                            break;

                        default:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormEstadisticaPedidos.this, "Error desconocido, intente nuevamente.", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                    }

                    try {
                        Thread.sleep(6000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (sewooLKP20 != null) {
                        sewooLKP20.desconectarImpresora();
                        if (progressDialog != null)
                            progressDialog.dismiss();
                    }
                }
                Looper.myLooper().quit();
            }
        }).start();
    }

    public void onClickRegresar(View view) {
        finish();
    }


}
