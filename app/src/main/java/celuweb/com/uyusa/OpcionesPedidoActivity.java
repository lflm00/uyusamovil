package celuweb.com.uyusa;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Detalle;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.MotivoCambio;
import celuweb.com.DataObject.Producto;
import celuweb.com.UI.OnClickListenerImpl;

public class OpcionesPedidoActivity extends Activity implements Sincronizador {
	
	private long mLastClickTime = 0;
	
	boolean editar;
	Detalle detalleAct;
	
	boolean setListener = true;
	
	Producto producto;
	Detalle detalleEdicion;
	
	Dialog dialogPedido;
	Dialog dialogEditar;
	Dialog dialogResumen;
	ProgressDialog progressDialog;
	
	//Por defecto lo toma como pedido
	boolean cambio = false;
	String strOpcion;
	
	Vector<MotivoCambio> listaMotivosCambio;
	
	private TextView lblTotalItems;
	private TextView lblTotalItemsPedido;
	
	private TextView lblSubTotal;
	private TextView lblTotalIva;
	private TextView lblDescuento;
	private TextView lblValorNetoPedido;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.opciones_pedido);
		
		Inicializar();
		InicializarDatos();
	}
	
	public void Inicializar() {
		
		lblTotalItems       = ((TextView) findViewById(R.id.lblTotalItems));
		lblTotalItemsPedido = ((TextView) findViewById(R.id.lblTotalItemsPedido));
		lblSubTotal         = ((TextView) findViewById(R.id.lblSubTotal));
		lblTotalIva         = ((TextView) findViewById(R.id.lblTotalIva));
		lblDescuento        = ((TextView) findViewById(R.id.lblDescuento));
		lblValorNetoPedido  = ((TextView) findViewById(R.id.lblValorNetoPedido));
	}
	
	public void InicializarDatos() {
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null && bundle.containsKey("cambio"))
			cambio = bundle.getBoolean("cambio");
		
		strOpcion = cambio ? "PNC" : "Pedido";
		setTitle("Registrar " + strOpcion);
		
		if (cambio) {
			
			//((Button)findViewById(R.id.btnUltimoPedido)).setVisibility(View.GONE);
			((LinearLayout)findViewById(R.id.panelInfoPNC)).setVisibility(View.VISIBLE);
			((LinearLayout)findViewById(R.id.panelInfo)).setVisibility(View.GONE);
			
		} else {
			
			//((Button)findViewById(R.id.btnUltimoPedido)).setVisibility(View.VISIBLE);
			((LinearLayout)findViewById(R.id.panelInfoPNC)).setVisibility(View.GONE);
			((LinearLayout)findViewById(R.id.panelInfo)).setVisibility(View.VISIBLE);
		}
		
		if (Main.detallePedido == null)
			Main.detallePedido = new Hashtable<String, Detalle>();
		else
			CargarListaPedido();
		
		if (setListener) {
			
			SetKeyListener();
			SetFocusListener();
			SetListenerListView();
			setListener = false;
		}
	}
	
	public void OnClickTerminarPedido(View view) {
		/*retardo de 1500ms para evitar eventos de doble click.
		 * esto significa que solo se puede hacer click cada 1500ms.
		 * es decir despues de presionar el boton, se debe esperar que transcurran
		 * 1500ms para que se capture un nuevo evento.*/
		if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
		
		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		txtCodigoProducto.setError(null);
		
		OcultarTeclado(txtCodigoProducto);
		ValidarPedido();
	}
	
	public void OnClickCancelarPedido(View view) {
		
		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		OcultarTeclado(txtCodigoProducto);
		txtCodigoProducto.setError(null);
		CancelarPedido();
	}
	
	public void OnClickTomarFoto(View view) {
		
		if (Main.detallePedido.size() > 0) {
			
			Intent intent = new Intent(this, FormFotos.class);
			intent.putExtra("nroDoc", Main.encabezado.numero_doc);
			intent.putExtra("codCliente", Main.cliente.codigo);
			intent.putExtra("codVendedor", Main.usuario.codigoVendedor);
			
			startActivity(intent);
			
		} else {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Para tomar Fotos. Por favor primero ingrese los productos.")
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	public void OnClickOpcionesPedido(View view) {
		
		/*retardo de 1500ms para evitar eventos de doble click.
		 * esto significa que solo se puede hacer click cada 1500ms.
		 * es decir despues de presionar el boton, se debe esperar que transcurran
		 * 1500ms para que se capture un nuevo evento.*/
		if (SystemClock.elapsedRealtime() - mLastClickTime < 1500){
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
		
		int totalItems;
		EditText txtCodigoProducto;
		
		switch (view.getId()) {
		
			case R.id.btnAceptarOpcionesPedido:
				
				txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
				OcultarTeclado(txtCodigoProducto);
				
				totalItems = Main.detallePedido.size();
				
				if (totalItems >= Const.MAX_ITEMS) {
					
					Util.MostrarAlertDialog(this, "Se ha superado el maximo de Productos: " + Const.MAX_ITEMS + ".");
					
				} else {
					
					String codigoProducto = txtCodigoProducto.getText().toString().trim();
					
					if (codigoProducto.equals("")) {
						
						txtCodigoProducto.setError("Ingrese el codigo");
						txtCodigoProducto.requestFocus();
						
					} else {
						
						CargarProducto(codigoProducto);
						txtCodigoProducto.setError(null);
					}
				}
				break;
				
			case R.id.btnBuscarOpcionesPedido:
				
				totalItems = Main.detallePedido.size();
				
				if (totalItems >= Const.MAX_ITEMS) {
					
					Util.MostrarAlertDialog(this, "Se ha superado el maximo de Productos: " + Const.MAX_ITEMS + ".");
					
				} else {
					
					((EditText)findViewById(R.id.txtCodigoProducto)).setError(null);
					Intent formBuscarProducto = new Intent(this, FormBuscarProducto.class);
					startActivityForResult(formBuscarProducto, Const.RESP_BUSQUEDA_PRODUCTO);
				}
				break;
				
			/*case R.id.btnCancelarPedido:
				
				txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
				OcultarTeclado(txtCodigoProducto);
				txtCodigoProducto.setError(null);
				CancelarPedido();
				break;
				
			case R.id.btnTerminarPedido:
				
				txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
				txtCodigoProducto.setError(null);
				
				OcultarTeclado(txtCodigoProducto);
				ValidarPedido();
				break;*/
		}
	}
	
	public void OnClickUltimoPedido(View view) {
		
		((EditText)findViewById(R.id.txtCodigoProducto)).setError(null);
		int totalItems = Main.detallePedido.size();
		
		if (totalItems >= Const.MAX_ITEMS) {
			
			Util.MostrarAlertDialog(this, "Se ha superado el maximo de Items por pedido.");
			
		} else {
			
			Intent intent = new Intent(this, FormUltimoPedidoActivity.class);
			intent.putExtra("numeroDoc", Main.encabezado.numero_doc == null ? "-1" : Main.encabezado.numero_doc);
			
			startActivityForResult(intent, Const.RESP_ULTIMO_PEDIDO);
		}
	}
	
	private void AgregarProductoPedido(int cantidad, float descuento_autorizado, String codMotivo) {
		
		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		String codigoProducto = txtCodigoProducto.getText().toString();

		Detalle detalle = Main.detallePedido.get(codigoProducto);
		
		if (detalle == null) {
			
			if (producto != null) {
				
				detalle = new Detalle();
				detalle.codCliente       = Main.cliente.codigo;
				detalle.codProducto      = producto.codigo;
				detalle.nombProducto        = producto.descripcion;
				detalle.precio               = producto.precio;
				detalle.iva                  = producto.iva;
				detalle.descuento = descuento_autorizado;
				detalle.cantidad             = cantidad;
				detalle.tipo_pedido          = Const.PEDIDO_VENTA;
				detalle.codMotivo            = codMotivo;
				//detalle.cantidadInv          = producto.cantidadInv;
				detalle.inventario          = 0;
				
			} else {
				
				AlertResetRegistrarProducto("No se pudo leer la informacion del Producto");
			}
			
		} else {
			
			detalle.cantidad = cantidad;
			detalle.descuento = descuento_autorizado;
		}
		
		boolean agrego = AgregarProducto(detalle);
		
		if (agrego) {
			
			Main.detallePedido.put(codigoProducto, detalle);
			
			EditText txtCantidadProc = (EditText)dialogPedido.findViewById(R.id.txtCantidadProc);
			txtCantidadProc.setText("");
			
			OcultarTeclado(txtCantidadProc);
			
			txtCodigoProducto.setText("");
			txtCodigoProducto.requestFocus();
			
			CargarListaPedido();
			dialogPedido.cancel();
			
		} else {
			
			AlertResetRegistrarProducto("No se pudo Agregar el Producto!");
		}
	}
	
	private void ActualizarProductoPedido(Detalle detalle, int cantidad, float descuento_autorizado) {
		
		if (detalle == null) {
			
			AlertResetRegistrarProducto("No se pudo leer la informacion del Producto!");
			return;
		}
		
		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		String codProducto = txtCodigoProducto.getText().toString();
		
		boolean actualizo = DataBaseBO.ActualizarProductoPedido(Main.encabezado, codProducto, cantidad);
		
		if (actualizo) {
			
			detalle.cantidad = cantidad;
			detalle.descuento = descuento_autorizado;
			
			Main.detallePedido.put(codProducto, detalle);
			
			EditText txtCantidadProc = (EditText)dialogPedido.findViewById(R.id.txtCantidadProc);
			txtCantidadProc.setText("");
			
			OcultarTeclado(txtCantidadProc);
			
			txtCodigoProducto.setText("");
			txtCodigoProducto.requestFocus();
			
			CargarListaPedido();
			dialogPedido.cancel();
			
		} else {
			
			AlertResetRegistrarProducto("No se pudo Editar los datos del Producto!");
		}
	}
	
	public boolean AgregarProducto(Detalle detalle) {
		
		boolean registro = false;
		
		if (Main.encabezado == null)
			Main.encabezado = new Encabezado();
		
		if (Main.encabezado.numero_doc == null) {
			
			Main.encabezado.codigo_cliente = Main.cliente.codigo;      //Codigo del Cliente
			Main.encabezado.nombre_cliente = Main.cliente.Nombre;      //Nombre del Cliente
			Main.encabezado.razon_social   = Main.cliente.razonSocial; //Razon Social
			Main.encabezado.codigo_novedad = 1;                        //1 -> Venta, para no compra es MotivosCompras.codigo
			Main.encabezado.lista_precio   = Main.cliente.CodigoAmarre; //Lista de Precio
			//Main.encabezado.grupo_precio   = Main.cliente.grupoPrecio; //Lista de Precio
			Main.encabezado.hora_final     = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
			//Main.encabezado.tipo_cliente   = "V";                            //Tipo Cliente (Cliente viejo o Cliente nuevo)
			//Main.encabezado.extra_ruta     = Main.cliente.extra_ruta;        //Indica si el pedido se hace Sobre un cliente que no esta en el Rutero
			Main.encabezado.numero_doc     = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor); //Numero Unico por Pedido
			
			String version = ObtenerVersion();
			String imei = ObtenerImei();
			
			String tipoTrans = cambio ? "2" : "0";
			registro = DataBaseBO.RegistrarEncabezado(Main.encabezado, detalle, version, imei, tipoTrans);
			
		} else {
			
			registro = DataBaseBO.RegistrarProductoPedido(Main.encabezado, detalle);
		}
		
		return registro;
	}
		
	private boolean CargarProducto(String codigoProducto) {

		producto = new Producto();
		boolean cargo = true;//DataBaseBO.ProductoXCodigo(codigoProducto, Main.cliente.CodigoAmarre, 0, producto);
		
		if (cargo) {
			
			/*if( producto.promocionCumplio.equals( "1" ) ){
				
				if( 0 == 0 ){
					
					AlertResetRegistrarProducto("Producto promocion, no habilitado para cliente");
					return false;
				}
			}*/
			
			
			//if( producto.concatEstrato.indexOf( String.valueOf( "0" ) ) != -1 ){
			if( false ){
				
				AlertResetRegistrarProducto("El producto esta bloqueado para el estrato del cliente");
				return false;
			}
			else{
			
				if( DataBaseBO.productoRestringidoVenta( producto.codigo ) ){
										
					AlertResetRegistrarProducto("Producto Restringido para este cliente!!");
					return false;
				}
				else{
					
					//if( producto.bloqueadoCliente.indexOf( "" ) != -1 ){
					if( true ){
						
						if( DataBaseBO.clienteHabilitadoProductoST( producto.codigo ) ){
							
							return MostrarDialogPedido(codigoProducto);
						}
						else{
							
							AlertResetRegistrarProducto("Producto Para Clientes Especiales!!");
							return false;
						}
					}
					else{
				
						return MostrarDialogPedido(codigoProducto);
					}
				
				}
			}
			
		} else {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("No se encontro el Producto")
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			return false;
		}
	}
	
	public boolean MostrarDialogPedido(String codigoProducto) {
		
		try {
			
			if (onClickRegistrarProducto != null)
				onClickRegistrarProducto.reset();

			if (dialogPedido == null) {
				
				dialogPedido = new Dialog(this);
				dialogPedido.setContentView(R.layout.dialog_pedido);
				dialogPedido.setTitle("Registrar Producto");
				
				String[] items;
				Vector<String> listaItems = new Vector<String>();
				listaMotivosCambio = DataBaseBO.ListaMotivosCambio(listaItems);
				
				if (listaItems.size() > 0) {
					
					items = new String[listaItems.size()];
					listaItems.copyInto(items);
					
				} else {
					
					items = new String[] {};
				}
				
				Spinner cbMotivoCambio = (Spinner) dialogPedido.findViewById(R.id.cbMotivoCambio);
		    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
		        
		        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		        cbMotivoCambio.setAdapter(adapter);
				
			} else {
				
				
				((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).requestFocus();
			}
			
			dialogPedido.onContentChanged();
			
			if (cambio) {
				
				((TableLayout)dialogPedido.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.VISIBLE);
				
			} else {
				
				((TableLayout)dialogPedido.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.GONE);
			}
			
			if (producto != null) { //Se carga la Informacion del Producto
				
				((TextView)dialogPedido.findViewById(R.id.lblDescProducto)).setText(producto.descripcion);
				((TextView)dialogPedido.findViewById(R.id.lblPrecio)).setText("Precio: " + Util.SepararMiles("" + producto.precio) + " - Iva: " + producto.iva + "%");
				((TextView)dialogPedido.findViewById(R.id.lblInventario)).setText("Lim Venta: " + String.valueOf( producto.inventario ));
				
				detalleAct = Main.detallePedido.get(codigoProducto);
				
				if (detalleAct != null) {
					
					editar = true;
					((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).setText("" + detalleAct.cantidad);
					
					if (cambio)
						SelecionarMotivoCambio(detalleAct.codMotivo);
					
				} else {
					
					editar = false;
					((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).setText("");
				}
				
				((Button)dialogPedido.findViewById(R.id.btnVentaOpcionesPedido)).setOnClickListener(onClickRegistrarProducto);
				
				((Button)dialogPedido.findViewById(R.id.btnCancelarOpcionesPedido)).setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						
						dialogPedido.cancel();
						onClickRegistrarProducto.reset();
					}
				});
				
				dialogPedido.setCancelable(false);
				dialogPedido.show();
				
				return true;
				
			} else { //No se Encontro el Producto
				
				((TextView)dialogPedido.findViewById(R.id.lblDescProducto)).setText("-");
				((TextView)dialogPedido.findViewById(R.id.lblPrecio)).setText("-");
				
				AlertResetRegistrarProducto("No se encontro el Producto!");
				return false;
			}
			
		} catch (Exception e) {
			
			onClickRegistrarProducto.reset();
			return false;
		}
	}
	
	public void RegistrarProducto() {
		
		String cant  = ((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).getText().toString();
		int cantidad = Util.ToInt(cant);
		
		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		String codigoProducto = txtCodigoProducto.getText().toString();
		
		int datos[]  = DataBaseBO.ObtenerLimitesVentas( codigoProducto );
		int multiplo = DataBaseBO.ObtenerMultiploVentas( codigoProducto );
		
		if (cantidad == 0) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(dialogPedido.getContext());
			builder.setMessage("Debe ingresar la cantidad")
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).requestFocus();
					
					dialog.cancel();
					onClickRegistrarProducto.reset();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			
		} else if ( cantidad > producto.inventario ) {
			
			String msg = "";
			if (producto.inventario == 0)
				msg = "El producto: " + producto.codigo + " no tiene Inventario";
			else 
				msg = "La cantidad ingresada supera el Inventario del Producto: ";
			
			AlertDialog.Builder builder = new AlertDialog.Builder(dialogPedido.getContext());
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).requestFocus();
					
					dialog.cancel();
					onClickRegistrarProducto.reset();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			
		}
		else if( cantidad >= datos[ 0 ] && cantidad <= datos[ 1 ] ){
			
			if( !( cantidad % multiplo == 0 ) ){
				
				AlertDialog.Builder builder = new AlertDialog.Builder(dialogPedido.getContext());
				builder.setMessage("Cantidad No permitida Solo Multiplos de " + String.valueOf( multiplo ))
				.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).requestFocus();
						
						dialog.cancel();
						onClickRegistrarProducto.reset();
					}
				});
				
				AlertDialog alert = builder.create();
				alert.show();
			}
			else{
			
				String codMotivoCambio = "";
				float descuento = 0;//Main.cliente.descuento;
				
				if (cambio) {
					
					Spinner cbMotivoCambio = (Spinner) dialogPedido.findViewById(R.id.cbMotivoCambio);
					int index = cbMotivoCambio.getSelectedItemPosition();
					MotivoCambio motivo = listaMotivosCambio.elementAt(index);
					codMotivoCambio = motivo.codigo;
				}
				
				if (editar) {
				
					if (detalleAct != null) {
						
						detalleAct.codMotivo = codMotivoCambio; 
						ActualizarProductoPedido(detalleAct, cantidad, descuento);
						
					} else {
						
						AlertResetRegistrarProducto("No se encontro el Producto!");
					}
					
				} else {
					
					AgregarProductoPedido(cantidad, descuento, codMotivoCambio);
				}
			}
		}
		else {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(dialogPedido.getContext());
			builder.setMessage("La venta del producto no esta en el limite permitido")
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					((EditText)dialogPedido.findViewById(R.id.txtCantidadProc)).requestFocus();
					
					dialog.cancel();
					onClickRegistrarProducto.reset();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	private void SelecionarMotivoCambio(String codMotivo) {
		
		int size = listaMotivosCambio.size();
		for (int j = 0; j < size; j++) {
			
			MotivoCambio motivoCambio = listaMotivosCambio.elementAt(j);
			
			if (dialogPedido != null && motivoCambio.codigo.equals(codMotivo)) {
				
				((Spinner) dialogPedido.findViewById(R.id.cbMotivoCambio)).setSelection(j);
				break;
			}
		}
	}

	/**
	 * Verifica si el pedio tiene productos asignados, 
	 * en caso tal muestra el resumen del Pedido y pide al usuario las observaciones 
	 * Si el pedido esta Vacio, muestra un mensaje de advertencia al usuario
	 **/
	public void ValidarPedido() {
		
		if (Main.detallePedido.size() > 0) {
			
			if (cambio) {
				
				MostrarDialogResumen();
				
			} else {
				
				if( Main.usuario.codigoVendedor == null )
					DataBaseBO.CargarInfomacionUsuario();
				
				if( Main.usuario.pedidoMinimo > Main.encabezado.valor_neto ){
					
					AlertResetRegistrarProducto( "El pedido no supera el valor minimo permitido ( " + Main.usuario.pedidoMinimo + " )" );
				}
				else{
				
					MostrarDialogResumen();
				}
				
				//ValidarFormasPago();
			}
			
		} else {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("El " + strOpcion + " esta vacio. Por favor ingrese los productos.")
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	public void ValidarFormasPago() {
		
		/*if (Main.cliente.tipoCredito.equals(Const.CREDITO)) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("El Cliente es Tipo Credido. Desea Ingresar las Formas de Pago?")
			.setCancelable(false)
			.setPositiveButton("SI", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					MostrarFormFormasPago();
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
					MostrarDialogResumen();
				}
			});
			
			AlertDialog alertDialog = builder.create();
			alertDialog.show();
			
		} else {
			
			MostrarFormFormasPago();
		}*/
	}
	
	public void MostrarFormFormasPago() {
		
		Intent intent = new Intent(OpcionesPedidoActivity.this, FormFormasPago.class);
		String valor = Util.SepararMilesSin(Util.Redondear("" + Main.encabezado.valor_neto, 0)).replace(",", "");
		double valorNeto = Util.ToDouble(valor);
		
		intent.putExtra("totalRecuado", valorNeto);
		intent.putExtra("nroDoc", Main.encabezado.numero_doc);
		
		startActivityForResult(intent, Const.RESP_FORMAS_DE_PAGO);
	}
	
	public boolean MostrarDialogResumen() {
		
		try {
			
			if (onClickGuardarPedido != null)
				onClickGuardarPedido.reset();
			
			if (dialogResumen == null) {
				
				dialogResumen = new Dialog(this);
				dialogResumen.setContentView(R.layout.dialog_resumen_pedido);
				
				/**
				 * Se registran los eventos de los Botones del Formulario Dialog Resumen
				 */
				((Button)dialogResumen.findViewById(R.id.btnAceptarResumenPedido)).setOnClickListener(onClickGuardarPedido);
				
				/*((Button)dialogResumen.findViewById(R.id.btnAceptarResumenPedido)).setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						
						FinalizarPedido();
					}
				});*/
				
				((Button)dialogResumen.findViewById(R.id.btnCancelarResumenPedido)).setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						
						EditText txtObservacionPedido = ((EditText)dialogResumen.findViewById(R.id.txtObservacionPedido));
						OcultarTeclado(txtObservacionPedido);
						
						dialogResumen.cancel();
						onClickGuardarPedido.reset();
					}
				});
				
			} else {
				
				((EditText)dialogResumen.findViewById(R.id.txtObservacionPedido)).setText("");
				((EditText)dialogResumen.findViewById(R.id.txtObservacionPedido)).requestFocus();
				//((Spinner) dialogResumen.findViewById(R.id.cbMotivoCambio)).setSelection(0);
			}
			
			if (cambio) { //PNC
				
				//((CheckBox)dialogResumen.findViewById(R.id.checkFiado)).setVisibility(CheckBox.GONE);
				((TableLayout)dialogResumen.findViewById(R.id.tableLayoutResumenCliente)).setVisibility(TableLayout.GONE);
				//((TableLayout)dialogResumen.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.VISIBLE);
				
			} else { //Pedido
				
				/*if (Main.cliente.tipoCredito.equals("EF"))
					((CheckBox)dialogResumen.findViewById(R.id.checkFiado)).setVisibility(CheckBox.VISIBLE);
				else
					((CheckBox)dialogResumen.findViewById(R.id.checkFiado)).setVisibility(CheckBox.GONE);*/
				
				((TextView)dialogResumen.findViewById(R.id.lblSubTotalResumenPedido)).setText(lblSubTotal.getText().toString());
				((TextView)dialogResumen.findViewById(R.id.lblIVAResumenPedido)).setText(lblTotalIva.getText().toString());
				((TextView)dialogResumen.findViewById(R.id.lblDescuentoResumenPedido)).setText(lblDescuento.getText().toString());
				((TextView)dialogResumen.findViewById(R.id.lblTotalResumenPedido)).setText(lblValorNetoPedido.getText().toString());
				
				((TableLayout)dialogResumen.findViewById(R.id.tableLayoutResumenCliente)).setVisibility(TableLayout.VISIBLE);
				//((TableLayout)dialogResumen.findViewById(R.id.tblMotivoCambio)).setVisibility(TableLayout.GONE);
			}
			
			((TextView)dialogResumen.findViewById(R.id.lblClienteResumenPedido)).setText(Main.cliente.Nombre);
			dialogResumen.setTitle("Resumen " + strOpcion);
			
			dialogResumen.setCancelable(false);
			dialogResumen.show();
			
			return true;
			
		} catch (Exception e) {

			Log.e("MostrarDialogResumen", e.getMessage(), e);
			
			if (onClickGuardarPedido != null)
				onClickGuardarPedido.reset();
			
			return false;
		}
	}
	
	public void MostrarDialogEdicion() {
		
		if (dialogEditar == null) {
				
			dialogEditar = new Dialog(this);
			dialogEditar.setContentView(R.layout.dialog_editar);
			dialogEditar.setTitle("Opciones Edicion");
			
			((RadioButton)dialogEditar.findViewById(R.id.radioEditar)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					if (producto == null)
						producto = new Producto();

					producto.codigo      = detalleEdicion.codProducto;
					producto.descripcion = detalleEdicion.nombProducto;
					producto.precio      = detalleEdicion.precio;
					producto.iva         = detalleEdicion.iva;
					//producto.cantidadInv = detalleEdicion.cantidadInv;
					
					((EditText)findViewById(R.id.txtCodigoProducto)).setText(producto.codigo);
					
					dialogEditar.cancel();
					MostrarDialogPedido(producto.codigo);
				}
			});
			
			((RadioButton)dialogEditar.findViewById(R.id.radioEliminar)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					AlertDialog.Builder builder = new AlertDialog.Builder(OpcionesPedidoActivity.this);
					builder.setMessage("Esta Seguro de eliminar el producto " + detalleEdicion.codProducto)
					.setCancelable(false)
					.setPositiveButton("Si", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int id) {
							
							boolean elimino = DataBaseBO.EliminarProductoPedido(Main.encabezado, detalleEdicion.codProducto);
							
							if (elimino) {
								
								Main.detallePedido.remove(detalleEdicion.codProducto);
								CargarListaPedido();
								dialogEditar.cancel();
								
							} else {
								
								Util.MostrarAlertDialog(OpcionesPedidoActivity.this, "No se pudo eliminar el producto");
							}
						}
					})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int id) {
							
							dialogEditar.cancel();
						}
					});
					
					AlertDialog alert = builder.create();
					alert.show();
				}
			});
			
			((RadioButton)dialogEditar.findViewById(R.id.radioCancelar)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					dialogEditar.cancel();
				}
			});
			
		} else {
			
			((RadioButton)dialogEditar.findViewById(R.id.radioEditar)).requestFocus();		
		}
		
		dialogEditar.show();
	}
	
	public void CargarListaPedido() {
		
		Main.encabezado.sub_total = 0;
		Main.encabezado.total_iva = 0;
		Main.encabezado.valor_descuento = 0;
		
		ItemListView itemListView;
		Enumeration<Detalle> e = Main.detallePedido.elements();
		Vector<ItemListView> datosPedido = new Vector<ItemListView>();
		
		while (e.hasMoreElements()) {
			
			Detalle detalle = e.nextElement();
			itemListView = new ItemListView();
			
			float sub_total       = detalle.cantidad * detalle.precio;
			float valor_descuento = sub_total * 0 / 100;
			float valor_iva       = (sub_total - valor_descuento) * (detalle.iva / 100);
			
			Main.encabezado.sub_total += sub_total;
			Main.encabezado.total_iva += valor_iva;
			Main.encabezado.valor_descuento += valor_descuento;
			
			itemListView.titulo = detalle.codProducto + " - " + detalle.nombProducto;
			itemListView.subTitulo = "Precio: $" + detalle.precio + " - Iva: " + detalle.iva +  "% - Cant: " + detalle.cantidad;
			datosPedido.addElement(itemListView);
		}
		
		ItemListView[] listaItems = new ItemListView[datosPedido.size()];
		datosPedido.copyInto(listaItems);
		
		Main.encabezado.valor_neto     = Main.encabezado.sub_total + Main.encabezado.total_iva - Main.encabezado.valor_descuento;
		Main.encabezado.str_valor_neto = Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + Main.encabezado.valor_neto), 0)); 
		
		if (cambio) {
			
			Main.encabezado.sub_total = 0;
			Main.encabezado.total_iva = 0;
			Main.encabezado.valor_descuento = 0;
			
			lblTotalItems.setText("" + Main.detallePedido.size());
			
		} else {
			
			lblTotalItemsPedido.setText("" + Main.detallePedido.size());
			lblSubTotal.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + Main.encabezado.sub_total), 0)));
			lblTotalIva.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + Main.encabezado.total_iva), 0)));
			lblDescuento.setText(Util.SepararMilesSin(Util.RedondearFit(Util.QuitarE("" + Main.encabezado.valor_descuento), 0)));
			lblValorNetoPedido.setText(Main.encabezado.str_valor_neto);
		}
		
		ListViewAdapter adapter = new ListViewAdapter(this, listaItems, R.drawable.compra, 0);
		ListView listaPedido = (ListView)findViewById(R.id.listaPedido);
		listaPedido.setAdapter(adapter);
		
		//float disponible = Util.ToFloat(Main.cliente.cupo) - Main.cliente.carteraPendiente.total;
		//Main.encabezado.excedeCupo = Main.encabezado.valor_neto > disponible;
		
		/*if (Main.encabezado.excedeCupo) {
			
			Util.MostrarAlertDialog(this, "El valor del pedido excede el cupo disponible: " + Util.SepararMiles(Util.QuitarE("" + disponible)));
		}*/
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (resultCode == RESULT_OK) {
			
			if ( (requestCode == Const.RESP_BUSQUEDA_PRODUCTO || requestCode == Const.RESP_ULTIMO_PEDIDO) ) {
				
				if (data != null) {
					
					Bundle bundle = data.getExtras();
					Object productoSel = bundle.get("producto"); 
					
					if (productoSel != null && productoSel instanceof Producto) {
						
						producto = (Producto)productoSel;
						((EditText)findViewById(R.id.txtCodigoProducto)).setText(producto.codigo);
						
						MostrarDialogPedido(producto.codigo);
					}
					
				} else if (requestCode == Const.RESP_ULTIMO_PEDIDO) {
					
					CargarListaPedido();
				}
				
			} else if (requestCode == Const.RESP_FORMAS_DE_PAGO) {
				
				MostrarDialogResumen();
			}
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	
	    	CancelarPedido();
	    	return true;
	    }
	    
	    return super.onKeyDown(keyCode, event);
	}
	
	public void CancelarPedido() {
		
		if (Main.detallePedido.size() > 0) {
			
			/**
			 * Ya ha tomado un Pedido, Se pregunta al Usuario si desea Cancelarlo
			 **/
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Esta Seguro de cancelar el " + strOpcion + "? La informacion se Borrara")
			.setCancelable(false)
			.setPositiveButton("Si", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					boolean cancelo = DataBaseBO.CancelarPedido(Main.encabezado);
					
					if (cancelo) {
						
						Main.encabezado = new Encabezado();
						Main.detallePedido.clear();
						
						dialog.cancel();
						OpcionesPedidoActivity.this.finish();
						
					} else {
						
						Util.MostrarAlertDialog(OpcionesPedidoActivity.this, "No se pudo cancelar el " + strOpcion);
					}
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					moveTaskToBack(false);
					dialog.cancel();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			
		} else {
			
			/**
			 * El pedido esta vacio.
			 * Se verifica si se guardo un Encabezado en caso tal se borra.  
			 * Se cierra el formulario.
			 **/
			
			if (Main.encabezado.numero_doc != null) {
				
				DataBaseBO.CancelarPedido(Main.encabezado);
			}
			
			Main.encabezado = new Encabezado();
			OpcionesPedidoActivity.this.finish();
		}
	}
	
	/*public void GuardarPedido() {
		
		Main.encabezado.codigo_cliente = Main.cliente.codigo;      //Codigo del Cliente
		Main.encabezado.nombre_cliente = Main.cliente.nombre;      //Nombre del Cliente
		Main.encabezado.razon_social   = Main.cliente.razonSocial; //Razon Social
		Main.encabezado.codigo_novedad = 1;                        //1 -> Venta, para no compra es MotivosCompras.codigo
		Main.encabezado.lista_precio   = Main.cliente.listaPrecio; //Lista de Precio
		
		//Main.encabezado.valor_neto;   //Valor Neto de la Venta, Esta informacion se va calculando cada vez que se Agrega, Edita o Elimina un Producto del Pedido		
		//Main.encabezado.observacion   //Esta informacion se llena en el Dialog Resumen Pedido
		//Main.encabezado.hora_inicial  //Esta informacion se llena en el FormInfoCliente, cuando se da click en Nuevo Pedido
		
		Main.encabezado.hora_final     = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); //Hora en finaliza la Toma del Pedido
		Main.encabezado.tipo_cliente   = "V";                            //Tipo Cliente (Cliente viejo o Cliente nuevo)
		Main.encabezado.extra_ruta     = Main.cliente.extra_ruta;        //Indica si el pedido se hace Sobre un cliente que no esta en el Rutero
		Main.encabezado.numero_doc     = DataBaseBO.ObtenterNumeroDoc(); //Numero Unico por Pedido
		
		boolean ingreso = DataBaseBO.GuardarPedido(Main.encabezado, Main.detallePedido);
		
		if (ingreso) {
			
			//DataBaseBO.ActulizarEstadoRutero(Main.cliente.codigo);
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Pedido Registrado con Exito\n" + "Para el Cliente " + Main.cliente.nombre + "\n\n" +
							   "Desea Enviar el pedido?")	
			.setCancelable(false)
			.setPositiveButton("SI", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					progressDialog = ProgressDialog.show(OpcionesPedidoActivity.this, "", "Enviando Informacion Pedido...", true);
					progressDialog.show();
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					Sync sync = new Sync(OpcionesPedidoActivity.this, Const.ENVIAR_PEDIDO);
					sync.start();
				}
			})
			.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					OpcionesPedidoActivity.this.setResult(RESULT_OK);
					OpcionesPedidoActivity.this.finish();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			
			dialogResumen.cancel();
		}
	}*/
	
	public void FinalizarPedido() {
		
		/*if (Main.cliente.tipoCredito.equals("EF"))
			Main.encabezado.fiado = ((CheckBox)dialogResumen.findViewById(R.id.checkFiado)).isChecked() ? 1 : 0;
		else 
			Main.encabezado.fiado = 0;*/
			
		Main.encabezado.observacion = ((EditText)dialogResumen.findViewById(R.id.txtObservacionPedido)).getText().toString().trim();
		Main.encabezado.hora_final  = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora(); //Hora en finaliza la Toma del Pedido
		
		boolean finalizo = true;//DataBaseBO.FinalizarPedido(Main.encabezado);
		
		if (finalizo) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(strOpcion + " Registrado con Exito\n" + "Para el Cliente " + Main.cliente.Nombre)	
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					DataBaseBO.ActualizaEstadoRutero( Main.cliente.codigo );
					
					progressDialog = ProgressDialog.show(OpcionesPedidoActivity.this, "", "Enviando Informacion ...", true);
					progressDialog.show();
					
					//DataBaseBO.ActualizarSnEnvio(Main.encabezado.id, 1);
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					onClickGuardarPedido.reset();
					
					Sync sync = new Sync(OpcionesPedidoActivity.this, Const.ENVIAR_PEDIDO);
					sync.start();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			
			dialogResumen.cancel();
			
		} else {
			
			MostrarAlertDialog("No se pudo finalizar el " + strOpcion);
		}
	}
	
	public void SetListenerListView() {
		
		ListView listaOpciones = (ListView)findViewById(R.id.listaPedido);
		listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               
				ListViewAdapter adapter = (ListViewAdapter)parent.getAdapter();
            	ItemListView itemListView = adapter.listItems[position];
            	
            	String[] data = itemListView.titulo.split("-");
            	String codigoProducto = data[0].trim();
            	
            	detalleEdicion = Main.detallePedido.get(codigoProducto);
            	MostrarDialogEdicion();
            }
        });
	}
	
	public void SetKeyListener() {
		
		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		txtCodigoProducto.setOnKeyListener(new OnKeyListener() {
			
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				
		        if (keyCode == KeyEvent.KEYCODE_ENTER) {
		        	
					((Button)findViewById(R.id.btnAceptarOpcionesPedido)).requestFocus();
					return true;
		        }
				
		        return false;
		    }
		});
	}
	
	public void SetFocusListener() {
		
		EditText txtCodigoProducto = (EditText)findViewById(R.id.txtCodigoProducto);
		txtCodigoProducto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
		    @Override
		    public void onFocusChange(View v, boolean hasFocus) {
		    	
		    	if (!hasFocus) {
		    		
		    		/**
		    		 * Si NO tiene el foco se quita el mensaje de Error.
		    		 **/
		    		((EditText)findViewById(R.id.txtCodigoProducto)).setError(null);
		    	}
		    }
		});
	}

	@Override
	public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
		
		final String mensaje = ok ?  strOpcion + " Registrado con Exito en el servidor" : msg;
		
		if (progressDialog != null)
			progressDialog.cancel();
		
		this.runOnUiThread(new Runnable() {
			
			public void run() {
				
				AlertDialog.Builder builder = new AlertDialog.Builder(OpcionesPedidoActivity.this);
				builder.setMessage(mensaje)
						
				.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						Main.encabezado = new Encabezado();
						Main.detallePedido.clear();
						
						dialog.cancel();
						
						//OpcionesPedidoActivity.this.setResult(RESULT_OK);
						OpcionesPedidoActivity.this.finish();
					}
				});
				
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}
	
	public void OcultarTeclado(EditText editText) {
		
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}
	
	public String ObtenerVersion() {
		
		String version;
		
		try {
        	
        	version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			
		} catch (NameNotFoundException e) {
			
			version = "0.0";
			Log.e("OpcionesPedidoActivity", e.getMessage(), e);
		}
		
		return version;
	}
	
	public String ObtenerImei() {
		
		TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
	}
	
	public void MostrarAlertDialog(String mensaje) {
    	
		AlertDialog alertDialog;
			
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				dialog.cancel();
				onClickGuardarPedido.reset();
			}
		});

		alertDialog = builder.create();
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
    }
	
	public void AlertResetRegistrarProducto(String mensaje) {
		
		AlertDialog alertDialog;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				dialog.cancel();
				onClickRegistrarProducto.reset();
			}
		});

		alertDialog = builder.create();
    	alertDialog.setMessage(mensaje);
    	alertDialog.show();
	}
	
	private OnClickListenerImpl onClickTerminar = new OnClickListenerImpl() {
		
	    @Override
		public void onOneClick(View view) {
	    	
	    	//OcultarMensajeYTeclado();
			ValidarPedido();
	    }
	};
	
	private OnClickListenerImpl onClickGuardarPedido = new OnClickListenerImpl() {
		
	    @Override
		public void onOneClick(View view) {
	    	
	    	onClickTerminar.reset();
	    	FinalizarPedido();
		}
	};
	
	
	private OnClickListenerImpl onClickRegistrarProducto = new OnClickListenerImpl() {
		
	    @Override
		public void onOneClick(View view) {
	    	
	    	RegistrarProducto();
		}
	};
}
