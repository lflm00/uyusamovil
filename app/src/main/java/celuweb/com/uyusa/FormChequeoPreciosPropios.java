package celuweb.com.uyusa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ChequeoPrecios;

public class FormChequeoPreciosPropios extends CustomActivity /*implements GPS*/ {

	public static HashMap<String,ChequeoPrecios> hashMapChequeoPropio=new HashMap<>();
	public static HashMap<String,ChequeoPrecios> hashMapChequeoCompetencia=new HashMap<>();

	Vector<ChequeoPrecios> listaChequeo;
	RecyclerView recyclerView;
	RecyclerView.LayoutManager layoutManager;
	String idProgramacionChequeo;
	ChequeoAdapter adapter;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_chequeo_propios);

		inicializar();
		listaChequeo=new Vector<>();
		recyclerView= (RecyclerView) findViewById(R.id.recicler);
		adapter=new ChequeoAdapter(this,listaChequeo);
		layoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
		recyclerView.setLayoutManager(layoutManager);
		CargarChequeos();


	}

	private void inicializar() {
		Bundle bundle = getIntent().getExtras();
		if (bundle!=null){
			idProgramacionChequeo=bundle.getString("id");
		}
	}


	public void CargarChequeos() {
		listaChequeo = DataBaseBO.ListaChequeosPropios(idProgramacionChequeo);

		for (int i=0;i<listaChequeo.size();i++){
			recyclerView.setAdapter(adapter);

		}
		adapter.updateRecords(listaChequeo);
		recyclerView.setItemViewCacheSize(listaChequeo.size());

	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		return true;
	}

	public void OnClickRegresar(View view){
		if (hashMapChequeoCompetencia.size()>0 || hashMapChequeoPropio.size()>0){
			mostrarMensajeCancelar();
		}else {
			finish();
		}
	}

	private void mostrarMensajeCancelar() {
		AlertDialog.Builder builder=new AlertDialog.Builder(FormChequeoPreciosPropios.this);
		builder.setTitle("Cancelar Registro!");
		builder.setMessage("Esta Seguro de Cancelar El registro??, Recuerde que la informacion ingresada sera eliminada!");
		builder.setCancelable(false);
		builder.setPositiveButton("Cancelar Registro", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
				finish();
				hashMapChequeoPropio.clear();
				hashMapChequeoCompetencia.clear();
			}
		});
		builder.setNegativeButton("Continuar Registrando", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.create();
		builder.show();
	}


	public void OnClickGuardar(View view){

		if (hashMapChequeoCompetencia.size()>0 || hashMapChequeoPropio.size()>0){
			mostrarMensajeGuardar();
		}else {
			Util.MostrarAlertDialog(this,"No se ha ingresado un precio a guardar");
		}
    }

	private void mostrarMensajeGuardar() {
		AlertDialog.Builder builder=new AlertDialog.Builder(FormChequeoPreciosPropios.this);
		builder.setTitle("Guardar Registro!");
		builder.setMessage("Esta Seguro de Guardar El registro??, Recuerde que solo podra realizar un chequeo por dia a un mismo cliente!");
		builder.setCancelable(false);
		builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
				boolean guardo=DataBaseBO.guardarChequeoPrecios(Main.cliente.codigo,Main.usuario.codigoVendedor);
				if (guardo){
					Toast.makeText(getApplicationContext(),"Registro Exitoso",Toast.LENGTH_SHORT).show();
					hashMapChequeoCompetencia.clear();
					hashMapChequeoPropio.clear();
					finish();
				}else
					Toast.makeText(getApplicationContext(),"Registro Fallido",Toast.LENGTH_SHORT).show();
			}
		});
		builder.setNegativeButton("cancelar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.create();
		builder.show();
	}
}
