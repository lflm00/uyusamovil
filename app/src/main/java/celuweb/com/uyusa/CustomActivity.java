package celuweb.com.uyusa;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mikepenz.iconics.Iconics;
import com.mikepenz.iconics.context.IconicsContextWrapper;

import celuweb.com.fonts.Font;
import celuweb.com.fonts.FontIcon;

public class CustomActivity extends AppCompatActivity {
	public static final String TAG = CustomActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		loadFonts();

		super.onCreate(savedInstanceState);
		checkPermission();

	}

	@Override
	protected void onResume() {
		super.onResume();
		checkPermission();
		loadFonts();
	}

	private void loadFonts() {
		//Registra fuente iconos celuweb
		Iconics.init(getApplicationContext());
		Iconics.registerFont(new FontIcon());
		//Registra fuente aplicacion celuweb
		Font font = new Font();
		font.setCalligrapher(this);
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
	}

	/********************************************************************************************************************************
	 * A continuacion se definen los metodos para validar permisos.
	 * La idea es verificar siempre que se reanuda la aplicacion si tiene los permisos activos.
	 ********************************************************************************************************************************/
	private void checkPermission() {
		//permisos almacenamiento
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			checkPermissionStorage();
		} else
			//permisos telefono
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
				checkPermissionPhone();
			} else
				//permisos ubicacion
				if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					checkPermissionLocation();
				} else
					//permisos camara
					if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
						checkPermissionCamera();
					} else
						//permisos llamadas
						if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
							checkPermissionLLamadas();
						}
	}

	private void checkPermissionStorage() {
		Log.e(TAG, "checkPermissionStorage-> ");
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
				mostrarMensajePermisos(Const.RESP_PERMISOS_STORAGE);
			} else {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, Const.RESP_PERMISOS_STORAGE);
			}
		}
	}

	private void checkPermissionPhone() {
		Log.e(TAG, "checkPermissionPhone-> ");
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
				mostrarMensajePermisos(Const.RESP_PERMISOS_PHONE);
			} else {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, Const.RESP_PERMISOS_PHONE);
			}
		}
	}

	private void checkPermissionLocation() {
		Log.e(TAG, "checkPermissionLocation-> ");
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
				mostrarMensajePermisos(Const.RESP_PERMISOS_LOCATION);
			} else {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.RESP_PERMISOS_LOCATION);
			}
		}
	}

	private void checkPermissionCamera() {
		Log.e(TAG, "checkPermissionCamera-> ");
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
				mostrarMensajePermisos(Const.RESP_PERMISOS_CAMERA);
			} else {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, Const.RESP_PERMISOS_CAMERA);
			}
		}
	}

	private void checkPermissionLLamadas() {
		Log.e(TAG, "checkPermissionLLamadas-> ");
		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CALL_PHONE)) {
				mostrarMensajePermisos(Const.RESP_PERMISOS_CALL_PHONE);
			} else {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, Const.RESP_PERMISOS_CALL_PHONE);
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {


		Log.e(TAG, "onRequestPermissionsResult-> requestCode " + requestCode);
		Log.e(TAG, "onRequestPermissionsResult-> permissions " + permissions);
		Log.e(TAG, "onRequestPermissionsResult-> grantResults " + grantResults);


		switch (requestCode) {
			case Const.RESP_PERMISOS_STORAGE: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission granted
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
					checkPermission();
				} else {
					// permission denied
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
					//mostrarMensajePermisos(Const.RESP_PERMISOS_STORAGE);
				}
				return;
			}
			case Const.RESP_PERMISOS_PHONE: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission granted
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
					checkPermission();
				} else {
					// permission denied
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
					//mostrarMensajePermisos(Const.RESP_PERMISOS_PHONE);
				}
				return;
			}
			case Const.RESP_PERMISOS_LOCATION: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission granted
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
					checkPermission();
				} else {
					// permission denied
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
					//mostrarMensajePermisos(Const.RESP_PERMISOS_LOCATION);
				}
				return;
			}
			case Const.RESP_PERMISOS_CAMERA: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission granted
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
					checkPermission();
				} else {
					// permission denied
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
					//mostrarMensajePermisos(Const.RESP_PERMISOS_CAMERA);
				}
				return;
			}
			case Const.RESP_PERMISOS_CALL_PHONE: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission granted
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
					checkPermission();
				} else {
					// permission denied
					Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
					//mostrarMensajePermisos(Const.RESP_PERMISOS_CALL_PHONE);
				}
				return;
			}
		}
	}

	public void mostrarMensajePermisos(final int resp_permiso) {
		final Dialog dialog = new Dialog(CustomActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_generico);
		((TextView) dialog.findViewById(R.id.txtMensaje)).setText("Negar los permisos impedir? el correcto funcionamiento de la aplicaci?n");
		((Button) dialog.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				if (resp_permiso == Const.RESP_PERMISOS_STORAGE) {
					ActivityCompat.requestPermissions(CustomActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, Const.RESP_PERMISOS_STORAGE);
				} else if (resp_permiso == Const.RESP_PERMISOS_PHONE) {
					ActivityCompat.requestPermissions(CustomActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, Const.RESP_PERMISOS_PHONE);
				} else if (resp_permiso == Const.RESP_PERMISOS_LOCATION) {
					ActivityCompat.requestPermissions(CustomActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.RESP_PERMISOS_LOCATION);
				} else if (resp_permiso == Const.RESP_PERMISOS_CAMERA) {
					ActivityCompat.requestPermissions(CustomActivity.this, new String[]{Manifest.permission.CAMERA}, Const.RESP_PERMISOS_CAMERA);
				} else if (resp_permiso == Const.RESP_PERMISOS_CALL_PHONE) {
					ActivityCompat.requestPermissions(CustomActivity.this, new String[]{Manifest.permission.CALL_PHONE}, Const.RESP_PERMISOS_CALL_PHONE);
				}
			}
		});
		dialog.setCancelable(false);
		dialog.show();
	}
}
