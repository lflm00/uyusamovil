package celuweb.com.uyusa;

import java.util.Date;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.MotivoAbono;

public class FormCarteraRegistrarFactura extends Activity {

	Cartera cartera;
	Cartera carteraAux;
	Vector<MotivoAbono> listaMotivosAbono;
	boolean esPagoTotal = true;
	long tiempoClick1 = 0;
	private boolean isNota = false;
	boolean adicionarCarteraAux = false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_cartera_registrar_factura);
		
		if (Main.usuario == null || Main.usuario.codigoVendedor == null)
			DataBaseBO.CargarInfomacionUsuario();
		
		inicializar();
		cargarDatosRecaudo();
		Util.closeTecladoStartActivity(this);
	}
	
	public void inicializar(){
		adicionarCarteraAux = false;
		cartera = null;
		Bundle bundle = getIntent().getExtras();

		if (bundle != null && bundle.containsKey("cartera"))
			cartera = (Cartera) bundle.get("cartera");
		
		if (bundle != null && bundle.containsKey("carteraAux"))
			carteraAux = (Cartera) bundle.get("carteraAux");
		
		if (bundle != null && bundle.containsKey("nota"))
			isNota  = bundle.getBoolean("nota");
		
		if (bundle != null && bundle.containsKey("adicionarCarteraAux"))
			adicionarCarteraAux  = bundle.getBoolean("adicionarCarteraAux");

	}



	public void cargarDatosRecaudo() {
		
		if (cartera != null) {
			
			if(cartera.saldo < 0){
				
				RadioButton radioPagoParcial = ((RadioButton) findViewById(R.id.radioPagoParcial));
				
				radioPagoParcial.setEnabled(false);	
				radioPagoParcial.setClickable(false);	
				radioPagoParcial.setFocusableInTouchMode(false);	
				
			}

			String strSaldo = "" + cartera.saldo;
			strSaldo = Util.QuitarE(strSaldo.replace("e+", "E"));

			((RadioButton) findViewById(R.id.radioPagoTotal)).setChecked(true);

			((TextView) findViewById(R.id.lblDocRegistroRecaudo))
					.setText(cartera.documento);

			((TextView) findViewById(R.id.lblFechaRegistroRecaudo))
					.setText(cartera.fecha);
			((TextView) findViewById(R.id.lblVencimientoRegistroRecaudo))
					.setText(cartera.FechaVecto);
			((TextView) findViewById(R.id.lblDiasVencRegistroRecaudo))
					.setText("" + cartera.dias);

			((TextView) findViewById(R.id.lblValorRegistroRecaudo))
					.setText(Util.SepararMiles(strSaldo));

			
			EditText txtValorARecaudar = (EditText) findViewById(R.id.txtValorARecaudar);
			txtValorARecaudar.setText(strSaldo + "");
			txtValorARecaudar.setEnabled(false);

			Vector<String> listaItems = new Vector<String>();
			listaMotivosAbono = DataBaseBO.ListaMotivoAbono(listaItems);

			if (listaItems.size() > 0) {

				String[] items = new String[listaItems.size()];
				listaItems.copyInto(items);

				Spinner cbMotivoPagoParcial = (Spinner) findViewById(R.id.cbMotivoPagoParcial);
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item, items);

				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				cbMotivoPagoParcial.setAdapter(adapter);
			}
		}
		
		if(isNota)
			OnClickAceptarRegisrarRecaudo(null);
		
	}
	
	public void OnClickPagoTotal(View view) {
		
		if (cartera != null) {

			String strSaldo = "" + cartera.saldo;
			strSaldo = Util.QuitarE(strSaldo.replace("e+", "E"));

			EditText txtValorARecaudar = (EditText) findViewById(R.id.txtValorARecaudar);
			txtValorARecaudar.setText(strSaldo);
			txtValorARecaudar.setEnabled(false);

			((TableLayout) findViewById(R.id.tblLayoutMotivoAbono))
					.setVisibility(TableLayout.GONE);

		} else {

			Util.MostrarAlertDialog(this,
					"No se pudo Cargar la Informacion de la Factura");
		}
	}
	
	public void OnClickPagoParcial(View view) {
		
		EditText txtValorARecaudar = (EditText) findViewById(R.id.txtValorARecaudar);
		txtValorARecaudar.setText("");
		txtValorARecaudar.setEnabled(true);

		((TableLayout) findViewById(R.id.tblLayoutMotivoAbono))
				.setVisibility(TableLayout.VISIBLE);
		
	}
	
	public void OnClickAceptarRegisrarRecaudo(View view) {
		
		Date date = new Date(); 
		
		long tiempoClick2 = date.getTime();
		
		long dif1 = tiempoClick2-tiempoClick1;
		
		if(dif1 < 0){
			
			dif1 = dif1*(-1);
			
		}
		
		
	   if(dif1 > 2000){
		
		
		tiempoClick1 = 	tiempoClick2;
	
		String valor = ((EditText) findViewById(R.id.txtValorARecaudar)).getText().toString();
		float valorARecaudar = Util.ToFloat(Util.Redondear(valor, 2));


		if (valorARecaudar == 0) {

			Util.MostrarAlertDialog(this,
					"Por favor ingrese el valor del Recaudo");
			((EditText) findViewById(R.id.txtValorARecaudar)).requestFocus();

		} else {

			if (cartera != null) {
				
				
				CheckBox checkProntoPago = (CheckBox) findViewById(R.id.checkProntoPago);

				if(checkProntoPago.isChecked()) {
					cartera.prontoPago = true;
					cartera.valordpp = cartera.valorProntoPago;
				}
				else {
					cartera.prontoPago = false;
					cartera.valordpp = 0f;
				}

				cartera.valorARecaudar = valorARecaudar;
				
				RadioButton radioPagoTotal =((RadioButton) findViewById(R.id.radioPagoTotal));
				RadioButton radioPagoParcial =((RadioButton) findViewById(R.id.radioPagoParcial));
				
				if(radioPagoTotal.isChecked()){
					cartera.pagoTotal = true;
					System.out.println("Total is checked");
					cartera.esPagoParcial = false;
				}else if(radioPagoParcial.isChecked()){
					System.out.println("Parcial is checked");
					cartera.pagoTotal = false;
					cartera.esPagoParcial = true;
				}
				
				EditText txtObservacionRegistroRecaudo = (EditText)findViewById(R.id.txtObservacionRegistroRecaudo);
				
				cartera.observacion =  "";
				
					if (cartera.esPagoParcial && valorARecaudar >= cartera.saldo) {
						Util.MostrarAlertDialog(this,"El valor del pago parcial debe ser menor al valor total del recaudo");
						return;
					}
					
				
				if(radioPagoTotal.isChecked()){
					
				  cartera.observacion = txtObservacionRegistroRecaudo.getText().toString();
				
				}
				else{
				
				  Spinner cbMotivoPagoParcial = (Spinner) findViewById(R.id.cbMotivoPagoParcial);
				  MotivoAbono motivoAbono = listaMotivosAbono.elementAt(cbMotivoPagoParcial.getSelectedItemPosition());
				  cartera.observacion = txtObservacionRegistroRecaudo.getText().toString()+" "+motivoAbono.nombre;
				
				}

				System.out.println("RadioTotal checked es--> Total:"+cartera.pagoTotal+" o parcial:"+cartera.esPagoParcial);
				
				DataBaseBO.agregarFactura(cartera, carteraAux);
				Main.cartera.add(cartera);
				
				if(carteraAux!=null){
					
					if(adicionarCarteraAux){
					
					 Main.cartera.addElement(carteraAux);
					
					}
					
				}
				
				setResult(RESULT_OK);
				finish();
				

			} else {

				Util.MostrarAlertDialog(this,
						"No se pudo Cargar la Informacion de la Factura");
			}
		}
		
			
		
	   }
	}
	
	public void OnClickCancelarRegisrarRecaudo(View view) {
		
		setResult(RESULT_CANCELED);
		finish();
	}
	
	
	public void  onClickCheckProntoPago(View view){
		
		CheckBox checkProntoPago = (CheckBox)findViewById(R.id.checkProntoPago);
		
		if(checkProntoPago.isChecked()){
		if(cartera != null)
			if(cartera.dias2 <= 0){
				
				EditText txtValorPontoPago = (EditText)findViewById(R.id.txtProntoPago);
				txtValorPontoPago.setText(""+cartera.valorProntoPago);
				
				
				double valor = cartera.saldo-cartera.valorProntoPago;
				
				String strSaldo = "" + valor;
				strSaldo = Util.QuitarE(strSaldo.replace("e+", "E"));
				
			
				EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
				txtValorARecaudar.setText(strSaldo);
				txtValorARecaudar.setEnabled(false);
				
				cartera.conConsignacion = false;
				
				
			}else{
				
				
				
				 AlertDialog.Builder builder = new AlertDialog.Builder(FormCarteraRegistrarFactura.this);
					builder.setMessage("Por Vencimiento no se puede Aplicar Prontopago. Desea Habilitarlo para el pago con consignacion? ")
					.setCancelable(false)
					.setPositiveButton("Si", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int id) {
							
							dialog.cancel();
						
							
							//consignacion = 1
				            //F39.tprontopago.Text = convertcurrency(globalprontopago)
				            //trecaudo.Text = convertcurrency(Valmoneda(tsaldo.Text) - globalprontopago)
							
							cartera.conConsignacion = true;
							
							
							EditText txtValorPontoPago = (EditText)findViewById(R.id.txtProntoPago);
							txtValorPontoPago.setText(""+cartera.valorProntoPago);
							
							
							double valor = cartera.saldo-cartera.valorProntoPago;
							
							String strSaldo = "" + valor;
							strSaldo = Util.QuitarE(strSaldo.replace("e+", "E"));
							
						
							EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
							txtValorARecaudar.setText(strSaldo);
							//txtValorARecaudar.setEnabled(false);
							
							
							
							
						}
					})
					
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int id) {
							
							dialog.cancel();
							
							
							cartera.conConsignacion = false;
							
							 if(esPagoTotal){
				                        //trecaudo.Text = tsaldo.Text
				                        //F39.tprontopago.Text = convertcurrency(0)
								 
									String strSaldo = "" + cartera.saldo;
									strSaldo = Util.QuitarE(strSaldo.replace("e+", "E"));
									
									EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
									txtValorARecaudar.setText(strSaldo);
									txtValorARecaudar.setEnabled(false);
									
									
									EditText txtValorProntoPago = (EditText)findViewById(R.id.txtProntoPago);
									txtValorProntoPago.setText("0");
									
									((TableLayout) findViewById(R.id.tblLayoutMotivoAbono)).setVisibility(TableLayout.GONE);
							
								 
								 
								 
							 }else{
				                       // F39.tprontopago.Text = convertcurrency(0)
				                       // trecaudo.Text = convertcurrency(0)
								 
									EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
									txtValorARecaudar.setText("0");
									txtValorARecaudar.setEnabled(true);
									
									EditText txtValorProntoPago = (EditText)findViewById(R.id.txtProntoPago);
									txtValorProntoPago.setText("0");
									
									
									((TableLayout) findViewById(R.id.tblLayoutMotivoAbono)).setVisibility(TableLayout.VISIBLE);

								 
								 
								 
							 }
							 
							 ((CheckBox) findViewById(R.id.checkProntoPago)).setChecked(false);
							
							
							
						}
					});
					
					AlertDialog alert = builder.create();
					alert.show();
				 
				
				
				
			}
		
		
		}else{
			
			
			RadioButton rbPagoTotal = ((RadioButton) findViewById(R.id.radioPagoTotal));
			
			if(rbPagoTotal.isChecked()){
				
				
				EditText txtValorPontoPago = (EditText)findViewById(R.id.txtProntoPago);
				txtValorPontoPago.setText("0");
				
				
				double valor = cartera.saldo;
				
				String strSaldo = "" + valor;
				strSaldo = Util.QuitarE(strSaldo.replace("e+", "E"));
				
			
				EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
				txtValorARecaudar.setText(strSaldo);
				txtValorARecaudar.setEnabled(false);
				
				
				
				
			}else{
				
				EditText txtValorPontoPago = (EditText)findViewById(R.id.txtProntoPago);
				txtValorPontoPago.setText("0");
				
				
				EditText txtValorARecaudar = (EditText)findViewById(R.id.txtValorARecaudar);
				txtValorARecaudar.setText("0");
				
				
			}
			
			
			
			
		}
		
		
	}
	
	@Override
	protected void onResume() {
		
		super.onResume();
		
		//Main.usuario = null;
		//Main.cliente = null;
		
		if(Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null){
		 
			DataBaseBO.CargarInfomacionUsuario();
			
		}
		
		
		if(Main.cliente == null || Main.cliente.codigo == null){
			
			int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
			Cliente clienteSel;
			
				if(tipoDeClienteSelec == 1){
						
					clienteSel = DataBaseBO.CargarClienteSeleccionado();	
						
				}else{
						
						
					clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();
						
						
				}
				
				if(clienteSel != null)
				  Main.cliente = clienteSel; 
					
			
		    }
		
		refrescarDatosRecaudo();

	}


	public void refrescarDatosRecaudo() {
		
		
		if(Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null){
			 
			DataBaseBO.CargarInfomacionUsuario();
			
		}
		
		
		if(Main.cliente == null || Main.cliente.codigo == null){
			
			int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
			Cliente clienteSel;
			
				if(tipoDeClienteSelec == 1){
						
					clienteSel = DataBaseBO.CargarClienteSeleccionado();	
						
				}else{
						
						
					clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();
						
						
				}
				
				if(clienteSel != null)
				  Main.cliente = clienteSel; 
					
			
		    }
		
		
		
//    	objeto obj = DataBaseBO.obtenerObjeto();
//    	
//    	if( obj != null){
//    		
//    		Main.cartera = obj.cartera;
//    		Main.listaFormasPago = obj.listaFormasPago;
//    		Main.total_forma_pago = obj.total_forma_pago;
//    		Main.total_descuento = obj.total_descuento;
//    		Main.total_recaudo = obj.total_recaudo;
//
//    	}

	}





}
