package celuweb.com.uyusa;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import celuweb.com.DataObject.URL;

/**
 * 
 * Activity para lanzar la pagina web de PQR.
 * @author JICZ
 *
 */
public class FormWebPqrActivity extends Activity {

	
	
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_web_pqr);	

		URL u = new URL();
		String url = u.darUrl();	
		
		WebView webViewPqr = (WebView) this.findViewById(R.id.webViewPQR);
		webViewPqr.loadUrl(url);	
	}
}
