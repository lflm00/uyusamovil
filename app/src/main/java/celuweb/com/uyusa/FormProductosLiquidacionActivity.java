package celuweb.com.uyusa;
import java.util.UUID;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.ConsecutivoFactura;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.Usuario;
import celuweb.com.compensacion.Compensacion;
import celuweb.com.compensacion.ICompensacion;
import celuweb.com.printer.sewoo.SewooLKP20;

public class FormProductosLiquidacionActivity extends Activity implements ICompensacion { 

	public static final String TAG = FormProductosLiquidacionActivity.class.getName();
	
	/**
	 * Control de evento de doble click
	 */
	private long mLastClickTime = 0;
	Typeface fontMM;
	Typeface font;
	String cod_cliente;
	ProgressDialog progressDialog;
	boolean salir;
	private Vector<Producto> listaRegistros;
	Cliente cliente = null;
	Usuario usuario = null;
	Dialog dialogEnviarInfo;
	boolean tienePedido = false;
	int opcionLiquidar = 0 ;
	boolean existeReg = false;
	boolean primerEjecucion = true;
	private  String macImpresora;
	String mensaje;
	String nroDocRegistro ;
	String nroDocRegistroGlobal ;
	Usuario liquidador;
	Dialog dialogLogin;
	Vector<Usuario> listaJefes;
	String tipo ;
	private String nroDocInforme;
	
	/**
	 * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
	 */
	private SewooLKP20 sewooLKP20;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_liquidacion_productos);
		
		Usuario user = DataBaseBO.CargarUsuario();
		nroDocInforme = DataBaseBO.ObtenterNumeroDocLiq(user.codigoVendedor);
		
		Util.closeTecladoStartActivity(this);
		cargarBundle();
		inicializarVista();
	}
	

	private void cargarBundle() {

		Bundle bundle = getIntent().getExtras();

		if (bundle != null)
			opcionLiquidar = bundle.getInt("liquidacionOpciones");

		liquidador = new Usuario();
		if (bundle != null && bundle.containsKey("liquidador"))
			liquidador = (Usuario) bundle.getSerializable("liquidador");

		if (liquidador != null)
			System.out.println("Liquidador:\n\n" + liquidador.codigoVendedor + " - " + liquidador.nombreVendedor);

	}
	
	private void inicializarVista() {
		
		switch (opcionLiquidar) {
		case Const.IS_PRODUCTO_DANADO:
				tipo = "DANADO";
			((TextView)findViewById(R.id.txtInfo)).setText("Registro de Productos Danados");
			break;
			
		case Const.IS_INVENTARIO_LIQUIDACION:
			tipo = "PRODUCTO INVENTARIO";
			((TextView)findViewById(R.id.txtInfo)).setText("Registro de Inventario");
			break;

		default:
			((TextView)findViewById(R.id.txtInfo)).setText("Registros de Productos Liquidacion");
			break;
		}
		
	}


	@Override
	public void onResume() {
		super.onResume();
		
		cliente = Main.cliente;
		usuario = Main.usuario;
		if (cliente==null) {
			cliente = DataBaseBO.CargarClienteSeleccionado();
		}
		if (usuario==null) {
			usuario = DataBaseBO.CargarUsuario();
		}
		
		progressDialog = ProgressDialog.show(FormProductosLiquidacionActivity.this, "", "Cargando informacion...", true);
		progressDialog.show();

		
		Compensacion compensacion = new Compensacion(FormProductosLiquidacionActivity.this,(ICompensacion)FormProductosLiquidacionActivity.this, nroDocInforme, usuario.codigoVendedor, usuario.bodega);
		compensacion.execute("null");
			

	}
	
	
	 private class Task extends AsyncTask<String, Integer, Long> {
		  
		  protected Long doInBackground(String... urls) {

			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
		   
		   
			FormProductosLiquidacionActivity.this.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					cargarListaProductosLiquidacion();
				}
			});

		   return 0l;
		  }

		  protected void onProgressUpdate(Integer... progress) {

		  }

		protected void onPostExecute(Long result) {

			FormProductosLiquidacionActivity.this.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					if (progressDialog != null)
						progressDialog.dismiss();

				}
			});

		}
		 }
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	


	public boolean cargarListaProductosLiquidacion(){

		boolean state = false;
		String and  = "";
		
		if(opcionLiquidar == Const.IS_PRODUCTO_DANADO || opcionLiquidar == Const.IS_INVENTARIO_LIQUIDACION)
			and = " AND diferencia <> 0 ";
		
		
		listaRegistros = DataBaseBO.listaProductosLiquidados(tipo, and);
		
		if (listaRegistros != null) {

			TableLayout tablaHeadListaProductosLiq = (TableLayout)findViewById(R.id.tablaHeadListaProductosLiq);
			tablaHeadListaProductosLiq.removeAllViews();

			LayoutInflater inflater1 = this.getLayoutInflater();
			View viewhead = inflater1.inflate(R.layout.list_item_tabla_head_registros_liquidados, null);

			if (opcionLiquidar == Const.IS_PRODUCTO_DANADO || opcionLiquidar == Const.IS_INVENTARIO_LIQUIDACION) {
				String[] head = { "Codigo", "Producto", "Fisico", "Inv. Final", "Diferencia" };
				for (int i = 0; i < head.length; i++) {
					TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
					textView.setText(head[i]);
					textView.setVisibility(View.VISIBLE);
				}
			} else {
				
				String[] head = { "Codigo", "Producto", "Ingresado", "Inventario", "Transaccion", "Diferencia" };
				for (int i = 0; i < head.length; i++) {
					TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
					textView.setText(head[i]);
					textView.setVisibility(View.VISIBLE);
				}
			}
			
			tablaHeadListaProductosLiq.addView(viewhead);

			TableLayout tablaBodyListaProductosLiq = (TableLayout)findViewById(R.id.tablaBodyListaProductosLiq);
			tablaBodyListaProductosLiq.removeAllViews();
			int position = 0;
			for (Producto item : listaRegistros) {
				if(pintarDetalleLiquidacion(item, tablaBodyListaProductosLiq, position)){
					state = true;
				}
				position++;
			}
			
			if( progressDialog != null )
				progressDialog.dismiss();
		} else {

			TableLayout tablaListaAgendaEjecutiva = (TableLayout)findViewById(R.id.tablaHeadListaProductosLiq);
			tablaListaAgendaEjecutiva.removeAllViews();
			state = false;
			Util.mostrarToast(FormProductosLiquidacionActivity.this, "No existen registros de liquidacion.");
		}
		return state;
	}
	
	
	public boolean pintarDetalleLiquidacion(final Producto item, LinearLayout tablaBodyListaProductosLiq, int position) {

		LayoutInflater inflater = this.getLayoutInflater();
		View view = inflater.inflate(R.layout.list_item_tabla_registros_liquidados, null);

		TableRow rowTableElemento = (TableRow)view.findViewById(R.id.rowTableElemento);
		rowTableElemento.setTag(item);
		
		TextView codigo = (TextView) view.findViewById(R.id.column0);
		codigo.setText(" "+item.codigo);
		codigo.setTag(item);
		codigo.setVisibility(View.VISIBLE);
		if ( position % 2 == 1 ) {
			codigo.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		TextView descripcion = (TextView) view.findViewById(R.id.column1);
		descripcion.setText(item.descripcion);
		descripcion.setTag(item);
		descripcion.setVisibility(View.VISIBLE);
		if ( position % 2 == 1 ) {
			descripcion.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		
		LinearLayout columnCantIngresado = (LinearLayout) view.findViewById(R.id.column2);
		columnCantIngresado.setVisibility(View.VISIBLE);
		
		EditText edtIngresado = (EditText) view.findViewById(R.id.edTxtCantIngresado);
		edtIngresado.setText(""+item.cantidadDigitada);
		edtIngresado.setTag(item);
		edtIngresado.setEnabled(false);
		
		if ( position % 2 == 1 ) {
			columnCantIngresado.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		LinearLayout columnInventario = (LinearLayout) view.findViewById(R.id.column3);
		columnInventario.setVisibility(View.VISIBLE);
		EditText edtInventario= (EditText) view.findViewById(R.id.edTxtCantidadInv);
		if (opcionLiquidar == Const.IS_INVENTARIO_LIQUIDACION)
			edtInventario.setText(""+(item.cantidadInventario - 0));	//edtInventario.setText(""+(item.cantidadInventario - item.cantidadTransaccion));
		else
			edtInventario.setText(""+item.cantidadInventario);
			
		edtInventario.setTag(item);
		edtInventario.setEnabled(false);
		
		if ( position % 2 == 1 ) {
			columnInventario.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		LinearLayout columnCantTrans = (LinearLayout) view.findViewById(R.id.column4);

		if (opcionLiquidar == Const.IS_PRODUCTO_DANADO || opcionLiquidar == Const.IS_INVENTARIO_LIQUIDACION) {
			columnCantTrans.setVisibility(View.GONE);
		} else {
			columnCantTrans.setVisibility(View.VISIBLE);
		}

		EditText edtTransaccion = (EditText) view.findViewById(R.id.edTxtCantidadTrans);
		edtTransaccion.setText("" + item.cantidadTransaccion);
		edtTransaccion.setTag(item);
		edtTransaccion.setEnabled(false);
			
		if (position % 2 == 1) {
			columnCantTrans.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		
		
		LinearLayout columnCantDiferencia = (LinearLayout) view.findViewById(R.id.column5);
		columnCantDiferencia.setVisibility(View.VISIBLE);
		
		EditText edtDiferencian = (EditText) view.findViewById(R.id.edTxtDiferencia);
		edtDiferencian.setText(""+item.diferencia);
		edtDiferencian.setTag(item);
		edtDiferencian.setEnabled(false);
		
		if ( position % 2 == 1 ) {
			columnCantDiferencia.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		tablaBodyListaProductosLiq.addView(view);
		return true;

	}

	
	public void salir() {
		
		if(DataBaseBO.existeCompensacion()){
			
			String nroDocCompensacion = DataBaseBO.ObtenterNroDocCompensacion("COMPENSACIONES");
			
			if(!DataBaseBO.eliminarCompensacion(nroDocCompensacion))
				Util.MostrarAlertDialog(this,  "Ocurri� un problema al eliminar la informaci�n.");
			
		}
	
		if(!DataBaseBO.eliminarRegistrosLiq(tipo)){
			Util.MostrarAlertDialog(FormProductosLiquidacionActivity.this, "Ocurrio un problema al eliminar registro temporal");	
			
		}else{
			
			Intent returnIntent = new Intent();
			returnIntent.putExtra("liquidacionOpciones", opcionLiquidar);
			returnIntent.putExtra("liquidador", liquidador);
			setResult(Activity.RESULT_CANCELED, returnIntent);
			finish();
		}
	}
	
	public void cancelar() {
		finish();
	}
	


	public void onClickGuardar(View view) {
		/* Evitar evento de doble click */
		if (SystemClock.elapsedRealtime() - mLastClickTime < 5000) {
			return;
		}
		mLastClickTime = SystemClock.elapsedRealtime();
		
		/*Guardar el informe de liquidacion*/
		ConsecutivoFactura consecutivo = DataBaseBO.obtenerConsecutivoFactura();
		DataBaseBO.guardarInformeLiquidado(nroDocInforme, consecutivo, tipo);
		
		Intent returnIntent = new Intent();
		returnIntent.putExtra("result", true);
		setResult(Activity.RESULT_OK, returnIntent);
		finish();
		
	}
	
	public void onClickImprimir(View view) {
		
		impresionTirilla();
		
	}
	
	

	


	protected void mensajeGuardar(String mensaje) {
		
		AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(FormProductosLiquidacionActivity.this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				
				
				dialog.cancel();
				salir();
			}
		});
		alertDialog = builder.create();
		alertDialog.setMessage(mensaje);
		alertDialog.show();
		
	}


	public void onClickSalir(View view) {
		
		AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(FormProductosLiquidacionActivity.this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
				salir();
			}
		}).setCancelable(false).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				
			}
			
			
		});

		alertDialog = builder.create();
		alertDialog.setMessage("Est� seguro de cancelar el registro?");
		alertDialog.show();
		
		
//		mostrarDialogoLogin();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			mostrarDialogoLogin();
		}
		return true;
	}
	
	public void mostrarDialogoLogin() {

		if (dialogLogin != null) {
			
			if (dialogLogin.isShowing()) {
				dialogLogin.dismiss();
			}
			dialogLogin.cancel();
		}

		dialogLogin = new Dialog(this);
		dialogLogin.setContentView(R.layout.dialog_login);
		dialogLogin.setTitle("Login");

		EditText etClave = (EditText) dialogLogin.findViewById(R.id.etClave);
		etClave.setText("");

		Spinner spUsuarios = (Spinner) dialogLogin.findViewById(R.id.spUsuarios);

		String[] items;
		Vector<String> listaItems = new Vector<String>();
		listaJefes = DataBaseBO.ListaLiquidadores(listaItems, false); //false para cargar jefes, true para cargar liquidadores

		if (listaItems.size() > 0) {

			items = new String[listaItems.size()];
			listaItems.copyInto(items);

		} else {

			items = new String[] {};
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spUsuarios.setAdapter(adapter);

		((Button) dialogLogin.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				if (listaJefes.size() > 0) {

					EditText etClave = (EditText) dialogLogin.findViewById(R.id.etClave);

					if (etClave.getText().toString().equals("")) {

						Util.mostrarToast(FormProductosLiquidacionActivity.this, "Ingrese Contrasena");
						etClave.requestFocus();

					} else {

						Spinner spUsuarios = (Spinner) dialogLogin.findViewById(R.id.spUsuarios);

						String usuario = listaJefes.elementAt(spUsuarios.getSelectedItemPosition()).codigoVendedor;
						String clave = etClave.getText().toString();

						if (DataBaseBO.LogInLiquidadores(usuario, clave, false)) {

							salir();

						} else {

							Util.MostrarAlertDialog(FormProductosLiquidacionActivity.this, "Ocurrio un error al ingresar");
							etClave.setText("");

						}

					}

				} else {

					EditText etClave = (EditText) dialogLogin.findViewById(R.id.etClave);
					Util.mostrarToast(FormProductosLiquidacionActivity.this, "Seleccione un Usuario");
					etClave.setText("");
				}

			}
		});

		((Button) dialogLogin.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				dialogLogin.cancel();
			}
		});

		dialogLogin.setCancelable(false);
		dialogLogin.show();
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialogLogin.getWindow();
		lp.copyFrom(window.getAttributes());
		//This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);

	}	
	
	private Handler handlerFinish = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			if (progressDialog != null)
				progressDialog.cancel();

		}
	};



	
	
	@Override
	public void registrarCompensacionTerminada(boolean ejecucionTerminada) {
		
		if (ejecucionTerminada) {
			
			final ProgressDialog progress = ProgressDialog.show(this, "Liquidando.", "Terminando liquidacion...", true);
			progress.show();
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					
					/*generar las acciones en hilo principal de views*/
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							if(progress != null){
								progress.dismiss();
							}
							
							new Task().execute("");
//							
						}
					});
				}
			}).start();
		}
		else {
			Util.MostrarAlertDialog(this, "Error calculando la compensacion, por favor intente nuevamente.");
			return;
		}
	}
	
	
	private void impresionTirilla() {
		
		
		progressDialog = ProgressDialog.show(FormProductosLiquidacionActivity.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
		progressDialog.show();
		
		
		SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
		macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

		SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
		String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");

		if (macImpresora.equals("-")) {
			
			Util.MostrarAlertDialog(FormProductosLiquidacionActivity.this, "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");
			if (progressDialog != null)
				progressDialog.cancel();
			
			
		} else {
			
			if (!tipoImpresora.equals("Intermec")) {
				sewooLKP20 = new SewooLKP20(FormProductosLiquidacionActivity.this);
				imprimirSewooLKP20(macImpresora);
			} else {
				imprimirTirillaGeneral(macImpresora);
			}
		}
		
	}
	

	/**
	 * Imprimir la factura del pedido.
	 * @param macImpresora
	 * @param numero_doc
	 * @param copiaPrint
	 */
	protected void imprimirSewooLKP20(final String macImpresora ) {
		
		
		final Usuario usuario = DataBaseBO.ObtenerUsuario();
		
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				Looper.prepare();

				if (macImpresora.equals("-")) {
					if (progressDialog != null)
						progressDialog.dismiss();
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Toast.makeText(FormProductosLiquidacionActivity.this, "Aun no hay Impresora Predeterminada.\n\nPor Favor primero Configure la Impresora!",
									Toast.LENGTH_SHORT).show();
						}
					});
				} else {

					if (sewooLKP20 == null) {
						sewooLKP20 = new SewooLKP20(FormProductosLiquidacionActivity.this);
					}
					int conect = sewooLKP20.conectarImpresora(macImpresora);

					switch (conect) {
					case 1:
						
						sewooLKP20.generarImpresionLiquidacion(opcionLiquidar, usuario, liquidador, tipo);
						sewooLKP20.imprimirBuffer(true);
						
						break;

					case -2:
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(FormProductosLiquidacionActivity.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
							}
						});
						break;

					case -8:
						if (progressDialog != null)
							progressDialog.dismiss();
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Util.MostrarAlertDialog(FormProductosLiquidacionActivity.this, "Bluetooth apagado. Por favor habilite el bluetoth para imprimir.");
							}
						});
						break;

					default:
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(FormProductosLiquidacionActivity.this, "Error desconocido, intente nuevamente.", Toast.LENGTH_SHORT).show();
							}
						});
						break;
					}

					try {
						Thread.sleep(2500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (sewooLKP20 != null) {
						sewooLKP20.desconectarImpresora();
						if (progressDialog != null)
							progressDialog.dismiss();
					}
				}
				Looper.myLooper().quit();
			}
		}).start();
	}
	
	private void imprimirTirillaGeneral(final String macAddress) {

		new Thread(new Runnable() {

			public void run() {

				mensaje = "";
				BluetoothSocket socket = null;

				try {

					Looper.prepare();

					BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

					if (bluetoothAdapter == null) {

						mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

					} else if (!bluetoothAdapter.isEnabled()) {

						mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

					} else {

						BluetoothDevice printer = null;

						printer = bluetoothAdapter.getRemoteDevice(macAddress);

						if (printer == null) {

							mensaje = "No se pudo establecer la conexion con la Impresora.";

						} else {

							UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

							SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
							String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");

							if (tipoImpresora.equals("Intermec")) {

								socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);

							} else {

								socket = printer.createRfcommSocketToServiceRecord(uuid);

							}

							if (socket != null) {

								socket.connect();

								Thread.sleep(3500);

								if (tipoImpresora.equals("Intermec")) {

									ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.generarImpresionLiquidacion(nroDocRegistro, opcionLiquidar, liquidador, tipo));

								}

								handlerFinish.sendEmptyMessage(0);

							} else {

								mensaje = "No se pudo abrir la conexion con la Impresora.\n\nPor Favor intente de nuevo.";
							}

						}

					}

					if (!mensaje.equals("")) {

						handlerFinish.sendEmptyMessage(0);
					}

					Looper.myLooper().quit();

				} catch (Exception e) {

					String motivo = e.getMessage();

					mensaje = "No se pudo ejecutar la Impresion.";

					if (motivo != null) {
						mensaje += "\n\n" + motivo;
					}

					handlerFinish.sendEmptyMessage(0);

				} finally {

				}
			}

		}).start();

	}

	
}
