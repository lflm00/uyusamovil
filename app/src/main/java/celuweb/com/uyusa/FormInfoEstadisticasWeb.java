/**
 * 
 */
package celuweb.com.uyusa;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import celuweb.com.DataObject.URL;

/**
 * @author JICZ
 *
 */
public class FormInfoEstadisticasWeb extends Activity {

	/**
	 * 
	 */
	public FormInfoEstadisticasWeb() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_web_info_estadisticas);
		
		// el cero es para usar la URLInfo que esta en el metodo sobrecargado de la clase URL.
		URL u = new URL(0);
		String url = u.darUrl(0);	
		
		WebView webViewPqr = (WebView) this.findViewById(R.id.webViewInfoEstadisticas);
		webViewPqr.loadUrl(url);
	}

	
	
	
}
