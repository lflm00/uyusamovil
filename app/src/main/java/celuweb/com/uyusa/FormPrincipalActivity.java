package celuweb.com.uyusa;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.system.ErrnoException;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.util.ArrayList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import celuweb.com.BusinessObject.ConfigBO;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.DataBaseBOJ;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Config;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.EstadisticaRecorrido;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;

public class FormPrincipalActivity extends CustomActivity implements Sincronizador {
    Dialog dialogSalir;
    ProgressDialog progressDialog, progressDialog2;
    AlertDialog alertDialog;

    AlertDialog alert;
    boolean nextPedido;
    Intent formRutaProgramadaG;
    int cantClientes = 0;
    int visitas = 0;
    double valorVenta = 0;
    private Dialog dialogAgotados;
    private ArrayList<ItemListView> items;

    /**
     * Variables Coordenadas
     **/
    public double latitud;
    public double longitud;
    private Handler timer = new Handler();
    TaskGPS taskGPS;
    public long time = 2 * 60 * 1000; //Cada 2 minutos lanza la captura de Coordenadas

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_principal);
        Inicializar();
        cargarVersion();
        iniciarServicioGPS();

        mostrarDialogAgotadosYdevolucionesClientes();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.blue_dark));
        }

        if (estaGPSEncendido(getApplicationContext())) {


            obtenerCoordenada();

        } else {
            mostrarMensajeActivarGPS();
        }
    }


    @Override
    protected void onResume() {
        CargarInfoUsuario();
        lanzarTimerGPS(LocationManager.NETWORK_PROVIDER);
        super.onResume();
    }


    public void iniciarServicioGPS() {
        if (!isServiceGPSRunning()) {
            Intent intent = new Intent(this, ServiceGPS.class);
            startService(intent);
        }
    }

    protected boolean isServiceGPSRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            String nameSevice = service.service.getClassName();
            if (ServiceGPS.class.getName().equals(nameSevice)) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint("MissingPermission")
    public void Inicializar() {
        ConfigBO.CrearConfigDB();
        // DataBaseBO.ActualizarSnEnvio();
        DataBaseBO.BorrarPedidosSinTerminar();
        try {
            Main.versionApp = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            Log.i("FormPrincipalActivity", "Version = " + Main.versionApp);
        } catch (NameNotFoundException e) {
            Main.versionApp = "0.0.0";
            Log.e("FormPrincipalActivity", e.getMessage(), e);
        }
        setTitle("ZZVende " + Main.versionApp + Const.TITULO);
        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Main.deviceId = manager.getDeviceId();
        Log.i("FormPrincipalActivity", "deviceId = " + Main.deviceId);
    }

    private int cargarTotalClientesRuta() {
        int diaActual = Util.ObtenerDiaSemanaNumeroEntero();
        String diaRuta2 = String.valueOf(cargaRuta2(diaActual));
        int total = DataBaseBO.getCantClientesRutero(diaRuta2);
        return total;
    }

    private String cargaRuta2(int position) {

        String strDia = "";

        switch (position) {

            case 0:
                strDia = "DOMINGO";
                break;
            case 1:
                strDia = "LUNES";
                break;
            case 2:
                strDia = "MARTES";
                break;
            case 3:
                strDia = "MIERCOLES";
                break;
            case 4:
                strDia = "JUEVES";
                break;
            case 5:
                strDia = "VIERNES";
                break;
            case 6:
                strDia = "SABADO";
                break;
            default:
                strDia = "SIN RUTA";
        }

        return strDia;
    }

    public void CargarInfoUsuario() {
        DataBaseBO.CargarInfomacionUsuario();
        Usuario usuario = DataBaseBO.ObtenerUsuario();
        EstadisticaRecorrido estadisticaRecorrido = DataBaseBO.CargarEstadisticas(Main.usuario.fechaConsecutivo);
        visitas = DataBaseBO.obtenerTotalDeVisitas();
        cantClientes = cargarTotalClientesRuta();
        valorVenta = estadisticaRecorrido.total_venta;
        if (usuario != null && usuario.codigoVendedor != null) {
            String msg = usuario.nombreVendedor;
            ((TextView) findViewById(R.id.lblUsuario)).setText(Html.fromHtml(msg));
            ((TextView) findViewById(R.id.lblFechaLabores)).setText(usuario.fechaLabores);
        } else {
            String msg = getString(R.string.without_data);
            ((TextView) findViewById(R.id.lblUsuario)).setText(Html.fromHtml(msg));
            ((TextView) findViewById(R.id.lblFechaLabores)).setText("");
        }
        ((TextView) findViewById(R.id.txtVisita)).setText(cantClientes + "\n" + getString(R.string.customers_to_visit));
        ((TextView) findViewById(R.id.txtVisitados)).setText(visitas + "\n" + getString(R.string.customers_visited));
        String valorVentaStr = Util.SepararMiles(Util.QuitarE(Util.Redondear(String.valueOf(valorVenta), 2)));
        ((TextView) findViewById(R.id.txtTotalVentas)).setText(valorVentaStr + "\n" + getString(R.string.total_order));
    }

    public void OnClickClienteNuevo(View view) {
        if (DataBaseBO.ExisteInformacion("Clientes")) {
            Intent intent = new Intent(this, FormClienteNuevo.class);
            startActivity(intent);
        } else {
            MostrarAlert(getString(R.string.msg_new_client_download_info), false);
        }
    }

    public void onClickFormPrincipalActivity(View view) {
        switch (view.getId()) {
            case R.id.btnPedido:
                boolean cabeceraDisponible = DataBaseBO.cabeceraDisponibles();
                DataBaseBO.setRutaProgramada();
                if (Main.rutaProgramada.equals("")) {
                    formRutaProgramadaG = new Intent(this, FormRutaProgramadaActivity.class);
                    MostrarAlert(getString(R.string.msg_route_calendar_no_exist), true);
                } else {
                    Intent formRutaProgramada = new Intent(this, FormRutaProgramadaActivity.class);
                    startActivity(formRutaProgramada);
                }
                break;
            case R.id.btnEstadisticas:
                Intent intent = new Intent(this, FormMenuEstadistica.class);
                startActivityForResult(intent, Const.RESP_FORM_INICIAR_DIA);
                break;
            //
            // case R.id.btnCerrarSesion:
            //
            // CerrarAplicacion();
            // break;
            case R.id.btnConsultarPrecios:
                Intent intent1 = new Intent(this, FormConsultarPreciosActivity.class);
                startActivityForResult(intent1, Const.RESP_FORM_INICIAR_DIA);
                break;

        }
    }

    public boolean validarHoraMaximaEnvio() throws ParseException {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String time = Util.FechaActual("HH:mm");
        String endtime = DataBaseBO.ObtenerHoraMaximoEnvio();
        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if (date1.before(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }





    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
        switch (codeRequest) {
            case Const.ENVIAR_PEDIDO:
                break;


        }
    }


    public void OnClickCerrarSesion(View view) {

        if (DataBaseBO.HayInformacionXEnviar1()) {

            AlertDialog alertDialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    setResult(RESULT_OK);
                    Sync sync = new Sync(FormPrincipalActivity.this, Const.ENVIAR_PEDIDO);
                    sync.start();
                    finish();
                    dialog.cancel();
                }
            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alertDialog = builder.create();
            alertDialog.setMessage(getString(R.string.msg_leave_app));
            alertDialog.show();

        }

    }

    public void MostrarAlert(String mensaje, boolean continuarPedido) {
        nextPedido = continuarPedido;
        if (alert == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    if (nextPedido) {
                        startActivity(formRutaProgramadaG);
                    }
                }
            });
            alert = builder.create();
        }
        alert.setMessage(mensaje);
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.RESP_FORM_INICIAR_DIA && resultCode == RESULT_OK) {
            CargarInfoUsuario();
        }
    }

    public void MostrarAlert(String mensaje) {
        if (alert == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alert = builder.create();
        }
        alert.setMessage(mensaje);
        alert.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //CerrarAplicacion();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * mostrar el dialog de agotados y deviluciones, solamente cuando hay items
     * que mostrar.
     */
    public void mostrarDialogAgotadosYdevolucionesClientes() {
        dialogAgotados = new Dialog(this, R.style.AppTheme);
        dialogAgotados.setContentView(R.layout.dialog_agotydev_clientes);
        dialogAgotados.setCancelable(true);
        ListView listaViewClientes = (ListView) dialogAgotados.findViewById(R.id.listaClientesAgotDev);
        cargarListaClientesConAgotadosYDevoluciones(listaViewClientes);
        if (!items.isEmpty()) {
            dialogAgotados.show();
        }
    }

    /**
     * cargar la lista de pedidos no sincronizados.
     */
    private void cargarListaClientesConAgotadosYDevoluciones(ListView listaViewClientes) {
        // lista de productos (referencias) cargados.
        ArrayList<Cliente> listaClientes = DataBaseBO.clientesConAgotadosYDevoluciones();
        items = new ArrayList<ItemListView>();
        for (Cliente cliente : listaClientes) {
            ItemListView item = new ItemListView();
            item.titulo = cliente.codigo; // el titulo del item es el codigo del
            // cliente.
            item.subTitulo = cliente.razonSocial;
            items.add(item);
        }
        // cargar el adapter en el listView
        ListViewClientesAgotyDevAdapter adapter = new ListViewClientesAgotyDevAdapter(FormPrincipalActivity.this, items);
        listaViewClientes.setAdapter(adapter);
        listaViewClientes.setOnItemClickListener(listenerListView);
    }

    private OnItemClickListener listenerListView = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
            if (DataBaseBO.ExisteDataBase()) {
                if (DataBaseBO.TerminoLabores()) {
                    Util.MostrarAlertDialog(FormPrincipalActivity.this, getString(R.string.msg_finalizo_order));
                } else if (DataBaseBO.ExisteInformacion("Rutero")) {
                    Config config = ConfigBO.ObtenerConfigUsuario();
                    if (config == null) {
                        MostrarAlert(getString(R.string.msg_config_empty));
                    } else {
                        if (Util.estaActualLaFecha()) {
                            CargarInformacionCliente(position);
                            dialogAgotados.dismiss();
                        } else {
                            Util.MostrarAlertDialog(FormPrincipalActivity.this, getString(R.string.msg_new_date_download_info), 1);
                        }
                    }
                } else {
                    MostrarAlert(getString(R.string.msg_without_rout_download_info));
                }
            } else {
                MostrarAlert(getString(R.string.msg_without_rout_download_info2));
            }
        }
    };

    /**
     * cargar informacion del cliente seleccionado para mostrar su informacion
     *
     * @param position
     */
    public void CargarInformacionCliente(final int position) {
        ItemListView item = items.get(position);
        final Cliente clienteSel = DataBaseBO.BuscarCliente(item.titulo);
        boolean tienePedido = DataBaseBO.ExistePedidoCliente(clienteSel.codigo);
        if (tienePedido) {
            AlertDialog alertDialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Usuario usuario = DataBaseBO.CargarUsuario();
                    if (usuario == null) {
                        Toast.makeText(getBaseContext(), getString(R.string.msg_without_info_user_), Toast.LENGTH_LONG).show();
                    } else {
                        Main.cliente = clienteSel;
                        DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
                        Intent formInfoCliente = new Intent(FormPrincipalActivity.this, FormInfoClienteActivity.class);
                        startActivity(formInfoCliente);
                    }
                    dialog.cancel();
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alertDialog = builder.create();
            alertDialog.setMessage(getString(R.string.msg_customer_with_order) + clienteSel.razonSocial + getString(R.string.msg_customer_with_order_two));
            alertDialog.show();
        } else {
            Usuario usuario = DataBaseBO.CargarUsuario();
            if (usuario == null) {
                Toast.makeText(getBaseContext(), getString(R.string.msg_without_info_user_), Toast.LENGTH_LONG).show();
            } else {
                // Guarda la Referencia al Cliente seleccionado.
                Main.cliente = clienteSel;
                DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
                Intent formInfoCliente = new Intent(FormPrincipalActivity.this, FormInfoClienteActivity.class);
                startActivity(formInfoCliente);
            }
        }
    }

    public void cargarVersion() {
        String version = Util.obtenerVersion(this);
        ((TextView) findViewById(R.id.txtVersion)).setText("Version " + version);
    }

    /**
     * Metodos para capturar coordenadas y guardarlas
     **/


    /***
     *
     * @return
     */

    public boolean obtenerCoordenada() {

        boolean estado = false;
        GPSTracker gpsTracker = new GPSTracker(this);
        Location location = gpsTracker.getLocation();
        boolean estadoGPS = gpsTracker.isEstadoGPS();
        double lati = 0;
        double longi = 0;
        //String direccionCapturada = "Direccion Aprox Capturada: ";
        //String ciudadCapturada = "Ciudad Capturada: ";
        if (location != null && estadoGPS) {
            lati = location.getLatitude();
            longi = location.getLongitude();
            //direccionCapturada += gpsTracker.getAddress(location);
            //ciudadCapturada += gpsTracker.getCity(location);
        }

        Log.i("***", " coordenadas capturada LAT: " + lati + " LONG: " + longi);

        if (estadoGPS) {
            if (lati != 0 && longi != 0) {
                Coordenada coordenada = new Coordenada();
                coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                coordenada.codigoCliente = "0";
                coordenada.latitud = latitud;
                coordenada.longitud = longitud;
                coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
                coordenada.id = Coordenada.obtenerId(Main.cliente.codigo);
                latitud = lati;
                longitud = longi;
                boolean guardo = Coordenada.save(FormPrincipalActivity.this, coordenada);
                DataBaseBO.guardarCoordenada(coordenada, Main.cliente.codigo);
                if (guardo) {
                    Log.i("GUARDO", "OK");
                    //    /*  Toast.makeText(getApplicationContext(), "Coordenadas Obtenidas con exito \n"+
                    //      lati+"\n"+longi, Toast.LENGTH_LONG).show();*/


                }
                estado = true;
            } else {

                estado = false;
            }
        } else {
            Util.mostrarAlertDialog(FormPrincipalActivity.this, "Por favor encienda el GPS");
        }

        return estado;
    }


    public static boolean estaGPSEncendido(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return statusOfGPS;


    }

    private void mostrarMensajeActivarGPS() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormPrincipalActivity.this);
        builder.setMessage("El GPS est? desactivado. Por favor act?velo").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, 1100);
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void MostrarAlertDialogSinCoordenada() {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                String mensaje = "";
                String bton = "";

                mensaje = "No se obtuvieron las Coordenadas,Desea Continuar sin Coordenadas";
                bton = "Continuar Sin Coordenada";

                AlertDialog.Builder builder = new AlertDialog.Builder(FormPrincipalActivity.this);
                builder.setCancelable(false).setPositiveButton(bton, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();

                    }
                });

                alertDialog = builder.create();


                alertDialog.setMessage(mensaje);
                alertDialog.show();

            }
        });


    }

    public void lanzarTimerGPS(String provider) {

        //Se guarda el Provider para Captura de Coordenadas
        setGPSProvider(provider);

        Timer timer = new Timer();
        taskGPS = new FormPrincipalActivity.TaskGPS();
        timer.schedule(taskGPS, 0, time);
    }

    public void setGPSProvider(String provider) {
        SharedPreferences settings = getSharedPreferences("settings_gps", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("provider", provider);
        editor.commit();
    }

    private class TaskGPS extends TimerTask {

        public void run() {

            if (handlerGPS != null)
                handlerGPS.sendEmptyMessage(0);
        }
    }

    private Handler handlerGPS = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (latitud == 0 && longitud == 0) {
                //      MostrarAlertDialogSinCoordenada();
            } else {
                obtenerCoordenada();
            }
        }
    };
}
