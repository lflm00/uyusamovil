package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Categoria;
import celuweb.com.DataObject.ClienteNuevo;
import celuweb.com.DataObject.Encabezado;

public class FormEditarCliente extends Activity implements Sincronizador {
	
	Dialog dialogRuta;
	ProgressDialog progressDialog;
	Vector<Categoria> listaCategorias;
	String[] dias;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.form_editar_cliente);		
		
		dias = new String[] { getResources().getString(R.string.lunes),getResources().getString(R.string.martes),getResources().getString(R.string.miercoles),getResources().getString(R.string.jueves),getResources().getString(R.string.viernes),getResources().getString(R.string.sabado),getResources().getString(R.string.domingo)};		
		CargarCiudad();
		CargarCategorias();
		cargarDatosCliente();
		cargarRuta();
	}
	
	public void CargarCiudad() {
		
		String[] items;
		Vector<String> listaCiudad = DataBaseBO.ListaCiudadCliente();

		if (listaCiudad.size() > 0) {

			items = new String[listaCiudad.size()];
			listaCiudad.copyInto(items);

		} else {

			items = new String[] {};

			if (listaCiudad != null)
				listaCiudad.removeAllElements();
		}
		
		Spinner cbCiudadRutero = (Spinner) findViewById(R.id.cbCiudad);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbCiudadRutero.setAdapter(adapter);
        
        cbCiudadRutero.setSelection(obtenerIdCiudad(listaCiudad));
	}
	
	
	private void cargarDatosCliente(){
		
		/*((EditText)findViewById(R.id.txtNombreCliente)).setText(Main.cliente.nombre);
		((EditText)findViewById(R.id.txtRazonSocial)).setText(Main.cliente.razonSocial);
		((EditText)findViewById(R.id.txtNit)).setText(Main.cliente.nit);
		((EditText)findViewById(R.id.txtDireccion)).setText(Main.cliente.direccion);
		((EditText)findViewById(R.id.txtTelefono)).setText(Main.cliente.telefono);
		((EditText)findViewById(R.id.txtBarrio)).setText(Main.cliente.territorio);
		((EditText)findViewById(R.id.txtOrdenVisita)).setText(""+Main.cliente.ordenVisita);*/
	}
	
	public void OnClickSeleccionarRuta(View view) {
		
		MostrardialogRuta();
	}
	
	public void MostrardialogRuta() {
		
		if (dialogRuta == null) {
			
			dialogRuta = new Dialog(this);
			dialogRuta.requestWindowFeature(Window.FEATURE_LEFT_ICON);
			dialogRuta.setContentView(R.layout.dialog_dias_ruta);
			dialogRuta.setTitle("Ingresar Ruta");
		}
		
		((Button)dialogRuta.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				boolean lunes     = ((CheckBox)dialogRuta.findViewById(R.id.checkLunes)).isChecked();
				boolean martes    = ((CheckBox)dialogRuta.findViewById(R.id.checkMartes)).isChecked();
				boolean miercoles = ((CheckBox)dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
				boolean jueves    = ((CheckBox)dialogRuta.findViewById(R.id.checkJueves)).isChecked();
				boolean viernes   = ((CheckBox)dialogRuta.findViewById(R.id.checkViernes)).isChecked();
				boolean sabado    = ((CheckBox)dialogRuta.findViewById(R.id.checkSabado)).isChecked();
				boolean domingo   = ((CheckBox)dialogRuta.findViewById(R.id.checkDomingo)).isChecked();
				
				String ruta = "";
				ruta += lunes ? "Lunes" : "";
				ruta += martes ? (ruta.equals("") ? "" : ", " ) + "Martes" : "";
				ruta += miercoles ? (ruta.equals("") ? "" : ", " ) + "Miercoles" : "";
				ruta += jueves ? (ruta.equals("") ? "" : ", " ) + "Jueves" : "";
				ruta += viernes ? (ruta.equals("") ? "" : ", " ) + "Viernes" : "";
				ruta += sabado ? (ruta.equals("") ? "" : ", " ) + "Sabado" : "";
				ruta += domingo ? (ruta.equals("") ? "" : ", " ) + "Domingo" : "";
				
				String rutaParada = Util.SepararPalabrasTextView(ruta, 30);
				(((TextView)findViewById(R.id.lblRutaParada))).setText(Html.fromHtml(rutaParada));
				
				dialogRuta.cancel();
			}
		});
		
		((Button) dialogRuta.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {

				dialogRuta.cancel();
			}
		});
		
		dialogRuta.setCancelable(false);
		dialogRuta.show();
		dialogRuta.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_list);
	}
	
	private int obtenerIdCiudad(Vector<String> listaCiudad){
		
		for (int i=0; i<listaCiudad.size(); i++) {
			
			/*Log.d("Main.cliente.ciudad", ""+Main.cliente.ciudad);
			Log.d("Main.cliente.ciudad", ""+listaCiudad.get(i));
			if(Main.cliente.ciudad.equals(listaCiudad.get(i))){
				return i;
			}	*/	
		}	
		
		return 0;
	}
	
	public void CargarCategorias() {
		
		String[] items;
		Vector<String> listaItems = new Vector<String>();
		listaCategorias = DataBaseBO.ListaCategorias(listaItems);

		if (listaItems.size() > 0) {

			items = new String[listaItems.size()];
			listaItems.copyInto(items);

		} else {

			items = new String[] {};

			if (listaCategorias != null)
				listaCategorias.removeAllElements();
		}
		
		Spinner cbCategoria = (Spinner) findViewById(R.id.cbCategoria);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbCategoria.setAdapter(adapter);
	}
	
	private void cargarRuta(){
		
		//String rutaParada = Util.SepararPalabrasTextView(binaryToDay(Main.cliente.ruta_parada), 30);
		//(((TextView)findViewById(R.id.lblRutaParada))).setText(Html.fromHtml(rutaParada));
	}
	
	private String binaryToDay(String binario) {

		String cadenaDias = "";
		for (int i = 0; i < binario.length() - 1; i++) {

			if (binario.substring(i, i + 1).equals("1")) {
				cadenaDias += dias[i] + ", ";
				habilitarDia(i);
			}
		}

		if (cadenaDias.length() > 1) {
			cadenaDias = cadenaDias.substring(0, cadenaDias.length() - 2);
		}
		return cadenaDias;
	}
	
	public void habilitarDia(final int posDia) {
		
		if (dialogRuta == null) {
			
			dialogRuta = new Dialog(this);
			dialogRuta.requestWindowFeature(Window.FEATURE_LEFT_ICON);
			dialogRuta.setContentView(R.layout.dialog_dias_ruta);
			dialogRuta.setTitle("Ingresar Ruta");
		}
			
		switch(posDia) {
			case 0:
				 ((CheckBox)dialogRuta.findViewById(R.id.checkLunes)).setChecked(true);
				break;
				
			case 1:
				 ((CheckBox)dialogRuta.findViewById(R.id.checkMartes)).setChecked(true);
				break;
			
			case 2:
				 ((CheckBox)dialogRuta.findViewById(R.id.checkMiercoles)).setChecked(true);
				break;
			
			case 3:
				 ((CheckBox)dialogRuta.findViewById(R.id.checkJueves)).setChecked(true);
				break;
				
			case 4:
				 ((CheckBox)dialogRuta.findViewById(R.id.checkViernes)).setChecked(true);
				break;
			
			case 5:
				 ((CheckBox)dialogRuta.findViewById(R.id.checkSabado)).setChecked(true);
				break;
				
			case 6:
				 ((CheckBox)dialogRuta.findViewById(R.id.checkDomingo)).setChecked(true);
				break;	
		}
	}
	
	public void OnClickGuardarCliente(View view) {
		
		ClienteNuevo clienteNuevo = new ClienteNuevo();
		int index = ((Spinner) findViewById(R.id.cbCategoria)).getSelectedItemPosition();;
		Categoria categoria = listaCategorias.elementAt(index);
		
		clienteNuevo.codigo      = Main.cliente.codigo; 
		clienteNuevo.nombre      = ((EditText)findViewById(R.id.txtNombreCliente)).getText().toString().trim();
		clienteNuevo.razonSocial = ((EditText)findViewById(R.id.txtRazonSocial)).getText().toString().trim();
		clienteNuevo.nit         = clienteNuevo.codigo; 
		clienteNuevo.direccion   = ((EditText)findViewById(R.id.txtDireccion)).getText().toString().trim();
		clienteNuevo.ciudad      = ((Spinner) findViewById(R.id.cbCiudad)).getSelectedItem().toString();
		clienteNuevo.telefono    = ((EditText)findViewById(R.id.txtTelefono)).getText().toString().trim();
		clienteNuevo.vendedor    = Main.usuario.codigoVendedor;
		clienteNuevo.ruta_parada = ObtenerRutaParada();
		clienteNuevo.actividad   = categoria.codigo;
		clienteNuevo.ordenVisita = Util.ToInt(((EditText)findViewById(R.id.txtOrdenVisita)).getText().toString());
		clienteNuevo.tipoDoc     = "1";
		clienteNuevo.territorio  = ((EditText)findViewById(R.id.txtBarrio)).getText().toString().trim();
		clienteNuevo.agencia     = "";
		
		if (clienteNuevo.nombre.equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese el nombre del Cliente");
			((EditText)findViewById(R.id.txtNombreCliente)).requestFocus();
			return;
		}
		
		if (clienteNuevo.razonSocial.equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese la Razon Social");
			((EditText)findViewById(R.id.txtRazonSocial)).requestFocus();
			return;
		}
		
		if (clienteNuevo.nit.equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese el Nit");
			((EditText)findViewById(R.id.txtNit)).requestFocus();
			return;
		}
		
		if (clienteNuevo.direccion.equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese la Direccion");
			((EditText)findViewById(R.id.txtDireccion)).requestFocus();
			return;
		}
		
		if (clienteNuevo.ordenVisita == 0) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese el Orden Visita.");
			((EditText)findViewById(R.id.txtOrdenVisita)).requestFocus();
			return;
		}
		
		if (clienteNuevo.ruta_parada.trim().equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese la Ruta");
			return;
		}
		
		if (clienteNuevo.territorio.trim().equals("")) {
			
			Util.MostrarAlertDialog(this, "Por favor ingrese el barrio");
			((EditText)findViewById(R.id.txtBarrio)).requestFocus();
			return;
		}
		
		boolean ingreso = DataBaseBO.GuardarClienteMod(clienteNuevo);

		if (ingreso) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Cliente Actualizado con Exito")	
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					progressDialog = ProgressDialog.show(FormEditarCliente.this, "", "Enviando Informacion Cliente...", true);
					progressDialog.show();
					
					//DataBaseBO.ActualizarSnEnvio(Main.encabezado.id, 1);
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					Sync sync = new Sync(FormEditarCliente.this, Const.ENVIAR_PEDIDO);
					sync.start();
				}
			});
			/*.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					//DataBaseBO.ActualizarSnEnvio(Main.encabezado.id, 0);
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					FormClienteNuevo.this.setResult(RESULT_OK);
					FormClienteNuevo.this.finish();
				}
			});*/
			
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	public void OnClickCancelar(View view) {
		
		finish();
	}
	
	public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
	
		final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : msg;
		
		if (progressDialog != null)
			progressDialog.cancel();
		
		this.runOnUiThread(new Runnable() {
			
			public void run() {
				
				AlertDialog.Builder builder = new AlertDialog.Builder(FormEditarCliente.this);
				builder.setMessage(mensaje)
						
				.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.cancel();
						
						FormEditarCliente.this.setResult(RESULT_OK);
						FormEditarCliente.this.finish();
					}
				});
				
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}
	
	public String ObtenerRutaParada() {
		
		String rutaParada = "";
		
		if (dialogRuta != null) {
			
			boolean lunes     = ((CheckBox)dialogRuta.findViewById(R.id.checkLunes)).isChecked();
			boolean martes    = ((CheckBox)dialogRuta.findViewById(R.id.checkMartes)).isChecked();
			boolean miercoles = ((CheckBox)dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
			boolean jueves    = ((CheckBox)dialogRuta.findViewById(R.id.checkJueves)).isChecked();
			boolean viernes   = ((CheckBox)dialogRuta.findViewById(R.id.checkViernes)).isChecked();
			boolean sabado    = ((CheckBox)dialogRuta.findViewById(R.id.checkSabado)).isChecked();
			boolean domingo   = ((CheckBox)dialogRuta.findViewById(R.id.checkDomingo)).isChecked();
			
			rutaParada += lunes ? "1" : "0";
			rutaParada += martes ? "1" : "0";
			rutaParada += miercoles ? "1" : "0";
			rutaParada += jueves ? "1" : "0";
			rutaParada += viernes ? "1" : "0";
			rutaParada += sabado ? "1" : "0";
			rutaParada += domingo ? "1" : "0";
		}

		return rutaParada;
	}
}
