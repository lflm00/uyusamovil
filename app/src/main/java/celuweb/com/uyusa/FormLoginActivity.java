package celuweb.com.uyusa;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.internal.NavigationMenu;
import com.mikepenz.iconics.Iconics;
import com.mikepenz.iconics.context.IconicsContextWrapper;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import celuweb.com.BusinessObject.ConfigBO;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Config;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.Usuario;
import io.github.yavski.fabspeeddial.FabSpeedDial;

public class FormLoginActivity extends AppCompatActivity implements Sincronizador {

    public static final String TAG = FormLoginActivity.class.getName();

    TaskGPS taskGPS;
    Criteria criteria;
    GPSListener gpsListener;
    Dialog dialogConfiguracion;
    Dialog dialogActualizarInventario;
    Dialog dialogIniciarDia;
    LocationManager locationManager;
    Location currentLocation = null;

    ProgressDialog progressDialog;

    //Registra Coordenadas Cada 10 Minutos
    public long time = 10 * 60 * 1000;


    //Registra Coordenadas Cada 10 Minutos
    public long time2 = 10 * 60 * 1000;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.main_autoventa);
        super.onCreate(savedInstanceState);
        checkPermission();
        validarVista();
        CargarVersion();
        loadFonts();


        turnGPSOn();



        turnGPSOn_();

        String listaCLientesSel = "2:74.4@3:71.7";
        String[] data = listaCLientesSel.split("@");
        Log.e(TAG, "--------------------------------------------");
        Log.e(TAG, "listaCLientesSel-> " + listaCLientesSel);
        Log.e(TAG, "datos[]length-> " + data.length);
        Log.e(TAG, "datos[0]-> " + data[0]);
        Log.e(TAG, "datos[1]-> " + data[1]);
        Log.e(TAG, "--------------------------------------------");


    }

    private void loadFonts() {
        //Registra fuente iconos celuweb
        Iconics.init(getApplicationContext());
        Iconics.registerFont(new celuweb.com.fonts.FontIcon());
        //Registra fuente aplicacion celuweb
        celuweb.com.fonts.Font font = new celuweb.com.fonts.Font();
        font.setCalligrapher(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }


    @Override
    protected void onResume() {
        validarVista();
        accionBtnFlotante();
        super.onResume();
    }

    private void accionBtnFlotante() {

        FabSpeedDial fabSpeedDial = (FabSpeedDial) findViewById(R.id.fabSpeedDial);
        fabSpeedDial.setMenuListener(new FabSpeedDial.MenuListener() {

            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                Util.mostrarToast(getApplicationContext(), menuItem.getTitle() + "");
                return true;
            }

            @Override
            public void onMenuClosed() {

            }
        });
    }


    private void validarVista() {

        Usuario usuario = DataBaseBO.ObtenerUsuario();


        if (usuario != null) {
            EditText txtUsuarioMain = (EditText) findViewById(R.id.txtUsuarioMain);
            try {
                txtUsuarioMain.setText(usuario.codigoVendedor + "");

            } catch (Exception e) {
                // TODO: handle exception
            }


        }
    }

    public void CargarVersion() {

        String versionApp;

        try {

            versionApp = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            Log.i(TAG, "CargarVersion -> " + versionApp);

        } catch (NameNotFoundException e) {

            versionApp = "1.0";
            Log.e(TAG, "CargarVersion -> " + e.getMessage(), e);
        }

        setTitle(getTitle() + " " + versionApp + Const.TITULO);
    }

    public void OnClickFabMenu(View view) {
        System.err.println("dialog");
    }


    public void OnClickLogin(View view) {

        final String usuario = ((EditText) findViewById(R.id.txtUsuarioMain)).getText().toString().trim().toUpperCase();
        final String password = ((EditText) findViewById(R.id.txtClaveMain)).getText().toString().trim().toUpperCase();


        if (usuario.equals("") || password.equals("")) {

            Util.MostrarAlertDialogEstandar(FormLoginActivity.this, getString(R.string.msg_user_incorrect));
            ((EditText) findViewById(R.id.txtUsuarioMain)).requestFocus();

        } else {

            if (Util.estaGPSEncendido(this)) {

                if (Util.checkSDCard()) {

                    if (DataBaseBO.ExisteDataBase()) {

                        boolean existe = DataBaseBO.LogIn(usuario, password);

                        if (existe) {

                            //MostrarFormPrincipal();
                            actualizarInfo();

                        } else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                    LogInServer(usuario, password);
                                }

                            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                }
                            });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.setMessage(getString(R.string.msg_user_incorrect_server));
                            alertDialog.show();
                        }

                    } else {

                        // No existe Base de datos, Se loguea con el Servidor
                        LogInServer(usuario, password);
                    }

                } else {


                    Util.MostrarAlertDialogEstandar(this, getString(R.string.msg_card_not_exist));


                }
            } else {

                mostrarMensajeActivarGPS();

            }
        }
    }

    private void mostrarMensajeActivarGPS() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.msg_disable_gps)
                .setCancelable(false)

                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(myIntent, Const.ENABLE_GPS);
                        dialog.cancel();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();

    }

    public void OnClickSalir(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.msg_leave_app)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        if (locationManager != null)
                            locationManager.removeUpdates(gpsListener);

                        finish();
                        System.exit(0);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void LogInServer(String usuario, String password) {

        if (IsOnline()) {

            progressDialog = new ProgressDialog(FormLoginActivity.this);
            ;//, "", R.string.msg_logging_in, true);
            progressDialog.setTitle(R.string.msg_logging_in_title);
            progressDialog.setMessage(getString(R.string.msg_logging_in));
            progressDialog.setCancelable(false);
            progressDialog.show();

            Sync sync = new Sync(FormLoginActivity.this, Const.LOGIN);
            sync.usuario = usuario;
            sync.clave = password;
            sync.start();

        } else {

            Util.MostrarAlertDialogEstandar(this, getString(R.string.msg_without_connection));
        }
    }

    public boolean IsOnline() {

        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting())
            return true;

        return true;
    }

    public void MostrarFormPrincipal() {

        IniciarTimerGPS();


		iniciarGPS2();
        EditText txtUsuarioMain = (EditText) findViewById(R.id.txtUsuarioMain);
        OcultarTeclado(txtUsuarioMain);
        Intent intent = new Intent(this, FormPrincipalActivity.class);
        startActivityForResult(intent, Const.RESP_FORM_LOGIN);
    }

    public void OcultarTeclado(EditText editText) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Const.RESP_FORM_LOGIN) {

            ((EditText) findViewById(R.id.txtUsuarioMain)).setText("");
            ((EditText) findViewById(R.id.txtClaveMain)).setText("");
            ((EditText) findViewById(R.id.txtUsuarioMain)).requestFocus();
        }
    }

    @Override
    public void RespSync(boolean ok, final String respuestaServer, final String msg, int codeRequest) {


        switch (codeRequest) {

            case Const.LOGIN:


                try {

                    if (progressDialog != null)
                        progressDialog.cancel();

                    if (ok) {


                        String codigoUsuario = "";
                        String nombreUsuario = "";
                        String fechaLabor = "";
                        String aux = "";
                        String bodega = "";

                        String[] arreglo2 = Util.split(respuestaServer, ";");

                        if (arreglo2.length >= 5) {


                            codigoUsuario = arreglo2[1];
                            nombreUsuario = arreglo2[2];
                            fechaLabor = arreglo2[3];
                            bodega = arreglo2[4];


                            codigoUsuario = Util.quitaEspacios(codigoUsuario);
                            bodega = Util.quitaEspacios(bodega);

                        }


                        boolean hayInfo = DataBaseBO.HayInformacionXEnviar1();
                        final String usuarioActual = DataBaseBO.ObtenerUsuarioActual();

                        if (hayInfo) {

                            if (usuarioActual == null) {

                                ConfigBO.CrearConfigDB();
                                String usuarioLogin = ((EditText) findViewById(R.id.txtUsuarioMain)).getText().toString().trim();
                                ConfigBO.GuardarConfigUsuario(usuarioLogin, bodega, 1);

                                //No Existe usuario Actual, Carga el Formulario Principal
                                //MostrarFormPrincipal();
                                actualizarInfo();

                            } else {

                                String usuarioLogin = ((EditText) findViewById(R.id.txtUsuarioMain)).getText().toString().trim();

                                if (usuarioActual.equals(usuarioLogin)) {

                                    ConfigBO.CrearConfigDB();
                                    ConfigBO.GuardarConfigUsuario(usuarioLogin, bodega, 1);
                                    //El Usuario Actual es igual al Usuario que se Logueo, Carga el Formulario Principal
                                    //MostrarFormPrincipal();
                                    actualizarInfo();

                                } else {

                                    this.runOnUiThread(new Runnable() {

                                        public void run() {

                                            Util.MostrarAlertDialog(FormLoginActivity.this, getString(R.string.msg_pending_information) + usuarioActual + ".\n\n" + getString(R.string.msg_pending_information_part_two) + usuarioActual);
                                        }
                                    });
                                }
                            }

                        } else {

                            //No hay Informacion Pendiente por enviar.
                            //Verifica si el Usuario Actual es diferente del Usuario que se esta Logueando, en Caso tal Borra la Base de Datos

                            String usuarioLogin = ((EditText) findViewById(R.id.txtUsuarioMain)).getText().toString().trim();

                            if (usuarioActual != null) {

                                if (!usuarioActual.equals(usuarioLogin)) {


                                    boolean borro = Util.BorrarDataBase();
                                    ConfigBO.CrearConfigDB();
                                    ConfigBO.GuardarConfigUsuario(usuarioLogin, bodega, 1);


                                    if (!borro) {

                                        Util.MostrarAlertDialogEstandar(this, getString(R.string.msg_user_incorrect_try_again));
                                        return;
                                    }


                                }
                            }

                            ConfigBO.CrearConfigDB();
                            ConfigBO.GuardarConfigUsuario(usuarioLogin, bodega, 1);
                            //MostrarFormPrincipal();
                            actualizarInfo();
                        }

                    } else {

                        this.runOnUiThread(new Runnable() {

                            public void run() {

                                ((TextView) findViewById(R.id.txtClaveMain)).setText("");
                                ((EditText) findViewById(R.id.txtUsuarioMain)).requestFocus();

                                Util.MostrarAlertDialog(FormLoginActivity.this, msg);
                            }
                        });
                    }

                } catch (Exception e) {

                    String mensaje = e.getMessage();
                    Log.e(TAG, "RespSync -> " + mensaje, e);
                }

			/*this.runOnUiThread(new Runnable() {

			public void run() {

				onClickLogin.reset();
			}
		});*/

                break;

            case Const.ENVIAR_PEDIDO:
                RespuestaEnviarInfo(ok, respuestaServer, msg);
                break;

            case Const.ENVIAR_COORDENADAS:

                break;

            case Const.DOWNLOAD_DATA_BASE:
                RespuestaDownloadInfo(ok, respuestaServer, msg);
                break;
            case Const.DOWNLOAD_VERSION_APP:
                RespuestaDownloadVersionApp(ok, respuestaServer, msg);
                break;


        }
    }


    public void RespSync_(boolean ok, final String respuestaServer, final String msg, int codeRequest) {

        try {

            if (progressDialog != null)
                progressDialog.cancel();

            if (ok) {

                boolean hayInfo = DataBaseBO.HayInformacionXEnviar1();
                final String usuarioActual = DataBaseBO.ObtenerUsuarioActual();

                if (hayInfo) {

                    if (usuarioActual == null) {

                        //No Existe usuario Actual, Carga el Formulario Principal
                        //MostrarFormPrincipal();
                        actualizarInfo();

                    } else {

                        String usuarioLogin = ((EditText) findViewById(R.id.txtUsuarioMain)).getText().toString().trim();

                        if (usuarioActual.equals(usuarioLogin)) {

                            //El Usuario Actual es igual al Usuario que se Logueo, Carga el Formulario Principal
                            //MostrarFormPrincipal();
                            actualizarInfo();

                        } else {

                            this.runOnUiThread(new Runnable() {

                                public void run() {

                                    Util.MostrarAlertDialog(FormLoginActivity.this,
                                            R.string.msg_pending_information + usuarioActual + ".\n\n" +
                                                    R.string.msg_pending_information_part_two + usuarioActual);
                                }
                            });
                        }
                    }

                } else {

                    //No hay Informacion Pendiente por enviar.
                    //Verifica si el Usuario Actual es diferente del Usuario que se esta Logueando, en Caso tal Borra la Base de Datos

                    String usuarioLogin = ((EditText) findViewById(R.id.txtUsuarioMain)).getText().toString().trim();

                    if (usuarioActual != null) {

                        if (!usuarioActual.equals(usuarioLogin)) {

                            boolean borro = Util.BorrarDataBase();
                            ConfigBO.GuardarConfigUsuario(usuarioLogin, "", 1);

                            if (!borro) {

                                Util.MostrarAlertDialogEstandar(this, getString(R.string.msg_user_incorrect_try_again));
                                return;
                            }
                        }
                    }

                    //MostrarFormPrincipal();
                    actualizarInfo();
                }

            } else {

                this.runOnUiThread(new Runnable() {

                    public void run() {

                        ((TextView) findViewById(R.id.txtClaveMain)).setText("");
                        ((EditText) findViewById(R.id.txtUsuarioMain)).requestFocus();

                        Util.MostrarAlertDialog(FormLoginActivity.this, msg);
                    }
                });
            }

        } catch (Exception e) {

            String mensaje = e.getMessage();
            Log.e(TAG, "RespSync -> " + mensaje, e);
        }

		/*this.runOnUiThread(new Runnable() {

			public void run() {

				onClickLogin.reset();
			}
		});*/
    }

    /**
     * Coordenadas
     **/

    public void InicializarGPS() {

        if (criteria == null) {

            criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(criteria, true);

        if (provider == null)
            provider = LocationManager.GPS_PROVIDER;

        if (gpsListener == null)
            gpsListener = new GPSListener();

        //Registra el Listener de Coordenadas.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(provider, 0, 0, gpsListener);
    }

    public void IniciarTimerGPS() {

        Timer timer = new Timer();
        taskGPS = new TaskGPS();
        timer.schedule(taskGPS, 0, time);
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (currentLocation != null) {

                if (locationManager != null)
                    locationManager.removeUpdates(gpsListener);

                DataBaseBO.validarUsuario();

                if (DataBaseBO.ExisteDataBase()) {
                    if (Main.usuario != null && Main.usuario.codigoVendedor != null) {


                        Coordenada coordenada = new Coordenada();
                        coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                        coordenada.codigoCliente = "0";
                        coordenada.latitud = currentLocation.getLatitude();
                        coordenada.longitud = currentLocation.getLongitude();
                        coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                        coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
                        coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);

                        Sync sync = new Sync(FormLoginActivity.this, Const.ENVIAR_COORDENADAS);
                        sync.coordenada = coordenada;
                        sync.start();


                    }
                } else {


                }
            }
        }
    };

    private Handler handlerGPS = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            registrarListenerGPS();
        }
    };

    public void registrarListenerGPS() {

        try {

            String provider = getGPSProvider();

            if (provider != null && !provider.equals("")) {

                if (locationManager == null)
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                if (gpsListener == null)
                    gpsListener = new GPSListener();

                //Registra el Listener de Coordenadas.
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(provider, 0, 0, gpsListener);

            } else {

                Log.e(TAG, "registrarListenerGPS -> No se encontro el Provider");
            }

        } catch (Exception e) {

            Log.e(TAG, "registrarListenerGPS -> " + e.getMessage(), e);
        }
    }

    public String getGPSProvider() {

        SharedPreferences settings = getSharedPreferences(Const.SETTINGS_GPS, MODE_PRIVATE);
        return settings.getString(Const.PROVIDER, "");
    }

    private class GPSListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            if (location != null) {

                currentLocation = location;
                handler.sendEmptyMessage(0);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }


    private class TaskGPS extends TimerTask {

        public void run() {

            if (handlerGPS != null)
                handlerGPS.sendEmptyMessage(0);
        }
    }

    private void turnGPSOn() {

        try {

            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (!provider.contains("gps")) { //if gps is disabled
                final Intent poke = new Intent();
                poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                poke.setData(Uri.parse("3"));
                sendBroadcast(poke);
            }
        }catch (Exception e){
            System.out.println("GPS: Exception"+e);
        }


    }

    private void turnGPSOn_() {

        try {


                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                intent.putExtra("enabled", true);
                sendBroadcast(intent);



        } catch (Exception e) {
            System.out.println("GPS: exception"+e);

        }

    }

    private void turnGPSOff(){
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(provider.contains("gps")){ //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }



    public void iniciarGPS2() {

        turnOnGPS();
        Coordenada.crearTablaCoordenadas();

		/*if (isProviderEnabled(LocationManager.GPS_PROVIDER)) {

			//Verifica si GPS_PROVIDER esta Activo. En caso tal se lanza el Timer de Captura de Coordenadas
			lanzarTimerGPS(LocationManager.GPS_PROVIDER);

    	}else*/
        if (isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            //Verifica si NETWORK_PROVIDER esta Activo. En caso tal se lanza el Timer de Captura de Coordenadas
            lanzarTimerGPS(LocationManager.NETWORK_PROVIDER);

        } else {

            /**
             * Se remueve de los Settings del Movil el nombre del proveedor de GPS.
             * Esto es porque el GPS esta Apagado.
             **/
            removeGPSProvider();

            DataBaseBO.validarUsuario();

            if (DataBaseBO.ExisteDataBase()) {
                if (Main.usuario != null && Main.usuario.codigoVendedor != null) {

                    /**
                     * Se envia la coordenada 0,0 con estado de que el GPS esta Apagado
                     **/

                    Coordenada coordenada = new Coordenada();
                    coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                    coordenada.codigoCliente = "0";
                    coordenada.latitud = 0;
                    coordenada.longitud = 0;
                    coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                    coordenada.estado = Coordenada.ESTADO_GPS_APAGADO;
                    coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                    //coordenada.fecha          =  Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();


                    Sync sync = new Sync(FormLoginActivity.this, Const.ENVIAR_COORDENADAS);
                    sync.coordenada = coordenada;
                    sync.start();

                }
            } else {


            }
        }
    }

    protected void turnOnGPS() {

        //Se valida si se puede Activar el GPS
        boolean turnOn = canToggleGPS();

        if (turnOn) {

            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (provider != null && !provider.contains("gps")) {

                final Intent intent = new Intent();
                intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
                intent.setData(Uri.parse("3"));
                sendBroadcast(intent);
            }
        }
    }

    private boolean canToggleGPS() {

        PackageManager packageManager = getPackageManager();
        PackageInfo packageInfo = null;

        try {

            packageInfo = packageManager.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);

        } catch (NameNotFoundException e) {

            //Paquete no encontrado
            return false;
        }

        if (packageInfo != null) {

            for (ActivityInfo actInfo : packageInfo.receivers) {

                String name = actInfo.name;
                boolean exported = actInfo.exported;

                //Verifica si el receiver es exported. En caso tal se puede actiar el GPS.
                if (name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && exported) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isProviderEnabled(String provider) {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager != null) {

            return manager.isProviderEnabled(provider);
        }

        return false;
    }

    public void lanzarTimerGPS(String provider) {

        //Se guarda el Provider para Captura de Coordenadas
        setGPSProvider(provider);

        Timer timer = new Timer();
        taskGPS = new TaskGPS();
        timer.schedule(taskGPS, 0, time2);
    }

    public void setGPSProvider(String provider) {

        SharedPreferences settings = getSharedPreferences(Const.SETTINGS_GPS, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Const.PROVIDER, provider);
        editor.commit();
    }

    public void removeGPSProvider() {

        SharedPreferences settings = getSharedPreferences(Const.SETTINGS_GPS, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(Const.PROVIDER);
        editor.commit();
    }

    /**
     * Permisos
     * **/

    public boolean isPackageExists(String targetPackage) {
        PackageManager pm = getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage,
                    PackageManager.GET_META_DATA);
        } catch (NameNotFoundException e) {
            return false;
        }
        return true;
    }

    private void checkPermission() {
        try {
            //permisos almacenamiento
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                checkPermissionStorage();
            } else
                //permisos telefono
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    checkPermissionPhone();
                } else
                    //permisos ubicacion
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        checkPermissionLocation();
                    } else
                        //permisos camara
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            checkPermissionCamera();
                        } else
                            //permisos llamadas
                            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                checkPermissionLLamadas();
                            }
        }catch (Exception e){
            System.out.println(e);
        }
    }

    private void checkPermissionStorage() {
        Log.e(TAG, "checkPermissionStorage-> ");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                mostrarMensajePermisos(Const.RESP_PERMISOS_STORAGE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, Const.RESP_PERMISOS_STORAGE);
            }
        }
    }

    private void checkPermissionPhone() {
        Log.e(TAG, "checkPermissionPhone-> ");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                mostrarMensajePermisos(Const.RESP_PERMISOS_PHONE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, Const.RESP_PERMISOS_PHONE);
            }
        }
    }

    private void checkPermissionLocation() {
        Log.e(TAG, "checkPermissionLocation-> ");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                mostrarMensajePermisos(Const.RESP_PERMISOS_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.RESP_PERMISOS_LOCATION);
            }
        }
    }

    private void checkPermissionCamera() {
        Log.e(TAG, "checkPermissionCamera-> ");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                mostrarMensajePermisos(Const.RESP_PERMISOS_CAMERA);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, Const.RESP_PERMISOS_CAMERA);
            }
        }
    }

    private void checkPermissionLLamadas() {
        Log.e(TAG, "checkPermissionLLamadas-> ");
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CALL_PHONE)) {
                mostrarMensajePermisos(Const.RESP_PERMISOS_CALL_PHONE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, Const.RESP_PERMISOS_CALL_PHONE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {


        Log.e(TAG, "onRequestPermissionsResult-> requestCode " + requestCode);
        Log.e(TAG, "onRequestPermissionsResult-> permissions " + permissions);
        Log.e(TAG, "onRequestPermissionsResult-> grantResults " + grantResults);


        switch (requestCode) {
            case Const.RESP_PERMISOS_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
                    checkPermission();
                } else {
                    // permission denied
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
                    //mostrarMensajePermisos(Const.RESP_PERMISOS_STORAGE);
                }
                return;
            }
            case Const.RESP_PERMISOS_PHONE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
                    checkPermission();
                } else {
                    // permission denied
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
                    //mostrarMensajePermisos(Const.RESP_PERMISOS_PHONE);
                }
                return;
            }
            case Const.RESP_PERMISOS_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
                    checkPermission();
                } else {
                    // permission denied
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
                    //mostrarMensajePermisos(Const.RESP_PERMISOS_LOCATION);
                }
                return;
            }
            case Const.RESP_PERMISOS_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
                    checkPermission();
                } else {
                    // permission denied
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
                    //mostrarMensajePermisos(Const.RESP_PERMISOS_CAMERA);
                }
                return;
            }
            case Const.RESP_PERMISOS_CALL_PHONE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_GRANTED" + 1);
                    checkPermission();
                } else {
                    // permission denied
                    Log.e(TAG, "onRequestPermissionsResult-> PERMISSION_DENIED" + 2);
                    //mostrarMensajePermisos(Const.RESP_PERMISOS_CALL_PHONE);
                }
                return;
            }
        }
    }

    public void mostrarMensajePermisos(final int resp_permiso) {
        final Dialog dialog = new Dialog(FormLoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_generico);
        ((TextView) dialog.findViewById(R.id.txtMensaje)).setText("Negar los permisos impedir? el correcto funcionamiento de la aplicaci?n");
        ((Button) dialog.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                if (resp_permiso == Const.RESP_PERMISOS_STORAGE) {
                    ActivityCompat.requestPermissions(FormLoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, Const.RESP_PERMISOS_STORAGE);
                } else if (resp_permiso == Const.RESP_PERMISOS_PHONE) {
                    ActivityCompat.requestPermissions(FormLoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, Const.RESP_PERMISOS_PHONE);
                } else if (resp_permiso == Const.RESP_PERMISOS_LOCATION) {
                    ActivityCompat.requestPermissions(FormLoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.RESP_PERMISOS_LOCATION);
                } else if (resp_permiso == Const.RESP_PERMISOS_CAMERA) {
                    ActivityCompat.requestPermissions(FormLoginActivity.this, new String[]{Manifest.permission.CAMERA}, Const.RESP_PERMISOS_CAMERA);
                } else if (resp_permiso == Const.RESP_PERMISOS_CALL_PHONE) {
                    ActivityCompat.requestPermissions(FormLoginActivity.this, new String[]{Manifest.permission.CALL_PHONE}, Const.RESP_PERMISOS_CALL_PHONE);
                }
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }


    private void checkPermissionInstallPackages() {
        Log.e(TAG, "REQUEST_INSTALL_PACKAGES-> ");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.REQUEST_INSTALL_PACKAGES) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.REQUEST_INSTALL_PACKAGES)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.REQUEST_INSTALL_PACKAGES}, 1);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.REQUEST_INSTALL_PACKAGES}, 1);
            }
        }
    }

    public void actualizarInfo() {

        if (!IsOnline()) {
            AlertDialog alertDialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(FormLoginActivity.this);
            builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                    MostrarFormPrincipal();
                }
            });
            alertDialog = builder.create();
            alertDialog.setMessage(getString(R.string.msg_without_connection_1));
            alertDialog.show();
        } else {
            if (DataBaseBO.HayInformacionXEnviar1()) {
                //Util.MostrarAlertDialog(this, "Hay informacion pendiente por Enviar. Antes de Iniciar Dia, por favor Envie Informacion");
                progressDialog = ProgressDialog.show(FormLoginActivity.this, "", "Enviando Informacion...", true);
                progressDialog.show();
                Sync sync = new Sync(FormLoginActivity.this, Const.ENVIAR_PEDIDO);
                sync.start();
            } else {
                Config config = ConfigBO.ObtenerConfigUsuario();
                if (config == null) {
                    AlertDialog alertDialog;
                    AlertDialog.Builder builder = new AlertDialog.Builder(FormLoginActivity.this);
                    builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                            MostrarDialogConfiguracion();
                        }
                    });
                    alertDialog = builder.create();
                    alertDialog.setMessage("Para descargar informaci?n debe primero establecer la configuracion del usuario");
                    alertDialog.show();
                } else {
                    MostrarDialogIniciarDia();
                }
            }
        }

    }

    public void MostrarDialogConfiguracion() {

        if (dialogConfiguracion == null) {
            dialogConfiguracion = new Dialog(this);
            dialogConfiguracion.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogConfiguracion.setContentView(R.layout.dialog_configuracion);
            // dialogConfiguracion.setTitle("Configuracion");
        }
        ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).requestFocus();
        Config config = ConfigBO.ObtenerConfigUsuario();
        if (config == null) {
            ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).setText("");
            ((EditText) dialogConfiguracion.findViewById(R.id.txtBodegaConfig)).setText("");
        } else {
            ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).setText(config.usuario);
            ((EditText) dialogConfiguracion.findViewById(R.id.txtBodegaConfig)).setText(config.bodega);
        }
        ((Button) dialogConfiguracion.findViewById(R.id.btnGuardarConfig)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String usuario = ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).getText().toString().trim();
                String bodega = ((EditText) dialogConfiguracion.findViewById(R.id.txtBodegaConfig)).getText().toString().trim();
                if (usuario.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(dialogConfiguracion.getContext());
                    builder.setMessage("Por favor ingrese el usuario").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).requestFocus();
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    boolean guardo = ConfigBO.GuardarConfigUsuario(usuario, bodega, 1);
                    if (guardo) {
                        Config config = new Config();
                        config.usuario = usuario;
                        config.bodega = bodega;
                        dialogConfiguracion.cancel();
                        MostrarDialogIniciarDia();
                        Log.i("FormMenuEstadistica", "Guardo la Configuracion");
                    } else {
                        Log.e("FormMenuEstadistica", DataBaseBO.mensaje);
                    }
                }
            }
        });
        ((Button) dialogConfiguracion.findViewById(R.id.btnCancelarConfig)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                dialogConfiguracion.cancel();
            }
        });
        dialogConfiguracion.setCancelable(false);
        dialogConfiguracion.show();
        dialogConfiguracion.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_settings);
    }

    public void MostrarDialogIniciarDia() {

        runOnUiThread(new Runnable() {
            public void run() {
                progressDialog = ProgressDialog.show(FormLoginActivity.this, "", "Descargando Informacion...", true);
                progressDialog.show();
                Sync sync = new Sync(FormLoginActivity.this, Const.DOWNLOAD_DATA_BASE);
                sync.version = ObtenerVersion();
                sync.imei = ObtenerImei();
                sync.start();
            }
        });



		/*if (dialogIniciarDia == null) {
			dialogIniciarDia = new Dialog(this);
			dialogIniciarDia.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogIniciarDia.setContentView(R.layout.dialog_iniciar_dia);
		}
		String mensaje = "<b>Usuario:</b> " + config.usuario + "<br /><br />";
		mensaje += "<b>Bodega:</b> " + config.bodega + "<br /><br />";
		mensaje += "Esta seguro de Iniciar Dia?<br /><br />";
		mensaje += "Iniciar Dia actualizara la informacion. Toda la informacion anterior se borrara.";
		((TextView) dialogIniciarDia.findViewById(R.id.lblMsgIniciarDia)).setText(Html.fromHtml(mensaje));
		((Button) dialogIniciarDia.findViewById(R.id.btnAceptarIniciarDia)).setOnClickListener(new View.OnClickListener() {
			public void onClick (View v) {

				dialogIniciarDia.cancel();
				progressDialog = ProgressDialog.show(FormLoginActivity.this, "", "Descargando Informacion...", true);
				progressDialog.show();
				Sync sync = new Sync(FormLoginActivity.this, Const.DOWNLOAD_DATA_BASE);
				sync.version = ObtenerVersion();
				sync.imei = ObtenerImei();
				sync.start();
			}
		});
		((Button) dialogIniciarDia.findViewById(R.id.btnCancelarIniciarDia)).setOnClickListener(new View.OnClickListener() {
			public void onClick (View v) {

				dialogIniciarDia.cancel();
			}
		});
		dialogIniciarDia.setCancelable(false);
		dialogIniciarDia.show();*/
    }

    public String ObtenerVersion() {

        String versionApp;
        try {
            versionApp = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            versionApp = "0.0";
            Log.e("FormMenuEstadistica", "ObtenerVersion: " + e.getMessage(), e);
        }
        return versionApp;
    }

    public String ObtenerImei() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "NoPermissionToRead";
        }
        Main.deviceId = manager.getDeviceId();
        if (Main.deviceId == null) {
            WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null) {
                    String mac = wifiInfo.getMacAddress();
                    if (mac != null) {
                        Main.deviceId = mac.replace(":", "").toUpperCase();
                    }
                }
            }
        }
        return Main.deviceId;
    }

    public void RespuestaDownloadVersionApp(final boolean ok, final String respuestaServer, String msg) {
        if (progressDialog != null)
            progressDialog.cancel();
        this.runOnUiThread(new Runnable() {
            public void run() {
                if (ok) {
                    File fileApp = new File(Util.DirApp(), Const.fileNameApk);
                    if (fileApp.exists()) {
                        Uri uri = Uri.fromFile(fileApp);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, "application/vnd.android.package-archive");
                        startActivityForResult(intent, Const.RESP_ACTUALIZAR_VERSION);
                    } else {
                        Util.MostrarAlertDialog(FormLoginActivity.this, "No se pudo actualizar la version.");
                    }
                } else {
                    Util.MostrarAlertDialog(FormLoginActivity.this, respuestaServer);
                }
            }
        });
    }

    public void ActualizarVersionApp() {

        final String versionSvr = DataBaseBO.ObtenerVersionApp();
        String versionApp = ObtenerVersion();
        if (versionSvr != null && versionApp != null) {
            float versionServer = Util.ToFloat(versionSvr.replace(".", ""));
            float versionLocal = Util.ToFloat(versionApp.replace(".", ""));
            if (versionLocal < versionServer) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormLoginActivity.this);
                builder.setMessage("Hay una version de la aplicacion: " + versionSvr).setCancelable(false).setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        progressDialog = ProgressDialog.show(FormLoginActivity.this, "", "Descargando Version " + versionSvr + "...", true);
                        progressDialog.show();
                        Sync sync = new Sync(FormLoginActivity.this, Const.DOWNLOAD_VERSION_APP);
                        sync.start();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                // mostrarAlertDialog(this, "Informacion Descargada
                // Correctamente");
                //Util.MostrarAlertDialog(this, "Informacion Descargada Correctamente");
                MostrarFormPrincipal();
            }
        }
    }

    public void RespuestaEnviarInfo(final boolean ok, String respuestaServer, String msg) {
        final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : "No se pudo registrar informacion.\nMotivo: Fallo de conexion.";
        if (progressDialog != null) progressDialog.cancel();
        this.runOnUiThread(new Runnable() {

            public void run() {

                if (ok) {
                    MostrarDialogIniciarDia();
                } else {
                    //Util.MostrarAlertDialog(FormLoginActivity.this, mensaje);
                    MostrarFormPrincipal();
                }


                /*AlertDialog.Builder builder = new AlertDialog.Builder(FormLoginActivity.this);
                builder.setMessage(mensaje).setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //actualizarInfo();
                        //CargarInformacionCliente(cliente);
                        MostrarDialogIniciarDia();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();*/
            }
        });
    }

    public void RespuestaDownloadInfo(final boolean ok, final String respuestaServer, String msg) {
        final String mensaje = ok ? "Informacion Actualizada correctamente" : msg;
        if (progressDialog != null)
            progressDialog.cancel();
        this.runOnUiThread(new Runnable() {
            public void run() {
                if (DataBaseBO.CargarInfomacionUsuario()) {
                    if (ok) {
                        ActualizarVersionApp();
                    } else {
                        //Util.MostrarAlertDialog(FormLoginActivity.this, mensaje);
                        MostrarFormPrincipal();
                    }
                } else {
                    if (ok)
                        Util.MostrarAlertDialog(FormLoginActivity.this, "No se pudo Iniciar dia, por favor intente nuevamente.");
                    else
                        Util.MostrarAlertDialog(FormLoginActivity.this, "No se pudo Iniciar dia, por favor intente nuevamente.\n\nMotivo: " + respuestaServer);
                }
            }
        });
    }

    public void MostrarDialogActualizarInventario(Config config) {

        if (dialogActualizarInventario == null) {
            dialogActualizarInventario = new Dialog(this);
            dialogActualizarInventario.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialogActualizarInventario.setContentView(R.layout.dialog_iniciar_dia);
            dialogActualizarInventario.setTitle("Actualizar Inventario");
        }
        String mensaje = "<b>Usuario:</b> " + config.usuario + "<br />";
        mensaje += "<b>Bodega:</b> " + config.bodega + "<br /><br />";
        mensaje += "Esta seguro de Actualizar el Inventario?<br />";
        ((TextView) dialogActualizarInventario.findViewById(R.id.lblMsgIniciarDia)).setText(Html.fromHtml(mensaje));
        ((Button) dialogActualizarInventario.findViewById(R.id.btnAceptarIniciarDia)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                dialogActualizarInventario.cancel();
                progressDialog = ProgressDialog.show(FormLoginActivity.this, "", "Descargando Inventario...", true);
                progressDialog.show();
                Sync sync = new Sync(FormLoginActivity.this, Const.DOWNLOAD_DATA_BASE);
                sync.version = ObtenerVersion();
                sync.imei = ObtenerImei();
                sync.start();
            }
        });
        ((Button) dialogActualizarInventario.findViewById(R.id.btnCancelarIniciarDia)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                dialogActualizarInventario.cancel();
            }
        });
        dialogActualizarInventario.setCancelable(false);
        dialogActualizarInventario.show();
        dialogActualizarInventario.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_inventario);
    }
}