package celuweb.com.uyusa;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import celuweb.com.DataObject.HistorialRecaudoCliente;

public class HistorialRecaudoAdapter extends RecyclerView.Adapter<HistorialRecaudoAdapter.ViewHolder> {

    ArrayList<HistorialRecaudoCliente> list;
    Context context;

    public HistorialRecaudoAdapter(ArrayList<HistorialRecaudoCliente> list){
        this.list = list;
    }

    @Override
    public HistorialRecaudoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_historial_recaudo_cliente, parent, false);
        ViewHolder holder = new HistorialRecaudoAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistorialRecaudoCliente item = list.get(position);
        holder.doc.setText(item.documento);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView doc;

        public ViewHolder(View view){
            super(view);
            doc = view.findViewById(R.id.documentoHistorialRecaudo);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, FormCarteraInfoHistorialRecaudo.class);
            intent.putExtra("DOC_RECAUDO", list.get(getAdapterPosition()).documento);
            context.startActivity(intent);
        }
    }
}
