package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.ItemListView;

public class FormCarteraRecaudo extends Activity {
	
	Cartera carteraSel;
	Dialog dialogEditar;
	static TextView lblOtrosDescuentos;
	//public static float totalDescuento = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.from_cartera_recuado);
		
		CargarListaRecaudo();
		SetListenerListView();
		
		//Intent i = getParent().getIntent();
	    //float totalDescuentos = i.getFloatExtra("totalDescuentos", 0);
	    
		lblOtrosDescuentos = (TextView)findViewById(R.id.lblOtrosDescuentos);
	}
	
	public static void SetTotalDescuento(float descuento) {
		
		lblOtrosDescuentos.setText("Otros Descuentos: " + Util.SepararMiles("" + descuento));
		Main.total_descuento = descuento;
	}
	
	public void OnClickAgregarCarteraRecaudo(View view) {
		
		Intent formInfoCartera = new Intent(this, FormInfoCarteraActivity.class);
		startActivityForResult(formInfoCartera, Const.RESP_FROM_AGREGAR_CARTERA);
	}
	
	public void OnClickContinuarCartera(View view) {
		
		if (Main.cartera.size() > 0) {
			
			if (Main.total_descuento > Main.total_recaudo) {
				
				Util.MostrarAlertDialog(this, "Los descuentos son superiores al recaudo." + "\n\n" + "Agregue mas facturas o elimine descuentos");
				
			} else {
				
				Intent formFormasPago = new Intent(this, FormFormasPago.class);
				startActivityForResult(formFormasPago, Const.RESP_RECAUDO_EXITOSO);
			}
			
		} else {
			
			Util.MostrarAlertDialog(this, "Debe ingresar primero las facturas");
		}
	}
	
	public void OnClickCancelarCartera(View view) {
		
		if (Main.cartera.size() > 0 ) {
			
			AlertDialog alertDialog;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					Main.total_recaudo = 0;
					Main.total_descuento = 0;
					Main.total_forma_pago = 0;
					
					Main.cartera.clear();
					Main.listaDescuentos.clear();
					Main.listaFormaPago.clear();
					
					dialog.cancel();
					FormCarteraRecaudo.this.finish();
				}
			}).setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});

			alertDialog = builder.create();
	    	alertDialog.setMessage("Esta Seguro de Cancelar el Recaudo para este cliente?");
	    	alertDialog.show();
	    	
		} else { 
			
			this.finish();
		}
	}
	
	public void CargarListaRecaudo() {
		
		ItemListView[] listaItems;
		ItemListView itemListView;
		Vector<ItemListView> items = new Vector<ItemListView>();
		float total_recaudo = 0;
		
		for (Cartera cartera : Main.cartera) {
			
			total_recaudo += cartera.valorARecaudar;
			
			itemListView = new ItemListView();
			itemListView.referencia = cartera.referencia;
			itemListView.titulo = "Doc: " + Util.ToLong(cartera.referencia) + "  Saldo: " + Util.SepararMiles("" + cartera.saldo);
			//itemListView.subTitulo = "Dias: " + cartera.dias + ". Vecimiento: " + cartera.FechaVecto;
			itemListView.subTitulo = "Valor Recaudo: "  + Util.SepararMiles("" + cartera.valorARecaudar); //+ " Dias: " + cartera.dias;
			items.addElement(itemListView);
		}
		
		Main.total_recaudo = total_recaudo;
		
		//if (total > 0) {
						
			TextView lblValorRecaudado = (TextView)findViewById(R.id.lblValorRecaudado);
			lblValorRecaudado.setText("Total: " + Util.SepararMiles("" + (Main.total_recaudo - Main.total_descuento)));
			
		//} else {
			
			//TextView lblValorRecaudado = (TextView)findViewById(R.id.lblValorRecaudado);
			//lblValorRecaudado.setText("Total: $0");
		//}
		
		if (items.size() > 0) {
			
			listaItems = new ItemListView[items.size()];
			items.copyInto(listaItems);
			
			ListAdapter adapter = new ListAdapter(this, listaItems, 0);
			ListView listaPedido = (ListView)findViewById(R.id.listaCarteraRecaudo);
			listaPedido.setAdapter(adapter);
			
		} else {
			
			ListAdapter adapter = new ListAdapter(this, new ItemListView[]{}, 0);
			ListView listaPedido = (ListView)findViewById(R.id.listaCarteraRecaudo);
			listaPedido.setAdapter(adapter);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == Const.RESP_FROM_AGREGAR_CARTERA && resultCode == RESULT_OK) {
			
			CargarListaRecaudo();
		}
		
		if (requestCode == Const.RESP_RECAUDO_EXITOSO && resultCode == RESULT_OK) {
			
			TabActivity ta = (TabActivity) this.getParent();
		    ta.setResult(RESULT_OK);
			ta.finish();
		}
	}
	
	public void MostrarDialogEdicion() {
		
		if (dialogEditar == null) {
				
			dialogEditar = new Dialog(this);
			dialogEditar.setContentView(R.layout.dialog_edit_cartera);
			dialogEditar.setTitle("Opciones Edicion");
			
			/*((RadioButton)dialogEditar.findViewById(R.id.radioEditar)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					if (producto == null)
						producto = new Producto();

					producto.codigo      = detalleEdicion.codigo_producto;
					producto.descripcion = detalleEdicion.desc_producto;
					producto.precio      = detalleEdicion.precio;
					producto.iva         = detalleEdicion.iva;
					
					((EditText)findViewById(R.id.txtCodigoProducto)).setText(producto.codigo);
					
					dialogEditar.cancel();
					MostrarDialogPedido(producto.codigo);
				}
			});*/
			
			((RadioButton)dialogEditar.findViewById(R.id.radioEliminar)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					if (carteraSel != null) {
						
						AlertDialog.Builder builder = new AlertDialog.Builder(FormCarteraRecaudo.this);
						builder.setMessage("Esta Seguro de eliminar el recaudo " + carteraSel.referencia)
						.setCancelable(false)
						.setPositiveButton("Si", new DialogInterface.OnClickListener() {
							
							public void onClick(DialogInterface dialog, int id) {
								
								Main.cartera.remove(carteraSel);
								CargarListaRecaudo();
								dialogEditar.cancel();
							}
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							public void onClick(DialogInterface dialog, int id) {
								
								dialogEditar.cancel();
							}
						});
						
						AlertDialog alert = builder.create();
						alert.show();
						
					} else {
						
						Util.MostrarAlertDialog(FormCarteraRecaudo.this, "Error obteniendo la informacion de la Cartera");
					}
				}
			});
			
			((RadioButton)dialogEditar.findViewById(R.id.radioCancelar)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					dialogEditar.cancel();
				}
			});
			
		} else {
			
			((RadioButton)dialogEditar.findViewById(R.id.radioEliminar)).requestFocus();		
		}
		
		dialogEditar.show();
	}
	
	public void SetListenerListView() {
		
		ListView listView = (ListView)findViewById(R.id.listaCarteraRecaudo);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               
				ListAdapter adapter = (ListAdapter)parent.getAdapter();
            	ItemListView itemListView = adapter.listItems[position];
            	String referencia = itemListView.referencia;
            	
            	int i = 0;
            	carteraSel = null;
            	
            	for (Cartera cartera : Main.cartera) {
            		
            		if (cartera.referencia.equals(referencia)) {
            			
            			carteraSel = Main.cartera.elementAt(i);
            			break;
            		}
            		
            		i++;
            	}
            	
            	MostrarDialogEdicion();
            }
        });
	}
}
