/**
 *
 */
package celuweb.com.uyusa;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.DirectionsJSONParser;
import celuweb.com.DataObject.Encabezado;

public class MapsActivityClientesPotenciales extends FragmentActivity
  implements OnMapReadyCallback, OnMarkerClickListener, LocationListener {
  public static final String TAG = MapsActivityClientesPotenciales.class.getName();
  Vector<Cliente> listaClientes;
  Cliente clienteSel;
  Dialog dialogAbastecimiento;
  long mLastClickTime = 0;
  PolylineOptions rectOptions;
  Polyline polyline;
  Vector<Encabezado> listaPedidos;
  private GoogleMap mMap;
  String listaclientesstr;
  
  private LatLng posicionActual;
  private LatLng posicionCliente;
  private float zoomCamara;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps_clientes_potenciales);
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    Bundle bundle = getIntent().getExtras();
    if (bundle != null) {
      Log.e(TAG, " bundle ->  ");
      try {
        if (bundle.containsKey("clienteSel")) {
          clienteSel = (Cliente) bundle.getSerializable("clienteSel");
          Log.e(TAG, " clienteSel ->  " + clienteSel.Nombre);
        }
        if (bundle.containsKey("listaClientesPotenciales")) {
          listaclientesstr = bundle.getString("listaClientesPotenciales");
          Log.e(TAG, " listaclientesstr ->  " + listaclientesstr);
        }
      } catch (Exception e) {
        // TODO: handle exception
      }
    }
    if (listaclientesstr.equals("")) {
      String keysre[] = {"LISTACLIENTES"};
      String datosre[] = Util.getPrefence(this, Const.PREFERENCELISTA, keysre);
      Log.e(TAG, " datosre[] ->  " + datosre.length);
      if (datosre.length > 0) {
        if (!datosre[0].equals("")) {
          listaclientesstr = datosre[0].toString();
        }
      }
    }
    listaClientes = DataBaseBO.buscarClientesPotencialesSeleccionados(listaclientesstr);
    try {
      GPSTracker gpsTracker = new GPSTracker(this);
      Location location = gpsTracker.getLocation();
      boolean estadoGPS = gpsTracker.isEstadoGPS();
      if (estadoGPS) {
        if (location != null) {
          double latitud = location.getLatitude();
          double longitud = location.getLongitude();
          posicionActual = new LatLng(latitud, longitud);
        }
      } else {
        Util.mostrarAlertDialog(this, "Por favor encienda el GPS.");
      }
  
  
      if (clienteSel!=null){
        if (clienteSel.latitud!=0 && clienteSel.longitud!=0){
          posicionCliente = new LatLng(clienteSel.latitud, clienteSel.longitud);
        }
      }
      
        
    } catch (Exception e) {
      Log.e(TAG, "Captura Cooredenada -> " + e.getMessage());
    }
    
  }
  
  private Bitmap createStoreMarker(String orden) {
    View markerLayout = getLayoutInflater().inflate(R.layout.store_marker_layout, null);
    ImageView markerImage = (ImageView) markerLayout.findViewById(R.id.marker_image);
    TextView markerRating = (TextView) markerLayout.findViewById(R.id.marker_text);
    markerImage.setImageResource(R.drawable.marker);
    markerRating.setText(orden);
    markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
    markerLayout.layout(0, 0, markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight());
    final Bitmap bitmap = Bitmap.createBitmap(markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    markerLayout.draw(canvas);
    return bitmap;
  }
  
  @Override
  public void onMapReady(GoogleMap map) {
    zoomCamara = 17f;
    mMap = map;
    mMap.setMyLocationEnabled(true);
    mMap.setOnMarkerClickListener(this);
    //mMap.moveCamera(CameraUpdateFactory.newLatLng(posicionActual));
    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(posicionActual, zoomCamara));
    LatLng latlang = null;
    if (listaClientes != null && !listaClientes.isEmpty()) {
      for (Cliente cliente : listaClientes) {
        if (cliente.latitud != 0 && cliente.longitud != 0) {
          String titulo = cliente.Nombre +" - "+cliente.razonSocial + "\n" + cliente.diff + " metros";
          latlang = new LatLng(cliente.latitud, cliente.longitud);
          mMap.addMarker(new MarkerOptions()
                           .position(latlang)
                           .title("")
                           .snippet("")
                           .icon(BitmapDescriptorFactory.fromBitmap(createStoreMarker(titulo))));
        }
      }
  
  
      // Getting URL to the Google Directions API
      String url = getDirectionsUrl(posicionActual, posicionCliente);
  
      DownloadTask downloadTask = new DownloadTask();
  
      // Start downloading json data from Google Directions API
      downloadTask.execute(url);
      
      /*Polyline line = mMap.addPolyline(new PolylineOptions()
                                        .add(posicionActual,
                                             posicionCliente)
                                        .width(5)
                                        .color(Color.RED));*/
      
  
      /*map.addPolyline(new PolylineOptions().geodesic(true)
                        .add(new LatLng(-31.534113, -68.526428))
                        .add(new LatLng(-31.537332, -68.526085))
                        .add(new LatLng(-31.538064, -68.538359))
  
  
      private final static String LINE = "f_n_EdagaLbScApCtkA";
      List decodedPath = PolyUtil.decode(LINE);
      getMap().addPolyline(new PolylineOptions().addAll(decodedPath));*/
      
      
			/*rectOptions = new PolylineOptions();
			for (Marker mar : listaMarcadores) {
				rectOptions.add(mar.getPosition()).color(Color.parseColor("#009FDF"));
			}
			polyline = map.addPolyline(rectOptions);*/
      CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(listaClientes.elementAt(0).latitud, listaClientes.elementAt(0).longitud)).zoom(17.5f).bearing(300).tilt(50).build();
      changeCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
      
    }
    
  }
  
  @Override
  public boolean onMarkerClick(Marker marker) {
    Util.mostrarToast(this,"Click marker");
    return true;
  }
  
  public void onZoomIn(View view) {
    changeCamera(CameraUpdateFactory.zoomIn());
  }
  
  public void onZoomOut(View view) {
    changeCamera(CameraUpdateFactory.zoomOut());
  }
  
  private void changeCamera(CameraUpdate update) {
    changeCamera(update, null);
  }
  
  private void changeCamera(CameraUpdate update, CancelableCallback callback) {
    mMap.animateCamera(update, callback);
    
  }
  
  @Override
  public void onLocationChanged(Location location) {
  
    // Getting URL to the Google Directions API
    String url = getDirectionsUrl(posicionActual, posicionCliente);
  
    DownloadTask downloadTask = new DownloadTask();
  
    // Start downloading json data from Google Directions API
    downloadTask.execute(url);
    
  }
  
  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {
  }
  
  @Override
  public void onProviderEnabled(String provider) {
  }
  
  @Override
  public void onProviderDisabled(String provider) {
  }
  
  
  
  
  
  
  private String getDirectionsUrl(LatLng origin,LatLng dest){
    
    
    // Origin of route
    String str_origin = "origin="+origin.latitude+","+origin.longitude;
    
    // Destination of route
    String str_dest = "destination="+dest.latitude+","+dest.longitude;
    
    // Sensor enabled
    String sensor = "sensor=false";
    
    // Building the parameters to the web service
    String parameters = str_origin+"&"+str_dest+"&"+sensor;
    
    // Output format
    String output = "json";
    
    // Building the url to the web service
    String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
    
    return url;
  }
  
  
  
  /** A method to download json data from url */
  private String downloadUrl(String strUrl) throws IOException{
    String data = "";
    InputStream iStream = null;
    HttpURLConnection urlConnection = null;
    try{
      URL url = new URL(strUrl);
      
      // Creating an http connection to communicate with url
      urlConnection = (HttpURLConnection) url.openConnection();
      
      // Connecting to url
      urlConnection.connect();
      
      // Reading data from url
      iStream = urlConnection.getInputStream();
      
      BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
      
      StringBuffer sb = new StringBuffer();
      
      String line = "";
      while( ( line = br.readLine()) != null){
        sb.append(line);
      }
      
      data = sb.toString();
      
      br.close();
      
    }catch(Exception e){
      Log.d("Exception while downloading url", e.toString());
    }finally{
      iStream.close();
      urlConnection.disconnect();
    }
    return data;
  }
  
  // Fetches data from url passed
  private class DownloadTask extends AsyncTask<String, Void, String>{
    
    // Downloading data in non-ui thread
    @Override
    protected String doInBackground(String... url) {
      
      // For storing data from web service
      String data = "";
      
      try{
        // Fetching the data from web service
        data = downloadUrl(url[0]);
      }catch(Exception e){
        Log.d("Background Task",e.toString());
      }
      return data;
    }
    
    // Executes in UI thread, after the execution of
    // doInBackground()
    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      
      ParserTask parserTask = new ParserTask();
      
      // Invokes the thread for parsing the JSON data
      parserTask.execute(result);
    }
  }
  
  /** A class to parse the Google Places in JSON format */
  private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
    
    // Parsing the data in non-ui thread
    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
      
      JSONObject jObject;
      List<List<HashMap<String, String>>> routes = null;
      
      try{
        jObject = new JSONObject(jsonData[0]);
        DirectionsJSONParser parser = new DirectionsJSONParser();
        
        // Starts parsing data
        routes = parser.parse(jObject);
      }catch(Exception e){
        e.printStackTrace();
      }
      return routes;
    }
    
    // Executes in UI thread, after the parsing process
    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
      ArrayList<LatLng> points = null;
      PolylineOptions lineOptions = null;
      MarkerOptions markerOptions = new MarkerOptions();
      
      // Traversing through all the routes
      for(int i=0;i<result.size();i++){
        points = new ArrayList<LatLng>();
        lineOptions = new PolylineOptions();
        
        // Fetching i-th route
        List<HashMap<String, String>> path = result.get(i);
        
        // Fetching all the points in i-th route
        for(int j=0;j<path.size();j++){
          HashMap<String,String> point = path.get(j);
          
          double lat = Double.parseDouble(point.get("lat"));
          double lng = Double.parseDouble(point.get("lng"));
          LatLng position = new LatLng(lat, lng);
          
          points.add(position);
        }
        
        // Adding all the points in the route to LineOptions
        lineOptions.addAll(points);
        lineOptions.width(10);
        lineOptions.color(Color.BLUE);
      }
      
      // Drawing polyline in the Google Map for the i-th route
      mMap.addPolyline(lineOptions);
    }
  }
}