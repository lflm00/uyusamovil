package celuweb.com.uyusa;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import celuweb.com.DataObject.ItemListView;

public class ListAdapterUltimo extends ArrayAdapter<ItemListView> {
	
	Activity context;
	ItemListView[] listItems;
	
	ListAdapterUltimo(Activity context, ItemListView[] listItems) {
		
		super(context, R.layout.item, listItems);
		this.listItems = listItems; 
		this.context = context;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = context.getLayoutInflater();
		View item = inflater.inflate(R.layout.item, null);
		
		TextView lblTitulo = (TextView)item.findViewById(R.id.lblTitulo);
		lblTitulo.setText(listItems[position].titulo);
		
		TextView lblSubtitulo = (TextView)item.findViewById(R.id.lblSubTitulo);
		lblSubtitulo.setText(listItems[position].subTitulo);
		
		if (listItems[position].colorTitulo != 0) {
			
			lblTitulo.setTextColor(listItems[position].colorTitulo);
			lblSubtitulo.setTextColor(listItems[position].colorTitulo);
		}
		
		return(item);
    }
	
	@Override
	public ItemListView getItem(int position) {
		
		return listItems[position];
	}
}
