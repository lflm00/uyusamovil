package celuweb.com.uyusa;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class FormCarteraTabs extends CustomTabActivity {

	TabHost tabHost;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_cartera_tabs);
		
		
		
		cargarTabs();
	}
	
	public void cargarTabs() {
		
		Intent intent;
		tabHost = getTabHost();
		tabHost.getTabWidget().setDividerDrawable(R.drawable.tab_divider);
		
		intent = new Intent().setClass(this, FormCarteraFacturas.class);
		SetupTab(new TextView(this), "  Facturas  ", intent);
		
		intent = new Intent().setClass(this, FormCarteraDescuentos.class);
		//SetupTab(new TextView(this), "  Descuentos  ", intent);
		
		tabHost.setCurrentTab(0);
	}
	
	private void SetupTab(final View view, final String tag, Intent intent) {
		
		View tabView = CreateTabView(tabHost.getContext(), tag);
		TabSpec setContent = tabHost.newTabSpec(tag).setIndicator(tabView).setContent(intent);
		tabHost.addTab(setContent);
	}
	
	private static View CreateTabView(final Context context, final String text) {

		View view = LayoutInflater.from(context).inflate(R.layout.tab_bg, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		return view;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == Const.RESP_RECAUDO_EXITOSO && resultCode == RESULT_OK) {
			
			setResult(RESULT_OK);
			finish();
		}
	}

    
	
}
