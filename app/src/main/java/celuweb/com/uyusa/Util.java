package celuweb.com.uyusa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.typeface.IIcon;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.RegistroRango;
import celuweb.com.DataObject.Usuario;
import celuweb.com.DataObject.objeto;

public class Util extends AppCompatActivity {

    private static Typeface typeface;
    private static final String TAG = Util.class.getName();

    public static String mensaje = "";

    public static int ToInt(String value) {
        try {
            return Integer.parseInt(value);

        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static double ToDouble(String value) {

        try {

            return Double.parseDouble(value);

        } catch (NumberFormatException e) {

            return 0;
        }
    }

    public static long ToLong(String value) {
        try {
            return Long.parseLong(value);

        } catch (NumberFormatException e) {
            return 0L;
        }
    }

    public static float ToFloat(String value) {
        try {
            return Float.parseFloat(value);

        } catch (NumberFormatException e) {
            return 0F;
        }
    }

    @SuppressLint("NewApi")
    public static String round(String numero, int decimales) {
        String formato = "";
        /* dar un formato numerico valido al numero */
        numero = numero.replace(",", ".");
        for (int i = 1; i <= decimales; i++) {
            formato += "0";
        }
        if (formato.length() > 0) {
            formato = "#0." + formato;
        } else {
            formato = "##.00";
        }
        DecimalFormat df = new DecimalFormat(formato, DecimalFormatSymbols.getInstance(Locale.US));
        BigDecimal bd = new BigDecimal(numero);
        bd = bd.setScale(decimales, BigDecimal.ROUND_HALF_UP);
        return df.format(bd.doubleValue());
    }

    public static String Redondear(String numero, int cantDec) {
        int tamNumero = 0;
        double numRedondear;
        int cantAfterPunto;
        if (numero.indexOf(".") == -1) {
            return numero;
        }
        tamNumero = numero.length();
        cantAfterPunto = tamNumero - (numero.indexOf(".") + 1);
        if (cantAfterPunto <= cantDec) return numero;
        String numeroSumar = "0.";
        for (int i = 0; i < cantDec; i++) {
            numeroSumar = numeroSumar.concat("0");
        }
        numeroSumar = numeroSumar.concat("5");
        numRedondear = Double.parseDouble(numero);
        numRedondear = numRedondear + Double.parseDouble(numeroSumar);
        numero = String.valueOf(numRedondear);
        tamNumero = numero.length();
        cantAfterPunto = tamNumero - (numero.indexOf(".") + 1);
        if (cantAfterPunto <= cantDec) return numero;
        else {
            if (cantDec == 0) numero = numero.substring(0, numero.indexOf("."));
            else numero = numero.substring(0, (numero.indexOf(".") + 1 + cantDec));
            return numero;
        }
    }

    public static String RedondearFit(String numero, int cantDec) {
        int tamNumero = 0;
        double numRedondear;
        int cantAfterPunto;
        if (numero.indexOf(".") == -1) {
            return numero;
        }
        tamNumero = numero.length();
        cantAfterPunto = tamNumero - (numero.indexOf(".") + 1);
        if (cantAfterPunto <= cantDec) {
            int falta = cantDec - cantAfterPunto;
            if (falta > 0) {
                for (int i = 0; i < falta; i++)
                    numero += "0";
            }
            return numero;
        }
        String numeroSumar = "0.";
        for (int i = 0; i < cantDec; i++) {
            numeroSumar = numeroSumar.concat("0");
        }
        numeroSumar = numeroSumar.concat("5");
        numRedondear = Double.parseDouble(numero);
        numRedondear = numRedondear + Double.parseDouble(numeroSumar);
        numero = String.valueOf(numRedondear);
        tamNumero = numero.length();
        cantAfterPunto = tamNumero - (numero.indexOf(".") + 1);
        if (cantAfterPunto <= cantDec) {
            int falta = cantDec - cantAfterPunto;
            if (falta > 0) {
                for (int i = 0; i < falta; i++)
                    numero += "0";
            }
            return numero;

        } else {
            if (cantDec == 0) numero = numero.substring(0, numero.indexOf("."));
            else numero = numero.substring(0, (numero.indexOf(".") + 1 + cantDec));
            return numero;
        }
    }

    public static String SepararMiles(String numero) {
        String cantidad;
        String cantidadAux1;
        String cantidadAux2;
        boolean tieneMenos;
        int posPunto;
        int i;
        cantidad = "";
        cantidadAux1 = "";
        cantidadAux2 = "";
        tieneMenos = false;
        if (numero.indexOf("-") != -1) {
            String aux;
            tieneMenos = true;
            aux = numero.substring(0, numero.indexOf("-"));
            aux = aux + numero.substring(numero.indexOf("-") + 1, numero.length());
            numero = aux;
        }
        if (numero.indexOf(".") == -1) {
            if (numero.length() > 3) {
                cantidad = ColocarComas(numero, numero.length());

            } else {
                if (tieneMenos) numero = "$-" + numero;
                else numero = "$" + numero;
                return numero;
            }

        } else {
            posPunto = numero.indexOf(".");
            for (i = 0; i < posPunto; i++) {
                cantidadAux1 = cantidadAux1 + numero.charAt(i);
            }
            for (i = posPunto; i < numero.length(); i++) {
                cantidadAux2 = cantidadAux2 + numero.charAt(i);
            }
            if (cantidadAux1.length() > 3) {
                cantidad = ColocarComas(cantidadAux1, posPunto);
                cantidad = cantidad + cantidadAux2;

            } else {
                if (tieneMenos) numero = "$-" + numero;
                else numero = "$" + numero;
                return numero;
            }
        }
        if (tieneMenos) cantidad = "$-" + cantidad;
        else cantidad = "$" + cantidad;
        return cantidad;
    }

    public static String SepararMilesSin(String numero) {
        String cantidad;
        String cantidadAux1;
        String cantidadAux2;
        boolean tieneMenos;
        int posPunto;
        int i;
        cantidad = "";
        cantidadAux1 = "";
        cantidadAux2 = "";
        tieneMenos = false;
        if (numero.indexOf("-") != -1) {
            String aux;
            tieneMenos = true;
            aux = numero.substring(0, numero.indexOf("-"));
            aux = aux + numero.substring(numero.indexOf("-") + 1, numero.length());
            numero = aux;
        }
        if (numero.indexOf(".") == -1) {
            if (numero.length() > 3) {
                cantidad = ColocarComas(numero, numero.length());

            } else {
                if (tieneMenos) numero = "-" + numero;
                else numero = "" + numero;
                return numero;
            }

        } else {
            posPunto = numero.indexOf(".");
            for (i = 0; i < posPunto; i++) {
                cantidadAux1 = cantidadAux1 + numero.charAt(i);
            }
            for (i = posPunto; i < numero.length(); i++) {
                cantidadAux2 = cantidadAux2 + numero.charAt(i);
            }
            if (cantidadAux1.length() > 3) {
                cantidad = ColocarComas(cantidadAux1, posPunto);
                cantidad = cantidad + cantidadAux2;

            } else {
                if (tieneMenos) numero = "-" + numero;
                else numero = "" + numero;
                return numero;
            }
        }
        if (tieneMenos) cantidad = "-" + cantidad;
        else cantidad = "" + cantidad;
        return cantidad;
    }

    private static String ColocarComas(String numero, int pos) {
        String cantidad;
        Vector<String> cantidadAux;
        String cantidadAux1;
        int i;
        int cont;
        cantidadAux = new Vector<String>();
        cantidad = "";
        cantidadAux1 = "";
        cont = 0;
        for (i = (pos - 1); i >= 0; i--) {
            if (cont == 3) {
                cantidadAux1 = "," + cantidadAux1;
                cantidadAux.addElement(cantidadAux1);
                cantidadAux1 = "";
                cont = 0;
            }
            cantidadAux1 = numero.charAt(i) + cantidadAux1;
            cont++;
        }
        cantidad = cantidadAux1;
        for (i = cantidadAux.size() - 1; i >= 0; i--) {
            cantidad = cantidad + cantidadAux.elementAt(i).toString();
        }
        return cantidad;
    }

    public static String ObtenerHora() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int hh = calendar.get(Calendar.HOUR_OF_DAY);
        String hora = hh < 10 ? "0" + hh : "" + hh;
        int mm = calendar.get(Calendar.MINUTE);
        String minutos = mm < 10 ? "0" + mm : "" + mm;
        int ss = calendar.get(Calendar.SECOND);
        String segundos = ss < 10 ? "0" + ss : "" + ss;
        return hora + ":" + minutos + ":" + segundos;
    }

    /**
     * Llenar con carateres a la izquierda
     *
     * @param cadena
     * @param tamano
     * @param caracter
     * @return
     */
    public static String lpad(String cadena, int tamano, String caracter) {
        int i;
        int tamano1;
        tamano1 = cadena.length();
        if (tamano1 > tamano) cadena = cadena.substring(0, tamano);
        tamano1 = cadena.length();
        for (i = tamano1; i < tamano; i++)
            cadena = caracter + cadena;
        return cadena;
    }

    public static File DirApp() {
        File SDCardRoot = Environment.getExternalStorageDirectory(); // Environment.getDataDirectory(); //
        File dirApp = new File(SDCardRoot.getPath() + "/" + Const.nameDirApp);
        // File dirApp = new File(SDCardRoot + "/data/" + "co.com.SuperRicas/" +
        // Const.nameDirApp);
        if (!dirApp.isDirectory()) dirApp.mkdirs();
        return dirApp;
    }

    public static void MostrarAlertDialog(Context context, String mensaje) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_generico);
        ((TextView) dialog.findViewById(R.id.txtMensajeGeneral)).setText(mensaje);
        ((Button) dialog.findViewById(R.id.btnAceptarGenerico)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
	        
		/*AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
			}
		});

		alertDialog = builder.create();
		alertDialog.setMessage(mensaje);
		alertDialog.show();*/
    }

    public static void MostrarAlertDialog(Context context, String mensaje, int tipoIcono) {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        if (tipoIcono == 1) {
            alertDialog.setIcon(R.drawable.alert);
            alertDialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            alertDialog.setTitle(context.getString(R.string.atencion));
            alertDialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.alert);

        }
        if (tipoIcono == 2) {
            alertDialog.setIcon(R.drawable.inform);
            alertDialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            alertDialog.setTitle(context.getString(R.string.mensaje));
            alertDialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.inform);

        }
        alertDialog.show();
    }

    public static String DateToStringMM_DD_YYYY(Date date) {
        Calendar calendario;
        String fechaRetorna = "";
        calendario = Calendar.getInstance();
        calendario.setTime(date);
        if ((calendario.get(Calendar.MONTH) + 1) < 10)
            fechaRetorna += "0" + (calendario.get(Calendar.MONTH) + 1);
        else fechaRetorna += String.valueOf(calendario.get(Calendar.MONTH) + 1);
        fechaRetorna += "/";
        if (calendario.get(Calendar.DAY_OF_MONTH) < 10)
            fechaRetorna += "0" + calendario.get(Calendar.DAY_OF_MONTH);
        else fechaRetorna += String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
        fechaRetorna += "/";
        fechaRetorna += String.valueOf(calendario.get(Calendar.YEAR));
        return fechaRetorna;
    }

    public static String QuitarE(String numero) {
        int posE;
        int cantMover;
        int posAux;
        int cantCeros;
        int posPunto;
        String cantMoverString;
        String cantidad;
        String cantidadAux1, cantidadAux2;
        cantMoverString = "";
        cantidad = "";
        if (!(numero.indexOf("E") != -1)) {
            return numero;
        } else {
            posE = numero.indexOf("E");
            posE++;
            while (posE < numero.length()) {
                cantMoverString = cantMoverString + numero.charAt(posE);
                posE++;
            }
            cantMover = Integer.parseInt(cantMoverString);
            posE = numero.indexOf("E");
            posAux = 0;
            posPunto = 0;
            while (posAux < posE) {
                if (numero.charAt(posAux) != '.') {
                    cantidad = cantidad + numero.charAt(posAux);
                } else {
                    posPunto = posAux;
                }
                posAux++;
            }
            if (cantidad.length() < (cantMover + posPunto)) {
                cantCeros = cantMover - cantidad.length() + posPunto;
                for (int i = 0; i < cantCeros; i++) {
                    cantidad = cantidad + "0";
                }
            } else {
                cantidadAux1 = cantidad.substring(0, (cantMover + posPunto));
                cantidadAux2 = cantidad.substring((cantMover + posPunto), cantidad.length());
                if (!cantidadAux2.equals("")) {
                    cantidad = cantidadAux1 + "." + cantidadAux2;
                } else {
                    cantidad = cantidadAux1;
                }
            }
        }
        return cantidad;
    }

    public static String FechaActual(String format) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static String ObtenerFechaId() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
        return sdf.format(date);
    }

    public static Drawable ResizedImage(byte[] image, int newWidth, int newHeight) {
        Matrix matrix;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;
        try {
            bitmapOriginal = BitmapFactory.decodeByteArray(image, 0, image.length);
            int width = bitmapOriginal.getWidth();
            int height = bitmapOriginal.getHeight();
            if (width == newWidth && height == newHeight) {
                return new BitmapDrawable(bitmapOriginal);
            }
            // Reescala el Ancho y el Alto de la Imagen
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            // Crea la Imagen con el nuevo Tamano
            resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
            mensaje = "Imagen escalada correctamente";
            return new BitmapDrawable(resizedBitmap);

        } catch (Exception e) {
            mensaje = "Error escalando la Imagen: " + e.toString();
            return null;

        } finally {
            matrix = null;
            resizedBitmap = null;
            bitmapOriginal = null;
            System.gc();
        }
    }

    public static Drawable ResizedImage(Drawable imgOriginal, int newWidth, int newHeight) {
        Matrix matrix;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;
        try {
            // bitmapOriginal = BitmapDrawable(resizedBitmap);
            bitmapOriginal = ((BitmapDrawable) imgOriginal).getBitmap();
            int width = bitmapOriginal.getWidth();
            int height = bitmapOriginal.getHeight();
            if (width == newWidth && height == newHeight) {
                return new BitmapDrawable(bitmapOriginal);
            }
            // Reescala el Ancho y el Alto de la Imagen
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            // Crea la Imagen con el nuevo Tamano
            resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
            mensaje = "Imagen escalada correctamente";
            return new BitmapDrawable(resizedBitmap);

        } catch (Exception e) {
            mensaje = "Error escalando la Imagen: " + e.toString();
            return null;

        } finally {
            matrix = null;
            resizedBitmap = null;
            bitmapOriginal = null;
            System.gc();
        }
    }

    public static boolean BorrarDataBase() {
        File dbFile = new File(Util.DirApp(), "DataBase.db");
        try {
            boolean borro = false;
            if (dbFile.exists()) borro = dbFile.delete();
            return borro;

        } catch (Exception e) {
            return false;
        }
    }

    public static String SepararPalabrasTextView(String cadena, int caracteres) {
        StringBuffer buffer = new StringBuffer();
        String mainString = cadena;
        if (mainString.equals("")) {
            buffer.append(mainString);
        }
        while (!mainString.equals("")) {
            if (mainString.length() <= caracteres) {
                if (!buffer.toString().equals("")) buffer.append("<br />");
                buffer.append(mainString);
                mainString = "";

            } else {
                String tempString;
                if (mainString.length() >= caracteres) {
                    if (mainString.lastIndexOf(' ') == -1) {
                        if (!buffer.toString().equals("")) buffer.append("<br />");
                        buffer.append(mainString);
                        mainString = "";
                        continue;

                    } else {
                        tempString = mainString.substring(0, caracteres);
                    }

                } else {
                    tempString = mainString;
                }
                if (tempString.lastIndexOf(' ') == -1) {
                    if (!buffer.toString().equals("")) buffer.append("<br />");
                    buffer.append(tempString);
                    if (!tempString.equals(mainString)) {
                        mainString = mainString.substring(caracteres + 1);

                    } else {
                        mainString = "";
                    }

                } else {
                    if (!buffer.toString().equals("")) buffer.append("<br />");
                    buffer.append(tempString.substring(0, tempString.lastIndexOf(' ')));
                    mainString = mainString.substring(tempString.lastIndexOf(' ') + 1);
                }
            }
        }
        return buffer.toString();
    }

    public static void Headers(TableLayout tabla, String[] cabecera, Context context) {
        TableRow fila = new TableRow(context);
        for (int i = 0; i < cabecera.length; i++) {
            TextView lbl = new TextView(context);
            lbl.setText("" + cabecera[i]);
            lbl.setPadding(5, 0, 5, 0);
            lbl.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            lbl.setTextColor(Color.argb(255, 255, 255, 255));
            lbl.setBackgroundColor(context.getResources().getColor(R.color.blue_onfocus));
            fila.addView(lbl);
        }
        tabla.addView(fila);
    }

    public static String replicate(String cadena, int cant) {
        String cadAux = "";
        for (int i = 0; i < cant; i++) {
            cadAux += cadena;
        }
        return cadAux;
    }

    public static void aplicarFuente(final Context context, final View view, int type) {
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), "fonts/PTS76F.ttf");
        }
        cambiarFuente(context, view, type);
    }

    private static void cambiarFuente(final Context context, final View view, int type) {
        try {
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int count = viewGroup.getChildCount();
                for (int i = 0; i < count; i++) {
                    View child = viewGroup.getChildAt(i);
                    cambiarFuente(context, child, type);
                }

            } else {
                switch (type) {
                    case Const.TEXTVIEW:
                        if (view instanceof TextView) ((TextView) view).setTypeface(typeface);
                        break;
                    case Const.BUTTON:
                    default:
                        if (view instanceof Button) ((Button) view).setTypeface(typeface);
                        break;
                }
            }

        } catch (Exception e) {
            String msg = e.getMessage();
            Log.e(TAG, "cambiarFuente -> " + msg, e);
        }
    }

    public static String[] split(String original, String separator) {
        Vector nodes = new Vector();
        // Parse nodes into vector
        int index = original.indexOf(separator);
        while (index >= 0) {
            nodes.addElement(original.substring(0, index));
            original = original.substring(index + separator.length());
            index = original.indexOf(separator);
        }
        // Get the last node
        nodes.addElement(original);
        // Create splitted string array
        String[] result = new String[nodes.size()];
        if (nodes.size() > 0) {
            for (int loop = 0; loop < nodes.size(); loop++)
                result[loop] = (String) nodes.elementAt(loop);
        }
        return result;
    }

    public static boolean checkSDCard() {
        boolean check = false;
        try {
            if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                check = true;

            } else {
                check = false;

            }

        } catch (Exception e) {
            check = false;

        }
        return check;

    }

    public static String getDecimalFormat(float numero) {
        DecimalFormat myFormatter = new DecimalFormat("0.00");
        String output = myFormatter.format(numero);
        return output;
    }

    public static String getDecimalFormatMargen(float numero) {
        DecimalFormat myFormatter = new DecimalFormat("0");
        String output = myFormatter.format(numero);
        return output;
    }

    public static String getDecimalFormat3(float numero) {
        DecimalFormat myFormatter = new DecimalFormat("0.000");
        String output = myFormatter.format(numero);
        return output;
    }

    public static String getDecimalFormat(double numero) {
        DecimalFormat myFormatter = new DecimalFormat("0.00");
        String output = myFormatter.format(numero);
        return output;
    }

    public static String getDecimalFormatCartera(double numero) {
        DecimalFormat myFormatter = new DecimalFormat("0");
        String output = myFormatter.format(numero);
        return output;
    }

    public static String quitaEspacios(String texto) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(texto);
        texto = "";
        while (tokens.hasMoreTokens()) {
            texto += " " + tokens.nextToken();
        }
        texto = texto.toString();
        texto = texto.trim();
        return texto;
    }

    public static void closeTecladoStartActivity(Activity context) {
        context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    public static void playSound(Context context) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        MediaPlayer mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setDataSource(context, soundUri);
        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepare();
            mMediaPlayer.start();
        }
    }

    public static void playSound2() {
        final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
        tg.startTone(ToneGenerator.TONE_PROP_BEEP);

    }

    public static String DateToStringYYYYMMDD() {
        Calendar calendario;
        String fechaRetorna = "";
        Date date = new Date();
        calendario = Calendar.getInstance();
        calendario.setTime(date);
        fechaRetorna += String.valueOf(calendario.get(Calendar.YEAR));
        if ((calendario.get(Calendar.MONTH) + 1) < 10)
            fechaRetorna += "0" + (calendario.get(Calendar.MONTH) + 1);
        else fechaRetorna += String.valueOf(calendario.get(Calendar.MONTH) + 1);
        if (calendario.get(Calendar.DAY_OF_MONTH) < 10)
            fechaRetorna += "0" + calendario.get(Calendar.DAY_OF_MONTH);
        else fechaRetorna += String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
        return fechaRetorna;
    }

    public static String DateToStringYYYYMMDD2() {
        Calendar calendario;
        String fechaRetorna = "";
        Date date = new Date();
        calendario = Calendar.getInstance();
        calendario.setTime(date);
        String[] days = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
        int dayOfWeek = calendario.get(Calendar.DAY_OF_WEEK);
        String day = days[dayOfWeek - 1];
        if (day.equals("SATURDAY")) {
            calendario.add(Calendar.DAY_OF_MONTH, 3);

        } else {
            calendario.add(Calendar.DAY_OF_MONTH, 2);
        }
        fechaRetorna += String.valueOf(calendario.get(Calendar.YEAR));
        if ((calendario.get(Calendar.MONTH) + 1) < 10)
            fechaRetorna += "0" + (calendario.get(Calendar.MONTH) + 1);
        else fechaRetorna += String.valueOf(calendario.get(Calendar.MONTH) + 1);
        if (calendario.get(Calendar.DAY_OF_MONTH + 1) < 10)
            fechaRetorna += "0" + calendario.get(Calendar.DAY_OF_MONTH);
        else fechaRetorna += String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
        return fechaRetorna;
    }

    /**
     * @param datePicker
     * @return a java.util.Date
     */
    public static java.util.Date getDateFromDatePicket(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }

    public static void guardarDatosDeRecaudo() {
        DataBaseBO.CrearInfoTemp();
        objeto obj = new objeto();
        obj.cartera = Main.cartera;
        obj.listaFormasPago = Main.listaFormasPago;
        obj.total_forma_pago = Main.total_forma_pago;
        obj.total_descuento = Main.total_descuento;
        obj.total_recaudo = Main.total_recaudo;
        DataBaseBO.borrarObjeto();
        // DataBaseBO.saveObject(obj);
    }

    public static boolean estaActualLaFecha() {
        String fechaActual;
        boolean bien = false;
        fechaActual = FechaActual("yyyy/MM/dd");
        if (fechaActual.equals(Main.usuario.fechaLabores)) {
            bien = true;
        }
        return bien;
    }
  
  /*public static boolean turnOnBluetooth() {
    try {
      BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if (bluetoothAdapter != null) {
        if (bluetoothAdapter.isEnabled()) {
          return true;
        }
        if (!bluetoothAdapter.isEnabled()) {
          boolean activo = bluetoothAdapter.enable();
          return activo;
        }
      }
      
    } catch (Exception e) {
      Log.e(TAG, "turnOnBluetooth -> " + e.getMessage(), e);
    }
    return false;
  }
  
  public static boolean turnOffBluetooth() {
    try {
      BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if (bluetoothAdapter != null) {
        if (bluetoothAdapter.isEnabled()) {
          return bluetoothAdapter.disable();
        }
      }
      return false;
      
    } catch (Exception e) {
      Log.e(TAG, "turnOffBluetooth -> " + e.getMessage(), e);
    }
    return false;
  }*/

    public static String CentrarLinea(String linea, int numSpace) {
        String centrado = "";
        int len = linea.length();
        if (len > numSpace) {
            linea = linea.substring(0, numSpace);
            return linea;
        }
        int espacios = (numSpace - len) / 2;
        for (int i = 0; i < espacios; i++) {
            centrado += " ";
        }
        if ((numSpace - len) % 2 != 0) centrado += " ";
        centrado = centrado + linea;
        for (int i = 0; i < espacios; i++) {
            centrado = centrado + " ";
        }
        return centrado;
    }

    /**
     * llenar con caracteres a la derecha
     *
     * @param cadena
     * @param tamano
     * @param caracter
     * @return
     */
    public static String rpad(String cadena, int tamano, String caracter) {
        int i;
        int tamano1;
        tamano1 = cadena.length();
        if (tamano1 > tamano) cadena = cadena.substring(0, tamano);
        tamano1 = cadena.length();
        for (i = tamano1; i < tamano; i++)
            cadena = cadena + caracter;
        return cadena;
    }

    public static String line(String cadena, int tamano) {
        String line = "";
        while (line.length() < tamano) {
            line += cadena;
        }
        return line;
    }

    public static Date converFecha(String date) {
        SimpleDateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
        Date valorFecha = null;
        try {
            valorFecha = fecha.parse(date);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return valorFecha;
    }

    public static void mostrarToast(Context context, String mensaje) {
        Toast toast1 = Toast.makeText(context, mensaje, Toast.LENGTH_SHORT);
        toast1.show();
    }

    public static int ObtenerDiaSemanaNumeroEntero() {
        Date fecha = new Date();
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(fecha);
        int dia = cal.get(Calendar.DAY_OF_WEEK) - 1;
        return dia;
    }
    // registro de coordenadas

    public static boolean estaGPSEncendido(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return statusOfGPS;
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    /**
     * metodo para definir el tipo de transaccion actual. por defecto la transaccion
     * es preventa.
     */
    public static boolean identificarTipoTransaccion() {
        boolean isAutoventa = false;
        Usuario usuario = DataBaseBO.ObtenerUsuario();
        if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
            isAutoventa = Const.IS_AUTOVENTA;
        }
        return isAutoventa;
    }

    public static String generarNumdoc(String seccion, String codCliente, String codUsuario) {
        System.out.println("@@@@@ " + codUsuario + "    " + codCliente);
        return seccion + codUsuario + codCliente + Util.FechaActual("yyyyMMddHHmmss");// +
        // Util.lpad(Util.FechaActual("Ms"),
        // 3, "0");
    }

    public static void BlueetoothON() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.disable()) {
            mBluetoothAdapter.enable();
        }

    }

    public static void mostrarDialogGeneral(Context context, String titulo, String mensaje) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_generico);
        ((TextView) dialog.findViewById(R.id.txtTituloGeneral)).setText(titulo);
        ((TextView) dialog.findViewById(R.id.txtMensajeGeneral)).setText(mensaje);
        ((Button) dialog.findViewById(R.id.btnAceptarGenerico)).setOnClickListener(new AdapterView.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.cancel();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public static void HeadersMapas(TableLayout tabla, final String[] cabecera, Context context) {
        final TableRow fila = new TableRow(context);
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 3, 0, 30);
        for (int i = 0; i < cabecera.length; i++) {
            TextView lbl = new TextView(context);
            lbl.setText("" + cabecera[i]);
            lbl.setPadding(5, 0, 5, 0);
            lbl.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            lbl.setTextColor(Color.argb(255, 255, 255, 255));
            lbl.setTextSize(8);
            // lbl.setBackgroundDrawable(context.getResources().getDrawable(android.R.drawable.editbox_dropdown_dark_frame));
            lbl.setBackground(context.getResources().getDrawable(R.drawable.table_cell_header));
            fila.addView(lbl);
        }

        /*
         * alinear la cuadricula para que se conserven del mismo tama�o que su par. el
         * valor height de categoria es siempre mas grande que el de total, por esto se
         * elige como referencia
         */
        ViewTreeObserver viewTreeObserver = fila.getChildAt(0).getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                int height = fila.getChildAt(0).getMeasuredHeight();
                for (int i = 0; i < cabecera.length; i++) {
                    fila.getChildAt(i).setMinimumHeight(height);
                }
            }
        });
        fila.setLayoutParams(params);
        tabla.addView(fila);
    }

    public static String obtenerVersion(Context context) {
        String version;
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            version = "0.0";
            Log.e(TAG, "obtenerVersion -> " + e.getMessage(), e);
        }
        return version;
    }

    public static void MostrarAlertDialogEstandar(Context context, String mensaje) {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    public static void cargarIconoEditText(Context context, EditText editText, IIcon icon, int color, int colorPress, int sizeDp) {
        //int sizeDp = Const.SIZEDP;
        /*float density = context.getResources().getDisplayMetrics().density;
        int width = Math.round(35 * density);
        int height = Math.round(30 * density);
        Log.e(TAG,"density: "+density);*/
        Drawable imgNormal = createFontIcon(context, icon, color, sizeDp);
        Drawable imgPressed = createFontIcon(context, icon, colorPress, sizeDp);
        StateListDrawable selectorStates = createSelector(imgNormal, imgPressed);
        editText.setCompoundDrawablesRelativeWithIntrinsicBounds(selectorStates, null, null, null);
        editText.setCompoundDrawablesRelative(selectorStates, null, null, null);
    }

    public static StateListDrawable createSelector(Drawable imgNormal, Drawable imgPressed) {
        StateListDrawable states = new StateListDrawable();
        //android:state_checked="true"
        states.addState(new int[]{android.R.attr.state_checked}, imgNormal);
        //android:state_window_focused="false"
        states.addState(new int[]{-android.R.attr.state_window_focused}, imgNormal);
        //android:state_pressed="true"
        states.addState(new int[]{android.R.attr.state_pressed}, imgPressed);
        //android:state_focused="true"
        states.addState(new int[]{android.R.attr.state_focused}, imgPressed);
        //Normal, without state
        states.addState(new int[]{}, imgNormal);
        return states;
    }

    public static Drawable createFontIcon(Context context, IIcon icon, int color, int sizeDp) {
        Drawable drawable = new IconicsDrawable(context).icon(icon).color(context.getResources().getColor(color)).sizeDp(sizeDp);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        return drawable;
    }

    public static boolean isWifiOn(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return wifiManager.isWifiEnabled();
    }

    public static boolean isMobileNetworkOn(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo.State mobile = connectivity.getNetworkInfo(0).getState();
            if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
                return true;
            }
        }
        return false;
    }

    public static boolean gpsIsOn(Context context) {
        boolean estadoGPS = false;
        LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (mLocationManager != null) {
            estadoGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
        return estadoGPS;
    }

    public static RegistroRango validarRegistroRango(final Context context, Cliente cliente) {
        RegistroRango registroRango = null;
        boolean wifi = isWifiOn(context);
        boolean mobnet = isMobileNetworkOn(context);
        if (!wifi && !mobnet) {
            mostrarAlertDialog(context, "Por favor encienda los medios de conexion a internet para continuar.");
        } else {
            boolean estadoGPS = false;
            estadoGPS = Util.gpsIsOn(context);
            if (estadoGPS) {
                GPSTracker gpsTracker = new GPSTracker(context);
                Location location = null;
                if (gpsTracker != null) {
                    location = gpsTracker.getLocation();
                }
                int estado = 1;
                double diff = -1;
                if (location != null) {
                    diff = Util.validarDiferenciaRango(location, cliente);
                    estado = Util.validarEstadoRango(diff);
                    registroRango = new RegistroRango();
                    registroRango.radio = DataBaseBO.obtenerDistanciaMaxima() + "";
                    registroRango.distancia = diff + "";
                    registroRango.perimetro = estado + "";
                    registroRango.latitud = location.getLatitude() + "";
                    registroRango.longitud = location.getLongitude() + "";
                } else {
                    registroRango = new RegistroRango();
                    registroRango.radio = DataBaseBO.obtenerDistanciaMaxima() + "";
                    registroRango.distancia = "-1";
                    registroRango.perimetro = "-1";
                    registroRango.latitud = "0";
                    registroRango.longitud = "0";
                }
            } else {
                mostrarAlertDialogGPSOff(context, "EL GPS ESTA DESACTIVADO, DEBE ACTIVARLO PARA CONTINUAR.");
            }
        }
        return registroRango;
    }

    public static double validarDiferenciaRango(Location location, Cliente cliente) {
        double distance = -1;
        if (location != null) {
            if (location.getLatitude() != 0 && location.getLongitude() != 0) {
                if (cliente == null) {
                    int tipoDeClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();
                    if (tipoDeClienteSeleccionado == 1) {
                        cliente = DataBaseBO.CargarClienteSeleccionado();
                    } else {
                        cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
                    }
                }
                if (cliente.latitud != 0 && cliente.longitud != 0) {
                    double latCliente = 0;
                    double lonCliente = 0;
                    if (cliente != null) {
                        latCliente = cliente.latitud;
                        lonCliente = cliente.longitud;
                    }
                    double latActual = location.getLatitude();
                    double lonAtual = location.getLongitude();
                    //float[]results = null;
                    //results = Location.distanceBetween(latCliente, lonCliente, latActual , lonAtual, results);
                    Location locationA = new Location("pointclient");
                    locationA.setLatitude(latCliente);
                    locationA.setLongitude(lonCliente);
                    Location locationB = new Location("pointcurrent");
                    locationB.setLatitude(latActual);
                    locationB.setLongitude(lonAtual);
                    distance = locationA.distanceTo(locationB);
                    float[] results = new float[5];
                    Location.distanceBetween(latCliente, lonCliente, latActual, lonAtual, results);
                    for (int i = 0; i < results.length; i++) {
                        Log.i(TAG, " +++++++ results: " + results[i]);
                    }
                    Log.i(TAG, " datos coordenadas cliente LAT: " + latCliente + " LONG: " + lonCliente);
                    Log.i(TAG, "datos coordenadas actuales LAT: " + latActual + " LONG: " + lonAtual);
                    Log.i(TAG, "DISTANCE TO: " + distance);

                } else {
                    distance = -99;
                }
            }
        }
        return distance;
    }

    public static int validarEstadoRango(double diff) {
        double distMax = DataBaseBO.obtenerDistanciaMaxima();
        Log.i("DISTANCE MAXIMA: ", distMax + "");
        Log.i("DISTANCE DIFF: ", diff + "");
        if (diff > distMax || diff < 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public static void mostrarAlertDialog(Context context, String mensaje) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_generico);
        ((TextView) dialog.findViewById(R.id.txtMensaje)).setText(mensaje);
        ((Button) dialog.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void mostrarAlertDialogGPSOff(final Context context, String mensaje) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_generico);
        ((TextView) dialog.findViewById(R.id.txtMensaje)).setText(mensaje);
        ((Button) dialog.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Intent settingsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                ((Activity) context).startActivity(settingsIntent);
                System.exit(0);
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void putPrefence(Context context, String preference, String key[], String data[]) {
        SharedPreferences preferencias = context.getSharedPreferences(preference, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        for (int i = 0; i < data.length; i++) {
            editor.putString(key[i], data[i]);
        }
        editor.commit();
    }

    public static String[] getPrefence(Context context, String preference, String key[]) {
        String datos[] = new String[key.length];
        SharedPreferences preferencias = context.getSharedPreferences(preference, Context.MODE_PRIVATE);
        for (int i = 0; i < key.length; i++) {
            datos[i] = preferencias.getString(key[i], "0");
        }
        return datos;
    }

    public static void clearPrefence(Context context, String preference) {
        SharedPreferences preferencias = context.getSharedPreferences(preference, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.clear();
        editor.commit();
    }

    public static String obtenerNumdoc(String seccion) {
        //seccion = seccion.replace(".", "");
        String seccionseparador = seccion + Util.FechaActual("yyyyMMdd") + Util.FechaActual("HHmmssSSS") + Util.lpad(Util.FechaActual("Ms"), 3, "0"); //agregarSeparador(seccion);
        return seccionseparador;
    }

    public static void MostrarToast(Context context, String mensaje) {
        Toast toast = Toast.makeText(context, mensaje, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void OcultarTeclado(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static boolean estaArchivo(String nombreArchivo) {
        File archivoEntrenamiento = new File(Util.DirApp(), nombreArchivo);
        try {
            if (archivoEntrenamiento.exists()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static Bitmap ResizedImageWaterMark(Bitmap foto, String nombre, Context context) {
        Bitmap bitmapResized = null;
        Bitmap bitmapOriginal = null;
        try {
            if (foto != null) {
                bitmapOriginal = foto;
                int width = bitmapOriginal.getWidth();
                int height = bitmapOriginal.getHeight();
                float scaleWidth = ((float) Const.WIDHTFOTO) / width;
                float scaleHeight = ((float) Const.HEIGHTFOTO) / height;
                Matrix matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);
                // Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0,
                // 0, width, height, matrix, false);
                bitmapResized = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);
                Canvas canvas = new Canvas(bitmapResized);
                String marcaAguaTxt = nombre + "   " + Util.FechaActual("yyyy-MM-dd") + "   " + Util.FechaActual("HH:mm:ss");
                Paint paint = new Paint();
                paint.setColor(Color.RED);
                paint.setTextSize(20);
                paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
                paint.setTextAlign(Paint.Align.LEFT);
                int yPos = canvas.getHeight();
                canvas.drawText(marcaAguaTxt, 0, yPos, paint);
                canvas.drawBitmap(bitmapResized, 0, 0, paint);
                /* Bitmap marcaAguaImg = BitmapFactory.decodeResource(context.getResources(),
                 * R.drawable.watermark); Paint paintImg = new Paint();
                 * paintImg.setTextAlign(Paint.Align.LEFT); canvas.drawBitmap(marcaAguaImg, 0,
                 * 0, paintImg); */
                /* marcaAguaImg = BitmapFactory.decodeResource(context.getResources(),
                 * R.drawable.logosmall); canvas.drawBitmap(marcaAguaImg, (canvas.getWidth() /
                 * 2) - (marcaAguaImg.getWidth() / 2),(canvas.getHeight() / 2) -
                 * (marcaAguaImg.getHeight() / 2), paintImg); canvas.drawBitmap(bitmapResized,
                 * 0, 0, paint); */
                // bitmapResized.compress(Bitmap.CompressFormat.JPEG, 50, fOut);
                bitmapResized = Bitmap.createBitmap(bitmapResized, 0, 0, width, height, matrix, true);
            }
        } catch (Exception e) {
        } finally {
            bitmapOriginal = null;
            System.gc();
        }
        return bitmapResized;
    }

    public static boolean isAppInstalled(Activity contexto, String packageName) {

        PackageManager pm = contexto.getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }


    public static String getFcmIntentName() {
        return "celuweb.com.Catalogo.FirebaseCloudMessaging";
    }

    public static String getNameMethod(Object object) {
        if (object != null) {
            if (object.getClass() != null) {
                if (object.getClass().getEnclosingMethod() != null) {
                    return object.getClass().getEnclosingMethod().getName();
                }
            }
        }
        return "(:)";
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static String getFormatCurrency(Double value) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setMinimumFractionDigits(0);
        format.setCurrency(Currency.getInstance("COP"));
        String result = format.format(value).replace("COP", "").trim();
        return "$" + result;
    }

    public static String setFormatoCOP(String numero) {

        String cantidad;
        String cantidadAux1;
        String cantidadAux2;
        boolean tieneMenos;

        int posPunto;
        int i;

        cantidad = "";
        cantidadAux1 = "";
        cantidadAux2 = "";

        tieneMenos = false;
        if (numero.indexOf("-") != -1) {

            String aux;
            tieneMenos = true;
            aux = numero.substring(0, numero.indexOf("-"));
            aux = aux + numero.substring(numero.indexOf("-") + 1, numero.length());
            numero = aux;
        }

        if (numero.indexOf(".") == -1) {

            if (numero.length() > 3) {

                cantidad = colocarComas(numero, numero.length());

            } else {

                if (tieneMenos)
                    numero = "$" + numero;
                else
                    numero = "$" + numero;

                return numero;
            }

        } else {

            posPunto = numero.indexOf(".");

            for (i = 0; i < posPunto; i++) {

                cantidadAux1 = cantidadAux1 + numero.charAt(i);
            }

            for (i = posPunto; i < numero.length(); i++) {

                cantidadAux2 = cantidadAux2 + numero.charAt(i);
            }

            if (cantidadAux1.length() > 3) {

                cantidad = colocarComas(cantidadAux1, posPunto);
                cantidad = cantidad + cantidadAux2;

            } else {

                if (tieneMenos)
                    numero = "$" + numero;
                else
                    numero = "$" + numero;

                return numero;
            }
        }

        if (tieneMenos)
            cantidad = "$" + cantidad;
        else
            cantidad = "$" + cantidad;

        return cantidad;
    }


    public static String colocarComas(String numero, int pos) {

        String cantidad;
        Vector<String> cantidadAux;
        String cantidadAux1;
        int i;
        int cont;

        cantidadAux = new Vector<String>();
        cantidad = "";
        cantidadAux1 = "";
        cont = 0;

        for (i = (pos - 1); i >= 0; i--) {

            if (cont == 3) {

                cantidadAux1 = "," + cantidadAux1;
                cantidadAux.addElement(cantidadAux1);
                cantidadAux1 = "";
                cont = 0;
            }

            cantidadAux1 = numero.charAt(i) + cantidadAux1;
            cont++;
        }

        cantidad = cantidadAux1;

        for (i = cantidadAux.size() - 1; i >= 0; i--) {
            cantidad = cantidad + cantidadAux.elementAt(i).toString();
        }
        return cantidad;
    }

    public static Float quitarComasYsignoPeso(String valor) {
        float valorDevolver = 0;
        char axu[] = valor.toCharArray();
        String completo = "";

        for (int i = 0; i < axu.length; i++) {

            if (axu[i] == '&') {

            } else if (axu[i] == '.') {

            } else if (axu[i] == ',') {

            } else if (axu[i] == ' ') {

            } else {
                completo = completo + axu[i];
            }

        }

        valorDevolver = Float.parseFloat(completo);

        return valorDevolver;
    }


    public static void mostrarToast1(Context contexto, String mensaje) {

        Toast toast = Toast.makeText(contexto, mensaje, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);

        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }

}
