/**
 *
 */

package celuweb.com.uyusa;

import java.util.ArrayList;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Ciudad;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Usuario;
import celuweb.com.keyboard.custom.CustomKeyboard;

/**
 * Permite modificar la informacion de un cliente
 * @author JICZ
 */
public class FormModificarCliente extends Activity {

    public static final String TAG = FormModificarCliente.class.getName();
    /**
     * cliente actual seleccionado y que sera modificado
     */
    private Cliente cliente;

    Vector<Ciudad> listaDireccion;

    private Usuario vendedor;

    public Spinner spDireccion;

    private EditText txtCodigo;

    private EditText txtRazon;

    private EditText txtPropietario;

    private EditText txtTelefono;

    private EditText txtEmail;

    private EditText txtContacto;

    private EditText txtDireccion;

    private Spinner cbRuta;

    boolean isClienteNuevo = false;

    public EditText edtLatitud;
    public EditText edtLongitud;

    Dialog dialogCertificacionCoordenadas;

    ProgressDialog progressDialog;

    /**
     * teclado personalizado para escribir direcciones.
     */
    CustomKeyboard customKeyboard;

    /**
     * lista de rutas disponble. (desde RU01 hasta RU05)
     */
    ArrayList<String> rutas_parada;




    @Override
    protected void onCreate (Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.form_modificar_cliente);

        String codigoCliente = "";

        //recibir codigo de cliente seleccionado
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            codigoCliente = bundle.getString("codigoCliente");
            isClienteNuevo = bundle.getBoolean("clienteNuevo");
        }

        if (codigoCliente != null) {

            if (isClienteNuevo)
                cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
            else
                cliente = DataBaseBO.BuscarCliente(codigoCliente);


            vendedor = DataBaseBO.ObtenerUsuario();
        }

        //verificar que el cliente se cargo correctamente.
        if (cliente == null) {
            Toast.makeText(getBaseContext(), "Fallo lectura de base de datos. Intente de nuevo", Toast.LENGTH_LONG).show();
            finish();
        }
        //habilitar el teclado personalizado
        customKeyboard = new CustomKeyboard(this, R.id.keyboardview, R.xml.keyboard_nom);
        // registrar el edittext de direccion
        customKeyboard.registerEditText(R.id.txtDireccionModificar);

        //cargar instancias de los view.
        instanciarViews();

        //cargar rutas parada
        rutas_parada = new ArrayList<String>(5); // capacidad inicial a 5.
        DataBaseBO.cargarRutasParada(rutas_parada);

        //mostrar la informacion del cliente.
        mostrarInfoClienteView();
    }



    /**
     * cargar las instancias de cada view utilizado.
     */


    private void instanciarViews () {

        txtCodigo = (EditText) findViewById(R.id.txtCodigoModificar);
        txtRazon = (EditText) findViewById(R.id.txtNegocioModificar);
        txtPropietario = (EditText) findViewById(R.id.txtPropietarioModificar);
        txtTelefono = (EditText) findViewById(R.id.txtTelefonoModificar);
        txtDireccion = (EditText) findViewById(R.id.txtDireccionModificar);
        txtEmail = (EditText) findViewById(R.id.txtEmailModificar);
        txtContacto = (EditText) findViewById(R.id.txtContactoModificar);
        cbRuta = (Spinner) findViewById(R.id.cbRutaModificar);
    }

    /**
     * Cargar las vistas con la informacion del cliente.
     */


    private void mostrarInfoClienteView () {

        txtCodigo.setText(Main.cliente.Nit);
        txtCodigo.setEnabled(false);
        txtRazon.setText(Main.cliente.razonSocial);
        txtRazon.setEnabled(false);
        txtPropietario.setText(Main.cliente.Nombre);
        txtPropietario.setEnabled(false);
        txtTelefono.setText(Main.cliente.Telefono);
        txtDireccion.setText(Main.cliente.direccion);
        txtEmail.setText(Main.cliente.email);
        txtContacto.setText(Main.cliente.contacto);

        //cargar el spinner con la lista de rutas.
        String[] items = new String[rutas_parada.size()];
        items = rutas_parada.toArray(items);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        cbRuta.setAdapter(adapter);

        //cargar la ruta inicialmente definidia.
        if (rutas_parada.contains(cliente.rutaParada)) {
            int index = rutas_parada.indexOf(cliente.rutaParada);
            cbRuta.setSelection(index);
        }
    }



    /**
     * captura las modificaciones efectuadas por el usuario.
     */
    private void capturarCambios () {

        String telefono = txtTelefono.getText().toString().trim();
        String direccion = txtDireccion.getText().toString().trim();

        if (telefono == null || telefono.length() == 0 || telefono.equals("")) {
            Toast.makeText(FormModificarCliente.this, "Ingrese un telefono", Toast.LENGTH_SHORT).show();
            return;
        }

        if (direccion == null || direccion.length() == 0 || direccion.equals("")) {
            Toast.makeText(FormModificarCliente.this, "Ingrese una direccion", Toast.LENGTH_SHORT).show();
            return;
        }

        cliente.Telefono = telefono;
        cliente.direccion = direccion;
        cliente.email = txtEmail.getText().toString().trim();
        cliente.contacto = txtContacto.getText().toString().trim();
       /* String rutaParada = rutas_parada.get(cbRuta.getSelectedItemPosition()).trim();
        if (rutaParada.length() >= 4) {
            cliente.rutaParada = rutaParada.substring(2) + Util.lpad(String.valueOf(cliente.ordenRuta), 3, "0");
        } else {
            cliente.rutaParada = rutaParada.substring(0, 1) + Util.lpad(String.valueOf(cliente.ordenRuta), 3, "0");
        }*/
    }



    /**
     * inserta los cambios realizados al cliente en la tabla clientesmod en temp.db
     */
    private void guardarClienteModificado () {
        //eliminar clientes que ya esten en la tabla, y conservar solamente el ultimo cambio realizado.
        DataBaseBO.elimiarClienteRepetidoModificado(cliente.codigo);
        boolean modificado = DataBaseBO.guardarClienteModificado(cliente, vendedor);
        if (modificado) {
            Toast.makeText(FormModificarCliente.this, "Cliente modificado correctamente", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(FormModificarCliente.this, "Fallo modificacion, Intente de nuevo.", Toast.LENGTH_SHORT).show();
        }
    }



    /**
     * modificar el cliente
     * @param v
     */
    public void OnClickAceptarModificarCliente (View v) {

        //capturarCambios();
        //guardarClienteModificado();
        alertCertificacionCoordenadas();
    }

    public void alertCertificacionCoordenadas(){
        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {



                dialog.cancel();
                dialogCertificacionCoordenadas();
            }
        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
                capturarCambios();
                guardarClienteModificado();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage("Desea certificar las coordenadas para este cliente?");
        alertDialog.show();
    }

    public void dialogCertificacionCoordenadas(){
        if (dialogCertificacionCoordenadas == null) {

            dialogCertificacionCoordenadas = new Dialog(this);
            dialogCertificacionCoordenadas.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogCertificacionCoordenadas.setContentView(R.layout.dialog_certificacion_coordenadas);
        }

         edtLatitud = (EditText) dialogCertificacionCoordenadas.findViewById(R.id.edtLatitud);
         edtLongitud = (EditText) dialogCertificacionCoordenadas.findViewById(R.id.edtLongitud);

        spDireccion = (Spinner) dialogCertificacionCoordenadas.findViewById(R.id.spDireccion);

        cargarDireccion();

        Button btnSi = (Button) dialogCertificacionCoordenadas.findViewById(R.id.btnSi);
        Button btnNo = (Button) dialogCertificacionCoordenadas.findViewById(R.id.btnNo);
        Button btnCapturaCoordenada= (Button) dialogCertificacionCoordenadas.findViewById(R.id.btnCapturaCoordenada);

        btnCapturaCoordenada.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onClickCapturarCoordenada();
            }
        });

        btnSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarCoordenadas();
                //dialogCertificacionCoordenadas.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtLatitud.setText("");
                edtLongitud.setText("");
                dialogCertificacionCoordenadas.dismiss();
            }
        });

        dialogCertificacionCoordenadas.setCancelable(false);
        dialogCertificacionCoordenadas.show();
        dialogCertificacionCoordenadas.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void guardarCoordenadas() {

        if (edtLatitud.getText().toString().equals("") || edtLongitud.getText().toString().equals("")){
            MostrarAlertDialog("Debe capturar coordenadas !");
        }else{
            int codigoDireccion = Integer.parseInt(listaDireccion.get(spDireccion.getSelectedItemPosition()).codigo);
            cliente.codigoDir = codigoDireccion;
            if (cliente.codigoDir != -1){

                boolean certificoCoordenadas = DataBaseBO.GuardarCertificacionCoordenadas(cliente);

                if (certificoCoordenadas) {
                    edtLatitud.setText("");
                    edtLongitud.setText("");
                    Util.mostrarToast(this, "Coordenadas certificadas correctamente!");
                    finish();
                } else {
                    edtLatitud.setText("");
                    edtLongitud.setText("");
                    Util.mostrarToast(this, "No se pudo certificar las coordenadas!");
                }
            }else {
                MostrarAlertDialog("Debe seleccionar direccion!");
            }
        }


    }

    public void MostrarAlertDialog(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();

            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    private void cargarDireccion() {

        String[] items;
        Vector<String> listaItems = new Vector<String>();
        listaDireccion = DataBaseBO.cargarListaDireccion(listaItems, Main.cliente.Nit);

        if (listaItems.size() > 0) {

            items = new String[listaItems.size()];
            listaItems.copyInto(items);

        } else {

            items = new String[]{};
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDireccion.setAdapter(adapter);
    }

    public void onClickCapturarCoordenada() {


        progressDialog = ProgressDialog.show(FormModificarCliente.this, "", "Por favor espere...\n\nObteniendo Coordenada...", true);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(2000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (obtenerCoordenada()) {
                                    if (progressDialog != null)
                                        progressDialog.cancel();
                                } else {
                                    if (progressDialog != null)
                                        progressDialog.cancel();
                                    Util.mostrarAlertDialog(FormModificarCliente.this, "No se obtuvo coordenada.\nVerifique su conexion.");
                                }

                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public boolean obtenerCoordenada() {

        boolean estado = false;
        GPSTracker gpsTracker = new GPSTracker(this);
        Location location = gpsTracker.getLocation();
        boolean estadoGPS = gpsTracker.isEstadoGPS();
        double latitud = 0;
        double longitud = 0;
        //String direccionCapturada = "Direccion Aprox Capturada: ";
        //String ciudadCapturada = "Ciudad Capturada: ";
        if (location != null && estadoGPS) {
            latitud = location.getLatitude();
            longitud = location.getLongitude();
            //direccionCapturada += gpsTracker.getAddress(location);
            //ciudadCapturada += gpsTracker.getCity(location);
        }

        Log.i(TAG," coordenadas capturada LAT: "+latitud+" LONG: "+longitud);

        if (estadoGPS) {
            if (latitud != 0 && longitud != 0) {

                final EditText edtLatitud = (EditText) dialogCertificacionCoordenadas.findViewById(R.id.edtLatitud);
                final EditText edtLongitud = (EditText) dialogCertificacionCoordenadas.findViewById(R.id.edtLongitud);

                edtLatitud.setText(latitud + "");
                edtLongitud.setText(longitud + "");

                cliente.latitud=latitud;
                cliente.longitud=longitud;
                cliente.Nit= Main.cliente.Nit;


                estado = true;
            } else {
                final Dialog dialogGenerico = new Dialog(FormModificarCliente.this);
                dialogGenerico.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogGenerico.setContentView(R.layout.dialog_generico_opcion);
                ((TextView) dialogGenerico.findViewById(R.id.txtMensaje)).setText("No es posible obtener su coordenada. Por favor intente nuevamente\n"
                        + "?Desea continuar sin capturar la coordenada?");
                ((Button) dialogGenerico.findViewById(R.id.btnAceptar)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogGenerico.cancel();

                    }
                });
                ((Button) dialogGenerico.findViewById(R.id.btnCancelar)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogGenerico.cancel();
                    }
                });
                ;
                dialogGenerico.setCancelable(false);
                dialogGenerico.show();


            }
        } else {
            Util.mostrarAlertDialog(FormModificarCliente.this, "Por favor encienda el GPS");
        }

        return estado;
    }

    /**
     * salir del formulario de modificar cliente
     * @param v
     */
    public void OnClickCancelarModificarCliente (View v) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Desea Salir de Este Formulario?").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener
                () {
            public void onClick (DialogInterface dialog, int id) {

                dialog.cancel();
                finish();
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick (DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * ocultar el teclado personalizado, cuando se presione boton back y el teclado es visible
     */
    public void onBackPressed () {

        if (customKeyboard != null && customKeyboard.isCustomKeyboardVisible())
            customKeyboard.hideCustomKeyboard();
        else
            super.onBackPressed();
    }

    public void onClickRegresar (View view) {

        finish();
    }

}
