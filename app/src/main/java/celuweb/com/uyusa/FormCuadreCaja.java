package celuweb.com.uyusa;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.CuadreCaja;

public class FormCuadreCaja extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_cuadre_caja);
		
		CargarCuadreCaja();
	}
	
	public void CargarCuadreCaja() {
		
		CuadreCaja cuadreCaja = DataBaseBO.CargarCuadreCaja("");
		
		((TextView) findViewById(R.id.lblTotalEfectivo)).setText("" + Util.SepararMilesSin("" + cuadreCaja.total_efectivo));
		((TextView) findViewById(R.id.lblTotalCheques)).setText("" + Util.SepararMilesSin("" + cuadreCaja.total_cheque));
		((TextView) findViewById(R.id.lblTotalVenta)).setText("" + Util.SepararMilesSin("" + cuadreCaja.total_venta));
	}
}
