package celuweb.com.uyusa;

import java.io.File;
import java.util.UUID;
import java.util.Vector;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;

import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.ConfigBO;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Config;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;
import celuweb.com.printer.sewoo.SewooLKP20;

public class FormMenuEstadistica extends CustomActivity implements Sincronizador {

    Dialog dialogEnviarInfo;

    Dialog dialogIniciarDia;

    Dialog dialogTerminarDia;

    Dialog dialogConfiguracion;

    Dialog dialogActualizarInventario;

    ProgressDialog progressDialog;

    Dialog dialogContrasenia;

    Vector<Usuario> liquidadores;

    private String macImpresora;

    String mensaje;

    /**
     * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
     */
    private SewooLKP20 sewooLKP20;

    ImageView btnAtras;

    ImageView btnPlusMas;

    ImageView btnSearch;

    LinearLayout lybtnAtras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_menu_estadistica);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        checkPermisos();
        // Remove default title text
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // Get access to the custom title view
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.estadisticas));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnAtras = (ImageView) toolbar.findViewById(R.id.btnAtras);
        btnPlusMas = (ImageView) toolbar.findViewById(R.id.btnPlusMas);
        btnSearch = (ImageView) toolbar.findViewById(R.id.btnSearch);
        lybtnAtras = (LinearLayout) toolbar.findViewById(R.id.lybtnAtras);
        btnAtras.setOnClickListener(botonAtrasListener);
        btnPlusMas.setOnClickListener(botonPlusAddListener);
        btnSearch.setOnClickListener(botonesSearchListener);
        lybtnAtras.setOnClickListener(lybotonAtrasListener);
        cargarInfoPedido();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.blue_dark));
        }
        cargarVersion();
    }


    @Override
    protected void onResume() {

        validarTipoUsuario();
        super.onResume();
    }


    public void OnClickEnviarInfo(View view) {

        if (DataBaseBO.HayInformacionXEnviar1()) {
            if (dialogEnviarInfo == null) {
                dialogEnviarInfo = new Dialog(this);
                dialogEnviarInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogEnviarInfo.setContentView(R.layout.dialog_enviar_info);
            }
            ((Button) dialogEnviarInfo.findViewById(R.id.btnAceptarEnviarInfo)).setOnClickListener(new OnClickListener() {
                public void onClick(View v) {

                    dialogEnviarInfo.cancel();
                    progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Enviando Informacion...", true);
                    progressDialog.show();
                    // DataBaseBO.ActualizarSnPedidosTmp();
                    Sync sync = new Sync(FormMenuEstadistica.this, Const.ENVIAR_PEDIDO);
                    sync.start();
                }
            });
            ((Button) dialogEnviarInfo.findViewById(R.id.btnCancelarEnviarInfo)).setOnClickListener(new OnClickListener() {
                public void onClick(View v) {

                    dialogEnviarInfo.cancel();
                }
            });
            dialogEnviarInfo.setCancelable(false);
            dialogEnviarInfo.show();
        } else {
            Util.MostrarAlertDialog(this, "No hay informacion pendiente por enviar");
        }
    }


    public void OnClickEfectividad(View view) {
        Usuario usuario = DataBaseBO.obtenerUsuario();
        if (usuario != null) {
            Intent intent = new Intent(this, FormEstadisticasActivity.class);
            startActivity(intent);
        } else
            Toast.makeText(this, "Por favor inicie dia primero!", Toast.LENGTH_SHORT).show();

    }


    public void OnClickActualizarInfo(View view) {

        if (DataBaseBO.HayInformacionXEnviar1()) {
            Util.MostrarAlertDialog(this, "Hay informacion pendiente por enviar. Antes de iniciar dia, por favor envie informacion");
        } else {
            Config config = ConfigBO.ObtenerConfigUsuario();
            if (config == null) {
                AlertDialog alertDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(FormMenuEstadistica.this);
                builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        MostrarDialogConfiguracion();
                    }
                });
                alertDialog = builder.create();
                alertDialog.setMessage("Para iniciar dia debe primero establecer la configuracion del usuario");
                alertDialog.show();
            } else {
                MostrarDialogIniciarDia(config);
            }
        }
    }


    public void OnClickTerminarDia(View view) {

        if (dialogTerminarDia == null) {
            dialogTerminarDia = new Dialog(this);
            dialogTerminarDia.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogTerminarDia.setContentView(R.layout.dialog_terminar_labores);
        }
        String mensaje = "<b>Esta seguro de terminar labores?</b><br /><br />";
        mensaje += "Una vez finalice labores no podra registrar mas pedidos.";
        ((TextView) dialogTerminarDia.findViewById(R.id.lblMsgTerminarDia)).setText(Html.fromHtml(mensaje));
        ((Button) dialogTerminarDia.findViewById(R.id.btnAceptarTerminarDia)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                if (DataBaseBO.HayInformacionXEnviar()) {
                    dialogTerminarDia.cancel();
                    progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Finalizando labores...", true);
                    progressDialog.show();
                    Sync sync = new Sync(FormMenuEstadistica.this, Const.TERMINAR_LABORES);
                    sync.imei = ObtenerImei();
                    sync.start();
                } else {
                    dialogTerminarDia.cancel();
                    progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Finalizando labores...", true);
                    progressDialog.show();
                    Sync sync = new Sync(FormMenuEstadistica.this, Const.TERMINAR_LABORES);
                    sync.version = ObtenerVersion();
                    sync.imei = ObtenerImei();
                    sync.start();
                }
            }
        });
        ((Button) dialogTerminarDia.findViewById(R.id.btnCancelarTerminarDia)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                dialogTerminarDia.cancel();
            }
        });
        dialogTerminarDia.setCancelable(false);
        dialogTerminarDia.show();
    }


    public void OnClickConfiguracion(View view) {

        if (DataBaseBO.HayInformacionXEnviar1()) {
            Util.MostrarAlertDialog(this, "Hay informacion pendiente por enviar.\n\nSi desea cambiar la configuracion del usuario, por favor primero envie informacion!");
        } else {
            MostrarDialogConfiguracion();
        }
    }


    public void OnClickInventario(View view) {

        if (DataBaseBO.HayInformacionXEnviar()) {
            Util.MostrarAlertDialog(this, "Hay informacion pendiente por enviar. Antes de actualizar inventario, por favor envie informacion");
        } else {
            Config config = ConfigBO.ObtenerConfigUsuario();
            if (config == null) {
                AlertDialog alertDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(FormMenuEstadistica.this);
                builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        MostrarDialogConfiguracion();
                    }
                });
                alertDialog = builder.create();
                alertDialog.setMessage("Para actualizar inventario debe primero establecer la configuracion del usuario");
                alertDialog.show();
            } else {
                MostrarDialogActualizarInventario(config);
            }
        }
    }


    public void OnClickInformeInventario(View view) {
        if (DataBaseBO.HayInformacionXEnviar1()) {
            Util.MostrarAlertDialog(FormMenuEstadistica.this, "Primero envie la informacion pendiente");
        } else {
            progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Actualizando informacion...", true);
            progressDialog.show();
            Sync sync = new Sync(FormMenuEstadistica.this, Const.ACTUALIZAR_INVENTARIO);
            sync.version = ObtenerVersion();
            sync.imei = ObtenerImei();
            sync.start();

        }

    }


    public void OnClickFiados(View view) {

        Intent intent = new Intent(this, FormFiados.class);
        startActivity(intent);
    }


    public void OnClickCuadreCaja(View view) {

        Intent intent = new Intent(this, FormCuadreCaja.class);
        startActivity(intent);
    }


    public void OnClickSalir(View view) {

        setResult(RESULT_OK);
        finish();
    }


    public void MostrarDialogIniciarDia(Config config) {

        if (dialogIniciarDia == null) {
            dialogIniciarDia = new Dialog(this);
            dialogIniciarDia.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogIniciarDia.setContentView(R.layout.dialog_iniciar_dia);
        }
        String mensaje = "<b>Usuario:</b> " + config.usuario + "<br /><br />";
        mensaje += "<b>Bodega:</b> " + config.bodega + "<br /><br />";
        mensaje += "Esta seguro de iniciar dia?<br /><br />";
        mensaje += "Iniciar dia actualizara la informacion. Toda la informacion anterior se borrara.";
        ((TextView) dialogIniciarDia.findViewById(R.id.lblMsgIniciarDia)).setText(Html.fromHtml(mensaje));
        ((Button) dialogIniciarDia.findViewById(R.id.btnAceptarIniciarDia)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                dialogIniciarDia.cancel();
                progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Descargando informacion...", true);
                progressDialog.show();
                Sync sync = new Sync(FormMenuEstadistica.this, Const.DOWNLOAD_DATA_BASE);
                sync.version = ObtenerVersion();
                sync.imei = ObtenerImei();
                sync.start();
            }
        });
        ((Button) dialogIniciarDia.findViewById(R.id.btnCancelarIniciarDia)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                dialogIniciarDia.cancel();
            }
        });
        dialogIniciarDia.setCancelable(false);
        dialogIniciarDia.show();
    }

    public void MostrarDialogConfiguracion() {

        if (dialogConfiguracion == null) {
            dialogConfiguracion = new Dialog(this);
            dialogConfiguracion.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogConfiguracion.setContentView(R.layout.dialog_configuracion);
            // dialogConfiguracion.setTitle("Configuracion");
        }
        ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).requestFocus();
        Config config = ConfigBO.ObtenerConfigUsuario();
        if (config == null) {
            ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).setText("");
            ((EditText) dialogConfiguracion.findViewById(R.id.txtBodegaConfig)).setText("");
        } else {
            ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).setText(config.usuario);
            ((EditText) dialogConfiguracion.findViewById(R.id.txtBodegaConfig)).setText(config.bodega);
        }
        ((Button) dialogConfiguracion.findViewById(R.id.btnGuardarConfig)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                String usuario = ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).getText().toString().trim();
                String bodega = ((EditText) dialogConfiguracion.findViewById(R.id.txtBodegaConfig)).getText().toString().trim();
                if (usuario.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(dialogConfiguracion.getContext());
                    builder.setMessage("Por favor ingrese el usuario").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            ((EditText) dialogConfiguracion.findViewById(R.id.txtUsuarioConfig)).requestFocus();
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    boolean guardo = ConfigBO.GuardarConfigUsuario(usuario, bodega, 1);
                    if (guardo) {
                        Config config = new Config();
                        config.usuario = usuario;
                        config.bodega = bodega;
                        dialogConfiguracion.cancel();
                        MostrarDialogIniciarDia(config);
                        Log.i("FormMenuEstadistica", "Guardo la Configuracion");
                    } else {
                        Log.e("FormMenuEstadistica", DataBaseBO.mensaje);
                    }
                }
            }
        });
        ((Button) dialogConfiguracion.findViewById(R.id.btnCancelarConfig)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                dialogConfiguracion.cancel();
            }
        });
        dialogConfiguracion.setCancelable(false);
        dialogConfiguracion.show();
        dialogConfiguracion.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_settings);
    }


    public void MostrarDialogActualizarInventario(Config config) {

        if (dialogActualizarInventario == null) {
            dialogActualizarInventario = new Dialog(this);
            dialogActualizarInventario.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialogActualizarInventario.setContentView(R.layout.dialog_iniciar_dia);
            dialogActualizarInventario.setTitle("Actualizar inventario");
        }
        String mensaje = "<b>Usuario:</b> " + config.usuario + "<br />";
        mensaje += "<b>Bodega:</b> " + config.bodega + "<br /><br />";
        mensaje += "Esta seguro de actualizar el inventario?<br />";
        ((TextView) dialogActualizarInventario.findViewById(R.id.lblMsgIniciarDia)).setText(Html.fromHtml(mensaje));
        ((Button) dialogActualizarInventario.findViewById(R.id.btnAceptarIniciarDia)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                dialogActualizarInventario.cancel();
                progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Descargando inventario...", true);
                progressDialog.show();
                Sync sync = new Sync(FormMenuEstadistica.this, Const.DOWNLOAD_DATA_BASE);
                sync.version = ObtenerVersion();
                sync.imei = ObtenerImei();
                sync.start();
            }
        });
        ((Button) dialogActualizarInventario.findViewById(R.id.btnCancelarIniciarDia)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                dialogActualizarInventario.cancel();
            }
        });
        dialogActualizarInventario.setCancelable(false);
        dialogActualizarInventario.show();
        dialogActualizarInventario.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_inventario);
    }


    public void ActualizarVersionApp() {

        final String versionSvr = DataBaseBO.ObtenerVersionApp();
        String versionApp = ObtenerVersion();
        if (versionSvr != null && versionApp != null) {
            float versionServer = Util.ToFloat(versionSvr.replace(".", ""));
            float versionLocal = Util.ToFloat(versionApp.replace(".", ""));
            if (versionLocal < versionServer) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormMenuEstadistica.this);
                builder.setMessage("Hay una version de la aplicacion: " + versionSvr).setCancelable(false).setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Descargando version " + versionSvr + "...", true);
                        progressDialog.show();
                        Sync sync = new Sync(FormMenuEstadistica.this, Const.DOWNLOAD_VERSION_APP);
                        sync.start();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                // mostrarAlertDialog(this, "Informacion Descargada
                // Correctamente");
                Util.MostrarAlertDialog(this, "Informacion descargada correctamente");
            }
        }
    }


    public String ObtenerVersion() {

        String versionApp;
        try {
            versionApp = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            versionApp = "0.0";
            Log.e("FormMenuEstadistica", "ObtenerVersion: " + e.getMessage(), e);
        }
        return versionApp;
    }


    public String ObtenerImei() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "NoPermissionToRead";
        }
        Main.deviceId = manager.getDeviceId();
        if (Main.deviceId == null) {
            WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null) {
                    String mac = wifiInfo.getMacAddress();
                    if (mac != null) {
                        Main.deviceId = mac.replace(":", "").toUpperCase();
                    }
                }
            }
        }
        return Main.deviceId;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
        switch (codeRequest) {
            case Const.DOWNLOAD_DATA_BASE:
                RespuestaDownloadInfo(ok, respuestaServer, msg);
                break;
            case Const.ENVIAR_PEDIDO:
                RespuestaEnviarInfo(ok, respuestaServer, msg);
                break;
            case Const.DOWNLOAD_VERSION_APP:
                RespuestaDownloadVersionApp(ok, respuestaServer, msg);
                break;
            case Const.ENVIAR_PEDIDO_TERMINAR:
                RespuestaEnviarInfoTerminarDia(ok, respuestaServer, msg);
                break;
            case Const.TERMINAR_LABORES:
                RespuestaTerminarLabores(ok, respuestaServer, msg);
                break;
            case Const.ACTUALIZAR_INVENTARIO:
                RespuestaActualizarInfo(ok, respuestaServer, msg);
                break;

        }
    }

    public void RespuestaActualizarInfo(boolean ok, String respuestaServer, String msg) {
        final String mensaje = ok ? "Inventario actualizado con exito!" : "No se pudo actualizar inventario.\nMotivo: Fallo de conexion.";
        if (progressDialog != null) progressDialog.cancel();
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormMenuEstadistica.this);
                builder.setMessage(mensaje).setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        Intent intent = new Intent(FormMenuEstadistica.this, FormInformeInventario.class);
                        startActivity(intent);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public void RespuestaDownloadInfo(final boolean ok, final String respuestaServer, String msg) {
        final String mensaje = ok ? "Informacion actualizada correctamente" : msg;
        if (progressDialog != null)
            progressDialog.cancel();
        this.runOnUiThread(new Runnable() {
            public void run() {
                if (DataBaseBO.CargarInfomacionUsuario()) {
                    if (ok) {
                        ActualizarVersionApp();
                    } else {
                        Util.MostrarAlertDialog(FormMenuEstadistica.this, mensaje);
                    }
                } else {
                    if (ok)
                        Util.MostrarAlertDialog(FormMenuEstadistica.this, "No se pudo iniciar dia, por favor intente nuevamente.");
                    else
                        Util.MostrarAlertDialog(FormMenuEstadistica.this, "No se pudo iniciar dia, por favor intente nuevamente.\n\nMotivo: " + respuestaServer);
                }
            }
        });
    }

    public void RespuestaEnviarInfo(boolean ok, String respuestaServer, String msg) {
        final String mensaje = ok ? "Informacion registrada con exito en el servidor" : msg;
        if (progressDialog != null)
            progressDialog.cancel();
        this.runOnUiThread(new Runnable() {
            public void run() {
                Util.MostrarAlertDialog(FormMenuEstadistica.this, mensaje);
            }
        });
    }

    public void RespuestaEnviarInfoTerminarDia(boolean ok, String respuestaServer, String msg) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Sync sync = new Sync(FormMenuEstadistica.this, Const.TERMINAR_LABORES);
                sync.start();
            }
        });
    }

    public void RespuestaTerminarLabores(boolean ok, String respuestaServer, String msg) {
        final String mensaje = ok ? "Terminar labores satisfactorio" : msg + "\n\nPor favor intente nuevamente";
        if (progressDialog != null)
            progressDialog.cancel();
        this.runOnUiThread(new Runnable() {
            public void run() {
                Util.MostrarAlertDialog(FormMenuEstadistica.this, mensaje);
            }
        });
    }

    public void RespuestaDownloadVersionApp(final boolean ok, final String respuestaServer, String msg) {
        if (progressDialog != null)
            progressDialog.cancel();
        this.runOnUiThread(new Runnable() {
            public void run() {
                if (ok) {
                    File fileApp = new File(Util.DirApp(), Const.fileNameApk);
                    if (fileApp.exists()) {
                        Uri uri = Uri.fromFile(fileApp);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, "application/vnd.android.package-archive");
                        startActivityForResult(intent, Const.RESP_ACTUALIZAR_VERSION);
                    } else {
                        Util.MostrarAlertDialog(FormMenuEstadistica.this, "No se pudo actualizar la version.");
                    }
                } else {
                    Util.MostrarAlertDialog(FormMenuEstadistica.this, respuestaServer);
                }
            }
        });
    }

    public void mostrarAlertDialog(Context context, String mensaje) {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Intent i = new Intent(FormMenuEstadistica.this, FormSeguimientoVentas.class);
                startActivity(i);
                finish();
            }
        });
        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    public void onClickInfRecaudo(View view) {
        // if (DataBaseBO.HayInformacionXEnviar1()) {
        //
        // Util.MostrarAlertDialog(this, "Hay informacion pendiente por Enviar.
        // por favor Envie Primero La Informacion");
        //
        // }else{
        Intent intent = new Intent(this, FormInfRecaudoActivity.class);
        startActivity(intent);
        // }
    }

    /**
     * lanzar actividad de info de estadisticas en el formulario web.
     *
     * @param view
     */
    public void OnClickInfoEstadisticas(View view) {
        Intent intent = new Intent(this, FormInfoEstadisticasWeb.class);
        startActivity(intent);
    }

    /**
     * mostrar seguimiento de ventas
     *
     * @param view
     */
    public void onClickSeguimientoVentas(View view) {
        Intent intent = new Intent(this, FormSeguimientoVentas.class);
        startActivity(intent);
    }

    public void onClickImpresora(View view) {
        // Intent intent = new Intent(this, FormConfigPrinter.class);
        Intent intent = new Intent(this, FormConfigPrinterNuevo.class);
        startActivity(intent);
    }

    /**
     * iniciar el activity que permite mostrar pedidos que han sido
     * sincronizados.
     *
     * @param view
     */
    public void onClickModificarPedidoSincronizado(View view) {
        // verificar que hay pedidos. sino hay pedidos. Mostrar alert informando
        // que no hay pedidos que mostrar.
        int cantPedidos = DataBaseBO.getCantPedidos();
        if (cantPedidos > 0) {
            // iniciar el activity solamente si hay pedidos que mostrar.
            Intent i = new Intent(FormMenuEstadistica.this, FormModificarPedidos.class);
            i.putExtra("sincronizado", true); // identificar que los pedidos a
            // mostrar ya fueron
            // sincronizados
            startActivity(i);
        } else {
            Util.MostrarAlertDialog(FormMenuEstadistica.this, "No hay pedidos para mostrar");
            return;
        }
    }

    public void OnClickContingencia(View view) {
        System.out.println(" contingencia ");
        celuweb.com.uyusa.Util.MostrarAlertDialog(this, "Modulo en construccion...");
    }

    /**
     * iniciar el activity que permite modificar pedidos que aun no han sido
     * sincronizados.
     *
     * @param view
     */
    public void onClickModificarPedidoNoSincronizado(View view) {
        // verificar que hay pedidos. sino hay pedidos. Mostrar alert informando
        // que no hay pedidos que mostrar.
        int cantPedidos = DataBaseBO.getCantPedidos();
        if (cantPedidos > 0) {
            // iniciar el activity solamente si hay pedidos que mostrar.
            Intent i = new Intent(FormMenuEstadistica.this, FormModificarPedidos.class);
            i.putExtra("sincronizado", false); // identificar que los pedidos a
            // mostrar no han sido
            // sincronizados
            startActivity(i);
        } else {
            Util.MostrarAlertDialog(FormMenuEstadistica.this, "No hay pedidos para mostrar");
            return;
        }
    }

    public void OnClickPedidoCargue(View view) {
        if (DataBaseBO.ExisteDataBase()) {
            DataBaseBO.CargarInfomacionUsuario();
            if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {// Solo
                // Autoventa
                boolean tieneCargue = DataBaseBO.ExisteCargue(Main.usuario.codigoVendedor);// solo
                // se
                // permite
                // un
                // cargue
                // en
                // el
                // dia
                if (!tieneCargue) {
                    DataBaseBO.crearVistaProductosAutoventa();
                    progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Cargando productos...", true);
                    progressDialog.show();
                    CargarProductos cp = new CargarProductos();
                    cp.start();
                } else {
                    if (dialogContrasenia != null) {
                        if (dialogContrasenia.isShowing()) {
                            dialogContrasenia.dismiss();
                        }
                        dialogContrasenia.cancel();
                    }
                    dialogContrasenia = new Dialog(this);
                    dialogContrasenia.setContentView(R.layout.dialog_terminar_labores);
                    dialogContrasenia.setTitle("Impresion cargue");
                    ((TextView) dialogContrasenia.findViewById(R.id.lblMsgTerminarDia)).setText("Ya registro el cargue el dia de hoy.\n\nDesea imprimir el registro?");
                    ((Button) dialogContrasenia.findViewById(R.id.btnAceptarTerminarDia)).setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            String nroDoc = DataBaseBO.obtenerNroDocCargue();
                            if (!nroDoc.equals(""))
                                imprimirTirillaCargue(nroDoc);
                            else
                                Util.mostrarToast(FormMenuEstadistica.this, "Ocurrio un error al cargar la informacion del cargue.");
                        }
                    });
                    ((Button) dialogContrasenia.findViewById(R.id.btnCancelarTerminarDia)).setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            dialogContrasenia.cancel();
                        }
                    });
                    dialogContrasenia.setCancelable(false);
                    dialogContrasenia.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = dialogContrasenia.getWindow();
                    lp.copyFrom(window.getAttributes());
                    // This makes the dialog take up the full width
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
            }
        }
    }

    protected void imprimirTirillaCargue(String nroDoc) {
        boolean isPedido = true;
        progressDialog = ProgressDialog.show(FormMenuEstadistica.this, "", "Por favor espere...\n\nProcesando informacion!", true);
        progressDialog.show();
        SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
        macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");
        SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
        String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");
        if (macImpresora.equals("-")) {
            Util.MostrarAlertDialog(FormMenuEstadistica.this, "Aun no hay impresora establecida.\n\nPor favor primero configure la impresora!");
            if (progressDialog != null)
                progressDialog.cancel();
        } else {
            if (!tipoImpresora.equals("Intermec")) {
                sewooLKP20 = new SewooLKP20(FormMenuEstadistica.this);
                imprimirSewooLKP20(macImpresora, nroDoc, "", isPedido, true);
            } else {
                imprimirTirillaGeneral(macImpresora, nroDoc, "", isPedido, true);
            }
        }
    }

    class CargarProductos extends Thread {
        public void run() {
            cargarProductos();
        }
    }

    public void cargarProductos() {
        Main.encabezado.codigo_novedad = 1;
        Main.encabezado.tipoTrans = 90;
        Main.itemsBusq = new ItemListView[]{};
        Main.codBusqProductos = "";
        Main.posOpBusqProductos = 0;
        Main.posOpLineas = 0;
        Main.primeraVez = true;
        DataBaseBO.CargarInfomacionUsuario();
        /*************************************************************
         * /////Creamos un cliente de manera temporal con el mismo codigo
         * /////del vendedor para registrarle un pedido de cargue sugerido
         *************************************************************/
        DataBaseBO.eliminarClientePropio(Main.usuario.codigoVendedor);
        Cliente clientePropio = new Cliente();
        clientePropio.codigo = Main.usuario.codigoVendedor;
        clientePropio.Nombre = "Vendedor: " + Main.usuario.nombreVendedor;
        clientePropio.razonSocial = "Vendedor: " + Main.usuario.nombreVendedor;
        // clientePropio.portafolio = DataBaseBO.ObtenerPortafolioDeClientes();
        clientePropio.portafolio = "('L1')";
        clientePropio.CodigoAmarre = DataBaseBO.ObtenerCodigoAmarreDeClientes();
        DataBaseBO.insertarInformacionClientePropio(clientePropio);
        DataBaseBO.GuardarCodPdv(clientePropio.codigo, 1);
        Cliente cliente = DataBaseBO.CargarClienteSeleccionado();
        Main.cliente = cliente;
        /****************************************************************/
        Main.encabezado.hora_inicial = Main.usuario.fechaLabores.replace('/', '-') + " " + Util.ObtenerHora();
        DataBaseBO.organizarTmpDescuentos();
        DataBaseBO.organizarTmpProductos(cliente.portafolio, Const.AUTOVENTA);
        DataBaseBO.ListarProductos(cliente.CodigoAmarre);
        if (progressDialog != null)
            progressDialog.dismiss();
        Intent formPedido = new Intent(this, FormPedidoActivity.class);
        formPedido.putExtra("cambio", false);
        formPedido.putExtra("esCargue", true);// es cargue
        startActivityForResult(formPedido, Const.RESP_PEDIDO_EXITOSO);
    }

    public void OnClickLoginLiquidar(View view) {
        if (DataBaseBO.HayInformacionXEnviar1()) {
            Util.MostrarAlertDialog(this, "Hay informacion pendiente por Enviar. \nAntes de querer liquidar, por favor Envie Informacion");
        } else {
            int diaActual = Util.ObtenerDiaSemanaNumeroEntero();
            String diaRuta2 = String.valueOf(cargaRuta2(diaActual));
            Vector<Cliente> listaClientes = DataBaseBO.ListaClientesRuteroAux(diaRuta2);
            if (listaClientes.size() > 0) {
                Util.MostrarAlertDialog(this, "Por favor Termine El Rutero\nClientes En El Rutero: " + listaClientes.size());
            } else {
                mostrarDialogoLogin();
            }
        }
    }

    public void mostrarDialogoLogin() {
        if (dialogContrasenia != null) {
            if (dialogContrasenia.isShowing()) {
                dialogContrasenia.dismiss();
            }
            dialogContrasenia.cancel();
        }
        dialogContrasenia = new Dialog(this);
        dialogContrasenia.setContentView(R.layout.dialog_login);
        dialogContrasenia.setTitle("Login");
        EditText etClave = (EditText) dialogContrasenia.findViewById(R.id.etClave);
        etClave.setText("");
        Spinner spUsuarios = (Spinner) dialogContrasenia.findViewById(R.id.spUsuarios);
        String[] items;
        Vector<String> listaItems = new Vector<String>();
        liquidadores = DataBaseBO.ListaLiquidadores(listaItems, true);
        if (listaItems.size() > 0) {
            items = new String[listaItems.size()];
            listaItems.copyInto(items);
        } else {
            items = new String[]{};
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUsuarios.setAdapter(adapter);
        ((Button) dialogContrasenia.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (liquidadores.size() > 0) {
                    EditText etClave = (EditText) dialogContrasenia.findViewById(R.id.etClave);
                    if (etClave.getText().toString().equals("")) {
                        Util.mostrarToast(FormMenuEstadistica.this, "Ingrese Contraseña");
                        etClave.requestFocus();
                    } else {
                        Spinner spUsuarios = (Spinner) dialogContrasenia.findViewById(R.id.spUsuarios);
                        String usuario = liquidadores.elementAt(spUsuarios.getSelectedItemPosition()).codigoVendedor;
                        String nombreLiquidador = liquidadores.elementAt(spUsuarios.getSelectedItemPosition()).nombreVendedor;
                        String clave = etClave.getText().toString();
                        if (DataBaseBO.LogInLiquidadores(usuario, clave, true)) {
                            Usuario liquidador = new Usuario();
                            liquidador.codigoVendedor = usuario;
                            liquidador.nombreVendedor = nombreLiquidador;
                            dialogContrasenia.cancel();
                            Intent intent = new Intent(FormMenuEstadistica.this, FormLiquidarOpcionesActivity.class);
                            intent.putExtra("liquidador", liquidador);
                            startActivityForResult(intent, Const.RESP_FORM_INICIAR_DIA);
                        } else {
                            Util.MostrarAlertDialog(FormMenuEstadistica.this, "Ocurrio un error al ingresar");
                        }
                    }
                } else {
                    Util.mostrarToast(FormMenuEstadistica.this, "Seleccione un Usuario");
                }
            }
        });
        ((Button) dialogContrasenia.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialogContrasenia.cancel();
            }
        });
        dialogContrasenia.setCancelable(false);
        dialogContrasenia.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogContrasenia.getWindow();
        lp.copyFrom(window.getAttributes());
        // This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    /**
     * Imprimir la factura del pedido.
     *
     * @param macImpresora
     * @param numero_doc
     * @param copiaPrint
     */
    protected void imprimirSewooLKP20(final String macImpresora, final String numero_doc, final String copiaPrint, final boolean isPedido, final boolean isCargue) {
        final Usuario usuario = DataBaseBO.ObtenerUsuario();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                if (macImpresora.equals("-")) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(FormMenuEstadistica.this, "Aun no hay impresora predeterminada.\n\nPor favor primero configure la impresora!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    if (sewooLKP20 == null) {
                        sewooLKP20 = new SewooLKP20(FormMenuEstadistica.this);
                    }
                    int conect = sewooLKP20.conectarImpresora(macImpresora);
                    switch (conect) {
                        case 1:
                            sewooLKP20.generarEncabezadoTirilla(numero_doc, usuario, copiaPrint, isPedido, false, isCargue, 0, false);
                            sewooLKP20.imprimirBuffer(true);
                            break;
                        case -2:
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(FormMenuEstadistica.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                        case -8:
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Util.MostrarAlertDialog(FormMenuEstadistica.this, "Bluetooth apagado. Por favor habilite el bluetooth para imprimir.");
                                }
                            });
                            break;
                        default:
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(FormMenuEstadistica.this, "Error desconocido, intente nuevamente.", Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                    }
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (sewooLKP20 != null) {
                        sewooLKP20.desconectarImpresora();
                        if (progressDialog != null)
                            progressDialog.dismiss();
                    }
                }
                Looper.myLooper().quit();
            }
        }).start();
    }

    private void imprimirTirillaGeneral(final String macAddress, final String numeroDoc, final String copiaPrint, final boolean isPedido, final boolean isCargue) {
        new Thread(new Runnable() {
            public void run() {
                mensaje = "";
                BluetoothSocket socket = null;
                try {
                    Looper.prepare();
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (bluetoothAdapter == null) {
                        mensaje = "No hubo conexion con la impresora.\n\nPor favor intente de nuevo.";
                    } else if (!bluetoothAdapter.isEnabled()) {
                        mensaje = "No hubo conexion con la impresora.\n\nPor favor intente de nuevo.";
                    } else {
                        BluetoothDevice printer = null;
                        printer = bluetoothAdapter.getRemoteDevice(macAddress);
                        if (printer == null) {
                            mensaje = "No se pudo establecer la conexion con la impresora.";
                        } else {
                            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                            SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                            String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");
                            if (tipoImpresora.equals("Intermec")) {
                                socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);
                            } else {
                                socket = printer.createRfcommSocketToServiceRecord(uuid);
                            }
                            if (socket != null) {
                                socket.connect();
                                Thread.sleep(3500);
                                if (tipoImpresora.equals("Intermec")) {
                                    ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoCargueImpresoraIntermec(numeroDoc));
                                }
                                handlerFinish.sendEmptyMessage(0);
                            } else {
                                mensaje = "No se pudo abrir la conexion con la impresora.\n\nPor favor intente de nuevo.";
                            }
                        }
                    }
                    if (!mensaje.equals("")) {
                        handlerFinish.sendEmptyMessage(0);
                    }
                    Looper.myLooper().quit();
                } catch (Exception e) {
                    String motivo = e.getMessage();
                    mensaje = "No se pudo ejecutar la Impresion.";
                    if (motivo != null) {
                        mensaje += "\n\n" + motivo;
                    }
                    handlerFinish.sendEmptyMessage(0);
                } finally {
                }
            }
        }).start();
    }

    private Handler handlerFinish = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (progressDialog != null)
                progressDialog.cancel();
            if (!mensaje.equals("")) {
                Util.MostrarAlertDialog(FormMenuEstadistica.this, mensaje);
            } else {
                dialogContrasenia.cancel();
            }
        }
    };

    private int cargaRuta2(int dia) {
        int rutaNueva = 0;
        switch (dia) {
            case 0:
                rutaNueva = 10;
                break;
            case 1:
                rutaNueva = 4;
                break;
            case 2:
                rutaNueva = 5;
                break;
            case 3:
                rutaNueva = 6;
                break;
            case 4:
                rutaNueva = 7;
                break;
            case 5:
                rutaNueva = 8;
                break;
            case 6:
                rutaNueva = 9;
                break;
            case 7:
                rutaNueva = 11;
                break;
            default:
                rutaNueva = 10;
        }
        return rutaNueva;
    }

    private void cargarInfoPedido() {
        final ProgressDialog progress = ProgressDialog.show(this, "Validando", "Validando informacion sin enviar...", true);
        progress.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                /* generar las acciones en hilo principal de views */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progress != null) {
                            progress.dismiss();
                        }
                        new Task().execute("");
                        //
                    }
                });
            }
        }).start();
    }

    private class Task extends AsyncTask<String, Integer, Long> {
        protected Long doInBackground(String... urls) {
            try {
                Thread.sleep(300);
            } catch (Exception e) {
            }
            FormMenuEstadistica.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (DataBaseBO.CargarTotalPedidosSinEnviar()) {
                        Util.mostrarDialogGeneral(FormMenuEstadistica.this, "ATENCION", "Existen 5 o mas pedidos por enviar.\nSe recomienda enviar informacion.");
                    }
                }
            });
            return 0l;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
            FormMenuEstadistica.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                }
            });
        }
    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    OnClickListener botonAtrasListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            finish();
        }
    };
    OnClickListener botonPlusAddListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            Util.mostrarToast(FormMenuEstadistica.this, "Evento De Boton Mas");
        }
    };
    OnClickListener botonesSearchListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            Util.mostrarToast(FormMenuEstadistica.this, "Evento De Boton Search");
        }
    };
    OnClickListener lybotonAtrasListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            finish();
        }
    };

    private void validarTipoUsuario() {
        Usuario user = DataBaseBO.CargarUsuario();
        if (user != null) {
            if (!user.tipoVenta.equals(Const.AUTOVENTA)) {
                ((LinearLayout) findViewById(R.id.trBotonesAutoventa)).setVisibility(View.GONE);
            }
        }
    }

    private void checkPermisos() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.REQUEST_INSTALL_PACKAGES) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "REQUEST_INSTALL_PACKAGES-> ");
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.REQUEST_INSTALL_PACKAGES) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.REQUEST_INSTALL_PACKAGES)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.REQUEST_INSTALL_PACKAGES}, 1);
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.REQUEST_INSTALL_PACKAGES}, 1);
                }
            }
        }
    }

    public void cargarVersion() {
        String version = Util.obtenerVersion(this);
        ((TextView) findViewById(R.id.txtVersion)).setText("Version " + version);
    }





    /**
     * Metodos para capturar coordenadas y guardarlas
     **/


    /***
     *
     * @return
     */
    /***
     public boolean obtenerCoordenada() {

     boolean estado = false;
     GPSTracker gpsTracker = new GPSTracker(this);
     Location location = gpsTracker.getLocation();
     boolean estadoGPS = gpsTracker.isEstadoGPS();
     double lati = 0;
     double longi = 0;
     //String direccionCapturada = "Direccion Aprox Capturada: ";
     //String ciudadCapturada = "Ciudad Capturada: ";
     if (location != null && estadoGPS) {
     lati = location.getLatitude();
     longi = location.getLongitude();
     //direccionCapturada += gpsTracker.getAddress(location);
     //ciudadCapturada += gpsTracker.getCity(location);
     }
     /**
     Log.i("***", " coordenadas capturada LAT: " + lati + " LONG: " + longi);

     if (estadoGPS) {
     if (lati != 0 && longi != 0) {
     Coordenada coordenada = new Coordenada();
     coordenada.codigoVendedor = Main.usuario.codigoVendedor;
     coordenada.codigoCliente = "0";
     coordenada.latitud = latitud;
     coordenada.longitud = longitud;
     coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
     coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
     coordenada.id = Coordenada.obtenerId(Main.cliente.codigo);
     latitud = lati;
     longitud = longi;
     boolean guardo = Coordenada.save(FormPrincipalActivity.this, coordenada);
     DataBaseBO.guardarCoordenada(coordenada, Main.cliente.codigo);
     if (guardo) {
     Log.i("GUARDO", "OK");
     /*  Toast.makeText(getApplicationContext(), "Coordenadas Obtenidas con exito \n"+
     lati+"\n"+longi, Toast.LENGTH_LONG).show();*/

    /**
     }
     estado = true;
     } else {

     estado = false;
     }
     } else {
     Util.mostrarAlertDialog(FormPrincipalActivity.this, "Por favor encienda el GPS");
     }

     return estado;
     }**/

    /**
     public static boolean estaGPSEncendido(Context context) {
     LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
     boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
     return statusOfGPS;
     }

     private void mostrarMensajeActivarGPS() {
     AlertDialog.Builder builder = new AlertDialog.Builder(FormPrincipalActivity.this);
     builder.setMessage("El GPS est? desactivado. Por favor act?velo").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

     public void onClick(DialogInterface dialog, int id) {
     Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
     startActivityForResult(myIntent, 1100);
     dialog.cancel();
     }
     });
     AlertDialog alert = builder.create();
     alert.show();

     }

     public void MostrarAlertDialogSinCoordenada() {

     runOnUiThread(new Runnable() {

    @Override
    public void run() {

    String mensaje = "";
    String bton = "";

    mensaje = "No se obtuvieron las Coordenadas,Desea Continuar sin Coordenadas";
    bton = "Continuar Sin Coordenada";

    AlertDialog.Builder builder = new AlertDialog.Builder(FormPrincipalActivity.this);
    builder.setCancelable(false).setPositiveButton(bton, new DialogInterface.OnClickListener() {

    public void onClick(DialogInterface dialog, int id) {

    dialog.cancel();
    }
    });
    builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

    @Override
    public void onClick(DialogInterface arg0, int arg1) {
    arg0.cancel();

    }
    });

    alertDialog = builder.create();


    alertDialog.setMessage(mensaje);
    alertDialog.show();

    }
    });


     }

     public void lanzarTimerGPS(String provider) {

     //Se guarda el Provider para Captura de Coordenadas
     setGPSProvider(provider);

     Timer timer = new Timer();
     taskGPS = new FormPrincipalActivity.TaskGPS();
     timer.schedule(taskGPS, 0, time);
     }

     public void setGPSProvider(String provider) {
     SharedPreferences settings = getSharedPreferences("settings_gps", MODE_PRIVATE);
     SharedPreferences.Editor editor = settings.edit();
     editor.putString("provider", provider);
     editor.commit();
     }

     private class TaskGPS extends TimerTask {

     public void run() {

     if (handlerGPS != null)
     handlerGPS.sendEmptyMessage(0);
     }
     }

     private Handler handlerGPS = new Handler() {

    @Override
    public void handleMessage(Message msg) {
    if (latitud == 0 && longitud == 0) {
    MostrarAlertDialogSinCoordenada();
    } else {
    obtenerCoordenada();
    }
    }
    };**/


}
