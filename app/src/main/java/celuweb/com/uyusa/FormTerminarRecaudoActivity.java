package celuweb.com.uyusa;

import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.core.app.ActivityCompat;

import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.DataObject.RecibosRecaudo;
import celuweb.com.DataObject.Usuario;
import celuweb.com.DataObject.objeto;
import celuweb.com.preferences.PreferencesDatosCliente;
import celuweb.com.printer.sewoo.SewooLKP20;

@SuppressLint("NewApi")
public class FormTerminarRecaudoActivity extends Activity implements Sincronizador {

    /**
     * @see android.app.Activity#onCreate(Bundle)
     */

    ProgressDialog progressDialog;

    long tiempoClick1 = 0;

    float total_recaudo = 0;

    Vector<FormaPago> listaFormaPago;

    private String macImpresora;

    String mensaje;

    Dialog dialogPedidoRegistrado;

    RecibosRecaudo recibosRecaudo;

    String codCliente = "";

    double totalRecuado;

    Cliente cliente;
    private int OPSEL;
    private String nroRecibo;
    private String nrodoc;

    /**
     * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
     */
    private SewooLKP20 sewooLKP20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_resumen_recaudo);

        inicializar();
        cargarTablaFacturas();
        CargarTablaFormasPago();
        //Util.turnOnBluetooth();

        Util.closeTecladoStartActivity(this);
    }


    private void inicializar() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            //			if (bundle.containsKey("totalRecuado"))
            //				totalRecuado = bundle.getFloat("totalRecuado");

            //if (bundle.containsKey("codigoCliente"))
            //	codCliente = bundle.getString("codigoCliente");
            OPSEL = bundle.getInt("formaPago");
            nroRecibo = bundle.getString("nrodoc");
        }

    }


    public void cargarTablaFacturas() {

        double total_recaudo = 0;
        double saldo = 0;

        TableLayout table = new TableLayout(this);
        table.setBackgroundColor(Color.WHITE);

        String[] cabecera = {getString(R.string.doc_), getString(R.string.saldo), getString(R.string.recaudo)};
        Util.Headers(table, cabecera, this);

        HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollResumen1);
        scroll.removeAllViews();
        scroll.addView(table);

        TextView textViewAux;

        //		Vector<Cartera> listadoCarteraRecaudar = DataBaseBO
        //				.CargarCarteraRecaudo(codCliente);

        for (Cartera cartera : Main.cartera) {

            TableRow fila = new TableRow(this);

            //			total_recaudo += cartera.valorARecaudar-cartera.valorProntoPago;
            total_recaudo += cartera.valorARecaudar;
            //String strValorARecaudar = "" + cartera.valorARecaudar;
            //strValorARecaudar = Util.QuitarE(strValorARecaudar.replace("e+", "E"));


            textViewAux = new TextView(this);
            textViewAux.setText(cartera.documento + "\n");
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            textViewAux.setTextSize(12);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);

            String strSaldo;
            cartera.saldo = Util.round((float) cartera.saldo, 2);
            String strMonto = "" + cartera.saldo;
            strSaldo = Util.QuitarE(strMonto.replace("e+", "E"));

            textViewAux = new TextView(this);
            textViewAux.setText(Util.SepararMiles(Util.Redondear(strSaldo, 2)));
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            textViewAux.setTextSize(12);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);

            String strValorARecaudar;
            cartera.valorARecaudar = Util.round((float) cartera.valorARecaudar, 2);
            String strTotal = "" + cartera.saldo;
            strValorARecaudar = Util.QuitarE(strTotal.replace("e+", "E"));

            textViewAux = new TextView(this);
            textViewAux.setText(Util.SepararMiles(Util.Redondear(strValorARecaudar, 2)));
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            textViewAux.setTextSize(12);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);

            table.addView(fila);

        }

        Main.total_recaudo = total_recaudo;
        EditText txtTotalRecaudo = ((EditText) (findViewById(R.id.txtTotalFacturass)));
        String strtotal_recaudo = total_recaudo + "";
        strtotal_recaudo = Util.QuitarE(strtotal_recaudo.replace("e+", "E"));
        txtTotalRecaudo.setText(Util.SepararMiles(Util.Redondear(strtotal_recaudo, 2)));
    }


    public void CargarTablaFormasPago() {

        //listaFormaPago = DataBaseBO.CargarFormasPago(nroDoc);

        TableLayout table = new TableLayout(this);
        table.setBackgroundColor(Color.WHITE);

        HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollResumen2);
        scroll.removeAllViews();
        scroll.addView(table);

        if (Main.listaFormasPago.size() > 0) {

            String[] headers = {getString(R.string._pago_), getString(R.string._forma_de_pago_)};
            Util.Headers(table, headers, this);

            TextView textViewAux;


            for (FormaPago formaPago : Main.listaFormasPago) {

                TableRow fila = new TableRow(this);


                String strMonto = "" + formaPago.monto;
                strMonto = Util.QuitarE(strMonto.replace("e+", "E"));

                textViewAux = new TextView(this);
                textViewAux.setText(Util.SepararMiles(Util.Redondear(strMonto, 2)));
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                textViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(formaPago.descripcion);
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                textViewAux.setTextSize(19);
                //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                textViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
                fila.addView(textViewAux);

                table.addView(fila);
            }

            EditText txtTotalFP = ((EditText) (findViewById(R.id.txtTotalFormaPagoo)));

            float totalPagos = TotalFormasPago();
            String strTotalPagos = "" + totalPagos;
            strTotalPagos = Util.QuitarE(strTotalPagos.replace("e+", "E"));

            txtTotalFP.setText(Util.SepararMilesSin(Util.Redondear(strTotalPagos, 2)));
        }
    }


    public void OnClikCancelarFormaPago2(View view) {
        /*Borrar los datos generados en las tablas temporales*/
        DataBaseBO.borrarTempFormaPagoRecaudos();
        finish();
        //		mostrarMensaje("Est� seguro de cancelar el recaudo?");

    }


    public void onClickTerminarFormaPago2(View view) {

        guardarRecaudo();
    }


    public void mostrarMensaje(String mensaje) {

        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {


                DataBaseBO.borrarTempFormaPagoDetalleRecaudos();
                eliminarRegRecaudo();
                finish();
                Intent intent = new Intent(FormTerminarRecaudoActivity.this, FormInfoClienteActivity.class);
                startActivityForResult(intent, Const.RESP_ELIMINAR_REGISTRO_RECAUDO);

                //
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();

    }


    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

        final String mensaje = ok ? getString(R.string.msg_informacion_exito_servidor) : msg;

        if (progressDialog != null)
            progressDialog.cancel();

        this.runOnUiThread(new Runnable() {

            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(FormTerminarRecaudoActivity.this);
                builder.setMessage(mensaje)

                        .setCancelable(false).setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        Main.encabezado = new Encabezado();
                        Main.detallePedido.clear();

                        dialog.cancel();

                        FormTerminarRecaudoActivity.this.setResult(RESULT_OK);
                        FormTerminarRecaudoActivity.this.finish();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }


    public void guardarRecaudo() {

        int formaPago = 0;

        Date date = new Date();

        long tiempoClick2 = date.getTime();

        long dif1 = tiempoClick2 - tiempoClick1;

        if (dif1 < 0) {

            dif1 = dif1 * (-1);
        }

        if (dif1 > 2000) {

            tiempoClick1 = tiempoClick2;


            Bundle bundle = getIntent().getExtras();

            if (bundle != null) {

                if (bundle.containsKey("nroRecibo"))
                    nroRecibo = bundle.getString("nroRecibo");

                //extraer opcion de forma pago. (cheque, efectivo, consignacion o cheque postfechado.)
                if (bundle.containsKey("formaPago"))
                    formaPago = bundle.getInt("formaPago");

                if (bundle.containsKey("nrodoc"))
                    nrodoc = bundle.getString("nrodoc");


            }

            nroRecibo = "1";

            if (nroRecibo != null) {

                String imei = obtenerImei();
                //				int consecutivo = obtenerConsecutivo();
                double totalRecaudo = Main.total_recaudo;
                float totalFP = TotalFormasPago();

                if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {

                    DataBaseBO.CargarInfomacionUsuario();

                }


                cliente = DataBaseBO.CargarClienteSeleccionado();

                final String consecutivo = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor);
                //				double totalRecaudo = total_recaudo;
                //				float totalFP = TotalFormasPago();

                //				boolean guardo = DataBaseBO.guardarRecaudoFormasDePagoNuevo(nroRecibo, totalRecaudo, consecutivo, totalFP);
                boolean guardo = DataBaseBO.guardarRecaudoFormasDePagoNuevo(nrodoc, totalRecaudo, totalFP, imei, nroRecibo);

                if (guardo) {

                    if (formaPago != 5) {
                        double nuevoCupo = (double) Main.cliente.Cupo + Main.total_recaudo;
                        String codigoCliente = PreferencesDatosCliente.getCodigoCliente(FormTerminarRecaudoActivity.this);
                        DataBaseBO.liberarCupoCliente(nuevoCupo, codigoCliente);
                    }

                    String complemento = getString(R.string.registrado_con_exito_para_el_cliente_) + cliente.Nombre;
                    mostrarDialogPedidoRegistrado(getString(R.string.recaudo) + " " + complemento, consecutivo);


                } else {

                    alertTerminarRecaudo(getString(R.string.no_se_pudo_guardar_recaudo_) + DataBaseBO.mensaje);
                    //alertTerminarRecaudo(getString(R.string.no_se_pudo_guardar_recaudo_));
                }

            } else {

                alertTerminarRecaudo(getString(R.string.no_se_pudo_leer_recaudo));
            }
        }
    }


    public void mostrarDialogPedidoRegistrado(String mensaje, final String nroDoc) {

        if (dialogPedidoRegistrado == null) {

            dialogPedidoRegistrado = new Dialog(this);
            dialogPedidoRegistrado.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogPedidoRegistrado.setContentView(R.layout.dialog_pedido_registrado);


            dialogPedidoRegistrado.setTitle(R.string.recaudo);

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

                public void onClick(View view) {

                    if (Main.usuario.codigoVendedor == null)
                        DataBaseBO.CargarInfomacionUsuario();

                    Main.total_recaudo = 0;
                    Main.total_descuento = 0;
                    Main.total_forma_pago = 0;

                    Main.cartera.removeAllElements();
                    Main.listaDescuentos.clear();
                    Main.listaFormasPago.clear();

                    DataBaseBO.borrarObjeto();


                    //					progressDialog = ProgressDialog.show(FormTerminarRecaudoActivity.this, "", "Enviando Informacion ...", true);
                    //					progressDialog.show();

                    //					Main.encabezado = new Encabezado();
                    dialogPedidoRegistrado.cancel();

                    //					Sync sync = new Sync(FormTerminarRecaudoActivity.this, Const.ENVIAR_PEDIDO);
                    //					sync.start();
                    FormTerminarRecaudoActivity.this.setResult(RESULT_OK);
                    FormTerminarRecaudoActivity.this.finish();
                }
            });

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setOnClickListener(new OnClickListener() {

                public void onClick(View view) {

                    progressDialog = ProgressDialog.show(FormTerminarRecaudoActivity.this, "", getString(R.string.por_favor_espere_procesando_informacion_), true);
                    progressDialog.show();

                    SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                    macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

                    SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                    String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");

                    if (macImpresora.equals("-")) {

                        if (progressDialog != null)
                            progressDialog.cancel();

                        Util.MostrarAlertDialog(FormTerminarRecaudoActivity.this, getString(R.string.msg_no_hay_impresora));


                    } else {

                        if (!tipoImpresora.equals("Intermec")) {
                            sewooLKP20 = new SewooLKP20(FormTerminarRecaudoActivity.this);
                            imprimirSewooLKP20(macImpresora, nroDoc, "", true);
                        } else {
                            imprimirTirillaGeneral(macImpresora, nroRecibo, nroDoc);
                        }


                    }
                }
            });
        }


        SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
        String macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

        if (macImpresora.equals("-")) {

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.GONE);

            String msg = mensaje + "<br /><br />" + getString(R.string.msg_no_hay_impresora);
            ((TextView) dialogPedidoRegistrado.findViewById(R.id.lblMsg)).setText(Html.fromHtml(msg));

        } else {

            ((Button) dialogPedidoRegistrado.findViewById(R.id.btnImprimir)).setVisibility(Button.VISIBLE);
            ((TextView) dialogPedidoRegistrado.findViewById(R.id.lblMsg)).setText(mensaje);
        }


        dialogPedidoRegistrado.setCancelable(false);
        dialogPedidoRegistrado.show();
        //dialogPedidoRegistrado.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_terminar);
    }


    private void imprimirTirillaGeneral(final String macAddress, final String nroRecibo, final String numeroDoc) {

        new Thread(new Runnable() {

            public void run() {

                mensaje = "";
                BluetoothSocket socket = null;

                try {

                    Looper.prepare();

                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                    if (bluetoothAdapter == null) {

                        mensaje = getString(R.string.sin_conexion_impresora_intente_de_nuevo_);

                    } else if (!bluetoothAdapter.isEnabled()) {

                        mensaje = getString(R.string.sin_conexion_impresora_intente_de_nuevo_);

                    } else {

                        BluetoothDevice printer = null;


                        printer = bluetoothAdapter.getRemoteDevice(macAddress);

                        if (printer == null) {

                            mensaje = getString(R.string.no_se_pudo_establecer_la_conexion_con_la_impresora_);

                        } else {

                            int state = printer.getBondState();

                            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

                            SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
                            String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");

                            if (tipoImpresora.equals("Intermec")) {

                                socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);
                            } else {
                                socket = printer.createRfcommSocketToServiceRecord(uuid);
                            }

                            if (socket != null) {

                                socket.connect();

                                Thread.sleep(3500);

                                if (tipoImpresora.equals("Intermec")) {

                                    ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoFormasDPagoItermec(numeroDoc));

                                } else {
                                    ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoFormasDPago(numeroDoc));
                                }


                                handlerFinish.sendEmptyMessage(0);

                            } else {

                                mensaje = getString(R.string.no_se_pudo_establecer_la_conexion_con_la_impresora_);
                            }
                            //
                            //							} else {
                            //
                            //								mensaje = "La Impresora: "
                            //										+ macAddress
                            //										+ " Actualmente no esta Acoplada con el Dispositivo Movil.\n\nPor Favor configure primero la impresora.";

                            //							}
                        }
                    }

                    if (!mensaje.equals("")) {


                        handlerFinish.sendEmptyMessage(0);
                    }

                    Looper.myLooper().quit();

                } catch (Exception e) {

                    String motivo = e.getMessage();

                    mensaje = getString(R.string.no_se_pudo_ejecutar_la_impresion_);

                    if (motivo != null) {
                        mensaje += "\n\n" + motivo;
                    }


                    handlerFinish.sendEmptyMessage(0);

                } finally {

                    try {


                    } catch (Exception e) {

                    }
                }
            }

        }).start();


    }


    private Handler handlerFinish = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (progressDialog != null)
                progressDialog.cancel();
        }
    };


    public String obtenerImei() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "NoPermissionToRead";
        }
        String deviceId = manager.getDeviceId();

        if (deviceId == null) {

            WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            if (wifiManager != null) {

                WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                if (wifiInfo != null) {

                    String mac = wifiInfo.getMacAddress();

                    if (mac != null) {

                        deviceId = mac.replace(":", "").toUpperCase(Locale.getDefault());
                    }
                }
            }
        }

        return deviceId;
    }


    public int obtenerConsecutivo() {

        SharedPreferences settings = getSharedPreferences(Const.SETTINGS_RECAUDO, 0);
        SharedPreferences.Editor editor = settings.edit();
        int consecutivo = settings.getInt(Const.CONSECUTIVO_RECAUDO, 0);
        editor.putInt(Const.CONSECUTIVO_RECAUDO, (consecutivo + 1) % 9999);
        editor.commit();

        return consecutivo;
    }


    public void alertTerminarRecaudo(String mensaje) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }


    public float TotalFormasPago() {

        float total = 0;
        for (FormaPago formaPago : Main.listaFormasPago) {

            total += formaPago.monto;

        }
        return total;
    }


    @Override
    protected void onResume() {
        super.onResume();
        refrescarDatosRecaudo();
    }


    public void refrescarDatosRecaudo() {


        if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {
            DataBaseBO.CargarInfomacionUsuario();
        }


        if (Main.cliente == null || Main.cliente.codigo == null) {

            int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;

            if (tipoDeClienteSelec == 1) {

                clienteSel = DataBaseBO.CargarClienteSeleccionado();

            } else {
                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();
            }
            if (clienteSel != null)
                Main.cliente = clienteSel;
        }


        objeto obj = DataBaseBO.obtenerObjeto();

        if (obj != null) {

            Main.cartera = obj.cartera;
            Main.listaFormasPago = obj.listaFormasPago;
            Main.total_forma_pago = obj.total_forma_pago;
            Main.total_descuento = obj.total_descuento;
            Main.total_recaudo = obj.total_recaudo;

        }

        cargarTablaFacturas();
        CargarTablaFormasPago();
    }

    /**
     * Imprimir la factura del pedido.
     *
     * @param macImpresora
     * @param numero_doc
     * @param copiaPrint
     */
    protected void imprimirSewooLKP20(final String macImpresora, final String numero_doc, final String copiaPrint, final boolean isPedido) {

        final Usuario usuario = DataBaseBO.ObtenerUsuario();


        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();

                if (macImpresora.equals("-")) {
                    if (progressDialog != null)
                        progressDialog.dismiss();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(FormTerminarRecaudoActivity.this, getString(R.string.msg_no_hay_impresora),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {

                    if (sewooLKP20 == null) {
                        sewooLKP20 = new SewooLKP20(FormTerminarRecaudoActivity.this);
                    }
                    int conect = sewooLKP20.conectarImpresora(macImpresora);

                    switch (conect) {
                        case 1:
                            sewooLKP20.generarRecaudoTirilla(numero_doc, usuario, isPedido);
                            sewooLKP20.imprimirBuffer(true);
                            break;

                        case -2:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormTerminarRecaudoActivity.this, "-2 " + R.string.fallo_conexion, Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;

                        case -8:
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Util.MostrarAlertDialog(FormTerminarRecaudoActivity.this, getString(R.string.bluetooth_apagado_por_favor_habilite_el_bluetoth_para_imprimir_));
                                }
                            });
                            break;

                        default:
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    Toast.makeText(FormTerminarRecaudoActivity.this, R.string.error_desconocido_intente_nuevamente_, Toast.LENGTH_SHORT).show();
                                }
                            });
                            break;
                    }

                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (sewooLKP20 != null) {
                        sewooLKP20.desconectarImpresora();
                        if (progressDialog != null)
                            progressDialog.dismiss();
                    }
                }
                Looper.myLooper().quit();
            }
        }).start();
    }

    public void eliminarRegRecaudo() {

        Main.total_recaudo = 0;
        Main.total_descuento = 0;
        Main.total_forma_pago = 0;

        Main.cartera.clear();
        Main.listaDescuentos.clear();
        Main.listaFormasPago.clear();

        Main.consignacion = false;

        DataBaseBO.borrarObjeto();
        Main.cartera.removeAllElements();
    }
}
