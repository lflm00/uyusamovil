package celuweb.com.uyusa;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.Vector;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.InputType;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.EncuestasBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.FotoEncuesta;
import celuweb.com.DataObject.ParametroPregunta;
import celuweb.com.DataObject.Pregunta;
import celuweb.com.DataObject.Respuesta;
import celuweb.com.DataObject.Usuario;
import android.view.ViewGroup.LayoutParams;
public class FormEncuestasPreguntasActivity extends CustomActivity implements View.OnClickListener,Sincronizador {

	int codEncuesta;
	int codPrimeraPregunta;
	String numDocEncuesta;
	boolean encuestaTerminada;

	FrameLayout fLayout;
	LinearLayout llPreguntas;

	TextView lblTituloPregunta;

	EditText txtPreguntaAbierta;

	Button btnFecha, btnFoto;

	ImageView imagenCapturada;

	CheckBox[] chcPreguntaSelMultiple;

	RadioGroup rgPreguntaSelUnica;
	RadioButton[] rbPreguntaSelUnica;

	DatePicker dpFecha;


	Pregunta pregunta;
	ParametroPregunta parametroPregunta;
	Vector<ParametroPregunta> listaParametrosPregunta;
	Vector<Respuesta> listaRespuestasPregunta;

	Drawable drwFecha;
	Drawable drwFoto;
	String nombreFoto;
	Bitmap foto;

	Cliente cliente;
	Usuario usuario;

	float widhtThumb = 500;
	float heightThumb = 500;

	private ProgressDialog progressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_encuestas_preguntas);

		codEncuesta = 0;
		codPrimeraPregunta = 0;
		encuestaTerminada = false;
		fLayout = (FrameLayout) findViewById(R.id.frameLayoutPreguntas);
		llPreguntas = (LinearLayout) findViewById(R.id.linearLayoutPreguntas);
		lblTituloPregunta = new TextView(this);
		foto = null;
		nombreFoto = "";
		drwFecha = getResources().getDrawable(R.drawable.calendario);
		drwFoto = getResources().getDrawable(R.drawable.op_fotos);

		cargarExtras();
		cargarPregunta();
		mostrarPreguntaActiva();


		String codCliente;
		String codVendedor;
		if (Main.cliente != null && Main.cliente.codigo != null){
			codCliente = Main.cliente.codigo;
		}else{
			//cliente = DataBaseBO.CargarClienteSeleccionado();
			cliente = Main.cliente;
			codCliente = cliente.codigo;
		}

		if (Main.usuario != null && Main.usuario.codigoVendedor!=null){
			codVendedor = Main.usuario.codigoVendedor;
		}else{
			usuario = DataBaseBO.ObtenerUsuario();
			Main.usuario = usuario;
			codVendedor = usuario.codigoVendedor;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("----------------------onResume ");
	}

	@Override
	protected void onDestroy() {
		borrarEncuesta();
		System.out.println("----------------------onDestroy ");
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		System.out.println("----------------------onPause ");
	}


	public void cargarExtras() {
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			if (bundle.containsKey("codEncuesta")) {
				codEncuesta = bundle.getInt("codEncuesta", 0);
			}
			if (bundle.containsKey("codPrimeraPregunta")) {
				codPrimeraPregunta = bundle.getInt("codPrimeraPregunta", 0);
			}
			if (bundle.containsKey("numDocEncuesta")) {
				numDocEncuesta = bundle.getString("numDocEncuesta", "");
			}

			if (numDocEncuesta.equals("")) {
				String keys[] = {"NUMDOC"};
				String datos[] = Util.getPrefence(this, EncuestasBO.PREFERENCEDOC, keys);
				if (datos.length>0) {
					numDocEncuesta = datos[0];
				}
			}
		}
	}

	public void cargarPregunta() {
		pregunta = EncuestasBO.obtenerPregunta(codEncuesta, codPrimeraPregunta, -1);
	}

	public void cargarPreguntaSiguiente() {
		if (pregunta!=null) {
			if (parametroPregunta!=null) {
				if (parametroPregunta.codPreguntaDestino!=-1) {
					pregunta = EncuestasBO.obtenerPregunta(codEncuesta, parametroPregunta.codPreguntaDestino, pregunta.codPregunta);
				}else{
					pregunta = EncuestasBO.obtenerPregunta(codEncuesta, pregunta.codPreguntaDestino,  pregunta.codPregunta);
				}
			}else{
				pregunta = EncuestasBO.obtenerPregunta(codEncuesta, pregunta.codPreguntaDestino,  pregunta.codPregunta);
			}
		}
	}

	public void cargarPreguntaAnterior() {
		if (pregunta!=null) {
			String codCliente;
			String codVendedor;

			if (Main.cliente != null){
				codCliente = Main.cliente.codigo;
			}else{
				//cliente = DataBaseBO.CargarClienteSeleccionado();
				cliente = Main.cliente;
				codCliente = cliente.codigo;
			}

			if (Main.usuario != null){
				codVendedor = Main.usuario.codigoVendedor;
			}else{
				usuario = DataBaseBO.ObtenerUsuario();
				Main.usuario = usuario;
				codVendedor = usuario.codigoVendedor;
			}
			if (numDocEncuesta.equals("")) {
				String keys[] = {"NUMDOC"};
				String datos[] = Util.getPrefence(this, EncuestasBO.PREFERENCEDOC, keys);
				if (datos.length>0) {
					numDocEncuesta = datos[0];
				}
			}
			EncuestasBO.borrarRespuestasPregunta(codEncuesta, pregunta.codPregunta, numDocEncuesta, codCliente, codVendedor);
			pregunta = EncuestasBO.obtenerPregunta(codEncuesta, pregunta.codPreguntaOrigen, -1);
		}
	}

	public void mostrarPreguntaActiva() {

		llPreguntas.removeAllViews();
		fLayout.invalidate();
		fLayout.postInvalidate();

		if(pregunta!=null){

			String codCliente;
			String codVendedor;

			if (Main.cliente != null){
				codCliente = Main.cliente.codigo;
			}else{
				//cliente = DataBaseBO.CargarClienteSeleccionado();
				cliente = Main.cliente;
				codCliente = cliente.codigo;
			}

			if (Main.usuario != null){
				codVendedor = Main.usuario.codigoVendedor;
			}else{
				usuario = DataBaseBO.ObtenerUsuario();
				Main.usuario = usuario;
				codVendedor = usuario.codigoVendedor;
			}


			lblTituloPregunta.setText(pregunta.descripcion);
			lblTituloPregunta.setTextColor(R.color.black);
			lblTituloPregunta.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);

			llPreguntas.addView(lblTituloPregunta);

			listaParametrosPregunta = EncuestasBO.listarParametrosPregunta(pregunta.codPregunta);

			listaRespuestasPregunta = EncuestasBO.listarRespuestasPregunta(pregunta.codEncuesta, pregunta.codPregunta, codCliente, codVendedor);

			switch (pregunta.codTipoPregunta) {

				//PREGUNTA ABIERTA
				case EncuestasBO.ABIERTA:
					preguntaAbierta(listaParametrosPregunta);
					break;

				//PREGUNTA SELECCIONMULTIPLE
				case EncuestasBO.SELECCIONMULTIPLE:
					preguntaSeleccionMultiple(listaParametrosPregunta);
					break;

				// PREGUNTA SELECCIONUNICA
				case EncuestasBO.SELECCIONUNICA:
					preguntaSeleccionUnica(listaParametrosPregunta);
					break;

				// PREGUNTA SELECCIONUNICA
				case EncuestasBO.SELECCIONUNICACONDICIONAL:
					preguntaSeleccionUnicaCondicional(listaParametrosPregunta);
					break;
			}

		} else {
			//mostrarDialogEncuestaTerminada();
		}

		fLayout.invalidate();
		fLayout.postInvalidate();
	}


	public void preguntaAbierta(Vector<ParametroPregunta> listaParametrosPregunta) {

		if (pregunta!=null) {
			parametroPregunta = null;

			String respuestaObtenida = "";
			String respuestaNumRespuesta = "";
			Respuesta respuestaPregunta = obtenerRespuesta(pregunta.codPregunta, -1);
			if (respuestaPregunta!=null) {
				respuestaObtenida = respuestaPregunta.respuesta;
				respuestaNumRespuesta = respuestaPregunta.numRespuesta;
			}


			LinearLayout lytAux = new LinearLayout(this);
			lytAux.setWeightSum(1f);
			LayoutParams params = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.5f);//width,height,weight

			switch (pregunta.codTipoDatoEncuesta) {

				case EncuestasBO.ALFANUMERICO:
					txtPreguntaAbierta = new EditText(this);
					txtPreguntaAbierta.setText(respuestaObtenida);
					InputFilter[] filterArrayText = new InputFilter[1];
					txtPreguntaAbierta.setInputType(InputType.TYPE_CLASS_TEXT);
					filterArrayText[0] = new InputFilter.LengthFilter(EncuestasBO.MAXLENGTH_TEXT);
					txtPreguntaAbierta.setFilters(filterArrayText);
					llPreguntas.addView(txtPreguntaAbierta);
					break;

				case EncuestasBO.FECHA:
					txtPreguntaAbierta = new EditText(this);
					txtPreguntaAbierta.setText(respuestaObtenida);
					txtPreguntaAbierta.setHint("Fecha");
					txtPreguntaAbierta.setEnabled(false);
					txtPreguntaAbierta.setLayoutParams(params);
					btnFecha = new Button(this);
					btnFecha.setId(EncuestasBO.IDBTNFECHA);
					btnFecha.setOnClickListener(this);
					btnFecha.setBackground(drwFecha);//setBackgroundColor(android.R.color.transparent);
					//btnFecha.setCompoundDrawablesWithIntrinsicBounds(null, drwFecha , null, null);//setCompoundDrawablesWithIntrinsicBounds(Drawable left, Drawable top, Drawable right, Drawable bottom)
					lytAux.setOrientation(LinearLayout.HORIZONTAL);
					lytAux.addView(txtPreguntaAbierta);
					lytAux.addView(btnFecha);
					llPreguntas.addView(lytAux);
					break;

				case EncuestasBO.NUMERICA:
					txtPreguntaAbierta = new EditText(this);
					txtPreguntaAbierta.setText(respuestaObtenida);
					InputFilter[] filterArrayNumber = new InputFilter[1];
					txtPreguntaAbierta.setInputType(InputType.TYPE_CLASS_NUMBER);
					filterArrayNumber[0] = new InputFilter.LengthFilter(EncuestasBO.MAXLENGTH_NUMBER);
					txtPreguntaAbierta.setFilters(filterArrayNumber);
					llPreguntas.addView(txtPreguntaAbierta);
					break;

				case EncuestasBO.FECHAHORA:
					txtPreguntaAbierta = new EditText(this);
					txtPreguntaAbierta.setText(respuestaObtenida);
					txtPreguntaAbierta.setHint("Fecha");
					txtPreguntaAbierta.setEnabled(false);
					txtPreguntaAbierta.setMinimumWidth(100);
					txtPreguntaAbierta.setLayoutParams(params);
					btnFecha = new Button(this);
					btnFecha.setId(EncuestasBO.IDBTNFECHAHORA);
					btnFecha.setOnClickListener(this);
					btnFecha.setBackground(drwFecha);//setBackgroundColor(android.R.color.transparent);
					//btnFecha.setCompoundDrawablesWithIntrinsicBounds(null, drwFecha , null, null);//setCompoundDrawablesWithIntrinsicBounds(Drawable left, Drawable top, Drawable right, Drawable bottom)
					lytAux.setOrientation(LinearLayout.HORIZONTAL);
					lytAux.addView(txtPreguntaAbierta);
					lytAux.addView(btnFecha);
					llPreguntas.addView(lytAux);
					break;

				case EncuestasBO.FOTO:
					btnFoto = new Button(this);
					btnFoto.setId(EncuestasBO.IDBTNFOTO);
					btnFoto.setOnClickListener(this);
					btnFoto.setBackground(drwFoto);
					imagenCapturada  = new ImageView(this);
					lytAux.addView(btnFoto);
					lytAux.addView(imagenCapturada);
					lytAux.setOrientation(LinearLayout.VERTICAL);
					llPreguntas.addView(lytAux);
					boolean hayFoto = EncuestasBO.obtenerRegistroFoto(respuestaObtenida, respuestaNumRespuesta);

					if (hayFoto) {
						if (nombreFoto.equals("")) {
							String keys[] = {"NOMBREFOTO"};
							String datos[] = Util.getPrefence(this, EncuestasBO.PREFERENCE, keys);
							if (datos.length>0) {
								nombreFoto = datos[0];
							}
						};
						foto = EncuestasBO.resizedImage(nombreFoto, FormEncuestasPreguntasActivity.this, EncuestasBO.WIDHTFOTO, EncuestasBO.HEIGHTFOTO);
						Bitmap fotoSmall = EncuestasBO.resizedImage(nombreFoto, FormEncuestasPreguntasActivity.this, widhtThumb, heightThumb);
						imagenCapturada.setImageBitmap(fotoSmall);
					}
					break;

			}
		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta");
		}
	}


	public void preguntaSeleccionMultiple(Vector<ParametroPregunta> listaParametrosPregunta) {

		if (pregunta!=null) {
			parametroPregunta = null;
			chcPreguntaSelMultiple = new CheckBox[listaParametrosPregunta.size()];
			for (int i = 0; i < listaParametrosPregunta.size(); i++) {
				parametroPregunta = listaParametrosPregunta.elementAt(i);

				boolean respuestaObtenida = false;
				Respuesta respuestaPregunta = obtenerRespuesta(pregunta.codPregunta, parametroPregunta.codParamPreg);
				if (respuestaPregunta!=null) {
					respuestaObtenida = true;
				}

				chcPreguntaSelMultiple[i] = new CheckBox(this);
				chcPreguntaSelMultiple[i].setText(parametroPregunta.descripcion);
				chcPreguntaSelMultiple[i].setTextColor(Color.BLACK);
				chcPreguntaSelMultiple[i].setChecked(respuestaObtenida);
				llPreguntas.addView(chcPreguntaSelMultiple[i]);
			}
		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta");
		}
	}

	public void preguntaSeleccionUnica(Vector<ParametroPregunta> listaParametrosPregunta) {

		if (pregunta!=null) {
			parametroPregunta = null;
			rbPreguntaSelUnica = new RadioButton[listaParametrosPregunta.size()];
			rgPreguntaSelUnica = new RadioGroup(this);
			rgPreguntaSelUnica.setOrientation(RadioGroup.VERTICAL);
			boolean respuestaObtenida = false;
			int respuestaParam = 0;
			for (int i = 0; i < listaParametrosPregunta.size(); i++) {
				parametroPregunta = listaParametrosPregunta.elementAt(i);
				Respuesta respuestaPregunta = obtenerRespuesta(pregunta.codPregunta, parametroPregunta.codParamPreg);
				if (respuestaPregunta!=null) {
					respuestaObtenida = true;
					respuestaParam = respuestaPregunta.codParamPreg;
				}
				int id = Integer.parseInt(pregunta.codPregunta+pregunta.codPregunta+parametroPregunta.codParamPreg+"");
				rbPreguntaSelUnica[i] = new RadioButton(this);
				rbPreguntaSelUnica[i].setId(id);
				rbPreguntaSelUnica[i].setText(parametroPregunta.descripcion);
				rbPreguntaSelUnica[i].setTextColor(Color.BLACK);
				rgPreguntaSelUnica.addView(rbPreguntaSelUnica[i]);
			}
			if (respuestaObtenida) {
				int idChk = Integer.parseInt(pregunta.codPregunta+pregunta.codPregunta+respuestaParam+"");
				rgPreguntaSelUnica.check(idChk);
			}
			llPreguntas.addView(rgPreguntaSelUnica);
		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta");
		}
	}

	public void preguntaSeleccionUnicaCondicional(Vector<ParametroPregunta> listaParametrosPregunta) {

		if (pregunta!=null) {
			parametroPregunta = null;
			rbPreguntaSelUnica = new RadioButton[listaParametrosPregunta.size()];
			rgPreguntaSelUnica = new RadioGroup(this);
			rgPreguntaSelUnica.setOrientation(RadioGroup.VERTICAL);
			boolean respuestaObtenida = false;
			int respuestaParam = 0;
			for (int i = 0; i < listaParametrosPregunta.size(); i++) {
				parametroPregunta = listaParametrosPregunta.elementAt(i);
				Respuesta respuestaPregunta = obtenerRespuesta(pregunta.codPregunta, parametroPregunta.codParamPreg);
				if (respuestaPregunta!=null) {
					respuestaObtenida = true;
					respuestaParam = respuestaPregunta.codParamPreg;
				}
				int id = Integer.parseInt(pregunta.codPregunta+pregunta.codPregunta+parametroPregunta.codParamPreg+"");
				rbPreguntaSelUnica[i] = new RadioButton(this);
				rbPreguntaSelUnica[i].setId(id);
				rbPreguntaSelUnica[i].setText(parametroPregunta.descripcion);
				rbPreguntaSelUnica[i].setTextColor(Color.BLACK);
				rgPreguntaSelUnica.addView(rbPreguntaSelUnica[i]);
			}
			if (respuestaObtenida) {
				int idChk = Integer.parseInt(pregunta.codPregunta+pregunta.codPregunta+respuestaParam+"");
				rgPreguntaSelUnica.check(idChk);
			}
			llPreguntas.addView(rgPreguntaSelUnica);
		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta");
		}
	}


	public Respuesta obtenerRespuesta(int codPregunta, int codParamPreg) {
		Respuesta respuesta = null;
		for (int i = 0; i < listaRespuestasPregunta.size(); i++) {
			if (listaRespuestasPregunta.get(i).codPregunta==codPregunta) {
				if (codParamPreg!=-1) {
					if (listaRespuestasPregunta.get(i).codParamPreg==codParamPreg) {
						respuesta = listaRespuestasPregunta.get(i);
						break;
					}
				}else{
					respuesta = listaRespuestasPregunta.get(i);
					break;
				}
			}
		}
		return respuesta;
	}






	public boolean validaRespuesta() {

		boolean bien = false;
		if (pregunta!=null) {

			int i = 0;
			switch (pregunta.codTipoPregunta) {

				//PREGUNTA ABIERTA
				case EncuestasBO.ABIERTA:

					switch (pregunta.codTipoDatoEncuesta) {

						case EncuestasBO.ALFANUMERICO:
							String datoS = txtPreguntaAbierta.getText().toString().trim();
							if (!datoS.equals("")) {
								bien = true;
							}
							break;

						case EncuestasBO.FECHA:
							String datoF = txtPreguntaAbierta.getText().toString().trim();
							if (!datoF.equals("")) {
								bien = true;
							}
							break;

						case EncuestasBO.NUMERICA:
							String datoN = txtPreguntaAbierta.getText().toString().trim();
							if (!datoN.equals("")) {
								bien = true;
							}
							break;

						case EncuestasBO.FECHAHORA:
							String datoFH = txtPreguntaAbierta.getText().toString().trim();
							if (!datoFH.equals("")) {
								bien = true;
							}
							break;

						case EncuestasBO.FOTO:
							if (foto!=null) {
								bien = true;
							}
							break;

					}

					break;

				//PREGUNTA SELECCIONMULTIPLE
				case EncuestasBO.SELECCIONMULTIPLE:
					for (i = 0; i < chcPreguntaSelMultiple.length; i++) {
						if (chcPreguntaSelMultiple[i].isChecked()) {
							bien = true;
							break;
						}
					}
					break;

				// PREGUNTA SELECCIONUNICA
				case EncuestasBO.SELECCIONUNICA:
					for (i = 0; i < rbPreguntaSelUnica.length; i++) {
						if (rbPreguntaSelUnica[i].isChecked()) {
							bien = true;
							break;
						}
					}
					break;

				// PREGUNTA SELECCIONUNICA
				case EncuestasBO.SELECCIONUNICACONDICIONAL:
					for (i = 0; i < rbPreguntaSelUnica.length; i++) {
						if (rbPreguntaSelUnica[i].isChecked()) {
							bien = true;
							break;
						}
					}
					break;
			}
		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta");
		}
		return bien;
	}



	public Vector<Respuesta> getRespuesta(String numDoc, String codCliente, String codVendedor) {
		Vector<Respuesta> respuestas = new Vector<Respuesta>();

		if (pregunta!=null) {
			int i = 0;
			Respuesta respuesta = null;
			switch (pregunta.codTipoPregunta) {

				//PREGUNTA ABIERTA
				case EncuestasBO.ABIERTA:

					switch (pregunta.codTipoDatoEncuesta) {

						case EncuestasBO.ALFANUMERICO:
							respuesta = new Respuesta();
							respuesta.codEncuesta = pregunta.codEncuesta;
							respuesta.codPregunta = pregunta.codPregunta;
							respuesta.codCliente = codCliente;
							respuesta.codVendedor = codVendedor;
							respuesta.numRespuesta = numDoc;
							respuesta.codParamPreg = -1;
							respuesta.descripcionParamPreg = "-1";
							respuesta.respuesta = txtPreguntaAbierta.getText().toString().trim();
							respuestas.add(respuesta);
							break;

						case EncuestasBO.FECHA:
							respuesta = new Respuesta();
							respuesta.codEncuesta = pregunta.codEncuesta;
							respuesta.codPregunta = pregunta.codPregunta;
							respuesta.codCliente = codCliente;
							respuesta.codVendedor = codVendedor;
							respuesta.numRespuesta = numDoc;
							respuesta.codParamPreg = -1;
							respuesta.descripcionParamPreg = "-1";
							respuesta.respuesta = txtPreguntaAbierta.getText().toString();
							respuestas.add(respuesta);
							break;

						case EncuestasBO.NUMERICA:
							respuesta = new Respuesta();
							respuesta.codEncuesta = pregunta.codEncuesta;
							respuesta.codPregunta = pregunta.codPregunta;
							respuesta.codCliente = codCliente;
							respuesta.codVendedor = codVendedor;
							respuesta.numRespuesta = numDoc;
							respuesta.codParamPreg = -1;
							respuesta.descripcionParamPreg = "-1";
							respuesta.respuesta = txtPreguntaAbierta.getText().toString().trim();
							respuestas.add(respuesta);
							break;

						case EncuestasBO.FECHAHORA:
							respuesta = new Respuesta();
							respuesta.codEncuesta = pregunta.codEncuesta;
							respuesta.codPregunta = pregunta.codPregunta;
							respuesta.codCliente = codCliente;
							respuesta.codVendedor = codVendedor;
							respuesta.numRespuesta = numDoc;
							respuesta.codParamPreg = -1;
							respuesta.descripcionParamPreg = "-1";
							respuesta.respuesta = txtPreguntaAbierta.getText().toString();
							respuestas.add(respuesta);
							break;

						case EncuestasBO.FOTO:

							if (foto!=null){
								String numDocFoto = "AFE"+pregunta.codEncuesta+pregunta.codPregunta+Util.FechaActual("yyyyMMddHHmmss");
								ByteArrayOutputStream stream = new ByteArrayOutputStream();
								foto.compress(Bitmap.CompressFormat.JPEG, 50, stream);
								byte[] bytes = stream.toByteArray();
								FotoEncuesta foto = new FotoEncuesta();
								foto.codEncuesta = pregunta.codEncuesta;
								foto.codPregunta = pregunta.codPregunta;
								foto.codParamPreg = -1;
								foto.codCliente = codCliente;
								foto.codVendedor = codVendedor;
								foto.numRespuesta = numDoc;
								foto.imagen = bytes;
								foto.numDoc = numDocFoto;
								boolean state = EncuestasBO.guardarFotoEncuesta(foto);
								if (state) {
									respuesta = new Respuesta();
									respuesta.codEncuesta = pregunta.codEncuesta;
									respuesta.codPregunta = pregunta.codPregunta;
									respuesta.codCliente = codCliente;
									respuesta.codVendedor = codVendedor;
									respuesta.numRespuesta = numDoc;
									respuesta.codParamPreg = -1;
									respuesta.descripcionParamPreg = "-1";
									respuesta.respuesta = numDocFoto;
									respuestas.add(respuesta);
								}
							}else{
								Util.MostrarToast(this, "Error al obtener la foto!");
							}
							break;
					}

					break;

				//PREGUNTA SELECCIONMULTIPLE
				case EncuestasBO.SELECCIONMULTIPLE:
					for (i = 0; i < chcPreguntaSelMultiple.length; i++) {
						if (chcPreguntaSelMultiple[i].isChecked()) {
							parametroPregunta = listaParametrosPregunta.elementAt(i);
							respuesta = new Respuesta();
							respuesta.codEncuesta = pregunta.codEncuesta;
							respuesta.codPregunta = pregunta.codPregunta;
							respuesta.codCliente = codCliente;
							respuesta.codVendedor = codVendedor;
							respuesta.numRespuesta = numDoc;
							respuesta.codParamPreg = parametroPregunta.codParamPreg;
							respuesta.descripcionParamPreg = parametroPregunta.descripcion;
							respuesta.respuesta = parametroPregunta.descripcion;
							respuestas.add(respuesta);
						}
					}
					break;

				// PREGUNTA SELECCIONUNICA
				case EncuestasBO.SELECCIONUNICA:
					for (i = 0; i < rbPreguntaSelUnica.length; i++) {
						if (rbPreguntaSelUnica[i].isChecked()) {
							parametroPregunta = listaParametrosPregunta.elementAt(i);
							respuesta = new Respuesta();
							respuesta.codEncuesta = pregunta.codEncuesta;
							respuesta.codPregunta = pregunta.codPregunta;
							respuesta.codCliente = codCliente;
							respuesta.codVendedor = codVendedor;
							respuesta.numRespuesta = numDoc;
							respuesta.codParamPreg = parametroPregunta.codParamPreg;
							respuesta.descripcionParamPreg = parametroPregunta.descripcion;
							respuesta.respuesta = parametroPregunta.descripcion;
							respuestas.add(respuesta);
							break;
						}
					}
					break;

				// PREGUNTA SELECCIONUNICA
				case EncuestasBO.SELECCIONUNICACONDICIONAL:
					for (i = 0; i < rbPreguntaSelUnica.length; i++) {
						if (rbPreguntaSelUnica[i].isChecked()) {
							parametroPregunta = listaParametrosPregunta.elementAt(i);
							respuesta = new Respuesta();
							respuesta.codEncuesta = pregunta.codEncuesta;
							respuesta.codPregunta = pregunta.codPregunta;
							respuesta.codCliente = codCliente;
							respuesta.codVendedor = codVendedor;
							respuesta.numRespuesta = numDoc;
							respuesta.codParamPreg = parametroPregunta.codParamPreg;
							respuesta.descripcionParamPreg = parametroPregunta.descripcion;
							respuesta.respuesta = parametroPregunta.descripcion;
							respuestas.add(respuesta);
							break;
						}
					}
					break;
			}
		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta");
		}
		return respuestas;
	}


	public void guardarRespuesta() {
		if (pregunta!=null) {
			String codCliente;
			String codVendedor;

			if (Main.cliente != null){
				codCliente = Main.cliente.codigo;
			}else{
				//cliente = DataBaseBO.CargarClienteSeleccionado();
				cliente = Main.cliente;
				codCliente = cliente.codigo;
			}

			if (Main.usuario != null){
				codVendedor = Main.usuario.codigoVendedor;
			}else{
				usuario = DataBaseBO.ObtenerUsuario();
				Main.usuario = usuario;
				codVendedor = usuario.codigoVendedor;
			}

			if (numDocEncuesta.equals("")) {
				String keys[] = {"NUMDOC"};
				String datos[] = Util.getPrefence(this, EncuestasBO.PREFERENCEDOC, keys);
				if (datos.length>0) {
					numDocEncuesta = datos[0];
				}
			}

			Vector<Respuesta> respuesta = getRespuesta(numDocEncuesta, codCliente, codVendedor);

			if (EncuestasBO.guardarRespuesta(pregunta, codCliente, codVendedor, respuesta)) {

				if (pregunta!=null) {
					if (parametroPregunta!=null) {
						if (parametroPregunta.codPreguntaDestino!=-1) {
							cargarPreguntaSiguiente();
							mostrarPreguntaActiva();
						}else{
							if (pregunta.codPreguntaDestino!=-1) {
								cargarPreguntaSiguiente();
								mostrarPreguntaActiva();
							}else{
								mostrarDialogEncuestaTerminada();
							}
						}
					}else{
						if (pregunta.codPreguntaDestino!=-1) {
							cargarPreguntaSiguiente();
							mostrarPreguntaActiva();
						}else{
							mostrarDialogEncuestaTerminada();
						}
					}
				}
				limpiarInstanciaFotos();

			}else {
				Util.MostrarAlertDialog(this, "La respuesta no pudo ser almacenada, por favor intente de nuevo");
			}

		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta");
		}
	}

	public void borrarEncuesta() {

		System.out.println("estra a borrar encuesta encuestaTerminada:"+encuestaTerminada);
		if (!encuestaTerminada) {
			String codCliente;
			String codVendedor;

			if (Main.cliente != null){
				codCliente = Main.cliente.codigo;
			}else{
				//cliente = DataBaseBO.CargarClienteSeleccionado();
				cliente = Main.cliente;
				codCliente = cliente.codigo;
			}

			if (Main.usuario != null){
				codVendedor = Main.usuario.codigoVendedor;
			}else{
				usuario = DataBaseBO.ObtenerUsuario();
				Main.usuario = usuario;
				codVendedor = usuario.codigoVendedor;
			}

			if (numDocEncuesta.equals("")) {
				String keys[] = {"NUMDOC"};
				String datos[] = Util.getPrefence(this, EncuestasBO.PREFERENCEDOC, keys);
				if (datos.length>0) {
					numDocEncuesta = datos[0];
				}
			}
			boolean borrado = EncuestasBO.borrarRespuestasEncuesta(codEncuesta, numDocEncuesta, codCliente, codVendedor);
			if (!borrado) {
				AlertDialog alertDialog;
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setCancelable(false)
						.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								salir();
								finish();
							}
						});
				alertDialog = builder.create();
				alertDialog.setMessage("No se logro borrar la encuesta!");
				alertDialog.show();
			}else{
				salir();
				finish();
			}
		}
	}


	public void OnClickAceptarEncuesta(View view) {
		if (txtPreguntaAbierta != null) {
			Util.OcultarTeclado(this, txtPreguntaAbierta);
		}
		if (validaRespuesta()) {
			guardarRespuesta();
		} else {
			Util.MostrarAlertDialog(this, "La respuesta no es valida para la pregunta");
		}
	}

	public void OnClickAtras(View view) {
		if (pregunta!=null) {
			if (pregunta.codPreguntaOrigen != -1) {
				cargarPreguntaAnterior();
				mostrarPreguntaActiva();
			}else{
				mostrarDialogEncuestaSalir();
			}
		}else{
			Util.MostrarAlertDialog(this, "Error cargando la pregunta anterior");
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case EncuestasBO.IDBTNFECHA:
				clickFecha();
				break;
			case EncuestasBO.IDBTNFECHAHORA:
				clickFechaHora();
				break;
			case EncuestasBO.IDBTNFOTO:
				clickTomarFoto();
				break;

		}


	}

	public void clickFecha() {
		DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				String day = "";
				String month = "";
				++monthOfYear;
				if (dayOfMonth < 10)
					day = "0" + dayOfMonth;
				else
					day = "" + dayOfMonth;
				if (monthOfYear < 10)
					month = "0" + monthOfYear;
				else
					month = "" + monthOfYear;
				txtPreguntaAbierta.setText(year + "-" + month + "-" + day);
			}
		};
		Calendar cal = Calendar.getInstance();
		DatePickerDialog datePickerDialog = new DatePickerDialog(this, listener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();
	}

	public void clickFechaHora() {

		System.out.println("click hora");

		TimePickerDialog.OnTimeSetListener listenerH = new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker view, int hour, int minute) {
				String hourOut = "";
				String minuteOut = "";
				if (hour < 10)
					hourOut = "0" + hour;
				else
					hourOut = "" + hour;
				if (minute < 10)
					minuteOut = "0" + minute;
				else
					minuteOut = "" + minute;

				String fechaIn = txtPreguntaAbierta.getText().toString().trim();
				txtPreguntaAbierta.setText(fechaIn + " " + hourOut + ":" + minuteOut);
			}
		};
		Calendar calhora = Calendar.getInstance();
		TimePickerDialog dateTimePickerDialog = new TimePickerDialog(this, listenerH, calhora.get(Calendar.HOUR), calhora.get(Calendar.MINUTE),  DateFormat.is24HourFormat(this));
		dateTimePickerDialog.show();

		DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				String day = "";
				String month = "";
				++monthOfYear;
				if (dayOfMonth < 10)
					day = "0" + dayOfMonth;
				else
					day = "" + dayOfMonth;
				if (monthOfYear < 10)
					month = "0" + monthOfYear;
				else
					month = "" + monthOfYear;

				txtPreguntaAbierta.setText(year + "-" + month + "-" + day);
			}
		};
		Calendar cal = Calendar.getInstance();
		DatePickerDialog datePickerDialog = new DatePickerDialog(this, listener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();

	}

	public void clickTomarFoto() {

		nombreFoto = "ENCFOTO.jpg";
		String keys[] = {"NOMBREFOTO"};
		String datos[] = {nombreFoto};
		Util.putPrefence(this, EncuestasBO.PREFERENCE, keys, datos);


		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String filePath = Util.DirApp().getPath() + "/" + nombreFoto;
		Uri output = Uri.fromFile(new File(filePath));
		intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
		startActivityForResult(intent, Const.RESP_TOMAR_FOTO);
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		System.out.println("request code: " + requestCode);
		System.out.println("result code:" + resultCode);

		if (resultCode == RESULT_OK) {

			if (requestCode == Const.RESP_TOMAR_FOTO ) {
				try {

					String codCliente;
					String codVendedor;

					if (Main.cliente != null){
						codCliente = Main.cliente.codigo;
					}else{
						//cliente = DataBaseBO.CargarClienteSeleccionado();
						cliente = Main.cliente;
						codCliente = cliente.codigo;
					}

					if (Main.usuario != null){
						codVendedor = Main.usuario.codigoVendedor;
					}else{
						usuario = DataBaseBO.ObtenerUsuario();
						Main.usuario = usuario;
						codVendedor = usuario.codigoVendedor;
					}

					if (nombreFoto.equals("")) {
						String keys[] = {"NOMBREFOTO"};
						String datos[] = Util.getPrefence(this, EncuestasBO.PREFERENCE, keys);
						if (datos.length>0) {
							nombreFoto = datos[0];
						}
					};
					String marcaAgua = "Cliente:"+ codCliente +"  Usuario:"+ codVendedor +"\n"+
							"Encuesta:"+codEncuesta+"  Pregunta:"+pregunta.codPregunta;
					foto = EncuestasBO.resizedImageWaterMark(nombreFoto, FormEncuestasPreguntasActivity.this, marcaAgua);

					Bitmap fotoSmall = EncuestasBO.resizedImage(nombreFoto, FormEncuestasPreguntasActivity.this, widhtThumb, heightThumb);
					imagenCapturada.setImageBitmap(fotoSmall);

				} catch (OutOfMemoryError error) {
					if (foto != null)
						foto.recycle();
					foto = null;
					System.gc();
					Util.MostrarAlertDialog(this, "No se pudo tomar la foto!");
				}
			}else{
				Util.MostrarAlertDialog(this,"OcurriÃ³ un error tomando la foto.");
			}

		}else{
			Util.MostrarAlertDialog(this,"Accion no completada");
		}
	}


	public void mostrarDialogEncuestaTerminada() {

		AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						String codCliente;
						String codVendedor;

						if (Main.cliente != null){
							codCliente = Main.cliente.codigo;
						}else{
							//cliente = DataBaseBO.CargarClienteSeleccionado();
							cliente = Main.cliente;
							codCliente = cliente.codigo;
						}

						if (Main.usuario != null){
							codVendedor = Main.usuario.codigoVendedor;
						}else{
							usuario = DataBaseBO.ObtenerUsuario();
							Main.usuario = usuario;
							codVendedor = usuario.codigoVendedor;
						}
						if (numDocEncuesta.equals("")) {
							String keys[] = {"NUMDOC"};
							String datos[] = Util.getPrefence(FormEncuestasPreguntasActivity.this, EncuestasBO.PREFERENCEDOC, keys);
							if (datos.length>0) {
								numDocEncuesta = datos[0];
							}
						}
						boolean estado = EncuestasBO.finalizarEncuesta(codEncuesta, numDocEncuesta, codCliente, codVendedor);
						if (estado) {
							encuestaTerminada = true;
							dialog.cancel();
							salir();
							finish();
						}else{
							encuestaTerminada = false;
							dialog.cancel();
							mostrarDialogErrorFinalizarEncuesta();
						}
					}
				});
		alertDialog = builder.create();
		alertDialog.setMessage("Encuesta terminada");
		alertDialog.show();
	}

	public void mostrarDialogEncuestaSalir() {

		AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						borrarEncuesta();
					}
				})
				.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		alertDialog = builder.create();
		alertDialog.setMessage("Desea salir de la encuesta?");
		alertDialog.show();
	}

	public void mostrarDialogErrorFinalizarEncuesta() {

		AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						salir();
						finish();
					}
				});
		alertDialog = builder.create();
		alertDialog.setMessage("Error finalizando encuesta.\nPor favor intente de nuevo.");
		alertDialog.show();
	}

	public void limpiarInstanciaFotos() {
		foto = null;
		Util.clearPrefence(this, EncuestasBO.PREFERENCE);
	}

	public void salir() {
		/*Enviar informacion antes de finalizar la actividad*/
		enviarInformacion();
	}


	/**
	 * Preguntar al usuario si desea enviar la informacion registrada de las encuaestas.
	 */
	private void enviarInformacion() {
		if (DataBaseBO.HayInformacionXEnviar()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false);
			builder.setIcon(android.R.attr.dialogIcon);
			builder.setTitle("¿Enviar Informacion?");
			builder.setMessage("Hay informacion disponible para sincronizar.");
			builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					progressDialog = ProgressDialog.show(FormEncuestasPreguntasActivity.this, "", "Enviando Informacion...", true);
					progressDialog.show();

					EncuestasBO.actualizarFechaSincronizacion();
					Sync sync = new Sync(FormEncuestasPreguntasActivity.this, Const.ENVIAR_PEDIDO);
					sync.start();
				}
			});
			builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					Util.clearPrefence(FormEncuestasPreguntasActivity.this, EncuestasBO.PREFERENCE);
					Util.clearPrefence(FormEncuestasPreguntasActivity.this, EncuestasBO.PREFERENCEDOC);
					setResult(RESULT_OK);
					finish();
				}
			});

		}
		else {
			Util.clearPrefence(FormEncuestasPreguntasActivity.this, EncuestasBO.PREFERENCE);
			Util.clearPrefence(FormEncuestasPreguntasActivity.this, EncuestasBO.PREFERENCEDOC);
			setResult(RESULT_OK);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			mostrarDialogEncuestaSalir();
		}
		return super.onKeyDown(keyCode, event);
	}

	public void OnClickRegresar(View view){
		mostrarDialogEncuestaSalir();
	}

	/*public void onAttachedToWindow() {
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		super.onAttachedToWindow();

	}*/




	@Override
	public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

		switch (codeRequest) {

			case Const.ENVIAR_PEDIDO:
				respuestaEnviarInfo(ok, respuestaServer, msg);
				break;
		}
	}



	public void respuestaEnviarInfo(boolean ok, String respuestaServer, String msg) {

		final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : msg;

		this.runOnUiThread(new Runnable() {

			public void run() {
				if (progressDialog != null)
					progressDialog.cancel();

				AlertDialog.Builder builder = new AlertDialog.Builder(FormEncuestasPreguntasActivity.this);
				builder.setIcon(android.R.attr.dialogIcon);
				builder.setMessage(mensaje);
				builder.setCancelable(false);
				builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Util.clearPrefence(FormEncuestasPreguntasActivity.this, EncuestasBO.PREFERENCE);
						Util.clearPrefence(FormEncuestasPreguntasActivity.this, EncuestasBO.PREFERENCEDOC);
						setResult(RESULT_OK);
						finish();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}
}