package celuweb.com.uyusa;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Producto;

public class FormEscanearCodigoDeBarrasActivity extends Activity {
    /** Called when the activity is first created. */
	
	private static final String BS_PACKAGE = "com.google.zxing.client.android";
	public int RESP_CODIGOBARRAS = 1000;
	ProgressDialog progressDialog;
	EditText etCodigo;
	int tipoInventario;
	boolean primerBarCode = true;
	TextView txtInfoDeCliente;
	Dialog dialogResumen;
//	Vector<MotivoQR> listadoMotivos;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.escanear_codigo_de_barras);
        etCodigo = (EditText) findViewById(R.id.etCodigo);
    	txtInfoDeCliente = (TextView)findViewById(R.id.txtInfoDeCliente);


    }
    
    
    public void onClickEmpezarEscaneo(View view){
    	
        Intent intentScan = new Intent(BS_PACKAGE + ".SCAN");
	    intentScan.putExtra("PROMPT_MESSAGE", "Enfoque entre 9 y 11 cm.viendo s�lo el c�digo de barras");
	    String targetAppPackage = findTargetAppPackage(intentScan);
	    if (targetAppPackage == null) {
	      showDownloadDialog();
	    } else{ 
	    	etCodigo.setText("");
	    	startActivityForResult(intentScan, RESP_CODIGOBARRAS);
	    
	    }
		
    	
    }
    
    
    
	private String findTargetAppPackage(Intent intent) {

		PackageManager pm = this.getPackageManager();
		List<ResolveInfo> availableApps = pm.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		if (availableApps != null) {
			for (ResolveInfo availableApp : availableApps) {
				String packageName = availableApp.activityInfo.packageName;
				if (BS_PACKAGE.contains(packageName)) {
					return packageName;
				}
			}
		}
		return null;
	}
	 

	
	
	
	@Override
	public void onResume(){
	    super.onResume();
	    DataBaseBO.validarUsuario();
		DataBaseBO.validarCliente();
		txtInfoDeCliente.setText(Main.cliente.codigo.substring(0, Main.cliente.codigo.length()-3)+"\n"+Main.cliente.Nombre);

	}
	
	
	
	
public void onClickConfigBarCode(View view) {
		
		InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
    	
    	if (imeManager != null) {
    		
    		imeManager.showInputMethodPicker();
    		//imeManager.toggleSoftInput(showFlags, hideFlags);
    		
    	} else {
    		
    		Toast.makeText(this, "No se pudo mostrar del show Input", Toast.LENGTH_LONG).show();
    	}
	}



@Override
public void onConfigurationChanged(Configuration newConfig) {
	
	super.onConfigurationChanged(newConfig);

	/*if (Functions.isCurrentActivity(this)) {
	
		//Si el teclado fisico esta disponible
		if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
			showInputMethod();
		}
	}*/
}



	private AlertDialog showDownloadDialog() {
		final String DEFAULT_TITLE = "Instalar Barcode Scanner?";
		final String DEFAULT_MESSAGE = "Esta aplicacion necesita Barcode Scanner. Quiere instalarla?";
		final String DEFAULT_YES = "Si";
		final String DEFAULT_NO = "No";

		AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
		downloadDialog.setTitle(DEFAULT_TITLE);
		downloadDialog.setMessage(DEFAULT_MESSAGE);
		downloadDialog.setPositiveButton(DEFAULT_YES,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogInterface, int i) {
						Uri uri = Uri
								.parse("market://details?id=" + BS_PACKAGE);
						Intent intent = new Intent(Intent.ACTION_VIEW, uri);
						try {
							FormEscanearCodigoDeBarrasActivity.this.startActivity(intent);
						} catch (ActivityNotFoundException anfe) {
							// Hmm, market is not installed
							Toast.makeText(
									FormEscanearCodigoDeBarrasActivity.this,
									"Android market no esta instalado,no puedo instalar Barcode Scanner",
									Toast.LENGTH_LONG).show();
						}
					}
				});
		downloadDialog.setNegativeButton(DEFAULT_NO,
				new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialogInterface, int i) {
					}
				});
		return downloadDialog.show();
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {

			if (requestCode == RESP_CODIGOBARRAS) {

				if (resultCode == Activity.RESULT_OK) {

					String contents = data.getStringExtra("SCAN_RESULT");
					
					int aux = 0;
//					if(DataBaseBO.ExisteCodigoEnProductos(contents)){
						if(aux != 0){
						MostarDialogResumen(contents);
						
					}else{
						Util.mostrarToast(this, "No existe un producto registrado con ese codigo");
					}

//					etCodigo.setText(contents);

				}
			}

		}
	}
	
	
	
	public void onClickGuardarCodigo(View view){
		
//	 String texto = etCodigo.getText().toString();
//	 
//	 if(texto.equals("")){
//		 
//		 Util.MostrarAlertDialog(this, "Por Favor Escanee el Codigo", 2);
//		 
//	 }else{
//		 
//		 if(DataBaseBO.ExisteCodigoQR(texto,Main.cliente.codigo)){
//			 
//			 mostrarMensajeYaExisteCodigo();
//			 
//		 }else{
//			 
//			boolean guardo = DataBaseBO.guardarCodigoQR(Main.cliente.codigo,texto,Main.usuario.codigoVendedor);
//			
//			
//			if(guardo){
//				
//				mostrarMensaje();
//				
//			}else{
//				
//				Util.MostrarAlertDialog(this, "Fallo al Guardar Informacion. Por Favor Intenelo Nuevamente", 2);
//				
//				
//			}
//			 
//			 
//		 }
//		 
//	 }
		Util.mostrarToast(this, "Guardo");
	}
	
	
	public void onClickCancelarCodigo(View view){
		
		
		finish();
		
	}
	
	
	public void mostrarMensaje(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Guardo el Codigo de Forma Exitosa")	
		.setCancelable(false)
		.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
				finish();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
		
		
	}
	
	
	public void mostrarMensaje2(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Guardo la Novedad de Forma Exitosa")	
		.setCancelable(false)
		.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
				finish();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
		
		
	}
	
	
//	public void  mostrarMensajeYaExisteCodigo(){
		
		
		/*AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Codigo Ya Existe y Ya Esta Asociado a Otro Cliente para Este Vendedor Desea Registrar la Novedad")	
		.setCancelable(false)
		.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
				
				String texto = etCodigo.getText().toString();
					
				
				boolean guardo = DataBaseBO.guardarNovedadCodigoQR(Main.cliente.codigo,texto,Main.usuario.codigoVendedor);
				
				
				if(guardo){
					
					mostrarMensaje2();
					
				}else{
					
					Util.MostrarAlertDialog(EscanearCodigoDeBarrasActivity.this, "Fallo al Guardar Informacion. Por Favor Intenelo Nuevamente", 2);
					
					
				}
				
			}
		})
		
		.setNegativeButton("cancelar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.cancel();
			}
		});


		AlertDialog alert = builder.create();
		alert.show();*/
		
		
		
//		final Dialog dialog = new Dialog(this);
//		dialog.setContentView(R.layout.spinner_dialog);
//		dialog.setTitle("Mensaje");
//
//
//		TextView text = (TextView) dialog.findViewById(R.id.dialog_label2);
//		text.setText("Codigo "+etCodigo.getText().toString()+" Ya Existe y Ya Esta Asociado a Otro Cliente para Este Vendedor Desea Registrar la Novedad?");
//		
//		Button dialogButton = (Button) dialog.findViewById(R.id.btnOK);
//
//		dialogButton.setOnClickListener(new OnClickListener() {
//			
//			public void onClick(View v) {
//				
//                String texto = etCodigo.getText().toString();
//					
//                Spinner spinner = (Spinner) dialog.findViewById(R.id.dialog_spinner);
//                
//                String novedad = "";
//                
//                if(listadoMotivos.size()>0){
//                	
//                	novedad = listadoMotivos.get(spinner.getSelectedItemPosition()).motivo;
//                	
//                }
//                
//				boolean guardo = DataBaseBO.guardarNovedadCodigoQR(Main.cliente.codigo,texto,Main.usuario.codigoVendedor,novedad);
//				
//				
//				if(guardo){
//					
//					mostrarMensaje2();
//					
//				}else{
//					
//					Util.MostrarAlertDialog(EscanearCodigoDeBarrasActivity.this, "Fallo al Guardar Informacion. Por Favor Intenelo Nuevamente", 2);
//					
//					
//				}
//				
//				
//				dialog.dismiss();
//				
//				
//				
//			}
//		});
//		
//		
//		Button dialogButton2 = (Button) dialog.findViewById(R.id.btnCancel);
//
//		dialogButton2.setOnClickListener(new OnClickListener() {
//			
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//		Spinner spinner = (Spinner) dialog.findViewById(R.id.dialog_spinner);
//		Vector<String> items = new Vector<String>();
//		listadoMotivos = DataBaseBO.ListaMotivosQR(items);
//		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items); 
//		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		spinner.setAdapter(spinnerArrayAdapter);
//		
//		dialog.show();
//		
//		
//		
//	}


	public void MostarDialogResumen(final String codigoProducto) {

		dialogResumen = new Dialog(this);
		dialogResumen.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialogResumen.setContentView(R.layout.resumen_producto);
		
//		if(encabezado.sincronizado == 0)
//			((Button)dialogResumen.findViewById(R.id.buttonAnular)).setVisibility(View.VISIBLE);
//		else
//			((Button)dialogResumen.findViewById(R.id.buttonAnular)).setVisibility(View.GONE);
		
//		String titulo = (encabezado.tipoTrans == 2)? "Devolucion":"Venta";
		dialogResumen.setTitle("Resumen Producto" );

//		encabezado.sub_total = 0;
//		encabezado.total_iva = 0;
//		encabezado.valor_descuento = 0;

		
		ItemListView itemListView;
		Hashtable<String, Producto> detalleProducto = DataBaseBO.ProductoXCodigo_(codigoProducto);

		Enumeration<Producto> e = detalleProducto.elements();
		Vector<ItemListView> datosPedido = new Vector<ItemListView>();

		while (e.hasMoreElements()) {

			Producto producto = e.nextElement();
			itemListView = new ItemListView();


			itemListView.titulo = producto.codigo + " - " + producto.descripcion;
			itemListView.subTitulo = "Precio: $" + producto.precioIva + " - Iva: " + producto.iva + " - Desc: " + producto.descuento + " - Precio + Iva:  "+producto.precioIva;
			datosPedido.addElement(itemListView);

			
		}

		ItemListView[] listaItems = new ItemListView[datosPedido.size()];
		datosPedido.copyInto(listaItems);

//		encabezado.valor_neto = encabezado.sub_total + encabezado.total_iva - encabezado.valor_descuento;
//		encabezado.str_valor_neto = Util.SepararMiles(Util.Redondear(Util.QuitarE("" + encabezado.valor_neto), 2));

		((TextView) dialogResumen.findViewById(R.id.lblCliente)).setText(Main.cliente.Nombre);

//		if (encabezado.tipoTrans == 0)
//			((TextView) dialogResumen.findViewById(R.id.lblValorNetoPedido)).setText(encabezado.str_valor_neto);
//		else if (encabezado.tipoTrans == 10)
//			((TextView) dialogResumen.findViewById(R.id.lblValorNetoPedido)).setText("0");

		ListViewAdapterPedido adapter = new ListViewAdapterPedido(this, listaItems, R.drawable.compra, 0);
		ListView listaResumenPedido = (ListView) dialogResumen.findViewById(R.id.listaResumenPedido);
		listaResumenPedido.setAdapter(adapter);

		((Button) dialogResumen.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				dialogResumen.cancel();
			}
		});
//		
//		((Button) dialogResumen.findViewById(R.id.buttonAnular)).setOnClickListener(new OnClickListener() {
//
//			public void onClick(View v) {
//				dialogResumen.cancel();
				
//				if(encabezado.anulado == 1){
//					Toast.makeText(FormEstadisticaPedidos.this,"Ya esta anulado.", Toast.LENGTH_SHORT).show();
//					return;
//				}
//				else {
//					mostrarAlertAnular(encabezado);
//					return;
//				}
//			}
//		});

		((Button) dialogResumen.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				
				dialogResumen.cancel();

//				progressDialog = ProgressDialog.show(FormEstadisticaPedidos.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
//				progressDialog.show();
//
//				SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
//				macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");
//
//				if (macImpresora.equals("-")) {
//
//					if (progressDialog != null)
//						progressDialog.cancel();
//					Util.MostrarAlertDialog(FormEstadisticaPedidos.this, "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");
//					
//					
//				} else {
//
//					imprimirTirillaGeneral(macImpresora, encabezado.numero_doc, encabezado.tipoTrans);
//				}
			}
		});

		dialogResumen.setCancelable(false);
		dialogResumen.show();
		dialogResumen.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.cliente);
	}

    
}