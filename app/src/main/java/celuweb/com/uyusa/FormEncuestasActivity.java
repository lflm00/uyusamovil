package celuweb.com.uyusa;

import java.util.Vector;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.EncuestasBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Encuestas;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;

public class FormEncuestasActivity extends CustomActivity /*implements GPS*/ {

    boolean primerEjecucion = true;

    Vector<Encuestas> listaEncuestas;

    int activoVendedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_encuestas);

        CargarEncuestas();
        SetListenerListView();

    }

    /**
     * metodo, permite validar si la encuesta esta disponible para el canal.
     *
     * @param listaItems
     * @param listaItems
     * @param listaEncuestas2, encuestas diposnibles.
     * @param canal,           canal que puede hacer la encuenta.
     */
    private void validarEncuesta(Vector<Encuestas> listaEncuestas2, String canal, Vector<ItemListView> listaItems) {

        int disponibles = 0;

        for (Encuestas encuestas : listaEncuestas2) {
            disponibles = DataBaseBO.encuestasDisponibles(encuestas.codigo, canal);
            // si no hay encuesta disponible para ese canal y codigo removerla de la lista de encuestas disponibles.
            if (disponibles == 0) {
                listaEncuestas2.remove(encuestas);
            }
        }
    }

    public void Inicializar() {

        activoVendedor = 0;

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            if (bundle.containsKey("activoVendedor")) {

                activoVendedor = bundle.getInt("activoVendedor");
            }
        }
    }

    //@Override
    public void respGPS(Location location) {

        Toast.makeText(getBaseContext(), "Coor: " + location.getLatitude() + " - " + location.getLongitude(), Toast.LENGTH_LONG).show();
    }


    public void CargarEncuestas() {

        Vector<ItemListView> listaItems = new Vector<ItemListView>();
        listaEncuestas = DataBaseBO.ListaEncuestas(listaItems, activoVendedor, Main.cliente.Canal);

        // validar si el canal y la encuesta, cumplen condiciones de canal_encuesta si es asi, se realiza la encuesta, de lo contrario, no se hace encuesta.
        //validarEncuesta(listaEncuestas, Main.cliente.Canal, listaItems);


        if (listaEncuestas.isEmpty()) {
            ItemListView itemListView = new ItemListView();
            listaItems.removeAllElements();
            itemListView.titulo = "!No hay encuestas disponibles para este canal!";
            listaItems.add(itemListView);
        }

        ItemListView[] items;

        if (listaItems.size() > 0) {

            items = new ItemListView[listaItems.size()];
            listaItems.copyInto(items);

        } else {

            items = new ItemListView[]{};

            if (listaEncuestas != null)
                listaEncuestas.removeAllElements();
        }

        ListViewAdapter adapter = new ListViewAdapter(this, items, R.drawable.op_efectividad, 0x2E65AD);
        ListView listaEncuestas = (ListView) findViewById(R.id.listaEncuestas);
        listaEncuestas.setAdapter(adapter);
    }

    public void SetListenerListView() {

        ListView listaOpciones = (ListView) findViewById(R.id.listaEncuestas);
        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CargarInformacionPreguntas(position);
            }
        });
    }

    public void CargarInformacionPreguntas(final int position) {

        if (listaEncuestas.isEmpty()) {
            try {
                this.finish();
            } catch (Throwable e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            Cliente cliente;
            Usuario usuario;
            String codCliente;
            String codVendedor;
            if (Main.cliente != null && Main.cliente.codigo != null) {
                codCliente = Main.cliente.codigo;
            } else {
                //cliente = DataBaseBO.CargarClienteSeleccionado();
                cliente = Main.cliente;
                codCliente = cliente.codigo;
            }
            if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
                codVendedor = Main.usuario.codigoVendedor;
            } else {
                usuario = DataBaseBO.ObtenerUsuario();
                Main.usuario = usuario;
                codVendedor = usuario.codigoVendedor;
            }

            Encuestas encuesta = listaEncuestas.elementAt(position);

            String numDocEncuesta = "AENC" + codVendedor + codCliente + Util.FechaActual("yyyyMMddHHmmssSSS");
            String keys[] = {"NUMDOC"};
            String datos[] = {numDocEncuesta};
            Util.putPrefence(this, EncuestasBO.PREFERENCEDOC, keys, datos);

            Intent formPreguntas = new Intent(FormEncuestasActivity.this, FormEncuestasPreguntasActivity.class);
            formPreguntas.putExtra("codEncuesta", Integer.parseInt(encuesta.codigo));
            formPreguntas.putExtra("codPrimeraPregunta", encuesta.codPrimeraPregunta);
            formPreguntas.putExtra("numDocEncuesta", numDocEncuesta);
            startActivityForResult(formPreguntas, Const.RESP_FORM_PREGUNTAS);
        }

    }

    public void OnClickCancelarEncuesta(View view) {

        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case Const.RESP_FORM_PREGUNTAS:

                    finish();
                    break;
            }
        }
    }

    public void OnClickRegresar(View view) {
        finish();
    }


}
