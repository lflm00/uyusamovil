package celuweb.com.uyusa;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.RecibosRecaudo;
import celuweb.com.DataObject.objeto;

public class FormCarteraFacturas extends Activity {

	Cartera carteraSel;

	Dialog dialogEditar;

	static TextView lblOtrosDescuentos;

	long tiempoClick1 = 0;

	Vector<Cartera> listadoCarteraRecaudar;

	String codCliente = "";
	long mLastClickTime = 0;

	Cliente cliente;
	//public static float totalDescuento = 0;



	@Override
	protected void onCreate (Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_cartera_facturas);
		
		/*limpiar tablas temporales*/
		DataBaseBO.borrarTempFormaPagoDetalleRecaudos();
		
		/*Borrar la tabla temporal de carteras con prontopago vencido*/
		DataBaseBO.borrarTablaTemporalCarteraProntoPagoConsig();
		
		/*crear una nueva tabla vacia para conservar las carteras con prontopago vencido*/
		DataBaseBO.crearTablaTemporalCarteraProntoPagoConsig();

		//		//recibir codigo de cliente seleccionado
		//				Bundle bundle = getIntent().getExtras();
		//				codCliente = bundle.getString("codigoCliente");
		//				if(codCliente != null) {
		//					cliente = DataBaseBO.BuscarCliente(codCliente);
		//				}
		inicializar();
		cargarListaRecaudo();
		SetListenerListView();

		Util.closeTecladoStartActivity(this);
	}



	public void inicializar () {

		lblOtrosDescuentos = (TextView) findViewById(R.id.lblOtrosDescuentos);
		final EditText txtNumeroReciboRecaudo = (EditText) findViewById(R.id.txtNumeroReciboRecaudo);

		txtNumeroReciboRecaudo.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		txtNumeroReciboRecaudo.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

		txtNumeroReciboRecaudo.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged (Editable e) {

			}



			@Override
			public void beforeTextChanged (CharSequence s, int start, int before, int count) {

			}



			@Override
			public void onTextChanged (CharSequence s, int start, int before, int count) {

				if (s.length() == 0) {

					txtNumeroReciboRecaudo.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

				} else {

					txtNumeroReciboRecaudo.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
				}
			}
		});
	}



	public static void SetTotalDescuento (float descuento) {

		lblOtrosDescuentos.setText("Otros Descuentos: " + Util.SepararMiles("" + descuento));
		Main.total_descuento = descuento;
	}



	public void OnClickAgregarCarteraRecaudo (View view) {

		/*retardo de 2000ms para evitar eventos de doble click.
		 * esto significa que solo se puede hacer click cada 2000ms.
		 * es decir despues de presionar el boton, se debe esperar que transcurran
		 * 1500ms para que se capture un nuevo evento.*/
		if (SystemClock.elapsedRealtime() - mLastClickTime < 3000){
			return;
		}
		mLastClickTime = SystemClock.elapsedRealtime();//inicializa esta variable en 0

		Intent intent = new Intent(this, FormCarteraInfoFacturas.class);
		startActivityForResult(intent, Const.RESP_FROM_AGREGAR_CARTERA);
	}

	public void OnClickHistorialRecaudo(View view){
		Intent intent = new Intent(this, FormCarteraListaHistorialRecaudo.class);
		startActivity(intent);
	}

	public void OnClickContinuarCartera (View view) {

		if (Main.cartera.size() > 0) {

			if (Main.total_descuento > Main.total_recaudo) {

				Util.MostrarAlertDialog(this, "Los descuentos son superiores al recaudo." + "\n\n" + "Agregue mas facturas o elimine descuentos");

			} else {

				Intent intent = new Intent(this, FormCarteraFormasPago.class);


				String strTotal = "" + (Main.total_recaudo - Main.total_descuento);
				strTotal = Util.QuitarE(strTotal.replace("e+", "E"));
				strTotal = Util.Redondear(strTotal, 2).replace(",", "");
				double valorNeto = Util.ToDouble(strTotal);

				intent.putExtra("codigoCliente", codCliente);
				intent.putExtra("totalRecuado", valorNeto);
				intent.putExtra("nroDoc", Main.encabezado.numero_doc);
				intent.putExtra("nroRecibo", 1);

				boolean guardo = guardarCarterasEnTablasTemporales();
				startActivityForResult(intent, Const.RESP_RECAUDO_EXITOSO);
			}
			//			}

		} else {

			Util.MostrarAlertDialog(this, "Debe ingresar primero las facturas");
		}
	}



	public void OnClickCancelarCartera (View view) {

		//		listadoCarteraRecaudar = DataBaseBO
		//				.CargarCarteraRecaudo("");

		if (Main.cartera.size() > 0) {

			AlertDialog alertDialog;

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {

				public void onClick (DialogInterface dialog, int id) {

					Main.total_recaudo = 0;
					Main.total_descuento = 0;
					Main.total_forma_pago = 0;

					Main.cartera.clear();
					Main.listaDescuentos.clear();
					Main.listaFormasPago.clear();

					Main.consignacion = false;

					DataBaseBO.borrarObjeto();
					Main.cartera.removeAllElements();
					//					Main.carterasRelacionadas.removeAllElements();

					dialog.cancel();
					FormCarteraFacturas.this.finish();
				}
			}).setNegativeButton("No", new DialogInterface.OnClickListener() {

				public void onClick (DialogInterface dialog, int id) {

					dialog.cancel();
				}
			});

			alertDialog = builder.create();
			alertDialog.setMessage("Esta Seguro de Cancelar el Recaudo para este cliente?");
			alertDialog.show();

		} else {
			DataBaseBO.borrarObjeto();
			this.finish();
		}
	}



	public void cargarListaRecaudo () {

		celuweb.com.DataObject.ItemListView[] listaItems;
		ItemListView itemListView;
		Vector<ItemListView> items = new Vector<ItemListView>();
		float total_recaudo = 0;

		for (Cartera cartera : Main.cartera) {

			total_recaudo += cartera.valorARecaudar;
			String strValorARecaudar = "" + cartera.valorARecaudar;
			strValorARecaudar = Util.QuitarE(strValorARecaudar.replace("e+", "E"));

			itemListView = new celuweb.com.DataObject.ItemListView();
			itemListView.referencia = cartera.referencia;
			itemListView.documento = cartera.documento;
			itemListView.titulo = "Doc: " + cartera.referencia + "  Saldo: " + Util.SepararMiles(Util.Redondear("" + cartera.saldo, 2));
			//itemListView.subTitulo = "Dias: " + cartera.dias + ". Vecimiento: " + cartera.FechaVecto;
			itemListView.subTitulo = "Valor Recaudo: " + Util.SepararMiles(Util.Redondear(strValorARecaudar, 2));
			items.addElement(itemListView);
		}

		Main.total_recaudo = total_recaudo;

		//if (total > 0) {

		String strTotal = "" + (Main.total_recaudo - Main.total_descuento);
		strTotal = Util.QuitarE(strTotal.replace("e+", "E"));

		//TextView lblValorRecaudado = (TextView)findViewById(R.id.lblValorRecaudado);
		//lblValorRecaudado.setText("Total: " + Util.SepararMiles(strTotal));

		EditText txtValorARecaudar = (EditText) findViewById(R.id.txtTotalDelRecaudo);
		txtValorARecaudar.setText(Util.SepararMiles(Util.Redondear(strTotal, 2)));

		//} else {

		//TextView lblValorRecaudado = (TextView)findViewById(R.id.lblValorRecaudado);
		//lblValorRecaudado.setText("Total: $0");
		//}

		if (items.size() > 0) {

			listaItems = new ItemListView[items.size()];
			items.copyInto(listaItems);

			ListAdapter adapter = new ListAdapter(this, listaItems, 0, R.layout.list_item_trans);
			ListView listaPedido = (ListView) findViewById(R.id.listaCarteraRecaudo);
			listaPedido.setAdapter(adapter);

		} else {

			ListAdapter adapter = new ListAdapter(this, new ItemListView[]{}, 0, R.layout.list_item_trans);
			ListView listaPedido = (ListView) findViewById(R.id.listaCarteraRecaudo);
			listaPedido.setAdapter(adapter);
		}
	}
	
	
	/*
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == Const.RESP_FROM_AGREGAR_CARTERA && resultCode == RESULT_OK) {
			
			cargarListaRecaudo();
		}
		
		if (requestCode == Const.RESP_RECAUDO_EXITOSO && resultCode == RESULT_OK) {
			
			TabActivity ta = (TabActivity) this.getParent();
		    ta.setResult(RESULT_OK);
			ta.finish();
		}
	}*/



	public void MostrarDialogEdicion () {

		if (dialogEditar == null) {

			dialogEditar = new Dialog(this);
			dialogEditar.setContentView(R.layout.dialog_edit_cartera);
			dialogEditar.setTitle("Opciones Edicion");


			((RadioButton) dialogEditar.findViewById(R.id.radioEliminar)).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick (View v) {

					if (carteraSel != null) {

						AlertDialog.Builder builder = new AlertDialog.Builder(FormCarteraFacturas.this);
						builder.setMessage("Esta Seguro de eliminar el recaudo " + carteraSel.referencia).setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {


							public void onClick (DialogInterface dialog, int id) {
								//
								//								//buscar elementos para quitar de la cartera
								ArrayList<Cartera> listaBorrar = new ArrayList<Cartera>();
								for (Cartera cartera : Main.cartera) {
									/*if ((carteraSel.saldo > 0) && cartera.referencia.equals(carteraSel.referencia)) {
										listaBorrar.add(cartera);
									} else if (cartera.documento.equals(carteraSel.documento)) {
										listaBorrar.add(cartera);
									}*/
									if (cartera.documento.equals(carteraSel.documento)) {
										listaBorrar.add(cartera);
									}
								}

								//quitar los elementos encontrados.
								for (Cartera carteraBorrar : listaBorrar) {
									Main.cartera.remove(carteraBorrar);
									/*borrar la cartera de la tabla temporal*/
									DataBaseBO.borrarCarteraTablaTemporalCarteraProntoPagoConsig(carteraBorrar.documento);
									DataBaseBO.quitarFacturaRecaudo(carteraBorrar);
								}

								listaBorrar.clear();
								listaBorrar = null;
								Util.guardarDatosDeRecaudo();
								refrescarDatosRecaudo();
								cargarListaRecaudo();
								dialogEditar.cancel();
							}
						}).setNegativeButton("No", new DialogInterface.OnClickListener() {

							public void onClick (DialogInterface dialog, int id) {

								dialogEditar.cancel();
							}
						});

						AlertDialog alert = builder.create();
						alert.show();

					} else {

						Util.MostrarAlertDialog(FormCarteraFacturas.this, "Error obteniendo la informacion de la Cartera");
					}
				}
			});

			((RadioButton) dialogEditar.findViewById(R.id.radioCancelar)).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick (View v) {

					dialogEditar.cancel();
				}
			});

		} else {

			((RadioButton) dialogEditar.findViewById(R.id.radioEliminar)).requestFocus();
		}

		dialogEditar.show();
	}



	public void SetListenerListView () {

		ListView listView = (ListView) findViewById(R.id.listaCarteraRecaudo);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick (AdapterView<?> parent, View view, int position, long id) {

				Date date = new Date();

				long tiempoClick2 = date.getTime();

				long dif1 = tiempoClick2 - tiempoClick1;

				if (dif1 < 0) {

					dif1 = dif1 * (-1);

				}


				if (dif1 > 2000) {


					tiempoClick1 = tiempoClick2;


					ListAdapter adapter = (ListAdapter) parent.getAdapter();
					ItemListView itemListView = adapter.listItems[position];
					String documento = itemListView.documento;

					int i = 0;
					carteraSel = null;

					for (Cartera cartera : Main.cartera) {

						if (cartera.documento.equals(documento)) {

							carteraSel = Main.cartera.elementAt(i);
							break;
						}

						i++;
					}

					MostrarDialogEdicion();

				}
			}//
		});
	}



	@Override
	public boolean onKeyDown (int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}



	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data) {

		if (requestCode == Const.RESP_RECAUDO_EXITOSO && resultCode == RESULT_OK) {

			setResult(RESULT_OK);
			finish();
		} else if (requestCode == Const.RESP_FROM_AGREGAR_CARTERA && resultCode == RESULT_OK) {

			cargarListaRecaudo();
			Util.guardarDatosDeRecaudo();
			refrescarDatosRecaudo();
		}


	}



	@Override
	protected void onResume () {

		super.onResume();
		refrescarDatosRecaudo();
	}



	public void refrescarDatosRecaudo () {

		if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {
			DataBaseBO.CargarInfomacionUsuario();
		}

		if (Main.cliente == null || Main.cliente.codigo == null) {

			int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
			Cliente clienteSel;

			if (tipoDeClienteSelec == 1) {

				clienteSel = DataBaseBO.CargarClienteSeleccionado();

			} else {

				clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();
			}

			if (clienteSel != null)
				Main.cliente = clienteSel;
		}

		objeto obj = DataBaseBO.obtenerObjeto();

		if (obj != null) {

			Main.cartera = obj.cartera;
			Main.listaFormasPago = obj.listaFormasPago;
			Main.total_forma_pago = obj.total_forma_pago;
			Main.total_descuento = obj.total_descuento;
			Main.total_recaudo = obj.total_recaudo;
		}

		cargarListaRecaudo();
	}



	public void mostrarConsecutivo () {

		String consecutivo = getIntent().getExtras().getString("ConsecutivoActual");

		if (consecutivo != null) {

			EditText txtNumeroReciboRecaudo = (EditText) findViewById(R.id.txtNumeroReciboRecaudo);
			txtNumeroReciboRecaudo.setText(consecutivo + "");

		} else {

			RecibosRecaudo recibosRecaudo = DataBaseBO.BuscarRecibo();

			if (recibosRecaudo != null) {

				EditText txtNumeroReciboRecaudo = (EditText) findViewById(R.id.txtNumeroReciboRecaudo);
				txtNumeroReciboRecaudo.setText(recibosRecaudo.actual + "");

			}


		}

	}



	/**
	 * Metodo que permite insertar en la tabla temporal
	 * tmpdetallerecaudo, las carteras que el usuario selecciono para recaudo.
	 * @return true si se insertaron correctamente, false caso contrario
	 */
	private boolean guardarCarterasEnTablasTemporales () {
		/*Cargar imei del dispositivo*/
		String imei = obtenerImei();
		
		/*se guardan las carteras que estan en la lista de carteras*/
		return DataBaseBO.guardarCarterasEnTablasTemporales(Main.cartera, imei);
	}



	/**
	 * Obtener IMEI del dispositivo.
	 * @return
	 */
	public String obtenerImei () {

		TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			return "NoPermissionToRead";
		}
		String deviceId = manager.getDeviceId();

		if (deviceId == null) {

			WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

			if (wifiManager != null) {

				WifiInfo wifiInfo = wifiManager.getConnectionInfo();

				if (wifiInfo != null) {

					String mac = wifiInfo.getMacAddress();

					if (mac != null) {

						deviceId = mac.replace(":", "").toUpperCase(Locale.getDefault());
					}
				}
			}
		}

		return deviceId;
	}
	
}
