package celuweb.com.uyusa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.DataBaseBOJ;
import celuweb.com.DataObject.Canal;
import celuweb.com.DataObject.Ciudad;
import celuweb.com.DataObject.ClienteNuevo;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.Foto;
import celuweb.com.DataObject.Ruta;
import celuweb.com.DataObject.SubCanal;

public class FormClienteNuevo2 extends Activity implements Sincronizador, OnClickListener {
    Dialog dialogRuta;
    ProgressDialog progressDialog, progressDialog2;

    Vector<Ciudad> listaCiudades;
    Vector<Canal> listaCanales;
    Vector<SubCanal> listaSubCanales;
    Vector<Ruta> listaRutas;

    int numeroIntentos = 0;
    ClienteNuevo clienteNuevo;
    Button btnGuardar;
    private Button btnCedula;
    private ImageButton btnEliminarCedula;
    int anchoImg, altoImg;
    boolean estaGuardando = false, fotoGuardada = false;
    AlertDialog alertDialog;
    private String numeroDocumento;

    private Dialog dialogoCoordenadas;
    public double latitud;
    public double longitud;
    private Handler timer = new Handler();
    GPSListener gpsListener;
    LocationManager locationManager;
    Location currentLocation = null;
    Location currentLocation2 = null;

    int guardar = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_cliente_nuevo2);
        numeroIntentos = 0;

        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        cargarDatos();
        cargarCanales();
        cargarListas();
        cargarDias();
        btnCedula = (Button) findViewById(R.id.btnTomarFotoCedula);
        btnCedula.setOnClickListener(this);
        btnEliminarCedula = (ImageButton) findViewById(R.id.btnElimiarFotoCedula);
        btnEliminarCedula.setOnClickListener(this);
        numeroDocumento = Util.generarNumdoc("MCN", Main.cliente.codigo, Main.usuario.codigoVendedor);
//		cargarRutas();
        inicializar();
    }

    private void inicializar() {

        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int ancho = display.getWidth();
        int alto = display.getHeight();

        anchoImg = (ancho * 150) / 140;
        altoImg = (alto * 180) / 220;

        if (Main.fotoActual != null) {
            ((ImageView) findViewById(R.id.imageFotoCedula)).setImageDrawable(Main.fotoActual);
        } else {
            SetPhotoDefault();
        }

    }

    public void CargarCiudad() {

        String[] items;
        Vector<String> listaCiudad = DataBaseBO.ListaCiudadCliente();

        if (listaCiudad.size() > 0) {
            items = new String[listaCiudad.size()];
            listaCiudad.copyInto(items);
        } else {
            items = new String[]{};
            if (listaCiudad != null)
                listaCiudad.removeAllElements();
        }

    }

    public void OnClickSeleccionarRuta(View view) {
        MostrardialogRuta();
    }


    /**
     * Coordenadas
     **/

    public void onClickCoordenadas(View view) {
        guarDarCoordenada();
    }

    private void guarDarCoordenada() {
        if (!DataBaseBOJ.hayCoordenadas(Main.cliente.codigo)) {
            mostrarDialogoCoordenadasCliente();
        } else {
            MostrarAlertDialog("El cliente ya cuenta con coordenadas, si desea modificarla por favor contacte a su administrador!");
        }
    }

    private void mostrarDialogoCoordenadasCliente() {

        if (dialogoCoordenadas == null) {
            dialogoCoordenadas = new Dialog(this);
            dialogoCoordenadas.setContentView(R.layout.dialog_coordenadas_cliente);
            ((ImageView) dialogoCoordenadas.findViewById(R.id.btnCoordenadas)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    capturarCoordenada();
                }
            });
            ((Button) dialogoCoordenadas.findViewById(R.id.btnAceptarResumenPedido)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Coordenada coordenada = new Coordenada();
                    coordenada.codigoCliente = Main.cliente.codigo;
                    coordenada.latitud = latitud;
                    coordenada.longitud = longitud;
                    if (coordenada.latitud == 0 && coordenada.longitud == 0) {
                        MostrarAlertDialog("Pulse el icono para capturar coordenadas!");
                    } else {

                        if (DataBaseBOJ.guardarCoordenadaClienteNuevo(coordenada)) {
                            MostrarAlertDialog("Coordenadas almacenadas con exito");
                            dialogoCoordenadas.dismiss();
                        } else {
                            MostrarAlertDialog("Error al registrar informacion");
                        }

                    }
                }
            });
            ((Button) dialogoCoordenadas.findViewById(R.id.btnCancelarResumenPedido)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogoCoordenadas.dismiss();
                    // finish();
                }
            });

        }

        dialogoCoordenadas.setCancelable(false);
        dialogoCoordenadas.show();

    }

    public void capturarCoordenada() {
        Coordenada.delete(this);
        EncenderServGps();
        progressCoordenada();
        if (isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            //Verifica si NETWORK_PROVIDER esta Activo. En caso tal Registra el
            // Listener de Captura de Coordenadas con NETWORK_PROVIDER
            registrarListenerGPS(LocationManager.NETWORK_PROVIDER);

        } else if (isProviderEnabled(LocationManager.GPS_PROVIDER)) { //
            // Verifica si GPS_PROVIDER esta Activo. En caso tal Registra el
            // Listener de Captura de Coordenadas con GPS_PROVIDER
            registrarListenerGPS(LocationManager.GPS_PROVIDER);

        } else {
            /**
             * Se guarda seguimiento indicando que el GPS esta apagado!
             **/
            DataBaseBO.validarUsuario();
            if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
                Coordenada coordenada = new Coordenada();
                coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                coordenada.codigoCliente = Main.cliente.codigo;
                coordenada.latitud = currentLocation.getLatitude();
                coordenada.longitud = currentLocation.getLongitude();
                coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                coordenada.estado = Coordenada.ESTADO_GPS_APAGADO;
                coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                Coordenada.save(this, coordenada);
            }
        }
    }

    public void registrarListenerGPS(String provider) {
        try {
            setGPSProvider(provider);
            //				String provider = getGPSProvider();
            if (provider != null && !provider.equals("")) {
                if (locationManager == null)
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (gpsListener == null) gpsListener = new GPSListener();
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(provider, 0, 0, gpsListener);

            } else {
                Log.e("****", "registrarListenerGPS -> No se encontro el Provider");
            }

        } catch (Exception e) {
            Log.e("***", "registrarListenerGPS -> " + e.getMessage(), e);
        }
    }

    private class GPSListener implements LocationListener {

        public void onLocationChanged(Location location) {
            if (location != null) {
                location.getLatitude();
                location.getLongitude();
                currentLocation = location;
                handler.sendEmptyMessage(0);
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    public void setGPSProvider(String provider) {
        SharedPreferences settings = getSharedPreferences("settings_gps", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("provider", provider);
        editor.commit();
    }

    protected void EncenderServGps() {
        // Se valida si se puede Activar el GPS
        boolean encender = validarDisponibilidad();
        if (encender) {
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (provider != null && !provider.contains("gps")) {
                final Intent intent = new Intent();
                intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
                intent.setData(Uri.parse("3"));
                sendBroadcast(intent);
            }
        }
    }

    public boolean isProviderEnabled(String provider) {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager != null) {
            return manager.isProviderEnabled(provider);
        }
        return false;
    }

    private boolean validarDisponibilidad() {
        PackageManager packageManager = getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);

        } catch (PackageManager.NameNotFoundException e) {
            // Paquete no encontrado
            return false;
        }
        if (packageInfo != null) {
            for (ActivityInfo actInfo : packageInfo.receivers) {
                String name = actInfo.name;
                boolean exported = actInfo.exported;
                // Verifica si el receiver es exported. En caso tal se puede
                // actiar el GPS.
                if (name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && exported) {
                    return true;
                }
            }
        }
        return false;
    }

    public void progressCoordenada() {
        progressDialog2 = new ProgressDialog(FormClienteNuevo2.this);
        progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog2.setMessage("Obteniendo coordenadas...");
        progressDialog2.setCancelable(false);
        progressDialog2.show();

        timer.postDelayed(forzarCierre, 1000 * 50); // 90 segundos (1.5 minutos) antes de ejecutarse el cierre. (tiempo en milisegundos)
    }

    public Runnable forzarCierre = new Runnable() {

        public boolean killMe = false;

        public void run() {

            if (killMe)
                return;

            if (locationManager != null && gpsListener != null)
                locationManager.removeUpdates(gpsListener); // remover el listener para evitar capturas basura.

            progressDialog2.dismiss();

            if (latitud == 0 && longitud == 0) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "No se pudo obtener la coordenada", Toast.LENGTH_SHORT).show();
                        progressDialog2.dismiss();
                    }
                });
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

            } else {

                currentLocation = currentLocation2;
                handler.sendEmptyMessage(0);
                ((TextView) dialogoCoordenadas.findViewById(R.id.labLatitud)).setText("" + latitud);
                ((TextView) dialogoCoordenadas.findViewById(R.id.lblLongitud)).setText("" + longitud);

            }
        }

        void killRunnable() {
            killMe = true;
        }

    };

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (locationManager != null && gpsListener != null) {
                locationManager.removeUpdates(gpsListener);
            }
            if (currentLocation != null && guardar == 0) {
                DataBaseBO.validarUsuario();
                if (Main.usuario != null && Main.usuario.codigoVendedor != null) {
                    Coordenada coordenada = new Coordenada();
                    coordenada.codigoVendedor = Main.usuario.codigoVendedor;
                    coordenada.codigoCliente = Main.cliente.codigo;
                    coordenada.latitud = currentLocation.getLatitude();
                    coordenada.longitud = currentLocation.getLongitude();
                    coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
                    coordenada.estado = Coordenada.ESTADO_GPS_CAPTURO;
                    coordenada.id = Coordenada.obtenerId(Main.usuario.codigoVendedor);
                    latitud = currentLocation.getLatitude();
                    longitud = currentLocation.getLongitude();
                    Log.i("COORDENADA CLIENTE", currentLocation.getLatitude() + "--" + currentLocation.getLongitude());
                    //progressDialog2.cancel();
                    boolean guardo = Coordenada.save(FormClienteNuevo2.this, coordenada);
                    if (guardo) {
                        Log.i("GUARDO", "OK");
                        guardar = -1;
                    }
                }
            }
        }
    };


    /**
     *
     */

    public void OnClickGuardarCliente(View view) {

        btnGuardar = (Button) findViewById(R.id.btnGuardarCN);

        if (((EditText) findViewById(R.id.txtOrden)).getText().toString().equals("")) {
            clienteNuevo.ordenVisita = 0;
            Main.cliente.ordenVisita = clienteNuevo.ordenVisita;
        } else {
            clienteNuevo.ordenVisita = Util.ToInt(((EditText) findViewById(R.id.txtOrden)).getText().toString().trim());
            Main.cliente.ordenVisita = clienteNuevo.ordenVisita;
        }

        clienteNuevo.observacion = ((EditText) findViewById(R.id.lblObservacion)).getText().toString();

        Spinner spListaPrecio = (Spinner) findViewById(R.id.cbLista);
        String txtLista = spListaPrecio.getSelectedItem().toString();

        clienteNuevo.listaPrecio = txtLista.substring(0, txtLista.indexOf("-"));
        Main.cliente.listaPrecio = clienteNuevo.listaPrecio;
        Main.cliente.CodigoAmarre = clienteNuevo.listaPrecio;

        Spinner spCanales = (Spinner) findViewById(R.id.cbCanal);
        int posCanal = spCanales.getSelectedItemPosition();
        Canal canal = listaCanales.elementAt(posCanal);

        clienteNuevo.Canal = canal.codigo + "";
        Main.cliente.Canal = clienteNuevo.Canal;

        Spinner spSubCanales = (Spinner) findViewById(R.id.cbSubCanal);
        int posSubCanal = spSubCanales.getSelectedItemPosition();

        SubCanal subCanal;
        clienteNuevo.SubCanal = "";
        Main.cliente.SubCanal = "";

        if (posSubCanal != -1) {
            subCanal = listaSubCanales.elementAt(posSubCanal);
            clienteNuevo.SubCanal = subCanal.codigo + "";
            Main.cliente.SubCanal = clienteNuevo.SubCanal;
        }

        clienteNuevo.vendedor = Main.usuario.codigoVendedor;

        Main.cliente.Bloqueado = "N";
        Main.cliente.tipologia = "";
        Main.cliente.diasCreacion = 0;


        String txtOrdenVisita = clienteNuevo.ordenVisita + "";
        if (txtOrdenVisita.length() == 2)
            txtOrdenVisita = "0" + txtOrdenVisita;
        else if (txtOrdenVisita.length() == 1)
            txtOrdenVisita = "00" + txtOrdenVisita;

        String rutaParada = "";

        clienteNuevo.ruta_parada = rutaParada + txtOrdenVisita;
        Main.cliente.rutaParada = clienteNuevo.ruta_parada;
        Main.cliente.AcomuladoBonif = "0";
        clienteNuevo.AcomuladoBonif = "0";
        Main.cliente.Canal = clienteNuevo.Canal;
        Main.cliente.codigo = clienteNuevo.codigo;
        Main.cliente.bodega = clienteNuevo.SubCanal;
        clienteNuevo.numeroDoc = numeroDocumento;

        /**    if (!fotoGuardada) {
         Toast.makeText(this, "Se debe ingresar una foto", Toast.LENGTH_SHORT).show();
         return;
         }**/

        if (DataBaseBO.GuardarClienteNuevo(clienteNuevo)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(FormClienteNuevo2.this);
            builder.setMessage("Se ha registrado la informacion del cliente nuevo de forma exitosa?")

                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                            /****Intent formInfoCliente = new Intent(FormClienteNuevo1.this, FormInfoClienteActivity.class);
                             startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);*****/

                            celuweb.com.DataObject.Usuario usuario = DataBaseBO.CargarUsuario();
                            if (usuario == null) {
                                Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del usuario", Toast.LENGTH_LONG).show();
                            } else {
                                DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 0);
                                FormClienteNuevo2.this.setResult(RESULT_OK);
                                Intent formInfoCliente = new Intent(FormClienteNuevo2.this, FormInfoClienteActivity.class);
                                formInfoCliente.putExtra("clienteNuevo", true);
                                finish();
                                startActivityForResult(formInfoCliente, Const.RESP_CLIENTE_NUEVO);
                            }

                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            FormClienteNuevo2.this.setResult(RESULT_OK);
                            dialog.cancel();
                            finish();

                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

        } else {
            Util.MostrarAlertDialog(this, "Fallo el Registro de Informacion del Cliente Nuevo");
            btnGuardar.setEnabled(true);
        }

    }

    public String ObtenerRutaParada() {

        String rutaParada = "";

        if (dialogRuta != null) {

            boolean lunes = ((CheckBox) dialogRuta.findViewById(R.id.checkLunes)).isChecked();
            boolean martes = ((CheckBox) dialogRuta.findViewById(R.id.checkMartes)).isChecked();
            boolean miercoles = ((CheckBox) dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
            boolean jueves = ((CheckBox) dialogRuta.findViewById(R.id.checkJueves)).isChecked();
            boolean viernes = ((CheckBox) dialogRuta.findViewById(R.id.checkViernes)).isChecked();
            boolean sabado = ((CheckBox) dialogRuta.findViewById(R.id.checkSabado)).isChecked();
            boolean domingo = ((CheckBox) dialogRuta.findViewById(R.id.checkDomingo)).isChecked();

            rutaParada += lunes ? "1" : "0";
            rutaParada += martes ? "1" : "0";
            rutaParada += miercoles ? "1" : "0";
            rutaParada += jueves ? "1" : "0";
            rutaParada += viernes ? "1" : "0";
            rutaParada += sabado ? "1" : "0";
            rutaParada += domingo ? "1" : "0";
        }

        return rutaParada;
    }

    public void MostrardialogRuta() {

        if (dialogRuta == null) {

            dialogRuta = new Dialog(this);
            dialogRuta.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialogRuta.setContentView(R.layout.dialog_dias_ruta);
            dialogRuta.setTitle("Ingresar Ruta");
        }

        ((Button) dialogRuta.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                boolean lunes = ((CheckBox) dialogRuta.findViewById(R.id.checkLunes)).isChecked();
                boolean martes = ((CheckBox) dialogRuta.findViewById(R.id.checkMartes)).isChecked();
                boolean miercoles = ((CheckBox) dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
                boolean jueves = ((CheckBox) dialogRuta.findViewById(R.id.checkJueves)).isChecked();
                boolean viernes = ((CheckBox) dialogRuta.findViewById(R.id.checkViernes)).isChecked();
                boolean sabado = ((CheckBox) dialogRuta.findViewById(R.id.checkSabado)).isChecked();
                boolean domingo = ((CheckBox) dialogRuta.findViewById(R.id.checkDomingo)).isChecked();

                String ruta = "";
                ruta += lunes ? "Lunes" : "";
                ruta += martes ? (ruta.equals("") ? "" : ", ") + "Martes" : "";
                ruta += miercoles ? (ruta.equals("") ? "" : ", ") + "Miercoles" : "";
                ruta += jueves ? (ruta.equals("") ? "" : ", ") + "Jueves" : "";
                ruta += viernes ? (ruta.equals("") ? "" : ", ") + "Viernes" : "";
                ruta += sabado ? (ruta.equals("") ? "" : ", ") + "Sabado" : "";
                ruta += domingo ? (ruta.equals("") ? "" : ", ") + "Domingo" : "";

                String rutaParada = Util.SepararPalabrasTextView(ruta, 30);

                dialogRuta.cancel();
            }
        });

        ((Button) dialogRuta.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                dialogRuta.cancel();
            }
        });

        dialogRuta.setCancelable(false);
        dialogRuta.show();
        dialogRuta.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_list);
    }

    public void OnClickCancelarPedido(View view) {
        finish();
    }

    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

        switch (codeRequest) {

            case Const.ENVIAR_PEDIDO:

                final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : msg;

                if (progressDialog != null)
                    progressDialog.cancel();

                this.runOnUiThread(new Runnable() {

                    public void run() {
				
				/*
				
				AlertDialog.Builder builder = new AlertDialog.Builder(FormClienteNuevo1.this);
				builder.setMessage(mensaje)
						
				.setCancelable(false)
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
					   
					
						dialog.cancel();
						
						Usuario usuario = DataBaseBO.CargarUsuario();
						
						if (usuario == null) {
							
							
							Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del Usuario", Toast.LENGTH_LONG).show();

							
						}else{
						
							
							DataBaseBO.GuardarCodPdv(Main.cliente.codigo,0);
							
							
							
							FormClienteNuevo.this.setResult(RESULT_OK);
							Intent formInfoCliente = new Intent(FormClienteNuevo.this, FormInfoClienteActivity.class);
							startActivityForResult(formInfoCliente,Const.RESP_CLIENTE_NUEVO);
						
						
						
						}
						
					
					
					}
				});
				
				AlertDialog alert = builder.create();
				alert.show();*/
                    }
                });


                break;
		
		
		/*
		case Const.VALIDAR_NIT:
			
		
			if (progressDialog != null)
				progressDialog.cancel();
			
			numeroIntentos++;
			
			if(respuestaServer.equals("Disponible")){
				
				
				guardarYEnviarClienteNuevo();
				
				
				
			}else{
				
			
				
			
				
				if(respuestaServer.equals("No")){
					
					Looper.prepare();
					Util.MostrarAlertDialog2(this, "Ya Existe en el Sistema Un Cliente con Igual Nit",1);
					Looper.loop();
					
					
					
				}else{
				
					Looper.prepare();
					Util.MostrarAlertDialog2(this, "Se detecto un Problema al Momento de Verificar el Nit "+msg,1);
					Looper.loop();
				
				
				}
				
				
			}	
			

			
		break;*/


        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.RESP_TOMAR_FOTO && resultCode == RESULT_OK) {

            Main.fotoActual = ResizedImage(anchoImg, altoImg);

            if (Main.fotoActual != null) {

                Main.guardarFoto = true;
                ImageView imgFoto = (ImageView) findViewById(R.id.imageFotoCedula);
                imgFoto.setImageDrawable(Main.fotoActual);
                OnClickGuardarFoto();
            }

        }
    }

    public void validarNit() {
		
		/*
		progressDialog = ProgressDialog.show(FormClienteNuevo.this, "", "Validando Nit...", true);
		progressDialog.show();
		
		
		btnGuardar.setEnabled(true);
		
		
		Sync sync = new Sync(FormClienteNuevo1.this, Const.VALIDAR_NIT);
		sync.nit = Main.cliente.nit;

		sync.start();*/

    }

    public void guardarYEnviarClienteNuevo() {


        boolean ingreso = DataBaseBO.GuardarClienteNuevo(clienteNuevo);

        Looper.prepare();

        if (ingreso) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Cliente Registrado con Exito")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
					/*
					progressDialog = ProgressDialog.show(FormClienteNuevo.this, "", "Enviando Informacion Cliente...", true);
					progressDialog.show();
					
						
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					Sync sync = new Sync(FormClienteNuevo.this, Const.ENVIAR_PEDIDO);
					sync.start();*/
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

            Looper.loop();


        }

    }

    public void guardarYEnviarClienteNuevo2() {


        boolean ingreso = DataBaseBO.GuardarClienteNuevo(clienteNuevo);


        if (ingreso) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Cliente Registrado con Exito")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
					/*
					
					progressDialog = ProgressDialog.show(FormClienteNuevo.this, "", "Enviando Informacion Cliente...", true);
					progressDialog.show();
					
					
					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();
					
					dialog.cancel();
					
					Sync sync = new Sync(FormClienteNuevo.this, Const.ENVIAR_PEDIDO);
					sync.start();*/
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();


        }

    }

    public void cargarCiudades() {


        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();
        listaCiudades = DataBaseBO.ListaCiudades(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbCiudad);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


    }

    public void cargarCanales() {


        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();
        listaCanales = DataBaseBO.ListaCanales(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbCanal);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here

                Canal canal = listaCanales.elementAt(position);
                cargarSubCanales(canal.codigo);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here


            }

        });


    }

    public void cargarSubCanales_(String canal) {


        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();
        listaSubCanales = DataBaseBO.ListaSubCanales(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbSubCanal);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


    }

    public void cargarSubCanales(String canal) {


        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();
        listaSubCanales = DataBaseBO.ListaSubCanales(listaItems, canal);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbSubCanal);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

    }

    public void cargarListas() {

        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();

        listaItems.add("L1-L1");

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbLista);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


    }

    public void cargarDias() {

        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();

        listaItems.add("Lunes");
        listaItems.add("Martes");
        listaItems.add("Miercoles");
        listaItems.add("Jueves");
        listaItems.add("Viernes");
        listaItems.add("Sabado");
        listaItems.add("Domingo");

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.cbDia);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


    }

    public void OnClickCancelarClienteNuevo(View v) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Desea Salir de Este Formulario?")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        finish();
                    }
                })

                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        AlertDialog alert = builder.create();
        alert.show();

    }

    public void cargarRutas() {


        ArrayAdapter<String> adapter;
        Vector<String> listaItems = new Vector<String>();
        listaRutas = DataBaseBO.ListaRutas(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        } else {

            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
        }

        Spinner spinner = (Spinner) findViewById(R.id.comboRutaClientes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


    }

    public void cargarDatos() {

        Bundle bundle = this.getIntent().getExtras();
        Object Cli = bundle.get("clientenuevo");

        if (Cli != null && Cli instanceof ClienteNuevo) {

            clienteNuevo = (ClienteNuevo) Cli;

        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnTomarFotoCedula:

                OnClickTomarFoto();
                break;

            case R.id.btnElimiarFotoCedula:
                eliminarFoto();
                SetPhotoDefault();
                break;

        }

    }

    public void OnClickTomarFoto() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String filePath = Util.DirApp().getPath() + "/foto.jpg";
        Uri output = Uri.fromFile(new File(filePath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, output);

        startActivityForResult(intent, Const.RESP_TOMAR_FOTO);

    }

    private void eliminarFoto() {
        if (DataBaseBO.borrarFotoSeleccionada(numeroDocumento, Main.cliente.codigo, Main.usuario.codigoVendedor)) {
            MostrarAlertDialog("Foto eliminada con ?xito");

        }
    }

    public void SetPhotoDefault() {

        Drawable fotoVacia = getResources().getDrawable(R.drawable.foto_vacia);
        Drawable img = Util.ResizedImage(fotoVacia, anchoImg, altoImg);

        if (img != null)
            ((ImageView) findViewById(R.id.imageFotoCedula)).setImageDrawable(img);

    }

    public Drawable ResizedImage(int newWidth, int newHeight) {

        Matrix matrix;
        FileInputStream fd = null;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;

        try {

            File fileImg = new File(Util.DirApp(), "foto.jpg");

            if (fileImg.exists()) {

                fd = new FileInputStream(fileImg.getPath());
                bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());

                int width = bitmapOriginal.getWidth();
                int height = bitmapOriginal.getHeight();

                if (width == newWidth && height == newHeight) {

                    return new BitmapDrawable(bitmapOriginal);
                }

                // Reescala el Ancho y el Alto de la Imagen
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                // Crea la Imagen con el nuevo Tamano
                resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);

                return new BitmapDrawable(resizedBitmap);
            }

            return null;

        } catch (Exception e) {

            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
            return null;

        } finally {

            if (fd != null) {

                try {

                    fd.close();

                } catch (IOException e) {
                }
            }

            fd = null;
            matrix = null;
            resizedBitmap = null;
            bitmapOriginal = null;
            System.gc();
        }
    }

    public void OnClickGuardarFoto() {

        if (!estaGuardando) {

            if (Main.guardarFoto) {


                String mensaje;
                Drawable imgFoto = ResizedImage(320, 480);

                if (imgFoto != null) {

                    Bitmap bitmap = ((BitmapDrawable) imgFoto).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byte[] byteArray = stream.toByteArray();

                    if (byteArray != null && byteArray.length > 0) {

                        Foto foto = new Foto();
                        foto.id = Util.ObtenerFechaId();
                        foto.codCliente = Main.cliente.codigo;
                        foto.codVendedor = Main.usuario.codigoVendedor;
                        foto.modulo = 3;
                        foto.nroDoc = numeroDocumento;

                        if (DataBaseBO.GuardarImagen(foto, byteArray)) {

                            Main.guardarFoto = false;
                            imgFoto = ResizedImage(anchoImg, altoImg);

                            if (imgFoto != null) {

                                Drawable imgGaleria = ResizedImage(30, 30);
                                Main.fotosGaleria.addElement(imgGaleria);
                                Main.listaInfoFotos.addElement(foto);

                                mensaje = "Foto Guardada con Exito";
                                fotoGuardada = true;
                                Main.fotoActual = null;
                                System.gc();

                            } else {

                                mensaje = "Foto Guardada con Exito, Error Visualizando la Img.";
                            }

                        } else {

                            mensaje = "Error guardando la Imagen: " + DataBaseBO.mensaje;
                        }

                    } else {

                        mensaje = "Error procesando la Imagen 2";
                    }

                } else {

                    mensaje = "Error procesando la Imagen 1";
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        //((ImageAdapter)(((Gallery) findViewById(R.id.galleryFotos))).getAdapter()).notifyDataSetChanged();

                    }
                });

                AlertDialog alert = builder.create();
                alert.setMessage(mensaje);
                alert.show();

            } else {

                MostrarAlertDialog("Debe capturar la imagen antes de guardarla");
            }

            estaGuardando = true;
        }
    }

    public void MostrarAlertDialog(String mensaje) {

        if (alertDialog == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                }
            });

            alertDialog = builder.create();
        }

        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }
}
