package celuweb.com.uyusa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Receiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		if (intent.getAction().equals(Intent.ACTION_DATE_CHANGED)) {
			
			Log.v("Receiver Class", "ACTION_DATE_CHANGED received");
		}
	}
}

