package celuweb.com.uyusa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Canal;
import celuweb.com.DataObject.Ciudad;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.ClienteNuevo;
import celuweb.com.DataObject.Ruta;
import celuweb.com.DataObject.SubCanal;
import celuweb.com.keyboard.custom.CustomKeyboard;


public class FormClienteNuevo1 extends Activity implements Sincronizador 
{
	Dialog dialogRuta;
	ProgressDialog progressDialog;
	private ProgressDialog progresValidarCliente;
	ProgressDialog progressDialogLanzarLector;

	boolean botonBloqueado = false;


	private static boolean validado;

	private TableLayout tableLayoutClienteNuevo;
	Vector<Ciudad> listaCiudades;
	Vector<Canal> listaCanales;
	Vector<SubCanal> listaSubCanales;
	Vector<Ruta> listaRutas;
	int numeroDeIntentosLector = 1;
	long mLastClickTime = 0;
	String fechaNCN = "";
	String nombreCN = "";
	boolean lectura = false;


	int numeroIntentos = 0;

	ClienteNuevo clienteNuevo;


	Button btnGuardar;


	private Button buttonVerificarClienteBD;
	
	CustomKeyboard customKeyboard;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_cliente_nuevo1);

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		customKeyboard = new CustomKeyboard(FormClienteNuevo1.this, R.id.keyboardviewNuevo, R.xml.keyboard_nom);
		// register the edittext
		customKeyboard.registerEditText(R.id.txtDireccion);		

		//permite verificar si un cliente ya existe en la base de datos local y global
		buttonVerificarClienteBD = (Button) findViewById(R.id.buttonVerificarClienteBD);
		buttonVerificarClienteBD.setOnClickListener(listenerVerificarClienteBD);

		//no mostrar el boton de guardar hasta verificar que nit del cliente.
		btnGuardar = (Button)findViewById(R.id.btnGuardarCN);
		validado = false;

		tableLayoutClienteNuevo = (TableLayout) findViewById(R.id.tableLayoutClienteNuevo);
		tableLayoutClienteNuevo.setVisibility(View.GONE);
		cargarCiudades();
		CargarTipoDocumento();
	}

	public void CargarCiudad() {

		String[] items;
		Vector<String> listaCiudad = DataBaseBO.ListaCiudadCliente();

		if (listaCiudad.size() > 0) {

			items = new String[listaCiudad.size()];
			listaCiudad.copyInto(items);

		} else {

			items = new String[] {};

			if (listaCiudad != null)
				listaCiudad.removeAllElements();
		}

		//Spinner cbCiudadRutero = (Spinner) findViewById(R.id.cbCiudad);
		//ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		//adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//cbCiudadRutero.setAdapter(adapter);
	}

	public void CargarTipoDocumento() {

		String[] items;
		Vector<String> listaItems = new Vector<String>();
		listaItems.add( "SELECCIONAR" );
		listaItems.add( "CEDULA" );
		listaItems.add( "NIT" );

		if (listaItems.size() > 0) {

			items = new String[listaItems.size()];
			listaItems.copyInto(items);

		} else {

			items = new String[] {};
		}

		Spinner cbCnTipoDoc = (Spinner) findViewById(R.id.cbTipoDoc);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cbCnTipoDoc.setAdapter(adapter);


		cbCnTipoDoc.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				configurarCampoDeTexto(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {}
		});
	}

	public void configurarCampoDeTexto(int pos){

		EditText txtCampoTexto = (EditText)findViewById(R.id.txtCodigo);
		InputFilter[] filters;

		if(pos == 0){

			txtCampoTexto.setText("");
			txtCampoTexto.setClickable(false);
			txtCampoTexto.setFocusable(false);
			txtCampoTexto.setEnabled(false);
			txtCampoTexto.setFocusableInTouchMode(false);

			((Button)findViewById(R.id.btnLeerCodigoCedula)).setVisibility(Button.GONE);

		}else

		if(pos == 1){

			if(numeroDeIntentosLector>3){

				txtCampoTexto.setText("");
				txtCampoTexto.setClickable(true);
				txtCampoTexto.setFocusable(true);
				txtCampoTexto.setEnabled(true);
				txtCampoTexto.setFocusableInTouchMode(true);
				((Button)findViewById(R.id.btnLeerCodigoCedula)).setVisibility(Button.GONE);

			}else{

				txtCampoTexto.setText("");
				txtCampoTexto.setClickable(true);
				txtCampoTexto.setFocusable(true);
				txtCampoTexto.setEnabled(true);
				txtCampoTexto.setFocusableInTouchMode(true);
				((Button)findViewById(R.id.btnLeerCodigoCedula)).setVisibility(Button.GONE);

			}

			filters = new InputFilter[1];
			filters[0] = new InputFilter.LengthFilter(15); //Filter to 10 characters
			txtCampoTexto .setFilters(filters);
			txtCampoTexto.setInputType(InputType.TYPE_CLASS_NUMBER);

		}else{

			txtCampoTexto.setText("");
			txtCampoTexto.setClickable(true);
			txtCampoTexto.setFocusable(true);
			txtCampoTexto.setEnabled(true);
			txtCampoTexto.setFocusableInTouchMode(true);

			filters = new InputFilter[1];
			filters[0] = new InputFilter.LengthFilter(15); //Filter to 9 characters
			txtCampoTexto .setFilters(filters);
			txtCampoTexto.setInputType(InputType.TYPE_CLASS_NUMBER);

			((Button)findViewById(R.id.btnLeerCodigoCedula)).setVisibility(Button.GONE);
		}
	}


	public void onClickLeerCodigoCedula(View view) {

		/*
		 * retardo de 2000ms para evitar eventos de doble click. esto significa
		 * que solo se puede hacer click cada 2000ms. es decir despues de
		 * presionar el boton, se debe esperar que transcurran 1500ms para que
		 * se capture un nuevo evento.
		 */
		if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
			return;
		}
		mLastClickTime = SystemClock.elapsedRealtime();

		if(numeroDeIntentosLector>3){

			EditText txtCampoTexto = (EditText)findViewById(R.id.txtCodigo);

			txtCampoTexto.setText("");
			txtCampoTexto.setClickable(true);
			txtCampoTexto.setFocusable(true);
			txtCampoTexto.setEnabled(true);
			txtCampoTexto.setFocusableInTouchMode(true);
			txtCampoTexto.requestFocus();
			((Button)findViewById(R.id.btnLeerCodigoCedula)).setVisibility(Button.GONE);

		}else{
			lanzarLector();
		}
	}

	public void lanzarLector() {

		File fileLector = new File(Util.DirApp(), "lectorcodigobarraspdf417.apk");

		if (!fileLector.exists()) {
			cargarLectorAPK();
		}

		if (Util.isAppInstalled(this, "co.com.LectorCodigoBarrasPDF417")) {
			abrirLector();
		} else {

			final File fileApp = new File(Util.DirApp(), "lectorcodigobarraspdf417.apk");

			AlertDialog.Builder builder = new AlertDialog.Builder(FormClienteNuevo1.this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int id) {

					dialog.cancel();

					Uri uri = Uri.fromFile(fileApp);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(uri, "application/vnd.android.package-archive");
					startActivity(intent);

				}
			});

			AlertDialog alertDialog = builder.create();
			alertDialog.setMessage("Es Necesario La Instalacion Del Lector De Codigo De Barras Para Continuar");
			alertDialog.show();

		}

	}

	public void abrirLector() {

		Intent launchIntent = new Intent("co.com.LectorCodigoBarrasPDF417.LECTORSCAN");

		if (launchIntent != null) {

			try {

				numeroDeIntentosLector++;

				((EditText) findViewById(R.id.txtCodigo)).setText("");
				launchIntent.putExtra("lector", "1");
				launchIntent.putExtra("Lector", "1");
				startActivityForResult(launchIntent, Const.RESP_LEER_CEDULA);

			} catch (Exception e) {

			}

		}

	}

	/*
	public void cargarLectorAPK(){

		try
		{

			Resources r = getResources();

			File f = new File(Util.DirApp(), "lectorcodigobarraspdf417.apk");

			if(!f.exists())
				f.createNewFile();

			InputStream is  = getResources().openRawResource(R.raw.lectorcodigobarraspdf417);
			OutputStream os = new FileOutputStream(f, true);

			final int buffer_size = 1024 * 1024;

			byte[] bytes = new byte[buffer_size];
			for (;;)
			{
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
			is.close();
			os.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
*/

	public void OnClickSeleccionarRuta(View view) 
	{
		MostrardialogRuta();
	}

	public void OnClickGuardarCliente(View view) {

		btnGuardar = (Button)findViewById(R.id.btnGuardarCN);

		if(!validado) {
			Toast.makeText(this, "Debe verificar primero para continuar", Toast.LENGTH_SHORT).show();
			btnGuardar.setEnabled(true);
			return;
		}

		clienteNuevo = new ClienteNuevo();		

		if (Main.cliente == null)
			Main.cliente = new Cliente();


		clienteNuevo.codigo      = ((EditText)findViewById(R.id.txtCodigo)).getText().toString().trim(); 
		Main.cliente.codigo      = clienteNuevo.codigo;

		clienteNuevo.nit = clienteNuevo.codigo;
		Main.cliente.Nit = clienteNuevo.nit;

		clienteNuevo.nombre      = ((EditText)findViewById(R.id.lblRazonComercial)).getText().toString().trim();
		Main.cliente.Nombre      = clienteNuevo.nombre;

		clienteNuevo.razonSocial = ((EditText)findViewById(R.id.lblRazonSocial)).getText().toString().trim();
		Main.cliente.razonSocial = clienteNuevo.razonSocial;

		clienteNuevo.direccion   = ((EditText)findViewById(R.id.txtDireccion)).getText().toString().trim();
		Main.cliente.direccion   = clienteNuevo.direccion;

		Spinner spCiudades = (Spinner)findViewById(R.id.cbCiudad);
		int posCiudad = spCiudades.getSelectedItemPosition();
		Ciudad ciudad = listaCiudades.elementAt(posCiudad);

		clienteNuevo.ciudad      = ""+ciudad.nombre;
		Main.cliente.Ciudad      = clienteNuevo.ciudad+""; 

		clienteNuevo.codCiudad      = ""+ciudad.codigo;
		Main.cliente.codCiudad      = clienteNuevo.codCiudad+""; 

		clienteNuevo.telefono    = ((EditText)findViewById(R.id.txtTelefono)).getText().toString().trim();
		Main.cliente.Telefono    = clienteNuevo.telefono;

		if(Main.usuario == null)
			DataBaseBO.CargarInfomacionUsuario();

		clienteNuevo.vendedor    = Main.usuario.codigoVendedor;

		/*clienteNuevo.barrio  = ((EditText)findViewById(R.id.txtBarrio)).getText().toString().trim();
		Main.cliente.barrio  = clienteNuevo.barrio;*/

		clienteNuevo.territorio = clienteNuevo.barrio;
		Main.cliente.territorio = clienteNuevo.territorio;

		clienteNuevo.vendedor = Main.usuario.codigoVendedor;

		Main.cliente.Bloqueado = "N";
		Main.cliente.tipologia = "";
		Main.cliente.diasCreacion = 0;

		if(Main.cliente.codigo.equals("")){

			Util.MostrarAlertDialog(this, "Por favor ingrese el codigo del Cliente",1);
			((EditText)findViewById(R.id.txtCodigo)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}


		if(Main.cliente.codigo.length() > 11){

			Util.MostrarAlertDialog(this, "El Codigo del Cliente no debe ser mayor a 11 Caracteres",1);
			((EditText)findViewById(R.id.txtCodigo)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}


		String txtAux1 = "";
		String txtAux2 = "";

		if(Main.cliente.codigo.length()>=4)
			txtAux1 = Main.cliente.codigo.substring(0, 4);

		if(Main.cliente.codigo.length()>=2)
			txtAux2 = Main.cliente.codigo.substring(0, 2);


		if(txtAux1.equals("0200")||txtAux1.equals("0400")||txtAux2.equals("02")||txtAux2.equals("04")){

			Util.MostrarAlertDialog(this, "El Codigo del Cliente es Invalido, No debe Empezar con 020 - 0200 - 040 - 0400",1);
			((EditText)findViewById(R.id.txtCodigo)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}

		if(DataBaseBO.ExisteCodigotEnClientes(Main.cliente.codigo)||DataBaseBO.ExisteCodigotEnClientesNuevos(Main.cliente.codigo)){

			Util.MostrarAlertDialog(this, "Existe un Cliente con el Mismo Codigo",1);
			((EditText)findViewById(R.id.txtCodigo)).requestFocus();
			btnGuardar.setEnabled(true);
			return;


		}




		if (clienteNuevo.nombre.length()> 30) {

			Util.MostrarAlertDialog(this, "El Nombre del Cliente es Mayor a 30 Caracteres",1);
			((EditText)findViewById(R.id.lblRazonComercial)).requestFocus();
			btnGuardar.setEnabled(true);
			return;
		}



		if (clienteNuevo.razonSocial.length() > 30){

			Util.MostrarAlertDialog(this, "La Razon Social del Cleinte es Mayor a 30 Caracteres",1);
			((EditText)findViewById(R.id.lblRazonSocial)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}



		if (clienteNuevo.razonSocial.equals("")){

			Util.MostrarAlertDialog(this, "Por Favor Ingrese la Razon Social del Cliente",1);
			((EditText)findViewById(R.id.lblRazonSocial)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}



		/*if (clienteNuevo.barrio.equals("")){

			Util.MostrarAlertDialog(this, "Por Favor Ingrese el Barrio del Cliente",1);
			((EditText)findViewById(R.id.txtBarrio)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}*/

		if (clienteNuevo.telefono.equals("")){

			Util.MostrarAlertDialog(this, "Por Favor Ingrese el Telefono del Cliente",1);
			((EditText)findViewById(R.id.txtTelefono)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}


		if (clienteNuevo.telefono.length() > 12){

			Util.MostrarAlertDialog(this, "El Telefono del Cliente es Invalido debe ser de Maximo 12 Caracteres",1);
			((EditText)findViewById(R.id.txtTelefono)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}


		if (clienteNuevo.direccion.equals("")){

			Util.MostrarAlertDialog(this, "No ha Ingresado La Direccion del Cliente",1);
			((EditText)findViewById(R.id.txtDireccion)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}


		if (clienteNuevo.direccion.length() > 40){

			Util.MostrarAlertDialog(this, "La Direccion del Cliente no debe ser Mayor a 40 Caracteres",1);
			((EditText)findViewById(R.id.txtDireccion)).requestFocus();
			btnGuardar.setEnabled(true);
			return;

		}



		if (clienteNuevo.ciudad.equals("")){


			Util.MostrarAlertDialog(this, "Por Favor Seleccione la Ciudad del Cliente",1);
			btnGuardar.setEnabled(true);
			return;

		}



		Intent data =  new Intent(this,FormClienteNuevo2.class);
		data.putExtra("clientenuevo", clienteNuevo);
		startActivityForResult(data,Const.RESP_CLIENTE_NUEVO);





	}

	public String ObtenerRutaParada() {

		String rutaParada = "";

		if (dialogRuta != null) {

			boolean lunes     = ((CheckBox)dialogRuta.findViewById(R.id.checkLunes)).isChecked();
			boolean martes    = ((CheckBox)dialogRuta.findViewById(R.id.checkMartes)).isChecked();
			boolean miercoles = ((CheckBox)dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
			boolean jueves    = ((CheckBox)dialogRuta.findViewById(R.id.checkJueves)).isChecked();
			boolean viernes   = ((CheckBox)dialogRuta.findViewById(R.id.checkViernes)).isChecked();
			boolean sabado    = ((CheckBox)dialogRuta.findViewById(R.id.checkSabado)).isChecked();
			boolean domingo   = ((CheckBox)dialogRuta.findViewById(R.id.checkDomingo)).isChecked();

			rutaParada += lunes ? "1" : "0";
			rutaParada += martes ? "1" : "0";
			rutaParada += miercoles ? "1" : "0";
			rutaParada += jueves ? "1" : "0";
			rutaParada += viernes ? "1" : "0";
			rutaParada += sabado ? "1" : "0";
			rutaParada += domingo ? "1" : "0";
		}

		return rutaParada;
	}

	public void MostrardialogRuta() {

		if (dialogRuta == null) {

			dialogRuta = new Dialog(this);
			dialogRuta.requestWindowFeature(Window.FEATURE_LEFT_ICON);
			dialogRuta.setContentView(R.layout.dialog_dias_ruta);
			dialogRuta.setTitle("Ingresar Ruta");
		}

		((Button)dialogRuta.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				boolean lunes     = ((CheckBox)dialogRuta.findViewById(R.id.checkLunes)).isChecked();
				boolean martes    = ((CheckBox)dialogRuta.findViewById(R.id.checkMartes)).isChecked();
				boolean miercoles = ((CheckBox)dialogRuta.findViewById(R.id.checkMiercoles)).isChecked();
				boolean jueves    = ((CheckBox)dialogRuta.findViewById(R.id.checkJueves)).isChecked();
				boolean viernes   = ((CheckBox)dialogRuta.findViewById(R.id.checkViernes)).isChecked();
				boolean sabado    = ((CheckBox)dialogRuta.findViewById(R.id.checkSabado)).isChecked();
				boolean domingo   = ((CheckBox)dialogRuta.findViewById(R.id.checkDomingo)).isChecked();

				String ruta = "";
				ruta += lunes ? "Lunes" : "";
				ruta += martes ? (ruta.equals("") ? "" : ", " ) + "Martes" : "";
				ruta += miercoles ? (ruta.equals("") ? "" : ", " ) + "Miercoles" : "";
				ruta += jueves ? (ruta.equals("") ? "" : ", " ) + "Jueves" : "";
				ruta += viernes ? (ruta.equals("") ? "" : ", " ) + "Viernes" : "";
				ruta += sabado ? (ruta.equals("") ? "" : ", " ) + "Sabado" : "";
				ruta += domingo ? (ruta.equals("") ? "" : ", " ) + "Domingo" : "";

				String rutaParada = Util.SepararPalabrasTextView(ruta, 30);

				dialogRuta.cancel();
			}
		});

		((Button) dialogRuta.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				dialogRuta.cancel();
			}
		});

		dialogRuta.setCancelable(false);
		dialogRuta.show();
		dialogRuta.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_list);
	}

	public void OnClickCancelarPedido(View view) 
	{	
		finish();
	}

	@Override
	public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {

		switch (codeRequest) {

		case Const.ENVIAR_PEDIDO:

			final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : msg;

			if (progressDialog != null)
				progressDialog.cancel();

			this.runOnUiThread(new Runnable() {

				public void run() {

				}
			});


			break;


		case Const.VERIFICAR_CLIENTE:
			//si la respuesta es correcta, extraer la informacion enviada del servidor.
			if(ok) {
				// si la respuesta es error, es porque el cliente no existe y se puede crear el nuevo cliente.
				if(respuestaServer.trim().startsWith("error")) {					
					runOnUiThread(new Runnable() {						
						@Override
						public void run() {
							progresValidarCliente.dismiss();
							String mensaje = "El Codigo o NIT ingresado no corresponde a ningun cliente de nuestra base de datos, se puede crear el cliente nuevo.";
							mostrarAlertConfirmandoVerificacion(mensaje);
							((EditText)findViewById(R.id.txtCodigo)).setEnabled(false);
							tableLayoutClienteNuevo.setVisibility(View.VISIBLE);
							validado = true;
						}
					});
				}
				else {
					final String[] info = respuestaServer.split(";");					
					runOnUiThread(new Runnable() {						
						@Override
						public void run() {							
							Cliente cliente = new Cliente();
							cliente.razonSocial = info[2];
							cliente.Bloqueado = info[4];
							progresValidarCliente.dismiss();
							String bloqueo = cliente.Bloqueado.equals("N")? " (No bloqueado)": " (bloqueado)";
							final String message = "El cliente ya se encuentra registrado bajo la razon: " 
									+ cliente.razonSocial + " y su estado de bloqueo es: " + cliente.Bloqueado + bloqueo 
									+ " No se puede registrar el cliente ";
							mostrarAlertConfirmandoVerificacion(message);							
						}
					});					
				}
			}
			else {
				runOnUiThread(new Runnable() {						
					@Override
					public void run() {
						progresValidarCliente.dismiss();
						String mensaje = "No se ha obtenido respuesta del servidor, intente de nuevo o verifique su estado de conexion a internet.";
						mostrarAlertConfirmandoVerificacion(mensaje);							
					}
				});
			}
			break;
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == Const.RESP_LEER_CEDULA && resultCode == RESULT_OK){
			if(data != null){
				String datos = data.getStringExtra("datos");
				if(datos != null){
					if(!datos.equals("")){
						if(datos.equals("no_leido")){
							mostrarMensajeNoLeido();
						}else{
							procesarCadena(datos);
						}
					}
				}
			}
		}else
		if (requestCode == Const.RESP_TOMAR_FOTO && resultCode == RESULT_OK) {

//			configurarItemsFormulario( true );

			if(!fechaNCN.equals("")){
				try{

					Date date = new SimpleDateFormat("yyyyMMdd").parse(fechaNCN);
					String dateString2 = new SimpleDateFormat("yyyy-MM-dd").format(date);
//					((EditText)findViewById(R.id.txtFechaDeNacimiento)).setText(dateString2);

				}catch(Exception e){}

			}

			if(!nombreCN.equals("")){

//				( ( EditText ) findViewById( R.id.txtCnPropietario ) ).setText(nombreCN);

			}


//			( ( EditText ) findViewById( R.id.txtCnRazonSocial ) ).requestFocus();

			/*Main.fotoClienteNuevo = ResizedImage(anchoImg, altoImg);

			if (Main.fotoClienteNuevo != null) {

				//ImageView imgFoto = (ImageView)findViewById(R.id.imageFoto);
				//imgFoto.setImageDrawable(Main.fotoNoCompra);

				String codCliente = ( ( EditText ) findViewById( R.id.txtCnDocumento ) ).getText().toString();

				Bitmap bitmap = ((BitmapDrawable)Main.fotoClienteNuevo).getBitmap();
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
				byte[] byteArray = stream.toByteArray();

				if( DataBaseBO.GuardarFotoClienteNuevo( codCliente , byteArray) ){

					Util.MostrarAlertDialog( FormClienteNuevo.this, "La foto fue guardada Correctamente" );
					configurarItemsFormulario( true );
				}
				else{

					Util.MostrarAlertDialog( FormClienteNuevo.this, "La foto no fue guardada correctamente, intente nuevamente" );
				}

			}
			else{

				Util.MostrarAlertDialog( FormClienteNuevo.this , "No capturo la foto, por favor intentelo nuevamente");
			}*/
		}else if(requestCode == Const.RESP_CLIENTE_NUEVO && resultCode == RESULT_OK) {

		    finish();
		}else{

			if(resultCode == RESULT_OK)
				finish();


		}

	}





	public void validarNit(){

		/*
		progressDialog = ProgressDialog.show(FormClienteNuevo.this, "", "Validando Nit...", true);
		progressDialog.show();


		btnGuardar.setEnabled(true);


		Sync sync = new Sync(FormClienteNuevo1.this, Const.VALIDAR_NIT);
		sync.nit = Main.cliente.nit;

		sync.start();*/

	}


	public void guardarYEnviarClienteNuevo(){


		boolean ingreso = DataBaseBO.GuardarClienteNuevo(clienteNuevo);

		Looper.prepare();

		if (ingreso) {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Cliente Registrado con Exito")	
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int id) {
					/*
					progressDialog = ProgressDialog.show(FormClienteNuevo.this, "", "Enviando Informacion Cliente...", true);
					progressDialog.show();


					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();

					dialog.cancel();

					Sync sync = new Sync(FormClienteNuevo.this, Const.ENVIAR_PEDIDO);
					sync.start();*/
				}
			});
			AlertDialog alert = builder.create();
			alert.show();

			Looper.loop();


		}

	}




	public void guardarYEnviarClienteNuevo2(){


		boolean ingreso = DataBaseBO.GuardarClienteNuevo(clienteNuevo);


		if (ingreso) {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Cliente Registrado con Exito")	
			.setCancelable(false)
			.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int id) {
					/*

					progressDialog = ProgressDialog.show(FormClienteNuevo.this, "", "Enviando Informacion Cliente...", true);
					progressDialog.show();


					Main.encabezado = new Encabezado();
					Main.detallePedido.clear();

					dialog.cancel();

					Sync sync = new Sync(FormClienteNuevo.this, Const.ENVIAR_PEDIDO);
					sync.start();*/
				}
			});
			AlertDialog alert = builder.create();
			alert.show();



		}

	}


	public void cargarCiudades(){


		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		listaCiudades = DataBaseBO.ListaCiudades(listaItems);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinner = (Spinner) findViewById(R.id.cbCiudad);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);		
	}



	public void cargarCanales() {

		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		listaCanales = DataBaseBO.ListaCanales(listaItems);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinner = (Spinner) findViewById(R.id.cbCanal);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				// your code here
				Canal canal = listaCanales.elementAt(position);
				cargarSubCanales(canal.codigo);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {

			}
		});
	}

	public void cargarSubCanales_(String canal){

		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		listaSubCanales = DataBaseBO.ListaSubCanales(listaItems);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinner = (Spinner) findViewById(R.id.cbSubCanal);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);


	}




	public void cargarSubCanales(String canal){


		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		listaSubCanales = DataBaseBO.ListaSubCanales(listaItems,canal);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinner = (Spinner) findViewById(R.id.cbSubCanal);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);


	}






	public void cargarListas(){

		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();

		listaItems.add("L1-L1");

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinner = (Spinner) findViewById(R.id.cbLista);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}



	public void cargarDias(){

		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();

		listaItems.add("Lunes");
		listaItems.add("Martes");
		listaItems.add("Miercoles");
		listaItems.add("Jueves");
		listaItems.add("Viernes");
		listaItems.add("Sabado");
		listaItems.add("Domingo");

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinner = (Spinner) findViewById(R.id.cbDia);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}




	public void OnClickCancelarClienteNuevo(View v) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Desea Salir de Este Formulario?")	
		.setCancelable(false)
		.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				finish();
			}
		})

		.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}


	public void cargarRutas(){
		
		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		listaRutas = DataBaseBO.ListaRutas(listaItems);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinner = (Spinner) findViewById(R.id.comboRutaClientes);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}


	/**
	 * verifica si se dispone de una conexion activa a internet.
	 * @return true si hay conexion, false en caso contrario.
	 */
	public boolean isOnline() {
		boolean enabled = true;	     
		ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connectivityManager.getActiveNetworkInfo();         
		if ((info == null || !info.isConnected() || !info.isAvailable())) {
			enabled = false;
		}

		return enabled; 
	}


	/**
	 * handler para captura de evento del boton verificarClienteBD
	 */
	private OnClickListener listenerVerificarClienteBD = new OnClickListener() {

		@Override
		public void onClick(View v) {

			verificarCliente();
		}
	};

	public void verificarCliente(){
		//capturar el codigo escrito por el usuario
		final String codigo = ((EditText)findViewById(R.id.txtCodigo)).getText().toString().trim();
		if(codigo == null || codigo.equals("") || codigo.length() == 0){
//				mostrarAlertConfirmandoVerificacion("Por favor ingrese un NIT para verificar");
			mostrarAlertConfirmandoVerificacion("Por favor ingrese un No Documento para verificar");
		} else {
			//mostrar progress dialog mientras se realiza la busqueda.
			progresValidarCliente = ProgressDialog.show(FormClienteNuevo1.this, "Verificando.", "Verficanco que el cliente no exista en la base de datos");
			progresValidarCliente.setCancelable(false);
			new Thread( new Runnable() {
				@Override
				public void run() {
					verificacionCliente(codigo);
				}
			}).start();
		}
	}
	/**
	 * verifica si un cliente ya existe en las bases de datos de luker local y nacionalmente.
	 * @param codigo
	 */
	public void verificacionCliente(String codigo) {
		try {
			//realizar verificacion en la base de datos local.
			Cliente cliente = verificarClienteBaseDatosLocal(codigo);

			//generar retardo para efecto visual
			Thread.sleep(1000);

			if(cliente != null) {
				progresValidarCliente.dismiss();
				String bloqueo = cliente.Bloqueado.equals("N")? " (No bloqueado)": " (bloqueado)";
				final String mensaje = "El cliente ya se encuentra registrado bajo la razon: " 
						+ cliente.razonSocial + " y su estado de bloqueo es: " + cliente.Bloqueado + bloqueo
						+ " No se puede registrar el cliente ";

				runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						mostrarAlertConfirmandoVerificacion(mensaje);
					}
				});				
			}
			else {
				//verificacion de acceso a red.
				if(isOnline()){
					//realizar verificacion en la web.
					verificarClienteBaseDatosGlobal(codigo);
				} else {
					progresValidarCliente.dismiss();
					final String mensaje = "No se detecta acceso a internet, Por favor verifique su estado de conexion.";					
					runOnUiThread(new Runnable() {					
						@Override
						public void run() {
							mostrarAlertConfirmandoVerificacion(mensaje);
						}
					});		
				}				
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * verifica si un cliente ya existe en las bases de datos de luker local y nacionalmente.
	 * @param codigo
	 */
	public void verificacionClienteNuevo(String codigo) {
		try {

			//verificacion de acceso a red.
			if (isOnline()) {
				//realizar verificacion en la web.
				verificarClienteBaseDatosGlobal(codigo);
			} else {
				//realizar verificacion en la base de datos local.
				Cliente cliente = verificarClienteBaseDatosLocal(codigo);

				//generar retardo para efecto visual
				Thread.sleep(1000);

				if(cliente != null) {
					progresValidarCliente.dismiss();
					String bloqueo = cliente.Bloqueado.equals("N")? " (No bloqueado)": " (bloqueado)";
					final String mensaje = "El cliente ya se encuentra registrado bajo la razon: "
							+ cliente.razonSocial + " y su estado de bloqueo es: " + cliente.Bloqueado + bloqueo
							+ " No se puede registrar el cliente ";

					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							mostrarAlertConfirmandoVerificacion(mensaje);
						}
					});
				}
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * permite verificar si un cliente se encuentra registrado en la base de datos de la web.
	 * se requiere de acceso a internet para obtener la respuesta.
	 */
	public void verificarClienteBaseDatosGlobal(String nit){
		Sync sync = new Sync(this, Const.VERIFICAR_CLIENTE);
		sync.codigoVerificar = nit;
		sync.start();
	}


	/**
	 * permite verificar si un cliente esta registrado en la base de datos local, 
	 * En la tabla clientes.
	 * @param nit
	 * @return CLiente, si el cliente existe, null en caso contrario
	 */
	public Cliente verificarClienteBaseDatosLocal(String nit){
		return DataBaseBO.verificarExistenciaClienteEnBD(nit);
	}


	/**
	 * alert para informar al usuario el resultado de la verificacion del codigo de cliente.
	 * @param mensaje
	 */
	private void mostrarAlertConfirmandoVerificacion(String mensaje) {
		DialogRespuesta dialogRespuesta=new DialogRespuesta(FormClienteNuevo1.this, mensaje);
		dialogRespuesta.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogRespuesta.setCancelable(false);
		dialogRespuesta.show();
	}
	
	public void onBackPressed() {
        if(customKeyboard!=null && customKeyboard.isCustomKeyboardVisible() ) customKeyboard.hideCustomKeyboard(); else super.onBackPressed();
    }

 class DialogRespuesta extends Dialog{

	 String mensaje;
	 Button btnAceptar;
	 TextView txtMensaje;
	public DialogRespuesta(Context context,String mensaje) {
		super(context);
		// TODO Auto-generated constructor stub
		this.mensaje=mensaje;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_cliente_nuevo_respuesta);
		btnAceptar=(Button) findViewById(R.id.btnAceptar);
		txtMensaje=(TextView) findViewById(R.id.txtMensaje);
		btnAceptar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				cancel();
				
			}
		});
		txtMensaje.setText(mensaje+"");
	}
	 
	 
 }


	public void mostrarMensajeNoLeido(){

		if(numeroDeIntentosLector>3){


			EditText txtCampoTexto = (EditText)findViewById(R.id.txtCodigo);


			txtCampoTexto.setText("");
			txtCampoTexto.setClickable(true);
			txtCampoTexto.setFocusable(true);
			txtCampoTexto.setEnabled(true);
			txtCampoTexto.setFocusableInTouchMode(true);
			txtCampoTexto.requestFocus();
			((Button)findViewById(R.id.btnLeerCodigoCedula)).setVisibility(Button.GONE);

		}else{

			if(progressDialogLanzarLector!=null)
				if(progressDialogLanzarLector.isShowing())
					progressDialogLanzarLector.cancel();

			progressDialogLanzarLector = new ProgressDialog(this);
			progressDialogLanzarLector.setMessage("Abriendo Lector");
			progressDialogLanzarLector.show();


			Handler handlerProgress = new Handler();
			handlerProgress.postDelayed(new Runnable() {
				public void run() {
					//my events

					FormClienteNuevo1.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							if(progressDialogLanzarLector != null)
								if(progressDialogLanzarLector.isShowing())
									progressDialogLanzarLector.cancel();
							lanzarLector();
						}
					});
				}
			}, 1000); //time in millis
		}


	}
	public void procesarCadena(String cadena) {

		fechaNCN = "";
		nombreCN = "";


		try {

			if (cadena != null) {
				if (!cadena.equals("")) {

					int posBuscada = 0;
					String[] datos = cadena.split("\n");
					String cadenaAux = "";
					boolean primerVez = true;

					for (int i = 0; i < datos.length; i++) {

						if (datos[i].equals("")) {

							if (primerVez) {

								cadenaAux = cadenaAux + "\n";

								primerVez = false;

							} else {

							}

						} else {

							cadenaAux = cadenaAux + datos[i];
							primerVez = true;

						}

					}

					Vector<String> listadoDatos = new Vector<String>();
					Vector<Character> caracteresPalabra = new Vector<Character>();

					char[] dat = cadenaAux.toCharArray();

					for (int j = 0; j < dat.length; j++) {

						if (Character.isLetterOrDigit((char) dat[j])) {

							caracteresPalabra.add((char) dat[j]);

						} else {

							if (caracteresPalabra.size() > 0) {

								char[] text = new char[caracteresPalabra.size()];

								for (int i = 0; i < caracteresPalabra.size(); i++) {

									text[i] = caracteresPalabra.elementAt(i);

								}

								listadoDatos.add(new String(text));
								caracteresPalabra.removeAllElements();

							} else {

							}

						}

					}

					for (int i = 0; i < listadoDatos.size(); i++) {

						if (listadoDatos.elementAt(i).startsWith("0M")||listadoDatos.elementAt(i).startsWith("0F")) {

							posBuscada = i;
							break;

						}

					}

					int posBuscada2 = 0;

					for (int i = posBuscada; i > 0; i--) {

						List<String> listado = Parse(listadoDatos.elementAt(i));

						if (listado.size() == 2) {

							posBuscada2 = i;
							break;

						}

					}

					String nombre = "";

					String cedula = Parse(listadoDatos.elementAt(posBuscada2)).get(0);

					if(cedula.length()>10)
						cedula = cedula.substring(cedula.length()-10, cedula.length());

					nombre = Parse(listadoDatos.elementAt(posBuscada2)).get(1) + " ";

					for (int i = posBuscada2 + 1; i < posBuscada; i++) {

						nombre = nombre + listadoDatos.elementAt(i) + " ";

					}

					String FechaNacimiento = listadoDatos.elementAt(posBuscada).substring(2, 10);

					String datosCompletos = "";

					for (int i = 0; i < listadoDatos.size(); i++) {

						datosCompletos = datosCompletos + listadoDatos.elementAt(i) + "\n";

					}

					//Util.MostrarAlertDialog(this, "Cedula: " + cedula);
					//Util.MostrarAlertDialog(this, "Nombre: " + nombre);
					//Util.MostrarAlertDialog(this, "Fecha Nacimiento: " + FechaNacimiento);

					cedula = cedula.replaceFirst("^0*", "");

					long documento = Long.parseLong(cedula);

					((EditText)findViewById(R.id.txtCodigo)).setText(documento+"");

					//((Button)findViewById(R.id.btnLeerCodigoCedula)).performClick();

					fechaNCN = FechaNacimiento;

					nombreCN = nombre;

					if(!nombreCN.equals("")){

						((EditText) findViewById( R.id.lblRazonComercial ) ).setText(nombreCN);

					}

					verficarDocumento();

					verificarCliente();

					lectura = true;

				}
			}

		} catch (Exception e) {
		}

	}

	public void verficarDocumento(){


		Spinner spcbCnTipoDocumento = (Spinner)findViewById(R.id.cbTipoDoc);

		String tipoDoc = spcbCnTipoDocumento.getSelectedItem().toString();
		String documento = ((EditText) findViewById(R.id.txtCodigo)).getText().toString();


		if(spcbCnTipoDocumento.getSelectedItemPosition() == 0){

			Util.MostrarAlertDialog(this, "Por Favor Seleccione Cedula o Nit");

		}else

		if(tipoDoc.equals("CEDULA")&&(documento.length()<4 || documento.length()>10)){


			Util.MostrarAlertDialog(this, "El Numero de la Cedula debe tener una longitud entre 4 y 10 Digitos");

		}else
		if(tipoDoc.equals("NIT")&&!(documento.length()==9)){

			Util.MostrarAlertDialog(this, "El Numero de Nit debe tener una longitud de 9 digitos");

		}else{


			/////
			progressDialog = ProgressDialog.show(this, "", "Verificando Documento...", true);
			progressDialog.show();

			Thread h = new Thread(){

				public void run(){

					validarCodigoClienteNuevo();
				}
			};

			h.start();
			////

		}
	}


	public void validarCodigoClienteNuevo(){

		final String mensaje = codigoClienteCorrecto();

		if( progressDialog != null ){

			progressDialog.dismiss();
		}

		if( mensaje.equals( "" ) ){


//			FormClienteNuevo1.this.runOnUiThread(new Runnable() {
//
//				@Override
//				public void run() {
//					// TODO Auto-generated method stub
//
//					( ( Button ) findViewById( R.id.btnGuardarClienteNuevoo ) ).setEnabled(true);
//				}
//			});



			/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			String filePath = Util.DirApp().getPath() + "/fotoCn.jpg";

			Uri output = Uri.fromFile(new File(filePath));
			intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
			startActivityForResult(intent, Const.RESP_TOMAR_FOTO);*/

			//configurarItemsFormulario( true );

//			String documento = ( ( EditText ) findViewById( R.id.txtCodigo ) ).getText().toString();
//
//			Intent intent = new Intent( FormClienteNuevo1.this, FormFotoClienteNuevoActividad2.class );
//			intent.putExtra( "documento" , documento);
//			startActivityForResult(intent, Const.RESP_TOMAR_FOTO);

		}
		else{

			this.runOnUiThread( new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Util.MostrarAlertDialog( FormClienteNuevo1.this , mensaje );
				}
			} );

		}
	}

	public String codigoClienteCorrecto(){

		int index = 0;
		String mensaje = "";
		String codigo = "";

		codigo = ( ( EditText ) findViewById( R.id.txtCodigo ) ).getText().toString();
		index = ( ( Spinner ) findViewById( R.id.cbTipoDoc ) ).getSelectedItemPosition();

		switch( index ){

			/*case 0:
					if( !( codigo.length() >= 4 && codigo.length() <= 10 ) ){

						mensaje = "La cantidad de giditos del numero de cedula no es valido para el registro, debe estar entre 4 y 10 digitos";
					}
					break;

			case 1:
					if( codigo.length() != 9 ){

						mensaje = "La cantidad de giditos del nit no es valido para el registro, debe ser de 9 digitos";
					}
					break;*/

			case 1:
				if( !( codigo.length() >= 4 && codigo.length() <= 10 ) ){

					mensaje = "La cantidad de digitos del numero de cedula no es valido para el registro, debe estar entre 4 y 10 digitos";
				}
				break;

			case 2:
				if( codigo.length() != 9 ){

					mensaje = "La cantidad de digitos del nit no es valido para el registro, debe ser de 9 digitos";
				}
				break;
		}

		if( mensaje.equals( "" ) ){

//			if( DataBaseBO.clienteYaIngresado(codigo) ){
//
//				mensaje = "El cliente ya se encuentra registrado en el sistema";
//			}
		}

		return mensaje;
	}

	private List<String> Parse(String str) {
		List<String> output = new ArrayList<String>();
		// Matcher match =
		// Pattern.compile("[0-9]+|[a-z]+|[A-Z]+|[?�]+|[?�]").matcher(str);
		// while (match.find()) {
		// output.add(match.group());
		// }

		Matcher match = Pattern.compile("[0-9]+|\\p{L}").matcher(str);

		String letras = "";

		while (match.find()) {

			String datos = match.group();

			boolean contieneSoloNumeros = true;

			for (int i = 0; i < datos.length(); i++) {

				if (Character.isDigit(datos.charAt(i))) {

				} else {

					contieneSoloNumeros = false;
					break;

				}

			}

			if (contieneSoloNumeros) {

				output.add(datos);

			} else {

				letras = letras + datos;

			}

		}

		if (!letras.equals("")) {

			output.add(letras);
		}

		return output;
	}





	public void cargarLectorAPK(){

		try
		{

			Resources r = getResources();

			File f = new File(Util.DirApp(), "lectorcodigobarraspdf417.apk");

			if(!f.exists())
				f.createNewFile();

			InputStream is  = getResources().openRawResource(R.raw.lectorcodigobarraspdf417);
			OutputStream os = new FileOutputStream(f, true);

			final int buffer_size = 1024 * 1024;

			byte[] bytes = new byte[buffer_size];
			for (;;)
			{
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
			is.close();
			os.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}



	}




}//final de clase
