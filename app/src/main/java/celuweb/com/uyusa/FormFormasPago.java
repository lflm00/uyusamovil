package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Banco;
import celuweb.com.DataObject.FormaPago;

public class FormFormasPago extends Activity implements OnClickListener {

    private RadioButton radioCheque;
    private RadioButton radioEfectivo;
    private TextView lblDiferenciaFormasPago;

    String nroDoc = "";
    double totalRecuado = 0;

    Dialog dialogFormasPago;

    Vector<Banco> listaBancos;
    Vector<FormaPago> listaFormaPago;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_formas_pago);

        Inicializar();
        CargarInformacion();
        CargarFormasPago();
    }

    public void Inicializar() {

        this.radioCheque = (RadioButton) findViewById(R.id.radioCheque);
        this.radioEfectivo = (RadioButton) findViewById(R.id.radioEfectivo);
        this.lblDiferenciaFormasPago = (TextView) findViewById(R.id.lblDiferenciaFormasPago);
    }

    public void CargarInformacion() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            if (bundle.containsKey("totalRecuado"))
                totalRecuado = bundle.getDouble("totalRecuado");

            if (bundle.containsKey("nroDoc"))
                nroDoc = bundle.getString("nroDoc");
        }

        String cadena = "Para Ingresar las Formas de Pago, Por favor Seleccione Efectivo o Cheque y haga Click en el Boton Agregar.";
        String mensaje = Util.SepararPalabrasTextView(cadena, 40);

        TextView lblFormaPago = (TextView) findViewById(R.id.lblFormaPago);
        lblFormaPago.setText(Html.fromHtml(mensaje));

        TextView lblTotalRecaudo = (TextView) findViewById(R.id.lblTotalRecaudo);
        lblTotalRecaudo.setText(Util.SepararMilesSin(Util.Redondear("" + totalRecuado, 0)));

        lblDiferenciaFormasPago.setText(Util.SepararMilesSin("" + (Main.total_recaudo - (Main.total_descuento + Main.total_forma_pago))));
    }

    public void CargarBancos() {

        Vector<String> listaItems = new Vector<String>();
        listaBancos = DataBaseBO.ListaBancos(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);

            Spinner cbBancosFormaPago = (Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            cbBancosFormaPago.setAdapter(adapter);
        }
    }

    public void CargarFormasPago() {

        listaFormaPago = DataBaseBO.CargarFormasPago(nroDoc);

        TableLayout table = new TableLayout(this);
        table.setBackgroundColor(Color.WHITE);

        HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollFormasDePago);
        scroll.removeAllViews();
        scroll.addView(table);

        if (listaFormaPago.size() > 0) {

            String[] headers = {" ", " VALOR ", " FORMA DE PAGO "};
            Util.Headers(table, headers, this);

            TextView textViewAux;
            ImageView imageViewAux;

            for (FormaPago formaPago : listaFormaPago) {

                TableRow fila = new TableRow(this);

                int ico_estado = R.drawable.op_borrar;
                imageViewAux = new ImageView(this);
                imageViewAux.setTag(formaPago.position);
                imageViewAux.setImageResource(ico_estado);
                imageViewAux.setOnClickListener(this);
                imageViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                fila.addView(imageViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(Util.SepararMiles("" + formaPago.monto));
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                textViewAux.setTextSize(19);
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(formaPago.descripcion);
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                textViewAux.setTextSize(19);
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                fila.addView(textViewAux);

                table.addView(fila);
            }
        }

        long totalPagos = TotalFormasPago();
        TextView lblTotalFormasPago = (TextView) findViewById(R.id.lblTotalFormasPago);
        lblTotalFormasPago.setText(Util.SepararMilesSin(Util.Redondear("" + totalPagos, 0)));

        TextView lblDiferenciaFormasPago = (TextView) findViewById(R.id.lblDiferenciaFormasPago);
        lblDiferenciaFormasPago.setText(Util.SepararMilesSin(Util.Redondear("" + (totalRecuado - totalPagos), 0)));
    }

    public void OnClickAgregar(View view) {

        MostrarDialogFormasPago(null);
    }

    public void OnClikTerminarFormaPago(View view) {

        long totalPagos = TotalFormasPago();

        if (totalPagos == totalRecuado) {

            setResult(RESULT_OK);
            finish();
			
			/*AlertDialog alertDialog;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					//Guardar Recaudo
					dialog.cancel();
					
					GuardarRecuado();
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
				}
			});

			alertDialog = builder.create();
	    	alertDialog.setMessage("La sumatoria de las formas de pago es superior al valor total de las facturas esta seguro de terminar el recaudo?");
	    	alertDialog.show();*/

        } else {

            Util.MostrarAlertDialog(this, "La sumatoria de las formas de pago de ser Igual al valor a Recuadar.");
        }
    }

    public void OnClikCancelarFormaPago(View view) {

        finish();
    }

    @Override
    public void onClick(View view) {

        String posTag = ((ImageView) view).getTag().toString();
        final int position = Util.ToInt(posTag);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Esta seguro de Eliminar la Forma de Pago?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        FormaPago formaPago = listaFormaPago.elementAt(position);
                        boolean elimino = DataBaseBO.EliminarFormaPago(formaPago.serial);

                        if (elimino) {

                            CargarFormasPago();

                        } else {

                            Util.MostrarAlertDialog(FormFormasPago.this, "No se pudo eliminar la Forma de Pago");
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean MostrarDialogFormasPago(FormaPago formaPago) {

        if (dialogFormasPago == null) {

            dialogFormasPago = new Dialog(this);
            dialogFormasPago.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialogFormasPago.setContentView(R.layout.dialog_formas_pago);
            dialogFormasPago.setTitle("Registrar Forma de Pago");

            CargarBancos();
        }

        String msgFormaPago = "";
        final String strTotalRecuado = Util.SepararMilesSin(Util.Redondear("" + totalRecuado, 0));
        String valorARecuadar = "<b>Valor A Recaudar $" + strTotalRecuado + "</b>";

        if (radioEfectivo.isChecked()) { //Forma Pago Efectivo

            msgFormaPago = "Pago en Efectivo";
            (((TableLayout) dialogFormasPago.findViewById(R.id.tableBanco))).setVisibility(TableLayout.GONE);
            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroCheque))).setVisibility(TableLayout.GONE);
            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroCuenta))).setVisibility(TableLayout.GONE);

        } else if (radioCheque.isChecked()) { //Forma Pago Cheque

            msgFormaPago = "Pago con Cheque";
            (((TableLayout) dialogFormasPago.findViewById(R.id.tableBanco))).setVisibility(TableLayout.VISIBLE);
            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroCheque))).setVisibility(TableLayout.VISIBLE);
            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroCuenta))).setVisibility(TableLayout.VISIBLE);

            if (formaPago != null) {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroCheque)).setText(formaPago.nroCheque);
                ((TextView) dialogFormasPago.findViewById(R.id.txtNroCuenta)).setText(formaPago.cuentaCheque);

            } else {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroCheque)).setText("");
                ((TextView) dialogFormasPago.findViewById(R.id.txtNroCuenta)).setText("");
            }
        }

        ((TextView) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).setText("");
        ((TextView) dialogFormasPago.findViewById(R.id.lblFormaPago)).setText(msgFormaPago);
        ((TextView) dialogFormasPago.findViewById(R.id.lblValorRecaudo)).setText(Html.fromHtml(valorARecuadar));

        String diferencia = lblDiferenciaFormasPago.getText().toString().replace(",", "");
        ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).setText(diferencia);

        ((Button) dialogFormasPago.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                String cant = ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).getText().toString();
                int monto = Util.ToInt(cant);

                if (monto == 0) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(dialogFormasPago.getContext());
                    builder.setMessage("Debe ingresar la cantidad")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int id) {

                                    ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).requestFocus();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    long totalPagos = TotalFormasPago();

                    if (monto + totalPagos <= totalRecuado) {

                        AgregarFormaPago(monto);

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(dialogFormasPago.getContext());
                        builder.setMessage("La sumatoria de las formas de Pago supera el Valor a Recaudar: $" + strTotalRecuado)
                                .setCancelable(false)
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int id) {

                                        ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).requestFocus();
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }
        });

        ((Button) dialogFormasPago.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                dialogFormasPago.cancel();
            }
        });

        dialogFormasPago.setCancelable(false);
        dialogFormasPago.show();
        dialogFormasPago.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_cartera);

        return true;
    }

    public long TotalFormasPago() {

        long total = 0;
        for (FormaPago formaPago : listaFormaPago) {

            total += formaPago.monto;
        }

        return total;
    }

    public void AgregarFormaPago(double monto) {

        //if (Main.formaPagoAct == null) {

        Main.formaPagoAct = new FormaPago();
        Main.formaPagoAct.nroDoc = nroDoc;
        Main.formaPagoAct.monto = monto;
        Main.formaPagoAct.serial = "A" + Util.ObtenerFechaId();

        if (radioEfectivo.isChecked()) {

            Main.formaPagoAct.formaPago = 1;
            Main.formaPagoAct.nroCheque = "";
            Main.formaPagoAct.codigoBanco = "";
            Main.formaPagoAct.cuentaCheque = "";

        } else if (radioCheque.isChecked()) {

            int index = ((Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
            Banco banco = listaBancos.elementAt(index);

            Main.formaPagoAct.formaPago = 2;
            Main.formaPagoAct.nroCheque = ((TextView) dialogFormasPago.findViewById(R.id.txtNroCheque)).getText().toString();
            Main.formaPagoAct.codigoBanco = banco.codigo;
            Main.formaPagoAct.cuentaCheque = ((TextView) dialogFormasPago.findViewById(R.id.txtNroCuenta)).getText().toString();
        }

        boolean agrego = DataBaseBO.AgregarFormaPago(Main.formaPagoAct);

        if (agrego) {

            CargarFormasPago();
            dialogFormasPago.cancel();

        } else {

            Util.MostrarAlertDialog(this, "No se pudo guardar la Forma de Pago");
        }
        //}
    }
	
	/*public void GuardarRecuado__() {
		
		boolean guardo = false; //DataBaseBO.GuardarRecaudo();
		
		if (guardo) {
			
			AlertDialog alertDialog;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int id) {
					
					dialog.cancel();
					
					Main.total_recaudo = 0;
					Main.total_descuento = 0;
					Main.total_forma_pago = 0;
					
					Main.cartera.removeAllElements();
					Main.listaDescuentos.clear();
					Main.listaFormaPago.clear();
					
					setResult(RESULT_OK);
					finish();
				}
			});

			alertDialog = builder.create();
	    	alertDialog.setMessage("Recaudo Guardado con exito para el cliente " + Main.cliente.razonSocial);
	    	alertDialog.show();
			
		} else {
			
			Util.MostrarAlertDialog(this, "Error guardando recaudo " + DataBaseBO.mensaje);
		}
	}*/

	/*private View.OnClickListener onClickHandler = new View.OnClickListener() {
		public void onClick(View target) {
			if (target.getId() == R.id.radioEfectivo) {
			} else if (target.getId() == R.id.radioCheque) {
			}
		}
	};*/
	
	/*public void OnClikAgregarFormaPago(View view) {
		
		Intent FormOpcionesPagos = new Intent(this, FormOpcionesPagosActivity.class);
		startActivityForResult(FormOpcionesPagos, Const.RESP_FORM_OPCIONES_PAGO);
	}*/
	
	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == Const.RESP_FORM_OPCIONES_PAGO && resultCode == RESULT_OK) {
			
			CargarFormasPago();
		}
	}*/
	
	/*public void CargarOpcionesFormaPago() {
	
		String[] items = new String[] {"Efectivo", "Cheque", "Cheque Postfechado", "Consignacion"};
		Spinner cbFormasDePago = (Spinner) dialogOpcionesPago.findViewById(R.id.cbFormasDePago);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
	    
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    cbFormasDePago.setAdapter(adapter);
	}*/
	
	/*public void CargarFormasPago____() {
	
		//ItemListView[] listaItems;
		//ItemListView itemListView;
		//Vector<ItemListView> items = new Vector<ItemListView>();
		
		Main.total_forma_pago = 0;
		Enumeration<FormaPago> e = Main.listaFormaPago.elements(); 
		
		while(e.hasMoreElements()) {
			
			FormaPago formaPago = e.nextElement();
			Main.total_forma_pago += formaPago.valor;
			
			itemListView = new ItemListView();
			itemListView.titulo = formaPago.descripcion + "  " + Util.SepararMilesSin("" + formaPago.valor);
			items.addElement(itemListView);
		}
		
		if (items.size() > 0) {
			
			listaItems = new ItemListView[items.size()];
			items.copyInto(listaItems);
			
			ListAdapter adapter = new ListAdapter(this, listaItems, 0);
			ListView listaPedido = (ListView)findViewById(R.id.listaFormasDePago);
			listaPedido.setAdapter(adapter);
			
		} else {
			
			ListAdapter adapter = new ListAdapter(this, new ItemListView[]{}, 0);
			ListView listaPedido = (ListView)findViewById(R.id.listaFormasDePago);
			listaPedido.setAdapter(adapter);
		}
		
		TextView lblTotalFormasPago = (TextView)findViewById(R.id.lblTotalFormasPago);
		lblTotalFormasPago.setText(Util.SepararMilesSin("" + Main.total_forma_pago));
		
		TextView lblDiferenciaFormasPago = (TextView)findViewById(R.id.lblDiferenciaFormasPago);
		lblDiferenciaFormasPago.setText(Util.SepararMilesSin("" + (Main.total_recaudo - (Main.total_descuento + Main.total_forma_pago))));
	}*/
}
