package celuweb.com.uyusa;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.DataObject.Usuario;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class ServiceGPS extends Service implements Sincronizador,
		LocationListener {

	private final IBinder binder = new LocalBinder();
	private double latitude;
	private double longitude;
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public void onCreate() {

		Log.i(TAG, "Iniciando Servicio GPS...");
		iniciarGPS();
	}

	@Override
	public void onDestroy() {

		Log.i(TAG, "Deteniendo Servicio GPS...");
		detenerGPS();
	}

	@Override
	public void onStart(Intent intent, int startid) {
	}

	@Override
	public void RespSync(boolean ok, String respuestaServer, String msg,
			int codeRequest) {
	}

	public class LocalBinder extends Binder {

		ServiceGPS getService() {
			return ServiceGPS.this;
		}
	}

	/**********************************************************************************************************
	 ************************************** Definicion del Modulo de GPS **************************************
	 *********************************************************************************************************/
	public void iniciarGPS() {

		Coordenada.crearTablaCoordenadas();

		lanzarHandlerGPS();

	}

	public boolean isProviderEnabled(String provider) {

		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		if (manager != null) {
			return manager.isProviderEnabled(provider);
		}

		return false;
	}

	public void lanzarHandlerGPS() {

		detenerGPS();

		if (gpsHandler == null) {
			gpsHandler = new Handler();
		}

		gpsHandler.postDelayed(runnable, 0);
	}

	public void detenerGPS() {

		if (gpsHandler != null && runnable != null) {
			gpsHandler.removeCallbacks(runnable);
		}

		if (locationManager != null) {
			locationManager.removeUpdates(this);
		}
	}

	public void enviarCoordenada(Location currentLocation) {

		double latitud = 0;
		double longitud = 0;
		int estado = Coordenada.ESTADO_GPS_APAGADO;
		if (currentLocation != null) {

			latitud = currentLocation.getLatitude();
			longitud = currentLocation.getLongitude();
			estado = Coordenada.ESTADO_GPS_CAPTURO;

		}

		Usuario usuario = DataBaseBO.obtenerUsuario();
		if (usuario != null) {

			Coordenada coordenada = new Coordenada();
			coordenada.codigoVendedor = usuario.codigoVendedor;
			coordenada.codigoCliente = "-6";
			coordenada.latitud = latitud;
			coordenada.longitud = longitud;
			coordenada.horaCoordenada = Util.FechaActual("HH:mm:ss");
			coordenada.estado = estado;
			coordenada.id = Coordenada.obtenerId(usuario.codigoVendedor);

			Sync sync = new Sync(ServiceGPS.this, Const.ENVIAR_COORDENADAS);
			sync.coordenada = coordenada;
			sync.start();

		}
		detenerGPS();

	}

	public void registrarListenerGPS() {

		try {

			enviar = true;
			Location location = null;

			if (locationManager == null) {
				locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			}

			// verficamos que se pueda iniciar los servicios de gpsProvider
			// y netWorProvider

			boolean isGPSEnable = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			boolean isNetworkEnable = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			// verificamos que se puedan capturar coordenadas
			if (isGPSEnable || isNetworkEnable) {

				if (isNetworkEnable) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER, 0, 0, this);

					if (locationManager != null) {

						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {

							if (location != null) {
								latitude = location.getLatitude();
								longitude = location.getLongitude();

							}
						}
					}

				}

				if (isGPSEnable) {

					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER, time,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

						if (locationManager != null) {

							location = locationManager
									.getLastKnownLocation(LocationManager.GPS_PROVIDER);

							if (location != null) {

								latitude = location.getLatitude();
								longitude = location.getLongitude();

							}

						}
					}
				}

			}

			enviarCoordenada(location);

		} catch (Exception e) {

			Log.e(TAG, "registrarListenerGPS -> " + e.getMessage(), e);
		}
	}

	private Runnable runnable = new Runnable() {

		@Override
		public void run() {

			registrarListenerGPS();
			gpsHandler.postDelayed(this, time);
		}
	};

	private class GPSListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {

			if (location != null && enviar) {

				enviar = false;
				enviarCoordenada(location);
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	static Handler gpsHandler;
	static GPSListener gpsListener;
	static LocationManager locationManager;

	static boolean enviar = true;

	// 3 Segundos para que el listener GPS espere entre una captura y otra.
	public long minTime = 3 * 1000;

	// Registra Coordenadas Cada 15 Minutos
	public long time = 10 * 60 * 1500;

	public static final String TAG = ServiceGPS.class.getName();

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();

			locationManager.removeUpdates(this);

		}
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}
}
