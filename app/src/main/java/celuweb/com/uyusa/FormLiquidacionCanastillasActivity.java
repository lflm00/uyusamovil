package celuweb.com.uyusa;

import java.util.UUID;
import java.util.Vector;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.BusinessObject.PrinterBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.RegCanastilla;
import celuweb.com.DataObject.Usuario;
import celuweb.com.printer.sewoo.SewooLKP20;

@SuppressLint("NewApi")
public class FormLiquidacionCanastillasActivity extends Activity  { 

	public static final String TAG = FormLiquidacionCanastillasActivity.class.getName();
	
	Typeface fontMM;
	Typeface font;
	String cod_cliente;
	ProgressDialog progressDialog;
	boolean salir;
	private Vector<RegCanastilla> listaProductos;
	Cliente cliente = null;
	Usuario usuario = null;
	Dialog dialogEnviarInfo;
	boolean tienePedido = false;
	int opcionLiquidar = 0 ;
	boolean existeReg = false;
	boolean primerEjecucion = true;
	private  String macImpresora;
	String mensaje;
	String nroDocRegistro ;
	String nroDocRegistroGlobal ;
	Usuario liquidador;
	boolean isPedido = false;
	Vector<RegCanastilla> listaItemCanastillaToSave2;
	
	/**
	 * Referencia para acceder a la confguracion de la impresora sewoo LK-P20
	 */
	private SewooLKP20 sewooLKP20;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_liquidacion_canastillas);
		
		Util.closeTecladoStartActivity(this);
		cargarBundle();
		inializarVista();
	}
	
	
	private void cargarBundle() {

		Bundle bundle = getIntent().getExtras();

		if (bundle != null)
			opcionLiquidar = bundle.getInt("liquidacionOpciones");

		liquidador = new Usuario();
		if (bundle != null && bundle.containsKey("liquidador"))
			liquidador = (Usuario) bundle.getSerializable("liquidador");

		if (liquidador != null)
			System.out.println("Liquidador:\n\n" + liquidador.codigoVendedor + " - " + liquidador.nombreVendedor);
		
		if(bundle != null)
			isPedido = bundle.getBoolean("isPedido");
		
		if(isPedido)
			System.out.println("ENTRO DESDE PEDIDO *********+");
		else 
			System.out.println("ENTR� DESDE LIQUIDACI�N");

	}
	
	private void inializarVista() {
		
		switch (opcionLiquidar) {
		case Const.CANASTILLA_VACIA:
			((TextView)findViewById(R.id.txtInfo)).setText("Canastas Vacias");
			break;
			
		case Const.INVENTARIO_CANASTA:
			((TextView)findViewById(R.id.txtInfo)).setText("Inventario Canastas");
			break;
			
		case Const.DESCARGA_CANASTA:
			((TextView)findViewById(R.id.txtInfo)).setText("Descarga Canastas");
			break;

		default:
			((TextView)findViewById(R.id.txtInfo)).setText("Liquidacion Canastillas");
			break;
		}
		
	}


	@Override
	public void onResume() {
		super.onResume();
		
		cliente = Main.cliente;
		usuario = Main.usuario;
		if (cliente==null) {
			cliente = DataBaseBO.CargarClienteSeleccionado();
		}
		if (usuario==null) {
			usuario = DataBaseBO.CargarUsuario();
		}
		
		cargarListaCanastilla();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	


	public boolean cargarListaCanastilla(){

		boolean state = false;
		
		listaProductos = DataBaseBO.listaCanastilla(cliente.codigo, "", false, opcionLiquidar);
		
		if (listaProductos != null) {

			TableLayout tablaHeadListaProductosInv = (TableLayout) findViewById(R.id.tablaHeadListaClientesPadre);
			tablaHeadListaProductosInv.removeAllViews();

			LayoutInflater inflater1 = this.getLayoutInflater();
			View viewhead = inflater1.inflate(R.layout.list_item_tabla_head_canastilla, null);
			
			existeReg = DataBaseBO.existeRegLiquidacionCanastilla(opcionLiquidar);
			
		
			if(opcionLiquidar == Const.CANASTILLA_VACIA){
				
				String[] head = { "Codigo", "Nombre",  "Vacias" };
				
				for (int i = 0; i < head.length; i++) {
					TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
					textView.setText(head[i]);
					textView.setVisibility(View.VISIBLE);	
				}
				
			}else if(opcionLiquidar == Const.INVENTARIO_CANASTA){
				
				String[] head = { "Codigo", "Nombre",  "Existente" };
				
				for (int i = 0; i < head.length; i++) {
					TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
					textView.setText(head[i]);
					textView.setVisibility(View.VISIBLE);	
					
				}
				
			}else if(opcionLiquidar == Const.DESCARGA_CANASTA){
				
				String[] head = { "Codigo", "Nombre",  "Descarga" };
				
				for (int i = 0; i < head.length; i++) {
					TextView textView = (TextView) viewhead.findViewWithTag("column" + i);
					textView.setText(head[i]);
					textView.setVisibility(View.VISIBLE);	
				
				}
			}
				
				
			tablaHeadListaProductosInv.addView(viewhead);

			TableLayout tablaBodyListaProductosInventario = (TableLayout) findViewById(R.id.tablaBodyListaClientesPadre);
			tablaBodyListaProductosInventario.removeAllViews();
			int position = 0;
			for (RegCanastilla clPadre : listaProductos) {
				if(pintarDetalleChequeo(clPadre, tablaBodyListaProductosInventario, position, existeReg)){
					state = true;
				}
				position++;
			}
		} else {

			TableLayout tablaListaAgendaEjecutiva = (TableLayout) findViewById(R.id.tablaHeadListaClientesPadre);
			tablaListaAgendaEjecutiva.removeAllViews();
			state = false;
			Util.mostrarToast(FormLiquidacionCanastillasActivity.this, "No existen Canastillas.");
		}
		
		if(existeReg)
			((Button)findViewById(R.id.btnGuardar)).setVisibility(View.GONE);
		
		
		return state;
	}
	
	public boolean pintarDetalleChequeo(final RegCanastilla item, LinearLayout tablaHeadListaProductosInv, int position, boolean existeReg) {

		LayoutInflater inflater = this.getLayoutInflater();
		View view = inflater.inflate(R.layout.list_item_tabla_canastilla, null);

		TableRow rowTableElemento = (TableRow)view.findViewById(R.id.rowTableElemento);
		rowTableElemento.setTag(item);
		
		TextView codigo = (TextView) view.findViewById(R.id.column0);
		codigo.setText(" "+item.codigo);
		codigo.setTag(item);
		codigo.setVisibility(View.VISIBLE);
		if ( position % 2 == 1 ) {
			codigo.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		TextView descripcion = (TextView) view.findViewById(R.id.column1);
		descripcion.setText(item.descripcion);
		descripcion.setTag(item);
		descripcion.setVisibility(View.VISIBLE);
		if ( position % 2 == 1 ) {
			descripcion.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		
		LinearLayout columnRecibida = (LinearLayout) view.findViewById(R.id.column3);
		columnRecibida.setVisibility(View.VISIBLE);
		
		EditText edtRecibida = (EditText) view.findViewById(R.id.edTxtRecibido);
		edtRecibida.setText(item.devuelta);
		edtRecibida.setTag(item);
		if(existeReg)edtRecibida.setEnabled(false);else edtRecibida.setEnabled(true);
		
		if ( position % 2 == 1 ) {
			columnRecibida.setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_table_list_item_gray));
		}
		
		
		tablaHeadListaProductosInv.addView(view);
		return true;

	}


	
	public void salir() {
	
			setResult(Activity.RESULT_OK);
			finish();
			
	}
	
	public void cancelar() {
		finish();
	}
	
	public void onClickGuardar(View view) {
		
		
		AlertDialog alertDialog;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false)
		
		.setPositiveButton("Si", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				dialog.cancel();
				continuarTransaccion();
			}
		})
		
		
		
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int id) {
				
				dialog.cancel();
			}
		});

		alertDialog = builder.create();
    	alertDialog.setMessage("Esta Seguro de Terminar?");
    	alertDialog.show();
		

		

	}
	
	protected void continuarTransaccion() {
		
		Usuario user = DataBaseBO.ObtenerUsuario();
		
		Vector<RegCanastilla> listaItemCanastillaToSave = new Vector<RegCanastilla>();
		nroDocRegistroGlobal = "RG"+user.codigoVendedor+ Util.FechaActual("yyyyMMddHHmmss");
		
		TableLayout tablaBodyListaContactos= (TableLayout) findViewById(R.id.tablaBodyListaClientesPadre);

		for (int i = 0; i < tablaBodyListaContactos.getChildCount(); i++) {

			TableRow rowTableElemento = (TableRow) tablaBodyListaContactos.getChildAt(i);
			RegCanastilla item = (RegCanastilla) rowTableElemento.getTag();
	
			EditText edtRecibida =(EditText) rowTableElemento.findViewById(R.id.edTxtRecibido);
			
			item.devuelta = edtRecibida.getText().toString();
			
			if(!item.devuelta.equals("")){

			RegCanastilla regCanastilla = new RegCanastilla();

			regCanastilla.codigo = item.codigo;
			regCanastilla.devuelta = item.devuelta != null ? item.devuelta: "0" ;
			regCanastilla.codCliente = cliente.codigo;
			regCanastilla.vendedor = user.codigoVendedor;
			regCanastilla.nroDoc = nroDocRegistroGlobal;

			listaItemCanastillaToSave.addElement(regCanastilla);
			
			}else{
				
				Util.mostrarToast(this, "Por favor ingrese una cantidad para "+item.descripcion);
				return;
			}
			

		}
		
		listaItemCanastillaToSave2 = listaItemCanastillaToSave;
		
		if (listaItemCanastillaToSave.size() > 0) {
			
			
			int contador = 0;
			
			for(int i=0;i<listaItemCanastillaToSave.size();i++){
				
				RegCanastilla regCanastilla  = listaItemCanastillaToSave.elementAt(i);
				
				if(Integer.parseInt(regCanastilla.devuelta) == 0)
					contador++;
				
			}
			
			if(contador == listaItemCanastillaToSave.size()){
				
				AlertDialog alertDialog;
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setCancelable(false)
				
				.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.cancel();
						
						
						if (DataBaseBO.guardarRegCanastilla(listaItemCanastillaToSave2, tienePedido, opcionLiquidar, usuario.codigoVendedor)) {
							
							if(opcionLiquidar == Const.INVENTARIO_CANASTA)
								if(DataBaseBO.guardarInformeLiquidadoCanastillas() && DataBaseBO.guardarEncabezadoCanastilla())
									System.out.println("Guardo exitosamente");
									
							
									mensajeGuardar("Guardo el registro exitosamente.");
							
						} else {
							Util.MostrarAlertDialog(FormLiquidacionCanastillasActivity.this,
									"Ocurrio un error al guardar el registro.");
							return;
						}
						
						
						
						
					}
				})
				
				
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.cancel();
					}
				});

				alertDialog = builder.create();
		    	alertDialog.setMessage("No ha registrado cantidades, desea continuar?");
		    	alertDialog.show();
			
				
			
			}else{
			
			

			if (DataBaseBO.guardarRegCanastilla(listaItemCanastillaToSave, tienePedido, opcionLiquidar, usuario.codigoVendedor)) {
				
				if(opcionLiquidar == Const.INVENTARIO_CANASTA)
					if(DataBaseBO.guardarInformeLiquidadoCanastillas() && DataBaseBO.guardarEncabezadoCanastilla())
						System.out.println("Guardo exitosamente");
						
				
						mensajeGuardar("Guardo el registro exitosamente.");
				
			} else {
				Util.MostrarAlertDialog(FormLiquidacionCanastillasActivity.this,
						"Ocurrio un error al guardar el registro.");
				return;
			}
			
		}
			
		}else{
			Util.mostrarToast(FormLiquidacionCanastillasActivity.this, "No tiene registros por guardar.");
			return;
		}
		
	}


	public void onClickImprimir (View view){
		
		if(!existeReg){
			Util.mostrarToast(this, "No existen registros para imprimir.");
		}else{
			
			nroDocRegistroGlobal = DataBaseBO.obtenerNroDocRegLiquidacionCanastas(opcionLiquidar);
			impresionTirillas();
			
		}
		
	}
	
	protected void mensajeGuardar(String mensaje) {
		
		AlertDialog alertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(FormLiquidacionCanastillasActivity.this);
		builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				
				
				dialog.cancel();
				salir();
			}
		});
		alertDialog = builder.create();
		alertDialog.setMessage(mensaje);
		alertDialog.show();
		
	}


	public void onClickSalir(View view) {
		cancelar();  	
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cancelar(); 
		}
		return true;
	}
	
	private void impresionTirillas() {
		
		String copiaPrint ="";
		if(primerEjecucion){
			primerEjecucion = false;
			copiaPrint= "ORIGINAL";
		}else{
			copiaPrint = "COPIA";
		}
		
		progressDialog = ProgressDialog.show(FormLiquidacionCanastillasActivity.this, "", "Por Favor Espere...\n\nProcesando Informacion!", true);
		progressDialog.show();
		
		
		SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
		macImpresora = settings.getString(Const.MAC_IMPRESORA, "-");

		SharedPreferences set = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
		String tipoImpresora = set.getString(Const.TIPO_IMPRESORA, "otro");

		if (macImpresora.equals("-")) {
			
			Util.MostrarAlertDialog(FormLiquidacionCanastillasActivity.this, "Aun no hay Impresora Establecida.\n\nPor Favor primero Configure la Impresora!");
			if (progressDialog != null)
				progressDialog.cancel();
			
			
		} else {
			if (!tipoImpresora.equals("Intermec")) {
				sewooLKP20 = new SewooLKP20(FormLiquidacionCanastillasActivity.this);
				imprimirSewooLKP20(macImpresora, nroDocRegistroGlobal);
			} else {
				imprimirTirillaGeneral(macImpresora, nroDocRegistroGlobal, copiaPrint);
			}
		}
		
	}
	

	/**
	 * Imprimir la factura del pedido.
	 * @param macImpresora
	 * @param numero_doc
	 * @param copiaPrint
	 */
	protected void imprimirSewooLKP20(final String macImpresora, final String numero_doc) {
		
		
		final Usuario usuario = DataBaseBO.ObtenerUsuario();
		
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				Looper.prepare();

				if (macImpresora.equals("-")) {
					if (progressDialog != null)
						progressDialog.dismiss();
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Toast.makeText(FormLiquidacionCanastillasActivity.this, "Aun no hay Impresora Predeterminada.\n\nPor Favor primero Configure la Impresora!",
									Toast.LENGTH_SHORT).show();
						}
					});
				} else {

					if (sewooLKP20 == null) {
						sewooLKP20 = new SewooLKP20(FormLiquidacionCanastillasActivity.this);
					}
					int conect = sewooLKP20.conectarImpresora(macImpresora);

					switch (conect) {
					case 1:
						sewooLKP20.generarCanastillaLiquidacionTirilla(opcionLiquidar, usuario, numero_doc, liquidador);
						sewooLKP20.imprimirBuffer(true);
						break;

					case -2:
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(FormLiquidacionCanastillasActivity.this, "-2 fallo conexion", Toast.LENGTH_SHORT).show();
							}
						});
						break;

					case -8:
						if (progressDialog != null)
							progressDialog.dismiss();
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Util.MostrarAlertDialog(FormLiquidacionCanastillasActivity.this, "Bluetooth apagado. Por favor habilite el bluetoth para imprimir.");
							}
						});
						break;

					default:
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(FormLiquidacionCanastillasActivity.this, "Error desconocido, intente nuevamente.", Toast.LENGTH_SHORT).show();
							}
						});
						break;
					}

					try {
						Thread.sleep(2500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (sewooLKP20 != null) {
						sewooLKP20.desconectarImpresora();
						if (progressDialog != null)
							progressDialog.dismiss();
					}
				}
				Looper.myLooper().quit();
			}
		}).start();
	}
	
	private void imprimirTirillaGeneral(final String macAddress,  final String numeroDoc, final String copiaPrint) { 

		
		new Thread(new Runnable() {

			public void run() {

				mensaje = "";
				BluetoothSocket socket = null;

				try {

					Looper.prepare();

					BluetoothAdapter bluetoothAdapter = BluetoothAdapter
							.getDefaultAdapter();
					

					if (bluetoothAdapter == null) {

						mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

					} else if (!bluetoothAdapter.isEnabled()) {

						mensaje = "No hubo conexion con la impresora.\n\nPor Favor intente de nuevo.";

					} else {

						BluetoothDevice printer = null;
					
						printer = bluetoothAdapter.getRemoteDevice(macAddress);

						if (printer == null) {

							mensaje = "No se pudo establecer la conexion con la Impresora.";

						} else{
							
								UUID uuid = UUID
										.fromString("00001101-0000-1000-8000-00805F9B34FB");
								
								SharedPreferences settings = getSharedPreferences(Const.CONFIG_IMPRESORA, MODE_PRIVATE);
								String tipoImpresora = settings.getString(Const.TIPO_IMPRESORA, "otro");
								
							
								if(tipoImpresora.equals("Intermec")){

								socket = printer.createInsecureRfcommSocketToServiceRecord(uuid);
								
								
								}else{
									
									socket = printer
											.createRfcommSocketToServiceRecord(uuid);
									
								}
								
								if (socket != null) {

									socket.connect();
						
									Thread.sleep(3500);
									
									if(tipoImpresora.equals("Intermec")){
										
										ReporstPrinter.ImprimiendoPrinter(socket, PrinterBO.formatoVentaPedidoItermerc( numeroDoc, copiaPrint, false, false));
								
									}
									
									
									handlerFinish.sendEmptyMessage(0);

								} else {

									mensaje = "No se pudo abrir la conexion con la Impresora.\n\nPor Favor intente de nuevo.";
								}
								
						}

						}
					

					if (!mensaje.equals("")) {

				
						handlerFinish.sendEmptyMessage(0);
					}

					Looper.myLooper().quit();

				} catch (Exception e) {

					String motivo = e.getMessage();

					mensaje = "No se pudo ejecutar la Impresion.";

					if (motivo != null) {
						mensaje += "\n\n" + motivo;
					}

			
					handlerFinish.sendEmptyMessage(0);

				} finally {

					
				}
			}

		}).start();
		
		
	}
	
	private Handler handlerFinish = new Handler() {
		
		@Override
		public void handleMessage(Message msg) {
			
			if (progressDialog != null)
				progressDialog.cancel();
			
			if(Main.usuario.tipoVenta.equals(Const.AUTOVENTA)){
				
			/*	progressDialog = ProgressDialog.show(FormInfoClienteActivity.this, "", "Enviando Informacion Pedido...", true);
				progressDialog.show();
		
				Sync sync = new Sync(FormInfoClienteActivity.this, Const.ENVIAR_PEDIDO);
				sync.start();  */
				
			}
			

	    }
	};


	
	
}
