package celuweb.com.uyusa;

/**
 * Created by john-f on 09/03/2018.

 */
import android.app.Application;
import android.os.StrictMode;

public class MyApplication extends Application {
    private static MyApplication instance;

    public MyApplication() {
        instance = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    public static MyApplication getInstance() {
        return instance;
    }

}