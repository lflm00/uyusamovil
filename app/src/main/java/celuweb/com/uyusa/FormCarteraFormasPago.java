package celuweb.com.uyusa;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cartera;
import celuweb.com.DataObject.CarteraConsignacion;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.DataObject.Foto;
import celuweb.com.DataObject.objeto;

public class FormCarteraFormasPago extends Activity implements OnClickListener {

    long tiempoClick1 = 0;

    private Spinner cbFormasDePago;

    private TextView lblDiferenciaFormasPago;

    private long mLastClickTime = 0;

    ProgressDialog progressDialog;

    static int opcSel = 1;

    String nroDoc = "";

    double totalRecuado = 0;

    String codCliente = "";

    Dialog dialogFormasPago;

    Vector<celuweb.com.DataObject.Banco> listaBancos;

    Dialog dialogBusquedaDeBancos;


    private int anchoImg, altoImg;
    private boolean estaGuardando = false, fotoGuardada = false;
    private AlertDialog alertDialog;

    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    private String idFoto;


    @Override
    protected void onCreate (Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_cartera_formas_pago);
        inicializar();
        idFoto = "efectivo";
        CargarInformacion();
        CargarTablaFormasPago();
        if (Main.fotoActual != null)
            Main.fotoActual = null;
    }



    public void inicializar () {

        String consecutivo = DataBaseBO.ObtenterNumeroDoc(Main.usuario.codigoVendedor);
        nroDoc = consecutivo;//DataBaseBO.obtenterNumeroDocRecaudo(consecutivo);

        this.lblDiferenciaFormasPago = (TextView) findViewById(R.id.lblDiferenciaFormasPago);
    }



    public void CargarInformacion () {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            if (bundle.containsKey("totalRecuado"))
                totalRecuado = bundle.getDouble("totalRecuado");

//            if (bundle.containsKey("nroDoc"))
//                nroDoc = bundle.getString("nroDoc");

            //			if(bundle.containsKey("codigoCliente"))
            //				codCliente = bundle.getString("codigoCliente");
        }


        //totalRecuado = Util.round(totalRecuado, 2);

        // String cadena =
        // "Para Ingresar las Formas de Pago, Por favor Seleccione una Forma de Pago y haga Click en el Boton Agregar.";
        String cadena = getString(R.string.seleccione_formas_de_pago);
        String mensaje = Util.SepararPalabrasTextView(cadena, 50);

        TextView lblFormaPago = (TextView) findViewById(R.id.lblFormaPago);
        lblFormaPago.setText(Html.fromHtml(mensaje));

        String strTotalRecaudo = "" + totalRecuado;
        strTotalRecaudo = Util.QuitarE(strTotalRecaudo.replace("e+", "E"));

        TextView lblTotalRecaudo = (TextView) findViewById(R.id.lblTotalRecaudo);
        lblTotalRecaudo.setText(Util.SepararMilesSin(Util.Redondear(strTotalRecaudo, 2))); /*
                                     * (Main.total_recaudo -
									 * Main.total_descuento)
									 */

        String strTotalFormaPago = Util.SepararMilesSin(Util.Redondear("" + Main.total_forma_pago, 2));

        TextView lblTotalFormasPago = (TextView) findViewById(R.id.lblTotalFormasPago);
        lblTotalFormasPago.setText(strTotalFormaPago);

        String strDiferencia = "" + (Main.total_recaudo - (Main.total_descuento + Main.total_forma_pago));
        strDiferencia = Util.QuitarE(strDiferencia.replace("e+", "E"));

        lblDiferenciaFormasPago.setText(strDiferencia);
    }



    public void CargarTablaFormasPago () {

        TableLayout table = new TableLayout(this);
        table.setBackgroundColor(Color.WHITE);

        HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollFormasDePago);
        scroll.removeAllViews();
        scroll.addView(table);

        if (Main.listaFormasPago.size() > 0) {

            String[] headers = {" ", getString(R.string._valor_), getString(R.string._forma_de_pago_)};
            Util.Headers(table, headers, this);

            TextView textViewAux;
            ImageView imageViewAux;

            int i = 0;

            for (FormaPago formaPago : Main.listaFormasPago) {

                TableRow fila = new TableRow(this);

                int ico_estado = R.drawable.op_borrar;
                imageViewAux = new ImageView(this);
                imageViewAux.setTag(i++ /* formaPago.position */);
                imageViewAux.setImageResource(ico_estado);
                imageViewAux.setOnClickListener(this);
                // imageViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                imageViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                imageViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
                fila.addView(imageViewAux);


                formaPago.monto = Util.round((float) formaPago.monto, 2);
                String strMonto = "" + formaPago.monto;
                strMonto = Util.QuitarE(strMonto.replace("e+", "E"));

                textViewAux = new TextView(this);
                textViewAux.setText(Util.SepararMiles(Util.Redondear(strMonto, 2)));
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                textViewAux.setTextSize(19);
                // textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                textViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
                fila.addView(textViewAux);

                textViewAux = new TextView(this);
                textViewAux.setText(formaPago.descripcion);
                textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
                textViewAux.setTextSize(19);
                // textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
                textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
                textViewAux.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
                fila.addView(textViewAux);

                table.addView(fila);
            }
        }

        float totalPagos = TotalFormasPago();
        String strTotalPagos = String.valueOf(totalPagos);
        strTotalPagos = Util.QuitarE(strTotalPagos.replace("e+", "E"));

        TextView lblTotalFormasPago = (TextView) findViewById(R.id.lblTotalFormasPago);
        lblTotalFormasPago.setText(Util.SepararMilesSin(Util.Redondear(strTotalPagos, 2)));

        String strDiferencia = String.valueOf(totalRecuado - totalPagos);
        strDiferencia = Util.QuitarE(strDiferencia.replace("e+", "E"));

        TextView lblDiferenciaFormasPago = (TextView) findViewById(R.id.lblDiferenciaFormasPago);
        lblDiferenciaFormasPago.setText(Util.SepararMilesSin(Util.Redondear(strDiferencia, 2)));
    }



    @Override
    public void onClick (View view) {

        String posTag = ((ImageView) view).getTag().toString();
        final int position = Util.ToInt(posTag);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.eliminar_forma_pago)).setCancelable(false).setPositiveButton(getString(R.string.yes), new
				DialogInterface.OnClickListener() {

            public void onClick (DialogInterface dialog, int id) {

                Main.listaFormasPago.remove(position);
                Util.guardarDatosDeRecaudo();
                refrescarDatosRecaudo();
                CargarTablaFormasPago();
            }
        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

            public void onClick (DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



    public void CargarFormaPago () {

        String[] items = new String[]{getString(R.string.efectivo), getString(R.string.cheque), getString(R.string.cheque_postfechado), getString(R
				.string.consignacion)};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbFormasDePago.setAdapter(adapter);
    }



    public void OnClickAgregar (View view) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }

        mLastClickTime = SystemClock.elapsedRealtime();
        mostrarDialogFormasPago(null);

    }



    public void OnClikCancelarFormaPago (View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

            public void onClick (DialogInterface dialog, int id) {

                dialog.cancel();
                Main.listaFormasPago.removeAllElements();
                Util.guardarDatosDeRecaudo();
                refrescarDatosRecaudo();

						/* borrar tablas temporales */
                //						DataBaseBO.borrarTempFormaPagoDetalleRecaudos();
                finish();

            }
        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

            public void onClick (DialogInterface dialog, int id) {

                dialog.cancel();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setMessage(getString(R.string.seguro_desea_cancelar_las_formas_de_pago_));
        alertDialog.show();

    }



    public void terminarRecaudo () {

        float totalPagos = TotalFormasPago();

        //Redondeo de total recuado - el total descuento con dos decimales
        double varAux = Double.parseDouble(Util.Redondear("" + (Main.total_recaudo - Main.total_descuento), 2));

        if (totalPagos > varAux) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

                public void onClick (DialogInterface dialog, int id) {

                    // Guardar Recaudo
                    dialog.cancel();
                    guardarRecaudo();
                    // onClickTerminarRecaudo.reset();
                }
            }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

                public void onClick (DialogInterface dialog, int id) {

                    dialog.cancel();
                    // onClickTerminarRecaudo.reset();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.setMessage(getString(R.string.txt_recaudo_superior));
            alertDialog.show();

        } else if (totalPagos == varAux) {

            guardarRecaudo();

        } else {

            alertTerminarRecaudo(getString(R.string.txt_recaudo_inferior));
        }
    }



    public void alertTerminarRecaudo (String mensaje) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {

            public void onClick (DialogInterface dialog, int id) {

                dialog.cancel();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }



    public float TotalFormasPago () {

        float total = 0;
        for (FormaPago formaPago : Main.listaFormasPago) {

            total += formaPago.monto;
        }

        total = Util.round(total, 2);
        return total;


    }



    public boolean mostrarDialogFormasPago (FormaPago formaPago) {

        if (dialogFormasPago == null) {

            dialogFormasPago = new Dialog(this);
            dialogFormasPago.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogFormasPago.setContentView(R.layout.dialog_formas_pago);
            dialogFormasPago.setTitle("Registrar Forma de Pago "+ formaPago); // +descripcionFP

            initDialog();
            cargarBancos();
            cargarPlazas();
        } else {
            cargarBancos();
        }

        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int ancho = display.getWidth();
        int alto = display.getHeight();

        anchoImg = (ancho * 150) / 140;
        altoImg = (alto * 180) / 220;

        if (Main.fotoActual != null) {

            ((ImageView) dialogFormasPago.findViewById(R.id.imageFotoPagos)).setImageDrawable(Main.fotoActual);

        } else {

            SetPhotoDefault();
        }


        String msgFormaPago = "";

        String strTotal = "" + totalRecuado;
        strTotal = Util.QuitarE(strTotal.replace("e+", "E"));

        //		final String strTotalRecuado = Util.SepararMilesSin(Util.Redondear(strTotal, 2));
        String valorARecuadar = getString(R.string._b_valor_a_recaudar_) + " " + strTotal;

        String tipoPago = "";
        // String tipoPago = cbFormasDePago.getSelectedItem().toString();

        if (FormCarteraFormasPago.opcSel == 1)
            tipoPago = "Efectivo";

        if (FormCarteraFormasPago.opcSel == 2) {
            tipoPago = "Cheque";
            estaGuardando = false;
        }

        if (FormCarteraFormasPago.opcSel == 3) {
            tipoPago = "cheque_postfechado";
            estaGuardando = false;
        }
        if (FormCarteraFormasPago.opcSel == 4) {
            tipoPago = "Consignacion";
            estaGuardando = false;
        }

        if(FormCarteraFormasPago.opcSel == 5) {
            tipoPago = "Transferencia";
            estaGuardando = false;
        }

        if (tipoPago.equals("Efectivo")) { // Forma Pago Efectivo

            msgFormaPago = getString(R.string.pago_en_efectivo);
            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblTomarFoto))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblGaleria))).setVisibility(TableLayout.GONE);


        } else if (tipoPago.equals("Cheque")) { // Forma Pago Cheque

            msgFormaPago = getString(R.string.pago_cheque);
            ((TextView) dialogFormasPago.findViewById(R.id.txtLblNroDoc)).setText(getString(R.string.nro_cheque));

            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblTomarFoto))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblGaleria))).setVisibility(TableLayout.VISIBLE);


            if (formaPago != null) {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText(formaPago.nroCheque);

            } else {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText("");
            }

        } else if (tipoPago.equals("cheque_postfechado")) { // Forma Pago Cheque
            // Postfechado

            msgFormaPago = getString(R.string.pago_cheque_postfechado);

            ((TextView) dialogFormasPago.findViewById(R.id.txtLblNroDoc)).setText(getString(R.string.nro_cheque));

            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblTomarFoto))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblGaleria))).setVisibility(TableLayout.VISIBLE);

            if (formaPago != null) {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText(formaPago.nroCheque);

            } else {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText("");
            }

        } else if (tipoPago.equals("Consignacion")) { // Forma Pago Consignacion

            msgFormaPago = getString(R.string.pago_consignacion);

            ((TextView) dialogFormasPago.findViewById(R.id.txtLblNroDoc)).setText(getString(R.string.pago_consignacion));
            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblTomarFoto))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblGaleria))).setVisibility(TableLayout.VISIBLE);

            if (formaPago != null) {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText(formaPago.nroCheque);

            } else {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText("");
            }
        } else if (tipoPago.equals("Transferencia")) { // Forma Pago Transferencia

            msgFormaPago = getString(R.string.pago_consignacion);

            ((TextView) dialogFormasPago.findViewById(R.id.txtLblNroDoc)).setText(getString(R.string.numero_transf));
            (((TableRow) dialogFormasPago.findViewById(R.id.rowNroDocumento))).setVisibility(TableRow.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblFecha))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblBanco))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblPlaza))).setVisibility(TableLayout.GONE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblTomarFoto))).setVisibility(TableLayout.VISIBLE);
            (((TableLayout) dialogFormasPago.findViewById(R.id.tblGaleria))).setVisibility(TableLayout.VISIBLE);

            if (formaPago != null) {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText(formaPago.nroCheque);

            } else {

                ((TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento)).setText("");
            }
        }

        ((TextView) dialogFormasPago.findViewById(R.id.lblFormaPago)).setText(msgFormaPago);
        ((TextView) dialogFormasPago.findViewById(R.id.lblValorRecaudo)).setText(Html.fromHtml(valorARecuadar));

        String diferencia = lblDiferenciaFormasPago.getText().toString().replace(",", "");
        ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).setText(diferencia);


        ((Button) dialogFormasPago.findViewById(R.id.btnTomarFoto)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickTomarFoto();
            }
        });

        ((Button) dialogFormasPago.findViewById(R.id.btnTomarFotoCedula)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });


        ((Button) dialogFormasPago.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {

            public void onClick (View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                String cant = ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).getText().toString();
                float monto = Util.ToFloat(cant);
                monto = Util.round(monto, 2);

                if(!fotoGuardada && FormCarteraFormasPago.opcSel != 1){
                    AlertDialog.Builder builder = new AlertDialog.Builder(dialogFormasPago.getContext());
                    builder.setMessage("Debe ingresar la foto").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface
                            .OnClickListener() {

                        public void onClick (DialogInterface dialog, int id) {

                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                }else if (monto == 0) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(dialogFormasPago.getContext());
                    builder.setMessage("Debe ingresar la cantidad").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface
							.OnClickListener() {

                        public void onClick (DialogInterface dialog, int id) {

                            ((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).requestFocus();
                            dialog.cancel();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    float totalPagos = TotalFormasPago();

                    float aux = monto + totalPagos;
                    aux = Util.round(aux, 2);
                    //
                    if (aux <= totalRecuado) {

                        AgregarFormaPago();

                    } else {
                        Util.MostrarAlertDialog(FormCarteraFormasPago.this, "El valor no puede ser superior al valor a recaudar " + Util
								.SepararMiles("" + totalRecuado));
                        //								AgregarFormaPago();

                    }
                }
            }
        });

        ((Button) dialogFormasPago.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {

            public void onClick (View v) {

                dialogFormasPago.cancel();
                Main.fotoActual = null;
                resetChecks();
            }
        });

        ((ImageButton) dialogFormasPago.findViewById(R.id.btnCalendar)).setOnClickListener(new OnClickListener() {

            public void onClick (View v) {

                /*retardo de 2000ms para evitar eventos de doble click.
                 * esto significa que solo se puede hacer click cada 2000ms.
                 * es decir despues de presionar el boton, se debe esperar que transcurran
                 * 1500ms para que se capture un nuevo evento.*/
                if (SystemClock.elapsedRealtime() - mLastClickTime < 2000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();//inicializa esta variable en 0


                DatePickerDialog.OnDateSetListener datePickerDialogListener = new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet (DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        MostrarFecha(year, monthOfYear + 1, dayOfMonth);
                    }
                };

                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(FormCarteraFormasPago.this, datePickerDialogListener, year, month, day);
                datePickerDialog.show();
            }
        });

        ((Button) dialogFormasPago.findViewById(R.id.btnBuscarBancos)).setOnClickListener(new OnClickListener() {

            public void onClick (View v) {

                mostrarDialogBusquedaDeBancos();
            }
        });

        ((EditText) dialogFormasPago.findViewById(R.id.txtFecha)).setText("");
        Spinner spcbBancosFormaPago = (Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago);

        if (spcbBancosFormaPago.getChildCount() > 0) {

            ArrayAdapter<String> adapterAux = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});

            spcbBancosFormaPago.setAdapter(adapterAux);

        }

        dialogFormasPago.setCancelable(false);
        //dialogFormasPago.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,R.drawable.op_agregar);
        dialogFormasPago.show();

        return true;
    }



    public void MostrarFecha (int anio, int mes, int dia) {

        StringBuilder sb = new StringBuilder();

        sb.append(anio);
        sb.append("-");
        sb.append(mes < 10 ? "0" + mes : mes);
        sb.append("-");
        sb.append(dia < 10 ? "0" + dia : dia);

        ((TextView) dialogFormasPago.findViewById(R.id.txtFecha)).setText(sb);
    }



    public void initDialog () {

//        Util.aplicarFuente(this, dialogFormasPago.findViewById(R.id.content), Const.BUTTON);
        final EditText txtFechaEntrega = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);

        txtFechaEntrega.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        txtFechaEntrega.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        txtFechaEntrega.setHint("Fecha Documento");

        txtFechaEntrega.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged (Editable e) {

            }



            @Override
            public void beforeTextChanged (CharSequence s, int start, int before, int count) {

            }



            @Override
            public void onTextChanged (CharSequence s, int start, int before, int count) {

                if (s.length() == 0) {

                    txtFechaEntrega.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

                } else {

                    txtFechaEntrega.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                }
            }
        });
    }



    public void cargarBancos () {

        Vector<String> listaItems = new Vector<String>();
        listaBancos = DataBaseBO.ListaBancos(listaItems);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);

            Spinner cbBancosFormaPago = (Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            cbBancosFormaPago.setAdapter(adapter);
        }
    }



    public void cargarPlazas () {

        String[] items = new String[]{getString(R.string.local), getString(R.string.otra)};
        Spinner cbPlazaFormasPago = (Spinner) dialogFormasPago.findViewById(R.id.cbPlazaFormasPago);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbPlazaFormasPago.setAdapter(adapter);
    }



    public void guardarRecaudo () {

        Intent intent = new Intent(this, FormTerminarRecaudoActivity.class);

        String nroRecibo = null;
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            if (bundle.containsKey("nroRecibo"))
                nroRecibo = String.valueOf(bundle.get("nroRecibo"));
        }

        intent.putExtra("formaPago", opcSel);
        intent.putExtra("nroRecibo", nroRecibo);
        intent.putExtra("nrodoc", nroDoc);
        startActivityForResult(intent, Const.RESP_RECAUDO_EXITOSO);
        //		startActivity(intent);
        //		FormCarteraFormasPago.this.finish();

    }



    public void AgregarFormaPago () {

        EditText txtValorFormaPago = (EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago);
        float valor = Util.ToFloat(txtValorFormaPago.getText().toString());

        if (valor > 0) {

            int codigoPago = 0;
            String codigoBanco = "0";
            String nroDocumento = ""; //Este valor aplica para Cheque, Cheque Posfechado y Consigancion

            int position = opcSel;
            String descFormaPago = "";

            if (FormCarteraFormasPago.opcSel == 1) {
                descFormaPago = "Efectivo";
                position = 0;
            }

            if (FormCarteraFormasPago.opcSel == 2) {
                descFormaPago = "Cheque";
                position = 1;
            }

            if (FormCarteraFormasPago.opcSel == 4) {
                descFormaPago = "Consignacion";
                position = 2;
            }

            if (FormCarteraFormasPago.opcSel == 3) {
                descFormaPago = getString(R.string.cheque_postfechado);
                position = 3;
            }

            if(FormCarteraFormasPago.opcSel == 5){
                descFormaPago = getString(R.string.transferencia);
                position = 4;
            }


            int indexBanco;
            EditText txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
            TextView txtNroDocumento = null;
            Spinner cbPlazaFormasPago = null;

            switch (position) {

                case 0: //Efectivo
                    codigoPago = 1;
                    codigoBanco = "-1";
                    break;

                case 1: //Cheque
                    codigoPago = 4;
                    //					txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
                    //					cbPlazaFormasPago = ((Spinner)findViewById(R.id.cbPlazaFormasPago));
                    indexBanco = ((Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
                    codigoBanco = "0";
                    codigoBanco += listaBancos.elementAt(indexBanco).codigo;
                    break;

                case 2: //Consignacion
                    codigoPago = 3;
                    //					txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
                    indexBanco = ((Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
                    codigoBanco = "0";
                    codigoBanco += listaBancos.elementAt(indexBanco).codigo;
                    break;

                case 3: //Cheque Posfechado
                    codigoPago = 5;
                    //					txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
                    cbPlazaFormasPago = ((Spinner) findViewById(R.id.cbPlazaFormasPago));
                    indexBanco = ((Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
                    codigoBanco = "0";
                    codigoBanco += listaBancos.elementAt(indexBanco).codigo;
                    break;

                case 4: //Trasferencia
                    codigoPago = 2;
                    //					txtFecha = (EditText) dialogFormasPago.findViewById(R.id.txtFecha);
                    //cbPlazaFormasPago = ((Spinner) findViewById(R.id.cbPlazaFormasPago));
                    indexBanco = ((Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago)).getSelectedItemPosition();
                    codigoBanco = "0";
                    codigoBanco += listaBancos.elementAt(indexBanco).codigo;
                    break;


            }

            if (position == 1 || position == 3 || position == 4) { //Si es Cheque o Cheque Posfechado o Trasferemcia

                txtNroDocumento = (TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento);
                nroDocumento = txtNroDocumento.getText().toString().trim();

                if (nroDocumento.equals("")) {

                    Util.MostrarAlertDialog(this, getString(R.string.por_favor_ingrese_el_numero_de_cheque));
                    txtNroDocumento.requestFocus();
                    return;
                }

                if (txtFecha != null) {

                    String fechaPago = txtFecha.getText().toString();

                    if (fechaPago.equals("")) {

                        Util.MostrarAlertDialog(this, getString(R.string.por_favor_ingrese_la_fecha_));
                        return;
                    }

                    /**
                     * Si dias = 0. Indica que CURRENT_DATE = fechaPago
                     * Si dias < 0. Indica que CURRENT_DATE < fechaPago (Fecha Acutal menor que la Fecha de Pago)
                     * Si dias > 0. Indica que CURRENT_DATE > fechaPago (Fecha Actual mayor que la Fecha de Pago)
                     **/
                    //					int dias = DataBaseBO.DiferenciaDias(fechaPago);
                    long dias = diferenciaEnDias(fechaPago);

                    System.out.println("Dias: " + dias);

                    if (position == 1 && (dias) < 0) {

                        Util.MostrarAlertDialog(this, getString(R.string.txt_fecha_mayor_forma_pago));
                        return;

                    } else if (position == 1 && (dias) > 0) {

                        Util.MostrarAlertDialog(this, getString(R.string.txt_fecha_menor_forma_pag));
                        return;

                    } else if (position == 3 && (dias) >= 0) {

                        Util.MostrarAlertDialog(this, getString(R.string.txt_fecha_mayor_forma_pago_poscheque));
                        return;
                    }

                } else {

                    Util.MostrarAlertDialog(this, getString(R.string.no_se_pudo_leer_la_fecha_));
                    return;
                }

            } else if (position == 2) { //Consignacion

                txtNroDocumento = (TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento);
                nroDocumento = txtNroDocumento.getText().toString().trim();

                if (nroDocumento.equals("")) {

                    Util.MostrarAlertDialog(this, getString(R.string.por_favor_ingrese_el_numero_de_la_consignacion));
                    txtNroDocumento.requestFocus();
                    return;
                }


                if (codigoBanco.equals("-1")) {

                    Util.MostrarAlertDialog(this, getString(R.string.sin_seleccion_banco_consignacion));
                    //txtNroDocumento.requestFocus();
                    return;
                }


                if (txtFecha != null) {

                    String fechaPago = txtFecha.getText().toString();

                    if (fechaPago.equals("")) {

                        Util.MostrarAlertDialog(this, getString(R.string.por_favor_ingrese_la_fecha_));
                        return;
                    }

                    /**
                     * Si dias = 0. Indica que CURRENT_DATE = fechaPago
                     * Si dias < 0. Indica que CURRENT_DATE < fechaPago (Fecha Acutal menor que la Fecha de Pago)
                     * Si dias > 0. Indica que CURRENT_DATE > fechaPago (Fecha Actual mayor que la Fecha de Pago)
                     **/
                    //					int dias = DataBaseBO.DiferenciaDias(fechaPago);
                    long dias = diferenciaEnDias(fechaPago);
                    if ((dias) < 0) {

                        Util.MostrarAlertDialog(this, "Fecha NO valida para forma de pago 'Consignacion'.\nDebe ser una fecha menor o igual a la de" +
								" hoy");
                        return;
                    }

                } else {

                    Util.MostrarAlertDialog(this, getString(R.string.no_se_pudo_leer_la_fecha_));
                    return;
                }
            } else {
                System.out.println("EFECTIVOOOOOOOOOO");
                txtNroDocumento = (TextView) dialogFormasPago.findViewById(R.id.txtNroDocumento);
                nroDocumento = "1";
            }



            FormaPago formaPago = new FormaPago();
            formaPago.monto = valor;
            formaPago.codigo = codigoPago;
            formaPago.nro_cheque_cons = nroDocumento;
            formaPago.fechaPago = txtFecha != null ? txtFecha.getText().toString() : "";
            formaPago.codigoBanco = position != 0 ? codigoBanco : "-1";
            formaPago.descripcion = descFormaPago;
            formaPago.nroDoc = nroDoc;
            formaPago.serial = "A" + Util.ObtenerFechaId();
            formaPago.idFoto = idFoto;

            if (cbPlazaFormasPago != null) {

                String plaza = cbPlazaFormasPago.getSelectedItem().toString();

                if (plaza.equals(R.string.local))
                    formaPago.plaza = "L";
                else if (plaza.equals(R.string.otra))
                    formaPago.plaza = "O";
            } else {
                formaPago.plaza = "";
            }
			
			/* verificar que la forma de pago no quede repetida. */
            boolean existeFormaPago = false;

            if (formaPago.codigo == 1)
                formaPago.fechaPago = Util.FechaActual("yyyy-MM-dd HH:mm:ss");

            //			for (Cartera cartera : Main.cartera) {
            //
            //				if(cartera != null){
            //					formaPago.nroDoc = cartera.documento;
            //				}
            //			}

            for (FormaPago f : Main.listaFormasPago) {
                if (f.codigo == formaPago.codigo && f.codigoBanco.equals(formaPago.codigoBanco) && f.nro_cheque_cons.equals(formaPago
						.nro_cheque_cons)) {
                    existeFormaPago = true;
                    break;
                }
            }

            if (!existeFormaPago) {
                Main.listaFormasPago.addElement(formaPago);
            } else {
                Util.MostrarAlertDialog(this, getString(R.string.forma_pago_existe_));
                return;
            }

            dialogFormasPago.cancel();

            Util.guardarDatosDeRecaudo();
            refrescarDatosRecaudo();
            CargarTablaFormasPago();

            //			Main.listaFormasPago.addElement(formaPago);
            //			Util.guardarDatosDeRecaudo();
            //			refrescarDatosRecaudo();
            //			dialogFormasPago.cancel();
            //			Main.formaPagoAct = formaPago;

            //			boolean agrego = DataBaseBO.AgregarFormaPago(Main.formaPagoAct);
            //
            //			if (agrego) {
            //
            //				CargarTablaFormasPago();
            //				dialogFormasPago.cancel();
            //
            //			} else {
            //
            //				Util.MostrarAlertDialog(this, "No se pudo guardar la Forma de Pago");
            //			}
            //


            resetChecks();
            //			((TableRow) dialogFormasPago.findViewById(R.id.tableRowConfirmarBanco)).setVisibility(View.GONE);

            //setResult(RESULT_OK);
            //finish();

        } else {

            Util.MostrarAlertDialog(this, getString(R.string.ingrese_valor_recaudo));
            txtValorFormaPago.requestFocus();
            return;
        }
    }



    public static long diferenciaEnDias (String fechaIngresada) {

        long diff = 0;

        try {

            String[] datos = Util.split(fechaIngresada, "-");

            Calendar thatDay = Calendar.getInstance();
            thatDay.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datos[2]));
            thatDay.set(Calendar.MONTH, Integer.parseInt(datos[1]) - 1);
            thatDay.set(Calendar.YEAR, Integer.parseInt(datos[0]));

            Calendar today = Calendar.getInstance();

            diff = today.getTimeInMillis() - thatDay.getTimeInMillis(); //result in millis

            diff = diff / (24 * 60 * 60 * 1000);

        } catch (Exception e) {

        }

        return diff;

    }



    public void OnClikTerminarFormaPago (View view) {

        Date date = new Date();

        long tiempoClick2 = date.getTime();

        long dif1 = tiempoClick2 - tiempoClick1;

        if (dif1 < 0) {

            dif1 = dif1 * (-1);

        }

        if (dif1 > 2000) {

            tiempoClick1 = tiempoClick2;

            // verificar que el monto recaudado sea igual o superior al monto
            // que se debe recaudar.
            float totalPagos = TotalFormasPago();

            if (totalPagos < totalRecuado) {

                AlertDialog.Builder builder = new AlertDialog.Builder(FormCarteraFormasPago.this);
                builder.setMessage(getString(R.string.la_sumatoria_de_las_formas_de_pago_es_inferior_al_valor_a_recaudar_) + totalRecuado)
						.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick (DialogInterface dialog, int id) {

                        //						((EditText) dialogFormasPago.findViewById(R.id.txtValorFormaPago)).requestFocus();
                        dialog.cancel();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
                return;
            }

            if (esRecaudoValido()) {
				/*
				 * valiar las carteras que deben ser pagadas con consignacion por
				 * aplicar prontopago vencido
				 */
                boolean aceptado = validarCarterasPagoConsignacionProntoPago();

                if (aceptado) {
					/* decidir si el pago es aceptado o no */
                    guardarFormasPagoTablaTemporal();
                    terminarRecaudo();
                } else {
                    String mensaje = getString(R.string.txt_factura_consignacion_large);
                    Util.MostrarAlertDialog(FormCarteraFormasPago.this, mensaje);
                    return;
                }

            } else {

                Util.MostrarAlertDialog(FormCarteraFormasPago.this, getString(R.string.txt_verificar_recaudo_large), 1);

            }
        }

    }



    public void OnClickPagoEfectivo (View view) {

        FormCarteraFormasPago.opcSel = 1;

        mostrarDialogFormasPago(null);

    }



    public void OnClickPagoCheque (View view) {

        FormCarteraFormasPago.opcSel = 2;

        mostrarDialogFormasPago(null);

    }



    public void OnClickPagoChequePostFechado (View view) {

        FormCarteraFormasPago.opcSel = 3;

        mostrarDialogFormasPago(null);

    }



    public void OnClickPagoConsignacion (View view) {

        FormCarteraFormasPago.opcSel = 4;

        mostrarDialogFormasPago(null);
    }

    public void OnClickTrasferencia(View view){
        FormCarteraFormasPago.opcSel = 5;

        mostrarDialogFormasPago(null);
    }


    public void resetChecks () {

        ((RadioButton) findViewById(R.id.radioEfectivo)).setChecked(false);
        ((RadioButton) findViewById(R.id.radioCheque)).setChecked(false);
        ((RadioButton) findViewById(R.id.radioChequePostFechado)).setChecked(false);
        ((RadioButton) findViewById(R.id.radioConsignacion)).setChecked(false);
        ((RadioButton) findViewById(R.id.radioTransferencia)).setChecked(false);

    }



    public boolean esRecaudoValido () {

        boolean esValido = false;
        boolean existeFormaPagoConsignacion = false;
        boolean obilgatorioPagoConsignacion = false;

        for (FormaPago formaPago : Main.listaFormasPago) {

            if (formaPago.codigo == 3) {

                existeFormaPagoConsignacion = true;

            }

        }

        for (Cartera cartera : Main.cartera) {

            if (cartera.conConsignacion) {

                obilgatorioPagoConsignacion = true;

            }

        }

        if (obilgatorioPagoConsignacion && !existeFormaPagoConsignacion) {

            return false;

        } else {

            return true;

        }

    }



    protected void onActivityResult (int requestCode, int resultCode, Intent data) {

        if (requestCode == Const.RESP_TOMAR_FOTO && resultCode == RESULT_OK) {

            Main.fotoActual = ResizedImage(anchoImg, altoImg);

            if (Main.fotoActual != null) {

                Main.guardarFoto = true;
                ImageView imgFoto = (ImageView) dialogFormasPago.findViewById(R.id.imageFotoPagos);
                imgFoto.setImageDrawable(Main.fotoActual);
                OnClickGuardarFoto();
            }

        }

        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            InputStream is;
            try {
                is = getContentResolver().openInputStream(imageUri);
                BufferedInputStream bis = new BufferedInputStream(is);
                Bitmap bitmap = BitmapFactory.decodeStream(bis);
                ImageView iv = (ImageView) dialogFormasPago.findViewById(R.id.imageFotoPagos);
                iv.setImageBitmap(bitmap);
                Main.guardarFoto = true;
                OnClickGuardarFotoGaleria(bitmap);
            } catch (FileNotFoundException e)
            {

            }

        }

        if (requestCode == Const.RESP_RECAUDO_EXITOSO && resultCode == RESULT_OK) {

            setResult(RESULT_OK);
            finish();

        }
    }



    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }



    public void cargarBancos2 (String busqueda) {

        Vector<String> listaItems = new Vector<String>();
        listaBancos = DataBaseBO.ListaBancos2(listaItems, busqueda);

        if (listaItems.size() > 0) {

            String[] items = new String[listaItems.size()];
            listaItems.copyInto(items);

            Spinner cbBancosFormaPago = (Spinner) dialogFormasPago.findViewById(R.id.cbBancosFormaPago);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            cbBancosFormaPago.setAdapter(adapter);
        }
    }



    public boolean mostrarDialogBusquedaDeBancos () {

        if (dialogBusquedaDeBancos == null) {

            dialogBusquedaDeBancos = new Dialog(this);
            dialogBusquedaDeBancos.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialogBusquedaDeBancos.setContentView(R.layout.dialog_buscar_banco);
            dialogBusquedaDeBancos.setTitle("Buscar Bancos");

        }

        ((Button) dialogBusquedaDeBancos.findViewById(R.id.btnAceptarBuscarBanco)).setOnClickListener(new OnClickListener() {

            public void onClick (View v) {

                EditText txtbusq = (EditText) dialogBusquedaDeBancos.findViewById(R.id.txtBusqBancos);

                if (txtbusq.getText().toString().length() >= 4) {

                    cargarBancos2(txtbusq.getText().toString());
                    dialogBusquedaDeBancos.cancel();

                } else {

                    Util.MostrarAlertDialog(FormCarteraFormasPago.this, getString(R.string.busqueda_ultimos_4_digitos));

                }
            }
        });

        ((Button) dialogBusquedaDeBancos.findViewById(R.id.btnCancelarBuscarBanco)).setOnClickListener(new OnClickListener() {

            public void onClick (View v) {

                dialogBusquedaDeBancos.cancel();

            }
        });

        ((EditText) dialogBusquedaDeBancos.findViewById(R.id.txtBusqBancos)).setText("");

        dialogBusquedaDeBancos.setCancelable(false);
        dialogBusquedaDeBancos.show();
        dialogBusquedaDeBancos.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.busqueda);

        return true;
    }



    @Override
    protected void onResume () {

        super.onResume();

        refrescarDatosRecaudo();
    }



    public void refrescarDatosRecaudo () {

        if (Main.usuario == null || Main.usuario.codigoVendedor == null || Main.usuario.bodega == null) {
            DataBaseBO.CargarInfomacionUsuario();
        }

        if (Main.cliente == null || Main.cliente.codigo == null) {

            int tipoDeClienteSelec = DataBaseBO.ObtenerTipoClienteSeleccionado();
            Cliente clienteSel;

            if (tipoDeClienteSelec == 1) {

                clienteSel = DataBaseBO.CargarClienteSeleccionado();

            } else {
                clienteSel = DataBaseBO.CargarClienteNuevoSeleccionado();
            }

            if (clienteSel != null)
                Main.cliente = clienteSel;
        }

        objeto obj = DataBaseBO.obtenerObjeto();

        if (obj != null) {

            Main.cartera = obj.cartera;
            Main.listaFormasPago = obj.listaFormasPago;
            Main.total_forma_pago = obj.total_forma_pago;
            Main.total_descuento = obj.total_descuento;
            Main.total_recaudo = obj.total_recaudo;

        }

        CargarTablaFormasPago();

    }



    /**
     * Metodo que inserta las formas de pago con que el cliente realiza un
     * recaudo.
     */
    private void guardarFormasPagoTablaTemporal () {

		/* generar un numero de documento temporal */
        String numeroDocTemporal = Util.FechaActual("yyyyMMddHHmmssSSS");

		/* llamado al metodo para insertar en la base de datos. */
        DataBaseBO.guardarFormaPagoTablaTemporal(nroDoc);
    }



    /**
     * Validar las carteras que han sido seleccionadas para pago con
     * consignacion por pronto pago vencido.
     * @return true, si el pago es con consignacion es correcto, false en caso
     * contrario.
     * <p>
     * Este metodo debe ser modificado si se define que se debe validar
     * por montos de consignacion y fechas de pagos.
     */
    private boolean validarCarterasPagoConsignacionProntoPago () {

        boolean validado = false;
        CarteraConsignacion carteraConsig = new CarteraConsignacion();

		/* cargar la cartera con el vencimiento mas antiguo */
        DataBaseBO.cargarCarteraConMaxProntoPagoVencido(carteraConsig);

        if (carteraConsig.diasVencidoProntoPago > 0) {

            // elegir la forma de pago con fecha mas antigua que alcance a
            // cubrir diasVencidoProntoPago
            for (FormaPago fp : Main.listaFormasPago) {
                // codigo = 3 son las consignaciones.
                if (fp.codigo == 3) {
                    long diasConsig = diferenciaDeDias(fp.fechaPago);
                    if (diasConsig >= carteraConsig.diasVencidoProntoPago) {
                        validado = true;
                        break;
                    }
                }
            }
        } else {
            validado = true; // no hay carteras para verificar se retorna true.
        }
        return validado;
    }



    /**
     * Calcular la diferencia de dias que hay entre la fecha del pago de la
     * consignacion y la fecha actual.
     * @param fechaPago
     *
     * @return
     */
    private long diferenciaDeDias (String fechaPago) {

        long dif_dias = 0;
        try {
            String formatoFechas = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(formatoFechas, Locale.getDefault());
            Date dateFechaPago = sdf.parse(fechaPago);
            Date dateFechaActual = sdf.parse(Util.FechaActual(formatoFechas));
            dif_dias = (dateFechaActual.getTime() - dateFechaPago.getTime()) / (1000 * 3600 * 24);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dif_dias;
    }

    public void OnClickTomarFoto() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String filePath = Util.DirApp().getPath() + "/foto.jpg";
        Uri output = Uri.fromFile(new File(filePath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, output);

        startActivityForResult(intent, Const.RESP_TOMAR_FOTO);

    }

    public void SetPhotoDefault() {

        Drawable fotoVacia = getResources().getDrawable(R.drawable.foto_vacia);
        Drawable img = Util.ResizedImage(fotoVacia, anchoImg, altoImg);

        if (img != null )
            ((ImageView) dialogFormasPago.findViewById(R.id.imageFotoPagos)).setImageDrawable(img);

    }

    public Drawable ResizedImage(int newWidth, int newHeight) {

        Matrix matrix;
        FileInputStream fd = null;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;

        try {


            File fileImg = new File(Util.DirApp(), "foto.jpg");

            if (fileImg.exists()) {

                fd = new FileInputStream(fileImg.getPath());
                bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());

                int width = bitmapOriginal.getWidth();
                int height = bitmapOriginal.getHeight();

                if (width == newWidth && height == newHeight) {

                    return new BitmapDrawable(bitmapOriginal);
                }

                // Reescala el Ancho y el Alto de la Imagen
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                // Crea la Imagen con el nuevo Tamano
                resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);

                return new BitmapDrawable(resizedBitmap);
            }

            return null;

        } catch(Exception e) {

            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
            return null;

        } finally {

            if (fd != null) {

                try {

                    fd.close();

                } catch (IOException e) { }
            }

            System.gc();
        }
    }

    public Drawable ResizedImageGalery(int newWidth, int newHeight, File direccion, String nombre) {

        Matrix matrix;
        FileInputStream fd = null;
        Bitmap resizedBitmap = null;
        Bitmap bitmapOriginal = null;

        try {

            File fileImg = new File(direccion, "/" + nombre);

            if (fileImg.exists()) {

                fd = new FileInputStream(fileImg.getPath());
                bitmapOriginal = BitmapFactory.decodeFileDescriptor(fd.getFD());

                int width = bitmapOriginal.getWidth();
                int height = bitmapOriginal.getHeight();

                if (width == newWidth && height == newHeight) {

                    return new BitmapDrawable(bitmapOriginal);
                }

                // Reescala el Ancho y el Alto de la Imagen
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                // Crea la Imagen con el nuevo Tamano
                resizedBitmap = Bitmap.createBitmap(bitmapOriginal, 0, 0, width, height, matrix, true);

                return new BitmapDrawable(resizedBitmap);
            }

            return null;

        } catch(Exception e) {

            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
            return null;

        } finally {

            if (fd != null) {

                try {

                    fd.close();

                } catch (IOException e) { }
            }

            System.gc();
        }
    }

    public void OnClickGuardarFoto() {



        if (!estaGuardando) {

            if (Main.guardarFoto) {


                String mensaje;
                Drawable imgFoto = ResizedImage(320, 480);

                if (imgFoto != null) {

                    Bitmap bitmap = ((BitmapDrawable)imgFoto).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byte[] byteArray = stream.toByteArray();

                    if (byteArray != null && byteArray.length > 0) {

                        Foto foto = new Foto();
                        foto.id          = Util.ObtenerFechaId();
                        idFoto  = foto.id;
                        foto.codCliente  = Main.cliente.codigo;
                        foto.codVendedor = Main.usuario.codigoVendedor;
                        foto.modulo      = 6;
                        foto.nroDoc      = nroDoc;

                        if (DataBaseBO.GuardarImagen(foto, byteArray)) {

                            Main.guardarFoto = false;
                            imgFoto = ResizedImage(anchoImg, altoImg);

                            if (imgFoto != null) {

                                Drawable imgGaleria = ResizedImage(30, 30);
                                Main.fotosGaleria.addElement(imgGaleria);
                                Main.listaInfoFotos.addElement(foto);

                                mensaje = "Foto Guardada con Exito";
                                fotoGuardada = true;
                                Main.fotoActual = null;
                                System.gc();

                            } else {

                                mensaje = "Foto Guardada con Exito, Error Visualizando la Img.";
                            }

                        } else {

                            mensaje = "Error guardando la Imagen: " + DataBaseBO.mensaje;
                        }

                    } else {

                        mensaje = "Error procesando la Imagen 2";
                    }

                } else {

                    mensaje = "Error procesando la Imagen 1";
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        //((ImageAdapter)(((Gallery) findViewById(R.id.galleryFotos))).getAdapter()).notifyDataSetChanged();

                    }
                });

                AlertDialog alert = builder.create();
                alert.setMessage(mensaje);
                alert.show();

            } else {

                MostrarAlertDialog("Debe capturar la imagen antes de guardarla");
            }

            estaGuardando = true;
        }
    }

    public void MostrarAlertDialog(String mensaje) {

        if (alertDialog == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                }
            });

            alertDialog = builder.create();
        }

        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    private void openGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    public void OnClickGuardarFotoGaleria(Bitmap bitmap) {

        if (!estaGuardando) {

            if (Main.guardarFoto) {

                String mensaje;
                //Drawable imgFoto = ResizedImageGalery(320, 480, fd, nombre);

                //se redimensiona la imagen adjunta
                Bitmap bitmapNew = Util.getResizedBitmap(bitmap, 480, 320);

                if (bitmapNew != null) {


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byte[] byteArray = stream.toByteArray();

                    if (byteArray != null && byteArray.length > 0) {

                        Foto foto = new Foto();
                        foto.id          = Util.ObtenerFechaId();
                        idFoto  = foto.id;
                        foto.codCliente  = Main.cliente.codigo;
                        foto.codVendedor = Main.usuario.codigoVendedor;
                        foto.modulo      = 6;
                        foto.nroDoc      = nroDoc;

                        if (DataBaseBO.GuardarImagen(foto, byteArray)) {

                            Main.guardarFoto = false;
                            bitmapNew = Util.getResizedBitmap(bitmap,altoImg, anchoImg);
                            //imgFoto = ResizedImage(anchoImg, altoImg);

                            if (bitmapNew != null) {

                                Bitmap bitmapAux = Util.getResizedBitmap(bitmap,30, 30);
                                Drawable d = new BitmapDrawable(getResources(), bitmapAux);
                                Drawable imgGaleria = d;
                                Main.fotosGaleria.addElement(imgGaleria);
                                Main.listaInfoFotos.addElement(foto);

                                mensaje = "Foto Guardada con Exito";
                                fotoGuardada = true;
                                Main.fotoActual = null;
                                System.gc();

                            } else {

                                mensaje = "Foto Guardada con Exito, Error Visualizando la Img.";
                            }

                        } else {

                            mensaje = "Error guardando la Imagen: " + DataBaseBO.mensaje;
                        }

                    } else {

                        mensaje = "Error procesando la Imagen 2";
                    }

                } else {

                    mensaje = "Error procesando la Imagen 1";
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        //((ImageAdapter)(((Gallery) findViewById(R.id.galleryFotos))).getAdapter()).notifyDataSetChanged();

                    }
                });

                AlertDialog alert = builder.create();
                alert.setMessage(mensaje);
                alert.show();

            } else {

                MostrarAlertDialog("Debe capturar la imagen antes de guardarla");
            }

            estaGuardando = true;
        }
    }

}
