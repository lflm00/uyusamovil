package celuweb.com.uyusa;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.HistorialRecaudoCliente;

public class FormCarteraInfoHistorialRecaudo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_cartera_info_historial_recaudo);
        cargarDatos();
    }

    public void cargarDatos(){
        TextView lblDocumentoHR = findViewById(R.id.lblDocumentoHR);
        TextView lblFechaHR = findViewById(R.id.lblFechaHR);
        TextView lblTipoPagoHR = findViewById(R.id.lblTipoPagoHR);
        TextView lblValorHR = findViewById(R.id.lblValorHR);
        TextView lblMontoHR = findViewById(R.id.lblMontoHR);

        String doc_recaudo = getIntent().getExtras().getString("DOC_RECAUDO");
        HistorialRecaudoCliente recaudo = DataBaseBO.obtenerRecaudoCliente(Main.cliente.codigo, doc_recaudo);

        if(recaudo != null){
            lblDocumentoHR.setText(recaudo.documento);
            lblMontoHR.setText(recaudo.monto.toString());
            lblTipoPagoHR.setText(recaudo.tipo_pago);
            lblValorHR.setText(recaudo.valor.toString());
            lblFechaHR.setText(recaudo.fecha_recaudo);
        }
    }

    public void OnClikCancelarInfoHistorialRecaudo (View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onBackPressed() {
    }
}
