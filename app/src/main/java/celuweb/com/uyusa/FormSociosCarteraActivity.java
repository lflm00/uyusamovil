package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cliente;

public class FormSociosCarteraActivity extends Activity implements OnClickListener {


    CheckBox chq;
    double total = 0.0;
    Vector<Cliente> listaClientes;

    /**
     * @see android.app.Activity#onCreate(Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO Put your code here

        setContentView(R.layout.form_clientes_cartera);

        chq = (CheckBox) findViewById(R.id.chqOpCarteraVencida);
        chq.setChecked(true);

        chq.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    cargarSociosConCartera(false);

                } else {

                    cargarSociosConCartera(true);


                }

            }
        });


        cargarSociosConCartera(false);


    }


    public void cargarSociosConCartera(boolean opcion) {


        if (!Main.pdialog.isShowing()) {


            Main.pdialog = ProgressDialog.show(FormSociosCarteraActivity.this,
                    "", "Cargando Informacion....", true);

            Main.pdialog.show();


        }
        total = 0.0;

        listaClientes = DataBaseBO.listadoClientesConCartera(opcion);

        TableLayout table = new TableLayout(this);
        table.setBackgroundColor(Color.WHITE);

        String[] cabecera = {"Codigo", "Nombre", "Saldo", "Saldo Vencido", "Documentos"};
        Util.Headers(table, cabecera, this);

        HorizontalScrollView scroll = (HorizontalScrollView) findViewById(R.id.scrollListadoSociosCartera);
        scroll.removeAllViews();
        scroll.addView(table);

        Cliente cliente;

        for (int i = 0; i < listaClientes.size(); i++) {


            cliente = listaClientes.elementAt(i);

            TextView textViewAux;
            TableRow fila = new TableRow(this);


//				ImageView imgAux = new ImageView(this);
//				imgAux.setImageResource(R.drawable.terminar);
//				imgAux.setOnClickListener(this);
//				imgAux.setTag(id);
//				imgAux.setAdjustViewBounds(true);

//				fila.addView(imgAux);


            textViewAux = new TextView(this);
            textViewAux.setText(cliente.codigo + "\n");
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            //textViewAux.setTextSize(19);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);

            textViewAux = new TextView(this);
            textViewAux.setText(cliente.Nombre + "\n");
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            //textViewAux.setTextSize(19);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);

            textViewAux = new TextView(this);
            textViewAux.setText(Util.SepararMiles(Util.getDecimalFormatCartera(cliente.saldo)) + "\n");
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            //textViewAux.setTextSize(19);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);

            total = total + cliente.saldo;

            textViewAux = new TextView(this);
            textViewAux.setText(Util.SepararMiles(Util.getDecimalFormatCartera(cliente.saldoVencido)) + "\n");
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            //textViewAux.setTextSize(19);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);

            textViewAux = new TextView(this);
            textViewAux.setText("" + cliente.documentos + "\n");
            textViewAux.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            textViewAux.setTextColor(Color.argb(255, 0, 0, 0));
            //textViewAux.setTextSize(19);
            //textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
            textViewAux.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.table_cell_row_1));
            fila.addView(textViewAux);


            table.addView(fila);


        }

        ((EditText) findViewById(R.id.totalSaldoListadoClientes)).setText(Util.SepararMiles(Util.getDecimalFormatCartera(total)));


        if (Main.pdialog != null)
            if (Main.pdialog.isShowing())
                Main.pdialog.cancel();


    }


    public void onClick(View view) {


        Object tag = view.getTag();
        int pos = Util.ToInt(tag.toString());
        Cliente cliente = listaClientes.elementAt(pos);
        cliente = DataBaseBO.BuscarCliente(cliente.codigo);
        Main.cliente = cliente;
        DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
        Intent intent = new Intent(this, FormCarteraInformacion.class);
        startActivity(intent);

    }

    public void onClickRegresar(View view) {
        finish();
    }


}
