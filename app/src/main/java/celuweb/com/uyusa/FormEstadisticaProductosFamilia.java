package celuweb.com.uyusa;

import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBOJ;
import celuweb.com.DataObject.Encabezado;
import celuweb.com.DataObject.Filtro;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Producto;

@SuppressLint("NewApi")
public class FormEstadisticaProductosFamilia extends CustomActivity {

	Dialog dialogResumen;
	Vector<Encabezado> listaPedidos;
	ProgressDialog progressDialog;
	String mensaje;
	int pedido = 1;
	boolean fallo = false;
	
	int positionFamilia;
	int positionClase;
	Vector<Filtro> listaFamilia;
	Vector<Filtro> listaClase;
	Filtro filtroFamilia;
	Filtro filtroClase;
	int posCodProducto;
	boolean primeraVez=true;
	Vector<Producto> listaProductos;
	Producto productoSel;
	ListViewAdapterFiltroPedido adapter;
	int positionProducto;

	public long tiempoClick1 = 0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_estadistica_prod_familia);

		cargarFamilia();
		
	}



private void cargarFamilia() {
		
		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		
		listaFamilia = DataBaseBOJ.listaFiltroFamiliaEstadistica(listaItems );

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

		} else {
			
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinnerFamilia = (Spinner)findViewById(R.id.spListaFamilia);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerFamilia.setAdapter(adapter);

		spinnerFamilia.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
					
				positionFamilia = position;
				
				filtroFamilia =listaFamilia.elementAt(position);
					String where = "";
					
					if(!filtroFamilia.filtroDescripcion.toString().equals("TODOS")){
						where = "WHERE l.Codigo = '"+filtroFamilia.filtroCodigo+"' ";
					}
					//cargaListaClase(where);
					cargarProductos2(where);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	protected void cargaListaClase(final String filtroFamilia) {
		
		
		ArrayAdapter<String> adapter;
		Vector<String> listaItems = new Vector<String>();
		listaClase= DataBaseBOJ.listaFiltroClaseEstadistica(listaItems,filtroFamilia);

		if (listaItems.size() > 0) {

			String[] items = new String[listaItems.size()];
			listaItems.copyInto(items);
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
		} else {

			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{});
		}

		Spinner spinnerClase = (Spinner)findViewById(R.id.spListaClase);        
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerClase.setAdapter(adapter);
		
		
		spinnerClase.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				
//				if(position!=0){	
				
					String filtro = "";
					filtro = listaClase.elementAt(position).filtroDescripcion;
				
				positionClase =position;
				
					filtroClase = listaClase.elementAt(position);
					String where = "";
					if(!filtroClase.filtroDescripcion.toString().equals("TODOS")){
						
						where = " where categoria = '"+filtroClase.filtroDescripcion+"' ";
					}else{
						
						if(!filtroFamilia.toString().equals("TODOS"))
						where = filtroFamilia;
						
					}
					cargarProductos2(where);
//				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
//		if(primeraVez && posCodProducto != -1){
//			
//			if ((posDescClase2!= -1)) {
//				((Spinner)findViewById(R.id.spListaClase)).setSelection(posDescClase2);
//			}
//			
//		}

		
	}
	
	public void cargarProductos2(String filtro){
		ItemListView[] items;
		Vector<ItemListView> listaItems = new Vector<ItemListView>();
		
		String and = "";
		
		listaProductos = DataBaseBOJ.listaProductoEstadisticas(filtro, listaItems, and);

		
		if (listaItems.size() > 0) {

			items = new ItemListView[listaItems.size()];
			listaItems.copyInto(items);

		} else {

			items = new ItemListView[] {};
			
			if (listaProductos != null)
				listaProductos.removeAllElements();

			Toast.makeText(getApplicationContext(), "Busqueda sin resultados", Toast.LENGTH_SHORT).show();
		}
		
		System.out.println("Precarga el producto seleccionado");
		Main.listaProductos = listaProductos;
		
		adapter = new ListViewAdapterFiltroPedido(this, items, R.drawable.compra, 0);
		ListView listaBusquedaProductos = (ListView) findViewById(R.id.listaHistorialPedidos);
		listaBusquedaProductos.setAdapter(adapter);
		
//		if(primeraVez && posCodProducto!= -1){
//			
//			primeraVez=false;
//			listaBusquedaProductos.performItemClick(listaBusquedaProductos.getAdapter().getView(posCodProducto, null, null), 
//					posCodProducto, listaBusquedaProductos.getAdapter().getItemId(posCodProducto));
//			
//			listaBusquedaProductos.getAdapter().getView(posCodProducto, null, null).setSelected(true);
//			listaBusquedaProductos.smoothScrollToPosition(posCodProducto);
//
//			positionProducto =posCodProducto;
//			
//			
//			listaBusquedaProductos.invalidateViews();
//			
//		}
		
		}
	
	public void onClickAceptar(View view) {

		finish();

	}


	
}
