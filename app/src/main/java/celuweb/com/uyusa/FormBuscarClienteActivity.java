package celuweb.com.uyusa;

import java.io.ByteArrayOutputStream;
import java.util.Locale;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.SystemClock;

import androidx.core.app.ActivityCompat;

import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.Conexion.Sync;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Foto;
import celuweb.com.DataObject.HabeasData;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Usuario;

public class FormBuscarClienteActivity extends Activity implements Sincronizador, View.OnClickListener {

    Vector<Cliente> listaClientes;
    private long mLastClickTime = 0;
    int posicionActual = 0;
    ProgressDialog progressDialog;
    Dialog dialogoFormularioTerminos;
    Dialog dialogoTerminos;
    private byte[] firma;
    Bitmap bitmap = null;
    Cliente cliente = null;
    Usuario usuario = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_buscar_cliente);
        DataBaseBO.CargarInfomacionUsuario();
        DataBaseBO.eliminarClientePropio(Main.usuario.codigoVendedor);
        CargarOpcionesBusqueda();
        SetListenerListView();
        Util.closeTecladoStartActivity(this);
    }

    public void OnClickFormBuscarCliente(View view) {
        EditText txtBusquedaCliente = (EditText) findViewById(R.id.txtBusquedaCliente);
        String cadBusqueda = txtBusquedaCliente.getText().toString().trim();
        OcultarTeclado(txtBusquedaCliente);
        if (cadBusqueda.equals("")) {
            Toast.makeText(getApplicationContext(), "Debe ingresar la opcion de Busqueda", Toast.LENGTH_SHORT).show();
            txtBusquedaCliente.requestFocus();

        } else {
            mostrarListaClientes(cadBusqueda);

        }
    }

    private void mostrarListaClientes(String cadBusqueda) {
        String where = "";
        String orderby = "";
        Spinner cbBusquedaCliente = (Spinner) findViewById(R.id.cbBusquedaCliente);
        String opBusqueda = cbBusquedaCliente.getSelectedItem().toString();
        if (opBusqueda.equals(Const.TODOS)) {
            where = "";
            orderby = "";

        } else if (opBusqueda.equals(Const.POR_NOMBRE)) {
            where = "WHERE Nombre LIKE '%" + cadBusqueda + "%'";
            orderby = "order by Nombre";

        } else if (opBusqueda.equals(Const.POR_CODIGO)) {
            where = "WHERE Codigo LIKE '%" + cadBusqueda + "%'";
            orderby = "order by codigo";

        } else if (opBusqueda.equals(Const.POR_RAZON_SOCIAL)) {
            where = "WHERE RazonSocial LIKE '%" + cadBusqueda + "%'";
            orderby = "order by RazonSocial";
        }

        ItemListView[] listaItems = null;
        Vector<ItemListView> listaItemsCliente = new Vector<ItemListView>();
        listaClientes = DataBaseBO.BuscarCliente(where, orderby, listaItemsCliente);
        if (listaItemsCliente.size() > 0) {
            listaItems = new ItemListView[listaItemsCliente.size()];
            listaItemsCliente.copyInto(listaItems);
            ListViewAdapterRemarca adapter = new ListViewAdapterRemarca(this, listaItems, R.drawable.drw_op_cliente, 0x2E65AD);
            ListView listaBusquedaClientes = (ListView) findViewById(R.id.listaBusquedaClientes);
            listaBusquedaClientes.setAdapter(adapter);

        } else {
            ListViewAdapterRemarca adapter = new ListViewAdapterRemarca(FormBuscarClienteActivity.this, new ItemListView[]{}, R.drawable.cliente, 0x2E65AD);
            ListView listaBusquedaClientes = (ListView) findViewById(R.id.listaBusquedaClientes);
            listaBusquedaClientes.setAdapter(adapter);
            if (listaClientes != null) listaClientes.removeAllElements();
            Toast.makeText(getApplicationContext(), "Busqueda sin resultados", Toast.LENGTH_SHORT).show();
        }

    }

    public void CargarOpcionesBusqueda() {
        Spinner cbBusquedaCliente = (Spinner) findViewById(R.id.cbBusquedaCliente);
        String[] items = new String[]{Const.TODOS, Const.POR_CODIGO, Const.POR_NOMBRE, Const.POR_RAZON_SOCIAL};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbBusquedaCliente.setAdapter(adapter);
        cbBusquedaCliente.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String opSel = ((Spinner) findViewById(R.id.cbBusquedaCliente)).getItemAtPosition(position).toString();
                if (opSel.equals(Const.TODOS)) {
                    mostrarListaClientes("");
                    ((LinearLayout) findViewById(R.id.lytBusqueda)).setVisibility(View.GONE);

                } else if (opSel.equals(Const.POR_NOMBRE)) {
                    ((LinearLayout) findViewById(R.id.lytBusqueda)).setVisibility(View.VISIBLE);
                    OpcionSeleccionada("Ingrese parte del nombre", InputType.TYPE_CLASS_TEXT);

                } else if (opSel.equals(Const.POR_CODIGO)) {
                    ((LinearLayout) findViewById(R.id.lytBusqueda)).setVisibility(View.VISIBLE);
                    OpcionSeleccionada("Ingrese parte del codigo", InputType.TYPE_CLASS_NUMBER);

                } else if (opSel.equals(Const.POR_RAZON_SOCIAL)) {
                    ((LinearLayout) findViewById(R.id.lytBusqueda)).setVisibility(View.VISIBLE);
                    OpcionSeleccionada("Ingrese parte de la razon s.", InputType.TYPE_CLASS_TEXT);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }

    public void OpcionSeleccionada(String label, int inputType) {
        //		TextView lblBusquedaCliente = (TextView)findViewById(R.id.lblBusquedaCliente);
        EditText txtBusquedaCliente = (EditText) findViewById(R.id.txtBusquedaCliente);
        txtBusquedaCliente.setText("");
        txtBusquedaCliente.setHint(label);
        //		lblBusquedaCliente.setHint(label);
        txtBusquedaCliente.setInputType(inputType);
        ListViewAdapter adapter = new ListViewAdapter(this, new ItemListView[]{}, R.drawable.cliente, 0x2E65AD);
        ListView listaBusquedaClientes = (ListView) findViewById(R.id.listaBusquedaClientes);
        listaBusquedaClientes.setAdapter(adapter);
        if (listaClientes != null) listaClientes.removeAllElements();
    }

    public void SetListenerListView() {
        ListView listaBusquedaClientes = (ListView) findViewById(R.id.listaBusquedaClientes);
        listaBusquedaClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                /* Evitar evento de doble click */
                if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
                    return;
                }
                posicionActual = position;
                mLastClickTime = SystemClock.elapsedRealtime();
                cliente = listaClientes.elementAt(position);
                if (DataBaseBO.HayInformacionXEnviar1()) {
                    progressDialog = ProgressDialog.show(FormBuscarClienteActivity.this, "", "Enviando Informacion...", true);
                    progressDialog.show();
                    Sync sync = new Sync(FormBuscarClienteActivity.this, Const.ENVIAR_PEDIDO);
                    sync.start();

                } else {
                    CargarInformacionCliente(cliente);
                    //actualizarInfo();
                }
            }
        });
    }

    public void mostrarDialogoFormularioTerminos() {

        if (dialogoFormularioTerminos == null) {
            dialogoFormularioTerminos = new Dialog(this);
            dialogoFormularioTerminos.setContentView(R.layout.dialog_formulario_terminos);
            dialogoFormularioTerminos.setTitle("Formulario Terminos y Condiciones");
            dialogoFormularioTerminos.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //String resumen = DataBaseBO.obtenerTerminosHabeasData().substring(0,30);
            String resumen = DataBaseBO.obtenerTerminosHabeasData();
            WebView myWebViewRDT = (WebView) dialogoFormularioTerminos.findViewById(R.id.webViewResumenDescripcionTerminos);
            myWebViewRDT.loadData(resumen, "text/html; charset=UTF-8", "UTF-8");
        }
        EditText edtxObservaciones = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxObservaciones);
        edtxObservaciones.setText("");
        EditText edtxEmail = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxEmail);
        edtxEmail.setText("");
        String fecha = Util.FechaActual("yyyy-MM-dd");
        TextView txtFecha = (TextView) dialogoFormularioTerminos.findViewById(R.id.txtFecha);
        txtFecha.setText(fecha);
        EditText edtxCiudad = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCiudad);
        edtxCiudad.setText(cliente.codCiudad + " - " + cliente.Ciudad);
        EditText edtxCedula = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCedula);
        edtxCedula.setText(cliente.codigo);
        String nombre = cliente.Nombre.toUpperCase(Locale.getDefault()).replace(".", " ");
        if (nombre.equals("")) {
            nombre = cliente.razonSocial.toUpperCase(Locale.getDefault()).replace(".", " ");
        }
        EditText edtxNombre = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxNombre);
        edtxNombre.setText(nombre);
        TextView txtLeerTerminos = (TextView) dialogoFormularioTerminos.findViewById(R.id.txtLeerTerminos);
        txtLeerTerminos.setOnClickListener(this);
        Button btnAceptarFormularioTerminos = (Button) dialogoFormularioTerminos.findViewById(R.id.btnAceptarFormularioTerminos);
        btnAceptarFormularioTerminos.setTag(cliente);
        btnAceptarFormularioTerminos.setOnClickListener(this);
        Button btnCancelarFormularioTerminos = (Button) dialogoFormularioTerminos.findViewById(R.id.btnCancelarFormularioTerminos);
        btnCancelarFormularioTerminos.setOnClickListener(this);
        Button btnFotoFormularioTerminos = (Button) dialogoFormularioTerminos.findViewById(R.id.btnFotoFormularioTerminos);
        btnFotoFormularioTerminos.setTag(cliente);
        btnFotoFormularioTerminos.setOnClickListener(this);
        dialogoFormularioTerminos.setCancelable(false);
        dialogoFormularioTerminos.show();
        Util.closeTecladoStartActivity(FormBuscarClienteActivity.this);
    }

    public void mostrarDialogoTerminos() {
        if (dialogoTerminos == null) {
            dialogoTerminos = new Dialog(this);
            dialogoTerminos.setContentView(R.layout.dialog_terminos);
            dialogoTerminos.setTitle(getResources().getString(R.string.terminos));
            dialogoTerminos.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            Button btnAceptarTerminos = (Button) dialogoTerminos.findViewById(R.id.btnAceptarTerminos);
            btnAceptarTerminos.setOnClickListener(this);
            Button btnCancelarTerminos = (Button) dialogoTerminos.findViewById(R.id.btnCancelarTerminos);
            btnCancelarTerminos.setOnClickListener(this);
            String descipcion = DataBaseBO.obtenerTerminosHabeasData();
            WebView myWebViewDT = (WebView) dialogoTerminos.findViewById(R.id.webViewDescripcionTerminos);
            myWebViewDT.loadData(descipcion, "text/html; charset=UTF-8", "UTF-8");
        }
        dialogoTerminos.setCancelable(false);
        dialogoTerminos.show();
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.txtLeerTerminos:
                mostrarDialogoTerminos();
                break;
            case R.id.btnAceptarFormularioTerminos:
                boolean habheasDataAceptado = false;
                RadioButton rdbAceptaTerminosSi = (RadioButton) dialogoFormularioTerminos.findViewById(R.id.rdbAceptaTerminosSi);
                RadioButton rdbAceptaTerminoNo = (RadioButton) dialogoFormularioTerminos.findViewById(R.id.rdbAceptaTerminosSi);
                RadioGroup rdgAceptaTerminos = (RadioGroup) dialogoFormularioTerminos.findViewById(R.id.rdgAceptaTerminos);
                int rdSeleccionado = rdgAceptaTerminos.getCheckedRadioButtonId();
                System.out.println("+++++++++++++++++++++ " + rdSeleccionado);
                if (rdSeleccionado <= 0) {
                    Util.MostrarAlertDialog(FormBuscarClienteActivity.this, "Por favor seleccione si acepta o no la manipulacion de datos");
                    return;
                }
                if (bitmap == null && rdbAceptaTerminosSi.isChecked()) {
                    Util.MostrarAlertDialog(FormBuscarClienteActivity.this, "Por favor tome la firma");
                    return;
                }
                if (rdbAceptaTerminosSi.isChecked()) {
                    habheasDataAceptado = true;
                } else if (rdbAceptaTerminoNo.isChecked()) {
                    habheasDataAceptado = false;
                }
                if (cliente == null) {
                    cliente = (Cliente) v.getTag();
                }
                gestionHabbeasData(habheasDataAceptado, cliente);
                break;
            case R.id.btnFotoFormularioTerminos:
                if (cliente == null) {
                    cliente = (Cliente) v.getTag();
                }
                if (firma != null || bitmap != null) {
                    AlertDialog alertDialog;
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            tomarFirma();
                        }
                    }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    alertDialog = builder.create();
                    alertDialog.setMessage("Ya ha tomado una firma.\n?Desea firmar nuevamente?");
                    alertDialog.show();
                } else {
                    tomarFirma();
                }
                break;
            case R.id.btnCancelarFormularioTerminos:
                dialogoFormularioTerminos.cancel();
                break;
            case R.id.btnAceptarTerminos:
                dialogoTerminos.cancel();
                break;
            case R.id.btnCancelarTerminos:
                dialogoTerminos.cancel();
                break;
            default:
                break;
        }
    }


    public void tomarFirma() {
        String nombre = cliente.Nombre.toUpperCase(Locale.getDefault()).replace(".", " ");
        if (nombre.equals("")) {
            nombre = cliente.razonSocial.toUpperCase(Locale.getDefault()).replace(".", " ");
        }
        Util.clearPrefence(FormBuscarClienteActivity.this, Const.PREFERENCEFIRMA);
        String keysre[] = {"CODIGO", "NOMBRE"};
        String datosre[] = {cliente.codigo, nombre};
        Util.putPrefence(FormBuscarClienteActivity.this, Const.PREFERENCEFIRMA, keysre, datosre);
        Intent formDibujoLibre = new Intent(this, FormDibujoLibre.class);
        startActivityForResult(formDibujoLibre, Const.RESP_FIRMA);
    }


    public void gestionHabbeasData(final boolean habheasDataAceptado, final Cliente cliente) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("?Desea almacenar la autorizacion?").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                guardarHabbeasData(habheasDataAceptado, cliente);
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert1 = builder1.create();
        alert1.show();
        /*
         * if (habheasDataAceptado) { guardarHabbeasData(habheasDataAceptado,
         * cliente); }else{ }
         */
    }

    public void guardarHabbeasData(final boolean habheasDataAceptado, final Cliente clienteIn) {
        if (usuario == null) {
            usuario = DataBaseBO.CargarUsuario();
        }
        EditText edtxCedula = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCedula);
        String cedula = edtxCedula.getText().toString();
        EditText edtxNombre = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxNombre);
        String nombre = edtxNombre.getText().toString();
        EditText edtxObservaciones = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxObservaciones);
        String observacion = edtxObservaciones.getText().toString();
        EditText edtxEmail = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxEmail);
        String email = edtxEmail.getText().toString();
        String numDoc = "A" + usuario.codigoVendedor + clienteIn.codigo + Util.FechaActual("yyyyMMddHHmmssSSS");
        ;
        HabeasData habeasData = new HabeasData();
        habeasData.numDoc = numDoc;
        habeasData.codigoCliente = cedula;
        habeasData.nit = cedula;
        habeasData.nombreCliente = nombre;
        habeasData.codigoCiudad = cliente.Ciudad;
        habeasData.email = email;
        habeasData.autorizo = habheasDataAceptado == true ? 1 : 0;
        habeasData.nuevo = "0";
        habeasData.observacion = observacion;
        habeasData.codigoVendedor = usuario.codigoVendedor;
        boolean estadoHab = DataBaseBO.guardarHabeasData(habeasData);
        if (estadoHab) {
            if (habheasDataAceptado) {
                if (bitmap == null) {
                    Util.MostrarAlertDialog(FormBuscarClienteActivity.this, "No se pudo almacenar la firma.\nPor favor intentelo nuevamente");
                    return;
                }
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] byteArray = stream.toByteArray();
                if (byteArray == null) {
                    Util.MostrarAlertDialog(FormBuscarClienteActivity.this, "No se pudo almacenar la firma.\nPor favor intentelo nuevamente");
                    return;
                }
                Foto foto = new Foto();
                foto.imagen = byteArray;
                foto.nroDoc = numDoc;
                estadoHab = DataBaseBO.guardarFirmaHabeasData(foto);
            }
            if (estadoHab) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setMessage("Autorizacion almacenada correctamente").setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        resetValoresDialogoFormularioTerminos();
                        if (habheasDataAceptado) {
                            CargarInformacionCliente(clienteIn);
                            dialogoFormularioTerminos.cancel();
                        } else {
                            dialogoFormularioTerminos.cancel();
                        }
                    }
                });
                AlertDialog alert1 = builder1.create();
                alert1.show();
            } else {
                Util.MostrarAlertDialog(FormBuscarClienteActivity.this, "Ocurrio un error al almacenar la firma de uso de datos");
            }
        } else {
            Util.MostrarAlertDialog(FormBuscarClienteActivity.this, "Ocurrio un error al almacenar la autorizacion de uso de datos");
        }
    }

    public void resetValoresDialogoFormularioTerminos() {
        bitmap = null;
        firma = null;
        RadioGroup rdgAceptaTerminos = (RadioGroup) dialogoFormularioTerminos.findViewById(R.id.rdgAceptaTerminos);
        rdgAceptaTerminos.clearCheck();
        EditText edtxCedula = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxCedula);
        edtxCedula.setText("");
        EditText edtxNombre = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxNombre);
        edtxNombre.setText("");
        EditText edtxObservaciones = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxObservaciones);
        edtxObservaciones.setText("");
        EditText edtxEmail = (EditText) dialogoFormularioTerminos.findViewById(R.id.edtxEmail);
        edtxEmail.setText("");
    }

    public void CargarInformacionCliente(final Cliente cliente) {
        Cliente clienteSel = cliente;
        boolean tienePedido = DataBaseBO.ExistePedidoCliente(clienteSel.codigo);

        Util.clearPrefence(this, Const.PREFERENCERE);
        String keysre[] = {"RUTERO", "CODIGOCLIENTE"};
        String datosre[] = {Const.EXTRARUTA + "", clienteSel.codigo + ""};
        Util.putPrefence(this, Const.PREFERENCERE, keysre, datosre);


        if (tienePedido) {
            AlertDialog alertDialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    /****
                     Main.cliente = listaClientes.elementAt(position);

                     Intent formInfoCliente = new Intent(FormBuscarClienteActivity.this, FormInfoClienteActivity.class);
                     startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);

                     dialog.cancel();****/
                    Usuario usuario = DataBaseBO.CargarUsuario();
                    if (usuario == null) {
                        //Util.MostrarAlertDialog(this, "No se pudo cargar la informacion del Usuario");
                        Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del Usuario", Toast.LENGTH_LONG).show();

                    } else {
                        // Guarda la Referencia al Cliente seleccionado.
                        Main.cliente = cliente;
                        DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
                        if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
                            if (DataBaseBO.GuardarSesionCliente(Main.cliente.codigo, Const.EXTRARUTA)) {
                                Intent formInfoCliente = new Intent(FormBuscarClienteActivity.this, FormInfoClienteActivity.class);
                                startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);
                            } else {
                                Util.MostrarAlertDialog(FormBuscarClienteActivity.this, "Ocurrio un problema al cargar el cliente");

                            }
                        } else {
                            Intent formInfoCliente = new Intent(FormBuscarClienteActivity.this, FormInfoClienteActivity.class);
                            startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);
                        }

                    }

                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alertDialog = builder.create();
            alertDialog.setMessage("El cliente " + clienteSel.razonSocial + " ya tiene un pedido para el dia de hoy.\n\nDesea tomar un nuevo pedido?");
            alertDialog.show();

        } else {
            //Guarda la Referencia al Cliente seleccionado.
            /******Main.cliente = listaClientes.elementAt(position);

             Intent formInfoCliente = new Intent(FormBuscarClienteActivity.this, FormInfoClienteActivity.class);
             startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);****/
            Usuario usuario = DataBaseBO.CargarUsuario();
            if (usuario == null) {
                //Util.MostrarAlertDialog(this, "No se pudo cargar la informacion del Usuario");
                Toast.makeText(getBaseContext(), "No se pudo cargar la informacion del Usuario", Toast.LENGTH_LONG).show();

            } else {
                //Guarda la Referencia al Cliente seleccionado.
                Main.cliente = cliente;
                DataBaseBO.GuardarCodPdv(Main.cliente.codigo, 1);
                Intent formInfoCliente = new Intent(FormBuscarClienteActivity.this, FormInfoClienteActivity.class);
                startActivityForResult(formInfoCliente, Const.RESP_PEDIDO_EXITOSO);
            }

        }
    }

    public void OcultarTeclado(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void OnClickRegresar(View view) {
        finish();
    }

    @Override
    public void RespSync(boolean ok, String respuestaServer, String msg, int codeRequest) {
        switch (codeRequest) {
            case Const.ENVIAR_PEDIDO:
                RespuestaEnviarInfo(ok, respuestaServer, msg);
                break;

            case Const.ACTUALIZAR_INVENTARIO:
                RespuestaActualizarInfo(ok, respuestaServer, msg);
                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Const.RESP_FIRMA:
                    procesarFirma(data);
                    break;
            }
        }
    }

    public void procesarFirma(Intent data) {
        try {
            String cedula = "";
            String nombre = "";
            String keysre[] = {"CODIGO", "NOMBRE"};
            String datosre[] = Util.getPrefence(FormBuscarClienteActivity.this, Const.PREFERENCEFIRMA, keysre);
            if (datosre.length > 0) {
                if (!datosre[0].equals("")) {
                    cedula = datosre[0];
                }
                if (!datosre[1].equals("")) {
                    nombre = datosre[1];
                }
            }
            if (cliente == null) {
                cedula = cliente.codigo;
                nombre = cliente.Nombre.toUpperCase(Locale.getDefault()).replace(".", " ");
                if (nombre.equals("")) {
                    nombre = cliente.razonSocial.toUpperCase(Locale.getDefault()).replace(".", " ");
                }
            }
            String nombreMarcaAgua = cedula + " - " + nombre;
            firma = data.getByteArrayExtra("imagen");
            if (firma != null) {
                bitmap = BitmapFactory.decodeByteArray(firma, 0, firma.length);
                if (bitmap != null) {
                    System.out.println("captura foto watermark 1");
                    bitmap = Util.ResizedImageWaterMark(bitmap, nombreMarcaAgua, FormBuscarClienteActivity.this);
                    System.out.println("captura foto watermark 2");
                }
            }
        } catch (Exception e) {
            System.out.println("Error escalando la Imagen: " + e.toString());
        } finally {
        }
    }


    public void RespuestaEnviarInfo(boolean ok, String respuestaServer, String msg) {
        final String mensaje = ok ? "Informacion Registrada con Exito en el servidor" : "No se pudo registrar informacion.\nMotivo: Fallo de conexion.";
        if (progressDialog != null) progressDialog.cancel();
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormBuscarClienteActivity.this);
                builder.setMessage(mensaje).setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        actualizarInfo();
                        //CargarInformacionCliente(cliente);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public void actualizarInfo() {

        if (!IsOnline()) {
            AlertDialog alertDialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(FormBuscarClienteActivity.this);
            builder.setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.cancel();
                    CargarInformacionCliente(cliente);
                }
            });
            alertDialog = builder.create();
            alertDialog.setMessage(getString(R.string.msg_without_connection_1));
            alertDialog.show();
        } else {
            progressDialog = ProgressDialog.show(FormBuscarClienteActivity.this, "", "Actualizando Informacion...", true);
            progressDialog.show();
            Sync sync = new Sync(FormBuscarClienteActivity.this, Const.ACTUALIZAR_INVENTARIO);
            sync.version = ObtenerVersion();
            sync.imei = ObtenerImei();
            sync.start();
        }

    }

    public String ObtenerVersion() {

        String versionApp;
        try {
            versionApp = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionApp = "0.0";
            Log.e("FormMenuEstadistica", "ObtenerVersion: " + e.getMessage(), e);
        }
        return versionApp;
    }


    public String ObtenerImei() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "NoPermissionToRead";
        }
        Main.deviceId = manager.getDeviceId();
        if (Main.deviceId == null) {
            WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null) {
                    String mac = wifiInfo.getMacAddress();
                    if (mac != null) {
                        Main.deviceId = mac.replace(":", "").toUpperCase();
                    }
                }
            }
        }
        return Main.deviceId;
    }

    public boolean IsOnline() {

        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting())
            return true;

        return true;
    }

    public void RespuestaActualizarInfo(boolean ok, String respuestaServer, String msg) {
        final String mensaje = ok ? "Inventario actualizado con exito!" : "No se pudo actualizar inventario.\nMotivo: Fallo de conexion.";
        if (progressDialog != null) progressDialog.cancel();
        this.runOnUiThread(new Runnable() {

            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(FormBuscarClienteActivity.this);
                builder.setMessage(mensaje).setCancelable(false).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        CargarInformacionCliente(cliente);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
