package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.Linea;
import celuweb.com.DataObject.Producto;


public class FormBuscarProducto extends Activity {

    Button btnBusquedaProduc;

    Vector<Linea> listaLineas;
    Vector<Producto> listaProductos;

    boolean primerEjecucion = true;
    boolean isPedido;
    int tipoTransaccion;
    private EditText txtOpBusquedaProducUno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_buscar_producto);

        btnBusquedaProduc = (Button) findViewById(R.id.btnBusquedaProduc);
        cargarBundle();
        CargarOpcionesBusqueda();
        SetListenerListView();

        txtOpBusquedaProducUno = (EditText) findViewById(R.id.txtOpBusquedaProducUno);

        if (Main.itemsBusq.length > 0) {
            listaProductos = Main.listaProductos;
            ListViewAdapter adapter = new ListViewAdapter(FormBuscarProducto.this, Main.itemsBusq, R.drawable.compra, 0);
            ListView listaBusquedaProductos = (ListView) findViewById(R.id.listaHistorialPedidos);
            listaBusquedaProductos.setAdapter(adapter);
        }

        ((EditText) findViewById(R.id.txtOpBusquedaProduc)).setText(Main.codBusqProductos);
        ((Spinner) findViewById(R.id.cbOpBusquedaProduc)).setSelection(Main.posOpBusqProductos, true);
        if (Main.posOpBusqProductos == 1) {
            txtOpBusquedaProducUno.setVisibility(View.VISIBLE);
            txtOpBusquedaProducUno.setText(Main.codBusqProductos2);
        } else {
            txtOpBusquedaProducUno.setVisibility(View.GONE);
        }
        String opSel = ((Spinner) findViewById(R.id.cbOpBusquedaProduc)).getItemAtPosition(Main.posOpBusqProductos).toString();

        if (Main.primeraVez) {

            if (opSel.equals(Const.POR_NOMBRE)) {
                txtOpBusquedaProducUno.setVisibility(View.VISIBLE);
                OpcionSeleccionada(getString(R.string.txt_part_of_name), InputType.TYPE_CLASS_TEXT);


            } else if (opSel.equals(Const.POR_CODIGO)) {
                txtOpBusquedaProducUno.setVisibility(View.GONE);
                OpcionSeleccionada(getString(R.string.txt_part_of_code), InputType.TYPE_CLASS_TEXT);

            }

            Main.primeraVez = false;
        }
    }

    private void cargarBundle() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isPedido = bundle.getBoolean("isPedido");
            tipoTransaccion = bundle.getInt("tipoTransaccion");
        }


    }

    public void CargarOpcionesBusqueda() {

        String[] items = new String[]{Const.POR_CODIGO, Const.POR_NOMBRE};
        Spinner cbOpBusquedaProduc = (Spinner) findViewById(R.id.cbOpBusquedaProduc);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        cbOpBusquedaProduc.setAdapter(adapter);
        cbOpBusquedaProduc.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if (primerEjecucion) {

                    primerEjecucion = false;

                } else {

                    String opSel = ((Spinner) findViewById(R.id.cbOpBusquedaProduc)).getItemAtPosition(position).toString();

                    if (opSel.equals(Const.POR_NOMBRE)) {
                        txtOpBusquedaProducUno.setVisibility(View.VISIBLE);
                        OpcionSeleccionada(getString(R.string.txt_part_of_name), InputType.TYPE_CLASS_TEXT);


                    } else if (opSel.equals(Const.POR_CODIGO)) {
                        txtOpBusquedaProducUno.setVisibility(View.GONE);
                        OpcionSeleccionada(getString(R.string.txt_part_of_code), InputType.TYPE_CLASS_NUMBER);

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }


    public void OpcionSeleccionada(String label, int inputType) {

        TextView lblOpBusquedaProduc = (TextView) findViewById(R.id.lblOpBusquedaProduc);
        //EditText txtOpBusquedaProduc = (EditText)findViewById(R.id.txtbuscarprecios);

        EditText txtOpBusquedaProduc = (EditText) findViewById(R.id.txtOpBusquedaProduc);


        txtOpBusquedaProduc.setText("");
        lblOpBusquedaProduc.setText(label);
        txtOpBusquedaProduc.setInputType(inputType);

        txtOpBusquedaProducUno.setText("");

        ListViewAdapter adapter = new ListViewAdapter(this, new ItemListView[]{}, R.drawable.producto, 0);
        ListView listaBusquedaProductos = (ListView) findViewById(R.id.listaHistorialPedidos);
        listaBusquedaProductos.setAdapter(adapter);

        if (listaProductos != null)
            listaProductos.removeAllElements();
    }

    public void OnClickFormBuscarProducto(View view) {

        btnBusquedaProduc.setEnabled(false);
        OcultarTeclado((EditText) findViewById(R.id.txtOpBusquedaProduc));
        EditText txtOpBusquedaProduc = (EditText) findViewById(R.id.txtOpBusquedaProduc);
        String cadBusqueda = txtOpBusquedaProduc.getText().toString().trim();
        Main.codBusqProductos = cadBusqueda;

        String codBusquedaDos = txtOpBusquedaProducUno.getText().toString().trim();
        Main.codBusqProductos2 = codBusquedaDos;

        if (cadBusqueda.equals("")) {

            Toast.makeText(getApplicationContext(), getString(R.string.txt_toast_search_alert), Toast.LENGTH_SHORT).show();
            txtOpBusquedaProduc.requestFocus();

        } else {

            boolean porCodigo = false;
            Spinner cbOpBusquedaProduc = (Spinner) findViewById(R.id.cbOpBusquedaProduc);
            String opBusqueda = cbOpBusquedaProduc.getSelectedItem().toString();

            Main.posOpBusqProductos = cbOpBusquedaProduc.getSelectedItemPosition();

            if (opBusqueda.equals(Const.POR_NOMBRE)) {
                porCodigo = false;
            } else if (opBusqueda.equals(Const.POR_CODIGO)) {
                porCodigo = true;
            }

            String and = "";
            if (Main.usuario.tipoVenta.equals(Const.AUTOVENTA)) {

                if (tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION || tipoTransaccion == Const.IS_AVERIA_TRANSPORTE) {
                    and = " AND saldo > 0 ";
                } else if (isPedido) {
                    and = " AND saldo > 0 ";
                }

            }

            ItemListView[] items;
            Vector<ItemListView> listaItems = new Vector<ItemListView>();
            listaProductos = DataBaseBO.BuscarProductos(porCodigo, cadBusqueda, codBusquedaDos, Main.cliente.CodigoAmarre, listaItems, and);

            if (listaItems.size() > 0) {

                items = new ItemListView[listaItems.size()];
                Main.itemsBusq = items;
                listaItems.copyInto(items);

            } else {

                items = new ItemListView[]{};
                Main.itemsBusq = items;

                if (listaProductos != null)
                    listaProductos.removeAllElements();

                Toast.makeText(getApplicationContext(), getString(R.string.txt_busqueda_sin_resultados), Toast.LENGTH_SHORT).show();
            }

            Main.listaProductos = listaProductos;

            ListViewAdapter adapter = new ListViewAdapter(this, items, R.drawable.prod, 0);
            ListView listaBusquedaProductos = (ListView) findViewById(R.id.listaHistorialPedidos);
            listaBusquedaProductos.setAdapter(adapter);
        }

        btnBusquedaProduc.setEnabled(true);
    }

    public void SetListenerListView() {

        ListView listaOpciones = (ListView) findViewById(R.id.listaHistorialPedidos);

        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {

                    Producto producto = listaProductos.elementAt(position);

                    Intent data = new Intent();
                    data.putExtra("producto", producto);

                    setResult(RESULT_OK, data);
                    FormBuscarProducto.this.finish();

                } catch (Exception e) {

                    String msg = e.getMessage();
                    Toast.makeText(getBaseContext(), getString(R.string.txt_error_busqueda_producto) + msg, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

	    if (keyCode == KeyEvent.KEYCODE_BACK) {

	        moveTaskToBack(false);
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}*/

    public void OcultarTeclado(EditText editText) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void AlertResetRegistrarProducto(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

    public void OnClickRegresar(View view) {

        finish();
    }


}
