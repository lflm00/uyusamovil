package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ItemListView;
import celuweb.com.DataObject.UltimoPedido;

public class FormUltimoPedidoActivity extends Activity {
	
	private Button btnCargarPedido;
	
	Dialog dialogMensaje;
	Dialog dialogCargarPedido;
	
	String numeroDoc = "";
	Vector<UltimoPedido> listaUltimoPedido;
	
	ProgressDialog progressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.from_ultimo_pedido);

		Inicializar();
		CargarUltimoPedido();
		SetListenerListView();
	}
	
	public void Inicializar() {

		//this.btnCargarPedido = (Button) findViewById(R.id.btnCargarPedido);

		Bundle bundle = getIntent().getExtras();
		
		if (bundle != null) {
			
			if (bundle.containsKey("numeroDoc"))
				numeroDoc = bundle.getString("numeroDoc"); 
		}
	}

	
	public void OnClickRegresar(View view) {
		this.setResult(RESULT_CANCELED);
		finish();
	}
	
	public void CargarUltimoPedido() {
		
		ItemListView[] listaItems;
		Vector<ItemListView> items = new Vector<ItemListView>();
		listaUltimoPedido =  DataBaseBO.getUltimoPedido(Main.cliente.codigo, items);
		
		if (items.size() > 0) {
			
			listaItems = new ItemListView[items.size()];
			items.copyInto(listaItems);
			
			ListAdapterUltimo adapter = new ListAdapterUltimo(this, listaItems);
			ListView listaPedido = (ListView)findViewById(R.id.listaUltimoPedido);
			listaPedido.setAdapter(adapter);
			
		} else {
			
			ListAdapterUltimo adapter = new ListAdapterUltimo(this, new ItemListView[]{});
			ListView listaPedido = (ListView)findViewById(R.id.listaUltimoPedido);
			listaPedido.setAdapter(adapter);
		}

	}
	
	public void SetListenerListView() {
		
		ListView listaOpciones = (ListView)findViewById(R.id.listaUltimoPedido);
		
		listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               
                try {
                	
                	UltimoPedido ultPedido = listaUltimoPedido.elementAt(position);

					Intent intent = new Intent(FormUltimoPedidoActivity.this, FormUltimoPedidoDetalleActivity.class);
					intent.putExtra("NUMDOC", ultPedido.numeroDoc);
					startActivityForResult(intent, Const.ENVIAR_PEDIDO);

//                	Producto producto = new Producto();
//                	DataBaseBO.ProductoXCodigo( ultPedido.codProducto, producto);
//
//            		Intent data =  new Intent();
//                	data.putExtra("producto", producto);
//                	data.putExtra("cantidad", ultPedido.cantidad);
//
//                	setResult(RESULT_OK, data);
//                	FormUltimoPedidoActivity.this.finish();
                	
                			
				} catch (Exception e) {
					
					String msg = e.getMessage();
					Toast.makeText(getBaseContext(), "No se pudo Cargar la Informacion del Producto: " + msg, Toast.LENGTH_LONG).show();
				}
            }
        });
	}
	


	public String ObtenerVersion() {
		
		String version;
		
		try {
        	
        	version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			
		} catch (NameNotFoundException e) {
			
			version = "0.0";
			Log.e("FormUltimoPedidoActivity", e.getMessage(), e);
		}
		
		return version;
	}
	
	public String ObtenerImei() {
		
		TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getDeviceId();
	}
	
	public void onClickRegresar(View view){
		
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(Const.ENVIAR_PEDIDO == requestCode && resultCode == RESULT_OK ){
			FormUltimoPedidoActivity.this.setResult(RESULT_OK);
			FormUltimoPedidoActivity.this.finish();
		}

	}
}
