package celuweb.com.uyusa;

import java.util.Vector;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Fiado;

public class FormFiados extends Activity implements OnClickListener {

	Dialog dialogFiado;
	Vector<Fiado> listaFiados;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form_fiados);
		
		CargarFiados();
	}
	
	private void CargarFiados() {
		
		//ValidarUsuario();
		
		TableLayout table = new TableLayout(this);
		table.setBackgroundColor(Color.WHITE);
		
		HorizontalScrollView scroll = (HorizontalScrollView)findViewById(R.id.scrollFiados);
		scroll.removeAllViews();
		scroll.addView(table);
		
		listaFiados = DataBaseBO.CargarListaFiados();
		//DataBaseBO.CargarMedicionesAgotados(listaAgotados, Main.puntoVta.codigo, Main.usuario.codigo);
		
		//String fechaAcutal = Util.FechaActual("dd/MM/yyyy");
		//DataBaseBO.CargarMedicionAgotados(listaAgotados, cliente, Main.usuario.codigo, fechaAcutal);
		
		if (listaFiados.size() > 0) {
			
			String[] cabecera = {"ESTADO", "TOTAL", "CLIENTE", "FECHA"};
			Util.Headers(table, cabecera, this);
			
			TextView textViewAux;
			ImageView imageViewAux;
			
			for (Fiado fiado : listaFiados) {
				
				int ico_estado;
				TableRow fila = new TableRow(this);
				
				if (fiado.fiado == 1)
					ico_estado = R.drawable.sin_medicion;
				else
					ico_estado = R.drawable.con_medicion;
				
				imageViewAux = new ImageView(this);
				imageViewAux.setTag(fiado.position);
				imageViewAux.setImageResource(ico_estado);
				imageViewAux.setOnClickListener(this);
				imageViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				fila.addView(imageViewAux);
				
				textViewAux = new TextView(this);
				textViewAux.setText(fiado.montoFact);			
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
				textViewAux.setTextColor(Color.argb(255,0,0,0));
				textViewAux.setTextSize(19);
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				fila.addView(textViewAux);
				
				textViewAux = new TextView(this);
				textViewAux.setText(fiado.nombre);			
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
				textViewAux.setTextColor(Color.argb(255,0,0,0));	
				textViewAux.setTextSize(19);
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				fila.addView(textViewAux);
				
				textViewAux = new TextView(this);
				textViewAux.setText(fiado.fecha);			
				textViewAux.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
				textViewAux.setTextColor(Color.argb(255,0,0,0));	
				textViewAux.setTextSize(19);
				textViewAux.setBackgroundDrawable(this.getResources().getDrawable(android.R.drawable.editbox_dropdown_light_frame));
				fila.addView(textViewAux);
				
				table.addView(fila);
			}
			
		} else {
			
			Toast.makeText(getApplicationContext(), "Busqueda sin resultados", Toast.LENGTH_SHORT).show();
		}
	}
	
	/*public void ValidarUsuario() {
		
		if (Main.usuario == null)
			Main.usuario = new Usuario();
		
		if (Main.usuario.codigoVendedor == null || Main.usuario.nombreVendedor == null) {
			
			Usuario usuario = DataBaseBO.ObtenerUsuario();
			Main.usuario.codigoVendedor = usuario.codigo;
			Main.usuario.nombreVendedor = usuario.nombre;
		}
	}*/
	
	@Override
	public void onClick(View view) {		
		
		String posTag = ((ImageView)view).getTag().toString();
		int position = Util.ToInt(posTag);
		
		MostrarDialgoFiado(position);
		Log.i("", "position = " + position);
	}
	
	public void MostrarDialgoFiado(int position) {
		
		final Fiado fiado = listaFiados.elementAt(position); //<u></u>
		
		if (fiado.fiado == 0) {
			
			Util.MostrarAlertDialog(this, "El pedido seleccionado ya esta en estado Cancelado!");
			
		} else {
			
			if (dialogFiado == null) {
				
				dialogFiado = new Dialog(this);
				dialogFiado.requestWindowFeature(Window.FEATURE_LEFT_ICON);
				dialogFiado.setContentView(R.layout.dialog_fiado);
				dialogFiado.setTitle("Registrar Pago Fiado");
			}
			
			String mensaje = "<b>Esta seguro de Cambiar el Estado de la Factura: " + fiado.numeroDoc + "?</b><br /><br />"; 
			mensaje +=	"El pedido realizado al Cliente: <b><i>" + fiado.nombre  + "</i></b> Quedara en Estado Cancelado!";
			
			((TextView) dialogFiado.findViewById(R.id.lblMsg)).setText(Html.fromHtml(mensaje));
			
			((Button)dialogFiado.findViewById(R.id.btnAceptar)).setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					
					RegistrarFiado(fiado);
				}
			});
			
			((Button) dialogFiado.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					
					dialogFiado.cancel();
				}
			});
			
			dialogFiado.setCancelable(false);
			dialogFiado.show();
			dialogFiado.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.op_fiados);
		}
	}
	
	public void RegistrarFiado(Fiado fiado) {
		
		fiado.fiado = 0;
		boolean ok = DataBaseBO.CancelarFiado(fiado);
		
		if (ok) {
			
			CargarFiados();
			dialogFiado.cancel();
			
		} else {
			
			Util.MostrarAlertDialog(this, "No se pudo Registrar el Pedido como Cancelado!");
		}
	}
}
