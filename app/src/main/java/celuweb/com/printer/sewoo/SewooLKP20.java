/**
 * 
 */
package celuweb.com.printer.sewoo;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import com.woosim.bt.WoosimPrinter;

import android.content.Context;
import android.util.Log;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.CabeceraTirilla;
import celuweb.com.DataObject.EstadisticaRecorrido;
import celuweb.com.DataObject.FormaPago;
import celuweb.com.DataObject.Impresion;
import celuweb.com.DataObject.ImpresionDetalle;
import celuweb.com.DataObject.ImpresionEncabezado;
import celuweb.com.DataObject.ImpresionNotaCredito;
import celuweb.com.DataObject.ImpresionRecaudo;
import celuweb.com.DataObject.InformeInventario;
import celuweb.com.DataObject.InformeRecaudo;
import celuweb.com.DataObject.Producto;
import celuweb.com.DataObject.RegCanastilla;
import celuweb.com.DataObject.TotalInventarioDia;
import celuweb.com.DataObject.Usuario;
import celuweb.com.uyusa.Const;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.Util;

/**
 * Clase que contiene los metodos y atributos necesarios para configurar la
 * impresora sewoo LK-P20B
 * 
 * @author JICZ
 * 
 */
public class SewooLKP20 {

	/* Datos de impresora necesarios para la conexion */
	public final static String CONFIG_IMPRESORA = "PRINTER";
	public final static String MAC_IMPRESORA = "MAC";
	public final static String NOMBREEMPRESA = "PANIFICADORA COUNTRY";
	/* ********************************************** */

	/* Tama�os maximos para cada columna-detalle de la tirilla */
	private static final int TAM_MAX_CODIGO = 10;
	private static final int TAM_MAX_VALOR_UNIT = 11;
	private static final int TAM_MAX_PRODUCTO = 32;
	private static final int TAM_MAX_CANTIDAD = 15;
	private static final int TAM_MAX_TOTALES = 26;
	private static final int TAM_MAX_TOTAL_UNIT = 16;
	private static final int TAM_MAX_NUM_DOC = 22;
	private static final int TAM_MAX_ESPACIO = 27;


	/**
	 * Comando rapido para imprimir negrilla
	 */
	private final static boolean NEGRILLA = true;

	/**
	 * Comando rapido para imprimir Texto normal
	 */
	private final static boolean NORMAL = false;
	
//	private static final int TAMANO_TEXTO_NORMAL = 0X00;
	private static final int TAMANO_TEXTO_NORMAL = 0X01;
	private static final int TAMANO_TEXTO_ALTO = 0X01;
	

	/**
	 * Activity desde donde es llamada la funcion de la impresora.
	 */
	private Context context;

	/**
	 * Identifica el lenguaje usado para la impresora. Lenguaje conpatible con
	 * el idioma espa�ol (OEM Latin 1 + Euro Symbol)
	 */
	public final static String CP858 = "CP858";

	/**
	 * Limpiar datos y buffer, reiniciar configuracion de la impresora.
	 */
	private static final byte[] initialize = { 0x1B, 0x40 };

	/**
	 * Seleccionar set de caracteres para idioma espa�ol
	 */
	private static final byte[] characterSet = { 0x1b, 0x52, 0x07 };

	/**
	 * Jusitificar al centro el texto siguiente a este comando
	 */
	private static final byte[] centrar = { 0x1b, 0x61, 49 };

	/**
	 * Jusitificar a la izquierda el texto siguiente a este comando
	 */
	private static final byte[] izquierda = { 0x1b, 0x61, 48 };

	/**
	 * Print mode. Character FONT B (9x24) = 42 caracteres por linea. 
	 */
	private static final byte[] characterSpacing = { 27, 33, 1 };

	/**
	 * Imprimir los datos en el bufer y alimenta una linea y vuelve al inicio de
	 * la linea. (0x0A)
	 */
	private static final byte[] LF = { 0x0A };

	/**
	 * Imprimir los datos en el bufer y alimenta una linea y vuelve al inicio de
	 * la linea
	 */
	private static final byte[] FF = { 0x0C };

	/**
	 * Referencia a la libreria de la impresora.
	 */
	private WoosimPrinter woosimPrinter;
	
	/*Asignacion del formateador para la numeracion de las tirillas.
	 * usando punto, como separador de decimales.*/
	private DecimalFormatSymbols simbolos;
	{
		simbolos = new DecimalFormatSymbols();
		simbolos.setDecimalSeparator('.');
	}	
	private DecimalFormat format;
	{
		format = new DecimalFormat("#0.00");
	}

	/**
	 * 
	 * @param context
	 */
	public SewooLKP20(Context context) {
		this.context = context;
		this.woosimPrinter = new WoosimPrinter();
	}

	/**
	 * Conectar a la impresora establecida como por defecto.
	 * 
	 * @return 1 = SUCCESS <BR>
	 *         -2 = FAIL<BR>
	 *         -5 = NOT_PARING<BR>
	 *         -6 = ALREADY_CONNECTED<BR>
	 *         -8 = BLUETOOTH_NOT_ENABLE<BR>
	 */
	public int conectarImpresora(String address) {
		int ret = this.woosimPrinter.BTConnection(address, false);
		Log.i("CONECTAR IMPRESORA " + address, " RET: -> " + ret);
		return ret;
	}

	/**
	 * Limpiar la memoria intermedia y desconectar la impresora.
	 */
	public void desconectarImpresora() {
		this.woosimPrinter.clearSpool();
		this.woosimPrinter.closeConnection();
	}

	/**
	 * Unicamente formato .bmp de 1 bit, (monocromatico) es compatible.
	 * 
	 * @param path
	 *            ruta a la imagen formato .bmp.
	 * @return 1 = SUCCESS <BR>
	 *         -2 = FAIL<BR>
	 *         -4 = NOT_FOUND_IMAGE_FILE<BR>
	 */
	public int imprimirLogo(String path) {
		try {
			return this.woosimPrinter.printBitmap(path);
		} catch (IOException e) {
			return -2;
		}
	}

	/**
	 * No se ha implementado para sewoo LK-P20B.
	 * Unicamente formato .bmp de 1 bit, (monocromatico) es compatible.
	 * 
	 * @param path
	 *            ruta a la imagen formato .bmp.
	 * @return 1 = SUCCESS <BR>
	 *         -2 = FAIL<BR>
	 *         -4 = NOT_FOUND_IMAGE_FILE<BR>
	 */
	public int imprimirLogoEmpresarial() {
		int ret = -2;

		try {
//			File file = new File(Util.DirApp(), PreferenceLogo.getNombreLogo(context).toString().trim());
			File file = new File("");
			String path = file.getAbsolutePath();
			if (file.exists()) {
				ret = this.woosimPrinter.printBitmap(path);
				/*
				 * Asignar tiempo de espera para que culmine la transmision
				 * bluethooth de datos de la imagen
				 */
				Thread.sleep(3000);
			} else {
				ret = -4;
			}
		} catch (IOException e) {
			Log.e("WoosimR240.java", e.getMessage().toString());
			ret = -2;
		} catch (InterruptedException e) {
			Log.e("WoosimR240.java", e.getMessage().toString());
			ret = -2;
		}
		return ret;
	}

	/**
	 * Cargar texto a imprimir, texto normal.
	 * 
	 * @param lineaTexto
	 * @return Numero de bytes<BR>
	 *         -3 = BUFFER_OVERFLOW cuando excede a 9999 bytes
	 */
	public int cargarTexto(String lineaTexto, int tamTexto, boolean bold) {
		izquierda();
		return this.woosimPrinter.saveSpool(CP858, lineaTexto, tamTexto, bold);
	}

	/**
	 * Cargar texto a imprimir, texto normal.
	 * 
	 * @param lineaTexto
	 * @param bold,
	 *            negrilla o texto normal
	 * @param tamFont,
	 *            tama�o de la fuente
	 * @return
	 */
	public int cargarTexto(String lineaTexto, boolean bold, int tamFont) {
		izquierda();
		return this.woosimPrinter.saveSpool(CP858, lineaTexto, tamFont, bold);
	}

	/**
	 * Cargar texto a imprimir, texto normal.
	 * 
	 * @param lineaTexto
	 * @return Numero de bytes<BR>
	 *         -3 = BUFFER_OVERFLOW cuando excede a 9999 bytes
	 */
	public int cargarTextoCentro(String lineaTextoCentrado, int tamFont, boolean bold) {
		centrar();
		return this.woosimPrinter.saveSpool(CP858, lineaTextoCentrado, tamFont, bold);
	}

	/**
	 * Imprimir el texto salvado.
	 * 
	 * @param borrarBuffer,
	 *            limpiar buffer despues de imprimir.
	 * @return 1 = SUCCESS <BR>
	 *         0x15 = NACK <BR>
	 *         0x04 = EOT<BR>
	 *         -1 = TIMEOUT<BR>
	 */
	public int imprimirBuffer(boolean borrarBuffer) {
		lf();
		lf();
		ff();
		return this.woosimPrinter.printSpool(borrarBuffer);
	}

	public int imprimirEstatus() {
		byte[] status = { 0x10, 0x04, 0x04 };
		return this.woosimPrinter.controlCommand(status, status.length);
	}

	/**
	 * Imprime el codigo de barras correspondiente al nit de la empresa
	 * 
	 * @return
	 */
	public int imprimirCodigoBarraPruebas() {
		byte[] barCode = { 0x1D, 0x6B, 0x00, '1', '4', '7', '5', '6', '9', '4', '0', '2', '0', '8', 0x00 };
		return this.woosimPrinter.controlCommand(barCode, barCode.length);
	}
	
	/**
	 * PErmite definir un tama�o de letras normal.
	 */
	private void characterSizeSetNormal() {
		/* Tama�o por default. */
		byte[] sizeNormal = { 29, 33, 0 };
		this.woosimPrinter.controlCommand(sizeNormal, sizeNormal.length);
	}

	/**
	 * Imprime un texto de prueba.
	 */
	public int generarPaginaPrueba() {
		inicializarImpresora();
		this.woosimPrinter.clearSpool();
		characterSpacingDefault();
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro("****************************************\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTextoCentro("EMPRESA PANAMENA DE ALIMENTOS\r\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTextoCentro("RUC: 147-569-40208 D.V. 18\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTextoCentro("Via Jose Agustin Arango - Juan Diaz\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTextoCentro("Telefono: 217-22-33\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTextoCentro("PRUEBA DE IMPRESORA SEWOO\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("PRUEBA: OK \n",TAMANO_TEXTO_NORMAL, NORMAL);
		int ret = imprimirCodigoBarraPruebas();
		cargarTextoCentro("****************************************\r\n",0, NEGRILLA);
		lf();
		lf();
		ff();
		return ret;
	}
	

	/**
	 * Metodo permite la impresion del encabezado que llevan todas las tirillas.
	 * @param original 
	 * @param fechaVenta 
	 * 
	 * @param NumeroFacturaVenta,
	 *            numero de la factua, debe coincidir con la resolucion DIAN y
	 *            estar vigente.
	 */
	public void generarEncabezadoTirilla(final String NumeroFacturaVenta, final Usuario usuario, String original, boolean isPedido, boolean isAnulado, final boolean isCargue, int varAux,  final boolean isAveria) {
	
		Impresion impCliente = null;
		String factura = "";
		
		if(!isCargue && !isAveria)
			impCliente = DataBaseBO.getImpresionCliente( NumeroFacturaVenta );
		
		
		if (usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido) 
			factura = DataBaseBO.obtenerFacturaNroDoc(NumeroFacturaVenta);

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();
		
		if(varAux == 0){
			/* limpiar el buffer */
			this.woosimPrinter.clearSpool();
		}

		characterSet(); // caracteres para idioma español.

		lf();
		ff();
		characterSpacingDefault();
		
		String titulo =""; 
		String subtitulo ="";
		
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		

		
//		
//		if (usuario.codigoVendedor.substring(0, 1).equals("R"))
//			titulo = "SOCIEDAD DE ALIMENTOS DE PRIMERA S.A";
//		else
//			titulo = "PRODUCTOS ALIMENTICIOS PASCUAL S.A";
		
		if(!isCargue){
			
			if (isPedido) {

				subtitulo = isAnulado ? "Pedido Anulado" : "Factura";

			} else {

				subtitulo = isAnulado ? "Devolucion Anulada" : "Devolucion";

			}
		} if(isAveria){
			subtitulo = "Averias Transporte";
		} 
		
		if(original.equals(""))
			original = "Copia ";
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTexto(original + "\r\n"+ subtitulo+"\r\n\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(titulo + "\r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).empresa+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).nit+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).direccion+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).telefono+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).texto+"\r\n\n",	TAMANO_TEXTO_ALTO, NEGRILLA);

		
		if (usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido) {
			cargarTexto("Numero Factura:    " + factura + "\r\n",TAMANO_TEXTO_ALTO, NEGRILLA);
		}else {
			cargarTexto("Numero Pedido:    " + NumeroFacturaVenta + "\r\n",TAMANO_TEXTO_ALTO, NEGRILLA);
		}
		
		cargarTexto("FECHA:            " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("VENDEDOR:         " + usuario.codigoVendedor + " - " + usuario.nombreVendedor + "\r\n\n",TAMANO_TEXTO_ALTO, NORMAL);
		
		
		if(!isCargue && !isAveria){
			
		cargarTexto("Cond. de Pago:\r\n",TAMANO_TEXTO_ALTO, NEGRILLA);
		
		
			cargarTexto("CLIENTE:   " + impCliente.nombre + "\r\n",TAMANO_TEXTO_ALTO, NEGRILLA);
		
			if(usuario.tipoVenta.equals(Const.AUTOVENTA)){
				cargarTexto("CODIGO:    " + impCliente.codigo + "\r\n",TAMANO_TEXTO_ALTO, NORMAL);
				cargarTexto("CONTACTO:   " + impCliente.razonSocial + "\r\n",TAMANO_TEXTO_ALTO, NORMAL);
			} else {
				cargarTexto("NEGOCIO:   " + impCliente.razonSocial + "\r\n",TAMANO_TEXTO_ALTO, NORMAL);	
				cargarTexto("CODIGO:    " + impCliente.codigo + "\r\n",TAMANO_TEXTO_ALTO, NORMAL);
			}
			
				cargarTexto("RUC:       " + impCliente.ruc + "\r\n",TAMANO_TEXTO_ALTO, NORMAL);
		}
	
		
		
		if(!isCargue && !isAveria){
			cargarTexto("DIRECCION: " + impCliente.direccion + "\r\n",TAMANO_TEXTO_ALTO, NORMAL);
		}
		//
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("CODIGO      DESCRIPCION                   \r\n",TAMANO_TEXTO_ALTO, NORMAL);
		
		if (!isCargue) {
			
			if (isPedido) 
				cargarTexto("CANTIDAD     PRECIO     DESC        TOTAL \r\n", TAMANO_TEXTO_ALTO, NORMAL);
			else
				cargarTexto("CANTIDAD       PRECIO                TOTAL\r\n", TAMANO_TEXTO_ALTO, NORMAL);
			
		} else {
			cargarTexto("CANT/CAJAS      PRECIO               TOTAL\r\n", TAMANO_TEXTO_ALTO, NORMAL);
		}
		
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_ALTO, NORMAL);
		//
		Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalle( NumeroFacturaVenta, isPedido, isCargue, isAveria);
		if(!isCargue && !isAveria)
			cargarDetallesPedido(listaDetalles, isPedido, usuario, NumeroFacturaVenta, impCliente.codigo, isCargue, isAveria);
		else
			cargarDetallesPedido(listaDetalles, isPedido, usuario, NumeroFacturaVenta, "", isCargue, isAveria);
	}



	/**
	 * Metodo permite formar los detalles que seran impresos. alineados de
	 * acuerdo a la impresora.
	 * 
	 * @param detalleImprimir
	 */
	public void cargarDetallesPedido(Vector<ImpresionDetalle> listaDetalles, boolean isPedido, Usuario usuario, String NumeroFacturaVenta, String codCliente, boolean isCargue, boolean isAveria) {

		/* Variables para montos totales */
		double subTotalImp = 0;
		double descuentoImp = 0;
		double ivaImp = 0;
		double total = 0;
		double valorDevolucion = 0;
		boolean existeAmarre = false;

		/* Dar formato a cada detalle del pedido. */
		for (ImpresionDetalle detalle : listaDetalles) {
			
			if(!isCargue)
				detalle.total = detalle.cantidad * detalle.precio;
			else
				detalle.total = detalle.precio * detalle.cantidadUnidades;
			
			double sub_total = 0;
			double valor_descuento = 0;
			double valor_iva = 0;

			sub_total = detalle.total;
			valor_descuento = sub_total * detalle.descuento / 100f;
			valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100f);

			subTotalImp += sub_total;
			descuentoImp += valor_descuento;
			ivaImp += valor_iva;
			
			if (usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido) {
				formatearTextoFactura(detalle, isCargue);
				cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);
				cargarTexto(detalle.cantidadStr + detalle.precioStr + "%"+detalle.descStr+ detalle.totalStr + "\r\n\n", TAMANO_TEXTO_ALTO, NORMAL);
			} else {
				formatearTexto(detalle, isCargue);
				cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);
				cargarTexto(detalle.cantidadStr + detalle.precioStr + detalle.totalStr + "\r\n\n", TAMANO_TEXTO_ALTO, NORMAL);
			}
			

			if (!isPedido && !isAveria)
				cargarTexto(detalle.descMotivo + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);

		}
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_ALTO, NORMAL);

		String strSubTotal = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(subTotalImp)), 2)));
		String strDescuento = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(descuentoImp)), 2)));
		String strIva = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(ivaImp)), 2)));
		String strDevolucion  = "";
		
		if (!isCargue && !isAveria) {

			if (isPedido) {

				if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
					existeAmarre = DataBaseBO.existeAmarreConDev(NumeroFacturaVenta, codCliente, valorDevolucion);
					valorDevolucion = DataBaseBO.obtenerValorDevAmarre(NumeroFacturaVenta, codCliente);

					System.out.println("ValorDevolucion por fuera del DB: " + valorDevolucion);
				}

				strDevolucion = String.valueOf(format.format(valorDevolucion));
				
				//encabezado.valor_neto = encabezado.sub_total + encabezado.total_iva - encabezado.valor_descuento;

				total = usuario.tipoVenta.equals(Const.AUTOVENTA) ? (subTotalImp + ivaImp - descuentoImp) - valorDevolucion : subTotalImp + ivaImp - descuentoImp;
			}else{
				
				total =  subTotalImp + ivaImp - descuentoImp;
			}
		}else{
			
			strDevolucion = String.valueOf(format.format(valorDevolucion));

			total =  subTotalImp + ivaImp - descuentoImp;
		}
		
		
		String strTotal = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(total)), 2)));
		/* Formatear totales. */
		
		strIva = Util.lpad(strIva, TAM_MAX_TOTALES, " ");
		strDescuento = Util.lpad(strDescuento, TAM_MAX_TOTALES, " ");
		strDevolucion = Util.lpad(strDevolucion, TAM_MAX_TOTALES, " "); 
		strTotal = Util.lpad(strTotal, TAM_MAX_TOTALES, " ");
		strSubTotal = Util.lpad(strSubTotal, TAM_MAX_TOTALES, " ");
		
		
		if(usuario.tipoVenta.equals(Const.AUTOVENTA)){
			cargarTexto("      TOTAL:    " + strTotal + "\r\n\n", TAMANO_TEXTO_ALTO, NEGRILLA);
		}
		
		cargarTexto("      SUBTOTAL: " + strSubTotal + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("      IVA:     " + strIva + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("      DESCUENTO:" + strDescuento + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);
		
		if(usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido){
			
			if(!isCargue && !isAveria)
				cargarTexto("      N.CREDITO:" + strDevolucion + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);
			
		}
		
		cargarTexto("------------------------------------------\r\n\n\n",TAMANO_TEXTO_ALTO, NORMAL);
		 
		
		if(usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido){
			
			if(!isCargue && !isAveria){
				
				cargarTexto("C A N A S T I L L A S"+"\r\n",TAMANO_TEXTO_ALTO, NORMAL) ;
				cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_ALTO, NORMAL);
				cargarTexto("Codigo            Descripcion             \r\n",TAMANO_TEXTO_ALTO, NORMAL);
				cargarTexto("      cant. Ent               Cant. Dev   \r\n",TAMANO_TEXTO_ALTO, NORMAL);
				cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_ALTO, NORMAL);
				
				Vector<RegCanastilla> vCanastilla = DataBaseBO.getImpresionCanastillas( NumeroFacturaVenta, false );

				RegCanastilla canastilla;

				if ( vCanastilla.size() > 0 ) {
					Enumeration< RegCanastilla > elements = vCanastilla.elements();

					while (elements.hasMoreElements()) {

						canastilla = elements.nextElement();
						
						formatearTextoDetalleCanasta(canastilla, false);
						cargarTexto(canastilla.codigo + canastilla.descripcion + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);
						cargarTexto(canastilla.entregada +  canastilla.devuelta + "\r\n", TAMANO_TEXTO_ALTO, NORMAL);

					}
				}
				cargarTexto("------------------------------------------\r\n\n\n",TAMANO_TEXTO_ALTO, NORMAL);
				
			}
			
		}
		
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_ALTO, NORMAL);
		
	}

	
	private void cargarResumenDetalleFormaPagoGeneral(Vector<FormaPago> listaDetalleFP) {  
		
		/*Variables para montos totales*/
		double totalMonto = 0;

		float efectivo= 0;
		float cheques = 0;
		float consignaciones = 0;

		/* Dar formato a cada detalle del pedido. */
		for (FormaPago formaPago : listaDetalleFP) {

			formatearTextoFormaPago(formaPago);

			if (formaPago.codigo == 1) {
				efectivo += formaPago.monto;
				totalMonto += formaPago.monto;
			} else if (formaPago.codigo == 3) {
				consignaciones += formaPago.monto;
				totalMonto += formaPago.monto;
			} else {
				cheques += formaPago.monto;
				totalMonto += formaPago.monto;
			}
		}

		String strEfectivo  = Util.SepararMiles(Util.round(Util.QuitarE(format.format(efectivo)), 2));
		String strCheques = Util.SepararMiles(Util.round(Util.QuitarE(format.format(cheques)), 2));
		String strConsignaciones       = Util.SepararMiles(Util.round(Util.QuitarE(format.format(consignaciones)), 2));
		String strMonto  = Util.SepararMiles(Util.round(Util.QuitarE(format.format(totalMonto)), 2));
		/*Formatear totales.*/
		strEfectivo = Util.lpad(strEfectivo, TAM_MAX_TOTALES, " ");
		strCheques = Util.lpad(strCheques, TAM_MAX_TOTAL_UNIT, " ");
		strConsignaciones = Util.lpad(strConsignaciones, TAM_MAX_TOTALES, " ");
		strMonto = Util.lpad(strMonto, TAM_MAX_TOTALES, " ");
		
		cargarTexto("  Efectivo: " + strEfectivo + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("  Cheques del Dia:    " + strCheques + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("  Depositos:" + strConsignaciones + "\r\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("  TOTAL:    " + strMonto + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);		
		cargarTexto("  Recaudo:  " + strMonto + "\r\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Cliente       Num Doc.               Total\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		Vector<InformeRecaudo> infRecaudo = DataBaseBO.ListaInformeDeRecaudo();
		
		cargarInformeRecaudo(infRecaudo);
		
	}
	


	private void cargarInformeRecaudo(Vector<InformeRecaudo> infRecaudo) {

		float totalCant = 0;

		for (InformeRecaudo informe : infRecaudo) {

			formatearTextoInformeRecaudo(informe);

			cargarTexto(informe.codCliente + informe.nroDoc + informe.StrValor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			totalCant += informe.valor;

		}

		String SrtMonto = Util.SepararMiles(Util.QuitarE(Util.round(format.format(totalCant), 2)));
		SrtMonto = Util.lpad(SrtMonto, TAM_MAX_TOTAL_UNIT, " ");
		cargarTexto("------------------------------------------\r\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("     	TOTAL:" + SrtMonto + "\r\n\n", TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTexto("------------------------------------------\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

		cargarTexto("Recibo de caja sujeto a aprobacion del \r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("departamento de cartera.\r\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
	}

	/**
	 * Metodo permite dar el maximo tama�o de caracteres permitidos para cada
	 * columna sin perder la forma. (maximo tama�o permitido para la impresora
	 * WSP-R240 = 384 dots = 42 caracteres maximo, 1 caracter = 9 dots )
	 * 
	 * @param detalle
	 */
	private void formatearTexto(ImpresionDetalle detalle, boolean isCargue) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas en el encabezado
		 */
		detalle.nombre = Util.rpad(detalle.nombre, TAM_MAX_PRODUCTO, " ");
		detalle.codigo = Util.rpad(detalle.codigo, TAM_MAX_CODIGO, " ");
		
		if(!isCargue)
			detalle.cantidadStr = Util.rpad("  " + Util.round(String.valueOf(detalle.cantidad),0), TAM_MAX_CANTIDAD, " ");
		else
			detalle.cantidadStr = Util.rpad("  " + Util.round(String.valueOf(detalle.cantidadUnidades),0)+"/"+ Util.round(String.valueOf(detalle.cantidad),0), TAM_MAX_CANTIDAD, " ");
		
		detalle.precioStr =   Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(detalle.precio)),2)), TAM_MAX_VALOR_UNIT, " ");
		detalle.totalStr =    Util.lpad(Util.SepararMiles(Util.round(String.valueOf(format.format(detalle.total)),2)), TAM_MAX_TOTAL_UNIT, " ");
	}
	
	/**
	 * Metodo permite dar el maximo tama�o de caracteres permitidos para cada
	 * columna sin perder la forma. (maximo tama�o permitido para la impresora
	 * WSP-R240 = 384 dots = 42 caracteres maximo, 1 caracter = 9 dots )
	 * 
	 * @param detalle
	 */
	private void formatearTextoDetalleCanasta(RegCanastilla canastilla, boolean isLiquidacionCanastilla) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas en el encabezado
		 */
		canastilla.descripcion = Util.rpad(canastilla.descripcion, TAM_MAX_PRODUCTO, " ");
		canastilla.codigo = Util.rpad(canastilla.codigo, TAM_MAX_CODIGO, " ");
		
		if(isLiquidacionCanastilla)
			canastilla.devuelta =    Util.lpad(Util.round(String.valueOf(format.format(canastilla.nroDevuelto)),0), 11, " ");
		else{
			
			canastilla.entregada =   Util.lpad(Util.round(String.valueOf(format.format(canastilla.nroEntrega)),0), 11, " ");
			canastilla.devuelta =    Util.lpad(Util.round(String.valueOf(format.format(canastilla.nroDevuelto)),0), 24, " ");
		}
		
		
	}


	private void formatearTextoFormaPago(FormaPago formaPago) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas en el encabezado
		 */
		formaPago.descripcion = Util.rpad(formaPago.descripcion, TAM_MAX_PRODUCTO, " ");
		formaPago.nroDoc = Util.rpad(formaPago.descripcion, TAM_MAX_TOTAL_UNIT, " ");
		formaPago.strMonto =    Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(formaPago.monto)),2)), TAM_MAX_VALOR_UNIT, " ");
	}
	
	
	private void formatearTextoDetalleRecaudo(FormaPago formaPago) {

		formaPago.factura = Util.rpad(formaPago.factura, TAM_MAX_PRODUCTO, " ");
		formaPago.strMonto =    Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(formaPago.monto)),2)), 10, " ");
	}
	
	/**
	 * Metodo permite dar el maximo tama�o de caracteres permitidos para cada
	 * columna sin perder la forma. (maximo tama�o permitido para la impresora
	 * WSP-R240 = 384 dots = 42 caracteres maximo, 1 caracter = 9 dots )
	 * 
	 * @param detalle
	 */
	private void formatearTextoInformeRecaudo(InformeRecaudo informe) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas
		 * en el encabezado
		 */

		informe.codCliente = Util.rpad(informe.codCliente, 12, " ");
		informe.nroDoc = Util.rpad(informe.nroDoc, TAM_MAX_NUM_DOC, " ");
		informe.StrValor = Util.lpad(Util.SepararMiles(Util.round(format.format(informe.valor), 2)), 8, " ");
	}
	
	private void formatearTextoLiquidacion(ImpresionDetalle detalle) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas
		 * en el encabezado
		 */
		detalle.nombre = Util.rpad(detalle.nombre, TAM_MAX_PRODUCTO, " ");
		detalle.codigo = Util.rpad(detalle.codigo, TAM_MAX_CODIGO, " ");
		
		detalle.strInventario = Util.lpad(String.valueOf(detalle.cantidadInventario), 8," ");
		detalle.strDigitada = Util.lpad(String.valueOf(detalle.cantidadDigitada), 14," ");
		detalle.strTransaccion = Util.lpad(String.valueOf(detalle.cantidadTransaccion), 8," ");
		detalle.strDiferencia= Util.lpad(String.valueOf(detalle.diferencia),14, " ");
	}
	
	private void formatearTextoLiquidacionLiq(Producto detalle) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas
		 * en el encabezado
		 */
		detalle.descripcion = Util.rpad(detalle.descripcion, TAM_MAX_PRODUCTO, " ");
		detalle.codigo = Util.rpad(detalle.codigo, TAM_MAX_CODIGO, " ");
		
		detalle.strInventario = Util.lpad(String.valueOf(detalle.cantidadInventario), 18," ");
		detalle.strDigitada = Util.lpad(String.valueOf(detalle.cantidadDigitada), 3," ");
		detalle.strTransaccion = Util.lpad(String.valueOf(detalle.cantidadTransaccion), 8," ");
		detalle.strDiferencia= Util.lpad(String.valueOf(detalle.diferencia),18, " ");
	}
	
	private void formatearTextoLiquidacionAveria(ImpresionDetalle detalle) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas
		 * en el encabezado
		 */
		detalle.nombre = Util.rpad(detalle.nombre, TAM_MAX_PRODUCTO, " ");
		detalle.codigo = Util.rpad(detalle.codigo, TAM_MAX_CODIGO, " ");
		
		
		detalle.strInventario = Util.lpad(" ", 8," ");
		detalle.strDigitada = Util.lpad(String.valueOf(detalle.cantidad), 22," ");
		detalle.strTransaccion = Util.lpad(" ", 8," ");
		detalle.strDiferencia= Util.lpad(" ",14, " ");
	}



	/**
	 * Inicializar la impresora.
	 */
	public int inicializarImpresora() {
		return this.woosimPrinter.controlCommand(initialize, initialize.length);
	}

	/**
	 * Inicializar la impresora a 42 cpl (caracteres por linea).
	 */
	public int characterSpacingDefault() {
		return this.woosimPrinter.controlCommand(characterSpacing, characterSpacing.length);
	}

	/**
	 * Seleccionar set de caracteres para idioma espa�ol.
	 */
	public int characterSet() {
		return this.woosimPrinter.controlCommand(characterSet, characterSet.length);
	}

	/**
	 * Imprimir los datos en el bufer y alimenta una linea y vuelve al inicio de
	 * la linea
	 */
	public int lf() {
		return this.woosimPrinter.controlCommand(LF, LF.length);
	}

	/**
	 * Imprimir los datos en el bufer y alimenta una linea y vuelve al inicio de
	 * la linea
	 */
	public int ff() {
		return this.woosimPrinter.controlCommand(FF, FF.length);
	}

	/**
	 * Justificar al centro el siguiente texto a este comando.
	 */
	public int centrar() {
		return this.woosimPrinter.controlCommand(centrar, centrar.length);
	}

	/**
	 * Justificar a la derecha el siguiente texto a este comando.
	 */
	public int izquierda() {
		return this.woosimPrinter.controlCommand(izquierda, izquierda.length);
	}

	/**
	 * @return the woosimPrinter
	 */
	public WoosimPrinter getWoosimPrinter() {
		return woosimPrinter;
	}

	/**
	 * @param woosimPrinter
	 *            the woosimPrinter to set
	 */
	public void setWoosimPrinter(WoosimPrinter woosimPrinter) {
		this.woosimPrinter = woosimPrinter;
	}

	/**
	 * 
	 * @param NumeroFacturaVenta
	 * @param usuario
	 * @param isPedido
	 */
	public void generarRecaudoTirilla(String NumeroFacturaVenta, Usuario usuario, boolean isPedido) {
		
//		int nroRecibo = 0;
//	
//		if(isPedido)
//			nroRecibo = 1;
		
		Impresion impCliente = DataBaseBO.getImpresionClienteRecaudo( NumeroFacturaVenta );

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		inicializarImpresora();
		characterSizeSetNormal();

		/* limpiar el buffer */
		this.woosimPrinter.clearSpool();

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();
		characterSpacingDefault();
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);

		
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		cargarTextoCentro(cabeceraTirilla.get(0).empresa+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).nit+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).direccion+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).telefono+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).texto+"\r\n\n",	TAMANO_TEXTO_ALTO, NEGRILLA);


		cargarTexto("Numero Recaudo:    " + NumeroFacturaVenta + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTexto("FECHA:            " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("VENDEDOR:         " + usuario.codigoVendedor + " - " + usuario.nombreVendedor + "\r\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("Cond. de Pago:\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTexto("CLIENTE:   " + impCliente.nombre + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTexto("NEGOCIO:   " + impCliente.razonSocial + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO:    " + impCliente.codigo + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("RUC:       " + impCliente.ruc + "    \r\n",TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("DIRECCION: " + impCliente.direccion + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		//
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		if(usuario.impresionRecaudo.equals("SI")){
			
			cargarTexto("              DETALLE RECAUDO             \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			Vector<FormaPago> listaDetalleFP = DataBaseBO.getDetalleRecaudo( NumeroFacturaVenta );
			cargarTexto("FACTURA                         VALOR     \r\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
			cargarDetalleRecaudo(listaDetalleFP);
			
			
		}
		
		
		cargarTexto("              FORMAS DE PAGO              \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		//
		Vector<FormaPago> listaDetalleFP = DataBaseBO.getImpresionRecaudo( NumeroFacturaVenta );
		cargarDetallesFormaPago(listaDetalleFP);
	}
	
	
	private void cargarDetallesFormaPago(Vector<FormaPago> listaDetalleFP) {
		
		/*Variables para montos totales*/
		float totalMonto = 0;

		/* Dar formato a cada detalle del pedido. */
		for (FormaPago formaPago : listaDetalleFP) {
			
			formatearTextoFormaPago(formaPago);
			cargarTexto("FORMA DE PAGO: " + "\r\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
			cargarTexto(formaPago.descripcion + formaPago.strMonto + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			totalMonto += formaPago.monto;  	
		}
		cargarTexto("---------------------------------------\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
//		
		String SrtMonto  = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(totalMonto)),2)));
		/*Formatear totales.*/
		SrtMonto = Util.lpad(SrtMonto, TAM_MAX_TOTAL_UNIT, " ");
		
		cargarTexto("      T. Recaudo:      " + SrtMonto + "\r\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTexto("      T. Formas:       " + SrtMonto + "\r\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTexto("---------------------------------------\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Recibo de caja sujeto a aprobacion del \r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("departamento de cartera.\r\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		
	}

	public void generarResumenRecaudo(Usuario usuario) {

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		inicializarImpresora();
		characterSizeSetNormal();

		/* limpiar el buffer */
		this.woosimPrinter.clearSpool();

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();
		characterSpacingDefault();
		String titulo = usuario.codigoVendedor.substring(0, 1).equals("R") ? "SOCIEDAD DE ALIMENTOS DE PRIMERA S.A" : "PRODUCTOS ALIMENTICIOS PASCUAL S.A";
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);

		
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		cargarTextoCentro(cabeceraTirilla.get(0).empresa+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).nit+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).direccion+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).telefono+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).texto+"\r\n\n",	TAMANO_TEXTO_ALTO, NEGRILLA);


		cargarTexto("FECHA:            " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("VENDEDOR:         " + usuario.codigoVendedor + " - " + usuario.nombreVendedor + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("COBRANZA\r\n",			TAMANO_TEXTO_NORMAL, NORMAL);
		//
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("              FORMAS DE PAGO              \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("------------------------------------------\r\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		//
		
		Vector<FormaPago> listaDetalleFP = DataBaseBO.getAllFormaPago();
		cargarResumenDetalleFormaPagoGeneral(listaDetalleFP);
		
	}
	
	
	/**
	 * Metodo permite la impresion del encabezado que llevan todas las tirillas.
	 * @param original 
	 * @param fechaVenta 
	 * 
	 * @param NumeroFacturaVenta,
	 *            numero de la factua, debe coincidir con la resolucion DIAN y
	 *            estar vigente.
	 */
	public void generarInformeZeta(final Usuario usuario, final boolean isInformeZeta) {
		

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		inicializarImpresora();
		characterSizeSetNormal();

		/* limpiar el buffer */
		this.woosimPrinter.clearSpool();

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();
		characterSpacingDefault();
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		cargarTextoCentro(cabeceraTirilla.get(0).empresa+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).nit+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).direccion+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).telefono+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).texto+"\r\n\n",	TAMANO_TEXTO_ALTO, NEGRILLA);


		cargarTexto("Serial:    123"+"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Hora:" + Util.FechaActual("HH:mm:ss") + "     Fecha:"+Util.FechaActual("yyyy-MM-dd")+"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		
		if(isInformeZeta)
			cargarTexto("REPORTE Z#: 1007"+"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		else
			cargarTexto("Liquidacion Diaria \r\n\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
		
		cargarTexto("VEND:" + usuario.codigoVendedor + " - " + usuario.nombreVendedor + "\r\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("Resumen de venta:\r\n\n",TAMANO_TEXTO_ALTO, NEGRILLA);
		EstadisticaRecorrido estadisticaRecorrido = DataBaseBO.getInformeResumenVenta();

		estadisticaRecorrido.str_venta_contado = Util.SepararMiles(Util.round(Util.QuitarE("" + format.format(estadisticaRecorrido.venta_contado)), 2));
		String devolucion_contado = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.dev_contado))), 2));
		String venta_credito = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.venta_credito))), 2));
		String devolucion_credito = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.dev_credito))), 2));
		String totalGlobal = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(estadisticaRecorrido.totalGlobal))), 2));
		
		cargarTexto("Ventas Contado   " + estadisticaRecorrido.str_venta_contado + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Devs Contado     " + devolucion_contado + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Ventas Credito   " + venta_credito + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Devs Credito     " + devolucion_credito + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Total Ventas     " + totalGlobal + "\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		
		cargarTexto("Liquidacion de valores" +  "\r\n\n",TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTexto("Ventas Contado" +  "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		
		Vector<FormaPago> listaFormaPago = DataBaseBO.getImpresionVentaContado();
		
		float totalVenta = 0;
		
		FormaPago efectivo = new FormaPago();
		efectivo.descripcion="Efectivo";
		efectivo.monto = estadisticaRecorrido.venta_contado  - estadisticaRecorrido.dev_contado;
		formatearTextoVentas(efectivo);
		cargarTexto(efectivo.descripcion+efectivo.strMonto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		for (FormaPago formaPago : listaFormaPago) {
			
			formatearTextoVentas(formaPago);
			cargarTexto(formaPago.descripcion+formaPago.strMonto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			totalVenta += formaPago.monto;
		}
		
		totalVenta += efectivo.monto;
		String Str_Total = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(totalVenta)), 2));
		cargarTexto("TOTAL                           "+Str_Total + "\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);

		cargarTexto("Cobranza" +  "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		Vector<FormaPago> listaFormaPagoCredito = DataBaseBO.getImpresionVentaCredito();
		
		float totalCredito= 0;
		for (FormaPago formaPago : listaFormaPagoCredito) {
			
			formatearTextoVentas(formaPago);
			cargarTexto(formaPago.descripcion+formaPago.strMonto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			totalCredito += formaPago.monto;
		}
		String str_Total_Credito = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(totalCredito)), 2));
		cargarTexto("TOTAL                           "+ str_Total_Credito + "\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
		
		
		cargarTexto("Total de valores a liquidar" +  "\r\n\n",TAMANO_TEXTO_ALTO, NEGRILLA);
		Vector<FormaPago> listaInformeLiquidacion = DataBaseBO.getImpresionLiquidacion();
		
		float totalALiquidar= 0;
		
		float totalCobranza = DataBaseBO.getTotalConbranza();
		
		FormaPago fp = new FormaPago();
		fp.descripcion="Efectivo";
		fp.monto = (estadisticaRecorrido.venta_contado  - estadisticaRecorrido.dev_contado) + totalCobranza;
		formatearTextoVentas(fp);
		cargarTexto(fp.descripcion+fp.strMonto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		for (FormaPago formaPago : listaInformeLiquidacion) {
			
			formatearTextoVentas(formaPago);
			cargarTexto(formaPago.descripcion+formaPago.strMonto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			totalALiquidar += formaPago.monto;
		}
		
		totalALiquidar+= fp.monto;
		String strTotalALiquidar= Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(totalALiquidar)), 2));
		cargarTexto("TOTAL" + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
		cargarTexto("A ENTREGAR                      "+  strTotalALiquidar + "\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
		
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("#DOC             GRAV.           NO.GR    \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("                 TP ITEM         ITEM     \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("Facturas Contado" + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
		
		Vector<ImpresionEncabezado> listaEncabezadoFacturaCon = DataBaseBO.getImpresionEncabezadoContado();
		double totalValorDescuento  = 0;
		double totalvalorNeto  = 0;
		
			for (ImpresionEncabezado factura : listaEncabezadoFacturaCon) {
				
				if(factura.anulado == 0 ){
					fomatearTextoFacturas(factura);
					cargarTexto(factura.numero_doc+factura.str_descuento+factura.str_valor_neto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
					totalValorDescuento+= factura.valor_descuento;
					totalvalorNeto += factura.valor_neto;
				}
			}
				
			String strTotalDescuento  = Util.SepararMiles(Util.round(Util.QuitarE("" + format.format(totalValorDescuento)), 2));
			String strTotalNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
			cargarTexto("TOTAL         " + strTotalDescuento + "      "+strTotalNeto+"\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
			
			cargarTexto("Facturas Credito" + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
			
			Vector<ImpresionEncabezado> listaEncabezadoFacturaCre = DataBaseBO.getImpresionEncabezadoCredito();
			totalValorDescuento  = 0;
			totalvalorNeto  = 0;
			
				for (ImpresionEncabezado factura : listaEncabezadoFacturaCre) {
					
					if(factura.anulado == 0 ){
						fomatearTextoFacturas(factura);
						cargarTexto(factura.numero_doc+factura.str_descuento+factura.str_valor_neto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
						totalValorDescuento+= factura.valor_descuento;
						totalvalorNeto += factura.valor_neto;
					}
				}
					
				strTotalDescuento  = Util.SepararMiles(Util.round(Util.QuitarE("" + format.format(totalValorDescuento)), 2));
				strTotalNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
				cargarTexto("TOTAL         " + strTotalDescuento + "      "+strTotalNeto+"\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
			
			
			cargarTexto("Facturas Anuladas" + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
			
			Vector<ImpresionEncabezado> listaEncabezadoFacturaAnulados = DataBaseBO.getImpresionEncabezadoAnulado();
			totalValorDescuento  = 0;
			totalvalorNeto  = 0;
			
				for (ImpresionEncabezado factura : listaEncabezadoFacturaAnulados) {
					
					if(factura.anulado == 1 ){
						fomatearTextoFacturas(factura);
						cargarTexto(factura.numero_doc+factura.str_descuento+factura.str_valor_neto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
						totalValorDescuento+= factura.valor_descuento;
						totalvalorNeto += factura.valor_neto;
					}
				}
					
				String strTotalDescuentoAnulado  = Util.SepararMiles(Util.round(Util.QuitarE("" + format.format(totalValorDescuento)), 2));
				String strTotalNetoAnulado = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
				cargarTexto("TOTAL         " + strTotalDescuentoAnulado + "      "+strTotalNetoAnulado+"\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
				
				
				
				cargarTexto("Recaudos" + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
				
				totalValorDescuento = 0;
				totalvalorNeto = 0;
				
				Vector<ImpresionRecaudo> listaRecaudo = DataBaseBO.getInformeRecaudo();
				cargarTexto("Cliente     Num Doc.             Total    \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
				
					for (ImpresionRecaudo factura : listaRecaudo) {
							
							fomatearTextoRecaudo(factura);
							cargarTexto(factura.codigo+factura.numero_doc+factura.str_valor_neto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
							totalvalorNeto += factura.total;
							
					}
						
					strTotalNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
					cargarTexto("TOTAL                            "+strTotalNeto+"\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
					
					
					cargarTexto("Notas Credito" + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
					
					totalValorDescuento = 0;
					totalvalorNeto = 0;
					
					Vector<ImpresionNotaCredito> listaNotasCredito = DataBaseBO.getInformeNotasCredito();
					
						for (ImpresionNotaCredito factura : listaNotasCredito) {
								
								fomatearTextoNtaCredito(factura);
								cargarTexto(factura.numero_doc+factura.strValorSinIva+factura.str_valor_neto+"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
								totalvalorNeto += factura.valor_neto;
								totalValorDescuento += factura.valorSinIva;
								
						}
							
						strTotalNeto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalvalorNeto))), 2));
						strTotalDescuento = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(totalValorDescuento))), 2));
						cargarTexto("TOTAL                 "+ strTotalDescuento+"      " + strTotalNeto+"\r\n\n",TAMANO_TEXTO_NORMAL, NEGRILLA);	
						
						
						if(isInformeZeta){
							
							Vector<FormaPago> listaGranTotal = DataBaseBO.getImpresionGranTotal();
							
							cargarTexto("ACUMULADO A LA FECHA" +  "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
							
							for (FormaPago formaPago : listaGranTotal) {
								
								formatearTextoGranTotal(formaPago);
								cargarTexto(formaPago.descripcion+formaPago.strMonto +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
							}
							
						}
						
						for (int i = 0; i < 6; i++) {
							cargarTexto("\r\n",TAMANO_TEXTO_NORMAL, NORMAL);	
						}
						

	}



	private void fomatearTextoFacturas(ImpresionEncabezado factura) {
		
		factura.str_descuento  = Util.SepararMiles(Util.round(Util.QuitarE("" + format.format(factura.valor_descuento)), 2));
		factura.str_valor_neto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_neto))), 2));
		factura.numero_doc = Util.rpad(factura.numero_doc, 14, " ");
		factura.str_descuento = Util.rpad(factura.str_descuento, 12, " ");
		factura.str_valor_neto = Util.rpad(factura.str_valor_neto, 12, " ");
		
	}

	/**
	 * Metodo permite formar los detalles que seran impresos. alineados de
	 * acuerdo a la impresora.
	 * 
	 * @param detalleImprimir
	 */
	public void cargarDetallesVenta(Vector<ImpresionDetalle> listaDetalles, boolean isPedido, Usuario usuario, String NumeroFacturaVenta, String codCliente) {

		/* Variables para montos totales */
		double subTotalImp = 0;
		double descuentoImp = 0;
		double ivaImp = 0;
		double total = 0;
		double valorDevolucion = 0;
		boolean existeAmarre = false;

		/* Dar formato a cada detalle del pedido. */
		for (ImpresionDetalle detalle : listaDetalles) {
			detalle.total = detalle.precio * detalle.cantidad;
			double sub_total = 0;
			double valor_descuento = 0;
			double valor_iva = 0;

			sub_total = detalle.total;
			valor_descuento = sub_total * detalle.descuento / 100;
			valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100);

			subTotalImp += sub_total;
			descuentoImp += valor_descuento;
			ivaImp += valor_iva;
			formatearTexto(detalle, false);
			cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto(detalle.cantidadStr + detalle.precioStr + detalle.totalStr + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			if (!isPedido)
				cargarTexto(detalle.descMotivo + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

		}
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);

		String strSubTotal = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(subTotalImp)), 2)));
		String strDescuento = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(descuentoImp)), 2)));
		String strIva = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(ivaImp)), 2)));
		String strDevolucion  = "";
		
		if (isPedido) {

			if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
				existeAmarre = DataBaseBO.existeAmarreConDev(NumeroFacturaVenta, codCliente, valorDevolucion);
				valorDevolucion = DataBaseBO.obtenerValorDevAmarre(NumeroFacturaVenta, codCliente);

				System.out.println("ValorDevolucion por fuera del DB: " + valorDevolucion);
			}

			strDevolucion = String.valueOf(format.format(valorDevolucion));

			total = usuario.tipoVenta.equals(Const.AUTOVENTA) ? (subTotalImp + ivaImp - descuentoImp) - valorDevolucion
					: subTotalImp + ivaImp - descuentoImp;

		}
		
		
		String strTotal = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(total)), 2)));
		/* Formatear totales. */
		
		strIva = Util.lpad(strIva, TAM_MAX_TOTALES, " ");
		strDescuento = Util.lpad(strDescuento, TAM_MAX_TOTALES, " ");
		strDevolucion = Util.lpad(strDevolucion, TAM_MAX_TOTALES, " "); 
		strTotal = Util.lpad(strTotal, TAM_MAX_TOTALES, " ");
		
		if(usuario.tipoVenta.equals(Const.AUTOVENTA)){
			cargarTexto("      TOTAL:    " + strTotal + "\r\n\n", TAMANO_TEXTO_ALTO, NEGRILLA);
		}
		
		cargarTexto("      SUBTOTAL: " + strSubTotal + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("      IVA:     " + strIva + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("      DESCUENTO:" + strDescuento + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		
		if(usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido){
			
			cargarTexto("      N.CREDITO:" + strDevolucion + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			
		}else{
			cargarTexto("      TOTAL:    " + strTotal + "\r\n", TAMANO_TEXTO_ALTO, NEGRILLA);
		}
		
		cargarTexto("------------------------------------------\r\n\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		 
		
		if(usuario.tipoVenta.equals(Const.AUTOVENTA) && isPedido){
			
			cargarTexto("C A N A S T I L L A S"+"\r\n",TAMANO_TEXTO_NORMAL, NORMAL) ;
			cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto("Codigo            Descripcion             \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto("      cant. Ent               Cant. Dev   \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			
			Vector<RegCanastilla> vCanastilla = DataBaseBO.getImpresionCanastillas( NumeroFacturaVenta, false );

			RegCanastilla canastilla;

			if ( vCanastilla.size() > 0 ) {
				Enumeration< RegCanastilla > elements = vCanastilla.elements();

				while (elements.hasMoreElements()) {

					canastilla = elements.nextElement();
					
					formatearTextoDetalleCanasta(canastilla, false);
					cargarTexto(canastilla.codigo + canastilla.descripcion + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
					cargarTexto(canastilla.entregada +  canastilla.devuelta + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

				}
			}
			cargarTexto("------------------------------------------\r\n\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		}
		
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		
	}
	
	/**
	 * Metodo permite dar el maximo tama�o de caracteres permitidos para cada
	 * columna sin perder la forma. (maximo tama�o permitido para la impresora
	 * WSP-R240 = 384 dots = 42 caracteres maximo, 1 caracter = 9 dots )
	 * 
	 * @param detalle
	 */
	private void formatearTextoVentas(FormaPago formaPago) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas en el encabezado
		 */
		formaPago.strMonto  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(formaPago.monto))), 2));
		formaPago.descripcion = Util.rpad(formaPago.descripcion, TAM_MAX_PRODUCTO, " ");
		formaPago.strMonto = Util.rpad(formaPago.strMonto, TAM_MAX_CODIGO, " ");
		
		
	}
	
	private void formatearTextoGranTotal(FormaPago formaPago) {

		
		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas en el encabezado
		 */
		
		if(!formaPago.descripcion.equals("PROXIMA FACTURA"))
			formaPago.strMonto  = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(formaPago.monto))), 2));
		else
			formaPago.strMonto= String.valueOf((int)formaPago.monto);
		

		formaPago.descripcion = Util.rpad(formaPago.descripcion, TAM_MAX_PRODUCTO, " ");
		formaPago.strMonto = Util.rpad(formaPago.strMonto, TAM_MAX_CODIGO, " ");
		
	}
	
	
	private void fomatearTextoRecaudo(ImpresionRecaudo factura ) {
		
//		factura.str_valor_neto = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.sub_total))), 2));
		factura.str_valor_neto = ""+factura.sub_total;
		factura.codigo = Util.rpad(factura.codigo, 12, " ");
		factura.numero_doc = Util.rpad(factura.numero_doc, 21, " ");
		factura.str_valor_neto = Util.rpad(factura.str_valor_neto, 9, " ");
		
	}
	
	private void fomatearTextoNtaCredito(ImpresionNotaCredito factura ) {
		
		factura.str_valor_neto= Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valor_neto))), 2));
		factura.strValorSinIva = Util.SepararMiles(Util.round(Util.QuitarE(String.valueOf(format.format(factura.valorSinIva))), 2));
		factura.numero_doc = Util.rpad(factura.numero_doc, 22, " ");
		factura.strValorSinIva = Util.rpad(factura.strValorSinIva, 12, " ");
		factura.str_valor_neto = Util.rpad(factura.str_valor_neto, 8, " ");
		
	}
	

	/**
	 * Imprime un texto de prueba.
	 */
	public void testPrinter() {
		inicializarImpresora();
		this.woosimPrinter.clearSpool();
		characterSpacingDefault();
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
	}
	
	/**
	 * Devuelve el tipo de Liquidacion
	 */
	public String cargarTipoLiquidacion(int tipoLiquidacion) {
		
		String tipo = "";

		switch (tipoLiquidacion) {
		case Const.CANASTILLA_VACIA:
			tipo = "CANASTAS VACIAS";
			break;
			
		case Const.INVENTARIO_CANASTA:
			tipo = "INVENTARIO CANASTAS";
			break;
			
		case Const.IS_DESCARGA_CANASTA:
			tipo = "DESCARGA CANASTAS";
			break;

		default:
			tipo = "SIN TIPO DE LIQUIDACION";
			break;
		}
		
		return tipo;
	}
	
	
	/**
	 * Metodo permite la impresion de la liquidacion de canastillas.
	 * @param original 
	 * @param fechaVenta 
	 *
	 */
	public void generarCanastillaLiquidacionTirilla(int opcionLiquidar,final Usuario usuario,String numeroDoc, Usuario liquidador) {

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();
		

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();
		characterSpacingDefault();
		
		String titulo =""; 
		String subtitulo = cargarTipoLiquidacion(opcionLiquidar);
		
		titulo = "#"+subtitulo+" - "+numeroDoc;
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto(titulo + "\r\n\r\n\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("FECHA:" + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("LIQUIDADOR: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto(liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		
		
		cargarTexto("CLIENTE:   " + usuario.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("NEGOCIO:   " + usuario.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO:    " + usuario.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("RUC:       " + usuario.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("DIRECCION: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
			
			
			if (opcionLiquidar == Const.INVENTARIO_CANASTA) {
				

				cargarTexto("CODIGO          DESCRIPCION               \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
				cargarTexto("   TEORICO        FISICO           DIF    \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
				cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);


			} else {
				
				cargarTexto("Codigo            Descripcion             \r\n", TAMANO_TEXTO_NORMAL, NORMAL);
				cargarTexto("      cant. Ent                           \r\n", TAMANO_TEXTO_NORMAL, NORMAL);
				cargarTexto("------------------------------------------\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

			}

			
			
			if (opcionLiquidar == Const.INVENTARIO_CANASTA){
				
				Vector<ImpresionDetalle> vCanastilla;
				vCanastilla = DataBaseBO.getImpresionCanastillasInv(numeroDoc, true);
				ImpresionDetalle detalle;
				
				if (vCanastilla.size() > 0) {
					Enumeration<ImpresionDetalle> elements = vCanastilla.elements();

					while (elements.hasMoreElements()) {

						detalle = elements.nextElement();

						formatearTextoLiquidacion(detalle);
						cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
						cargarTexto(detalle.strInventario + detalle.strDigitada+detalle.strDiferencia+"\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

					}
				}
				
			}else{
				
				Vector<RegCanastilla> vCanastilla;
				 vCanastilla = DataBaseBO.getImpresionCanastillas(numeroDoc, true);
				 RegCanastilla canastilla;
				 
				 if (vCanastilla.size() > 0) {
						Enumeration<RegCanastilla> elements = vCanastilla.elements();

						while (elements.hasMoreElements()) {

							canastilla = elements.nextElement();

							formatearTextoDetalleCanasta(canastilla, true);
							cargarTexto(canastilla.codigo + canastilla.descripcion + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
							cargarTexto(canastilla.devuelta + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

						}
					}
				 }

			

			
			cargarTexto("------------------------------------------\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);

		}
		
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
	
	}
	
	/**
	 * Metodo permite la impresion del encabezado que llevan todas las tirillas.
	 * @param original 
	 * @param fechaVenta 
	 * 
	 * @param NumeroFacturaVenta,
	 *            numero de la factua, debe coincidir con la resolucion DIAN y
	 *            estar vigente.
	 */
	public void generarTirillaPedidoLiquidacion(final String nroDoc, final Usuario liquidador, final Usuario user, int opcionLiquidar) {

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();	
		characterSpacingDefault();
		
		String titulo = "";
		
		if(opcionLiquidar == Const.PRODUCTO_DANADO)
			titulo = "# PRODUCTO DANADO: "+nroDoc;
		else
			titulo = "# REGISTRO INVENTARIO: "+nroDoc;
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto(titulo + "\r\n\r\n\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("FECHA:" + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("LIQUIDADOR: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto(liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		
		
		cargarTexto("CLIENTE:   " + user.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("NEGOCIO:   " + user.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO:    " + user.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("RUC:       " + user.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("DIRECCION: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		//
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO          DESCRIPCION               \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("   TEORICO        FISICO           DIF    \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		//
		
		boolean isDanado = false;
		if(opcionLiquidar == Const.PRODUCTO_DANADO){
			isDanado = true;
		}	
		
		Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleLiquidacion(user.codigoVendedor, isDanado);
		cargarDetallesPedidoLiquidacion(listaDetalles);
	}
	
	
	
	/**
	 * Metodo permite la impresion del inventario de producto
	 * @param original 
	 * @param fechaVenta 
	 * 
	 * @param NumeroFacturaVenta,
	 *            numero de la factua, debe coincidir con la resolucion DIAN y
	 *            estar vigente.
	 */
	public void generarTirillaInventarioDeProducto(final String nroDoc, final Usuario liquidador, final Usuario user, int opcionLiquidar) {

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();

		characterSet(); // caracteres para idioma español.

		lf();
		ff();	
		characterSpacingDefault();
		
		
		String tituloEnc =""; 
		String subtitulo ="";
		
		if (user.codigoVendedor.substring(0, 1).equals("R"))
			tituloEnc = "SOCIEDAD DE ALIMENTOS DE PRIMERA S.A";
		else
			tituloEnc = "PRODUCTOS ALIMENTICIOS PASCUAL S.A";
		
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		cargarTextoCentro(cabeceraTirilla.get(0).empresa+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).nit+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).direccion+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).telefono+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).texto+"\r\n\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		
		String titulo = "# INVENTARIO DE PRODUCTO: " + nroDoc;
		
				
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto(titulo + "\r\n\r\n\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("FECHA:" + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("LIQUIDADOR: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto(liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		
		
		cargarTexto("CLIENTE:   " + user.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("NEGOCIO:   " + user.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO:    " + user.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("RUC:       " + user.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("DIRECCION: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		//
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO          DESCRIPCION               \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("   TEORICO        FISICO           DIF    \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		//
		
			
		Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleLiquidacionInventarioProducto(user.codigoVendedor);
		cargarDetallesPedidoLiquidacionInventarioProductos(listaDetalles);
	}

	
	
	/**
	 * Metodo permite formar los detalles que seran impresos. alineados de
	 * acuerdo a la impresora.
	 * 
	 * @param detalleImprimir
	 */
	public void cargarDetallesPedidoLiquidacionInventarioProductos(Vector<ImpresionDetalle> listaDetalles) {


		/* Dar formato a cada detalle del pedido. */
		for (ImpresionDetalle detalle : listaDetalles) {

			formatearTextoLiquidacion(detalle);
			cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto(detalle.strInventario + detalle.strDigitada + detalle.strDiferencia + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

		}
		cargarTexto("------------------------------------------\r\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		
	}


	/**
	 * Metodo permite formar los detalles que seran impresos. alineados de
	 * acuerdo a la impresora.
	 * 
	 * @param detalleImprimir
	 */
	public void cargarDetallesPedidoLiquidacion(Vector<ImpresionDetalle> listaDetalles) {


		/* Dar formato a cada detalle del pedido. */
		for (ImpresionDetalle detalle : listaDetalles) {

			formatearTextoLiquidacion(detalle);
			cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto(detalle.strTransaccion + detalle.strDigitada + detalle.strDiferencia + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

		}
		cargarTexto("------------------------------------------\r\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
	}
	
	/**
	 * Metodo permite la impresion del encabezado que llevan todas las tirillas.
	 * @param original 
	 * @param transaccion 
	 * @param fechaVenta 
	 * 
	 * @param NumeroFacturaVenta,
	 *            numero de la factua, debe coincidir con la resolucion DIAN y
	 *            estar vigente.
	 */
	public void generarEncabezadoTirillaProductos(final String NumeroFacturaVenta, final Usuario usuario, String original, int transaccion) {
	
		Impresion impCliente = null;
		impCliente = DataBaseBO.getImpresionClienteLiquidador( NumeroFacturaVenta );
		boolean hayDatos = false;
		
		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();
		characterSpacingDefault();
		
		String titulo =""; 
		String subtitulo ="";
		
		if (usuario.codigoVendedor.substring(0, 1).equals("R"))
			titulo = "SOCIEDAD DE ALIMENTOS DE PRIMERA S.A";
		else
			titulo = "PRODUCTOS ALIMENTICIOS PASCUAL S.A";
		
		if(original.equals(""))
			subtitulo = "Copia ";
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTexto("" + "\r\n"+ subtitulo+"\r\n\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		
		ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
		
		cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
		cargarTextoCentro(cabeceraTirilla.get(0).empresa+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).nit+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).direccion+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).telefono+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(cabeceraTirilla.get(0).texto+"\r\n\n",	TAMANO_TEXTO_ALTO, NEGRILLA);

		if(DataBaseBO.ObtenerUsuario().tipoVenta.equals(Const.AUTOVENTA) && transaccion != Const.IS_SOBRANTE)
		    cargarTexto("Numero Factura:    " + NumeroFacturaVenta + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		else
		    cargarTexto("Numero:    " + NumeroFacturaVenta + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		cargarTexto("FECHA:            " + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("VENDEDOR:         " + usuario.codigoVendedor + " - " + usuario.nombreVendedor + "\r\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
			
		cargarTexto("Cond. de Pago:\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
		
		if(impCliente!= null){
			
			cargarTexto("CLIENTE:   " + impCliente.nombre + "\r\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
			cargarTexto("NEGOCIO:   " + impCliente.razonSocial + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto("CODIGO:    " + impCliente.codigo + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

			cargarTexto("RUC:       " + impCliente.ruc + " \r\n",TAMANO_TEXTO_ALTO, NORMAL);

			cargarTexto("DIRECCION: " + impCliente.direccion + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			
		}

		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO      DESCRIPCION                   \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CANTIDAD       PRECIO                TOTAL\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleLiquidacionProducto(NumeroFacturaVenta);
		cargarDetallesPedidoProductoLiquidados(listaDetalles, usuario, NumeroFacturaVenta);
		
	}

	/**
	 * Metodo permite formar los detalles que seran impresos. alineados de
	 * acuerdo a la impresora.
	 * 
	 * @param detalleImprimir
	 */
	public void cargarDetallesPedidoProductoLiquidados(Vector<ImpresionDetalle> listaDetalles, Usuario usuario, String NumeroFacturaVenta) {

		/* Variables para montos totales */
		double subTotalImp = 0;
		double descuentoImp = 0;
		double ivaImp = 0;
		double total = 0;

		/* Dar formato a cada detalle del pedido. */
		for (ImpresionDetalle detalle : listaDetalles) {
			
				detalle.total = detalle.precio * detalle.cantidad;
			
			double sub_total = 0;
			double valor_descuento = 0;
			double valor_iva = 0;

			sub_total = detalle.total;
			valor_descuento = sub_total * detalle.descuento / 100;
			valor_iva = (sub_total - valor_descuento) * (detalle.iva / 100);

			subTotalImp += sub_total;
			descuentoImp += valor_descuento;
			ivaImp += valor_iva;
			formatearTexto(detalle, false);
			cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto(detalle.cantidadStr + detalle.precioStr + detalle.totalStr + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

		}
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);

		String strSubTotal = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(subTotalImp)), 2)));
		String strDescuento = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(descuentoImp)), 2)));
		String strIva = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(ivaImp)), 2)));
		String strDevolucion  =  Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(0)), 2)));
		total =  subTotalImp + ivaImp - descuentoImp;
			
		String strTotal = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(total)), 2)));
		/* Formatear totales. */
		
		strIva = Util.lpad(strIva, TAM_MAX_TOTALES, " ");
		strDescuento = Util.lpad(strDescuento, TAM_MAX_TOTALES, " ");
		strDevolucion = Util.lpad(strDevolucion, TAM_MAX_TOTALES, " "); 
		strTotal = Util.lpad(strTotal, TAM_MAX_TOTALES, " ");
		strSubTotal = Util.lpad(strSubTotal, TAM_MAX_TOTALES, " ");

		cargarTexto("      SUBTOTAL: " + strSubTotal + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("      IVA:     " + strIva + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("      N.CREDITO:" + strDevolucion + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("      DESCUENTO:" + strDescuento + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("      TOTAL:    " + strTotal + "\r\n\n", TAMANO_TEXTO_ALTO, NEGRILLA);

		
		cargarTexto("------------------------------------------\r\n\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		 
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
		
	}

	public String formatoInventarioDiaSewoo() {
		
		String aux;		
		String strPrint; 
		char ret1 = 13;
		char ret2 = 10;
		char ESC  = 27;
		char XON  = 17;
		char GS = 29;
		char a = 64;
		
		/*Character font B(9x17) - 42 caracteres por linea*/
		char neg = 9; //negrilla
		char nor = 1; // normal
		String initialize = String.valueOf(ESC) + String.valueOf(a);
		String normal   = String.valueOf(ESC) + String.valueOf('!') + String.valueOf(nor);
		String negrilla = String.valueOf(ESC) + String.valueOf('!') + String.valueOf(neg);
		String enter = String.valueOf(ret1) + String.valueOf(ret2);
		
		
		
		strPrint = initialize + enter;	
		strPrint += negrilla + enter;
		strPrint += "Inventario" + enter;
		strPrint += "Vendedor: " + Main.usuario.codigoVendedor + enter;
		strPrint += "Fecha: " + Util.FechaActual("yyyy-MM-dd") + enter;
		strPrint += normal;
		strPrint += Util.lpad("", 42, "-") + enter;

		strPrint +=  Util.rpad("COD", 12, " ") + Util.rpad("PRODUCTO", 30, " ") + enter;
		strPrint +=  Util.rpad("", 12, " ") + Util.rpad("INICIAL", 12, " ")  + Util.rpad("VENTAS", 11, " ")+Util.rpad("DIF", 10, " ") + enter;

		strPrint += Util.lpad("", 42, "-") + enter;

		Vector<InformeInventario> lInformeInv = DataBaseBO.CargarInformeInventario(false);
		/*Agregar las canastas al informe de inventario*/
		DataBaseBO.aregarInformeCanastas(lInformeInv);
		InformeInventario infoInv;
		TotalInventarioDia totalInventarioDia = new TotalInventarioDia();
		DataBaseBO.calcularTotalesInventarioDia(totalInventarioDia);
		
		for( int i = 0; i < lInformeInv.size(); i++ ){
			infoInv = lInformeInv.elementAt( i );
			strPrint += Util.rpad( infoInv.codigo, 12, " ") + Util.rpad( "" + infoInv.nombre, 30, " ") + enter;
			strPrint += Util.lpad("" + Util.round(infoInv.invInicial+"",1), 16, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 12, " ")  +Util.lpad("" + Util.round(String.valueOf(infoInv.invActual-infoInv.cantVentas),1), 14, " ") + enter;
		}
		strPrint += Util.lpad("", 42, "-") + enter;
		strPrint += negrilla;
		/*Agrear lineas de totalizados*/
		strPrint +=  Util.rpad("Unidades ii: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalUnidadesII()), 20, " ") + enter;
		strPrint +=  Util.rpad("Valor cargue: ", 22, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(totalInventarioDia.getValorCargue()), 2)), 20, " ") + enter;
		strPrint +=  Util.rpad("Total canastillas: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalCanastillas()), 20, " ") + enter;
		strPrint += Util.lpad("", 42, "-") + enter;
		for (int i = 0; i < 2; i++) {
			strPrint += enter;
		}
		return strPrint;
	}
	
	/**
	 * Metodo permite la impresion del encabezado que llevan todas las tirillas.
	 * @param original 
	 * @param fechaVenta 
	 * 
	 * @param NumeroFacturaVenta,
	 *            numero de la factua, debe coincidir con la resolucion DIAN y
	 *            estar vigente.
	 */
	public void generarTirillaAveriaLiquidacion(final String nroDoc, final Usuario liquidador, final Usuario user, int opcionLiquidar, boolean isAveria) {

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();	
		characterSpacingDefault();
		
		String titulo = "";
		
		if(isAveria)
			titulo = "# Averias Transporte: "+nroDoc;
		else
			titulo = "# Compensaciones: "+nroDoc;
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto(titulo + "\r\n\r\n\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("FECHA:" + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("LIQUIDADOR: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto(liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		
		
		cargarTexto("CLIENTE:   " + user.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("NEGOCIO:   " + user.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO:    " + user.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("RUC:       " + user.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("DIRECCION: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		//
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO          DESCRIPCION               \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("                  Cantidad                \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		//
		
		boolean isDanado = false;
		if(opcionLiquidar == Const.PRODUCTO_DANADO){
			isDanado = true;
		}	
		
		Vector<ImpresionDetalle> listaDetalles = DataBaseBO.getImpresionDetalleAveria(nroDoc, user.codigoVendedor, isDanado);
		cargarDetallesAveriaLiquidacion(listaDetalles);
	}
	
	/**
	 * Metodo permite formar los detalles que seran impresos. alineados de
	 * acuerdo a la impresora.
	 * 
	 * @param detalleImprimir
	 */
	public void cargarDetallesAveriaLiquidacion(Vector<ImpresionDetalle> listaDetalles) {


		/* Dar formato a cada detalle del pedido. */
		for (ImpresionDetalle detalle : listaDetalles) {

			formatearTextoLiquidacionAveria(detalle);
			cargarTexto(detalle.codigo + detalle.nombre + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			cargarTexto(detalle.strDigitada + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

		}
		cargarTexto("------------------------------------------\r\n\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
	}
	
	
	/**
	 * Metodo permite la impresion de la liquidacion de canastillas.
	 * @param original 
	 * @param fechaVenta 
	 *
	 */
	public void generarImpresionLiquidacion(int opcionLiquidar, Usuario usuario, Usuario liquidador, String tipo) {

		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();
		

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();
		characterSpacingDefault();
		
		String titulo =""; 
		String subtitulo ="";

		if(opcionLiquidar == Const.IS_PRODUCTO_DANADO)
			subtitulo = "PRODUCTO DANADO";
		else
			subtitulo = "INVENTARIO";
		
		
		titulo = "# "+subtitulo;
		
		cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto(titulo + "\r\n\r\n\n",			TAMANO_TEXTO_ALTO, NORMAL);
		cargarTexto("FECHA:" + Util.FechaActual("yyyy-MM-dd HH:mm:ss") + "\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("LIQUIDADOR: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto(liquidador.codigoVendedor + " - " + liquidador.nombreVendedor +"\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		
		
		cargarTexto("CLIENTE:   " + usuario.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("NEGOCIO:   " + usuario.nombreVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CODIGO:    " + usuario.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("RUC:       " + usuario.codigoVendedor + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("DIRECCION: \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		
		if (usuario.tipoVenta.equals(Const.AUTOVENTA)) {
			
			String and ="";
			

				cargarTexto("CODIGO          DESCRIPCION               \r\n",TAMANO_TEXTO_NORMAL, NORMAL);
				cargarTexto("FISICO          INV. FINAL      DIFERENCIA\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
				cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
				
				
				Vector<Producto> vCanastilla;
				if(opcionLiquidar == Const.IS_PRODUCTO_DANADO || opcionLiquidar == Const.IS_INVENTARIO_LIQUIDACION)
					and = " AND diferencia <> 0 ";
				
				
				vCanastilla = DataBaseBO.listaProductosLiquidados(tipo, and);

				
				Producto detalle;
				
				if (vCanastilla.size() > 0) {
					Enumeration<Producto> elements = vCanastilla.elements();

					while (elements.hasMoreElements()) {

						detalle = elements.nextElement();

						formatearTextoLiquidacionLiq(detalle);
						cargarTexto(detalle.codigo + detalle.descripcion + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
						cargarTexto(detalle.strDigitada + detalle.strInventario+detalle.strDiferencia+"\r\n", TAMANO_TEXTO_NORMAL, NORMAL);

					}
				}
		
			
			cargarTexto("------------------------------------------\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);

		}
		
		
		cargarTexto("Aceptada Cliente.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("Firma.\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
		cargarTexto("CC.\r\n\n\n", TAMANO_TEXTO_NORMAL, NORMAL);
	
	}

	public void formatoInventarioDiaSewoo(boolean isLiquidacion) {
		
		/*
		 * Estimar un tiempo de espera para el hilo de sincronizacion bluetooth
		 */
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		inicializarImpresora();
		characterSizeSetNormal();
		

		characterSet(); // caracteres para idioma espa�ol.

		lf();
		ff();
		characterSpacingDefault();
		
		String titulo =""; 
		String subtitulo ="";

		
			Usuario usuario = DataBaseBO.ObtenerUsuario();
			String varAux = usuario.codigoVendedor.substring(0, 1);
			if (varAux.equals("R"))
				titulo = "Sociedad de alimentos de primera S.A";
			else {
				titulo = "Productos Alimenticios Pascual S.A";
			}
			


				cargarTextoCentro("         \r\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
				ArrayList<CabeceraTirilla> cabeceraTirilla = new ArrayList<CabeceraTirilla>();
				
				cabeceraTirilla= DataBaseBO.cargarCabeceraTirilla();
				cargarTextoCentro(cabeceraTirilla.get(0).empresa+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
				cargarTextoCentro(cabeceraTirilla.get(0).nit+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
				cargarTextoCentro(cabeceraTirilla.get(0).direccion+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
				cargarTextoCentro(cabeceraTirilla.get(0).telefono+"\r\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
				cargarTextoCentro(cabeceraTirilla.get(0).texto+"\r\n\n",	TAMANO_TEXTO_ALTO, NEGRILLA);
			

			
			cargarTexto("Inventario " + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			cargarTexto("Vendedor   " + usuario.codigoVendedor + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			cargarTexto("Fecha      " + Util.FechaActual("yyyy-MM-dd") + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			
			cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
			
			cargarTexto("COD         PRODUCTO                      " + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			
			if(!isLiquidacion)
				cargarTexto("            INICIAL     VENTAS            " + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			else
				cargarTexto("II         V          M          IF       \r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			
			cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);

	

		Vector<InformeInventario> lInformeInv = DataBaseBO.CargarInformeInventario(isLiquidacion);
		/*Agregar las canastas al informe de inventario*/
		DataBaseBO.aregarInformeCanastas(lInformeInv);
		InformeInventario infoInv;
		TotalInventarioDia totalInventarioDia = new TotalInventarioDia();
		DataBaseBO.calcularTotalesInventarioDia(totalInventarioDia);
		
		for( int i = 0; i < lInformeInv.size(); i++ ){
			
			infoInv = lInformeInv.elementAt( i );
			cargarTexto(Util.rpad( infoInv.codigo, 12, " ") + Util.rpad( "" + infoInv.nombre, 30, " ") + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			
			if(!isLiquidacion)
				cargarTexto(Util.lpad("" + Util.round(infoInv.invInicial+"",1), 16, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 12, " ")  + Util.rpad("", 14, " ")  + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			else
				cargarTexto(Util.lpad(Util.round(infoInv.invInicial+"",1), 11, " ") + Util.lpad("" + Util.round(String.valueOf(infoInv.cantVentas),1), 11, " ")  +Util.lpad("" + Util.round(String.valueOf(infoInv.sobrante),1), 11, " ")  + Util.rpad("" + Util.round(String.valueOf(infoInv.invActual),1), 10, " ")  + "\r\n",TAMANO_TEXTO_NORMAL, NEGRILLA);
			
		}
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
		/*Agrear lineas de totalizados*/
		
		cargarTextoCentro( Util.rpad("Unidades ii: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalUnidadesII()), 20, " ")+"\r\n\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro( Util.rpad("Valor cargue: ", 22, " ") + Util.rpad(Util.SepararMiles(Util.round(String.valueOf(totalInventarioDia.getValorCargue()), 2)), 20, " ")+"\r\n\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTextoCentro(  Util.rpad("Total canastillas: ", 22, " ") + Util.rpad(String.valueOf(totalInventarioDia.getTotalCanastillas()), 20, " ")+"\r\n\n",			TAMANO_TEXTO_ALTO, NEGRILLA);
		cargarTexto("------------------------------------------\r\n",TAMANO_TEXTO_NORMAL, NORMAL);
	}
	
	
	private void cargarDetalleRecaudo(Vector<FormaPago> listaDetalleFP) {
		
		/*Variables para montos totales*/
		float totalMonto = 0;

		/* Dar formato a cada detalle del pedido. */
		for (FormaPago formaPago : listaDetalleFP) {
			
			formatearTextoDetalleRecaudo(formaPago);

			cargarTexto(formaPago.factura + formaPago.strMonto + "\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
			totalMonto += formaPago.monto;  	
		}
		cargarTexto("---------------------------------------\r\n", TAMANO_TEXTO_NORMAL, NORMAL);
//		
		String SrtMonto  = Util.SepararMiles(Util.QuitarE(Util.round(String.valueOf(format.format(totalMonto)),2)));
		cargarTexto(Util.rpad("TOTAL RECAUDO:", TAM_MAX_PRODUCTO, " ") + Util.rpad(SrtMonto, TAM_MAX_VALOR_UNIT, " ") + "\r\n", TAMANO_TEXTO_NORMAL, NEGRILLA);
		
	}
	
	/**
	 * Metodo permite dar el maximo tama�o de caracteres permitidos para cada
	 * columna sin perder la forma. (maximo tama�o permitido para la impresora
	 * WSP-R240 = 384 dots = 42 caracteres maximo, 1 caracter = 9 dots )
	 * 
	 * @param detalle
	 */
	private void formatearTextoFactura(ImpresionDetalle detalle, boolean isCargue) {

		/*
		 * Formatear el texto de cada detalle segun las definiciones de columnas en el encabezado
		 */
		detalle.nombre = Util.rpad(detalle.nombre, TAM_MAX_PRODUCTO, " ");
		detalle.codigo = Util.rpad(detalle.codigo, TAM_MAX_CODIGO, " ");
		
//		float descuentoXProducto = 0;
//		descuentoXProducto= (detalle.precio*detalle.cantidad)*(detalle.descuento/100);
		
		detalle.cantidadStr = Util.rpad("  " + Util.round(String.valueOf(detalle.cantidad), 0), 13, " ");

		detalle.precioStr =   Util.rpad(Util.SepararMiles(Util.round(String.valueOf(format.format(detalle.precio)),2)), 11, " ");
		detalle.descStr =   Util.rpad(Util.SepararMilesSin(Util.round(String.valueOf(format.format(detalle.descuento)),2)), 5, " ");
		detalle.totalStr =    Util.lpad(Util.SepararMiles(Util.round(String.valueOf(format.format(detalle.total)),2)), 11, " ");
	}

}