package celuweb.com.fonts;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;

import me.anwarshahriar.calligrapher.Calligrapher;
public class Font {
    private static final String ttf_file = "montserrat_regular.ttf";
    private static final String path = "fonts/montserrat/";
    private static Typeface typeface = null;
    
    public static void setCalligrapher(final Context context) {
        typeface = Typeface.createFromAsset(context.getAssets(), path + ttf_file);
        if (typeface != null && context!=null) {
            Activity contextActivity = (Activity)context;
            Calligrapher calligrapher = new Calligrapher(contextActivity);
            calligrapher.setFont(contextActivity, path + ttf_file, true);
        }
    }
    
    public static Typeface getFont(final Context context) {
        if (typeface == null && context!=null) {
            typeface = Typeface.createFromAsset(context.getAssets(), path + ttf_file);
        }
        return typeface;
    }
}
