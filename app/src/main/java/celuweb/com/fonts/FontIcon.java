package celuweb.com.fonts;

import android.content.Context;
import android.graphics.Typeface;

import com.mikepenz.iconics.typeface.IIcon;
import com.mikepenz.iconics.typeface.ITypeface;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;


public class FontIcon implements ITypeface {
    private static final String TTF_FILE = "celuweb.ttf";

    private static Typeface typeface = null;

    private static HashMap<String, Character> mChars;

    @Override
    public IIcon getIcon(String key) {
        return Icon.valueOf(key);
    }

    @Override
    public HashMap<String, Character> getCharacters() {
        if (mChars == null) {
            HashMap<String, Character> aChars = new HashMap<String, Character>();
            for (Icon v : Icon.values()) {
                aChars.put(v.name(), v.character);
            }
            mChars = aChars;
        }

        return mChars;
    }

    @Override
    public String getMappingPrefix() {
        return "clw";
    }

    @Override
    public String getFontName() {
        return "Celuweb";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public int getIconCount() {
        return mChars.size();
    }

    @Override
    public Collection<String> getIcons() {
        Collection<String> icons = new LinkedList<String>();

        for (Icon value : Icon.values()) {
            icons.add(value.name());
        }

        return icons;
    }

    @Override
    public String getAuthor() {
        return "Celuweb";
    }

    @Override
    public String getUrl() {
        return "https://celuweb.com";
    }

    @Override
    public String getDescription() {
        return "FontIcon Celuweb";
    }

    @Override
    public String getLicense() {
        return "SIL OFL 1.1";
    }

    @Override
    public String getLicenseUrl() {
        return "http://scripts.sil.org/OFL";
    }

    @Override
    public Typeface getTypeface(Context context) {
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/icons/" + TTF_FILE);
            } catch (Exception e) {
                return null;
            }
        }
        return typeface;
    }

    public static enum Icon implements IIcon {
        clw_pedido_('\uf92b'),
        clw_estadisticas('\uf934'),
        clw_consultar_precios('\uf92d'),
        clw_rutero('\uf931'),
        clw_inventario_cliente('\uf96c'),
        clw_cliente_nuevo('\uf929'),
        clw_cartera('\uf925'),
        clw_map('\uf97b'),
        clw_no_compra('\uf97c'),
        clw_recaudo('\uf987'),
        clw_devoluciones('\uf930'),
        clw_inventario_canasta('\uf96f'),
        clw_editar('\uf932'),
        clw_efectividad('\uf933'),
        clw_informe_inventario('\uf96a'),
        clw_acoplar_imp('\uf908'),
        clw_iniciar_dia('\uf96b'),
        clw_regresar('\uf988'),
        clw_cargue('\uf923'),
        clw_terminar_dia('\ue999'),
        clw_enviar_info('\ue934'),
        clw_planograma('\uf915'),
        clw_search('\uf9dd');

        char character;

        Icon(char character) {
            this.character = character;
        }

        public String getFormattedName() {
            return "{" + name() + "}";
        }

        public char getCharacter() {
            return character;
        }

        public String getName() {
            return name();
        }

        // remember the typeface so we can use it later
        private static ITypeface typeface;

        public ITypeface getTypeface() {
            if (typeface == null) {
                typeface = new FontIcon();
            }
            return typeface;
        }
    }
}
