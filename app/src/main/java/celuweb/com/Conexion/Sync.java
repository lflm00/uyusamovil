package celuweb.com.Conexion;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.util.Log;

import celuweb.com.BusinessObject.ConfigBO;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Config;
import celuweb.com.DataObject.Coordenada;
import celuweb.com.uyusa.Const;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.Sincronizador;
import celuweb.com.uyusa.Util;

@SuppressWarnings("deprecation")
public class Sync extends Thread {
  
  private final static String TAG = "Conexion.Sync";
  
  boolean ok; // Indica si la Respuesta del Servidor fue OK o ERROR
  int codeRequest; // Indica el tipo de codeRequest a Sincronizar (Login,
  // DowloadDataBase, ....)
  Sincronizador sincronizador; // Clase que se encarga de procesar la
  // Respuesta del Servidor cuando finaliza el
  // proceso de Sincronizacion.
  
  public String usuario;
  public String clave;
  
  public String version;
  public String imei;
  
  public String url;
  public int cantTramas = 15;
  public String termino = "0";
  
  public int timeout = 2 * 60 * 1000; // 2 Minutos de Timeout para descargar y
  // enviar informacion!
  public int timeoutOne = 60 * 1000; // 1 Minuto de Timeout para Iniciar
  // Sesion con el Servidor y Terminar
  // labores
  public int time20=20000;
  public String cadenaEnviar = "";
  
  public Coordenada coordenada;
  
  public String codigoVerificar;
  
  /* DESCARGAR PDF */
  public String URL;
  public String nombreArchivo;
  
  /**
   Guarda el mensaje de la ejecucion de la sincrionizacion Puede ser un
   mensaje de Ok o de Error
   **/
  String mensaje;
  
  /**
   Guarda la respuesta del servidor, Puede ser un mensaje de Ok o de Error
   **/
  String respuestaServer = "";
  
  /**
   Contructor de la clase. Sincronizador es una interfaz que deben
   implementar las Actividades que sincronizan datos
   
   Cuando se termina la sincronizacion, se llama el metodo RespSync de la
   actividad que invoco el Sync con el estatus de la Sincronizacion
   **/
  public Sync(Sincronizador sincronizador, int codeRequest) {
    this.sincronizador = sincronizador;
    this.codeRequest = codeRequest;
  }
  
  public void run() {
    switch (codeRequest) {
      case Const.LOGIN:
        LogIn();
        break;
      case Const.DOWNLOAD_DATA_BASE:
        DownloadDataBase();
        break;
      case Const.ACTUALIZAR_INVENTARIO:
        UpdateInventarioDataBase();
        break;
      case Const.ENVIAR_PEDIDO:
      case Const.ENVIAR_PEDIDO_TERMINAR:
        EnviarPedido1();
        break;
      case Const.DOWNLOAD_VERSION_APP:
        DownloadVersionApp();
        break;
      case Const.TERMINAR_LABORES:
        TerminarLabores();
        break;
      case Const.ENVIAR_COORDENADAS:
        enviarCoordenadaPost();
        break;
      case Const.VERIFICAR_CLIENTE:
        verificarCliente();
        break;
      case Const.DESCARGAR_ARCHIVO_PDF:
        downloadFile();
        break;
      
    }
  }
  
  public void LogIn() {
    boolean ok = false;
    respuestaServer = "";
    HttpURLConnection urlConnection = null;
    try {
      String strURL = Const.URL_SYNC + "login.aspx?un=" + this.usuario + "&pw=" + this.clave;
      Log.i("LogIn", "URL Login: " + strURL);
      URL url = new URL(strURL);
      urlConnection = (HttpURLConnection) url.openConnection();
      BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
      int statusCode = urlConnection.getResponseCode();
      if (statusCode == HttpURLConnection.HTTP_OK) {
        /*****************************************************************
         * Si la Solicitud al Servidor fue Satisfactoria, lee la
         * Respuesta
         *****************************************************************/
        String line = null;
        while ((line = reader.readLine()) != null) {
          respuestaServer += line;
        }
        if (respuestaServer.startsWith("ok")) {
          ok = true;
          mensaje = "Login realizado con exito";
          
        } else {
          mensaje = "No se pudo Iniciar Sesion.\n" + respuestaServer;
        }
        
      } else if (statusCode == -1) {
        mensaje = "No se pudo Iniciar Sesion. Pagina No Encontrada: LogIn.aspx";
        
      } else {
        mensaje = MensajeHttpError(statusCode, "LogIn.aspx");
      }
      
    } catch (Exception e) {
      String msg = e.getMessage();
      if (msg.startsWith("http://")) msg = "\n\nMotivo: Pagina No Encontrada: LogIn.aspx";
      else msg = "\n\nMotivo: " + msg;
      mensaje = "No se pudo establecer la conexion con el servidor." + msg;
      
    } finally {
      if (urlConnection != null) urlConnection.disconnect();
    }
    sincronizador.RespSync(ok, respuestaServer, mensaje, codeRequest);
  }
  
  public void DownloadDataBase() {
    boolean ok = false;
    InputStream inputStream = null;
    FileOutputStream fileOutput = null;
    HttpURLConnection urlConnection = null;
    try {
      /************************************
       * Carga la Configuracion del Usuario.
       ************************************/
      Config config = ConfigBO.ObtenerConfigUsuario();
      if (config != null) {
        Locale locale = Locale.getDefault();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd@HH_mm_ss", locale);
        String fechaMovil = dateFormat.format(new Date());
        String urlDataBase = Const.URL_SYNC + "CrearDB.aspx?v=" + config.usuario + "&bg=" + config.bodega + "&fe=" + fechaMovil + "&lo=" + locale + "&vr=" + version + "&i=" + imei;
        Log.i(TAG, "URL DB = " + urlDataBase);
        URL url = new URL(urlDataBase);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setRequestProperty("Cache-Control", "no-cache");
        urlConnection.setRequestProperty("Pragma", "no-cache");
        urlConnection.setRequestProperty("Expires", "-1");
        urlConnection.setDoInput(true);
        /**
         * SE DEFINE EL TIEMPO DE ESPERA, MAXIMO ESPERA 2 MINUTOS
         **/
        urlConnection.setConnectTimeout(timeout);
        urlConnection.setReadTimeout(timeout);
        urlConnection.connect();
        inputStream = urlConnection.getInputStream();
        String contentDisposition = urlConnection.getHeaderField("Content-Disposition");
        if (contentDisposition != null) { // Viene Archivo Adjunto
          /**
           * Se obtiene la ruta del SD Card, para guardar la Base de
           * Datos. Y se crea el Archivo de la BD
           **/
          String fileName = "Temporal.zip";
          File file = new File(Util.DirApp(), fileName);
          if (file.exists()) file.delete();
          if (file.createNewFile()) {
            fileOutput = new FileOutputStream(file);
            long downloadedSize = 0;
            int bufferLength = 0;
            byte[] buffer = new byte[1024];
            /**
             * SE LEE LA INFORMACION DEL BUFFER Y SE ESCRIBE EL
             * CONTENIDO EN EL ARCHIVO DE SALIDA
             **/
            while ((bufferLength = inputStream.read(buffer)) > 0) {
              fileOutput.write(buffer, 0, bufferLength);
              downloadedSize += bufferLength;
            }
            fileOutput.flush();
            fileOutput.close();
            inputStream.close();
            long content_length = Util.ToLong(urlConnection.getHeaderField("content-length"));
            if (content_length == 0) {
              mensaje = "No hay conexion, por favor intente de nuevo";
              
            } else if (content_length != downloadedSize) { // La
              // longitud
              // de
              // descarga
              // no es
              // igual
              // al
              // Content
              // Length
              // del
              // Archivo
              mensaje = "No se pudo descargar la base de datos, por favor intente de nuevo";
              
            } else {
              Descomprimir(file);
              ok = true;
              mensaje = "Descargo correctamente la Base de Datos";
            }
            
          } else {
            mensaje = "No se pudo crear el archivo de la Base de Datos";
          }
          
        } else { // No hay archivo adjunto, se procesa el Mensaje de
          // respuesta del Servidor
          BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
          String line = null;
          while ((line = reader.readLine()) != null) {
            respuestaServer += line;
          }
          if (respuestaServer.equals("")) mensaje = "No se pudo descargar la base de datos, por favor intente de nuevo.";
          else mensaje = respuestaServer;
        }
        
      } else {
        Log.i("DownloadDataBase", "Falta establecer la configuracion del Usuario");
        mensaje = "Por favor, primero ingrese la configuracion del usuario";
      }
      
    } catch (Exception e) {
      String motivo = e.getMessage();
      if (motivo.startsWith("http://")) motivo = "Pagina no Encontrada: CrearDB.aspx";
      mensaje = "No se pudo descargar la Base de Datos\n\n";
      mensaje += "Motivo: " + motivo;
      Log.e(TAG, "DownloadDataBase: " + e.getMessage(), e);
      
    } finally {
      try {
        if (fileOutput != null) fileOutput.close();
        if (inputStream != null) inputStream.close();
        
      } catch (IOException e) {
      }
      if (urlConnection != null) urlConnection.disconnect();
    }
    sincronizador.RespSync(ok, mensaje, mensaje, codeRequest);
  }

  public void UpdateInventarioDataBase() {
    boolean ok = false;
    InputStream inputStream = null;
    FileOutputStream fileOutput = null;
    HttpURLConnection urlConnection = null;
    try {
      /************************************
       * Carga la Configuracion del Usuario.
       ************************************/
      Config config = ConfigBO.ObtenerConfigUsuario();
      if (config != null) {
        Locale locale = Locale.getDefault();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd@HH_mm_ss", locale);
        String fechaMovil = dateFormat.format(new Date());
        String urlDataBase = Const.URL_SYNC + "CrearDB.aspx?v=" + config.usuario + "&bg=" + config.bodega + "&fe=" + fechaMovil + "&lo=" + locale + "&vr=" + version + "&i=" + imei;
        Log.i(TAG, "URL DB = " + urlDataBase);
        URL url = new URL(urlDataBase);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setRequestProperty("Cache-Control", "no-cache");
        urlConnection.setRequestProperty("Pragma", "no-cache");
        urlConnection.setRequestProperty("Expires", "-1");
        urlConnection.setDoInput(true);
        /**
         * SE DEFINE EL TIEMPO DE ESPERA, MAXIMO ESPERA 2 MINUTOS
         **/
        urlConnection.setConnectTimeout(timeout);
        urlConnection.setReadTimeout(timeout);
        urlConnection.connect();
        inputStream = urlConnection.getInputStream();
        String contentDisposition = urlConnection.getHeaderField("Content-Disposition");
        if (contentDisposition != null) { // Viene Archivo Adjunto
          /**
           * Se obtiene la ruta del SD Card, para guardar la Base de
           * Datos. Y se crea el Archivo de la BD
           **/
          String fileName = "Temporal.zip";
          File file = new File(Util.DirApp(), fileName);
          if (file.exists()) file.delete();
          if (file.createNewFile()) {
            fileOutput = new FileOutputStream(file);
            long downloadedSize = 0;
            int bufferLength = 0;
            byte[] buffer = new byte[1024];
            /**
             * SE LEE LA INFORMACION DEL BUFFER Y SE ESCRIBE EL
             * CONTENIDO EN EL ARCHIVO DE SALIDA
             **/
            while ((bufferLength = inputStream.read(buffer)) > 0) {
              fileOutput.write(buffer, 0, bufferLength);
              downloadedSize += bufferLength;
            }
            fileOutput.flush();
            fileOutput.close();
            inputStream.close();
            long content_length = Util.ToLong(urlConnection.getHeaderField("content-length"));
            if (content_length == 0) {
              mensaje = "No hay conexion, por favor intente de nuevo";

            } else if (content_length != downloadedSize) { // La
              // longitud
              // de
              // descarga
              // no es
              // igual
              // al
              // Content
              // Length
              // del
              // Archivo
              mensaje = "No se pudo descargar la base de datos, por favor intente de nuevo";

            } else {
              Descomprimir(file);
              ok = true;
              mensaje = "Descargo correctamente la Base de Datos";
            }

          } else {
            mensaje = "No se pudo crear el archivo de la Base de Datos";
          }

        } else { // No hay archivo adjunto, se procesa el Mensaje de
          // respuesta del Servidor
          BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
          String line = null;
          while ((line = reader.readLine()) != null) {
            respuestaServer += line;
          }
          if (respuestaServer.equals("")) mensaje = "No se pudo descargar la base de datos, por favor intente de nuevo.";
          else mensaje = respuestaServer;
        }

      } else {
        Log.i("DownloadDataBase", "Falta establecer la configuracion del Usuario");
        mensaje = "Por favor, primero ingrese la configuracion del usuario";
      }

    } catch (Exception e) {
      String motivo = e.getMessage();
      if (motivo.startsWith("http://")) motivo = "Pagina no Encontrada: CrearDB.aspx";
      mensaje = "No se pudo descargar la Base de Datos\n\n";
      mensaje += "Motivo: " + motivo;
      Log.e(TAG, "DownloadDataBase: " + e.getMessage(), e);

    } finally {
      try {
        if (fileOutput != null) fileOutput.close();
        if (inputStream != null) inputStream.close();

      } catch (IOException e) {
      }
      if (urlConnection != null) urlConnection.disconnect();
    }
    sincronizador.RespSync(ok, mensaje, mensaje, codeRequest);
  }
  
  public void TerminarLabores() {
    ok = false;
    mensaje = "";
    HttpURLConnection urlConnection = null;
    /************************************
     * Carga la Configuracion del Usuario.
     ************************************/
    Config config = ConfigBO.ObtenerConfigUsuario();
    if (config != null) {
      try {
        Locale locale = Locale.getDefault();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd@HH_mm_ss", locale);
        String fechaMovil = dateFormat.format(new Date());
        String strURL = Const.URL_SYNC + "TerminarLabores.aspx?un=" + config.usuario + "&fe=" + fechaMovil + "&i=" + this.imei;
        Log.i(TAG, "URL TerminarLabores = " + strURL);
        URL url = new URL(strURL);
        urlConnection = (HttpURLConnection) url.openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        int statusCode = urlConnection.getResponseCode();
        if (statusCode == HttpURLConnection.HTTP_OK) {
          /*****************************************************************
           * Si la Solicitud al Servidor fue Satisfactoria, lee la
           * Respuesta
           *****************************************************************/
          String line = null;
          while ((line = reader.readLine()) != null) {
            respuestaServer += line;
          }
          if (respuestaServer.startsWith("ok")) {
            ok = true;
            DataBaseBO.ActulizarTerminoLabores();
            mensaje = "Terminar Labores Satisfactorio";
            
          } else {
            mensaje = respuestaServer;
          }
          
        } else if (statusCode == -1) {
          mensaje = "No se pudo Terminar Labores. Pagina No Encontrada: TerminarLabores.aspx";
          
        } else {
          mensaje = MensajeHttpError(statusCode, "TerminarLabores.aspx");
        }
        
      } catch (Exception e) {
        String msg = e.getMessage();
        if (msg.startsWith("http://")) msg = "\n\nMotivo: Pagina No Encontrada: TerminarLabores.aspx";
        else msg = "\n\nMotivo: " + msg;
        mensaje = "No se pudo establecer la conexion con el servidor." + msg;
      }
      
    } else {
      Log.i(TAG, "TerminarLabores: Falta establecer la configuracion del Usuario");
      mensaje = "Por favor, primero ingrese la configuracion del usuario";
    }
    sincronizador.RespSync(ok, respuestaServer, mensaje, codeRequest);
  }



	/*
   * public void UploadPage(String infoEnviar, String tabla) {
	 * 
	 * ok = false; mensaje = ""; String tramaEnviar;
	 * 
	 * infoEnviar = infoEnviar.replace(' ', '@'); tramaEnviar = "&enviar=" +
	 * infoEnviar + "&term=0&fechaLabor=" + Main.usuario.fechaLabores;
	 * 
	 * GetUrl(tabla); url = url + tramaEnviar; Log.i("UploadPage", "url = " +
	 * url);
	 * 
	 * try {
	 * 
	 * HttpClient httpClient = new DefaultHttpClient(); HttpContext localContext
	 * = new BasicHttpContext(); HttpGet httpGet = new HttpGet(url);
	 * 
	 * HttpResponse response = httpClient.execute(httpGet, localContext);
	 * BufferedReader reader = new BufferedReader(new
	 * InputStreamReader(response.getEntity().getContent()));
	 * 
	 * String line = null; while ((line = reader.readLine()) != null) {
	 * 
	 * respuestaServer += line + "\n"; }
	 * 
	 * if (respuestaServer.indexOf("ok") > -1) {
	 * 
	 * ok = true;
	 * 
	 * //if (codeRequest == Const.REGISTRAR_NOVEDAD) //
	 * DataBaseBO.ActualizarSyncNovedad(encabezado.numero_doc);
	 * 
	 * } else {
	 * 
	 * mensaje = respuestaServer; }
	 * 
	 * } catch (Exception e) {
	 * 
	 * ok = false; mensaje = "Error conectando con el servidor " +
	 * e.getMessage(); }
	 * 
	 * sincronizador.RespSync(ok, respuestaServer, mensaje, codeRequest); }
	 */
  
  public void Descomprimir(File fileZip) {
    try {
      if (fileZip.exists()) {
        String nameFile = fileZip.getName().replace(".zip", "");
        File fileZipAux = new File(Util.DirApp(), nameFile);
        if (!fileZipAux.exists()) fileZipAux.delete();
        FileInputStream fin = new FileInputStream(fileZip);
        ZipInputStream zin = new ZipInputStream(fin);
        ZipEntry ze = null;
        while ((ze = zin.getNextEntry()) != null) {
          Log.v("Descomprimir", "Unzipping " + ze.getName());
          if (ze.isDirectory()) {
            dirChecker(ze.getName());
            
          } else {
            String pathFile = Util.DirApp() + "/" + ze.getName();
            File file = new File(pathFile);
            FileOutputStream fout = new FileOutputStream(file);
            // long bytes = 0;
            int bufferLength = 0;
            byte[] buffer = new byte[1024];
            while ((bufferLength = zin.read(buffer)) > 0) {
              fout.write(buffer, 0, bufferLength);
              // bytes += bufferLength;
            }
            zin.closeEntry();
            fout.flush();
            fout.close();
          }
        }
        zin.close();
      }
      
    } catch (Exception e) {
      Log.e("Descomprimir", e.getMessage(), e);
    }
  }
  
  public void EnviarPedido() {
    termino = "0";
    ok = true;
    mensaje = "";
    enviarInformacion("Encabezado");
    if (ok) {
      enviarInformacion("Detalle");
    }
    if (ok) {
      DataBaseBO.ActualizarSyncDetalle();
      DataBaseBO.ActualizarSyncEncabezado();
    }
    sincronizador.RespSync(ok, respuestaServer, mensaje, codeRequest);
    // sincronizador.RespSync(ok, respuestaServer, "Informacion Recibida por
    // el servidor", codeRequest);
  }
  
  public void EnviarPedido1() {
    ok = false;
    String msg = "";
    if (ComprimirArchivo()) {
      File zipPedido = new File(Util.DirApp(), "Temp.zip");
      // File zipPedido = new File(Util.DirApp(), "Temp.db");
      if (!zipPedido.exists()) {
        Log.i("EnviarPedido", "El archivo Temp.zip no Existe");
        sincronizador.RespSync(ok, "", "El archivo Temp.zip no Existe", codeRequest);
        return;
      }
      DataOutputStream dos = null;
      HttpURLConnection conexion = null;
      BufferedReader bufferedReader = null;
      FileInputStream fileInputStream = null;
      byte[] buffer;
      int maxBufferSize = 1 * 1024 * 1024;
      int bytesRead, bytesAvailable, bufferSize;
      /************************************
       * Carga la Configuracion del Usuario.
       ************************************/
      Config config = ConfigBO.ObtenerConfigUsuario();
      if (config != null) {
        int consecutivo = DataBaseBO.ObtenerConsecutivoVend();
        String urlUpLoad = Const.URL_SYNC + "RegistrarPedido.aspx?un=" + config.usuario + "&termino=0&ext=zip&fechaLabor=" + Main.usuario.fechaLabores + "&co=" + consecutivo;
        Log.i("EnviarPedido", "URL Enviar Info = " + urlUpLoad);
        try {
          URL url = new URL(urlUpLoad);
          conexion = (HttpURLConnection) url.openConnection();
          conexion.setDoInput(true); // Permite Entradas
          conexion.setDoOutput(true); // Permite Salidas
          conexion.setUseCaches(false); // No usar cache
          /**
           * SE ESTABLECEN LOS HEADERS
           **/
          conexion.setRequestMethod("POST");
          conexion.setRequestProperty("Cache-Control", "no-cache");
          conexion.setRequestProperty("Pragma", "no-cache");
          conexion.setRequestProperty("Expires", "-1");
          conexion.setRequestProperty("Connection", "Keep-Alive");
          conexion.setRequestProperty("Content-Type", "multipart/form-data; boundary=---------------------------4664151417711");
          /**
           * SE DEFINE EL TIEMPO DE ESPERA, MAXIMO ESPERA 2 MINUTOS
           **/
          conexion.setConnectTimeout(time20);
          conexion.setReadTimeout(time20);
          /**
           * SE CREA EL BUFFER PARA ENVIAR LA INFORMACION DEL ARCHIVO
           **/
          fileInputStream = new FileInputStream(zipPedido);
          bytesAvailable = fileInputStream.available();
          bufferSize = Math.min(bytesAvailable, maxBufferSize);
          buffer = new byte[bufferSize];
          /**
           * ABRE LA CONEXION DEL FLUJO DE SALIDA. LEE Y ESCRIBE LA
           * INFORMACION DEL ARCHIVO EN EL BUFFER Y ENVIA LA
           * INFORMACION AL SERVIDOR.
           **/
          dos = new DataOutputStream(conexion.getOutputStream());
          bytesRead = fileInputStream.read(buffer, 0, bufferSize);
          while (bytesRead > 0) {
            dos.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
          }
          dos.flush();
          Log.i("EnviarPedido", "Enviando informacion del Archivo");
          /**
           * LEE LA RESPUESTA DEL SERVIDOR
           **/
          bufferedReader = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
          String line;
          respuestaServer = "";
          while ((line = bufferedReader.readLine()) != null) {
            respuestaServer += line;
          }
          if (respuestaServer.startsWith("ok")) {
            ok = true;
            System.out.println("Env�o de info satisfactoriamente, Respuesta es OK, borra temp");
            DataBaseBO.ActualizarSyncTablaEnvio();
            DataBaseBO.ActualizarSyncPedidos();
            DataBaseBO.BorrarInfoTemp();
            
          } else {
            if (respuestaServer.equals("")) msg = "Sin respuesta del servidor";
            else msg = respuestaServer;
          }
          Log.i("EnviarPedido", "respuesta: " + respuestaServer);
          
        } catch (Exception ex) {
          String motivo = ex.getMessage();
          if (motivo != null && motivo.startsWith("http://")) motivo = "Pagina no Encontrada: Registrar Informacion";
          msg = "No se pudo Registrar Informacion";
          if (motivo != null) msg += "\n\nMotivo: " + motivo;
          Log.e("EnviarPedido", msg, ex);
          
        } finally {
          try {
            if (bufferedReader != null) bufferedReader.close();
            if (fileInputStream != null) fileInputStream.close();
            if (dos != null) dos.close();
            if (conexion != null) conexion.disconnect();
            
          } catch (IOException e) {
            Log.e("FileUpLoad", "Error cerrando conexion: " + e.getMessage(), e);
          }
        }
        
      } else {
        Log.i("EnviarPedido", "Falta establecer la configuracion del Usuario");
        mensaje = "Por favor, primero ingrese la configuracion del usuario";
      }
      
    } else {
      msg = "Error comprimiendo la Base de datos Pedido";
      Log.e("FileUpLoad", msg);
    }
    if (respuestaServer.equals("")) respuestaServer = "error, Sin respuesta del servidor";
    sincronizador.RespSync(ok, respuestaServer, msg, codeRequest);
  }



	/*
	 * public void unzip(File fileZip) {
	 * 
	 * try {
	 * 
	 * if (fileZip.exists()) {
	 * 
	 * String nameFile = fileZip.getName().replace(".zip", ""); File dirCatalogo
	 * = new File(Util.DirApp(), nameFile);
	 * 
	 * if (!dirCatalogo.exists()) dirCatalogo.mkdir();
	 * 
	 * FileInputStream fin = new FileInputStream(fileZip); ZipInputStream zin =
	 * new ZipInputStream(fin);
	 * 
	 * ZipEntry ze = null;
	 * 
	 * while ((ze = zin.getNextEntry()) != null) {
	 * 
	 * Log.v("Decompress", "Unzipping " + ze.getName());
	 * 
	 * if(ze.isDirectory()) {
	 * 
	 * dirChecker(ze.getName());
	 * 
	 * } else {
	 * 
	 * String name = ze.getName(); File imagefile = new File( Util.DirApp() +
	 * "/" + name ); FileOutputStream fout = new FileOutputStream( imagefile );
	 * 
	 * long bytes = 0; int bufferLength = 0; byte[] buffer = new byte[1024];
	 * 
	 * while ( (bufferLength = zin.read(buffer)) > 0 ) {
	 * 
	 * fout.write(buffer, 0, bufferLength); bytes += bufferLength; }
	 * 
	 * zin.closeEntry(); fout.flush(); fout.close();
	 * 
	 * String path = imagefile.getPath();
	 * Main.imgCatago.addElement(Drawable.createFromPath(path)); } }
	 * zin.close(); }
	 * 
	 * } catch(Exception e) {
	 * 
	 * Log.e("Decompress", "unzip", e); } }
	 */
  
  public void DownloadVersionApp() {
    boolean ok = false;
    InputStream inputStream = null;
    FileOutputStream fileOutput = null;
    try {
      URL url = new URL(Const.URL_DOWNLOAD_NEW_VERSION);
      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
      Log.i("DownloadVersionApp", "URL App = " + Const.URL_DOWNLOAD_NEW_VERSION);
      // urlConnection.setRequestMethod("GET");
      // urlConnection.setDoInput(true);
      // urlConnection.setDoOutput(true);
      urlConnection.setRequestMethod("GET");
      urlConnection.setRequestProperty("Cache-Control", "no-cache");
      urlConnection.setRequestProperty("Pragma", "no-cache");
      urlConnection.setRequestProperty("Expires", "-1");
      urlConnection.setDoInput(true);
      urlConnection.connect();
      inputStream = urlConnection.getInputStream();
      File file = new File(Util.DirApp(), Const.fileNameApk);
      if (file.exists()) file.delete();
      if (file.createNewFile()) {
        fileOutput = new FileOutputStream(file);
        long downloadedSize = 0;
        int bufferLength = 0;
        byte[] buffer = new byte[1024];
        /**
         * SE LEE LA INFORMACION DEL BUFFER Y SE ESCRIBE EL CONTENIDO EN
         * EL ARCHIVO DE SALIDA
         **/
        while ((bufferLength = inputStream.read(buffer)) > 0) {
          fileOutput.write(buffer, 0, bufferLength);
          downloadedSize += bufferLength;
        }
        fileOutput.flush();
        fileOutput.close();
        inputStream.close();
        long content_length = Util.ToLong(urlConnection.getHeaderField("content-length"));
        if (content_length == 0) {
          ok = false;
          mensaje = "Error de conexion, por favor intente de nuevo";
          
        } else if (content_length != downloadedSize) { // La longitud de
          // descarga no
          // es igual al
          // Content
          // Length del
          // Archivo
          ok = false;
          mensaje = "Error descargando la nueva version, por favor intente de nuevo";
          
        } else {
          ok = true;
          mensaje = "Descargo correctamente la Nueva Version";
        }
        
      } else {
        mensaje = "Error Creando el Archivo de la Nueva Version";
        ok = false;
      }
      
    } catch (Exception e) {
      mensaje = "Error Descargando la Nueva version de la Aplicacion\n";
      mensaje += "Detalle Error: " + e.getMessage();
      Log.e("Sync DownloadVersionApp", e.getMessage(), e);
      ok = false;
      
    } finally {
      try {
        if (fileOutput != null) fileOutput.close();
        if (inputStream != null) inputStream.close();
        
      } catch (IOException e) {
      }
    }
    sincronizador.RespSync(ok, mensaje, mensaje, codeRequest);
  }
  
  private void dirChecker(String dir) {
    File f = new File(Util.DirApp().getPath() + "/" + dir);
    if (!f.isDirectory()) {
      f.mkdirs();
    }
  }
  
  public boolean ComprimirArchivo() {
    File zipPedido = new File(Util.DirApp(), "Temp.zip");
    if (zipPedido.exists()) zipPedido.delete();
    FileOutputStream out = null;
    GZIPOutputStream gZipOut = null;
    FileInputStream fileInputStream = null;
    try {
      File dbFile = new File(Util.DirApp(), "Temp.db");
      if (dbFile.exists()) {
        fileInputStream = new FileInputStream(dbFile);
        int lenFile = fileInputStream.available();
        byte[] buffer = new byte[fileInputStream.available()];
        int byteRead = fileInputStream.read(buffer);
        if (byteRead == lenFile) {
          out = new FileOutputStream(zipPedido);
          gZipOut = new GZIPOutputStream(out);
          gZipOut.write(buffer);
          return true;
        }
        if (zipPedido.exists()) zipPedido.delete();
      }
      return false;
      
    } catch (Exception e) {
      if (zipPedido.exists()) zipPedido.delete();
      Log.e("ComprimirArchivo", e.getMessage(), e);
      return false;
      
    } finally {
      try {
        if (gZipOut != null) gZipOut.close();
        if (out != null) out.close();
        if (fileInputStream != null) fileInputStream.close();
        
      } catch (Exception e) {
        Log.e("ComprimirArchivo", e.getMessage(), e);
      }
    }
  }



	/*
	 * public void GetUrl(String tabla) {
	 * 
	 * if( tabla.equals("Detalle") ) {
	 * 
	 * url = Const.URL_SYNC + "sync3.asp?bodega=00101";
	 * 
	 * } else if( tabla.equals("Novedades") ) {
	 * 
	 * url = Const.URL_SYNC + "Novedades.aspx?un=" + Main.usuario.codigoVendedor
	 * + "&version=" + Main.versionApp; } }
	 */
  
  public String MensajeHttpError(int statusCode, String pagina) {
    switch (statusCode) {
      case HttpStatus.SC_NOT_IMPLEMENTED:
        return "Conexion no Disponible: No Implementado.";
      case HttpStatus.SC_HTTP_VERSION_NOT_SUPPORTED:
        return "Conexion no Disponible: Version No Soportada.";
      case HttpStatus.SC_INTERNAL_SERVER_ERROR:
        return "Conexion no Disponible: Error Interno.";
      case HttpStatus.SC_GATEWAY_TIMEOUT:
        return "Conexion no Disponible: Tiempo de Conexion Excedido.";
      case HttpStatus.SC_BAD_GATEWAY:
        return "Conexion no Disponible: Mala Conexion.";
      case HttpStatus.SC_NOT_FOUND:
        return "Pagina No Encontrada: " + pagina + ".";
      default:
        return "Conexion no Disponible. Http Error Code: " + statusCode + ".";
    }
  }
  
  private void enviarInformacion(String tabla) {
    int i = 1, id = 0, k = 1;
    int j = 0;
    int numRegs = 0;
    int subtotal = 0;
    String enviar = "", parte = "", trama = "";
    Vector<String> tramas = null;
    parte = "unico";
    enviar = "";
    if (tabla.equals("Encabezado")) tramas = DataBaseBO.getTramasEncabezado();
    else if (tabla.equals("Detalle")) tramas = DataBaseBO.getTramasDetalle();
    for (int l = 0; l < tramas.size(); l++) {
      if (enviar.equals("")) {
        enviar = tramas.elementAt(l);
        i++;
        
      } else {
        enviar += "_" + tramas.elementAt(l);
        i++;
      }
      if (i == cantTramas) {
        parte = String.valueOf(k);
        try {
          // loadPageApp( enviar, parte, usuarioG, terminarl, j, tabla
          // );
          UploadPage(enviar, tabla, parte);
        } catch (Exception error) {
          return;
        }
        i = 1;
        k++;
        enviar = "";
        // get_testado2().setText("Iniciando Buffer de Envio......");
      }
      j++;
      // get_testado2().setText( "Preparando Datos......" +
      // String.valueOf( j ) );
    }
    if (k > 1) {
      parte = "final" + String.valueOf(k);
    }
    try {
      // loadPageApp( enviar, parte, usuarioG, terminarl, j, tabla);
      UploadPage(enviar, tabla, parte);
    } catch (Exception error) {
    }
  }
  
  public void UploadPage(String infoEnviar, String tabla, String parte) {
    mensaje = "";
    String tramaEnviar;
    infoEnviar = infoEnviar.replace(' ', '@');
    infoEnviar = infoEnviar.replace('#', '*');
    infoEnviar = infoEnviar.replace(':', '.');
    tramaEnviar = "&enviar=" + infoEnviar + "&parte=" + parte + "&term=" + termino + "&fechaLabor=" + Main.usuario.fechaLabores;
    GetUrl(tabla);
    url = url + tramaEnviar;
    Log.i("Jamg Envio", "url = " + url);
    try {
      HttpClient httpClient = new DefaultHttpClient();
      HttpContext localContext = new BasicHttpContext();
      HttpGet httpGet = new HttpGet(url);
      HttpResponse response = httpClient.execute(httpGet, localContext);
      BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
      String line = null;
      while ((line = reader.readLine()) != null) {
        respuestaServer += line + "\n";
      }
      if (respuestaServer.indexOf("listo2") > -1) {
        ok = true;
        mensaje = "Informacion Recibida por el servidor";
        
      } else if (respuestaServer.indexOf("error") > -1) {
        ok = false;
        mensaje = "Informacion no recibida por el servidor";
      }
      
    } catch (Exception e) {
      ok = false;
      mensaje = "Error conectando con el servidor " + e.getMessage();
    }
  }
  
  public void GetUrl(String tabla) {
    if (tabla.equals("TerminarLabores")) {
      url = Const.URL_SYNC + "TerminarLabores.aspx?un=" + Main.usuario.codigoVendedor.trim();
    }
  }
  
  public void enviarCoordenadaPost() {
    mensaje = "";
    respuestaServer = "";
    boolean ok = false;
    if (coordenada != null && coordenada.codigoVendedor != null) {
      InputStream inputStream = null;
      FileOutputStream fileOutput = null;
      HttpURLConnection urlConnection = null;
      try {
        Locale locale = Locale.getDefault();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd@HH_mm_ss", locale);
        String fechaMovil = dateFormat.format(new Date());
        String url = Const.URL_SYNC + "Coordenadas.aspx?un=" + coordenada.codigoVendedor + "&fe=" + fechaMovil;
        Log.i(TAG, "enviarCoordenadaPost -> url = " + url);
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        String separadorRows = ">>";
        String separadorCols = "@@";
        String tramaCoordenadas = coordenada.codigoCliente + separadorCols + coordenada.latitud + separadorCols + coordenada.longitud + separadorCols + coordenada.horaCoordenada + separadorCols + coordenada.estado + separadorCols + coordenada.id;
        String coordenadas = Coordenada.obtenerCoordenadas(separadorRows, separadorCols);
        if (coordenadas != null && coordenadas.length() > 0) {
          tramaCoordenadas += separadorRows + coordenadas;
        }
        List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair("coordenadas", tramaCoordenadas));
        Log.i("Params ", nameValuePairs.toString());
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();
        StatusLine status = response.getStatusLine();
        int statusCode = status.getStatusCode();
        if (statusCode == HttpURLConnection.HTTP_OK) {
          inputStream = entity.getContent();
          BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
          String line = null;
          while ((line = reader.readLine()) != null) {
            respuestaServer += line;
          }
          if (respuestaServer.equals("ok")) {
            ok = true;
            
          } else {
            if (respuestaServer.equals("")) {
              mensaje = "No se pudo registrar la coordenada";
              
            } else {
              if (respuestaServer.startsWith("<") || respuestaServer.contains("<")) {
                mensaje = "No se pudo registrar la Coordenada\n\nPor favor verifique su conexion a Internet!";
              } else {
                mensaje = respuestaServer;
              }
            }
          }
          
        } else if (statusCode == -1) {
          mensaje = "No se pudo Registrar la Coordenada. Pagina No Encontrada: Coordenadas.aspx";
          
        } else {
          mensaje = mensajeHttpError(statusCode, "Coordenadas.aspx");
        }
        
      } catch (Exception e) {
        String motivo = e.getMessage();
        mensaje = "No se pudo Registrar la Coordenada.";
        Log.e(TAG, "enviarCoordenadaPost -> " + motivo, e);
        if (motivo != null && motivo.startsWith("http://")) {
          motivo = "Pagina no Encontrada: Coordenadas.aspx";
        }
        if (motivo != null) {
          mensaje += "\n\nMotivo: " + motivo;
        }
        
      } finally {
        try {
          if (fileOutput != null) {
            fileOutput.close();
          }
          if (inputStream != null) {
            inputStream.close();
          }
          if (urlConnection != null) {
            urlConnection.disconnect();
          }
          
        } catch (Exception e) {
          Log.e(TAG, "enviarCoordenadaPost -> Exception en el finally", e);
        }
      }
      Log.i(TAG, "enviarCoordenadaPost -> respuestaServer = " + respuestaServer);
      
    } else {
      Log.i(TAG, "enviarCoordenadaPost -> No se pudo leer la Coordenada");
      mensaje = "No se pudo Registrar la Coordenada";
    }
    if (ok) {
      Coordenada.borrarCoordenadas();
    } else {
      Coordenada.guardarCoordenada(coordenada);
    }
    sincronizador.RespSync(ok, mensaje, mensaje, codeRequest);
  }
  
  public String mensajeHttpError(int statusCode, String pagina) {
    switch (statusCode) {
      case HttpStatus.SC_NOT_IMPLEMENTED:
        return "Conexion no Disponible: No Implementado.";
      case HttpStatus.SC_HTTP_VERSION_NOT_SUPPORTED:
        return "Conexion no Disponible: Version No Soportada.";
      case HttpStatus.SC_INTERNAL_SERVER_ERROR:
        return "Conexion no Disponible: Error Interno.";
      case HttpStatus.SC_GATEWAY_TIMEOUT:
        return "Conexion no Disponible: Tiempo de Conexion Excedido.";
      case HttpStatus.SC_BAD_GATEWAY:
        return "Conexion no Disponible: Mala Conexion.";
      case HttpStatus.SC_NOT_FOUND:
        return "Pagina No Encontrada: " + pagina + ".";
      default:
        return "Conexion no Disponible. Http Error Code: " + statusCode + ".";
    }
  }
  
  /**
   permtite una conexion a la web para verificar si un cliente ya esta
   registrado en la base de datos de clientes a nivel nacional.
   */
  private void verificarCliente() {
    boolean ok = false;
    InputStream inputStream = null;
    FileOutputStream fileOutput = null;
    HttpURLConnection urlConnection = null;
    try {
      /************************************
       * Carga la Configuracion del Usuario.
       ************************************/
      Config config = ConfigBO.ObtenerConfigUsuario();
      if (config != null) {
        String urlDataBase = Const.URL_SYNC + "VerificarCliente.aspx?codigo=" + codigoVerificar;
        Log.i(TAG, "URL DB = " + urlDataBase);
        URL url = new URL(urlDataBase);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setRequestProperty("Cache-Control", "no-cache");
        urlConnection.setRequestProperty("Pragma", "no-cache");
        urlConnection.setRequestProperty("Expires", "-1");
        urlConnection.setDoInput(true);
        /**
         * SE DEFINE EL TIEMPO DE ESPERA, MAXIMO ESPERA 2 MINUTOS
         **/
        urlConnection.setConnectTimeout(timeout);
        urlConnection.setReadTimeout(timeout);
        urlConnection.connect();
        inputStream = urlConnection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        while ((line = reader.readLine()) != null) {
          respuestaServer += line;
        }
        if (respuestaServer.equals("")) mensaje = "No se pudo verificar la respuesta del servidor.";
        else {
          mensaje = respuestaServer;
          ok = true;
        }
      }
    } catch (Exception e) {
      String motivo = e.getMessage();
      if (motivo.startsWith("http://")) motivo = "Pagina no Encontrada: VerificarCliente.aspx";
      mensaje = "No se pudo hacer la verficacion del NIT de cliente\n\n";
      mensaje += "Motivo: " + motivo;
      Log.e(TAG, "Verificar Cliente: " + e.getMessage(), e);
      
    } finally {
      try {
        if (fileOutput != null) fileOutput.close();
        if (inputStream != null) inputStream.close();
        
      } catch (IOException e) {
      }
      if (urlConnection != null) urlConnection.disconnect();
    }
    sincronizador.RespSync(ok, mensaje, mensaje, codeRequest);
  }
  
  public void downloadFile() {
    boolean ok = false;
    respuestaServer = "";
    InputStream inputStream = null;
    FileOutputStream fileOutput = null;
    HttpURLConnection urlConnection = null;
    try {
      String urlFile = URL;
      int init = (urlFile).lastIndexOf("http://");
      if (init == -1) {
        urlFile = "http://" + urlFile;
      }
      Log.i(TAG, "downloadFile -> urlDataFile = " + urlFile);
      URL url = new URL(urlFile);
      urlConnection = (HttpURLConnection) url.openConnection();
      urlConnection.setRequestMethod("GET");
      urlConnection.setRequestProperty("Cache-Control", "no-cache");
      urlConnection.setRequestProperty("Pragma", "no-cache");
      urlConnection.setRequestProperty("Expires", "-1");
      urlConnection.setDoInput(true);
      /**
       * SE DEFINE EL TIEMPO DE ESPERA, MAXIMO ESPERA 2 MINUTOS
       **/
      urlConnection.setConnectTimeout(timeout);
      urlConnection.setReadTimeout(timeout);
      urlConnection.connect();
      inputStream = urlConnection.getInputStream();
      int statusCode = urlConnection.getResponseCode();
      if (statusCode == HttpURLConnection.HTTP_OK) {
        /**
         * Se obtiene la ruta del SD Card, para guardar la Base de
         * Datos. Y se crea el Archivo de la BD
         **/
        String fileName = nombreArchivo;
        File file = new File(Util.DirApp().getPath(), "/Manuales/" + fileName);
        if (file.createNewFile()) {
          fileOutput = new FileOutputStream(file);
          long downloadedSize = 0;
          int bufferLength = 0;
          int bufferSize = 4 * 1024;
          byte[] buffer = new byte[bufferSize];
          /**
           * SE LEE LA INFORMACION DEL BUFFER Y SE ESCRIBE EL
           * CONTENIDO EN EL ARCHIVO DE SALIDA
           **/
          while ((bufferLength = inputStream.read(buffer)) > 0) {
            fileOutput.write(buffer, 0, bufferLength);
            downloadedSize += bufferLength;
          }
          fileOutput.flush();
          fileOutput.close();
          inputStream.close();
          long content_length = Util.ToLong(urlConnection.getHeaderField("content-length"));
          if (content_length == 0) {
            mensaje = "No hay conexion, por favor intente de nuevo";
            
          } else if (content_length != downloadedSize) {
            mensaje = "No se pudo descargar el Archivo, por favor intente de nuevo";
            
          } else {
            ok = true;
            mensaje = "Descargo correctamente el Archivo";
          }
          
        } else {
          mensaje = "No se pudo descargar el Archivo";
        }
        
      } else {
        mensaje = mensajeHttpError(statusCode, nombreArchivo);
      }
      
    } catch (ConnectException e) {
      mensaje = "Error de Conexion";
    } catch (Exception e) {
      String motivo = e.getMessage();
      Log.e(TAG, "downloadPDF -> " + motivo, e);
      mensaje = "No se pudo descargar el PDF";
      if (motivo != null) {
        if (motivo.startsWith("http://")) {
          motivo = "URL no Encontrada";
        }
        mensaje += "\n\nMotivo: " + motivo;
      }
      
    } finally {
      try {
        if (fileOutput != null) fileOutput.close();
        if (inputStream != null) inputStream.close();
        
      } catch (IOException e) {
      }
      if (urlConnection != null) urlConnection.disconnect();
    }
    sincronizador.RespSync(ok, mensaje, mensaje, codeRequest);
  }
  
}