package celuweb.com.DataObject;

import java.io.Serializable;

public class Planograma implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public int codigo;
	public String codigoCadena;
	public String codigoCanal;
	public String url;
	public String nombre;
	public String descripcion;
}
