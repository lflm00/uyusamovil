package celuweb.com.DataObject;

import java.io.Serializable;

public class Foto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String id;
	public String codCliente;
	public String codVendedor;
	public int modulo;
	public String nroDoc;
	public byte[] imagen;

	
}

