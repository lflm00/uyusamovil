package celuweb.com.DataObject;


import java.io.Serializable;

public class CabeceraTirilla implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String prefijo;
	public String empresa;
	public String nit;
	public String direccion;
	public String telefono;
	public String texto;

}
