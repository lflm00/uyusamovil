package celuweb.com.DataObject;

import java.io.Serializable;

public class UltimoPedido implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String codCliente;
	public String codProducto;
	public int cantidad;
	public String descProducto;

    public String numeroDoc;
	public String fechaTras;
}
