package celuweb.com.DataObject;

import java.io.Serializable;

public class FotoEncuesta implements Serializable {
	private static final long serialVersionUID = 1L;
	public int codEncuesta;
	public int codPregunta;
	public int codParamPreg;
	public String codCliente;
	public String codVendedor;
	public String numRespuesta;
	public String numDoc;
	public String fechaMovil;
	public String fechaLlegada;
	public byte[] imagen; 
	
}

