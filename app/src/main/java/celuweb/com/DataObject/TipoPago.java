package celuweb.com.DataObject;

import java.io.Serializable;

public class TipoPago implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int codigo;
	public int position;
	public String descripcion;
}
