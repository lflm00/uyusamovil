package celuweb.com.DataObject;

import java.io.Serializable;

public class FormaPago implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int position;
	public String descripcion;
	
	//Datos que se deben Ingresar de la Forma de Pago
	public String nroDoc; 
	public int formaPago;  //1 -> Efectivo 2 -> Cheque
	public double monto;
	
	public String nroCheque;
	public String cuentaCheque;  //Numero de cuenta del cheque
	public String fechaPago;
	public String codigoBanco;
	public String serial; //Id Unico por Cada Forma de Pago
	
	public String nro_cheque_cons; //Este valor aplica para Cheque, Cheque Posfechado y Consigancion
	
	public int codigo;
	//public float valor;
	public String plaza;

	public String strMonto;
	
	public float totalGlobal;
	
	public String factura;
	public String idFoto;

    //public String codigoCliente;
	//public String vendedor;
	//public String bodega; 
}
