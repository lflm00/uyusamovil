package celuweb.com.DataObject;


import java.io.Serializable;

public class Ciudad implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String codigo;
	public String nombre;
	public String direccion;

}
