package celuweb.com.DataObject;

import java.io.Serializable;

public class ResumenSync implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int totalClientes;
	public int totalReferencias;
	public int totalRutero;
	public int totalComplementos;
}
