package celuweb.com.DataObject;

import java.io.Serializable;

public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String mensaje;
	public String bodega;
	
	public String codigoVendedor;
    public String nombreVendedor;
	public String fechaLabores;
	public String fechaConsecutivo;
	public long   pedidoMinimo;
	public String tipoVenta;
	public int tipoRutero;

	public String viajero;

	public String impresionRecaudo;
}
