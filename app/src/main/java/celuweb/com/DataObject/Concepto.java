package celuweb.com.DataObject;

import java.io.Serializable;

public class Concepto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String codigoConcepto;
	public String nombreConcepto;
	public String tipo;
	public String transporte;
	public String obligatorio;

	
}
