package celuweb.com.DataObject;

import java.io.Serializable;

public class CarteraPendiente implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public float total;      //Sumatoria de cartera (Saldo - Abono)
	public int diasVencido;  //Total dias Vencido de la Cartera 
}
