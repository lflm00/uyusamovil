package celuweb.com.DataObject;

import java.io.Serializable;

public class Detalle implements Serializable, Comparable<Detalle> {

	private static final long serialVersionUID = 4L;
	
	
	public String numDoc;     //Codigo del Producto
	public String codCliente;      //Codigo del Cliente
	public String codProducto;     //Codigo del Producto
	public String nombProducto;     
	public long inventario;
	public float precio;               //Precio del Producto
	public String strPrecio;               //Precio del Producto en pantalla
	public float iva;                  //Iva del Producto
	public float descuento; //Descuento autorizado por linea
	public int cantidad;               //Cantidad del producto
	public int tipo_pedido;            //Para efectos del demo solo es 1(Venta)	
	public String codMotivo;  //Codigo del Motivo, solo aplica para Cambios
	public int trasferenciaBodega; //Permite saber si se va a cambiar de bodega o no.
	
	public float descuento_autorizado; //Descuento autorizado por linea
	
	public String desc_producto;       //Descripcion del Producto se usa para mostrar en Pantalla
	
	public String subTotalPrint; //Este subTotal se usa para el detalle de la Impresion del Recibo, este valor es: precio*cantidad
	public int indice;
	
	public String codMotivoCambio;
	public String    descripcionMotivoCambio;
	
	public String codGrupo = "";
	public String tipoGrupo = "";
	public String fecha;
	
	public String vendedor;
	public String motivo;
	public String bodega;
	public int sincronizado;
	public int cantCambio;
	public int item;
	public int tipoCliente;
	public String descripcion;
	public int cantidadCambio;
	
	public int posicion = 0;
	
	public int cantidadUnidades;
	
	
	/************************************
	 * MODULO DE BONIFICACIONES JAUM
	 ************************************/
	public String codigoBonos;
	public int cantidadBonos;
	public String codigoRelacion;
	
	
	public int factorCajas;

	public String observacion;
    public String transfCorreo;
	public String transfCorreo1;
	public String transfCorreo2;
	public String transfCorreo3;
	public String transfCorreo4;
	public String transfCorreo5;
    public int checkPromocion;


    public String toString() {
		
		return "cod_pro: " + nombProducto + ", cant = " + cantidad;
		
	}
	public String inven1;
	public String inven2;
	public String inven3;
	public String invenVendedor;
	public int detalleVinventario;

	
	@Override
	public int compareTo(Detalle another) {
		int lastCmp = posicion - another.posicion;
		return lastCmp;
	}
}
