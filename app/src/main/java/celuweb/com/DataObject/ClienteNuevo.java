package celuweb.com.DataObject;

import java.io.Serializable;

public class ClienteNuevo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String codigo;
	public String nombre;
	public String razonSocial;
	public String nit;
	public String direccion;
	public String ciudad;
	public String telefono;
	public String vendedor;
	public String ruta_parada;
	public String actividad;
	public int ordenVisita = 0;
	public String tipoDoc;
	public String territorio;
	public String agencia;
	public String barrio;
	public String listaPrecio;
	public String Canal;
	public String SubCanal;
	public String tipoCliente = "";
	public String codCiudad = "";
	public String AcomuladoBonif = "";
	public String numeroDoc;

    public String observacion = "";
}
