/**
 * 
 */
package celuweb.com.DataObject;

/**
 * @author JICZ
 *
 */
public interface DateSetFechaListener {

	/**
	 * contiene la respuesta del datepiker de la fecha en formato compatible con sqlite.
	 * @param fecha
	 */
	public void getFechaString(String fecha);
}
