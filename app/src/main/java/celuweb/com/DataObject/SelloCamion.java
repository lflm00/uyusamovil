package celuweb.com.DataObject;

import java.io.Serializable;

public class SelloCamion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String ruta;
	public String liquidador;
	public String fechaInicial;
	public String fechaFinal;
	public String nroSello;
	public String nroCamion;
	public String sicronizado;
	public String bandera;

	
}
