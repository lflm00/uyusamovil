package celuweb.com.DataObject;

import java.io.Serializable;

public class Encuestas implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String codigo;
	public String titulo;
	public int obligatoria = 0;
	public int codPrimeraPregunta = 0;
	public int unica = 0;
}
