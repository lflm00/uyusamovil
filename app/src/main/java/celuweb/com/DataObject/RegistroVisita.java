package celuweb.com.DataObject;

import java.io.Serializable;

public class RegistroVisita implements Serializable {

	private static final long serialVersionUID = 1L;

	public String numDoc;
	public String codigoVendedor;
	public String codigoCliente;
	public String fechaInicial;
	public String fechaFinal;
	public String ruta;
	public String codigoTipoEjecucionRuta;
	public String finalizado;
	public String nombreCliente;
	public String razonSocialCliente;
	public int diaVisita;
	public String radio;
	public String distancia;
	public String perimetro;
	public String latitud;
	public String longitud;
	public String id;
	public String numDocGestion;

}
