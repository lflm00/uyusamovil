package celuweb.com.DataObject;

import java.io.Serializable;

public class EstadisticaRecorrido implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int total_visitas;
	public int total_pedidos;
	public int total_pedidos_sync;
	public int total_pedidos_sin_sync;
	public int devoluciones;
	public float total_venta;
	public String str_total_venta;
	public float iva;
	public float efectividad;
	public double montoDevoluciones;
	public double totalVentaBruta;
	public double totalRecaudo;
	public double totalGlobal;
	public float descuento;
	
	
	/**
	 * Atributos necesarios para el informe de venta
	 * 
	 */
	public String str_venta_contado;
	public float venta_contado;
	public float dev_contado;
	public float venta_credito;
	public float dev_credito;
}
