package celuweb.com.DataObject;

import java.io.Serializable;

public class TotalInventarioDia implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5390741049653949927L;
	
	private int totalUnidadesII;
	private int totalCanastillas;
	private double valorCargue;
	
	/**
	 * @return the totalUnidadesII
	 */
	public int getTotalUnidadesII() {
		return totalUnidadesII;
	}
	/**
	 * @param totalUnidadesII the totalUnidadesII to set
	 */
	public void setTotalUnidadesII(int totalUnidadesII) {
		this.totalUnidadesII = totalUnidadesII;
	}
	/**
	 * @return the totalCanastillas
	 */
	public int getTotalCanastillas() {
		return totalCanastillas;
	}
	/**
	 * @param totalCanastillas the totalCanastillas to set
	 */
	public void setTotalCanastillas(int totalCanastillas) {
		this.totalCanastillas = totalCanastillas;
	}
	/**
	 * @return the valorCargue
	 */
	public double getValorCargue() {
		return valorCargue;
	}
	/**
	 * @param valorCargue the valorCargue to set
	 */
	public void setValorCargue(double valorCargue) {
		this.valorCargue = valorCargue;
	}
}
