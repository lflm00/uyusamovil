package celuweb.com.DataObject;

import java.io.Serializable;

public class DescuentoAutorizado implements Serializable {

	private static final long serialVersionUID = 1L;
	
	    public String bodega;
		public String vendedor;
		public String codigocliente;
		public String material;
		public String materialcompleto;
		public int cantidad;
		public int porcentaje;
		public double valor;
		public String autorizadopor;
		public String fecha;
		public String numerodoc;
		public double precio;
		public int sincronizado;
		public int sincronizadoandroid;
		public String descripcionproducto;
				
				
}
