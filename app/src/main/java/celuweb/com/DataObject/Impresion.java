package celuweb.com.DataObject;

import java.io.Serializable;

public class Impresion implements Serializable {

	private static final long serialVersionUID = 1L;

	public String bodega;
	public String descripcion;
	public String razonSocial; 
	public String nit;
	public String direccion;
	public String telefono;
	public String tipo;
	
	public String razonSocialCliente;
	public String nitCliente;

	public String codigo;

	public String nombre;

	public String serial;

	public String ruc;
}
