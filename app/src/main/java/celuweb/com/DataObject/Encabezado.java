package celuweb.com.DataObject;

import java.io.Serializable;

public class Encabezado implements Serializable {

    private static final long serialVersionUID = 3L;

public String version;
    public String codigo_cliente;  //Codigo del Cliente
    public String numero_doc;      //Numero Unico generado por cada encabezado
    public String fechaReal;
    public String nombre_motivo;
    public String descripcion;
    public int tipoTrans;          //0 -> Pedido, 2  -> Cambio (Tabla Encabezado -> tipoTrans)
    public String fechaTrans;
    public float valor_neto;            //Valor Neto de la Venta      (sub_total + total_iva)
    public float valor_descuento;       //Valor del Descuento
    public float total_iva;             //Valor Total IVA
    public String usuario;        // codigo vendedor
    public String hora_inicial;    //Hora Inicial
    public String hora_final;      //Hora Final
    public String bodega;      //Hora Final
    public int sincronizado;
    public int entregado;
    public int factura;
    public int sincronizadoMapas;
    public String serial;      //serial dispositivo
    public String fechaEntrega;    //Indica la fecha en que se debe entrega el pedido
    public String oc;
    public int syncmobile;
    public int anulado;
    public String contacto;
    public int detaVInventario;


    public String nombre_cliente;  //Nombre del Cliente, solo es para mostrar el Nombre en pantalla
    public String razon_social;    //Razon Social
    public int codigo_novedad;     //1 -> Venta, para no compra es MotivosCompras.codigo

    public String lista_precio;       //Lista de Precio

    public String str_valor_neto;       //Valor Neto con Separacion de Miles, se usa solo para mostrar en Pantalla
    public float sub_total;             //Valor Sub Total de la Venta (precio * cantidad)

    public String str_valor_neto_producto;
    public String observacion;     //Observacion del Pedido
    public String observacion2;    //Observacion del Pedido
    public String ordenCompra;     //Observacion del Pedido
    public int motivo;
    public String str_sub_total;
    public String amarre;
    public String valorVenta;
    public int esVenta;


    //Datos del consecutivo a insertar en el encabezado

    public String strFactura;
    public String idConsecutivo;
    public String prefijoConsecutivo;
    public int nroConsecutivo;
    public String sucursal;
    public float flete;
    public int checkPromocion;
    public int checkCotizacion;
}