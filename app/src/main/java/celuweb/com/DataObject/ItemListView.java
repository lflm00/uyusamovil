package celuweb.com.DataObject;

import java.io.Serializable;

import android.bluetooth.BluetoothDevice;

public class ItemListView implements Serializable {

    private static final long serialVersionUID = 6L;

    public String titulo;
    public String subTitulo;
    public String referencia;
    public String documento;
    public int colorTitulo;
    public int icono;
    public BluetoothDevice device;
    public int state;

    //atr historial pedidos
    public String meses;

    /**
     * permite definir si una fila
     * debe ir resaltada.
     * 0 = no remarcada,
     * 1 = remarcada.
     */
    public int resaltado = 0;
    public String isProspecto;
}