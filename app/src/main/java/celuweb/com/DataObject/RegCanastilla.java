package celuweb.com.DataObject;

import java.io.Serializable;

public class RegCanastilla implements Serializable {

	private static final long serialVersionUID = 1L;

	public String codigo;
	public String descripcion;
	public String nroDoc;
	public String entregada;
	public String devuelta;
	public String codCliente;

	public int cantidad;
	public int entregaMin;
	
	public int nroEntrega;
	public int nroDevuelto;

	public String consecutivo;

	public String vendedor;
	public String strCantidad;
}
