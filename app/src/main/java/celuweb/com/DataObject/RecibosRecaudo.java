package celuweb.com.DataObject;

import java.io.Serializable;



public class RecibosRecaudo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String vendedor;
	public int limiteInferior;
	public int limiteSuperior;
	public int actual;
	public String serie;
	public int activo;
	public String fechaActivacion;
	public int orden;
	public int id;
	
	public String numRecaudo;
	public String numRecibo;
	public long valor;

 
}
