package celuweb.com.DataObject;

import java.io.Serializable;

public class Tiempos implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String numeroDocumento;
	public String codigoVendedor;
	public String horaInicial;
	public String horaFinal;
	public String actividad;
	public String fecha;
	public String observacion;

	
}
