package celuweb.com.DataObject;


import java.io.Serializable;

public class Consignacion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String recibo;
	public String vendedor;
	public String fecha;
	public String codigoBanco;
	public String cuentaBanco;
	public String formaPago;
	public double valor;
	public String numeroCheque;
	public String fechaGrabo;
	public String fechaBanco;
	public String cargues;
    public String sincronizado;
    public String consecutivo;
}
