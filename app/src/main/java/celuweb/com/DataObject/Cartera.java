package celuweb.com.DataObject;

import java.io.Serializable;



public class Cartera implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String codCliente;    //Codigo del Cliente
	public String referencia;    //Numero Documento
	public double saldo;          //Saldo - abono
	public float abono;
	public int dias;
	public String FechaVecto;
	public String fecha;
	public String concepto;
	public String documento;
	public String descripcion;
	
	//Datos Registrados en el Recaudo
	public boolean pagoTotal = true; //true -> Pago Total, False -> Pago Parcial
	public double valorARecaudar;
	public int codigoMotivoAbono;
	public String observacion;
	
	public String strSaldo;
	public String strAbono;
	
	
	public String numero_fp;
	public String valor_fp;
	public String formapago;
	public String plaza;
	public String reference;
	public String bancod;
	public String fechapostd;
	public float valordpp;
	public int dias2;
	public float valorProntoPago;
	public boolean prontoPago = false; //true -> Pronto Pago, False -> No Pronto Pago
	
	public boolean esPagoParcial;
	
	
	public boolean conConsignacion = false;
	public float saldoAnterior;  
	public String vendedor="";

	public String nroFiscal;
	public String cupoCliente;
    public float saldoVencido;
	public String idFoto;
	public String facturadoMes;
	public String recaudadoMes;
	public String periodoFacturado;
	public String periodoRecaudado;
}
