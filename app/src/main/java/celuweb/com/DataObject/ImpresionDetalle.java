package celuweb.com.DataObject;

import java.io.Serializable;

public class ImpresionDetalle implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String codigo;
	public String nombre;
	public float precio;
	public String precioStr;
	public float iva;
	public float descuento;
	public int cantidad;
	public String cantidadStr;
	public int tipo;
	public String codMotivo;
	public String descMotivo;
	public String totalStr;
	public float total;
	public int cantidadUnidades;
	public int cantidadCajas;

	
	/**
	 * Atributos necesarios para imprimir productos liquidacion
	 */
	public String tipoProducto;
	public int cantidadDigitada;
	public int cantidadTransaccion;
	public int cantidadInventario;
	public int diferencia;
	public String strDigitada;
	public String strDiferencia;
	public String strTransaccion;
	public String strInventario;

	public String descStr;
}
