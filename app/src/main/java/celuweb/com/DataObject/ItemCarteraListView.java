package celuweb.com.DataObject;

import java.io.Serializable;

public class ItemCarteraListView implements Serializable {

	private static final long serialVersionUID = 6L;
	
	public String codigo;     //Codigo Referencia Cartera
	public String titulo;
	public String subTitulo;
	public boolean seleccionado;
}