package celuweb.com.DataObject;


import java.io.Serializable;

public class OrdenLiquidacion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public int regCanastaVacia;
	public int regProductoDanado;
	public int regAveria;
	public int regInventarioLiquidacion;
	public int regInventarioCanasta;
	public int regDescarga;
	public int regSello;

}
