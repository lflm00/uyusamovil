package celuweb.com.DataObject;

import java.io.Serializable;

public class DetalleGasto implements Serializable {

	private static final long serialVersionUID = 4L;
	
	 public String nombreConcepto;    
	 public double valor;
	 public String fecha;
	 public String concepto;
     public int id;
     public String nroDoc;
     public String vendedor;
     public String nit;
     public String observacion;
	 public double anticipo;
	 public String bodega;
	 public String sincronizado;
	 public String codigoConcepto;
			       
	
}
