package celuweb.com.DataObject;

import java.io.Serializable;

public class ParametroPregunta implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int codParamPreg;
	public int codPregunta;
	public String descripcion;
	public int codPreguntaDestino;

}
