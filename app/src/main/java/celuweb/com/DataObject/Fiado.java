package celuweb.com.DataObject;

import java.io.Serializable;

public class Fiado implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int position;
	public String numeroDoc;
	public String montoFact;
	public String nombre;
	public String fecha;
	public int fiado;
}
