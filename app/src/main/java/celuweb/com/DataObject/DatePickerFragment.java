package celuweb.com.DataObject;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;



/**
 * DatePicker que retorna entrega la fecha en un string compatible con sqlite.
 * Requiere minimo: API level 11, -> sdk 3.0
 * @author JICZ
 *
 */
@SuppressLint("NewApi") 
public class DatePickerFragment extends DialogFragment implements OnDateSetListener {

	/**
	 * atributo fecha, con formato compatible con sqlite, obtenido desde el datapicker.
	 */
	private String fechaDefinida;
	{
		fechaDefinida = "";
	}
	
	/**
	 * respuesta de la interfaz.
	 */
	private DateSetFechaListener dateSetFecha;





	/**
	 * Constructor
	 * @param dateSetFecha
	 */
	public DatePickerFragment(DateSetFechaListener dateSetFecha) {
		this.dateSetFecha = dateSetFecha;
	}





	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        int millisecond = c.get(Calendar.MILLISECOND);
        
        //dar formato de fecha compatible con sqlite.
        formatearFechaString(year, month, day, hour, minute, second, millisecond);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
	}
	
	
	
	
	
	/**
	 * permite dar un formato de fecha compatible con sqlite, (yyyy-MM-dd HH:mm:ss.SSS) ->
	 * basado de SimpleDateFormat;
	 * @param year
	 * @param month
	 * @param day
	 * @param millisecond 
	 * @param second 
	 * @param minute 
	 * @param hour 
	 */
	private void formatearFechaString(int year, int month, int day, int hour, int minute, int second, int millisecond) {
		
		//formatear la fecha. yyyy-MM-dd HH:mm:ss.sss  -> basado en SimpleDateFormat de java.
		String fecha = 	String.valueOf(year)+ "-" +
						(month < 10 ? ("0" + String.valueOf(month)) : String.valueOf(month)) + "-" +
						(day < 10 ? ("0" + String.valueOf(day)) : String.valueOf(day)) + " " +
						(hour < 10 ? ("0" + String.valueOf(hour)) : String.valueOf(hour))+ ":" +
						(minute < 10 ? ("0" + String.valueOf(minute)) : String.valueOf(minute))+ ":" +
						(second < 10 ? ("0" + String.valueOf(second)) : String.valueOf(second))+ "." +
						String.valueOf(millisecond);
		
		
		//setiar la fecha.
		this.setFechaSeleccionda(fecha);
	}



	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        int millisecond = c.get(Calendar.MILLISECOND);
        
        //formatear la fecha seleccionada
        formatearFechaString(year, monthOfYear + 1, dayOfMonth, hour, minute, second, millisecond);
        
        //respuesta a la interfaz
        dateSetFecha.getFechaString(this.getFechaSeleccionda());
	}



	/**
	 * @return the fechaSeleccionda
	 */
	public String getFechaSeleccionda() {
		return fechaDefinida;
	}



	/**
	 * @param fechaSeleccionda the fechaSeleccionda to set
	 */
	private void setFechaSeleccionda(String fechaEstablecida) {
		this.fechaDefinida = fechaEstablecida;
	}
	
}
