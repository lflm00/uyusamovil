/**
 * 
 */
package celuweb.com.DataObject;

import java.io.Serializable;

/**
 * @author JICZ
 *
 */
public class CarteraConsignacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7281597666257023012L;
	
	public String referencia;
	public String documento;
	public double saldoCartera;
	public int diasVencidoProntoPago;
}
