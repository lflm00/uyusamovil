package celuweb.com.DataObject;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import android.content.Context;
import android.util.Log;

/************************************
 * MODULO DE BONIFICACIONES JAUM
 ************************************/
public class Bonificaciones implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public String descripcionRegalo;
	public String codigoBono;
	public String codigoRegalo;
	public String auxiliarBono;
	public String tipoBono;
	public int cantidadPedidaBono;
	public int cantidadBono;
	public int cantidadDosBono;
	public int cantidadRegaloBono;
	public int cantidadAuxBono;
	public int cantidadInvBono;
	
	public static Bonificaciones get(Context context) {
		 
		Bonificaciones bonificaciones = null;
		String nameMethod = "get";
		 
		 if (context != null) {
			
			 String fileName = CLASS.getSimpleName();
			 
			 if (fileName != null) {

				 FileInputStream fileInputStream = null;
				 ObjectInputStream objInputStream = null;
				 
				 try {
					 
					 fileInputStream = context.openFileInput(fileName);
					 objInputStream = new ObjectInputStream(fileInputStream);
					 bonificaciones = (Bonificaciones) objInputStream.readObject();
					 
				 } catch(Exception e) {
					 
					 String msg = e.getMessage();
					 Log.e(TAG, nameMethod + " -> " + msg, e);
					 
				 } finally {
					 
					 try {
						 
						 if (fileInputStream != null) {
							 fileInputStream.close();
						 }
						 
						 if (objInputStream != null) {
							 objInputStream.close();
						 }
						 
					 } catch (Exception e) { }
				 }
				 
			 } else {
				 
				 Log.e(TAG, nameMethod + " -> No se pudo leer el nombre de la clase!");
			 }
			 
		 } else {
			 
			 Log.e(TAG, nameMethod + " -> No se pudo leer el Objeto, el Context es NULL");
		 }
		 
		 return bonificaciones;
	 }
	
	 public static boolean save(Context context, Bonificaciones obj) {
		 
		 boolean guardo = false;
		 String nameMethod = "save";
		 
		 if (context != null) {
			 
			 String fileName = CLASS.getSimpleName();
			 
			 if (fileName != null) {
				
				 if (obj != null) {
					 
					 FileOutputStream fileOutputStream = null;
					 ObjectOutputStream objOutputStream = null;
					 
					 try {
						 
						 context.deleteFile(fileName);
						 
						 fileOutputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
						 objOutputStream = new ObjectOutputStream(fileOutputStream);
						 objOutputStream.writeObject(obj);
						 guardo = true;
						 
					 } catch (Exception e) {
						 
						 String msg = e.getMessage();
						 Log.e(TAG, nameMethod + " -> " + msg, e);
						 
					 } finally {
						 
						 try {
							 
							 if (objOutputStream != null) {
								 objOutputStream.close();
							 }
							 
							 if (fileOutputStream != null) {
								 fileOutputStream.close();
							 }
							 
							 if (!guardo) {
								 context.deleteFile(fileName);
							 }
							 
						 } catch (Exception e) { }
					 }
					 
				 } else {
					 
					 /**
					  * Si el objeto es NULL, se borra el archivo!
					  **/
					 context.deleteFile(fileName);
				 }
				 
			 } else {
				 
				 Log.e(TAG, nameMethod + " -> No se pudo leer el nombre de la clase!");
			 }
			 
		 } else {
			 
			 Log.e(TAG, nameMethod + " -> No se pudo Eliminar el Objeto, el Context es NULL");
		 }
		 
		 return guardo;
	 }
	 
	 public static boolean delete(Context context) {
		 
		 boolean elimino = false;
		 String nameMethod = "delete";
		 
		 if (context != null) {
			 
			 String fileName = CLASS.getSimpleName();
			 
			 if (fileName != null) {
				 
				 elimino = context.deleteFile(fileName);
				 
			 } else {
				 
				 Log.e(TAG, nameMethod + " -> No se pudo leer el nombre de la clase!");
			 }
			 
		 } else {
			 
			 Log.e(TAG, nameMethod + " -> No se pudo Eliminar el Objeto, el Context es NULL");
		 }
		 
		 return elimino;
	 }
	 
	 
	 private static final Class<Bonificaciones> CLASS = Bonificaciones.class;
	 
	 private static final String TAG = CLASS.getName();
	
	
}
