package celuweb.com.DataObject;

import java.io.Serializable;

public class Producto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	public String codigo;
	public String descripcion;
	public float precio;
	public float iva;
	public float precioIva;
	public long inventario;
	public String inv1, inv2, inv3;
	public float precioDescuento;
	public float descuento;
	public String unidadesXCaja;
	public String ean;
	public String agrupacion;
	public String grupo;
	public String linea;
	public String unidadMedida;
	public float factorLibras;
	public String fechaLimite;
	public String devol;
	public String itf;
	public int impto1;
	public int indice;
	public int entregada;
	public int devuelta;
	public float promedio;
	public float promedio_fijo;
	
	
	/**
	 * Atributos necesarios para la liquidacion por producto
	 */
	
	public String tipo;
	public int cantidadDigitada;
	public int cantidadTransaccion;
	public int cantidadInventario;
	public int diferencia;


	public String strInventario;


	public String strDigitada;


	public String strTransaccion;


	public String strDiferencia;


	public float total;


	public int cantidad;


	public String categoria;


	public String lineaProd;

	public int cantidadCambio;
}
