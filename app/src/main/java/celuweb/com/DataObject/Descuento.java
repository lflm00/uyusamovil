package celuweb.com.DataObject;

import java.io.Serializable;

public class Descuento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int codigo;
	public String descripcion;
	public String valor;
}
