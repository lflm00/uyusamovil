package celuweb.com.DataObject;

/**
 * Created by Andres Rangel on 17/01/2018.
 */

public class ChequeoPrecios {

    private String id;
    private String descripcion;
    private String fechaInicial;
    private String fechaFinal;
    private String codProducto;
    private String codProductoCompetencia;
    private String precio;
    private int tieneCompetencia;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }


    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getTieneCompetencia() {
        return tieneCompetencia;
    }

    public void setTieneCompetencia(int tieneCompetencia) {
        this.tieneCompetencia = tieneCompetencia;
    }

    public String getCodProductoCompetencia() {
        return codProductoCompetencia;
    }

    public void setCodProductoCompetencia(String codProductoCompetencia) {
        this.codProductoCompetencia = codProductoCompetencia;
    }
}
