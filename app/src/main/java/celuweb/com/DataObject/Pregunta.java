package celuweb.com.DataObject;

import java.io.Serializable;
import java.util.Vector;

public class Pregunta implements Serializable {

	private static final long serialVersionUID = 1L;

	public String cod_pregunta;
	public int cod_encuesta;
	public int tipo; 
	public String titulo;
	public int oblig;
	public int ordePreg;
	public int idVar;
	
	public String id;
	public byte[] image;
	public String respuesta;
	public Vector<Integer> param_preg;
	
	public int icono;

	public String cod_param;
	
	
	
	
	public int codPregunta;
	public int codEncuesta;
	public int codTipoPregunta;
	public String descripcion;
	public int codTipoDatoEncuesta;
	public int codPreguntaDestino;
	public int codPreguntaOrigen;
	
	

	public String categoria;

	public int tipoPregunta;
	public int preguntaAsociada;
}
