package celuweb.com.DataObject;

import java.io.Serializable;

public class InformeInventario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String codigo;
	public String nombre;
	
	public int invInicial;
	public int invActual;
	public int cantVentas;	
	public int cantPNC;
	public int sobrante;

    public long precio;
    public float promedio;
}
