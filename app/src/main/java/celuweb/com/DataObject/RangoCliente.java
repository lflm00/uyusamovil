package celuweb.com.DataObject;

import java.io.Serializable;

public class RangoCliente implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public double diferencia;
	public String nombreCliente;
	public boolean estado;
}
