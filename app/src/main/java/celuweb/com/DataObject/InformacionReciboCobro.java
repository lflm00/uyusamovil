package celuweb.com.DataObject;

import java.util.Vector;

public class InformacionReciboCobro {

	public String numRecibo;
	public String codCliente;
	public String nombreCliente;
	public String razonSocial;
	public String direccion;
	public String telefono;
	public String ciudad;
	public String fecha;
	public String codVendedor;
	public String nombreVendedor;
	public long retefuente;
	public long flete;
	public long descuento;
	public long totalDesc;
	public String obserDesc = "";
	public String obserFP = "";
	public long efectivo     = 0;
	public long cheque       = 0;
	public long chequePost   = 0;
	public long consignacion = 0;
	public long totalRecaudo = 0;
	public Vector< FacturasImpresion > vFactImpresion;
	public Vector< ChequePostFechado > vChequePost;
}
