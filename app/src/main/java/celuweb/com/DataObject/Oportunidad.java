/**
 * 
 */
package celuweb.com.DataObject;

import java.io.Serializable;

/**
 * @author JICZ
 *
 */
public class Oportunidad implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1659460395363489921L;
	
	
	public String codigo;
	public String laBuena;
	public String campi;
	public String gourmet;
	public String olioSoya;
	public String sol;
	public String chocoExpress;
	public String lukafe;
	public String aromaPulverizado;
	public String fluocardent;
	public String tarritoRojo;
}
