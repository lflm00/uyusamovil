package celuweb.com.DataObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ListasCompensacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2233574416042085740L;

	/**
	 * Lista de productos sobrantes. diferencia negativa
	 */
	private ArrayList<ProductosCompensacion> listaSobrantes;

	/**
	 * lista de productos faltantes, diferencia positiva
	 */
	private ArrayList<ProductosCompensacion> listaFaltantes;



	/**
	 * @param listaSobrantes
	 *            the listaSobrantes to set
	 */
	public void setListaSobrantes(ArrayList<ProductosCompensacion> listaSobrantes) {
		this.listaSobrantes = listaSobrantes;
	}



	/**
	 * @param listaFaltantes
	 *            the listaFaltantes to set
	 */
	public void setListaFaltantes(ArrayList<ProductosCompensacion> listaFaltantes) {
		this.listaFaltantes = listaFaltantes;
	}



	/**
	 * @return the listaSobrantes
	 */
	public ArrayList<ProductosCompensacion> getListaSobrantes() {
		return listaSobrantes;
	}



	/**
	 * @return the listaFaltantes
	 */
	public ArrayList<ProductosCompensacion> getListaFaltantes() {
		return listaFaltantes;
	}

}
