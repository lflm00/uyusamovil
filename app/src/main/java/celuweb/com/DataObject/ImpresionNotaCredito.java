package celuweb.com.DataObject;

import java.io.Serializable;

public class ImpresionNotaCredito implements Serializable {

	private static final long serialVersionUID = 3L;
	
	
	public String codigo;  //Codigo del Cliente
	public String numero_doc;      //Numero Unico generado por cada encabezado
	public float valor_neto;            //Valor Neto de la Venta      (sub_total + total_iva)
	public float valor_descuento;       //Valor del Descuento
	public float total_iva;  
	public int factura;
	public String str_valor_neto;       //Valor Neto con Separacion de Miles, se usa solo para mostrar en Pantalla
	public float sub_total;             //Valor Sub Total de la Venta (precio * cantidad)
	public String str_valor_neto_producto; 
	public String str_sub_total;
	public String amarre;


	public int anulado;


	public long total;


	public float valorSinIva;


	public String strValorSinIva;
}