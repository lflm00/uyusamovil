package celuweb.com.DataObject;

import java.io.Serializable;

public class RegistroRango implements Serializable {

	private static final long serialVersionUID = 1L;
	public String radio;
	public String distancia;
	public String perimetro;
	public String latitud;
	public String longitud;
}