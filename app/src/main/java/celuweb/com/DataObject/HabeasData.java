package celuweb.com.DataObject;

import java.io.Serializable;

import celuweb.com.DataObject.HabeasData;

public class HabeasData implements Serializable {

	private static final long serialVersionUID = 1L;

	public String numDoc;
	public String codigoCliente;
	public String codigoVendedor;
	public String nit;
	public String nombreCliente;
	public String codigoCiudad;
	public String email;
	public String fecha;
	public int autorizo;
	public String nuevo;
	public String observacion;

}
