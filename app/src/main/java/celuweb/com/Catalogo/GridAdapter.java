package celuweb.com.Catalogo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import celuweb.com.uyusa.Const;
import celuweb.com.uyusa.R;
import celuweb.com.fonts.Font;

public class GridAdapter extends BaseAdapter {
    private Context mContext;
    private List<ItemCardView> mListItems;

    public GridAdapter(Context context, List<ItemCardView> listItems) {
        this.mContext = context;
        this.mListItems = listItems;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View item = convertView;
        final ViewHolder viewHolder;

        if (item == null) {
            item = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_view_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.lblNoItems = (TextView) item.findViewById(R.id.lblNoItems);
            viewHolder.imgPreview = (ImageView) item.findViewById(R.id.imageView);
            viewHolder.lblTitulo = (TextView) item.findViewById(R.id.lblDireccion);
            viewHolder.lblSubTitulo = (TextView) item.findViewById(R.id.lblSubTitulo);
            viewHolder.lblSubTitulo.setTypeface(Font.getFont(mContext));

            viewHolder.position = position;
            item.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.position = position;
        }

        String picture = Const.URL_IMGS + mListItems.get(position).codigo + ".png";
        Glide.with(mContext)
                .load(picture)
                //.placeholder(R.drawable.ic_holder)
                .dontAnimate()
                .override(150, 150)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.imgPreview);

        ItemCardView itemCardView = mListItems.get(position);
        int cantidad = itemCardView.cantidad;
        if (cantidad == 0) {
            viewHolder.lblNoItems.setVisibility(View.GONE);
        } else {
            String strCantidad = "" + cantidad;
            viewHolder.lblNoItems.setText(strCantidad);
            viewHolder.lblNoItems.setVisibility(View.VISIBLE);
        }

        if (itemCardView.precio == 0) {
            viewHolder.lblSubTitulo.setText("0");
            viewHolder.lblSubTitulo.setVisibility(View.VISIBLE);
        } else {
            viewHolder.lblSubTitulo.setVisibility(View.VISIBLE);
        }

        viewHolder.lblTitulo.setText(mListItems.get(position).titulo);
        viewHolder.lblSubTitulo.setText(mListItems.get(position).subTitulo);
        return item;
    }

    public void setData(List<ItemCardView> listItems) {
        this.mListItems = listItems;
    }

    public class ViewHolder {
        private int position;
        private TextView lblNoItems;
        private ImageView imgPreview;
        private TextView lblTitulo, lblSubTitulo;
    }
}
