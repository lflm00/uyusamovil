package celuweb.com.Catalogo;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


import java.util.List;
import java.util.Locale;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Categoria;
import celuweb.com.uyusa.R;
import celuweb.com.uyusa.Util;
import celuweb.com.fonts.FontIcon;
import celuweb.com.fonts.FontIconCatalogo;

public class CatalogoActivity extends AppCompatActivity {
    // Tag del Activity
    //private static final String TAG = CatalogoActivity.class.getName();

    public final static int RESP_DETALLE_PEDIDO = 1000;

    //private Menu mMenu;
    private MenuItem mMenuCart;
    //private MenuItem mMenuSearch;

    private int codTienda = -1;

    private AlertDialog mAlertDialog;

    //Permite controlar el evento de doble click
    private long mLastClickTime = 0;

    private static final int mMenuSizeDp = 32;
    private static final int mWhiteColor = android.R.color.white;

    private static Drawable shoppingCart = null;
    public static int tipoTransaccion=0;
    public static int tipoClienteSeleccionado=0;
    public static boolean tipoTransaccionActual=false;
    public static boolean esCargue=false;
    public static boolean cambio=false;
    public static final boolean AUTOVENTA = true;

    /**
     * CONSTANTE PARA IDENTIFICAR EL TIPO DE TRANSACCION PREVENTA PARA EL PEDIDO
     * ACTUAL.
     */
    public static final boolean PREVENTA = false;

    private static Drawable getShoppingCart(Context context) {
        if (shoppingCart == null) {
            shoppingCart = Util.createFontIcon(context,
                    FontIcon.Icon.clw_editar,
                    mWhiteColor,
                    mMenuSizeDp);
        }
        return shoppingCart;
    }

    public int getCodTienda() {
        if (codTienda == -1) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                codTienda = bundle.getInt("codTienda", -1);
                tipoTransaccion=bundle.getInt("tipoTransaccion");
                tipoClienteSeleccionado=bundle.getInt("tipoClienteSeleccionado");
                tipoTransaccionActual=bundle.getBoolean("tipoTransaccionActual");
                esCargue=bundle.getBoolean("esCargue");
                cambio=bundle.getBoolean("cambio");
            }
        }
        return codTienda;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogo);

        //// TO-DO: 13/05/2017 Activar cuando se agregue la opcion de buscar.
        //handleIntent(getIntent());



        setupToolbar();
        setupTabLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();

            registerReceivers();
            //loadMessages(getIntent());
/*
            SyncPedidos syncPedidos = new SyncPedidos();
            syncPedidos.execute();*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        createOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.search:
                onClickSearch();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*@Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }*/

    private void createOptionsMenu(final Menu menu) {
        // Inflate el menu. Agrega los items a la barra de herramientas.
        getMenuInflater().inflate(R.menu.menu_catalogo, menu);

        mMenuCart = menu.findItem(R.id.cart);
        if (mMenuCart != null) {
            View viewMenuItem = mMenuCart.getActionView();
            if (viewMenuItem != null) {
                ImageButton btnCart = (ImageButton) viewMenuItem.findViewById(R.id.btnCart);
                if (btnCart != null) {
                    btnCart.setImageDrawable(getShoppingCart(this));
                    btnCart.setOnClickListener(mOnClickCart);
                }

                TextView lblNoItems = (TextView) viewMenuItem.findViewById(R.id.lblNoItems);
                setTotalItems(lblNoItems);
                mMenuCart.setVisible(false);
            }
        }

        //mMenu = menu;
        MenuItem mMenuSearch = menu.findItem(R.id.search);
        if (mMenuSearch != null) {
            Drawable drawable = Util.createFontIcon(this, FontIconCatalogo.Icon.clw_search, mWhiteColor, mMenuSizeDp);
            mMenuSearch.setIcon(drawable);
        }

        //Nota: Esta implementacion funciono correctamente, solo que por tiempo
        //se cambio la forma como se haria la busqueda, se manda a una nueva actividad.
        /* mMenuSearch = menu.findItem(R.id.search);
        if (mMenuSearch != null) {
            Drawable drawable = Util.createFontIcon(this, GoogleMaterial.Icon.gmd_search, mWhiteColor, mMenuSizeDp);
            mMenuSearch.setIcon(drawable);

            //Listener para controlar cuando se expande o contrae el menu
            MenuItemCompat.setOnActionExpandListener(mMenuSearch, mOnActionExpandListener);

            // Associate searchable configuration with the SearchView
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) mMenuSearch.getActionView();
            if (searchView != null) {
                //Toma la configuracion definida en @xml/searchable
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

                //Se cambia la fuente de la caja de busqueda
                SearchView.SearchAutoComplete searchAutoComplete;
                searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
                if (searchAutoComplete != null) {
                    searchAutoComplete.setTypeface(Typeface.DEFAULT);
                }
            }
        }*/
    }

    /*private void setItemsVisibility(boolean visible) {
        if (mMenu != null) {
            int size = mMenu.size();
            for (int i = 0; i < size; ++i) {
                MenuItem item = mMenu.getItem(i);
                if (item != mMenuSearch) {
                    item.setVisible(visible);
                }
            }
        }
    }*/

    /*private void handleIntent(Intent intent) {
        String intentAction = intent.getAction();
        if (Intent.ACTION_SEARCH.equals(intentAction)) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(CatalogoActivity.this, "Busco lo siguiente: " + query, Toast.LENGTH_SHORT).show();
            closeSearchView();
        }
    }*/

    /*private void closeSearchView() {
        if (mMenuSearch != null) {
            mMenuSearch.collapseActionView();
        }
    }*/

    //Oculta la opcion de busqueda, y activa de nuevo el menu
    //Activar este metodo para guardar las sugerencias frecuentes de busqueda.
    private void setupTabLayout() {
        int codTienda = getCodTienda();
        List<Categoria> tabItems = DataBaseBO.listaCategorias(codTienda);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), tabItems));
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
    }


    /*private void loadDataSuggestions() {
        SearchRecentSuggestions suggestions =
                new SearchRecentSuggestions(this,
                        SampleRecentSuggestionsProvider.AUTHORITY,
                        SampleRecentSuggestionsProvider.MODE);

        suggestions.clearHistory();
        //suggestions.saveRecentQuery("papa", null);
        //suggestions.saveRecentQuery("panela", null);
    }*/

    ///////////////////////////////
    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void updateTotalItems() {
        if (mMenuCart != null) {
            View viewMenuItem = mMenuCart.getActionView();
            if (viewMenuItem != null) {
                TextView lblNoItems = (TextView) viewMenuItem.findViewById(R.id.lblNoItems);
                setTotalItems(lblNoItems);
            }
        } else {
            // Dispara la ejecucion del metodo: onCreateOptionsMenu()
            invalidateOptionsMenu();
        }
    }

    private void setTotalItems(TextView lblNoItems) {
        if (lblNoItems != null) {
            int codTienda = getCodTienda();
            int totalItems = 5;//DataBaseBO.getTotalItems(codTienda);
            if (totalItems == 0) {
                lblNoItems.setText("");
                lblNoItems.setVisibility(View.INVISIBLE);
            } else {
                String strTotalItems = String.format(Locale.getDefault(), "%d", totalItems);
                if (totalItems < 10) {
                    strTotalItems = " " + strTotalItems + " ";
                }

                lblNoItems.setText(strTotalItems);
                lblNoItems.setVisibility(View.VISIBLE);
            }
        }
    }

    /*MenuItemCompat.OnActionExpandListener mOnActionExpandListener = new MenuItemCompat.OnActionExpandListener() {
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            setItemsVisibility(false);
            return true;
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            setItemsVisibility(true);
            return true;
        }
    };*/

    private void onClickSearch() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
            return;
        }
       Intent intent = new Intent(CatalogoActivity.this, BuscarProductoActivity.class);
        intent.putExtra("codTienda", getCodTienda());
        startActivityForResult(intent, RESP_DETALLE_PEDIDO);
    }

    View.OnClickListener mOnClickCart = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 600) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
           /* Intent intent = new Intent(CatalogoActivity.this, DetallePedidoActivity.class);
            intent.putExtra("codTienda", getCodTienda());
            startActivityForResult(intent, RESP_DETALLE_PEDIDO);*/
        }
    };

    /*View.OnClickListener mOnClickSearch = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESP_DETALLE_PEDIDO) {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof PageFragment) {
                    ((PageFragment) fragment).updateAdapter();
                }
            }

            //
            updateTotalItems();
        }
    }

   /* private void loadMessages(Intent intent) {
        List<Notificacion> listItems = new ArrayList<>();
        UsuarioBO.cargarNotificaciones(listItems);

        int size = listItems.size();
        if (size > 0) {
            String titulo = "";
            String mensaje = "";
            for (Notificacion notificacion : listItems) {
                if (TextUtils.isEmpty(titulo)) {
                    titulo = notificacion.titulo;
                }

                if (TextUtils.isEmpty(mensaje)) {
                    mensaje += notificacion.mensaje;
                } else {
                    mensaje += "\n" + notificacion.mensaje;
                }
            }
            showNotificationMessage(titulo, mensaje);
            UsuarioBO.marcarNotificacionLeida(listItems);

        } else {
            if (intent != null) {
                managePushNotification(intent);
            }
        }
    }
*/
    private void managePushNotification(@NonNull Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String title = bundle.getString("title");
            String message = bundle.getString("message");

            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(message)) {
                showNotificationMessage(title, message);
            }
        }
    }

    private synchronized void showNotificationMessage(String title, String message) {
        if (mAlertDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public synchronized void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            mAlertDialog = null;
                            //mCountMessage = 1;
                        }
                    });

            mAlertDialog = builder.create();
            mAlertDialog.setMessage(message);
            mAlertDialog.show();
        } else {
            //mAlertDialog.setTitle(title + " (" + ++mCountMessage + ")");
            String msg = "";
            final TextView textView = (TextView) mAlertDialog.findViewById(android.R.id.message);
            if (textView != null) {
                msg = textView.getText().toString();
            }
            mAlertDialog.setMessage(message + "\n" + msg);
        }
       // Util.playSoundNotification(getApplicationContext());
    }

    private void registerReceivers() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver((mMessageReceiver),
                new IntentFilter(Util.getFcmIntentName())
        );
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           // loadMessages(intent);
        }
    };
}
