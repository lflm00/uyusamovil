package celuweb.com.Catalogo;

public class ItemCardView {
    public long id;
    public int cantidad;
    public double precio;

    public String codigo;
    public String titulo;
    public String subTitulo;

    public float iva;
    public float precioIva;
    public long inventario;
    public float precioDescuento;
    public float descuento;
    public String unidadesXCaja;
    public String ean;
    public String agrupacion;
    public String grupo;
    public String linea;
    public String unidadMedida;
    public float factorLibras;
    public String fechaLimite;
    public String devol;
    public String itf;
    public int impto1;
    public int indice;

    public ItemCardView() { }

    /*public ItemCardView(int preview, String titulo, String subTitulo) {
        this.preview = preview;
        this.titulo = titulo;
        this.subTitulo = subTitulo;
    }*/

    /*
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPreview() {
        return preview;
    }

    public void setPreview(int preview) {
        this.preview = preview;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubTitulo() {
        return subTitulo;
    }

    public void setSubTitulo(String subTitulo) {
        this.subTitulo = subTitulo;
    }
    */
}
