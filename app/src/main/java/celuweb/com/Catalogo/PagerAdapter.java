package celuweb.com.Catalogo;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;



import java.util.List;

import celuweb.com.DataObject.Categoria;

public class PagerAdapter extends FragmentPagerAdapter {
    private final int PAGE_COUNT;

    private List<Categoria> mListItems;

    public PagerAdapter(FragmentManager fm, List<Categoria> listItems) {
        super(fm);
        this.mListItems = listItems;
        PAGE_COUNT = listItems.size();
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(mListItems.get(position).codig);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mListItems.get(position).descripcion;
    }
}