package celuweb.com.Catalogo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.iconics.typeface.IIcon;


import java.util.List;
import java.util.Locale;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Detalle;
import celuweb.com.uyusa.Const;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.R;
import celuweb.com.uyusa.Util;
import celuweb.com.fonts.Font;
import celuweb.com.fonts.FontIconCatalogo;

public class PageFragment extends Fragment implements AdapterView.OnItemClickListener
        //implements GridAdapter.IAdapterListener
        /*CatalogoAdapter.IAdapterListener*/ {
    public static final String ARG_PAGE = "ARG_PAGE";

    //private static final String IMAGE_CACHE_DIR = "thumbs";

    private int mPage;

    private int codTienda = -1;

    private int mPosition;

    GridView mGridView;
    GridAdapter mAdapter;

    List<ItemCardView> mListItems;


    private double mPrecio;
    private TextView mLblCantidad;
    private TextView mTxtTotal;

    //Permite controlar el evento de doble click
    private long mLastClickTime = 0;

    public void updateAdapter() {
        mPage = getArguments().getInt(ARG_PAGE);
        int codigo = getCodTienda();
        mListItems = DataBaseBO.getAdapterCatalog(codigo, mPage);
        mAdapter.setData(mListItems);
        //mAdapter = new GridAdapter(getActivity(), mListItems);
        //mGridView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public static PageFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

        int codigo = getCodTienda();
        mListItems = DataBaseBO.getAdapterCatalog(codigo, mPage);
        mAdapter = new GridAdapter(getActivity(), mListItems);

        /*mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_preview_size);
        //mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(getActivity(), IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(getActivity(), mImageThumbSize);
        //mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(getActivity().getSupportFragmentManager(), cacheParams);*/
    }

    private int getCodTienda() {
        if (codTienda == -1) {
            Activity activity = getActivity();
            if (activity instanceof CatalogoActivity) {
                CatalogoActivity catalogoActivity = (CatalogoActivity) activity;
                codTienda = catalogoActivity.getCodTienda();
            }
        }
        return codTienda;
    }

    /*
    //@Override
    public View onCreateView_(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        if (view instanceof RecyclerView) {
            Activity activity = getActivity();
            GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 3);

            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(30);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerView.setLayoutManager(gridLayoutManager);

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    // Pause fetcher to ensure smoother scrolling when flinging
                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                        // Before Honeycomb pause image loading on scroll to help with performance
                        if (!Utils.hasHoneycomb()) {
                            //mImageFetcher.setPauseWork(true);
                            Glide.with(PageFragment.this).pauseRequests();
                        }
                    } else {
                        //mImageFetcher.setPauseWork(false);
                        Glide.with(PageFragment.this).resumeRequests();
                    }
                }
            });

            List<ItemCardView> listItems = DataBaseBO.getAdapterCatalog(mPage);
            CatalogoAdapter adapter = new CatalogoAdapter(activity, listItems, this);
            recyclerView.setAdapter(adapter);
        }
        return view;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page_2, container, false);
        if (view instanceof GridView) {
            mGridView = (GridView) view;
            mGridView.setAdapter(mAdapter);
            mGridView.setOnItemClickListener(this);
            mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                    // Pause fetcher to ensure smoother scrolling when flinging
                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                        // Before Honeycomb pause image loading on scroll to help with performance
                        if (!Util.hasHoneycomb()) {
                            Glide.with(getActivity()).pauseRequests();
                        }
                    } else {
                        Glide.with(getActivity()).resumeRequests();
                    }
                }

                @Override
                public void onScroll(AbsListView absListView, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                }
            });
        }
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        ItemCardView producto = mListItems.get(position);
        boolean bloqueado=DataBaseBO.consultarProductoBloqueado(producto.codigo,Main.cliente.codigo);
        if (bloqueado){
            Util.MostrarAlertDialog(getActivity(),"El producto se encuentra bloqueado por el area Administrativa.");
            return;
        }else {
            showDialog(position);
        }
    }

    private void showDialog(final int position) {
        mPosition = position;
        ItemCardView item = mListItems.get(position);

        /*ProductDialogFragment dialogFragment = ProductDialogFragment.newInstance("Detalle Producto", item);
        dialogFragment.show(getFragmentManager(), "fragment_product_dialog");*/



        /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View myview = getLayoutInflater().inflate(R.layout.custom_dialog_layout, null);
        final AlertDialog alertDialog = builder.create();
        builder.setView(myview);
        alertDialog .show();*/

        /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Detalle Pedido")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = builder.create();


        View view = alertDialog.getLayoutInflater().inflate(R.layout.fragment_product_dialog, null);
        alertDialog.setView(view);
        //alertDialog.setMessage(mensaje);
        alertDialog.show();*/


        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_product_dialog, null);

        TextView lblPrecio = (TextView) view.findViewById(R.id.lblPrecio);

        if (item.precio == 0) {
          /*  LinearLayout linearPrice = (LinearLayout)view.findViewById(R.id.linearPrice);
            linearPrice.setVisibility(View.INVISIBLE);

            ImageView imgBarLeft = (ImageView)view.findViewById(R.id.imgBarLeft);
            imgBarLeft.setVisibility(View.INVISIBLE);

            ImageView imgBarRight = (ImageView)view.findViewById(R.id.imgBarRight);
            imgBarRight.setVisibility(View.INVISIBLE);

            lblPrecio.setVisibility(View.VISIBLE);*/

            lblPrecio.setText(item.subTitulo);
            lblPrecio.setTypeface(Font.getFont(getActivity()));

        } else {

            lblPrecio.setText(item.subTitulo);
            lblPrecio.setTypeface(Font.getFont(getActivity()));
        }

        TextView lblTitulo = (TextView) view.findViewById(R.id.lblDireccion);
        lblTitulo.setText(item.titulo);

        ImageButton imgAdd = (ImageButton) view.findViewById(R.id.imgAdd);
        imgAdd.setImageDrawable(createSelector(FontIconCatalogo.Icon.clw_plus));
        imgAdd.setOnClickListener(mOnPlusClick);

        ImageButton imgMinus = (ImageButton) view.findViewById(R.id.imgMinus);
        imgMinus.setImageDrawable(createSelector(FontIconCatalogo.Icon.clw_minus));
        imgMinus.setOnClickListener(mOnMinusClick);

        mPrecio = item.precio;
        mLblCantidad = (TextView) view.findViewById(R.id.lblCantidad);

        mTxtTotal = (TextView) view.findViewById(R.id.txtTotal);
        mTxtTotal.setTypeface(Font.getFont(getActivity()));
        updateTotal(item.cantidad);

        String picture = Const.URL_IMGS + item.codigo + ".png";
        //if (!TextUtils.isEmpty(item.picture)) {
            ImageView imgPreview = (ImageView) view.findViewById(R.id.imageView);
            if (imgPreview != null) {
                Glide.with(getActivity()).load(picture)
                        //.placeholder(R.drawable.ic_holder)
                        .dontAnimate()
                        //.thumbnail(1.0f)
                        .override(150, 150)
                        //.sizeMultiplier(1.0f)
                        //.crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgPreview);
            }
        //}

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Detalle Producto")
                .setView(view)
                .setCancelable(false)
                .setPositiveButton("Aceptar", mOnClickAceptar)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    private StateListDrawable createSelector(IIcon icon) {
        int sizeDp = 36;
        Drawable imgNormal = Util.createFontIcon(getActivity(), icon, R.color.colorPrimaryDark, sizeDp);
        Drawable imgPressed = Util.createFontIcon(getActivity(), icon, android.R.color.darker_gray, sizeDp);

        return Util.createSelector(imgNormal, imgPressed);
        //editText.setCompoundDrawablesWithIntrinsicBounds(selectorStates, null, null, null);
        //editText.setCompoundDrawablesRelative(selectorState null, null, null);
    }

    private void updateTotal(int cantidad) {
        //Actualiza la nueva cantidad
        Locale locale = new Locale("es_US");
        mLblCantidad.setText(String.format(locale, "%d", cantidad));

        //Actualiza el nuevo total
        double total = (double) cantidad * mPrecio;
        String strTotal = Util.setFormatoCOP(""+total);
        mTxtTotal.setText(strTotal);
    }

    private void updateTotalItems() {
        Activity activity = getActivity();
        if (activity instanceof CatalogoActivity) {
            CatalogoActivity catalogoActivity = (CatalogoActivity)activity;
            catalogoActivity.updateTotalItems();
        }
    }

    private DialogInterface.OnClickListener mOnClickAceptar = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            if (mLblCantidad != null) {
                String strCantidad = mLblCantidad.getText().toString();
                int cantidad = Integer.parseInt(strCantidad);

                ItemCardView item = mListItems.get(mPosition);
                boolean ok = false;

                if (cantidad>0) {
                    ok = AgregarProductoPedido(cantidad, 0, "", item);//DataBaseBO.validarProductoDetalle(getCodTienda(), item.codigo, item.precio, cantidad);
                } else{
                    Main.detallePedido.remove(item.codigo);
                    ok=true;
                }

                if (ok) {
                    item.cantidad = cantidad;
                    mAdapter.notifyDataSetChanged();
                    updateTotalItems();
                }
            }
            dialog.dismiss();
        }
    };

    private View.OnClickListener mOnMinusClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mLblCantidad != null) {
                String strCantidad = mLblCantidad.getText().toString();
                int cantidad = Integer.parseInt(strCantidad);
                if (cantidad > 0) {
                    cantidad--;
                    updateTotal(cantidad);
                }
            }
        }
    };

    private View.OnClickListener mOnPlusClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mLblCantidad != null) {
                String strCantidad = mLblCantidad.getText().toString();
                int cantidad = Integer.parseInt(strCantidad) + 1;
                updateTotal(cantidad);
            }
        }
    };

    private boolean AgregarProductoPedido(int cantidad, float descuento, String codMotivo,ItemCardView producto) {

        if (CatalogoActivity.tipoTransaccion == Const.IS_PRODUCTO_DANADO || CatalogoActivity.tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION) {

          //  AgregarProductoPedido2(cantidad, descuento, codMotivo);
            //return;

        }


        Cliente cliente;

        Detalle detalle = Main.detallePedido.get(producto.codigo);
        String precioProducto = "";

        if (detalle == null) {

            if (producto != null) {

                CatalogoActivity.tipoClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

                if (CatalogoActivity.tipoClienteSeleccionado == 1) {

                    cliente = DataBaseBO.CargarClienteSeleccionado();

                } else {

                    cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
                }

                if (CatalogoActivity.tipoTransaccionActual == CatalogoActivity.AUTOVENTA) {

                    if (CatalogoActivity.tipoTransaccion > 0 || CatalogoActivity.esCargue) {

                        cliente = new Cliente();
                        cliente.codigo = "0";
                    }

                }

                detalle = new Detalle();
                detalle.codCliente = cliente.codigo; // tipoTransaccionActual ==
                // AUTOVENTA &&
                // isAveriaTransporte ? "0" : cliente.codigo;
                detalle.codProducto = producto.codigo;
                detalle.nombProducto = producto.titulo;
                detalle.precio = (float) producto.precio;
                detalle.iva = producto.iva;
                detalle.descuento = descuento;
                detalle.cantidad = cantidad;
                detalle.tipo_pedido = Const.PEDIDO_VENTA;
                detalle.codMotivo = "0";
                detalle.inventario = producto.inventario;
                detalle.indice = producto.indice;

                detalle.codGrupo = producto.grupo;
                // detalle.tipoGrupo = DataBaseBO.tipoGrupo(producto.grupo);

                if (CatalogoActivity.cambio) {

                    //detalle.codMotivoCambio = motivoCambioSel.codigo;
                    //detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                    detalle.codMotivo = "1";
                }
            } else {

                AlertResetRegistrarProducto(getString(R.string.no_se_pudo_leer_la_informacion_del_producto));
            }
        } else {

            detalle.cantidad = cantidad;
            detalle.descuento = descuento;
            detalle.precio = (float) producto.precio;

            if (CatalogoActivity.cambio) {

               // detalle.codMotivoCambio = motivoCambioSel.codigo;
                //detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                detalle.codMotivo = "1";

            }
        }

        int posicion = Main.detallePedido.size();
        detalle.posicion = posicion;
        Main.detallePedido.put(producto.codigo, detalle);


        if (Main.detallePedido.containsKey(producto.codigo)){
            return true;
        }else {
            return false;
        }

    }

    public void AlertResetRegistrarProducto(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }

}