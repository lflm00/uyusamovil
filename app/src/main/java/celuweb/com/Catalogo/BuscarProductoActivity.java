package celuweb.com.Catalogo;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.mikepenz.iconics.typeface.IIcon;

import java.util.List;
import java.util.Locale;

import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.Cliente;
import celuweb.com.DataObject.Detalle;
import celuweb.com.uyusa.Const;
import celuweb.com.uyusa.Main;
import celuweb.com.uyusa.R;
import celuweb.com.uyusa.Util;
import celuweb.com.fonts.Font;
import celuweb.com.fonts.FontIconCatalogo;
import celuweb.com.Catalogo.widget.input.ValidatedTextInputLayout;
public class BuscarProductoActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    // Tag del Activity
    //private static final String TAG = BuscarProductoActivity.class.getName();

    private GridView mGridView;
    private GridAdapter mAdapter;
    private List<ItemCardView> mListItems;

    private double mPrecio;
    private TextView mLblCantidad;
    private TextView mTxtTotal;

    private int mPosition;
    private int codTienda = -1;
    private AlertDialog mAlertDialog;

    private ValidatedTextInputLayout txtLayoutBusqueda;
    private EditText mTxtBusqueda;

    //Permite controlar el evento de doble click
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_producto);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        showDialogPedido(position);
    }

    private void init() {
        setupToolbar();

        ImageButton imgButtonBarCode = (ImageButton) findViewById(R.id.imgButtonBarCode);
       // imgButtonBarCode.setOnClickListener(mOnClickBarCode);

        txtLayoutBusqueda = (ValidatedTextInputLayout) findViewById(R.id.txtLayoutBusqueda);
        mTxtBusqueda = (EditText) findViewById(R.id.txtBusqueda);
        mTxtBusqueda.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.buscar || id == EditorInfo.IME_NULL ||
                        id == EditorInfo.IME_ACTION_SEARCH || id == EditorInfo.IME_ACTION_GO) {
                    buscarProductos();
                    return true;
                }
                return false;
            }
        });

        mGridView = (GridView) findViewById(R.id.gridView);
        mGridView.setOnItemClickListener(this);
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                // Pause fetcher to ensure smoother scrolling when flinging
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    // Before Honeycomb pause image loading on scroll to help with performance
                    if (!Util.hasHoneycomb()) {
                        Glide.with(BuscarProductoActivity.this).pauseRequests();
                    }
                } else {
                    Glide.with(BuscarProductoActivity.this).resumeRequests();
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
            }
        });
    }

    public int getCodTienda() {
        if (codTienda == -1) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                codTienda = bundle.getInt("codTienda", -1);
            }
        }
        return codTienda;
    }

    private void showDialogPedido(final int position) {
        mPosition = position;
        ItemCardView item = mListItems.get(position);

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_product_dialog, null);

        TextView lblPrecio = (TextView) view.findViewById(R.id.lblPrecio);

        if (item.precio == 0) {
          /*  LinearLayout linearPrice = (LinearLayout)view.findViewById(R.id.linearPrice);
            linearPrice.setVisibility(View.INVISIBLE);

            ImageView imgBarLeft = (ImageView)view.findViewById(R.id.imgBarLeft);
            imgBarLeft.setVisibility(View.INVISIBLE);

            ImageView imgBarRight = (ImageView)view.findViewById(R.id.imgBarRight);
            imgBarRight.setVisibility(View.INVISIBLE);

            lblPrecio.setVisibility(View.VISIBLE);*/
            lblPrecio.setText(item.subTitulo);
            lblPrecio.setTypeface(Font.getFont(getApplicationContext()));

        } else {
            lblPrecio.setText(item.subTitulo);
            lblPrecio.setTypeface(Font.getFont(getApplicationContext()));
        }

        TextView lblTitulo = (TextView) view.findViewById(R.id.lblDireccion);
        lblTitulo.setText(item.titulo);

        ImageButton imgAdd = (ImageButton) view.findViewById(R.id.imgAdd);
        imgAdd.setImageDrawable(createSelector(FontIconCatalogo.Icon.clw_plus));
        imgAdd.setOnClickListener(mOnPlusClick);

        ImageButton imgMinus = (ImageButton) view.findViewById(R.id.imgMinus);
        imgMinus.setImageDrawable(createSelector(FontIconCatalogo.Icon.clw_minus));
        imgMinus.setOnClickListener(mOnMinusClick);

        mPrecio = item.precio;
        mLblCantidad = (TextView) view.findViewById(R.id.lblCantidad);

        //mPrecio = item.precio;
        mLblCantidad = (TextView) view.findViewById(R.id.lblCantidad);

        mTxtTotal = (TextView) view.findViewById(R.id.txtTotal);
        mTxtTotal.setTypeface(Font.getFont(this));
        updateTotal(item.cantidad);

        String picture = Const.URL_IMGS + item.codigo + ".png";
        ImageView imgPreview = (ImageView) view.findViewById(R.id.imageView);
        if (imgPreview != null) {
            Glide.with(this).load(picture)
                    //.placeholder(R.drawable.ic_holder)
                    .dontAnimate()
                    .override(150, 150)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgPreview);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Detalle Producto")
                .setView(view)
                .setCancelable(false)
                .setPositiveButton("Aceptar", mOnClickAceptar)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }

    private DialogInterface.OnClickListener mOnClickAceptar = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int id) {
            if (mLblCantidad != null) {
                String strCantidad = mLblCantidad.getText().toString();
                int cantidad = Integer.parseInt(strCantidad);

                ItemCardView item = mListItems.get(mPosition);
                boolean ok = false;

                if (cantidad>0) {
                    ok = AgregarProductoPedido(cantidad, 0, "", item);//DataBaseBO.validarProductoDetalle(getCodTienda(), item.codigo, item.precio, cantidad);
                } else{
                    Main.detallePedido.remove(item.codigo);
                    ok=true;
                }

                if (ok) {
                    item.cantidad = cantidad;
                    mAdapter.notifyDataSetChanged();
                }
            }
            dialog.dismiss();
        }
    };

    private View.OnClickListener mOnPlusClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mLblCantidad != null) {
                String strCantidad = mLblCantidad.getText().toString();
                int cantidad = Integer.parseInt(strCantidad) + 1;
                updateTotal(cantidad);
            }
        }
    };

    private View.OnClickListener mOnMinusClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mLblCantidad != null) {
                String strCantidad = mLblCantidad.getText().toString();
                int cantidad = Integer.parseInt(strCantidad);
                if (cantidad > 0) {
                    cantidad--;
                    updateTotal(cantidad);
                }
            }
        }
    };

    private void updateTotal(int cantidad) {
        //Actualiza la nueva cantidad
        mLblCantidad.setText(String.format(Locale.getDefault(), "%d", cantidad));

        //Actualiza el nuevo total
        double total = (double) cantidad * mPrecio;
        String strTotal = Util.setFormatoCOP(""+total);
        mTxtTotal.setText(strTotal);
    }

    private StateListDrawable createSelector(IIcon icon) {
        int sizeDp = 36;
        Drawable imgNormal = Util.createFontIcon(getApplicationContext(), icon, R.color.colorPrimaryDark, sizeDp);
        Drawable imgPressed = Util.createFontIcon(getApplicationContext(), icon, android.R.color.darker_gray, sizeDp);

        return Util.createSelector(imgNormal, imgPressed);
    }

    //Activar este metodo para guardar las sugerencias frecuentes de busqueda.
    /*private void loadDataSuggestions() {
        SearchRecentSuggestions suggestions =
                new SearchRecentSuggestions(this,
                        SampleRecentSuggestionsProvider.AUTHORITY,
                        SampleRecentSuggestionsProvider.MODE);

        suggestions.clearHistory();
        //suggestions.saveRecentQuery("papa", null);
        //suggestions.saveRecentQuery("panela", null);
    }*/

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

   /* private void loadMessages(Intent intent) {
        List<Notificacion> listItems = new ArrayList<>();
        UsuarioBO.cargarNotificaciones(listItems);

        int size = listItems.size();
        if (size > 0) {
            String titulo = "";
            String mensaje = "";
            for (Notificacion notificacion : listItems) {
                if (TextUtils.isEmpty(titulo)) {
                    titulo = notificacion.titulo;
                }

                if (TextUtils.isEmpty(mensaje)) {
                    mensaje += notificacion.mensaje;
                } else {
                    mensaje += "\n" + notificacion.mensaje;
                }
            }
            showNotificationMessage(titulo, mensaje);
            UsuarioBO.marcarNotificacionLeida(listItems);

        } else {
            if (intent != null) {
                managePushNotification(intent);
            }
        }
    }*/

    private void managePushNotification(@NonNull Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String title = bundle.getString("title");
            String message = bundle.getString("message");

            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(message)) {
                showNotificationMessage(title, message);
            }
        }
    }

    private synchronized void showNotificationMessage(String title, String message) {
        if (mAlertDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title)
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public synchronized void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            mAlertDialog = null;
                            //mCountMessage = 1;
                        }
                    });

            mAlertDialog = builder.create();
            mAlertDialog.setMessage(message);
            mAlertDialog.show();
        } else {
            //mAlertDialog.setTitle(title + " (" + ++mCountMessage + ")");
            String msg = "";
            final TextView textView = (TextView) mAlertDialog.findViewById(android.R.id.message);
            if (textView != null) {
                msg = textView.getText().toString();
            }
            mAlertDialog.setMessage(message + "\n" + msg);
        }
       // Util.playSoundNotification(getApplicationContext());
    }

    private void registerReceivers() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver((mMessageReceiver),
                new IntentFilter(Util.getFcmIntentName())
        );
    }

    private void buscarProductos() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 700) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        if (!txtLayoutBusqueda.validate()) {
            return;
        }

        int codigo = getCodTienda();
        String cadBusqueda = mTxtBusqueda.getText().toString();
        mListItems = DataBaseBO.buscarProducto(codigo, cadBusqueda);

        if (mAdapter == null) {
            mAdapter = new GridAdapter(this, mListItems);
            mGridView.setAdapter(mAdapter);
        } else {
            mAdapter.setData(mListItems);
            mAdapter.notifyDataSetChanged();
        }

        if (mListItems.size() == 0) {
            Toast.makeText(this, "Busqueda sin resultados!.", Toast.LENGTH_SHORT).show();
        } else {
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }



    private void buscarProductoPorCodigoBarras(String codigoBarras) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 700) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        int codigo = getCodTienda();
       // mListItems = DataBaseBO.buscarProductoPorCodigoBarras(codigo, codigoBarras);

        if (mAdapter == null) {
            mAdapter = new GridAdapter(this, mListItems);
            mGridView.setAdapter(mAdapter);
        } else {
            mAdapter.setData(mListItems);
            mAdapter.notifyDataSetChanged();
        }

        if (mListItems.size() == 0) {
            Toast.makeText(this, "Producto no encontrado!.", Toast.LENGTH_SHORT).show();
        } else {
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //loadMessages(intent);
        }
    };

    //private static final String BS_PACKAGE = "com.google.zxing.client.android";
   /* private View.OnClickListener mOnClickBarCode = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            IntentIntegrator intentIntegrator = new IntentIntegrator(BuscarProductoActivity.this);
            intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.PRODUCT_CODE_TYPES);
            intentIntegrator.setPrompt("Scan");
            intentIntegrator.setCameraId(0);
            intentIntegrator.setBeepEnabled(true);
            intentIntegrator.setBarcodeImageEnabled(false);
            intentIntegrator.initiateScan();
        }
    };*/

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
          /*  case IntentIntegrator.REQUEST_CODE: {
                if (resultCode != RESULT_CANCELED) {
                    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    String codigoBarras = scanResult.getContents();
                    if (!TextUtils.isEmpty(codigoBarras)) {
                        //mTxtBusqueda.setText(codigoBarras);
                        buscarProductoPorCodigoBarras(codigoBarras);
                    }
                }
                break;
            }*/
        }
    }

    private boolean AgregarProductoPedido(int cantidad, float descuento, String codMotivo,ItemCardView producto) {

        if (CatalogoActivity.tipoTransaccion == Const.IS_PRODUCTO_DANADO || CatalogoActivity.tipoTransaccion == Const.IS_INVENTARIO_LIQUIDACION) {

            //  AgregarProductoPedido2(cantidad, descuento, codMotivo);
            //return;

        }


        Cliente cliente;

        Detalle detalle = Main.detallePedido.get(producto.codigo);
        String precioProducto = "";

        if (detalle == null) {

            if (producto != null) {

                CatalogoActivity.tipoClienteSeleccionado = DataBaseBO.ObtenerTipoClienteSeleccionado();

                if (CatalogoActivity.tipoClienteSeleccionado == 1) {

                    cliente = DataBaseBO.CargarClienteSeleccionado();

                } else {

                    cliente = DataBaseBO.CargarClienteNuevoSeleccionado();
                }

                if (CatalogoActivity.tipoTransaccionActual == CatalogoActivity.AUTOVENTA) {

                    if (CatalogoActivity.tipoTransaccion > 0 || CatalogoActivity.esCargue) {

                        cliente = new Cliente();
                        cliente.codigo = "0";
                    }

                }

                detalle = new Detalle();
                detalle.codCliente = cliente.codigo; // tipoTransaccionActual ==
                // AUTOVENTA &&
                // isAveriaTransporte ? "0" : cliente.codigo;
                detalle.codProducto = producto.codigo;
                detalle.nombProducto = producto.titulo;
                detalle.precio = (float) producto.precio;
                detalle.iva = producto.iva;
                detalle.descuento = descuento;
                detalle.cantidad = cantidad;
                detalle.tipo_pedido = Const.PEDIDO_VENTA;
                detalle.codMotivo = "0";
                detalle.inventario = producto.inventario;
                detalle.indice = producto.indice;

                detalle.codGrupo = producto.grupo;
                // detalle.tipoGrupo = DataBaseBO.tipoGrupo(producto.grupo);

                if (CatalogoActivity.cambio) {

                    //detalle.codMotivoCambio = motivoCambioSel.codigo;
                    //detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                    detalle.codMotivo = "1";
                }
            } else {

                AlertResetRegistrarProducto(getString(R.string.no_se_pudo_leer_la_informacion_del_producto));
            }
        } else {

            detalle.cantidad = cantidad;
            detalle.descuento = descuento;
            detalle.precio = (float) producto.precio;

            if (CatalogoActivity.cambio) {

                // detalle.codMotivoCambio = motivoCambioSel.codigo;
                //detalle.descripcionMotivoCambio = motivoCambioSel.concepto;
                detalle.codMotivo = "1";

            }
        }

        int posicion = Main.detallePedido.size();
        detalle.posicion = posicion;
        Main.detallePedido.put(producto.codigo, detalle);


        if (Main.detallePedido.containsKey(producto.codigo)){
            return true;
        }else {
            return false;
        }

    }

    public void AlertResetRegistrarProducto(String mensaje) {

        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
        builder.setCancelable(false).setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(mensaje);
        alertDialog.show();
    }
}
