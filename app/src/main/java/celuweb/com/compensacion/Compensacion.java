package celuweb.com.compensacion;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import celuweb.com.BusinessObject.DataBaseBO;
import celuweb.com.DataObject.ProductosCompensacion;

public class Compensacion extends AsyncTask<String, Void, Boolean> {

	/**
	 * Contexto
	 */
	private Context context;

	/**
	 * Interface para generar evento de terminacion de calculo de compensacion.
	 */
	private ICompensacion iCompensacion;

	/**
	 * Dialog para mostrar al usuario mientras se hacen los calculos.
	 */
	private ProgressDialog progressDialog;

	/**
	 * Numero documento de la transaccion actual.
	 */
	private String numeroDocumento;

	/**
	 * Codigo del vendedor.
	 */
	private String codigoVendedor;
	
	/**
	 * Bodega del vendedor
	 */
	private String bodega;



	@Override
	protected void onPreExecute() {
		progressDialog = new ProgressDialog(getContext());
		progressDialog.setTitle("Espere...");
		progressDialog.setMessage("Calculando compensaciones.");
		progressDialog.setCancelable(false);
		progressDialog.show();
		super.onPreExecute();
	}



	public Compensacion(final Context context, final ICompensacion iCompensacion, final String numeroDocumento, String codigoVendedor, String bodega) {
		super();
		this.context = context;
		this.iCompensacion = iCompensacion;
		this.numeroDocumento = numeroDocumento;
		this.codigoVendedor = codigoVendedor;
		this.bodega = bodega;
	}



	@Override
	protected Boolean doInBackground(String... params) {
		return ejecutarAlgoritmoCompensacion();
	}



	@Override
	protected void onPostExecute(Boolean result) {
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
		this.iCompensacion.registrarCompensacionTerminada(result);
		super.onPostExecute(result);
	}



	/**
	 * Algoritmo para realizar la compensacion de productos faltantes y
	 * sobrantes en una liquidacion.
	 * 
	 * @return true si la ejecucion se completo correctamente, false en caso
	 *         contrario.
	 */
	private Boolean ejecutarAlgoritmoCompensacion() {
		boolean ejecucionCorrecta = true;
		/* Llenar listas de productos faltantes */
		ArrayList<ProductosCompensacion> listaFaltantes = new ArrayList<>();

		/* lista de detalles de compensacion */
		ArrayList<ProductosCompensacion> listaDetallesCompensacion = new ArrayList<>();

		DataBaseBO.llenarListaFaltantes(listaFaltantes);
		/* verificar que existan productos para compensar */
		if (!listaFaltantes.isEmpty()) {
			ejecucionCorrecta = DataBaseBO.iniciarCalculoDeCompensacion(listaFaltantes, listaDetallesCompensacion);
			/*Si la lista de detalles esta vacia, la ejecucion es correcta pero no habia productos para compensar*/
			if(listaDetallesCompensacion.isEmpty())
				ejecucionCorrecta = true;
			else
				generarEncabezadoYDetallesCompensacion(listaDetallesCompensacion);
		}
		return ejecucionCorrecta;
	}



	/**
	 * insertar el movimiento de la compensacion calculada en BD.
	 * @param listaDetallesCompensacion
	 */
	private void generarEncabezadoYDetallesCompensacion(final ArrayList<ProductosCompensacion> listaDetallesCompensacion) {
		DataBaseBO.generarEncabezadoYDetallesCompensacion(this.bodega, this.codigoVendedor, this.numeroDocumento, listaDetallesCompensacion);
	}



	/**
	 * @return the context
	 */
	public Context getContext() {
		return context;
	}



	/**
	 * @param context
	 *            the context to set
	 */
	public void setContext(Context context) {
		this.context = context;
	}
}
