package celuweb.com.compensacion;

public interface ICompensacion {

	/**
	 * se ejecuta cuando la compensacion ha finalizado.
	 */
	public void registrarCompensacionTerminada(boolean ejecucionTerminada);
}
